package edu.ustb.sei.mde.taileduml.diagram.providers;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.common.core.service.AbstractProvider;
import org.eclipse.gmf.runtime.common.core.service.IOperation;
import org.eclipse.gmf.runtime.common.ui.services.parser.GetParserOperation;
import org.eclipse.gmf.runtime.common.ui.services.parser.IParser;
import org.eclipse.gmf.runtime.common.ui.services.parser.IParserProvider;
import org.eclipse.gmf.runtime.common.ui.services.parser.ParserService;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.emf.ui.services.parser.ParserHintAdapter;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.gmf.tooling.runtime.parsers.EnumParser;

import edu.ustb.sei.mde.taileduml.TailedUMLPackage;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.AcceptActionNameEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.ActivityNameEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.AssignActionNameEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.CallActionNameEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.ClassNameEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.ConstraintTypeEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.ControlFlowConditionEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.EnumerationLiteralNameEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.EnumerationNameEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.InterfaceNameEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.ObjectNodeNameEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.OperationName2EditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.OperationNameEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.PackageNameEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.PrimitiveTypeInstanceClassEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.PrimitiveTypeNameEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.PropertyNameEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.ReferenceNameEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.SendActionNameEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.VariationActivityFragmentNameEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.VariationNameEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.VariationPointNameEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.VxActionNameEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.WrappingLabelEditPart;
import edu.ustb.sei.mde.taileduml.diagram.parsers.AssignActionLabelExpressionLabelParser;
import edu.ustb.sei.mde.taileduml.diagram.parsers.MessageFormatParser;
import edu.ustb.sei.mde.taileduml.diagram.part.TUMLVisualIDRegistry;
import edu.ustb.sei.mde.taileduml.vxmodel.VxModelPackage;

/**
 * @generated
 */
public class TUMLParserProvider extends AbstractProvider implements
		IParserProvider {

	/**
	 * @generated
	 */
	private IParser packageName_5008Parser;

	/**
	 * @generated
	 */
	private IParser getPackageName_5008Parser() {
		if (packageName_5008Parser == null) {
			EAttribute[] features = new EAttribute[] { TailedUMLPackage.eINSTANCE
					.getNamedElement_Name() };
			EAttribute[] editableFeatures = new EAttribute[] { TailedUMLPackage.eINSTANCE
					.getNamedElement_Name() };
			MessageFormatParser parser = new MessageFormatParser(features,
					editableFeatures);
			packageName_5008Parser = parser;
		}
		return packageName_5008Parser;
	}

	/**
	 * @generated
	 */
	private IParser activityName_5009Parser;

	/**
	 * @generated
	 */
	private IParser getActivityName_5009Parser() {
		if (activityName_5009Parser == null) {
			EAttribute[] features = new EAttribute[] { TailedUMLPackage.eINSTANCE
					.getNamedElement_Name() };
			MessageFormatParser parser = new MessageFormatParser(features);
			activityName_5009Parser = parser;
		}
		return activityName_5009Parser;
	}

	/**
	 * @generated
	 */
	private IParser primitiveTypeName_5010Parser;

	/**
	 * @generated
	 */
	private IParser getPrimitiveTypeName_5010Parser() {
		if (primitiveTypeName_5010Parser == null) {
			EAttribute[] features = new EAttribute[] { TailedUMLPackage.eINSTANCE
					.getNamedElement_Name() };
			EAttribute[] editableFeatures = new EAttribute[] { TailedUMLPackage.eINSTANCE
					.getNamedElement_Name() };
			MessageFormatParser parser = new MessageFormatParser(features,
					editableFeatures);
			primitiveTypeName_5010Parser = parser;
		}
		return primitiveTypeName_5010Parser;
	}

	/**
	 * @generated
	 */
	private IParser primitiveTypeInstanceClass_5011Parser;

	/**
	 * @generated
	 */
	private IParser getPrimitiveTypeInstanceClass_5011Parser() {
		if (primitiveTypeInstanceClass_5011Parser == null) {
			EAttribute[] features = new EAttribute[] { TailedUMLPackage.eINSTANCE
					.getPrimitiveType_InstanceClass() };
			EAttribute[] editableFeatures = new EAttribute[] { TailedUMLPackage.eINSTANCE
					.getPrimitiveType_InstanceClass() };
			MessageFormatParser parser = new MessageFormatParser(features,
					editableFeatures);
			primitiveTypeInstanceClass_5011Parser = parser;
		}
		return primitiveTypeInstanceClass_5011Parser;
	}

	/**
	 * @generated
	 */
	private IParser variationActivityFragmentName_5020Parser;

	/**
	 * @generated
	 */
	private IParser getVariationActivityFragmentName_5020Parser() {
		if (variationActivityFragmentName_5020Parser == null) {
			EAttribute[] features = new EAttribute[] { TailedUMLPackage.eINSTANCE
					.getNamedElement_Name() };
			MessageFormatParser parser = new MessageFormatParser(features);
			variationActivityFragmentName_5020Parser = parser;
		}
		return variationActivityFragmentName_5020Parser;
	}

	/**
	 * @generated
	 */
	private IParser className_5003Parser;

	/**
	 * @generated
	 */
	private IParser getClassName_5003Parser() {
		if (className_5003Parser == null) {
			EAttribute[] features = new EAttribute[] { TailedUMLPackage.eINSTANCE
					.getNamedElement_Name() };
			MessageFormatParser parser = new MessageFormatParser(features);
			className_5003Parser = parser;
		}
		return className_5003Parser;
	}

	/**
	 * @generated
	 */
	private IParser propertyName_5001Parser;

	/**
	 * @generated
	 */
	private IParser getPropertyName_5001Parser() {
		if (propertyName_5001Parser == null) {
			EAttribute[] features = new EAttribute[] { TailedUMLPackage.eINSTANCE
					.getNamedElement_Name() };
			MessageFormatParser parser = new MessageFormatParser(features);
			propertyName_5001Parser = parser;
		}
		return propertyName_5001Parser;
	}

	/**
	 * @generated
	 */
	private IParser operationName_5002Parser;

	/**
	 * @generated
	 */
	private IParser getOperationName_5002Parser() {
		if (operationName_5002Parser == null) {
			EAttribute[] features = new EAttribute[] { TailedUMLPackage.eINSTANCE
					.getNamedElement_Name() };
			MessageFormatParser parser = new MessageFormatParser(features);
			operationName_5002Parser = parser;
		}
		return operationName_5002Parser;
	}

	/**
	 * @generated
	 */
	private IParser enumerationName_5005Parser;

	/**
	 * @generated
	 */
	private IParser getEnumerationName_5005Parser() {
		if (enumerationName_5005Parser == null) {
			EAttribute[] features = new EAttribute[] { TailedUMLPackage.eINSTANCE
					.getNamedElement_Name() };
			MessageFormatParser parser = new MessageFormatParser(features);
			enumerationName_5005Parser = parser;
		}
		return enumerationName_5005Parser;
	}

	/**
	 * @generated
	 */
	private IParser enumerationLiteralName_5004Parser;

	/**
	 * @generated
	 */
	private IParser getEnumerationLiteralName_5004Parser() {
		if (enumerationLiteralName_5004Parser == null) {
			EAttribute[] features = new EAttribute[] { TailedUMLPackage.eINSTANCE
					.getNamedElement_Name() };
			MessageFormatParser parser = new MessageFormatParser(features);
			enumerationLiteralName_5004Parser = parser;
		}
		return enumerationLiteralName_5004Parser;
	}

	/**
	 * @generated
	 */
	private IParser interfaceName_5007Parser;

	/**
	 * @generated
	 */
	private IParser getInterfaceName_5007Parser() {
		if (interfaceName_5007Parser == null) {
			EAttribute[] features = new EAttribute[] { TailedUMLPackage.eINSTANCE
					.getNamedElement_Name() };
			EAttribute[] editableFeatures = new EAttribute[] { TailedUMLPackage.eINSTANCE
					.getNamedElement_Name() };
			MessageFormatParser parser = new MessageFormatParser(features,
					editableFeatures);
			interfaceName_5007Parser = parser;
		}
		return interfaceName_5007Parser;
	}

	/**
	 * @generated
	 */
	private IParser operationName_5006Parser;

	/**
	 * @generated
	 */
	private IParser getOperationName_5006Parser() {
		if (operationName_5006Parser == null) {
			EAttribute[] features = new EAttribute[] { TailedUMLPackage.eINSTANCE
					.getNamedElement_Name() };
			MessageFormatParser parser = new MessageFormatParser(features);
			operationName_5006Parser = parser;
		}
		return operationName_5006Parser;
	}

	/**
	 * @generated
	 */
	private IParser variationPointName_5013Parser;

	/**
	 * @generated
	 */
	private IParser getVariationPointName_5013Parser() {
		if (variationPointName_5013Parser == null) {
			EAttribute[] features = new EAttribute[] { TailedUMLPackage.eINSTANCE
					.getNamedElement_Name() };
			MessageFormatParser parser = new MessageFormatParser(features);
			variationPointName_5013Parser = parser;
		}
		return variationPointName_5013Parser;
	}

	/**
	 * @generated
	 */
	private IParser variationName_5014Parser;

	/**
	 * @generated
	 */
	private IParser getVariationName_5014Parser() {
		if (variationName_5014Parser == null) {
			EAttribute[] features = new EAttribute[] { TailedUMLPackage.eINSTANCE
					.getNamedElement_Name() };
			MessageFormatParser parser = new MessageFormatParser(features);
			variationName_5014Parser = parser;
		}
		return variationName_5014Parser;
	}

	/**
	 * @generated
	 */
	private IParser callActionName_5021Parser;

	/**
	 * @generated
	 */
	private IParser getCallActionName_5021Parser() {
		if (callActionName_5021Parser == null) {
			EAttribute[] features = new EAttribute[] { TailedUMLPackage.eINSTANCE
					.getNamedElement_Name() };
			MessageFormatParser parser = new MessageFormatParser(features);
			callActionName_5021Parser = parser;
		}
		return callActionName_5021Parser;
	}

	/**
	 * @generated
	 */
	private IParser sendActionName_5022Parser;

	/**
	 * @generated
	 */
	private IParser getSendActionName_5022Parser() {
		if (sendActionName_5022Parser == null) {
			EAttribute[] features = new EAttribute[] { TailedUMLPackage.eINSTANCE
					.getNamedElement_Name() };
			MessageFormatParser parser = new MessageFormatParser(features);
			sendActionName_5022Parser = parser;
		}
		return sendActionName_5022Parser;
	}

	/**
	 * @generated
	 */
	private IParser acceptActionName_5023Parser;

	/**
	 * @generated
	 */
	private IParser getAcceptActionName_5023Parser() {
		if (acceptActionName_5023Parser == null) {
			EAttribute[] features = new EAttribute[] { TailedUMLPackage.eINSTANCE
					.getNamedElement_Name() };
			MessageFormatParser parser = new MessageFormatParser(features);
			acceptActionName_5023Parser = parser;
		}
		return acceptActionName_5023Parser;
	}

	/**
	 * @generated
	 */
	private IParser vxActionName_5024Parser;

	/**
	 * @generated
	 */
	private IParser getVxActionName_5024Parser() {
		if (vxActionName_5024Parser == null) {
			EAttribute[] features = new EAttribute[] { TailedUMLPackage.eINSTANCE
					.getNamedElement_Name() };
			MessageFormatParser parser = new MessageFormatParser(features);
			vxActionName_5024Parser = parser;
		}
		return vxActionName_5024Parser;
	}

	/**
	 * @generated
	 */
	private IParser objectNodeName_5025Parser;

	/**
	 * @generated
	 */
	private IParser getObjectNodeName_5025Parser() {
		if (objectNodeName_5025Parser == null) {
			EAttribute[] features = new EAttribute[] { TailedUMLPackage.eINSTANCE
					.getNamedElement_Name() };
			MessageFormatParser parser = new MessageFormatParser(features);
			objectNodeName_5025Parser = parser;
		}
		return objectNodeName_5025Parser;
	}

	/**
	 * @generated
	 */
	private IParser getAssignActionLabel_5026Parser() {
		return new AssignActionLabelExpressionLabelParser();
	}

	/**
	 * @generated
	 */
	private IParser referenceName_6004Parser;

	/**
	 * @generated
	 */
	private IParser getReferenceName_6004Parser() {
		if (referenceName_6004Parser == null) {
			EAttribute[] features = new EAttribute[] { TailedUMLPackage.eINSTANCE
					.getNamedElement_Name() };
			MessageFormatParser parser = new MessageFormatParser(features);
			referenceName_6004Parser = parser;
		}
		return referenceName_6004Parser;
	}

	/**
	 * @generated
	 */
	private IParser constraintType_6001Parser;

	/**
	 * @generated
	 */
	private IParser getConstraintType_6001Parser() {
		if (constraintType_6001Parser == null) {
			EAttribute editableFeature = VxModelPackage.eINSTANCE
					.getConstraint_Type();
			EnumParser parser = new EnumParser(editableFeature);
			constraintType_6001Parser = parser;
		}
		return constraintType_6001Parser;
	}

	/**
	 * @generated
	 */
	private IParser controlFlowCondition_6002Parser;

	/**
	 * @generated
	 */
	private IParser getControlFlowCondition_6002Parser() {
		if (controlFlowCondition_6002Parser == null) {
			EAttribute[] features = new EAttribute[] { TailedUMLPackage.eINSTANCE
					.getControlFlow_Condition() };
			MessageFormatParser parser = new MessageFormatParser(features);
			parser.setViewPattern("[{0}]"); //$NON-NLS-1$
			parser.setEditorPattern("[{0}]"); //$NON-NLS-1$
			parser.setEditPattern("[{0}]"); //$NON-NLS-1$
			controlFlowCondition_6002Parser = parser;
		}
		return controlFlowCondition_6002Parser;
	}

	/**
	 * @generated
	 */
	protected IParser getParser(int visualID) {
		switch (visualID) {
		case PackageNameEditPart.VISUAL_ID:
			return getPackageName_5008Parser();
		case ActivityNameEditPart.VISUAL_ID:
			return getActivityName_5009Parser();
		case PrimitiveTypeNameEditPart.VISUAL_ID:
			return getPrimitiveTypeName_5010Parser();
		case PrimitiveTypeInstanceClassEditPart.VISUAL_ID:
			return getPrimitiveTypeInstanceClass_5011Parser();
		case VariationActivityFragmentNameEditPart.VISUAL_ID:
			return getVariationActivityFragmentName_5020Parser();
		case ClassNameEditPart.VISUAL_ID:
			return getClassName_5003Parser();
		case PropertyNameEditPart.VISUAL_ID:
			return getPropertyName_5001Parser();
		case OperationNameEditPart.VISUAL_ID:
			return getOperationName_5002Parser();
		case EnumerationNameEditPart.VISUAL_ID:
			return getEnumerationName_5005Parser();
		case EnumerationLiteralNameEditPart.VISUAL_ID:
			return getEnumerationLiteralName_5004Parser();
		case InterfaceNameEditPart.VISUAL_ID:
			return getInterfaceName_5007Parser();
		case OperationName2EditPart.VISUAL_ID:
			return getOperationName_5006Parser();
		case VariationPointNameEditPart.VISUAL_ID:
			return getVariationPointName_5013Parser();
		case VariationNameEditPart.VISUAL_ID:
			return getVariationName_5014Parser();
		case CallActionNameEditPart.VISUAL_ID:
			return getCallActionName_5021Parser();
		case SendActionNameEditPart.VISUAL_ID:
			return getSendActionName_5022Parser();
		case AcceptActionNameEditPart.VISUAL_ID:
			return getAcceptActionName_5023Parser();
		case VxActionNameEditPart.VISUAL_ID:
			return getVxActionName_5024Parser();
		case ObjectNodeNameEditPart.VISUAL_ID:
			return getObjectNodeName_5025Parser();
		case AssignActionNameEditPart.VISUAL_ID:
			return getAssignActionLabel_5026Parser();
		case ReferenceNameEditPart.VISUAL_ID:
			return getReferenceName_6004Parser();

		case ConstraintTypeEditPart.VISUAL_ID:
			return getConstraintType_6001Parser();
		case ControlFlowConditionEditPart.VISUAL_ID:
			return getControlFlowCondition_6002Parser();
		}
		return null;
	}

	/**
	 * Utility method that consults ParserService
	 * @generated
	 */
	public static IParser getParser(IElementType type, EObject object,
			String parserHint) {
		return ParserService.getInstance().getParser(
				new HintAdapter(type, object, parserHint));
	}

	/**
	 * @generated
	 */
	public IParser getParser(IAdaptable hint) {
		String vid = (String) hint.getAdapter(String.class);
		if (vid != null) {
			return getParser(TUMLVisualIDRegistry.getVisualID(vid));
		}
		View view = (View) hint.getAdapter(View.class);
		if (view != null) {
			return getParser(TUMLVisualIDRegistry.getVisualID(view));
		}
		return null;
	}

	/**
	 * @generated
	 */
	public boolean provides(IOperation operation) {
		if (operation instanceof GetParserOperation) {
			IAdaptable hint = ((GetParserOperation) operation).getHint();
			if (TUMLElementTypes.getElement(hint) == null) {
				return false;
			}
			return getParser(hint) != null;
		}
		return false;
	}

	/**
	 * @generated
	 */
	private static class HintAdapter extends ParserHintAdapter {

		/**
		 * @generated
		 */
		private final IElementType elementType;

		/**
		 * @generated
		 */
		public HintAdapter(IElementType type, EObject object, String parserHint) {
			super(object, parserHint);
			assert type != null;
			elementType = type;
		}

		/**
		 * @generated
		 */
		public Object getAdapter(Class adapter) {
			if (IElementType.class.equals(adapter)) {
				return elementType;
			}
			return super.getAdapter(adapter);
		}
	}

}
