package edu.ustb.sei.mde.taileduml.diagram.edit.policies;

import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;

import edu.ustb.sei.mde.taileduml.diagram.edit.commands.ClassCreateCommand;
import edu.ustb.sei.mde.taileduml.diagram.edit.commands.EnumerationCreateCommand;
import edu.ustb.sei.mde.taileduml.diagram.edit.commands.InterfaceCreateCommand;
import edu.ustb.sei.mde.taileduml.diagram.edit.commands.VariationCreateCommand;
import edu.ustb.sei.mde.taileduml.diagram.edit.commands.VariationPointCreateCommand;
import edu.ustb.sei.mde.taileduml.diagram.providers.TUMLElementTypes;

/**
 * @generated
 */
public class PackagePackageCompartmentItemSemanticEditPolicy extends
		TUMLBaseItemSemanticEditPolicy {

	/**
	 * @generated
	 */
	public PackagePackageCompartmentItemSemanticEditPolicy() {
		super(TUMLElementTypes.Package_2001);
	}

	/**
	 * @generated
	 */
	protected Command getCreateCommand(CreateElementRequest req) {
		if (TUMLElementTypes.Class_3001 == req.getElementType()) {
			return getGEFWrapper(new ClassCreateCommand(req));
		}
		if (TUMLElementTypes.Enumeration_3004 == req.getElementType()) {
			return getGEFWrapper(new EnumerationCreateCommand(req));
		}
		if (TUMLElementTypes.Interface_3006 == req.getElementType()) {
			return getGEFWrapper(new InterfaceCreateCommand(req));
		}
		if (TUMLElementTypes.VariationPoint_3015 == req.getElementType()) {
			return getGEFWrapper(new VariationPointCreateCommand(req));
		}
		if (TUMLElementTypes.Variation_3016 == req.getElementType()) {
			return getGEFWrapper(new VariationCreateCommand(req));
		}
		return super.getCreateCommand(req);
	}

}
