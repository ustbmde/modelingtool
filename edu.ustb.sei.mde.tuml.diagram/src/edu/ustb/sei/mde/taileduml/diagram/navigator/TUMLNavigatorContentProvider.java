package edu.ustb.sei.mde.taileduml.diagram.navigator;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;

import org.eclipse.core.resources.IFile;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.edit.domain.AdapterFactoryEditingDomain;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.workspace.util.WorkspaceSynchronizer;
import org.eclipse.gmf.runtime.emf.core.GMFEditingDomainFactory;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.gmf.runtime.notation.Edge;
import org.eclipse.gmf.runtime.notation.Node;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.ui.IMemento;
import org.eclipse.ui.navigator.ICommonContentExtensionSite;
import org.eclipse.ui.navigator.ICommonContentProvider;

import edu.ustb.sei.mde.taileduml.diagram.edit.parts.AcceptActionEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.ActivityActivityCompartmentEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.ActivityEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.ActivityFinalNodeEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.ActivityRelatedOperationEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.AssignActionEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.CallActionEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.CallActionMethodEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.ClassClassAttributeCompartmentEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.ClassClassOperationCompartmentEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.ClassEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.ConstraintEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.ControlFlowEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.DecisionNodeEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.EmptyActionEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.EnumerationEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.EnumerationEnumerationCompartmentEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.EnumerationLiteralEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.ForkNodeEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.GeneralizationEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.InitialNodeEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.InterfaceEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.InterfaceInterfaceOperationCompartmentEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.JoinNodeEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.MergeNodeEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.ModelEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.ObjectFlowEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.ObjectNodeEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.Operation2EditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.OperationEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.PackageEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.PackagePackageCompartmentEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.PrimitiveTypeEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.PropertyEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.RealizationEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.ReferenceEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.SendActionEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.VariationActivityFragmentEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.VariationActivityFragmentVariationActivityFragmentCompartmentEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.VariationEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.VariationPointEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.VariationPointVariationsEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.VxActionEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.VxActionVariantFragmentsEditPart;
import edu.ustb.sei.mde.taileduml.diagram.part.Messages;
import edu.ustb.sei.mde.taileduml.diagram.part.TUMLVisualIDRegistry;

/**
 * @generated
 */
public class TUMLNavigatorContentProvider implements ICommonContentProvider {

	/**
	 * @generated
	 */
	private static final Object[] EMPTY_ARRAY = new Object[0];

	/**
	 * @generated
	 */
	private Viewer myViewer;

	/**
	 * @generated
	 */
	private AdapterFactoryEditingDomain myEditingDomain;

	/**
	 * @generated
	 */
	private WorkspaceSynchronizer myWorkspaceSynchronizer;

	/**
	 * @generated
	 */
	private Runnable myViewerRefreshRunnable;

	/**
	 * @generated
	 */
	@SuppressWarnings({ "unchecked", "serial", "rawtypes" })
	public TUMLNavigatorContentProvider() {
		TransactionalEditingDomain editingDomain = GMFEditingDomainFactory.INSTANCE
				.createEditingDomain();
		myEditingDomain = (AdapterFactoryEditingDomain) editingDomain;
		myEditingDomain.setResourceToReadOnlyMap(new HashMap() {
			public Object get(Object key) {
				if (!containsKey(key)) {
					put(key, Boolean.TRUE);
				}
				return super.get(key);
			}
		});
		myViewerRefreshRunnable = new Runnable() {
			public void run() {
				if (myViewer != null) {
					myViewer.refresh();
				}
			}
		};
		myWorkspaceSynchronizer = new WorkspaceSynchronizer(editingDomain,
				new WorkspaceSynchronizer.Delegate() {
					public void dispose() {
					}

					public boolean handleResourceChanged(final Resource resource) {
						unloadAllResources();
						asyncRefresh();
						return true;
					}

					public boolean handleResourceDeleted(Resource resource) {
						unloadAllResources();
						asyncRefresh();
						return true;
					}

					public boolean handleResourceMoved(Resource resource,
							final URI newURI) {
						unloadAllResources();
						asyncRefresh();
						return true;
					}
				});
	}

	/**
	 * @generated
	 */
	public void dispose() {
		myWorkspaceSynchronizer.dispose();
		myWorkspaceSynchronizer = null;
		myViewerRefreshRunnable = null;
		myViewer = null;
		unloadAllResources();
		((TransactionalEditingDomain) myEditingDomain).dispose();
		myEditingDomain = null;
	}

	/**
	 * @generated
	 */
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		myViewer = viewer;
	}

	/**
	 * @generated
	 */
	void unloadAllResources() {
		for (Resource nextResource : myEditingDomain.getResourceSet()
				.getResources()) {
			nextResource.unload();
		}
	}

	/**
	 * @generated
	 */
	void asyncRefresh() {
		if (myViewer != null && !myViewer.getControl().isDisposed()) {
			myViewer.getControl().getDisplay()
					.asyncExec(myViewerRefreshRunnable);
		}
	}

	/**
	 * @generated
	 */
	public Object[] getElements(Object inputElement) {
		return getChildren(inputElement);
	}

	/**
	 * @generated
	 */
	public void restoreState(IMemento aMemento) {
	}

	/**
	 * @generated
	 */
	public void saveState(IMemento aMemento) {
	}

	/**
	 * @generated
	 */
	public void init(ICommonContentExtensionSite aConfig) {
	}

	/**
	 * @generated
	 */
	public Object[] getChildren(Object parentElement) {
		if (parentElement instanceof IFile) {
			IFile file = (IFile) parentElement;
			URI fileURI = URI.createPlatformResourceURI(file.getFullPath()
					.toString(), true);
			Resource resource = myEditingDomain.getResourceSet().getResource(
					fileURI, true);
			ArrayList<TUMLNavigatorItem> result = new ArrayList<TUMLNavigatorItem>();
			ArrayList<View> topViews = new ArrayList<View>(resource
					.getContents().size());
			for (EObject o : resource.getContents()) {
				if (o instanceof View) {
					topViews.add((View) o);
				}
			}
			result.addAll(createNavigatorItems(
					selectViewsByType(topViews, ModelEditPart.MODEL_ID), file,
					false));
			return result.toArray();
		}

		if (parentElement instanceof TUMLNavigatorGroup) {
			TUMLNavigatorGroup group = (TUMLNavigatorGroup) parentElement;
			return group.getChildren();
		}

		if (parentElement instanceof TUMLNavigatorItem) {
			TUMLNavigatorItem navigatorItem = (TUMLNavigatorItem) parentElement;
			if (navigatorItem.isLeaf() || !isOwnView(navigatorItem.getView())) {
				return EMPTY_ARRAY;
			}
			return getViewChildren(navigatorItem.getView(), parentElement);
		}

		return EMPTY_ARRAY;
	}

	/**
	 * @generated
	 */
	private Object[] getViewChildren(View view, Object parentElement) {
		switch (TUMLVisualIDRegistry.getVisualID(view)) {

		case ModelEditPart.VISUAL_ID: {
			LinkedList<TUMLAbstractNavigatorItem> result = new LinkedList<TUMLAbstractNavigatorItem>();
			Diagram sv = (Diagram) view;
			TUMLNavigatorGroup links = new TUMLNavigatorGroup(
					Messages.NavigatorGroupName_Model_1000_links,
					"icons/linksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getChildrenByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(PackageEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(ActivityEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(Collections.singleton(sv),
					TUMLVisualIDRegistry
							.getType(PrimitiveTypeEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					TUMLVisualIDRegistry
							.getType(VariationActivityFragmentEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getDiagramLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(RealizationEditPart.VISUAL_ID));
			links.addChildren(createNavigatorItems(connectedViews, links, false));
			connectedViews = getDiagramLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry
							.getType(GeneralizationEditPart.VISUAL_ID));
			links.addChildren(createNavigatorItems(connectedViews, links, false));
			connectedViews = getDiagramLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(ReferenceEditPart.VISUAL_ID));
			links.addChildren(createNavigatorItems(connectedViews, links, false));
			connectedViews = getDiagramLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(ConstraintEditPart.VISUAL_ID));
			links.addChildren(createNavigatorItems(connectedViews, links, false));
			connectedViews = getDiagramLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(ControlFlowEditPart.VISUAL_ID));
			links.addChildren(createNavigatorItems(connectedViews, links, false));
			connectedViews = getDiagramLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(ObjectFlowEditPart.VISUAL_ID));
			links.addChildren(createNavigatorItems(connectedViews, links, false));
			connectedViews = getDiagramLinksByType(
					Collections.singleton(sv),
					TUMLVisualIDRegistry
							.getType(ActivityRelatedOperationEditPart.VISUAL_ID));
			links.addChildren(createNavigatorItems(connectedViews, links, false));
			connectedViews = getDiagramLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry
							.getType(CallActionMethodEditPart.VISUAL_ID));
			links.addChildren(createNavigatorItems(connectedViews, links, false));
			connectedViews = getDiagramLinksByType(
					Collections.singleton(sv),
					TUMLVisualIDRegistry
							.getType(VxActionVariantFragmentsEditPart.VISUAL_ID));
			links.addChildren(createNavigatorItems(connectedViews, links, false));
			connectedViews = getDiagramLinksByType(
					Collections.singleton(sv),
					TUMLVisualIDRegistry
							.getType(VariationPointVariationsEditPart.VISUAL_ID));
			links.addChildren(createNavigatorItems(connectedViews, links, false));
			if (!links.isEmpty()) {
				result.add(links);
			}
			return result.toArray();
		}

		case PackageEditPart.VISUAL_ID: {
			LinkedList<TUMLAbstractNavigatorItem> result = new LinkedList<TUMLAbstractNavigatorItem>();
			Node sv = (Node) view;
			Collection<View> connectedViews;
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					TUMLVisualIDRegistry
							.getType(PackagePackageCompartmentEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					TUMLVisualIDRegistry.getType(ClassEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					TUMLVisualIDRegistry
							.getType(PackagePackageCompartmentEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					TUMLVisualIDRegistry.getType(EnumerationEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					TUMLVisualIDRegistry
							.getType(PackagePackageCompartmentEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					TUMLVisualIDRegistry.getType(InterfaceEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					TUMLVisualIDRegistry
							.getType(PackagePackageCompartmentEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					TUMLVisualIDRegistry
							.getType(VariationPointEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					TUMLVisualIDRegistry
							.getType(PackagePackageCompartmentEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					TUMLVisualIDRegistry.getType(VariationEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			return result.toArray();
		}

		case ActivityEditPart.VISUAL_ID: {
			LinkedList<TUMLAbstractNavigatorItem> result = new LinkedList<TUMLAbstractNavigatorItem>();
			Node sv = (Node) view;
			TUMLNavigatorGroup outgoinglinks = new TUMLNavigatorGroup(
					Messages.NavigatorGroupName_Activity_2002_outgoinglinks,
					"icons/outgoingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					TUMLVisualIDRegistry
							.getType(ActivityActivityCompartmentEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					TUMLVisualIDRegistry
							.getType(DecisionNodeEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					TUMLVisualIDRegistry
							.getType(ActivityActivityCompartmentEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					TUMLVisualIDRegistry.getType(MergeNodeEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					TUMLVisualIDRegistry
							.getType(ActivityActivityCompartmentEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					TUMLVisualIDRegistry.getType(InitialNodeEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					TUMLVisualIDRegistry
							.getType(ActivityActivityCompartmentEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					TUMLVisualIDRegistry
							.getType(ActivityFinalNodeEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					TUMLVisualIDRegistry
							.getType(ActivityActivityCompartmentEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					TUMLVisualIDRegistry.getType(ForkNodeEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					TUMLVisualIDRegistry
							.getType(ActivityActivityCompartmentEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					TUMLVisualIDRegistry.getType(JoinNodeEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					TUMLVisualIDRegistry
							.getType(ActivityActivityCompartmentEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					TUMLVisualIDRegistry.getType(CallActionEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					TUMLVisualIDRegistry
							.getType(ActivityActivityCompartmentEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					TUMLVisualIDRegistry.getType(SendActionEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					TUMLVisualIDRegistry
							.getType(ActivityActivityCompartmentEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					TUMLVisualIDRegistry
							.getType(AcceptActionEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					TUMLVisualIDRegistry
							.getType(ActivityActivityCompartmentEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					TUMLVisualIDRegistry.getType(VxActionEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					TUMLVisualIDRegistry
							.getType(ActivityActivityCompartmentEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					TUMLVisualIDRegistry.getType(ObjectNodeEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					TUMLVisualIDRegistry
							.getType(ActivityActivityCompartmentEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					TUMLVisualIDRegistry
							.getType(AssignActionEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					TUMLVisualIDRegistry
							.getType(ActivityActivityCompartmentEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					TUMLVisualIDRegistry.getType(EmptyActionEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getOutgoingLinksByType(
					Collections.singleton(sv),
					TUMLVisualIDRegistry
							.getType(ActivityRelatedOperationEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			if (!outgoinglinks.isEmpty()) {
				result.add(outgoinglinks);
			}
			return result.toArray();
		}

		case VariationActivityFragmentEditPart.VISUAL_ID: {
			LinkedList<TUMLAbstractNavigatorItem> result = new LinkedList<TUMLAbstractNavigatorItem>();
			Node sv = (Node) view;
			TUMLNavigatorGroup incominglinks = new TUMLNavigatorGroup(
					Messages.NavigatorGroupName_VariationActivityFragment_2004_incominglinks,
					"icons/incomingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					TUMLVisualIDRegistry
							.getType(VariationActivityFragmentVariationActivityFragmentCompartmentEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					TUMLVisualIDRegistry.getType(InitialNodeEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					TUMLVisualIDRegistry
							.getType(VariationActivityFragmentVariationActivityFragmentCompartmentEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					TUMLVisualIDRegistry
							.getType(ActivityFinalNodeEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					TUMLVisualIDRegistry
							.getType(VariationActivityFragmentVariationActivityFragmentCompartmentEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					TUMLVisualIDRegistry
							.getType(DecisionNodeEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					TUMLVisualIDRegistry
							.getType(VariationActivityFragmentVariationActivityFragmentCompartmentEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					TUMLVisualIDRegistry.getType(MergeNodeEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					TUMLVisualIDRegistry
							.getType(VariationActivityFragmentVariationActivityFragmentCompartmentEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					TUMLVisualIDRegistry.getType(ForkNodeEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					TUMLVisualIDRegistry
							.getType(VariationActivityFragmentVariationActivityFragmentCompartmentEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					TUMLVisualIDRegistry.getType(JoinNodeEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					TUMLVisualIDRegistry
							.getType(VariationActivityFragmentVariationActivityFragmentCompartmentEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					TUMLVisualIDRegistry.getType(VxActionEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					TUMLVisualIDRegistry
							.getType(VariationActivityFragmentVariationActivityFragmentCompartmentEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					TUMLVisualIDRegistry.getType(SendActionEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					TUMLVisualIDRegistry
							.getType(VariationActivityFragmentVariationActivityFragmentCompartmentEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					TUMLVisualIDRegistry.getType(CallActionEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					TUMLVisualIDRegistry
							.getType(VariationActivityFragmentVariationActivityFragmentCompartmentEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					TUMLVisualIDRegistry
							.getType(AcceptActionEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					TUMLVisualIDRegistry
							.getType(VariationActivityFragmentVariationActivityFragmentCompartmentEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					TUMLVisualIDRegistry.getType(ObjectNodeEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					TUMLVisualIDRegistry
							.getType(VariationActivityFragmentVariationActivityFragmentCompartmentEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					TUMLVisualIDRegistry
							.getType(AssignActionEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					TUMLVisualIDRegistry
							.getType(VariationActivityFragmentVariationActivityFragmentCompartmentEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					TUMLVisualIDRegistry.getType(EmptyActionEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getIncomingLinksByType(
					Collections.singleton(sv),
					TUMLVisualIDRegistry
							.getType(VxActionVariantFragmentsEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			if (!incominglinks.isEmpty()) {
				result.add(incominglinks);
			}
			return result.toArray();
		}

		case ClassEditPart.VISUAL_ID: {
			LinkedList<TUMLAbstractNavigatorItem> result = new LinkedList<TUMLAbstractNavigatorItem>();
			Node sv = (Node) view;
			TUMLNavigatorGroup incominglinks = new TUMLNavigatorGroup(
					Messages.NavigatorGroupName_Class_3001_incominglinks,
					"icons/incomingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			TUMLNavigatorGroup outgoinglinks = new TUMLNavigatorGroup(
					Messages.NavigatorGroupName_Class_3001_outgoinglinks,
					"icons/outgoingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					TUMLVisualIDRegistry
							.getType(ClassClassAttributeCompartmentEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					TUMLVisualIDRegistry.getType(PropertyEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					TUMLVisualIDRegistry
							.getType(ClassClassOperationCompartmentEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					TUMLVisualIDRegistry.getType(OperationEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getIncomingLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(RealizationEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(RealizationEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			connectedViews = getIncomingLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry
							.getType(GeneralizationEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry
							.getType(GeneralizationEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			connectedViews = getIncomingLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(ReferenceEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(ReferenceEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			connectedViews = getIncomingLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(ConstraintEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(ConstraintEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			if (!incominglinks.isEmpty()) {
				result.add(incominglinks);
			}
			if (!outgoinglinks.isEmpty()) {
				result.add(outgoinglinks);
			}
			return result.toArray();
		}

		case OperationEditPart.VISUAL_ID: {
			LinkedList<TUMLAbstractNavigatorItem> result = new LinkedList<TUMLAbstractNavigatorItem>();
			Node sv = (Node) view;
			TUMLNavigatorGroup incominglinks = new TUMLNavigatorGroup(
					Messages.NavigatorGroupName_Operation_3003_incominglinks,
					"icons/incomingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getIncomingLinksByType(
					Collections.singleton(sv),
					TUMLVisualIDRegistry
							.getType(ActivityRelatedOperationEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getIncomingLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry
							.getType(CallActionMethodEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			if (!incominglinks.isEmpty()) {
				result.add(incominglinks);
			}
			return result.toArray();
		}

		case EnumerationEditPart.VISUAL_ID: {
			LinkedList<TUMLAbstractNavigatorItem> result = new LinkedList<TUMLAbstractNavigatorItem>();
			Node sv = (Node) view;
			Collection<View> connectedViews;
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					TUMLVisualIDRegistry
							.getType(EnumerationEnumerationCompartmentEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					TUMLVisualIDRegistry
							.getType(EnumerationLiteralEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			return result.toArray();
		}

		case InterfaceEditPart.VISUAL_ID: {
			LinkedList<TUMLAbstractNavigatorItem> result = new LinkedList<TUMLAbstractNavigatorItem>();
			Node sv = (Node) view;
			TUMLNavigatorGroup incominglinks = new TUMLNavigatorGroup(
					Messages.NavigatorGroupName_Interface_3006_incominglinks,
					"icons/incomingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			TUMLNavigatorGroup outgoinglinks = new TUMLNavigatorGroup(
					Messages.NavigatorGroupName_Interface_3006_outgoinglinks,
					"icons/outgoingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getChildrenByType(
					Collections.singleton(sv),
					TUMLVisualIDRegistry
							.getType(InterfaceInterfaceOperationCompartmentEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					TUMLVisualIDRegistry.getType(Operation2EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getIncomingLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(RealizationEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(RealizationEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			connectedViews = getIncomingLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry
							.getType(GeneralizationEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry
							.getType(GeneralizationEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			connectedViews = getIncomingLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(ReferenceEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(ReferenceEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			connectedViews = getIncomingLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(ConstraintEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(ConstraintEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			if (!incominglinks.isEmpty()) {
				result.add(incominglinks);
			}
			if (!outgoinglinks.isEmpty()) {
				result.add(outgoinglinks);
			}
			return result.toArray();
		}

		case Operation2EditPart.VISUAL_ID: {
			LinkedList<TUMLAbstractNavigatorItem> result = new LinkedList<TUMLAbstractNavigatorItem>();
			Node sv = (Node) view;
			TUMLNavigatorGroup incominglinks = new TUMLNavigatorGroup(
					Messages.NavigatorGroupName_Operation_3007_incominglinks,
					"icons/incomingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getIncomingLinksByType(
					Collections.singleton(sv),
					TUMLVisualIDRegistry
							.getType(ActivityRelatedOperationEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getIncomingLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry
							.getType(CallActionMethodEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			if (!incominglinks.isEmpty()) {
				result.add(incominglinks);
			}
			return result.toArray();
		}

		case VariationPointEditPart.VISUAL_ID: {
			LinkedList<TUMLAbstractNavigatorItem> result = new LinkedList<TUMLAbstractNavigatorItem>();
			Node sv = (Node) view;
			TUMLNavigatorGroup incominglinks = new TUMLNavigatorGroup(
					Messages.NavigatorGroupName_VariationPoint_3015_incominglinks,
					"icons/incomingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			TUMLNavigatorGroup outgoinglinks = new TUMLNavigatorGroup(
					Messages.NavigatorGroupName_VariationPoint_3015_outgoinglinks,
					"icons/outgoingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getIncomingLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(RealizationEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(RealizationEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			connectedViews = getIncomingLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry
							.getType(GeneralizationEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry
							.getType(GeneralizationEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			connectedViews = getIncomingLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(ReferenceEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(ReferenceEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			connectedViews = getIncomingLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(ConstraintEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(ConstraintEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			connectedViews = getOutgoingLinksByType(
					Collections.singleton(sv),
					TUMLVisualIDRegistry
							.getType(VariationPointVariationsEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			if (!incominglinks.isEmpty()) {
				result.add(incominglinks);
			}
			if (!outgoinglinks.isEmpty()) {
				result.add(outgoinglinks);
			}
			return result.toArray();
		}

		case VariationEditPart.VISUAL_ID: {
			LinkedList<TUMLAbstractNavigatorItem> result = new LinkedList<TUMLAbstractNavigatorItem>();
			Node sv = (Node) view;
			TUMLNavigatorGroup incominglinks = new TUMLNavigatorGroup(
					Messages.NavigatorGroupName_Variation_3016_incominglinks,
					"icons/incomingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			TUMLNavigatorGroup outgoinglinks = new TUMLNavigatorGroup(
					Messages.NavigatorGroupName_Variation_3016_outgoinglinks,
					"icons/outgoingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getIncomingLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(RealizationEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(RealizationEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			connectedViews = getIncomingLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry
							.getType(GeneralizationEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry
							.getType(GeneralizationEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			connectedViews = getIncomingLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(ReferenceEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(ReferenceEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			connectedViews = getIncomingLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(ConstraintEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(ConstraintEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			connectedViews = getIncomingLinksByType(
					Collections.singleton(sv),
					TUMLVisualIDRegistry
							.getType(VariationPointVariationsEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			if (!incominglinks.isEmpty()) {
				result.add(incominglinks);
			}
			if (!outgoinglinks.isEmpty()) {
				result.add(outgoinglinks);
			}
			return result.toArray();
		}

		case DecisionNodeEditPart.VISUAL_ID: {
			LinkedList<TUMLAbstractNavigatorItem> result = new LinkedList<TUMLAbstractNavigatorItem>();
			Node sv = (Node) view;
			TUMLNavigatorGroup incominglinks = new TUMLNavigatorGroup(
					Messages.NavigatorGroupName_DecisionNode_3021_incominglinks,
					"icons/incomingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			TUMLNavigatorGroup outgoinglinks = new TUMLNavigatorGroup(
					Messages.NavigatorGroupName_DecisionNode_3021_outgoinglinks,
					"icons/outgoingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getIncomingLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(ControlFlowEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(ControlFlowEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			connectedViews = getIncomingLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(ObjectFlowEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(ObjectFlowEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			if (!incominglinks.isEmpty()) {
				result.add(incominglinks);
			}
			if (!outgoinglinks.isEmpty()) {
				result.add(outgoinglinks);
			}
			return result.toArray();
		}

		case MergeNodeEditPart.VISUAL_ID: {
			LinkedList<TUMLAbstractNavigatorItem> result = new LinkedList<TUMLAbstractNavigatorItem>();
			Node sv = (Node) view;
			TUMLNavigatorGroup incominglinks = new TUMLNavigatorGroup(
					Messages.NavigatorGroupName_MergeNode_3022_incominglinks,
					"icons/incomingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			TUMLNavigatorGroup outgoinglinks = new TUMLNavigatorGroup(
					Messages.NavigatorGroupName_MergeNode_3022_outgoinglinks,
					"icons/outgoingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getIncomingLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(ControlFlowEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(ControlFlowEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			connectedViews = getIncomingLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(ObjectFlowEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(ObjectFlowEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			if (!incominglinks.isEmpty()) {
				result.add(incominglinks);
			}
			if (!outgoinglinks.isEmpty()) {
				result.add(outgoinglinks);
			}
			return result.toArray();
		}

		case InitialNodeEditPart.VISUAL_ID: {
			LinkedList<TUMLAbstractNavigatorItem> result = new LinkedList<TUMLAbstractNavigatorItem>();
			Node sv = (Node) view;
			TUMLNavigatorGroup incominglinks = new TUMLNavigatorGroup(
					Messages.NavigatorGroupName_InitialNode_3023_incominglinks,
					"icons/incomingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			TUMLNavigatorGroup outgoinglinks = new TUMLNavigatorGroup(
					Messages.NavigatorGroupName_InitialNode_3023_outgoinglinks,
					"icons/outgoingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getIncomingLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(ControlFlowEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(ControlFlowEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			connectedViews = getIncomingLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(ObjectFlowEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(ObjectFlowEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			if (!incominglinks.isEmpty()) {
				result.add(incominglinks);
			}
			if (!outgoinglinks.isEmpty()) {
				result.add(outgoinglinks);
			}
			return result.toArray();
		}

		case ActivityFinalNodeEditPart.VISUAL_ID: {
			LinkedList<TUMLAbstractNavigatorItem> result = new LinkedList<TUMLAbstractNavigatorItem>();
			Node sv = (Node) view;
			TUMLNavigatorGroup incominglinks = new TUMLNavigatorGroup(
					Messages.NavigatorGroupName_ActivityFinalNode_3024_incominglinks,
					"icons/incomingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			TUMLNavigatorGroup outgoinglinks = new TUMLNavigatorGroup(
					Messages.NavigatorGroupName_ActivityFinalNode_3024_outgoinglinks,
					"icons/outgoingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getIncomingLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(ControlFlowEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(ControlFlowEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			connectedViews = getIncomingLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(ObjectFlowEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(ObjectFlowEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			if (!incominglinks.isEmpty()) {
				result.add(incominglinks);
			}
			if (!outgoinglinks.isEmpty()) {
				result.add(outgoinglinks);
			}
			return result.toArray();
		}

		case ForkNodeEditPart.VISUAL_ID: {
			LinkedList<TUMLAbstractNavigatorItem> result = new LinkedList<TUMLAbstractNavigatorItem>();
			Node sv = (Node) view;
			TUMLNavigatorGroup incominglinks = new TUMLNavigatorGroup(
					Messages.NavigatorGroupName_ForkNode_3025_incominglinks,
					"icons/incomingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			TUMLNavigatorGroup outgoinglinks = new TUMLNavigatorGroup(
					Messages.NavigatorGroupName_ForkNode_3025_outgoinglinks,
					"icons/outgoingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getIncomingLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(ControlFlowEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(ControlFlowEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			connectedViews = getIncomingLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(ObjectFlowEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(ObjectFlowEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			if (!incominglinks.isEmpty()) {
				result.add(incominglinks);
			}
			if (!outgoinglinks.isEmpty()) {
				result.add(outgoinglinks);
			}
			return result.toArray();
		}

		case JoinNodeEditPart.VISUAL_ID: {
			LinkedList<TUMLAbstractNavigatorItem> result = new LinkedList<TUMLAbstractNavigatorItem>();
			Node sv = (Node) view;
			TUMLNavigatorGroup incominglinks = new TUMLNavigatorGroup(
					Messages.NavigatorGroupName_JoinNode_3026_incominglinks,
					"icons/incomingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			TUMLNavigatorGroup outgoinglinks = new TUMLNavigatorGroup(
					Messages.NavigatorGroupName_JoinNode_3026_outgoinglinks,
					"icons/outgoingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getIncomingLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(ControlFlowEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(ControlFlowEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			connectedViews = getIncomingLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(ObjectFlowEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(ObjectFlowEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			if (!incominglinks.isEmpty()) {
				result.add(incominglinks);
			}
			if (!outgoinglinks.isEmpty()) {
				result.add(outgoinglinks);
			}
			return result.toArray();
		}

		case CallActionEditPart.VISUAL_ID: {
			LinkedList<TUMLAbstractNavigatorItem> result = new LinkedList<TUMLAbstractNavigatorItem>();
			Node sv = (Node) view;
			TUMLNavigatorGroup incominglinks = new TUMLNavigatorGroup(
					Messages.NavigatorGroupName_CallAction_3027_incominglinks,
					"icons/incomingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			TUMLNavigatorGroup outgoinglinks = new TUMLNavigatorGroup(
					Messages.NavigatorGroupName_CallAction_3027_outgoinglinks,
					"icons/outgoingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getIncomingLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(ControlFlowEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(ControlFlowEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			connectedViews = getIncomingLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(ObjectFlowEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(ObjectFlowEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			connectedViews = getOutgoingLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry
							.getType(CallActionMethodEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			if (!incominglinks.isEmpty()) {
				result.add(incominglinks);
			}
			if (!outgoinglinks.isEmpty()) {
				result.add(outgoinglinks);
			}
			return result.toArray();
		}

		case SendActionEditPart.VISUAL_ID: {
			LinkedList<TUMLAbstractNavigatorItem> result = new LinkedList<TUMLAbstractNavigatorItem>();
			Node sv = (Node) view;
			TUMLNavigatorGroup incominglinks = new TUMLNavigatorGroup(
					Messages.NavigatorGroupName_SendAction_3028_incominglinks,
					"icons/incomingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			TUMLNavigatorGroup outgoinglinks = new TUMLNavigatorGroup(
					Messages.NavigatorGroupName_SendAction_3028_outgoinglinks,
					"icons/outgoingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getIncomingLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(ControlFlowEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(ControlFlowEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			connectedViews = getIncomingLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(ObjectFlowEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(ObjectFlowEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			if (!incominglinks.isEmpty()) {
				result.add(incominglinks);
			}
			if (!outgoinglinks.isEmpty()) {
				result.add(outgoinglinks);
			}
			return result.toArray();
		}

		case AcceptActionEditPart.VISUAL_ID: {
			LinkedList<TUMLAbstractNavigatorItem> result = new LinkedList<TUMLAbstractNavigatorItem>();
			Node sv = (Node) view;
			TUMLNavigatorGroup incominglinks = new TUMLNavigatorGroup(
					Messages.NavigatorGroupName_AcceptAction_3029_incominglinks,
					"icons/incomingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			TUMLNavigatorGroup outgoinglinks = new TUMLNavigatorGroup(
					Messages.NavigatorGroupName_AcceptAction_3029_outgoinglinks,
					"icons/outgoingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getIncomingLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(ControlFlowEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(ControlFlowEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			connectedViews = getIncomingLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(ObjectFlowEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(ObjectFlowEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			if (!incominglinks.isEmpty()) {
				result.add(incominglinks);
			}
			if (!outgoinglinks.isEmpty()) {
				result.add(outgoinglinks);
			}
			return result.toArray();
		}

		case VxActionEditPart.VISUAL_ID: {
			LinkedList<TUMLAbstractNavigatorItem> result = new LinkedList<TUMLAbstractNavigatorItem>();
			Node sv = (Node) view;
			TUMLNavigatorGroup incominglinks = new TUMLNavigatorGroup(
					Messages.NavigatorGroupName_VxAction_3030_incominglinks,
					"icons/incomingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			TUMLNavigatorGroup outgoinglinks = new TUMLNavigatorGroup(
					Messages.NavigatorGroupName_VxAction_3030_outgoinglinks,
					"icons/outgoingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getIncomingLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(ControlFlowEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(ControlFlowEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			connectedViews = getIncomingLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(ObjectFlowEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(ObjectFlowEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			connectedViews = getOutgoingLinksByType(
					Collections.singleton(sv),
					TUMLVisualIDRegistry
							.getType(VxActionVariantFragmentsEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			if (!incominglinks.isEmpty()) {
				result.add(incominglinks);
			}
			if (!outgoinglinks.isEmpty()) {
				result.add(outgoinglinks);
			}
			return result.toArray();
		}

		case ObjectNodeEditPart.VISUAL_ID: {
			LinkedList<TUMLAbstractNavigatorItem> result = new LinkedList<TUMLAbstractNavigatorItem>();
			Node sv = (Node) view;
			TUMLNavigatorGroup incominglinks = new TUMLNavigatorGroup(
					Messages.NavigatorGroupName_ObjectNode_3031_incominglinks,
					"icons/incomingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			TUMLNavigatorGroup outgoinglinks = new TUMLNavigatorGroup(
					Messages.NavigatorGroupName_ObjectNode_3031_outgoinglinks,
					"icons/outgoingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getIncomingLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(ControlFlowEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(ControlFlowEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			connectedViews = getIncomingLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(ObjectFlowEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(ObjectFlowEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			if (!incominglinks.isEmpty()) {
				result.add(incominglinks);
			}
			if (!outgoinglinks.isEmpty()) {
				result.add(outgoinglinks);
			}
			return result.toArray();
		}

		case AssignActionEditPart.VISUAL_ID: {
			LinkedList<TUMLAbstractNavigatorItem> result = new LinkedList<TUMLAbstractNavigatorItem>();
			Node sv = (Node) view;
			TUMLNavigatorGroup incominglinks = new TUMLNavigatorGroup(
					Messages.NavigatorGroupName_AssignAction_3032_incominglinks,
					"icons/incomingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			TUMLNavigatorGroup outgoinglinks = new TUMLNavigatorGroup(
					Messages.NavigatorGroupName_AssignAction_3032_outgoinglinks,
					"icons/outgoingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getIncomingLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(ControlFlowEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(ControlFlowEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			connectedViews = getIncomingLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(ObjectFlowEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(ObjectFlowEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			if (!incominglinks.isEmpty()) {
				result.add(incominglinks);
			}
			if (!outgoinglinks.isEmpty()) {
				result.add(outgoinglinks);
			}
			return result.toArray();
		}

		case EmptyActionEditPart.VISUAL_ID: {
			LinkedList<TUMLAbstractNavigatorItem> result = new LinkedList<TUMLAbstractNavigatorItem>();
			Node sv = (Node) view;
			TUMLNavigatorGroup incominglinks = new TUMLNavigatorGroup(
					Messages.NavigatorGroupName_EmptyAction_3033_incominglinks,
					"icons/incomingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			TUMLNavigatorGroup outgoinglinks = new TUMLNavigatorGroup(
					Messages.NavigatorGroupName_EmptyAction_3033_outgoinglinks,
					"icons/outgoingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getIncomingLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(ControlFlowEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(ControlFlowEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			connectedViews = getIncomingLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(ObjectFlowEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(ObjectFlowEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			if (!incominglinks.isEmpty()) {
				result.add(incominglinks);
			}
			if (!outgoinglinks.isEmpty()) {
				result.add(outgoinglinks);
			}
			return result.toArray();
		}

		case RealizationEditPart.VISUAL_ID: {
			LinkedList<TUMLAbstractNavigatorItem> result = new LinkedList<TUMLAbstractNavigatorItem>();
			Edge sv = (Edge) view;
			TUMLNavigatorGroup target = new TUMLNavigatorGroup(
					Messages.NavigatorGroupName_Realization_4001_target,
					"icons/linkTargetNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			TUMLNavigatorGroup source = new TUMLNavigatorGroup(
					Messages.NavigatorGroupName_Realization_4001_source,
					"icons/linkSourceNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getLinksTargetByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(ClassEditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksTargetByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(InterfaceEditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksTargetByType(Collections.singleton(sv),
					TUMLVisualIDRegistry
							.getType(VariationPointEditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksTargetByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(VariationEditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksSourceByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(ClassEditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			connectedViews = getLinksSourceByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(InterfaceEditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			connectedViews = getLinksSourceByType(Collections.singleton(sv),
					TUMLVisualIDRegistry
							.getType(VariationPointEditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			connectedViews = getLinksSourceByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(VariationEditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			if (!target.isEmpty()) {
				result.add(target);
			}
			if (!source.isEmpty()) {
				result.add(source);
			}
			return result.toArray();
		}

		case GeneralizationEditPart.VISUAL_ID: {
			LinkedList<TUMLAbstractNavigatorItem> result = new LinkedList<TUMLAbstractNavigatorItem>();
			Edge sv = (Edge) view;
			TUMLNavigatorGroup target = new TUMLNavigatorGroup(
					Messages.NavigatorGroupName_Generalization_4002_target,
					"icons/linkTargetNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			TUMLNavigatorGroup source = new TUMLNavigatorGroup(
					Messages.NavigatorGroupName_Generalization_4002_source,
					"icons/linkSourceNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getLinksTargetByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(ClassEditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksTargetByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(InterfaceEditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksTargetByType(Collections.singleton(sv),
					TUMLVisualIDRegistry
							.getType(VariationPointEditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksTargetByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(VariationEditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksSourceByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(ClassEditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			connectedViews = getLinksSourceByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(InterfaceEditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			connectedViews = getLinksSourceByType(Collections.singleton(sv),
					TUMLVisualIDRegistry
							.getType(VariationPointEditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			connectedViews = getLinksSourceByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(VariationEditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			if (!target.isEmpty()) {
				result.add(target);
			}
			if (!source.isEmpty()) {
				result.add(source);
			}
			return result.toArray();
		}

		case ReferenceEditPart.VISUAL_ID: {
			LinkedList<TUMLAbstractNavigatorItem> result = new LinkedList<TUMLAbstractNavigatorItem>();
			Edge sv = (Edge) view;
			TUMLNavigatorGroup target = new TUMLNavigatorGroup(
					Messages.NavigatorGroupName_Reference_4003_target,
					"icons/linkTargetNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			TUMLNavigatorGroup source = new TUMLNavigatorGroup(
					Messages.NavigatorGroupName_Reference_4003_source,
					"icons/linkSourceNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getLinksTargetByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(ClassEditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksTargetByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(InterfaceEditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksTargetByType(Collections.singleton(sv),
					TUMLVisualIDRegistry
							.getType(VariationPointEditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksTargetByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(VariationEditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksSourceByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(ClassEditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			connectedViews = getLinksSourceByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(InterfaceEditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			connectedViews = getLinksSourceByType(Collections.singleton(sv),
					TUMLVisualIDRegistry
							.getType(VariationPointEditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			connectedViews = getLinksSourceByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(VariationEditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			if (!target.isEmpty()) {
				result.add(target);
			}
			if (!source.isEmpty()) {
				result.add(source);
			}
			return result.toArray();
		}

		case ConstraintEditPart.VISUAL_ID: {
			LinkedList<TUMLAbstractNavigatorItem> result = new LinkedList<TUMLAbstractNavigatorItem>();
			Edge sv = (Edge) view;
			TUMLNavigatorGroup target = new TUMLNavigatorGroup(
					Messages.NavigatorGroupName_Constraint_4004_target,
					"icons/linkTargetNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			TUMLNavigatorGroup source = new TUMLNavigatorGroup(
					Messages.NavigatorGroupName_Constraint_4004_source,
					"icons/linkSourceNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getLinksTargetByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(ClassEditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksTargetByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(InterfaceEditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksTargetByType(Collections.singleton(sv),
					TUMLVisualIDRegistry
							.getType(VariationPointEditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksTargetByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(VariationEditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksSourceByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(ClassEditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			connectedViews = getLinksSourceByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(InterfaceEditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			connectedViews = getLinksSourceByType(Collections.singleton(sv),
					TUMLVisualIDRegistry
							.getType(VariationPointEditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			connectedViews = getLinksSourceByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(VariationEditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			if (!target.isEmpty()) {
				result.add(target);
			}
			if (!source.isEmpty()) {
				result.add(source);
			}
			return result.toArray();
		}

		case ControlFlowEditPart.VISUAL_ID: {
			LinkedList<TUMLAbstractNavigatorItem> result = new LinkedList<TUMLAbstractNavigatorItem>();
			Edge sv = (Edge) view;
			TUMLNavigatorGroup target = new TUMLNavigatorGroup(
					Messages.NavigatorGroupName_ControlFlow_4005_target,
					"icons/linkTargetNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			TUMLNavigatorGroup source = new TUMLNavigatorGroup(
					Messages.NavigatorGroupName_ControlFlow_4005_source,
					"icons/linkSourceNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getLinksTargetByType(Collections.singleton(sv),
					TUMLVisualIDRegistry
							.getType(DecisionNodeEditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksTargetByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(MergeNodeEditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksTargetByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(InitialNodeEditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksTargetByType(Collections.singleton(sv),
					TUMLVisualIDRegistry
							.getType(ActivityFinalNodeEditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksTargetByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(ForkNodeEditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksTargetByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(JoinNodeEditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksTargetByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(CallActionEditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksTargetByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(SendActionEditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksTargetByType(Collections.singleton(sv),
					TUMLVisualIDRegistry
							.getType(AcceptActionEditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksTargetByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(VxActionEditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksTargetByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(ObjectNodeEditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksTargetByType(Collections.singleton(sv),
					TUMLVisualIDRegistry
							.getType(AssignActionEditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksTargetByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(EmptyActionEditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksSourceByType(Collections.singleton(sv),
					TUMLVisualIDRegistry
							.getType(DecisionNodeEditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			connectedViews = getLinksSourceByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(MergeNodeEditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			connectedViews = getLinksSourceByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(InitialNodeEditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			connectedViews = getLinksSourceByType(Collections.singleton(sv),
					TUMLVisualIDRegistry
							.getType(ActivityFinalNodeEditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			connectedViews = getLinksSourceByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(ForkNodeEditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			connectedViews = getLinksSourceByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(JoinNodeEditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			connectedViews = getLinksSourceByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(CallActionEditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			connectedViews = getLinksSourceByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(SendActionEditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			connectedViews = getLinksSourceByType(Collections.singleton(sv),
					TUMLVisualIDRegistry
							.getType(AcceptActionEditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			connectedViews = getLinksSourceByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(VxActionEditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			connectedViews = getLinksSourceByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(ObjectNodeEditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			connectedViews = getLinksSourceByType(Collections.singleton(sv),
					TUMLVisualIDRegistry
							.getType(AssignActionEditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			connectedViews = getLinksSourceByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(EmptyActionEditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			if (!target.isEmpty()) {
				result.add(target);
			}
			if (!source.isEmpty()) {
				result.add(source);
			}
			return result.toArray();
		}

		case ObjectFlowEditPart.VISUAL_ID: {
			LinkedList<TUMLAbstractNavigatorItem> result = new LinkedList<TUMLAbstractNavigatorItem>();
			Edge sv = (Edge) view;
			TUMLNavigatorGroup target = new TUMLNavigatorGroup(
					Messages.NavigatorGroupName_ObjectFlow_4006_target,
					"icons/linkTargetNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			TUMLNavigatorGroup source = new TUMLNavigatorGroup(
					Messages.NavigatorGroupName_ObjectFlow_4006_source,
					"icons/linkSourceNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getLinksTargetByType(Collections.singleton(sv),
					TUMLVisualIDRegistry
							.getType(DecisionNodeEditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksTargetByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(MergeNodeEditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksTargetByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(InitialNodeEditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksTargetByType(Collections.singleton(sv),
					TUMLVisualIDRegistry
							.getType(ActivityFinalNodeEditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksTargetByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(ForkNodeEditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksTargetByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(JoinNodeEditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksTargetByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(CallActionEditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksTargetByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(SendActionEditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksTargetByType(Collections.singleton(sv),
					TUMLVisualIDRegistry
							.getType(AcceptActionEditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksTargetByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(VxActionEditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksTargetByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(ObjectNodeEditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksTargetByType(Collections.singleton(sv),
					TUMLVisualIDRegistry
							.getType(AssignActionEditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksTargetByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(EmptyActionEditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksSourceByType(Collections.singleton(sv),
					TUMLVisualIDRegistry
							.getType(DecisionNodeEditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			connectedViews = getLinksSourceByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(MergeNodeEditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			connectedViews = getLinksSourceByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(InitialNodeEditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			connectedViews = getLinksSourceByType(Collections.singleton(sv),
					TUMLVisualIDRegistry
							.getType(ActivityFinalNodeEditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			connectedViews = getLinksSourceByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(ForkNodeEditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			connectedViews = getLinksSourceByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(JoinNodeEditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			connectedViews = getLinksSourceByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(CallActionEditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			connectedViews = getLinksSourceByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(SendActionEditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			connectedViews = getLinksSourceByType(Collections.singleton(sv),
					TUMLVisualIDRegistry
							.getType(AcceptActionEditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			connectedViews = getLinksSourceByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(VxActionEditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			connectedViews = getLinksSourceByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(ObjectNodeEditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			connectedViews = getLinksSourceByType(Collections.singleton(sv),
					TUMLVisualIDRegistry
							.getType(AssignActionEditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			connectedViews = getLinksSourceByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(EmptyActionEditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			if (!target.isEmpty()) {
				result.add(target);
			}
			if (!source.isEmpty()) {
				result.add(source);
			}
			return result.toArray();
		}

		case ActivityRelatedOperationEditPart.VISUAL_ID: {
			LinkedList<TUMLAbstractNavigatorItem> result = new LinkedList<TUMLAbstractNavigatorItem>();
			Edge sv = (Edge) view;
			TUMLNavigatorGroup target = new TUMLNavigatorGroup(
					Messages.NavigatorGroupName_ActivityRelatedOperation_4007_target,
					"icons/linkTargetNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			TUMLNavigatorGroup source = new TUMLNavigatorGroup(
					Messages.NavigatorGroupName_ActivityRelatedOperation_4007_source,
					"icons/linkSourceNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getLinksTargetByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(OperationEditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksTargetByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(Operation2EditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksSourceByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(ActivityEditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			if (!target.isEmpty()) {
				result.add(target);
			}
			if (!source.isEmpty()) {
				result.add(source);
			}
			return result.toArray();
		}

		case CallActionMethodEditPart.VISUAL_ID: {
			LinkedList<TUMLAbstractNavigatorItem> result = new LinkedList<TUMLAbstractNavigatorItem>();
			Edge sv = (Edge) view;
			TUMLNavigatorGroup target = new TUMLNavigatorGroup(
					Messages.NavigatorGroupName_CallActionMethod_4008_target,
					"icons/linkTargetNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			TUMLNavigatorGroup source = new TUMLNavigatorGroup(
					Messages.NavigatorGroupName_CallActionMethod_4008_source,
					"icons/linkSourceNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getLinksTargetByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(OperationEditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksTargetByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(Operation2EditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksSourceByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(CallActionEditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			if (!target.isEmpty()) {
				result.add(target);
			}
			if (!source.isEmpty()) {
				result.add(source);
			}
			return result.toArray();
		}

		case VxActionVariantFragmentsEditPart.VISUAL_ID: {
			LinkedList<TUMLAbstractNavigatorItem> result = new LinkedList<TUMLAbstractNavigatorItem>();
			Edge sv = (Edge) view;
			TUMLNavigatorGroup target = new TUMLNavigatorGroup(
					Messages.NavigatorGroupName_VxActionVariantFragments_4009_target,
					"icons/linkTargetNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			TUMLNavigatorGroup source = new TUMLNavigatorGroup(
					Messages.NavigatorGroupName_VxActionVariantFragments_4009_source,
					"icons/linkSourceNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getLinksTargetByType(
					Collections.singleton(sv),
					TUMLVisualIDRegistry
							.getType(VariationActivityFragmentEditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksSourceByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(VxActionEditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			if (!target.isEmpty()) {
				result.add(target);
			}
			if (!source.isEmpty()) {
				result.add(source);
			}
			return result.toArray();
		}

		case VariationPointVariationsEditPart.VISUAL_ID: {
			LinkedList<TUMLAbstractNavigatorItem> result = new LinkedList<TUMLAbstractNavigatorItem>();
			Edge sv = (Edge) view;
			TUMLNavigatorGroup target = new TUMLNavigatorGroup(
					Messages.NavigatorGroupName_VariationPointVariations_4010_target,
					"icons/linkTargetNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			TUMLNavigatorGroup source = new TUMLNavigatorGroup(
					Messages.NavigatorGroupName_VariationPointVariations_4010_source,
					"icons/linkSourceNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection<View> connectedViews;
			connectedViews = getLinksTargetByType(Collections.singleton(sv),
					TUMLVisualIDRegistry.getType(VariationEditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksSourceByType(Collections.singleton(sv),
					TUMLVisualIDRegistry
							.getType(VariationPointEditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			if (!target.isEmpty()) {
				result.add(target);
			}
			if (!source.isEmpty()) {
				result.add(source);
			}
			return result.toArray();
		}
		}
		return EMPTY_ARRAY;
	}

	/**
	 * @generated
	 */
	private Collection<View> getLinksSourceByType(Collection<Edge> edges,
			String type) {
		LinkedList<View> result = new LinkedList<View>();
		for (Edge nextEdge : edges) {
			View nextEdgeSource = nextEdge.getSource();
			if (type.equals(nextEdgeSource.getType())
					&& isOwnView(nextEdgeSource)) {
				result.add(nextEdgeSource);
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	private Collection<View> getLinksTargetByType(Collection<Edge> edges,
			String type) {
		LinkedList<View> result = new LinkedList<View>();
		for (Edge nextEdge : edges) {
			View nextEdgeTarget = nextEdge.getTarget();
			if (type.equals(nextEdgeTarget.getType())
					&& isOwnView(nextEdgeTarget)) {
				result.add(nextEdgeTarget);
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	private Collection<View> getOutgoingLinksByType(
			Collection<? extends View> nodes, String type) {
		LinkedList<View> result = new LinkedList<View>();
		for (View nextNode : nodes) {
			result.addAll(selectViewsByType(nextNode.getSourceEdges(), type));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private Collection<View> getIncomingLinksByType(
			Collection<? extends View> nodes, String type) {
		LinkedList<View> result = new LinkedList<View>();
		for (View nextNode : nodes) {
			result.addAll(selectViewsByType(nextNode.getTargetEdges(), type));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private Collection<View> getChildrenByType(
			Collection<? extends View> nodes, String type) {
		LinkedList<View> result = new LinkedList<View>();
		for (View nextNode : nodes) {
			result.addAll(selectViewsByType(nextNode.getChildren(), type));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private Collection<View> getDiagramLinksByType(
			Collection<Diagram> diagrams, String type) {
		ArrayList<View> result = new ArrayList<View>();
		for (Diagram nextDiagram : diagrams) {
			result.addAll(selectViewsByType(nextDiagram.getEdges(), type));
		}
		return result;
	}

	// TODO refactor as static method
	/**
	 * @generated
	 */
	private Collection<View> selectViewsByType(Collection<View> views,
			String type) {
		ArrayList<View> result = new ArrayList<View>();
		for (View nextView : views) {
			if (type.equals(nextView.getType()) && isOwnView(nextView)) {
				result.add(nextView);
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	private boolean isOwnView(View view) {
		return ModelEditPart.MODEL_ID.equals(TUMLVisualIDRegistry
				.getModelID(view));
	}

	/**
	 * @generated
	 */
	private Collection<TUMLNavigatorItem> createNavigatorItems(
			Collection<View> views, Object parent, boolean isLeafs) {
		ArrayList<TUMLNavigatorItem> result = new ArrayList<TUMLNavigatorItem>(
				views.size());
		for (View nextView : views) {
			result.add(new TUMLNavigatorItem(nextView, parent, isLeafs));
		}
		return result;
	}

	/**
	 * @generated
	 */
	public Object getParent(Object element) {
		if (element instanceof TUMLAbstractNavigatorItem) {
			TUMLAbstractNavigatorItem abstractNavigatorItem = (TUMLAbstractNavigatorItem) element;
			return abstractNavigatorItem.getParent();
		}
		return null;
	}

	/**
	 * @generated
	 */
	public boolean hasChildren(Object element) {
		return element instanceof IFile || getChildren(element).length > 0;
	}

}
