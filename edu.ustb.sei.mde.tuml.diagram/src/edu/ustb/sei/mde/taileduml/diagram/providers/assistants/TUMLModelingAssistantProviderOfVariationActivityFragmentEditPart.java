package edu.ustb.sei.mde.taileduml.diagram.providers.assistants;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;

import edu.ustb.sei.mde.taileduml.diagram.edit.parts.VariationActivityFragmentEditPart;
import edu.ustb.sei.mde.taileduml.diagram.providers.TUMLElementTypes;
import edu.ustb.sei.mde.taileduml.diagram.providers.TUMLModelingAssistantProvider;

/**
 * @generated
 */
public class TUMLModelingAssistantProviderOfVariationActivityFragmentEditPart
		extends TUMLModelingAssistantProvider {

	/**
	 * @generated
	 */
	@Override
	public List<IElementType> getRelTypesOnTarget(IAdaptable target) {
		IGraphicalEditPart targetEditPart = (IGraphicalEditPart) target
				.getAdapter(IGraphicalEditPart.class);
		return doGetRelTypesOnTarget((VariationActivityFragmentEditPart) targetEditPart);
	}

	/**
	 * @generated
	 */
	public List<IElementType> doGetRelTypesOnTarget(
			VariationActivityFragmentEditPart target) {
		List<IElementType> types = new ArrayList<IElementType>(1);
		types.add(TUMLElementTypes.VxActionVariantFragments_4009);
		return types;
	}

	/**
	 * @generated
	 */
	@Override
	public List<IElementType> getTypesForSource(IAdaptable target,
			IElementType relationshipType) {
		IGraphicalEditPart targetEditPart = (IGraphicalEditPart) target
				.getAdapter(IGraphicalEditPart.class);
		return doGetTypesForSource(
				(VariationActivityFragmentEditPart) targetEditPart,
				relationshipType);
	}

	/**
	 * @generated
	 */
	public List<IElementType> doGetTypesForSource(
			VariationActivityFragmentEditPart target,
			IElementType relationshipType) {
		List<IElementType> types = new ArrayList<IElementType>();
		if (relationshipType == TUMLElementTypes.VxActionVariantFragments_4009) {
			types.add(TUMLElementTypes.VxAction_3030);
		}
		return types;
	}

}
