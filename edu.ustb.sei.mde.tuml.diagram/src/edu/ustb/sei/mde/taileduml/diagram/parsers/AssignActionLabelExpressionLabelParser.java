package edu.ustb.sei.mde.taileduml.diagram.parsers;

import java.util.Collections;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.MultiStatus;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.emf.workspace.util.WorkspaceSynchronizer;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.common.core.command.UnexecutableCommand;
import org.eclipse.gmf.runtime.common.ui.services.parser.IParserEditStatus;
import org.eclipse.gmf.runtime.common.ui.services.parser.ParserEditStatus;
import org.eclipse.gmf.runtime.emf.commands.core.command.AbstractTransactionalCommand;
import org.eclipse.gmf.tooling.runtime.parsers.ExpressionLabelParserBase;
import org.eclipse.jface.text.contentassist.IContentAssistProcessor;

import edu.ustb.sei.mde.taileduml.AssignAction;
import edu.ustb.sei.mde.taileduml.CopyTask;
import edu.ustb.sei.mde.taileduml.TailedUMLFactory;
import edu.ustb.sei.mde.taileduml.TailedUMLPackage;
import edu.ustb.sei.mde.taileduml.diagram.expressions.TUMLOCLFactory;

/**
 * @generated
 */
public class AssignActionLabelExpressionLabelParser extends
		ExpressionLabelParserBase {
	/**
	 * @generated
	 */
	public AssignActionLabelExpressionLabelParser() {
	}

	/**
	 * @generated
	 */
	@Override
	protected String getExpressionBody() {
		return TUMLOCLFactory.getExpression(0,
				TailedUMLPackage.eINSTANCE.getAssignAction(), null).body();
	}

	/**
	 * @generated
	 */
	public String getEditString(IAdaptable element, int flags) {
		EObject target = (EObject) element.getAdapter(EObject.class);
		Object result = TUMLOCLFactory.getExpression(1,
				TailedUMLPackage.eINSTANCE.getAssignAction(), null).evaluate(
				target);
		return String.valueOf(result);
	}

	/**
	 * @generated
	 */
	public IParserEditStatus isValidEditString(IAdaptable element,
			String editString) {
		return ParserEditStatus.EDITABLE_STATUS;
	}

	/**
	 * @generated
	 */
	public ICommand getParseCommand(IAdaptable element, final String newString,
			int flags) {
		final EObject target = (EObject) element.getAdapter(EObject.class);
		if (!validateValues(target, newString)) {
			return UnexecutableCommand.INSTANCE;
		}
		TransactionalEditingDomain editingDomain = TransactionUtil
				.getEditingDomain(target);
		if (editingDomain == null) {
			return UnexecutableCommand.INSTANCE;
		}
		IFile affectedFile = WorkspaceSynchronizer.getFile(target.eResource());
		return new AbstractTransactionalCommand(
				editingDomain,
				"Set Values", affectedFile == null ? null : Collections.singletonList(affectedFile)) { //$NON-NLS-1$ 
			protected CommandResult doExecuteWithResult(
					IProgressMonitor monitor, IAdaptable info)
					throws ExecutionException {
				return new CommandResult(updateValues(target, newString));
			}
		};
	}

	/**
	 * @generated
	 */
	public IContentAssistProcessor getCompletionProcessor(IAdaptable element) {
		return null;
	}

	/**
	 * @generated NOT
	 */
	private boolean validateValues(EObject target, String newString) {
		String[] buf = newString.split(";");
		AssignAction act = (AssignAction) target;
		//act.getCopies().clear();

		for (String s : buf) {
			s = s.trim();
			if (s.length() == 0)
				continue;
			int indexOf = s.indexOf('=');
			if (indexOf == -1)
				return false;
			String f = s.substring(0, indexOf);
			f = f.trim();
			String t = s.substring(indexOf + 1);
			t = t.trim();

			if (f.startsWith("$")) {
				int indexOfInFrom = f.indexOf('.');
				String name = f.substring(1, indexOfInFrom);
			}

			if (t.startsWith("$")) {
				int indexOfInTo = t.indexOf('.');
				String name = t.substring(1, indexOfInTo);
			}
		}
		return true;
	}

	/**
	 * @generated NOT
	 */
	private IStatus updateValues(EObject target, String newString)
			throws ExecutionException {
		String[] buf = newString.split(";");
		AssignAction act = (AssignAction) target;
		act.getCopies().clear();

		for (String s : buf) {
			s = s.trim();
			if (s.length() == 0)
				continue;
			int indexOf = s.indexOf('=');
			if (indexOf == -1)
				continue;
			String f = s.substring(0, indexOf);
			f = f.trim();
			String t = s.substring(indexOf + 1);
			t = t.trim();

			CopyTask task = TailedUMLFactory.eINSTANCE.createCopyTask();

			if (f.startsWith("$")) {
				int indexOfInFrom = f.indexOf('.');
				task.setFromVar(f.substring(1, indexOfInFrom));
				task.setFromExpr(f.substring(indexOfInFrom + 1));
			} else {
				task.setFromExpr(f);
			}

			if (t.startsWith("$")) {
				int indexOfInTo = t.indexOf('.');
				task.setToVar(t.substring(1, indexOfInTo));
				task.setToExpr(t.substring(indexOfInTo + 1));
			} else {
				task.setToExpr(t);
			}

			act.getCopies().add(task);

		}

		return MultiStatus.OK_STATUS;
	}

}
