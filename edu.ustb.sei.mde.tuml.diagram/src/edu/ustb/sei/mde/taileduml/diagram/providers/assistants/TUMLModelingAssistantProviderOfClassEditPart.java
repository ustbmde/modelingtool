package edu.ustb.sei.mde.taileduml.diagram.providers.assistants;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;

import edu.ustb.sei.mde.taileduml.diagram.edit.parts.ClassEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.InterfaceEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.VariationEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.VariationPointEditPart;
import edu.ustb.sei.mde.taileduml.diagram.providers.TUMLElementTypes;
import edu.ustb.sei.mde.taileduml.diagram.providers.TUMLModelingAssistantProvider;

/**
 * @generated
 */
public class TUMLModelingAssistantProviderOfClassEditPart extends
		TUMLModelingAssistantProvider {

	/**
	 * @generated
	 */
	@Override
	public List<IElementType> getTypesForPopupBar(IAdaptable host) {
		List<IElementType> types = new ArrayList<IElementType>(2);
		types.add(TUMLElementTypes.Property_3002);
		types.add(TUMLElementTypes.Operation_3003);
		return types;
	}

	/**
	 * @generated
	 */
	@Override
	public List<IElementType> getRelTypesOnSource(IAdaptable source) {
		IGraphicalEditPart sourceEditPart = (IGraphicalEditPart) source
				.getAdapter(IGraphicalEditPart.class);
		return doGetRelTypesOnSource((ClassEditPart) sourceEditPart);
	}

	/**
	 * @generated
	 */
	public List<IElementType> doGetRelTypesOnSource(ClassEditPart source) {
		List<IElementType> types = new ArrayList<IElementType>(4);
		types.add(TUMLElementTypes.Realization_4001);
		types.add(TUMLElementTypes.Generalization_4002);
		types.add(TUMLElementTypes.Reference_4003);
		types.add(TUMLElementTypes.Constraint_4004);
		return types;
	}

	/**
	 * @generated
	 */
	@Override
	public List<IElementType> getRelTypesOnSourceAndTarget(IAdaptable source,
			IAdaptable target) {
		IGraphicalEditPart sourceEditPart = (IGraphicalEditPart) source
				.getAdapter(IGraphicalEditPart.class);
		IGraphicalEditPart targetEditPart = (IGraphicalEditPart) target
				.getAdapter(IGraphicalEditPart.class);
		return doGetRelTypesOnSourceAndTarget((ClassEditPart) sourceEditPart,
				targetEditPart);
	}

	/**
	 * @generated
	 */
	public List<IElementType> doGetRelTypesOnSourceAndTarget(
			ClassEditPart source, IGraphicalEditPart targetEditPart) {
		List<IElementType> types = new LinkedList<IElementType>();
		if (targetEditPart instanceof ClassEditPart) {
			types.add(TUMLElementTypes.Realization_4001);
		}
		if (targetEditPart instanceof InterfaceEditPart) {
			types.add(TUMLElementTypes.Realization_4001);
		}
		if (targetEditPart instanceof VariationPointEditPart) {
			types.add(TUMLElementTypes.Realization_4001);
		}
		if (targetEditPart instanceof VariationEditPart) {
			types.add(TUMLElementTypes.Realization_4001);
		}
		if (targetEditPart instanceof ClassEditPart) {
			types.add(TUMLElementTypes.Generalization_4002);
		}
		if (targetEditPart instanceof InterfaceEditPart) {
			types.add(TUMLElementTypes.Generalization_4002);
		}
		if (targetEditPart instanceof VariationPointEditPart) {
			types.add(TUMLElementTypes.Generalization_4002);
		}
		if (targetEditPart instanceof VariationEditPart) {
			types.add(TUMLElementTypes.Generalization_4002);
		}
		if (targetEditPart instanceof ClassEditPart) {
			types.add(TUMLElementTypes.Reference_4003);
		}
		if (targetEditPart instanceof InterfaceEditPart) {
			types.add(TUMLElementTypes.Reference_4003);
		}
		if (targetEditPart instanceof VariationPointEditPart) {
			types.add(TUMLElementTypes.Reference_4003);
		}
		if (targetEditPart instanceof VariationEditPart) {
			types.add(TUMLElementTypes.Reference_4003);
		}
		if (targetEditPart instanceof ClassEditPart) {
			types.add(TUMLElementTypes.Constraint_4004);
		}
		if (targetEditPart instanceof InterfaceEditPart) {
			types.add(TUMLElementTypes.Constraint_4004);
		}
		if (targetEditPart instanceof VariationPointEditPart) {
			types.add(TUMLElementTypes.Constraint_4004);
		}
		if (targetEditPart instanceof VariationEditPart) {
			types.add(TUMLElementTypes.Constraint_4004);
		}
		return types;
	}

	/**
	 * @generated
	 */
	@Override
	public List<IElementType> getTypesForTarget(IAdaptable source,
			IElementType relationshipType) {
		IGraphicalEditPart sourceEditPart = (IGraphicalEditPart) source
				.getAdapter(IGraphicalEditPart.class);
		return doGetTypesForTarget((ClassEditPart) sourceEditPart,
				relationshipType);
	}

	/**
	 * @generated
	 */
	public List<IElementType> doGetTypesForTarget(ClassEditPart source,
			IElementType relationshipType) {
		List<IElementType> types = new ArrayList<IElementType>();
		if (relationshipType == TUMLElementTypes.Realization_4001) {
			types.add(TUMLElementTypes.Class_3001);
			types.add(TUMLElementTypes.Interface_3006);
			types.add(TUMLElementTypes.VariationPoint_3015);
			types.add(TUMLElementTypes.Variation_3016);
		} else if (relationshipType == TUMLElementTypes.Generalization_4002) {
			types.add(TUMLElementTypes.Class_3001);
			types.add(TUMLElementTypes.Interface_3006);
			types.add(TUMLElementTypes.VariationPoint_3015);
			types.add(TUMLElementTypes.Variation_3016);
		} else if (relationshipType == TUMLElementTypes.Reference_4003) {
			types.add(TUMLElementTypes.Class_3001);
			types.add(TUMLElementTypes.Interface_3006);
			types.add(TUMLElementTypes.VariationPoint_3015);
			types.add(TUMLElementTypes.Variation_3016);
		} else if (relationshipType == TUMLElementTypes.Constraint_4004) {
			types.add(TUMLElementTypes.Class_3001);
			types.add(TUMLElementTypes.Interface_3006);
			types.add(TUMLElementTypes.VariationPoint_3015);
			types.add(TUMLElementTypes.Variation_3016);
		}
		return types;
	}

	/**
	 * @generated
	 */
	@Override
	public List<IElementType> getRelTypesOnTarget(IAdaptable target) {
		IGraphicalEditPart targetEditPart = (IGraphicalEditPart) target
				.getAdapter(IGraphicalEditPart.class);
		return doGetRelTypesOnTarget((ClassEditPart) targetEditPart);
	}

	/**
	 * @generated
	 */
	public List<IElementType> doGetRelTypesOnTarget(ClassEditPart target) {
		List<IElementType> types = new ArrayList<IElementType>(4);
		types.add(TUMLElementTypes.Realization_4001);
		types.add(TUMLElementTypes.Generalization_4002);
		types.add(TUMLElementTypes.Reference_4003);
		types.add(TUMLElementTypes.Constraint_4004);
		return types;
	}

	/**
	 * @generated
	 */
	@Override
	public List<IElementType> getTypesForSource(IAdaptable target,
			IElementType relationshipType) {
		IGraphicalEditPart targetEditPart = (IGraphicalEditPart) target
				.getAdapter(IGraphicalEditPart.class);
		return doGetTypesForSource((ClassEditPart) targetEditPart,
				relationshipType);
	}

	/**
	 * @generated
	 */
	public List<IElementType> doGetTypesForSource(ClassEditPart target,
			IElementType relationshipType) {
		List<IElementType> types = new ArrayList<IElementType>();
		if (relationshipType == TUMLElementTypes.Realization_4001) {
			types.add(TUMLElementTypes.Class_3001);
			types.add(TUMLElementTypes.Interface_3006);
			types.add(TUMLElementTypes.VariationPoint_3015);
			types.add(TUMLElementTypes.Variation_3016);
		} else if (relationshipType == TUMLElementTypes.Generalization_4002) {
			types.add(TUMLElementTypes.Class_3001);
			types.add(TUMLElementTypes.Interface_3006);
			types.add(TUMLElementTypes.VariationPoint_3015);
			types.add(TUMLElementTypes.Variation_3016);
		} else if (relationshipType == TUMLElementTypes.Reference_4003) {
			types.add(TUMLElementTypes.Class_3001);
			types.add(TUMLElementTypes.Interface_3006);
			types.add(TUMLElementTypes.VariationPoint_3015);
			types.add(TUMLElementTypes.Variation_3016);
		} else if (relationshipType == TUMLElementTypes.Constraint_4004) {
			types.add(TUMLElementTypes.Class_3001);
			types.add(TUMLElementTypes.Interface_3006);
			types.add(TUMLElementTypes.VariationPoint_3015);
			types.add(TUMLElementTypes.Variation_3016);
		}
		return types;
	}

}
