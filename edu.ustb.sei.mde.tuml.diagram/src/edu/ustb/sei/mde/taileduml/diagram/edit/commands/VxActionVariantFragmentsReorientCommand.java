package edu.ustb.sei.mde.taileduml.diagram.edit.commands;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.emf.type.core.commands.EditElementCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.ReorientReferenceRelationshipRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.ReorientRelationshipRequest;

import edu.ustb.sei.mde.taileduml.diagram.edit.policies.TUMLBaseItemSemanticEditPolicy;
import edu.ustb.sei.mde.taileduml.vxactivity.VariationActivityFragment;
import edu.ustb.sei.mde.taileduml.vxactivity.VxAction;

/**
 * @generated
 */
public class VxActionVariantFragmentsReorientCommand extends EditElementCommand {

	/**
	 * @generated
	 */
	private final int reorientDirection;

	/**
	 * @generated
	 */
	private final EObject referenceOwner;

	/**
	 * @generated
	 */
	private final EObject oldEnd;

	/**
	 * @generated
	 */
	private final EObject newEnd;

	/**
	 * @generated
	 */
	public VxActionVariantFragmentsReorientCommand(
			ReorientReferenceRelationshipRequest request) {
		super(request.getLabel(), null, request);
		reorientDirection = request.getDirection();
		referenceOwner = request.getReferenceOwner();
		oldEnd = request.getOldRelationshipEnd();
		newEnd = request.getNewRelationshipEnd();
	}

	/**
	 * @generated
	 */
	public boolean canExecute() {
		if (false == referenceOwner instanceof VxAction) {
			return false;
		}
		if (reorientDirection == ReorientRelationshipRequest.REORIENT_SOURCE) {
			return canReorientSource();
		}
		if (reorientDirection == ReorientRelationshipRequest.REORIENT_TARGET) {
			return canReorientTarget();
		}
		return false;
	}

	/**
	 * @generated
	 */
	protected boolean canReorientSource() {
		if (!(oldEnd instanceof VariationActivityFragment && newEnd instanceof VxAction)) {
			return false;
		}
		return TUMLBaseItemSemanticEditPolicy.getLinkConstraints()
				.canExistVxActionVariantFragments_4009(getNewSource(),
						getOldTarget());
	}

	/**
	 * @generated
	 */
	protected boolean canReorientTarget() {
		if (!(oldEnd instanceof VariationActivityFragment && newEnd instanceof VariationActivityFragment)) {
			return false;
		}
		return TUMLBaseItemSemanticEditPolicy.getLinkConstraints()
				.canExistVxActionVariantFragments_4009(getOldSource(),
						getNewTarget());
	}

	/**
	 * @generated
	 */
	protected CommandResult doExecuteWithResult(IProgressMonitor monitor,
			IAdaptable info) throws ExecutionException {
		if (!canExecute()) {
			throw new ExecutionException(
					"Invalid arguments in reorient link command"); //$NON-NLS-1$
		}
		if (reorientDirection == ReorientRelationshipRequest.REORIENT_SOURCE) {
			return reorientSource();
		}
		if (reorientDirection == ReorientRelationshipRequest.REORIENT_TARGET) {
			return reorientTarget();
		}
		throw new IllegalStateException();
	}

	/**
	 * @generated
	 */
	protected CommandResult reorientSource() throws ExecutionException {
		getOldSource().getVariantFragments().remove(getOldTarget());
		getNewSource().getVariantFragments().add(getOldTarget());
		return CommandResult.newOKCommandResult(referenceOwner);
	}

	/**
	 * @generated
	 */
	protected CommandResult reorientTarget() throws ExecutionException {
		getOldSource().getVariantFragments().remove(getOldTarget());
		getOldSource().getVariantFragments().add(getNewTarget());
		return CommandResult.newOKCommandResult(referenceOwner);
	}

	/**
	 * @generated
	 */
	protected VxAction getOldSource() {
		return (VxAction) referenceOwner;
	}

	/**
	 * @generated
	 */
	protected VxAction getNewSource() {
		return (VxAction) newEnd;
	}

	/**
	 * @generated
	 */
	protected VariationActivityFragment getOldTarget() {
		return (VariationActivityFragment) oldEnd;
	}

	/**
	 * @generated
	 */
	protected VariationActivityFragment getNewTarget() {
		return (VariationActivityFragment) newEnd;
	}
}
