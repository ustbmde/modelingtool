package edu.ustb.sei.mde.taileduml.diagram.preferences;

import org.eclipse.gmf.runtime.diagram.ui.preferences.ConnectionsPreferencePage;

import edu.ustb.sei.mde.taileduml.diagram.part.TUMLDiagramEditorPlugin;

/**
 * @generated
 */
public class DiagramConnectionsPreferencePage extends ConnectionsPreferencePage {

	/**
	 * @generated
	 */
	public DiagramConnectionsPreferencePage() {
		setPreferenceStore(TUMLDiagramEditorPlugin.getInstance()
				.getPreferenceStore());
	}
}
