package edu.ustb.sei.mde.taileduml.diagram.providers;

import edu.ustb.sei.mde.taileduml.diagram.part.TUMLDiagramEditorPlugin;

/**
 * @generated
 */
public class ElementInitializers {

	protected ElementInitializers() {
		// use #getInstance to access cached instance
	}

	/**
	 * @generated
	 */
	public static ElementInitializers getInstance() {
		ElementInitializers cached = TUMLDiagramEditorPlugin.getInstance()
				.getElementInitializers();
		if (cached == null) {
			TUMLDiagramEditorPlugin.getInstance().setElementInitializers(
					cached = new ElementInitializers());
		}
		return cached;
	}
}
