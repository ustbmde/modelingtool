package edu.ustb.sei.mde.taileduml.diagram.preferences;

import org.eclipse.gmf.runtime.diagram.ui.preferences.DiagramsPreferencePage;

import edu.ustb.sei.mde.taileduml.diagram.part.TUMLDiagramEditorPlugin;

/**
 * @generated
 */
public class DiagramGeneralPreferencePage extends DiagramsPreferencePage {

	/**
	 * @generated
	 */
	public DiagramGeneralPreferencePage() {
		setPreferenceStore(TUMLDiagramEditorPlugin.getInstance()
				.getPreferenceStore());
	}
}
