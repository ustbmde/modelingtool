package edu.ustb.sei.mde.taileduml.diagram.providers.assistants;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;

import edu.ustb.sei.mde.taileduml.diagram.edit.parts.AcceptActionEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.ActivityFinalNodeEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.AssignActionEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.CallActionEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.DecisionNodeEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.EmptyActionEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.ForkNodeEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.InitialNodeEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.JoinNodeEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.MergeNodeEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.ObjectNodeEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.SendActionEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.VxActionEditPart;
import edu.ustb.sei.mde.taileduml.diagram.providers.TUMLElementTypes;
import edu.ustb.sei.mde.taileduml.diagram.providers.TUMLModelingAssistantProvider;

/**
 * @generated
 */
public class TUMLModelingAssistantProviderOfInitialNodeEditPart extends
		TUMLModelingAssistantProvider {

	/**
	 * @generated
	 */
	@Override
	public List<IElementType> getRelTypesOnSource(IAdaptable source) {
		IGraphicalEditPart sourceEditPart = (IGraphicalEditPart) source
				.getAdapter(IGraphicalEditPart.class);
		return doGetRelTypesOnSource((InitialNodeEditPart) sourceEditPart);
	}

	/**
	 * @generated
	 */
	public List<IElementType> doGetRelTypesOnSource(InitialNodeEditPart source) {
		List<IElementType> types = new ArrayList<IElementType>(2);
		types.add(TUMLElementTypes.ControlFlow_4005);
		types.add(TUMLElementTypes.ObjectFlow_4006);
		return types;
	}

	/**
	 * @generated
	 */
	@Override
	public List<IElementType> getRelTypesOnSourceAndTarget(IAdaptable source,
			IAdaptable target) {
		IGraphicalEditPart sourceEditPart = (IGraphicalEditPart) source
				.getAdapter(IGraphicalEditPart.class);
		IGraphicalEditPart targetEditPart = (IGraphicalEditPart) target
				.getAdapter(IGraphicalEditPart.class);
		return doGetRelTypesOnSourceAndTarget(
				(InitialNodeEditPart) sourceEditPart, targetEditPart);
	}

	/**
	 * @generated
	 */
	public List<IElementType> doGetRelTypesOnSourceAndTarget(
			InitialNodeEditPart source, IGraphicalEditPart targetEditPart) {
		List<IElementType> types = new LinkedList<IElementType>();
		if (targetEditPart instanceof DecisionNodeEditPart) {
			types.add(TUMLElementTypes.ControlFlow_4005);
		}
		if (targetEditPart instanceof MergeNodeEditPart) {
			types.add(TUMLElementTypes.ControlFlow_4005);
		}
		if (targetEditPart instanceof InitialNodeEditPart) {
			types.add(TUMLElementTypes.ControlFlow_4005);
		}
		if (targetEditPart instanceof ActivityFinalNodeEditPart) {
			types.add(TUMLElementTypes.ControlFlow_4005);
		}
		if (targetEditPart instanceof ForkNodeEditPart) {
			types.add(TUMLElementTypes.ControlFlow_4005);
		}
		if (targetEditPart instanceof JoinNodeEditPart) {
			types.add(TUMLElementTypes.ControlFlow_4005);
		}
		if (targetEditPart instanceof CallActionEditPart) {
			types.add(TUMLElementTypes.ControlFlow_4005);
		}
		if (targetEditPart instanceof SendActionEditPart) {
			types.add(TUMLElementTypes.ControlFlow_4005);
		}
		if (targetEditPart instanceof AcceptActionEditPart) {
			types.add(TUMLElementTypes.ControlFlow_4005);
		}
		if (targetEditPart instanceof VxActionEditPart) {
			types.add(TUMLElementTypes.ControlFlow_4005);
		}
		if (targetEditPart instanceof ObjectNodeEditPart) {
			types.add(TUMLElementTypes.ControlFlow_4005);
		}
		if (targetEditPart instanceof AssignActionEditPart) {
			types.add(TUMLElementTypes.ControlFlow_4005);
		}
		if (targetEditPart instanceof EmptyActionEditPart) {
			types.add(TUMLElementTypes.ControlFlow_4005);
		}
		if (targetEditPart instanceof DecisionNodeEditPart) {
			types.add(TUMLElementTypes.ObjectFlow_4006);
		}
		if (targetEditPart instanceof MergeNodeEditPart) {
			types.add(TUMLElementTypes.ObjectFlow_4006);
		}
		if (targetEditPart instanceof InitialNodeEditPart) {
			types.add(TUMLElementTypes.ObjectFlow_4006);
		}
		if (targetEditPart instanceof ActivityFinalNodeEditPart) {
			types.add(TUMLElementTypes.ObjectFlow_4006);
		}
		if (targetEditPart instanceof ForkNodeEditPart) {
			types.add(TUMLElementTypes.ObjectFlow_4006);
		}
		if (targetEditPart instanceof JoinNodeEditPart) {
			types.add(TUMLElementTypes.ObjectFlow_4006);
		}
		if (targetEditPart instanceof CallActionEditPart) {
			types.add(TUMLElementTypes.ObjectFlow_4006);
		}
		if (targetEditPart instanceof SendActionEditPart) {
			types.add(TUMLElementTypes.ObjectFlow_4006);
		}
		if (targetEditPart instanceof AcceptActionEditPart) {
			types.add(TUMLElementTypes.ObjectFlow_4006);
		}
		if (targetEditPart instanceof VxActionEditPart) {
			types.add(TUMLElementTypes.ObjectFlow_4006);
		}
		if (targetEditPart instanceof ObjectNodeEditPart) {
			types.add(TUMLElementTypes.ObjectFlow_4006);
		}
		if (targetEditPart instanceof AssignActionEditPart) {
			types.add(TUMLElementTypes.ObjectFlow_4006);
		}
		if (targetEditPart instanceof EmptyActionEditPart) {
			types.add(TUMLElementTypes.ObjectFlow_4006);
		}
		return types;
	}

	/**
	 * @generated
	 */
	@Override
	public List<IElementType> getTypesForTarget(IAdaptable source,
			IElementType relationshipType) {
		IGraphicalEditPart sourceEditPart = (IGraphicalEditPart) source
				.getAdapter(IGraphicalEditPart.class);
		return doGetTypesForTarget((InitialNodeEditPart) sourceEditPart,
				relationshipType);
	}

	/**
	 * @generated
	 */
	public List<IElementType> doGetTypesForTarget(InitialNodeEditPart source,
			IElementType relationshipType) {
		List<IElementType> types = new ArrayList<IElementType>();
		if (relationshipType == TUMLElementTypes.ControlFlow_4005) {
			types.add(TUMLElementTypes.DecisionNode_3021);
			types.add(TUMLElementTypes.MergeNode_3022);
			types.add(TUMLElementTypes.InitialNode_3023);
			types.add(TUMLElementTypes.ActivityFinalNode_3024);
			types.add(TUMLElementTypes.ForkNode_3025);
			types.add(TUMLElementTypes.JoinNode_3026);
			types.add(TUMLElementTypes.CallAction_3027);
			types.add(TUMLElementTypes.SendAction_3028);
			types.add(TUMLElementTypes.AcceptAction_3029);
			types.add(TUMLElementTypes.VxAction_3030);
			types.add(TUMLElementTypes.ObjectNode_3031);
			types.add(TUMLElementTypes.AssignAction_3032);
			types.add(TUMLElementTypes.EmptyAction_3033);
		} else if (relationshipType == TUMLElementTypes.ObjectFlow_4006) {
			types.add(TUMLElementTypes.DecisionNode_3021);
			types.add(TUMLElementTypes.MergeNode_3022);
			types.add(TUMLElementTypes.InitialNode_3023);
			types.add(TUMLElementTypes.ActivityFinalNode_3024);
			types.add(TUMLElementTypes.ForkNode_3025);
			types.add(TUMLElementTypes.JoinNode_3026);
			types.add(TUMLElementTypes.CallAction_3027);
			types.add(TUMLElementTypes.SendAction_3028);
			types.add(TUMLElementTypes.AcceptAction_3029);
			types.add(TUMLElementTypes.VxAction_3030);
			types.add(TUMLElementTypes.ObjectNode_3031);
			types.add(TUMLElementTypes.AssignAction_3032);
			types.add(TUMLElementTypes.EmptyAction_3033);
		}
		return types;
	}

	/**
	 * @generated
	 */
	@Override
	public List<IElementType> getRelTypesOnTarget(IAdaptable target) {
		IGraphicalEditPart targetEditPart = (IGraphicalEditPart) target
				.getAdapter(IGraphicalEditPart.class);
		return doGetRelTypesOnTarget((InitialNodeEditPart) targetEditPart);
	}

	/**
	 * @generated
	 */
	public List<IElementType> doGetRelTypesOnTarget(InitialNodeEditPart target) {
		List<IElementType> types = new ArrayList<IElementType>(2);
		types.add(TUMLElementTypes.ControlFlow_4005);
		types.add(TUMLElementTypes.ObjectFlow_4006);
		return types;
	}

	/**
	 * @generated
	 */
	@Override
	public List<IElementType> getTypesForSource(IAdaptable target,
			IElementType relationshipType) {
		IGraphicalEditPart targetEditPart = (IGraphicalEditPart) target
				.getAdapter(IGraphicalEditPart.class);
		return doGetTypesForSource((InitialNodeEditPart) targetEditPart,
				relationshipType);
	}

	/**
	 * @generated
	 */
	public List<IElementType> doGetTypesForSource(InitialNodeEditPart target,
			IElementType relationshipType) {
		List<IElementType> types = new ArrayList<IElementType>();
		if (relationshipType == TUMLElementTypes.ControlFlow_4005) {
			types.add(TUMLElementTypes.DecisionNode_3021);
			types.add(TUMLElementTypes.MergeNode_3022);
			types.add(TUMLElementTypes.InitialNode_3023);
			types.add(TUMLElementTypes.ActivityFinalNode_3024);
			types.add(TUMLElementTypes.ForkNode_3025);
			types.add(TUMLElementTypes.JoinNode_3026);
			types.add(TUMLElementTypes.CallAction_3027);
			types.add(TUMLElementTypes.SendAction_3028);
			types.add(TUMLElementTypes.AcceptAction_3029);
			types.add(TUMLElementTypes.VxAction_3030);
			types.add(TUMLElementTypes.ObjectNode_3031);
			types.add(TUMLElementTypes.AssignAction_3032);
			types.add(TUMLElementTypes.EmptyAction_3033);
		} else if (relationshipType == TUMLElementTypes.ObjectFlow_4006) {
			types.add(TUMLElementTypes.DecisionNode_3021);
			types.add(TUMLElementTypes.MergeNode_3022);
			types.add(TUMLElementTypes.InitialNode_3023);
			types.add(TUMLElementTypes.ActivityFinalNode_3024);
			types.add(TUMLElementTypes.ForkNode_3025);
			types.add(TUMLElementTypes.JoinNode_3026);
			types.add(TUMLElementTypes.CallAction_3027);
			types.add(TUMLElementTypes.SendAction_3028);
			types.add(TUMLElementTypes.AcceptAction_3029);
			types.add(TUMLElementTypes.VxAction_3030);
			types.add(TUMLElementTypes.ObjectNode_3031);
			types.add(TUMLElementTypes.AssignAction_3032);
			types.add(TUMLElementTypes.EmptyAction_3033);
		}
		return types;
	}

}
