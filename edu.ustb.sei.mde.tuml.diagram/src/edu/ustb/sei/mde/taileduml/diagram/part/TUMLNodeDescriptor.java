package edu.ustb.sei.mde.taileduml.diagram.part;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.tooling.runtime.update.UpdaterNodeDescriptor;

/**
 * @generated
 */
public class TUMLNodeDescriptor extends UpdaterNodeDescriptor {
	/**
	 * @generated
	 */
	public TUMLNodeDescriptor(EObject modelElement, int visualID) {
		super(modelElement, visualID);
	}

}
