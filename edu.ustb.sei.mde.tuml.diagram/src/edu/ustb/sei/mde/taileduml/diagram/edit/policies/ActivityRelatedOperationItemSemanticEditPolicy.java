package edu.ustb.sei.mde.taileduml.diagram.edit.policies;

import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.emf.type.core.commands.DestroyReferenceCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.DestroyReferenceRequest;

import edu.ustb.sei.mde.taileduml.TailedUMLPackage;
import edu.ustb.sei.mde.taileduml.diagram.providers.TUMLElementTypes;

/**
 * @generated
 */
public class ActivityRelatedOperationItemSemanticEditPolicy extends
		TUMLBaseItemSemanticEditPolicy {

	/**
	 * @generated
	 */
	public ActivityRelatedOperationItemSemanticEditPolicy() {
		super(TUMLElementTypes.ActivityRelatedOperation_4007);
	}

	/**
	 * @generated NOT
	 */
	protected Command getDestroyReferenceCommand(DestroyReferenceRequest req) {
		req.setContainingFeature(TailedUMLPackage.eINSTANCE
				.getActivity_RelatedOperation());
		return getGEFWrapper(new DestroyReferenceCommand(req));
	}

}
