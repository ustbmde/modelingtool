package edu.ustb.sei.mde.taileduml.diagram.part;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.gmf.tooling.runtime.update.DiagramUpdater;

import edu.ustb.sei.mde.taileduml.AcceptAction;
import edu.ustb.sei.mde.taileduml.Activity;
import edu.ustb.sei.mde.taileduml.ActivityFinalNode;
import edu.ustb.sei.mde.taileduml.ActivityGroup;
import edu.ustb.sei.mde.taileduml.ActivityNode;
import edu.ustb.sei.mde.taileduml.AssignAction;
import edu.ustb.sei.mde.taileduml.CallAction;
import edu.ustb.sei.mde.taileduml.Class;
import edu.ustb.sei.mde.taileduml.Classifier;
import edu.ustb.sei.mde.taileduml.ControlFlow;
import edu.ustb.sei.mde.taileduml.DecisionNode;
import edu.ustb.sei.mde.taileduml.EmptyAction;
import edu.ustb.sei.mde.taileduml.Enumeration;
import edu.ustb.sei.mde.taileduml.EnumerationLiteral;
import edu.ustb.sei.mde.taileduml.ForkNode;
import edu.ustb.sei.mde.taileduml.Generalization;
import edu.ustb.sei.mde.taileduml.InitialNode;
import edu.ustb.sei.mde.taileduml.Interface;
import edu.ustb.sei.mde.taileduml.JoinNode;
import edu.ustb.sei.mde.taileduml.MergeNode;
import edu.ustb.sei.mde.taileduml.Model;
import edu.ustb.sei.mde.taileduml.ObjectFlow;
import edu.ustb.sei.mde.taileduml.ObjectNode;
import edu.ustb.sei.mde.taileduml.Operation;
import edu.ustb.sei.mde.taileduml.Package;
import edu.ustb.sei.mde.taileduml.PrimitiveType;
import edu.ustb.sei.mde.taileduml.Property;
import edu.ustb.sei.mde.taileduml.Realization;
import edu.ustb.sei.mde.taileduml.Reference;
import edu.ustb.sei.mde.taileduml.SendAction;
import edu.ustb.sei.mde.taileduml.TailedUMLPackage;
import edu.ustb.sei.mde.taileduml.Type;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.AcceptActionEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.ActivityActivityCompartmentEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.ActivityEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.ActivityFinalNodeEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.ActivityRelatedOperationEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.AssignActionEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.CallActionEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.CallActionMethodEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.ClassClassAttributeCompartmentEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.ClassClassOperationCompartmentEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.ClassEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.ConstraintEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.ControlFlowEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.DecisionNodeEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.EmptyActionEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.EnumerationEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.EnumerationEnumerationCompartmentEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.EnumerationLiteralEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.ForkNodeEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.GeneralizationEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.InitialNodeEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.InterfaceEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.InterfaceInterfaceOperationCompartmentEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.JoinNodeEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.MergeNodeEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.ModelEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.ObjectFlowEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.ObjectNodeEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.Operation2EditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.OperationEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.PackageEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.PackagePackageCompartmentEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.PrimitiveTypeEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.PropertyEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.RealizationEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.ReferenceEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.SendActionEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.VariationActivityFragmentEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.VariationActivityFragmentVariationActivityFragmentCompartmentEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.VariationEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.VariationPointEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.VariationPointVariationsEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.VxActionEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.VxActionVariantFragmentsEditPart;
import edu.ustb.sei.mde.taileduml.diagram.providers.TUMLElementTypes;
import edu.ustb.sei.mde.taileduml.vxactivity.VariationActivityFragment;
import edu.ustb.sei.mde.taileduml.vxactivity.VxAction;
import edu.ustb.sei.mde.taileduml.vxactivity.VxActivityPackage;
import edu.ustb.sei.mde.taileduml.vxmodel.Constraint;
import edu.ustb.sei.mde.taileduml.vxmodel.Variation;
import edu.ustb.sei.mde.taileduml.vxmodel.VariationPoint;
import edu.ustb.sei.mde.taileduml.vxmodel.VxModelPackage;

/**
 * @generated
 */
public class TUMLDiagramUpdater {

	/**
	 * @generated
	 */
	public static List<TUMLNodeDescriptor> getSemanticChildren(View view) {
		switch (TUMLVisualIDRegistry.getVisualID(view)) {
		case ModelEditPart.VISUAL_ID:
			return getModel_1000SemanticChildren(view);
		case PackagePackageCompartmentEditPart.VISUAL_ID:
			return getPackagePackageCompartment_7001SemanticChildren(view);
		case ClassClassAttributeCompartmentEditPart.VISUAL_ID:
			return getClassClassAttributeCompartment_7002SemanticChildren(view);
		case ClassClassOperationCompartmentEditPart.VISUAL_ID:
			return getClassClassOperationCompartment_7003SemanticChildren(view);
		case EnumerationEnumerationCompartmentEditPart.VISUAL_ID:
			return getEnumerationEnumerationCompartment_7004SemanticChildren(view);
		case InterfaceInterfaceOperationCompartmentEditPart.VISUAL_ID:
			return getInterfaceInterfaceOperationCompartment_7005SemanticChildren(view);
		case ActivityActivityCompartmentEditPart.VISUAL_ID:
			return getActivityActivityCompartment_7006SemanticChildren(view);
		case VariationActivityFragmentVariationActivityFragmentCompartmentEditPart.VISUAL_ID:
			return getVariationActivityFragmentVariationActivityFragmentCompartment_7007SemanticChildren(view);
		}
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<TUMLNodeDescriptor> getModel_1000SemanticChildren(
			View view) {
		if (!view.isSetElement()) {
			return Collections.emptyList();
		}
		Model modelElement = (Model) view.getElement();
		LinkedList<TUMLNodeDescriptor> result = new LinkedList<TUMLNodeDescriptor>();
		for (Iterator<?> it = modelElement.getPackages().iterator(); it
				.hasNext();) {
			Package childElement = (Package) it.next();
			int visualID = TUMLVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == PackageEditPart.VISUAL_ID) {
				result.add(new TUMLNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		for (Iterator<?> it = modelElement.getActivities().iterator(); it
				.hasNext();) {
			ActivityGroup childElement = (ActivityGroup) it.next();
			int visualID = TUMLVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == ActivityEditPart.VISUAL_ID) {
				result.add(new TUMLNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == VariationActivityFragmentEditPart.VISUAL_ID) {
				result.add(new TUMLNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		for (Iterator<?> it = modelElement.getPrimitiveTypes().iterator(); it
				.hasNext();) {
			PrimitiveType childElement = (PrimitiveType) it.next();
			int visualID = TUMLVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == PrimitiveTypeEditPart.VISUAL_ID) {
				result.add(new TUMLNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List<TUMLNodeDescriptor> getPackagePackageCompartment_7001SemanticChildren(
			View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.emptyList();
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.emptyList();
		}
		Package modelElement = (Package) containerView.getElement();
		LinkedList<TUMLNodeDescriptor> result = new LinkedList<TUMLNodeDescriptor>();
		for (Iterator<?> it = modelElement.getClassifiers().iterator(); it
				.hasNext();) {
			Type childElement = (Type) it.next();
			int visualID = TUMLVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == ClassEditPart.VISUAL_ID) {
				result.add(new TUMLNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == EnumerationEditPart.VISUAL_ID) {
				result.add(new TUMLNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == InterfaceEditPart.VISUAL_ID) {
				result.add(new TUMLNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == VariationPointEditPart.VISUAL_ID) {
				result.add(new TUMLNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == VariationEditPart.VISUAL_ID) {
				result.add(new TUMLNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List<TUMLNodeDescriptor> getClassClassAttributeCompartment_7002SemanticChildren(
			View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.emptyList();
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.emptyList();
		}
		Class modelElement = (Class) containerView.getElement();
		LinkedList<TUMLNodeDescriptor> result = new LinkedList<TUMLNodeDescriptor>();
		for (Iterator<?> it = modelElement.getOwnedAttributes().iterator(); it
				.hasNext();) {
			Property childElement = (Property) it.next();
			int visualID = TUMLVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == PropertyEditPart.VISUAL_ID) {
				result.add(new TUMLNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List<TUMLNodeDescriptor> getClassClassOperationCompartment_7003SemanticChildren(
			View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.emptyList();
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.emptyList();
		}
		Class modelElement = (Class) containerView.getElement();
		LinkedList<TUMLNodeDescriptor> result = new LinkedList<TUMLNodeDescriptor>();
		for (Iterator<?> it = modelElement.getOwnedOperations().iterator(); it
				.hasNext();) {
			Operation childElement = (Operation) it.next();
			int visualID = TUMLVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == OperationEditPart.VISUAL_ID) {
				result.add(new TUMLNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List<TUMLNodeDescriptor> getEnumerationEnumerationCompartment_7004SemanticChildren(
			View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.emptyList();
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.emptyList();
		}
		Enumeration modelElement = (Enumeration) containerView.getElement();
		LinkedList<TUMLNodeDescriptor> result = new LinkedList<TUMLNodeDescriptor>();
		for (Iterator<?> it = modelElement.getLiterals().iterator(); it
				.hasNext();) {
			EnumerationLiteral childElement = (EnumerationLiteral) it.next();
			int visualID = TUMLVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == EnumerationLiteralEditPart.VISUAL_ID) {
				result.add(new TUMLNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List<TUMLNodeDescriptor> getInterfaceInterfaceOperationCompartment_7005SemanticChildren(
			View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.emptyList();
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.emptyList();
		}
		Interface modelElement = (Interface) containerView.getElement();
		LinkedList<TUMLNodeDescriptor> result = new LinkedList<TUMLNodeDescriptor>();
		for (Iterator<?> it = modelElement.getOwnedOperations().iterator(); it
				.hasNext();) {
			Operation childElement = (Operation) it.next();
			int visualID = TUMLVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == Operation2EditPart.VISUAL_ID) {
				result.add(new TUMLNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List<TUMLNodeDescriptor> getActivityActivityCompartment_7006SemanticChildren(
			View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.emptyList();
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.emptyList();
		}
		Activity modelElement = (Activity) containerView.getElement();
		LinkedList<TUMLNodeDescriptor> result = new LinkedList<TUMLNodeDescriptor>();
		for (Iterator<?> it = modelElement.getNodes().iterator(); it.hasNext();) {
			ActivityNode childElement = (ActivityNode) it.next();
			int visualID = TUMLVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == DecisionNodeEditPart.VISUAL_ID) {
				result.add(new TUMLNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == MergeNodeEditPart.VISUAL_ID) {
				result.add(new TUMLNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == InitialNodeEditPart.VISUAL_ID) {
				result.add(new TUMLNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == ActivityFinalNodeEditPart.VISUAL_ID) {
				result.add(new TUMLNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == ForkNodeEditPart.VISUAL_ID) {
				result.add(new TUMLNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == JoinNodeEditPart.VISUAL_ID) {
				result.add(new TUMLNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == CallActionEditPart.VISUAL_ID) {
				result.add(new TUMLNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == SendActionEditPart.VISUAL_ID) {
				result.add(new TUMLNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == AcceptActionEditPart.VISUAL_ID) {
				result.add(new TUMLNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == VxActionEditPart.VISUAL_ID) {
				result.add(new TUMLNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == ObjectNodeEditPart.VISUAL_ID) {
				result.add(new TUMLNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == AssignActionEditPart.VISUAL_ID) {
				result.add(new TUMLNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == EmptyActionEditPart.VISUAL_ID) {
				result.add(new TUMLNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List<TUMLNodeDescriptor> getVariationActivityFragmentVariationActivityFragmentCompartment_7007SemanticChildren(
			View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.emptyList();
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.emptyList();
		}
		VariationActivityFragment modelElement = (VariationActivityFragment) containerView
				.getElement();
		LinkedList<TUMLNodeDescriptor> result = new LinkedList<TUMLNodeDescriptor>();
		for (Iterator<?> it = modelElement.getNodes().iterator(); it.hasNext();) {
			ActivityNode childElement = (ActivityNode) it.next();
			int visualID = TUMLVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == InitialNodeEditPart.VISUAL_ID) {
				result.add(new TUMLNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == ActivityFinalNodeEditPart.VISUAL_ID) {
				result.add(new TUMLNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == DecisionNodeEditPart.VISUAL_ID) {
				result.add(new TUMLNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == MergeNodeEditPart.VISUAL_ID) {
				result.add(new TUMLNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == ForkNodeEditPart.VISUAL_ID) {
				result.add(new TUMLNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == JoinNodeEditPart.VISUAL_ID) {
				result.add(new TUMLNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == VxActionEditPart.VISUAL_ID) {
				result.add(new TUMLNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == SendActionEditPart.VISUAL_ID) {
				result.add(new TUMLNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == CallActionEditPart.VISUAL_ID) {
				result.add(new TUMLNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == AcceptActionEditPart.VISUAL_ID) {
				result.add(new TUMLNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == ObjectNodeEditPart.VISUAL_ID) {
				result.add(new TUMLNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == AssignActionEditPart.VISUAL_ID) {
				result.add(new TUMLNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == EmptyActionEditPart.VISUAL_ID) {
				result.add(new TUMLNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getContainedLinks(View view) {
		switch (TUMLVisualIDRegistry.getVisualID(view)) {
		case ModelEditPart.VISUAL_ID:
			return getModel_1000ContainedLinks(view);
		case PackageEditPart.VISUAL_ID:
			return getPackage_2001ContainedLinks(view);
		case ActivityEditPart.VISUAL_ID:
			return getActivity_2002ContainedLinks(view);
		case PrimitiveTypeEditPart.VISUAL_ID:
			return getPrimitiveType_2003ContainedLinks(view);
		case VariationActivityFragmentEditPart.VISUAL_ID:
			return getVariationActivityFragment_2004ContainedLinks(view);
		case ClassEditPart.VISUAL_ID:
			return getClass_3001ContainedLinks(view);
		case PropertyEditPart.VISUAL_ID:
			return getProperty_3002ContainedLinks(view);
		case OperationEditPart.VISUAL_ID:
			return getOperation_3003ContainedLinks(view);
		case EnumerationEditPart.VISUAL_ID:
			return getEnumeration_3004ContainedLinks(view);
		case EnumerationLiteralEditPart.VISUAL_ID:
			return getEnumerationLiteral_3005ContainedLinks(view);
		case InterfaceEditPart.VISUAL_ID:
			return getInterface_3006ContainedLinks(view);
		case Operation2EditPart.VISUAL_ID:
			return getOperation_3007ContainedLinks(view);
		case VariationPointEditPart.VISUAL_ID:
			return getVariationPoint_3015ContainedLinks(view);
		case VariationEditPart.VISUAL_ID:
			return getVariation_3016ContainedLinks(view);
		case DecisionNodeEditPart.VISUAL_ID:
			return getDecisionNode_3021ContainedLinks(view);
		case MergeNodeEditPart.VISUAL_ID:
			return getMergeNode_3022ContainedLinks(view);
		case InitialNodeEditPart.VISUAL_ID:
			return getInitialNode_3023ContainedLinks(view);
		case ActivityFinalNodeEditPart.VISUAL_ID:
			return getActivityFinalNode_3024ContainedLinks(view);
		case ForkNodeEditPart.VISUAL_ID:
			return getForkNode_3025ContainedLinks(view);
		case JoinNodeEditPart.VISUAL_ID:
			return getJoinNode_3026ContainedLinks(view);
		case CallActionEditPart.VISUAL_ID:
			return getCallAction_3027ContainedLinks(view);
		case SendActionEditPart.VISUAL_ID:
			return getSendAction_3028ContainedLinks(view);
		case AcceptActionEditPart.VISUAL_ID:
			return getAcceptAction_3029ContainedLinks(view);
		case VxActionEditPart.VISUAL_ID:
			return getVxAction_3030ContainedLinks(view);
		case ObjectNodeEditPart.VISUAL_ID:
			return getObjectNode_3031ContainedLinks(view);
		case AssignActionEditPart.VISUAL_ID:
			return getAssignAction_3032ContainedLinks(view);
		case EmptyActionEditPart.VISUAL_ID:
			return getEmptyAction_3033ContainedLinks(view);
		case RealizationEditPart.VISUAL_ID:
			return getRealization_4001ContainedLinks(view);
		case GeneralizationEditPart.VISUAL_ID:
			return getGeneralization_4002ContainedLinks(view);
		case ReferenceEditPart.VISUAL_ID:
			return getReference_4003ContainedLinks(view);
		case ConstraintEditPart.VISUAL_ID:
			return getConstraint_4004ContainedLinks(view);
		case ControlFlowEditPart.VISUAL_ID:
			return getControlFlow_4005ContainedLinks(view);
		case ObjectFlowEditPart.VISUAL_ID:
			return getObjectFlow_4006ContainedLinks(view);
		}
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getIncomingLinks(View view) {
		switch (TUMLVisualIDRegistry.getVisualID(view)) {
		case PackageEditPart.VISUAL_ID:
			return getPackage_2001IncomingLinks(view);
		case ActivityEditPart.VISUAL_ID:
			return getActivity_2002IncomingLinks(view);
		case PrimitiveTypeEditPart.VISUAL_ID:
			return getPrimitiveType_2003IncomingLinks(view);
		case VariationActivityFragmentEditPart.VISUAL_ID:
			return getVariationActivityFragment_2004IncomingLinks(view);
		case ClassEditPart.VISUAL_ID:
			return getClass_3001IncomingLinks(view);
		case PropertyEditPart.VISUAL_ID:
			return getProperty_3002IncomingLinks(view);
		case OperationEditPart.VISUAL_ID:
			return getOperation_3003IncomingLinks(view);
		case EnumerationEditPart.VISUAL_ID:
			return getEnumeration_3004IncomingLinks(view);
		case EnumerationLiteralEditPart.VISUAL_ID:
			return getEnumerationLiteral_3005IncomingLinks(view);
		case InterfaceEditPart.VISUAL_ID:
			return getInterface_3006IncomingLinks(view);
		case Operation2EditPart.VISUAL_ID:
			return getOperation_3007IncomingLinks(view);
		case VariationPointEditPart.VISUAL_ID:
			return getVariationPoint_3015IncomingLinks(view);
		case VariationEditPart.VISUAL_ID:
			return getVariation_3016IncomingLinks(view);
		case DecisionNodeEditPart.VISUAL_ID:
			return getDecisionNode_3021IncomingLinks(view);
		case MergeNodeEditPart.VISUAL_ID:
			return getMergeNode_3022IncomingLinks(view);
		case InitialNodeEditPart.VISUAL_ID:
			return getInitialNode_3023IncomingLinks(view);
		case ActivityFinalNodeEditPart.VISUAL_ID:
			return getActivityFinalNode_3024IncomingLinks(view);
		case ForkNodeEditPart.VISUAL_ID:
			return getForkNode_3025IncomingLinks(view);
		case JoinNodeEditPart.VISUAL_ID:
			return getJoinNode_3026IncomingLinks(view);
		case CallActionEditPart.VISUAL_ID:
			return getCallAction_3027IncomingLinks(view);
		case SendActionEditPart.VISUAL_ID:
			return getSendAction_3028IncomingLinks(view);
		case AcceptActionEditPart.VISUAL_ID:
			return getAcceptAction_3029IncomingLinks(view);
		case VxActionEditPart.VISUAL_ID:
			return getVxAction_3030IncomingLinks(view);
		case ObjectNodeEditPart.VISUAL_ID:
			return getObjectNode_3031IncomingLinks(view);
		case AssignActionEditPart.VISUAL_ID:
			return getAssignAction_3032IncomingLinks(view);
		case EmptyActionEditPart.VISUAL_ID:
			return getEmptyAction_3033IncomingLinks(view);
		case RealizationEditPart.VISUAL_ID:
			return getRealization_4001IncomingLinks(view);
		case GeneralizationEditPart.VISUAL_ID:
			return getGeneralization_4002IncomingLinks(view);
		case ReferenceEditPart.VISUAL_ID:
			return getReference_4003IncomingLinks(view);
		case ConstraintEditPart.VISUAL_ID:
			return getConstraint_4004IncomingLinks(view);
		case ControlFlowEditPart.VISUAL_ID:
			return getControlFlow_4005IncomingLinks(view);
		case ObjectFlowEditPart.VISUAL_ID:
			return getObjectFlow_4006IncomingLinks(view);
		}
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getOutgoingLinks(View view) {
		switch (TUMLVisualIDRegistry.getVisualID(view)) {
		case PackageEditPart.VISUAL_ID:
			return getPackage_2001OutgoingLinks(view);
		case ActivityEditPart.VISUAL_ID:
			return getActivity_2002OutgoingLinks(view);
		case PrimitiveTypeEditPart.VISUAL_ID:
			return getPrimitiveType_2003OutgoingLinks(view);
		case VariationActivityFragmentEditPart.VISUAL_ID:
			return getVariationActivityFragment_2004OutgoingLinks(view);
		case ClassEditPart.VISUAL_ID:
			return getClass_3001OutgoingLinks(view);
		case PropertyEditPart.VISUAL_ID:
			return getProperty_3002OutgoingLinks(view);
		case OperationEditPart.VISUAL_ID:
			return getOperation_3003OutgoingLinks(view);
		case EnumerationEditPart.VISUAL_ID:
			return getEnumeration_3004OutgoingLinks(view);
		case EnumerationLiteralEditPart.VISUAL_ID:
			return getEnumerationLiteral_3005OutgoingLinks(view);
		case InterfaceEditPart.VISUAL_ID:
			return getInterface_3006OutgoingLinks(view);
		case Operation2EditPart.VISUAL_ID:
			return getOperation_3007OutgoingLinks(view);
		case VariationPointEditPart.VISUAL_ID:
			return getVariationPoint_3015OutgoingLinks(view);
		case VariationEditPart.VISUAL_ID:
			return getVariation_3016OutgoingLinks(view);
		case DecisionNodeEditPart.VISUAL_ID:
			return getDecisionNode_3021OutgoingLinks(view);
		case MergeNodeEditPart.VISUAL_ID:
			return getMergeNode_3022OutgoingLinks(view);
		case InitialNodeEditPart.VISUAL_ID:
			return getInitialNode_3023OutgoingLinks(view);
		case ActivityFinalNodeEditPart.VISUAL_ID:
			return getActivityFinalNode_3024OutgoingLinks(view);
		case ForkNodeEditPart.VISUAL_ID:
			return getForkNode_3025OutgoingLinks(view);
		case JoinNodeEditPart.VISUAL_ID:
			return getJoinNode_3026OutgoingLinks(view);
		case CallActionEditPart.VISUAL_ID:
			return getCallAction_3027OutgoingLinks(view);
		case SendActionEditPart.VISUAL_ID:
			return getSendAction_3028OutgoingLinks(view);
		case AcceptActionEditPart.VISUAL_ID:
			return getAcceptAction_3029OutgoingLinks(view);
		case VxActionEditPart.VISUAL_ID:
			return getVxAction_3030OutgoingLinks(view);
		case ObjectNodeEditPart.VISUAL_ID:
			return getObjectNode_3031OutgoingLinks(view);
		case AssignActionEditPart.VISUAL_ID:
			return getAssignAction_3032OutgoingLinks(view);
		case EmptyActionEditPart.VISUAL_ID:
			return getEmptyAction_3033OutgoingLinks(view);
		case RealizationEditPart.VISUAL_ID:
			return getRealization_4001OutgoingLinks(view);
		case GeneralizationEditPart.VISUAL_ID:
			return getGeneralization_4002OutgoingLinks(view);
		case ReferenceEditPart.VISUAL_ID:
			return getReference_4003OutgoingLinks(view);
		case ConstraintEditPart.VISUAL_ID:
			return getConstraint_4004OutgoingLinks(view);
		case ControlFlowEditPart.VISUAL_ID:
			return getControlFlow_4005OutgoingLinks(view);
		case ObjectFlowEditPart.VISUAL_ID:
			return getObjectFlow_4006OutgoingLinks(view);
		}
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getModel_1000ContainedLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getPackage_2001ContainedLinks(
			View view) {
		Package modelElement = (Package) view.getElement();
		LinkedList<TUMLLinkDescriptor> result = new LinkedList<TUMLLinkDescriptor>();
		result.addAll(getContainedTypeModelFacetLinks_Realization_4001(modelElement));
		result.addAll(getContainedTypeModelFacetLinks_Generalization_4002(modelElement));
		result.addAll(getContainedTypeModelFacetLinks_Reference_4003(modelElement));
		result.addAll(getContainedTypeModelFacetLinks_Constraint_4004(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getActivity_2002ContainedLinks(
			View view) {
		Activity modelElement = (Activity) view.getElement();
		LinkedList<TUMLLinkDescriptor> result = new LinkedList<TUMLLinkDescriptor>();
		result.addAll(getContainedTypeModelFacetLinks_ControlFlow_4005(modelElement));
		result.addAll(getContainedTypeModelFacetLinks_ObjectFlow_4006(modelElement));
		result.addAll(getOutgoingFeatureModelFacetLinks_Activity_RelatedOperation_4007(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getPrimitiveType_2003ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getVariationActivityFragment_2004ContainedLinks(
			View view) {
		VariationActivityFragment modelElement = (VariationActivityFragment) view
				.getElement();
		LinkedList<TUMLLinkDescriptor> result = new LinkedList<TUMLLinkDescriptor>();
		result.addAll(getContainedTypeModelFacetLinks_ControlFlow_4005(modelElement));
		result.addAll(getContainedTypeModelFacetLinks_ObjectFlow_4006(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getClass_3001ContainedLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getProperty_3002ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getOperation_3003ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getEnumeration_3004ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getEnumerationLiteral_3005ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getInterface_3006ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getOperation_3007ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getVariationPoint_3015ContainedLinks(
			View view) {
		VariationPoint modelElement = (VariationPoint) view.getElement();
		LinkedList<TUMLLinkDescriptor> result = new LinkedList<TUMLLinkDescriptor>();
		result.addAll(getOutgoingFeatureModelFacetLinks_VariationPoint_Variations_4010(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getVariation_3016ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getDecisionNode_3021ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getMergeNode_3022ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getInitialNode_3023ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getActivityFinalNode_3024ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getForkNode_3025ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getJoinNode_3026ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getCallAction_3027ContainedLinks(
			View view) {
		CallAction modelElement = (CallAction) view.getElement();
		LinkedList<TUMLLinkDescriptor> result = new LinkedList<TUMLLinkDescriptor>();
		result.addAll(getOutgoingFeatureModelFacetLinks_CallAction_Method_4008(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getSendAction_3028ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getAcceptAction_3029ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getVxAction_3030ContainedLinks(
			View view) {
		VxAction modelElement = (VxAction) view.getElement();
		LinkedList<TUMLLinkDescriptor> result = new LinkedList<TUMLLinkDescriptor>();
		result.addAll(getOutgoingFeatureModelFacetLinks_VxAction_VariantFragments_4009(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getObjectNode_3031ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getAssignAction_3032ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getEmptyAction_3033ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getRealization_4001ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getGeneralization_4002ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getReference_4003ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getConstraint_4004ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getControlFlow_4005ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getObjectFlow_4006ContainedLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getPackage_2001IncomingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getActivity_2002IncomingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getPrimitiveType_2003IncomingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getVariationActivityFragment_2004IncomingLinks(
			View view) {
		VariationActivityFragment modelElement = (VariationActivityFragment) view
				.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<TUMLLinkDescriptor> result = new LinkedList<TUMLLinkDescriptor>();
		result.addAll(getIncomingFeatureModelFacetLinks_VxAction_VariantFragments_4009(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getClass_3001IncomingLinks(View view) {
		Class modelElement = (Class) view.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<TUMLLinkDescriptor> result = new LinkedList<TUMLLinkDescriptor>();
		result.addAll(getIncomingTypeModelFacetLinks_Realization_4001(
				modelElement, crossReferences));
		result.addAll(getIncomingTypeModelFacetLinks_Generalization_4002(
				modelElement, crossReferences));
		result.addAll(getIncomingTypeModelFacetLinks_Reference_4003(
				modelElement, crossReferences));
		result.addAll(getIncomingTypeModelFacetLinks_Constraint_4004(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getProperty_3002IncomingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getOperation_3003IncomingLinks(
			View view) {
		Operation modelElement = (Operation) view.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<TUMLLinkDescriptor> result = new LinkedList<TUMLLinkDescriptor>();
		result.addAll(getIncomingFeatureModelFacetLinks_Activity_RelatedOperation_4007(
				modelElement, crossReferences));
		result.addAll(getIncomingFeatureModelFacetLinks_CallAction_Method_4008(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getEnumeration_3004IncomingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getEnumerationLiteral_3005IncomingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getInterface_3006IncomingLinks(
			View view) {
		Interface modelElement = (Interface) view.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<TUMLLinkDescriptor> result = new LinkedList<TUMLLinkDescriptor>();
		result.addAll(getIncomingTypeModelFacetLinks_Realization_4001(
				modelElement, crossReferences));
		result.addAll(getIncomingTypeModelFacetLinks_Generalization_4002(
				modelElement, crossReferences));
		result.addAll(getIncomingTypeModelFacetLinks_Reference_4003(
				modelElement, crossReferences));
		result.addAll(getIncomingTypeModelFacetLinks_Constraint_4004(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getOperation_3007IncomingLinks(
			View view) {
		Operation modelElement = (Operation) view.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<TUMLLinkDescriptor> result = new LinkedList<TUMLLinkDescriptor>();
		result.addAll(getIncomingFeatureModelFacetLinks_Activity_RelatedOperation_4007(
				modelElement, crossReferences));
		result.addAll(getIncomingFeatureModelFacetLinks_CallAction_Method_4008(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getVariationPoint_3015IncomingLinks(
			View view) {
		VariationPoint modelElement = (VariationPoint) view.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<TUMLLinkDescriptor> result = new LinkedList<TUMLLinkDescriptor>();
		result.addAll(getIncomingTypeModelFacetLinks_Realization_4001(
				modelElement, crossReferences));
		result.addAll(getIncomingTypeModelFacetLinks_Generalization_4002(
				modelElement, crossReferences));
		result.addAll(getIncomingTypeModelFacetLinks_Reference_4003(
				modelElement, crossReferences));
		result.addAll(getIncomingTypeModelFacetLinks_Constraint_4004(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getVariation_3016IncomingLinks(
			View view) {
		Variation modelElement = (Variation) view.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<TUMLLinkDescriptor> result = new LinkedList<TUMLLinkDescriptor>();
		result.addAll(getIncomingTypeModelFacetLinks_Realization_4001(
				modelElement, crossReferences));
		result.addAll(getIncomingTypeModelFacetLinks_Generalization_4002(
				modelElement, crossReferences));
		result.addAll(getIncomingTypeModelFacetLinks_Reference_4003(
				modelElement, crossReferences));
		result.addAll(getIncomingTypeModelFacetLinks_Constraint_4004(
				modelElement, crossReferences));
		result.addAll(getIncomingFeatureModelFacetLinks_VariationPoint_Variations_4010(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getDecisionNode_3021IncomingLinks(
			View view) {
		DecisionNode modelElement = (DecisionNode) view.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<TUMLLinkDescriptor> result = new LinkedList<TUMLLinkDescriptor>();
		result.addAll(getIncomingTypeModelFacetLinks_ControlFlow_4005(
				modelElement, crossReferences));
		result.addAll(getIncomingTypeModelFacetLinks_ObjectFlow_4006(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getMergeNode_3022IncomingLinks(
			View view) {
		MergeNode modelElement = (MergeNode) view.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<TUMLLinkDescriptor> result = new LinkedList<TUMLLinkDescriptor>();
		result.addAll(getIncomingTypeModelFacetLinks_ControlFlow_4005(
				modelElement, crossReferences));
		result.addAll(getIncomingTypeModelFacetLinks_ObjectFlow_4006(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getInitialNode_3023IncomingLinks(
			View view) {
		InitialNode modelElement = (InitialNode) view.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<TUMLLinkDescriptor> result = new LinkedList<TUMLLinkDescriptor>();
		result.addAll(getIncomingTypeModelFacetLinks_ControlFlow_4005(
				modelElement, crossReferences));
		result.addAll(getIncomingTypeModelFacetLinks_ObjectFlow_4006(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getActivityFinalNode_3024IncomingLinks(
			View view) {
		ActivityFinalNode modelElement = (ActivityFinalNode) view.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<TUMLLinkDescriptor> result = new LinkedList<TUMLLinkDescriptor>();
		result.addAll(getIncomingTypeModelFacetLinks_ControlFlow_4005(
				modelElement, crossReferences));
		result.addAll(getIncomingTypeModelFacetLinks_ObjectFlow_4006(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getForkNode_3025IncomingLinks(
			View view) {
		ForkNode modelElement = (ForkNode) view.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<TUMLLinkDescriptor> result = new LinkedList<TUMLLinkDescriptor>();
		result.addAll(getIncomingTypeModelFacetLinks_ControlFlow_4005(
				modelElement, crossReferences));
		result.addAll(getIncomingTypeModelFacetLinks_ObjectFlow_4006(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getJoinNode_3026IncomingLinks(
			View view) {
		JoinNode modelElement = (JoinNode) view.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<TUMLLinkDescriptor> result = new LinkedList<TUMLLinkDescriptor>();
		result.addAll(getIncomingTypeModelFacetLinks_ControlFlow_4005(
				modelElement, crossReferences));
		result.addAll(getIncomingTypeModelFacetLinks_ObjectFlow_4006(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getCallAction_3027IncomingLinks(
			View view) {
		CallAction modelElement = (CallAction) view.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<TUMLLinkDescriptor> result = new LinkedList<TUMLLinkDescriptor>();
		result.addAll(getIncomingTypeModelFacetLinks_ControlFlow_4005(
				modelElement, crossReferences));
		result.addAll(getIncomingTypeModelFacetLinks_ObjectFlow_4006(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getSendAction_3028IncomingLinks(
			View view) {
		SendAction modelElement = (SendAction) view.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<TUMLLinkDescriptor> result = new LinkedList<TUMLLinkDescriptor>();
		result.addAll(getIncomingTypeModelFacetLinks_ControlFlow_4005(
				modelElement, crossReferences));
		result.addAll(getIncomingTypeModelFacetLinks_ObjectFlow_4006(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getAcceptAction_3029IncomingLinks(
			View view) {
		AcceptAction modelElement = (AcceptAction) view.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<TUMLLinkDescriptor> result = new LinkedList<TUMLLinkDescriptor>();
		result.addAll(getIncomingTypeModelFacetLinks_ControlFlow_4005(
				modelElement, crossReferences));
		result.addAll(getIncomingTypeModelFacetLinks_ObjectFlow_4006(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getVxAction_3030IncomingLinks(
			View view) {
		VxAction modelElement = (VxAction) view.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<TUMLLinkDescriptor> result = new LinkedList<TUMLLinkDescriptor>();
		result.addAll(getIncomingTypeModelFacetLinks_ControlFlow_4005(
				modelElement, crossReferences));
		result.addAll(getIncomingTypeModelFacetLinks_ObjectFlow_4006(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getObjectNode_3031IncomingLinks(
			View view) {
		ObjectNode modelElement = (ObjectNode) view.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<TUMLLinkDescriptor> result = new LinkedList<TUMLLinkDescriptor>();
		result.addAll(getIncomingTypeModelFacetLinks_ControlFlow_4005(
				modelElement, crossReferences));
		result.addAll(getIncomingTypeModelFacetLinks_ObjectFlow_4006(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getAssignAction_3032IncomingLinks(
			View view) {
		AssignAction modelElement = (AssignAction) view.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<TUMLLinkDescriptor> result = new LinkedList<TUMLLinkDescriptor>();
		result.addAll(getIncomingTypeModelFacetLinks_ControlFlow_4005(
				modelElement, crossReferences));
		result.addAll(getIncomingTypeModelFacetLinks_ObjectFlow_4006(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getEmptyAction_3033IncomingLinks(
			View view) {
		EmptyAction modelElement = (EmptyAction) view.getElement();
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer
				.find(view.eResource().getResourceSet().getResources());
		LinkedList<TUMLLinkDescriptor> result = new LinkedList<TUMLLinkDescriptor>();
		result.addAll(getIncomingTypeModelFacetLinks_ControlFlow_4005(
				modelElement, crossReferences));
		result.addAll(getIncomingTypeModelFacetLinks_ObjectFlow_4006(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getRealization_4001IncomingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getGeneralization_4002IncomingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getReference_4003IncomingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getConstraint_4004IncomingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getControlFlow_4005IncomingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getObjectFlow_4006IncomingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getPackage_2001OutgoingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getActivity_2002OutgoingLinks(
			View view) {
		Activity modelElement = (Activity) view.getElement();
		LinkedList<TUMLLinkDescriptor> result = new LinkedList<TUMLLinkDescriptor>();
		result.addAll(getOutgoingFeatureModelFacetLinks_Activity_RelatedOperation_4007(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getPrimitiveType_2003OutgoingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getVariationActivityFragment_2004OutgoingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getClass_3001OutgoingLinks(View view) {
		Class modelElement = (Class) view.getElement();
		LinkedList<TUMLLinkDescriptor> result = new LinkedList<TUMLLinkDescriptor>();
		result.addAll(getOutgoingTypeModelFacetLinks_Realization_4001(modelElement));
		result.addAll(getOutgoingTypeModelFacetLinks_Generalization_4002(modelElement));
		result.addAll(getOutgoingTypeModelFacetLinks_Reference_4003(modelElement));
		result.addAll(getOutgoingTypeModelFacetLinks_Constraint_4004(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getProperty_3002OutgoingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getOperation_3003OutgoingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getEnumeration_3004OutgoingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getEnumerationLiteral_3005OutgoingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getInterface_3006OutgoingLinks(
			View view) {
		Interface modelElement = (Interface) view.getElement();
		LinkedList<TUMLLinkDescriptor> result = new LinkedList<TUMLLinkDescriptor>();
		result.addAll(getOutgoingTypeModelFacetLinks_Realization_4001(modelElement));
		result.addAll(getOutgoingTypeModelFacetLinks_Generalization_4002(modelElement));
		result.addAll(getOutgoingTypeModelFacetLinks_Reference_4003(modelElement));
		result.addAll(getOutgoingTypeModelFacetLinks_Constraint_4004(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getOperation_3007OutgoingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getVariationPoint_3015OutgoingLinks(
			View view) {
		VariationPoint modelElement = (VariationPoint) view.getElement();
		LinkedList<TUMLLinkDescriptor> result = new LinkedList<TUMLLinkDescriptor>();
		result.addAll(getOutgoingTypeModelFacetLinks_Realization_4001(modelElement));
		result.addAll(getOutgoingTypeModelFacetLinks_Generalization_4002(modelElement));
		result.addAll(getOutgoingTypeModelFacetLinks_Reference_4003(modelElement));
		result.addAll(getOutgoingTypeModelFacetLinks_Constraint_4004(modelElement));
		result.addAll(getOutgoingFeatureModelFacetLinks_VariationPoint_Variations_4010(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getVariation_3016OutgoingLinks(
			View view) {
		Variation modelElement = (Variation) view.getElement();
		LinkedList<TUMLLinkDescriptor> result = new LinkedList<TUMLLinkDescriptor>();
		result.addAll(getOutgoingTypeModelFacetLinks_Realization_4001(modelElement));
		result.addAll(getOutgoingTypeModelFacetLinks_Generalization_4002(modelElement));
		result.addAll(getOutgoingTypeModelFacetLinks_Reference_4003(modelElement));
		result.addAll(getOutgoingTypeModelFacetLinks_Constraint_4004(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getDecisionNode_3021OutgoingLinks(
			View view) {
		DecisionNode modelElement = (DecisionNode) view.getElement();
		LinkedList<TUMLLinkDescriptor> result = new LinkedList<TUMLLinkDescriptor>();
		result.addAll(getOutgoingTypeModelFacetLinks_ControlFlow_4005(modelElement));
		result.addAll(getOutgoingTypeModelFacetLinks_ObjectFlow_4006(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getMergeNode_3022OutgoingLinks(
			View view) {
		MergeNode modelElement = (MergeNode) view.getElement();
		LinkedList<TUMLLinkDescriptor> result = new LinkedList<TUMLLinkDescriptor>();
		result.addAll(getOutgoingTypeModelFacetLinks_ControlFlow_4005(modelElement));
		result.addAll(getOutgoingTypeModelFacetLinks_ObjectFlow_4006(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getInitialNode_3023OutgoingLinks(
			View view) {
		InitialNode modelElement = (InitialNode) view.getElement();
		LinkedList<TUMLLinkDescriptor> result = new LinkedList<TUMLLinkDescriptor>();
		result.addAll(getOutgoingTypeModelFacetLinks_ControlFlow_4005(modelElement));
		result.addAll(getOutgoingTypeModelFacetLinks_ObjectFlow_4006(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getActivityFinalNode_3024OutgoingLinks(
			View view) {
		ActivityFinalNode modelElement = (ActivityFinalNode) view.getElement();
		LinkedList<TUMLLinkDescriptor> result = new LinkedList<TUMLLinkDescriptor>();
		result.addAll(getOutgoingTypeModelFacetLinks_ControlFlow_4005(modelElement));
		result.addAll(getOutgoingTypeModelFacetLinks_ObjectFlow_4006(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getForkNode_3025OutgoingLinks(
			View view) {
		ForkNode modelElement = (ForkNode) view.getElement();
		LinkedList<TUMLLinkDescriptor> result = new LinkedList<TUMLLinkDescriptor>();
		result.addAll(getOutgoingTypeModelFacetLinks_ControlFlow_4005(modelElement));
		result.addAll(getOutgoingTypeModelFacetLinks_ObjectFlow_4006(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getJoinNode_3026OutgoingLinks(
			View view) {
		JoinNode modelElement = (JoinNode) view.getElement();
		LinkedList<TUMLLinkDescriptor> result = new LinkedList<TUMLLinkDescriptor>();
		result.addAll(getOutgoingTypeModelFacetLinks_ControlFlow_4005(modelElement));
		result.addAll(getOutgoingTypeModelFacetLinks_ObjectFlow_4006(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getCallAction_3027OutgoingLinks(
			View view) {
		CallAction modelElement = (CallAction) view.getElement();
		LinkedList<TUMLLinkDescriptor> result = new LinkedList<TUMLLinkDescriptor>();
		result.addAll(getOutgoingTypeModelFacetLinks_ControlFlow_4005(modelElement));
		result.addAll(getOutgoingTypeModelFacetLinks_ObjectFlow_4006(modelElement));
		result.addAll(getOutgoingFeatureModelFacetLinks_CallAction_Method_4008(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getSendAction_3028OutgoingLinks(
			View view) {
		SendAction modelElement = (SendAction) view.getElement();
		LinkedList<TUMLLinkDescriptor> result = new LinkedList<TUMLLinkDescriptor>();
		result.addAll(getOutgoingTypeModelFacetLinks_ControlFlow_4005(modelElement));
		result.addAll(getOutgoingTypeModelFacetLinks_ObjectFlow_4006(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getAcceptAction_3029OutgoingLinks(
			View view) {
		AcceptAction modelElement = (AcceptAction) view.getElement();
		LinkedList<TUMLLinkDescriptor> result = new LinkedList<TUMLLinkDescriptor>();
		result.addAll(getOutgoingTypeModelFacetLinks_ControlFlow_4005(modelElement));
		result.addAll(getOutgoingTypeModelFacetLinks_ObjectFlow_4006(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getVxAction_3030OutgoingLinks(
			View view) {
		VxAction modelElement = (VxAction) view.getElement();
		LinkedList<TUMLLinkDescriptor> result = new LinkedList<TUMLLinkDescriptor>();
		result.addAll(getOutgoingTypeModelFacetLinks_ControlFlow_4005(modelElement));
		result.addAll(getOutgoingTypeModelFacetLinks_ObjectFlow_4006(modelElement));
		result.addAll(getOutgoingFeatureModelFacetLinks_VxAction_VariantFragments_4009(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getObjectNode_3031OutgoingLinks(
			View view) {
		ObjectNode modelElement = (ObjectNode) view.getElement();
		LinkedList<TUMLLinkDescriptor> result = new LinkedList<TUMLLinkDescriptor>();
		result.addAll(getOutgoingTypeModelFacetLinks_ControlFlow_4005(modelElement));
		result.addAll(getOutgoingTypeModelFacetLinks_ObjectFlow_4006(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getAssignAction_3032OutgoingLinks(
			View view) {
		AssignAction modelElement = (AssignAction) view.getElement();
		LinkedList<TUMLLinkDescriptor> result = new LinkedList<TUMLLinkDescriptor>();
		result.addAll(getOutgoingTypeModelFacetLinks_ControlFlow_4005(modelElement));
		result.addAll(getOutgoingTypeModelFacetLinks_ObjectFlow_4006(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getEmptyAction_3033OutgoingLinks(
			View view) {
		EmptyAction modelElement = (EmptyAction) view.getElement();
		LinkedList<TUMLLinkDescriptor> result = new LinkedList<TUMLLinkDescriptor>();
		result.addAll(getOutgoingTypeModelFacetLinks_ControlFlow_4005(modelElement));
		result.addAll(getOutgoingTypeModelFacetLinks_ObjectFlow_4006(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getRealization_4001OutgoingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getGeneralization_4002OutgoingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getReference_4003OutgoingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getConstraint_4004OutgoingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getControlFlow_4005OutgoingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<TUMLLinkDescriptor> getObjectFlow_4006OutgoingLinks(
			View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	private static Collection<TUMLLinkDescriptor> getContainedTypeModelFacetLinks_Realization_4001(
			Package container) {
		LinkedList<TUMLLinkDescriptor> result = new LinkedList<TUMLLinkDescriptor>();
		for (Iterator<?> links = container.getRelationships().iterator(); links
				.hasNext();) {
			EObject linkObject = (EObject) links.next();
			if (false == linkObject instanceof Realization) {
				continue;
			}
			Realization link = (Realization) linkObject;
			if (RealizationEditPart.VISUAL_ID != TUMLVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			Classifier dst = link.getTarget();
			Classifier src = link.getSource();
			result.add(new TUMLLinkDescriptor(src, dst, link,
					TUMLElementTypes.Realization_4001,
					RealizationEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<TUMLLinkDescriptor> getContainedTypeModelFacetLinks_Generalization_4002(
			Package container) {
		LinkedList<TUMLLinkDescriptor> result = new LinkedList<TUMLLinkDescriptor>();
		for (Iterator<?> links = container.getRelationships().iterator(); links
				.hasNext();) {
			EObject linkObject = (EObject) links.next();
			if (false == linkObject instanceof Generalization) {
				continue;
			}
			Generalization link = (Generalization) linkObject;
			if (GeneralizationEditPart.VISUAL_ID != TUMLVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			Classifier dst = link.getTarget();
			Classifier src = link.getSource();
			result.add(new TUMLLinkDescriptor(src, dst, link,
					TUMLElementTypes.Generalization_4002,
					GeneralizationEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<TUMLLinkDescriptor> getContainedTypeModelFacetLinks_Reference_4003(
			Package container) {
		LinkedList<TUMLLinkDescriptor> result = new LinkedList<TUMLLinkDescriptor>();
		for (Iterator<?> links = container.getRelationships().iterator(); links
				.hasNext();) {
			EObject linkObject = (EObject) links.next();
			if (false == linkObject instanceof Reference) {
				continue;
			}
			Reference link = (Reference) linkObject;
			if (ReferenceEditPart.VISUAL_ID != TUMLVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			Classifier dst = link.getTarget();
			Classifier src = link.getSource();
			result.add(new TUMLLinkDescriptor(src, dst, link,
					TUMLElementTypes.Reference_4003,
					ReferenceEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<TUMLLinkDescriptor> getContainedTypeModelFacetLinks_Constraint_4004(
			Package container) {
		LinkedList<TUMLLinkDescriptor> result = new LinkedList<TUMLLinkDescriptor>();
		for (Iterator<?> links = container.getRelationships().iterator(); links
				.hasNext();) {
			EObject linkObject = (EObject) links.next();
			if (false == linkObject instanceof Constraint) {
				continue;
			}
			Constraint link = (Constraint) linkObject;
			if (ConstraintEditPart.VISUAL_ID != TUMLVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			Classifier dst = link.getTarget();
			Classifier src = link.getSource();
			result.add(new TUMLLinkDescriptor(src, dst, link,
					TUMLElementTypes.Constraint_4004,
					ConstraintEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<TUMLLinkDescriptor> getContainedTypeModelFacetLinks_ControlFlow_4005(
			ActivityGroup container) {
		LinkedList<TUMLLinkDescriptor> result = new LinkedList<TUMLLinkDescriptor>();
		for (Iterator<?> links = container.getEdges().iterator(); links
				.hasNext();) {
			EObject linkObject = (EObject) links.next();
			if (false == linkObject instanceof ControlFlow) {
				continue;
			}
			ControlFlow link = (ControlFlow) linkObject;
			if (ControlFlowEditPart.VISUAL_ID != TUMLVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			ActivityNode dst = link.getTarget();
			ActivityNode src = link.getSource();
			result.add(new TUMLLinkDescriptor(src, dst, link,
					TUMLElementTypes.ControlFlow_4005,
					ControlFlowEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<TUMLLinkDescriptor> getContainedTypeModelFacetLinks_ObjectFlow_4006(
			ActivityGroup container) {
		LinkedList<TUMLLinkDescriptor> result = new LinkedList<TUMLLinkDescriptor>();
		for (Iterator<?> links = container.getEdges().iterator(); links
				.hasNext();) {
			EObject linkObject = (EObject) links.next();
			if (false == linkObject instanceof ObjectFlow) {
				continue;
			}
			ObjectFlow link = (ObjectFlow) linkObject;
			if (ObjectFlowEditPart.VISUAL_ID != TUMLVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			ActivityNode dst = link.getTarget();
			ActivityNode src = link.getSource();
			result.add(new TUMLLinkDescriptor(src, dst, link,
					TUMLElementTypes.ObjectFlow_4006,
					ObjectFlowEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<TUMLLinkDescriptor> getIncomingTypeModelFacetLinks_Realization_4001(
			Classifier target,
			Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences) {
		LinkedList<TUMLLinkDescriptor> result = new LinkedList<TUMLLinkDescriptor>();
		Collection<EStructuralFeature.Setting> settings = crossReferences
				.get(target);
		for (EStructuralFeature.Setting setting : settings) {
			if (setting.getEStructuralFeature() != TailedUMLPackage.eINSTANCE
					.getRelationship_Target()
					|| false == setting.getEObject() instanceof Realization) {
				continue;
			}
			Realization link = (Realization) setting.getEObject();
			if (RealizationEditPart.VISUAL_ID != TUMLVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			Classifier src = link.getSource();
			result.add(new TUMLLinkDescriptor(src, target, link,
					TUMLElementTypes.Realization_4001,
					RealizationEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<TUMLLinkDescriptor> getIncomingTypeModelFacetLinks_Generalization_4002(
			Classifier target,
			Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences) {
		LinkedList<TUMLLinkDescriptor> result = new LinkedList<TUMLLinkDescriptor>();
		Collection<EStructuralFeature.Setting> settings = crossReferences
				.get(target);
		for (EStructuralFeature.Setting setting : settings) {
			if (setting.getEStructuralFeature() != TailedUMLPackage.eINSTANCE
					.getRelationship_Target()
					|| false == setting.getEObject() instanceof Generalization) {
				continue;
			}
			Generalization link = (Generalization) setting.getEObject();
			if (GeneralizationEditPart.VISUAL_ID != TUMLVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			Classifier src = link.getSource();
			result.add(new TUMLLinkDescriptor(src, target, link,
					TUMLElementTypes.Generalization_4002,
					GeneralizationEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<TUMLLinkDescriptor> getIncomingTypeModelFacetLinks_Reference_4003(
			Classifier target,
			Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences) {
		LinkedList<TUMLLinkDescriptor> result = new LinkedList<TUMLLinkDescriptor>();
		Collection<EStructuralFeature.Setting> settings = crossReferences
				.get(target);
		for (EStructuralFeature.Setting setting : settings) {
			if (setting.getEStructuralFeature() != TailedUMLPackage.eINSTANCE
					.getRelationship_Target()
					|| false == setting.getEObject() instanceof Reference) {
				continue;
			}
			Reference link = (Reference) setting.getEObject();
			if (ReferenceEditPart.VISUAL_ID != TUMLVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			Classifier src = link.getSource();
			result.add(new TUMLLinkDescriptor(src, target, link,
					TUMLElementTypes.Reference_4003,
					ReferenceEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<TUMLLinkDescriptor> getIncomingTypeModelFacetLinks_Constraint_4004(
			Classifier target,
			Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences) {
		LinkedList<TUMLLinkDescriptor> result = new LinkedList<TUMLLinkDescriptor>();
		Collection<EStructuralFeature.Setting> settings = crossReferences
				.get(target);
		for (EStructuralFeature.Setting setting : settings) {
			if (setting.getEStructuralFeature() != TailedUMLPackage.eINSTANCE
					.getRelationship_Target()
					|| false == setting.getEObject() instanceof Constraint) {
				continue;
			}
			Constraint link = (Constraint) setting.getEObject();
			if (ConstraintEditPart.VISUAL_ID != TUMLVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			Classifier src = link.getSource();
			result.add(new TUMLLinkDescriptor(src, target, link,
					TUMLElementTypes.Constraint_4004,
					ConstraintEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<TUMLLinkDescriptor> getIncomingTypeModelFacetLinks_ControlFlow_4005(
			ActivityNode target,
			Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences) {
		LinkedList<TUMLLinkDescriptor> result = new LinkedList<TUMLLinkDescriptor>();
		Collection<EStructuralFeature.Setting> settings = crossReferences
				.get(target);
		for (EStructuralFeature.Setting setting : settings) {
			if (setting.getEStructuralFeature() != TailedUMLPackage.eINSTANCE
					.getActivityEdge_Target()
					|| false == setting.getEObject() instanceof ControlFlow) {
				continue;
			}
			ControlFlow link = (ControlFlow) setting.getEObject();
			if (ControlFlowEditPart.VISUAL_ID != TUMLVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			ActivityNode src = link.getSource();
			result.add(new TUMLLinkDescriptor(src, target, link,
					TUMLElementTypes.ControlFlow_4005,
					ControlFlowEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<TUMLLinkDescriptor> getIncomingTypeModelFacetLinks_ObjectFlow_4006(
			ActivityNode target,
			Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences) {
		LinkedList<TUMLLinkDescriptor> result = new LinkedList<TUMLLinkDescriptor>();
		Collection<EStructuralFeature.Setting> settings = crossReferences
				.get(target);
		for (EStructuralFeature.Setting setting : settings) {
			if (setting.getEStructuralFeature() != TailedUMLPackage.eINSTANCE
					.getActivityEdge_Target()
					|| false == setting.getEObject() instanceof ObjectFlow) {
				continue;
			}
			ObjectFlow link = (ObjectFlow) setting.getEObject();
			if (ObjectFlowEditPart.VISUAL_ID != TUMLVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			ActivityNode src = link.getSource();
			result.add(new TUMLLinkDescriptor(src, target, link,
					TUMLElementTypes.ObjectFlow_4006,
					ObjectFlowEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<TUMLLinkDescriptor> getIncomingFeatureModelFacetLinks_Activity_RelatedOperation_4007(
			Operation target,
			Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences) {
		LinkedList<TUMLLinkDescriptor> result = new LinkedList<TUMLLinkDescriptor>();
		Collection<EStructuralFeature.Setting> settings = crossReferences
				.get(target);
		for (EStructuralFeature.Setting setting : settings) {
			if (setting.getEStructuralFeature() == TailedUMLPackage.eINSTANCE
					.getActivity_RelatedOperation()) {
				result.add(new TUMLLinkDescriptor(setting.getEObject(), target,
						TUMLElementTypes.ActivityRelatedOperation_4007,
						ActivityRelatedOperationEditPart.VISUAL_ID));
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<TUMLLinkDescriptor> getIncomingFeatureModelFacetLinks_CallAction_Method_4008(
			Operation target,
			Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences) {
		LinkedList<TUMLLinkDescriptor> result = new LinkedList<TUMLLinkDescriptor>();
		Collection<EStructuralFeature.Setting> settings = crossReferences
				.get(target);
		for (EStructuralFeature.Setting setting : settings) {
			if (setting.getEStructuralFeature() == TailedUMLPackage.eINSTANCE
					.getCallAction_Method()) {
				result.add(new TUMLLinkDescriptor(setting.getEObject(), target,
						TUMLElementTypes.CallActionMethod_4008,
						CallActionMethodEditPart.VISUAL_ID));
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<TUMLLinkDescriptor> getIncomingFeatureModelFacetLinks_VxAction_VariantFragments_4009(
			VariationActivityFragment target,
			Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences) {
		LinkedList<TUMLLinkDescriptor> result = new LinkedList<TUMLLinkDescriptor>();
		Collection<EStructuralFeature.Setting> settings = crossReferences
				.get(target);
		for (EStructuralFeature.Setting setting : settings) {
			if (setting.getEStructuralFeature() == VxActivityPackage.eINSTANCE
					.getVxAction_VariantFragments()) {
				result.add(new TUMLLinkDescriptor(setting.getEObject(), target,
						TUMLElementTypes.VxActionVariantFragments_4009,
						VxActionVariantFragmentsEditPart.VISUAL_ID));
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<TUMLLinkDescriptor> getIncomingFeatureModelFacetLinks_VariationPoint_Variations_4010(
			Variation target,
			Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences) {
		LinkedList<TUMLLinkDescriptor> result = new LinkedList<TUMLLinkDescriptor>();
		Collection<EStructuralFeature.Setting> settings = crossReferences
				.get(target);
		for (EStructuralFeature.Setting setting : settings) {
			if (setting.getEStructuralFeature() == VxModelPackage.eINSTANCE
					.getVariationPoint_Variations()) {
				result.add(new TUMLLinkDescriptor(setting.getEObject(), target,
						TUMLElementTypes.VariationPointVariations_4010,
						VariationPointVariationsEditPart.VISUAL_ID));
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<TUMLLinkDescriptor> getOutgoingTypeModelFacetLinks_Realization_4001(
			Classifier source) {
		Package container = null;
		// Find container element for the link.
		// Climb up by containment hierarchy starting from the source
		// and return the first element that is instance of the container class.
		for (EObject element = source; element != null && container == null; element = element
				.eContainer()) {
			if (element instanceof Package) {
				container = (Package) element;
			}
		}
		if (container == null) {
			return Collections.emptyList();
		}
		LinkedList<TUMLLinkDescriptor> result = new LinkedList<TUMLLinkDescriptor>();
		for (Iterator<?> links = container.getRelationships().iterator(); links
				.hasNext();) {
			EObject linkObject = (EObject) links.next();
			if (false == linkObject instanceof Realization) {
				continue;
			}
			Realization link = (Realization) linkObject;
			if (RealizationEditPart.VISUAL_ID != TUMLVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			Classifier dst = link.getTarget();
			Classifier src = link.getSource();
			if (src != source) {
				continue;
			}
			result.add(new TUMLLinkDescriptor(src, dst, link,
					TUMLElementTypes.Realization_4001,
					RealizationEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<TUMLLinkDescriptor> getOutgoingTypeModelFacetLinks_Generalization_4002(
			Classifier source) {
		Package container = null;
		// Find container element for the link.
		// Climb up by containment hierarchy starting from the source
		// and return the first element that is instance of the container class.
		for (EObject element = source; element != null && container == null; element = element
				.eContainer()) {
			if (element instanceof Package) {
				container = (Package) element;
			}
		}
		if (container == null) {
			return Collections.emptyList();
		}
		LinkedList<TUMLLinkDescriptor> result = new LinkedList<TUMLLinkDescriptor>();
		for (Iterator<?> links = container.getRelationships().iterator(); links
				.hasNext();) {
			EObject linkObject = (EObject) links.next();
			if (false == linkObject instanceof Generalization) {
				continue;
			}
			Generalization link = (Generalization) linkObject;
			if (GeneralizationEditPart.VISUAL_ID != TUMLVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			Classifier dst = link.getTarget();
			Classifier src = link.getSource();
			if (src != source) {
				continue;
			}
			result.add(new TUMLLinkDescriptor(src, dst, link,
					TUMLElementTypes.Generalization_4002,
					GeneralizationEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<TUMLLinkDescriptor> getOutgoingTypeModelFacetLinks_Reference_4003(
			Classifier source) {
		Package container = null;
		// Find container element for the link.
		// Climb up by containment hierarchy starting from the source
		// and return the first element that is instance of the container class.
		for (EObject element = source; element != null && container == null; element = element
				.eContainer()) {
			if (element instanceof Package) {
				container = (Package) element;
			}
		}
		if (container == null) {
			return Collections.emptyList();
		}
		LinkedList<TUMLLinkDescriptor> result = new LinkedList<TUMLLinkDescriptor>();
		for (Iterator<?> links = container.getRelationships().iterator(); links
				.hasNext();) {
			EObject linkObject = (EObject) links.next();
			if (false == linkObject instanceof Reference) {
				continue;
			}
			Reference link = (Reference) linkObject;
			if (ReferenceEditPart.VISUAL_ID != TUMLVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			Classifier dst = link.getTarget();
			Classifier src = link.getSource();
			if (src != source) {
				continue;
			}
			result.add(new TUMLLinkDescriptor(src, dst, link,
					TUMLElementTypes.Reference_4003,
					ReferenceEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<TUMLLinkDescriptor> getOutgoingTypeModelFacetLinks_Constraint_4004(
			Classifier source) {
		Package container = null;
		// Find container element for the link.
		// Climb up by containment hierarchy starting from the source
		// and return the first element that is instance of the container class.
		for (EObject element = source; element != null && container == null; element = element
				.eContainer()) {
			if (element instanceof Package) {
				container = (Package) element;
			}
		}
		if (container == null) {
			return Collections.emptyList();
		}
		LinkedList<TUMLLinkDescriptor> result = new LinkedList<TUMLLinkDescriptor>();
		for (Iterator<?> links = container.getRelationships().iterator(); links
				.hasNext();) {
			EObject linkObject = (EObject) links.next();
			if (false == linkObject instanceof Constraint) {
				continue;
			}
			Constraint link = (Constraint) linkObject;
			if (ConstraintEditPart.VISUAL_ID != TUMLVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			Classifier dst = link.getTarget();
			Classifier src = link.getSource();
			if (src != source) {
				continue;
			}
			result.add(new TUMLLinkDescriptor(src, dst, link,
					TUMLElementTypes.Constraint_4004,
					ConstraintEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<TUMLLinkDescriptor> getOutgoingTypeModelFacetLinks_ControlFlow_4005(
			ActivityNode source) {
		ActivityGroup container = null;
		// Find container element for the link.
		// Climb up by containment hierarchy starting from the source
		// and return the first element that is instance of the container class.
		for (EObject element = source; element != null && container == null; element = element
				.eContainer()) {
			if (element instanceof ActivityGroup) {
				container = (ActivityGroup) element;
			}
		}
		if (container == null) {
			return Collections.emptyList();
		}
		LinkedList<TUMLLinkDescriptor> result = new LinkedList<TUMLLinkDescriptor>();
		for (Iterator<?> links = container.getEdges().iterator(); links
				.hasNext();) {
			EObject linkObject = (EObject) links.next();
			if (false == linkObject instanceof ControlFlow) {
				continue;
			}
			ControlFlow link = (ControlFlow) linkObject;
			if (ControlFlowEditPart.VISUAL_ID != TUMLVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			ActivityNode dst = link.getTarget();
			ActivityNode src = link.getSource();
			if (src != source) {
				continue;
			}
			result.add(new TUMLLinkDescriptor(src, dst, link,
					TUMLElementTypes.ControlFlow_4005,
					ControlFlowEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<TUMLLinkDescriptor> getOutgoingTypeModelFacetLinks_ObjectFlow_4006(
			ActivityNode source) {
		ActivityGroup container = null;
		// Find container element for the link.
		// Climb up by containment hierarchy starting from the source
		// and return the first element that is instance of the container class.
		for (EObject element = source; element != null && container == null; element = element
				.eContainer()) {
			if (element instanceof ActivityGroup) {
				container = (ActivityGroup) element;
			}
		}
		if (container == null) {
			return Collections.emptyList();
		}
		LinkedList<TUMLLinkDescriptor> result = new LinkedList<TUMLLinkDescriptor>();
		for (Iterator<?> links = container.getEdges().iterator(); links
				.hasNext();) {
			EObject linkObject = (EObject) links.next();
			if (false == linkObject instanceof ObjectFlow) {
				continue;
			}
			ObjectFlow link = (ObjectFlow) linkObject;
			if (ObjectFlowEditPart.VISUAL_ID != TUMLVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			ActivityNode dst = link.getTarget();
			ActivityNode src = link.getSource();
			if (src != source) {
				continue;
			}
			result.add(new TUMLLinkDescriptor(src, dst, link,
					TUMLElementTypes.ObjectFlow_4006,
					ObjectFlowEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<TUMLLinkDescriptor> getOutgoingFeatureModelFacetLinks_Activity_RelatedOperation_4007(
			Activity source) {
		LinkedList<TUMLLinkDescriptor> result = new LinkedList<TUMLLinkDescriptor>();
		Operation destination = source.getRelatedOperation();
		if (destination == null) {
			return result;
		}
		result.add(new TUMLLinkDescriptor(source, destination,
				TUMLElementTypes.ActivityRelatedOperation_4007,
				ActivityRelatedOperationEditPart.VISUAL_ID));
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<TUMLLinkDescriptor> getOutgoingFeatureModelFacetLinks_CallAction_Method_4008(
			CallAction source) {
		LinkedList<TUMLLinkDescriptor> result = new LinkedList<TUMLLinkDescriptor>();
		Operation destination = source.getMethod();
		if (destination == null) {
			return result;
		}
		result.add(new TUMLLinkDescriptor(source, destination,
				TUMLElementTypes.CallActionMethod_4008,
				CallActionMethodEditPart.VISUAL_ID));
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<TUMLLinkDescriptor> getOutgoingFeatureModelFacetLinks_VxAction_VariantFragments_4009(
			VxAction source) {
		LinkedList<TUMLLinkDescriptor> result = new LinkedList<TUMLLinkDescriptor>();
		for (Iterator<?> destinations = source.getVariantFragments().iterator(); destinations
				.hasNext();) {
			VariationActivityFragment destination = (VariationActivityFragment) destinations
					.next();
			result.add(new TUMLLinkDescriptor(source, destination,
					TUMLElementTypes.VxActionVariantFragments_4009,
					VxActionVariantFragmentsEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<TUMLLinkDescriptor> getOutgoingFeatureModelFacetLinks_VariationPoint_Variations_4010(
			VariationPoint source) {
		LinkedList<TUMLLinkDescriptor> result = new LinkedList<TUMLLinkDescriptor>();
		for (Iterator<?> destinations = source.getVariations().iterator(); destinations
				.hasNext();) {
			Variation destination = (Variation) destinations.next();
			result.add(new TUMLLinkDescriptor(source, destination,
					TUMLElementTypes.VariationPointVariations_4010,
					VariationPointVariationsEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static final DiagramUpdater TYPED_INSTANCE = new DiagramUpdater() {
		/**
		 * @generated
		 */
		@Override
		public List<TUMLNodeDescriptor> getSemanticChildren(View view) {
			return TUMLDiagramUpdater.getSemanticChildren(view);
		}

		/**
		 * @generated
		 */
		@Override
		public List<TUMLLinkDescriptor> getContainedLinks(View view) {
			return TUMLDiagramUpdater.getContainedLinks(view);
		}

		/**
		 * @generated
		 */
		@Override
		public List<TUMLLinkDescriptor> getIncomingLinks(View view) {
			return TUMLDiagramUpdater.getIncomingLinks(view);
		}

		/**
		 * @generated
		 */
		@Override
		public List<TUMLLinkDescriptor> getOutgoingLinks(View view) {
			return TUMLDiagramUpdater.getOutgoingLinks(view);
		}
	};

}
