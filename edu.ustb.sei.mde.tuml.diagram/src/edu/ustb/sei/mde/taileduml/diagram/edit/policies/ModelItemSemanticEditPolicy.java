package edu.ustb.sei.mde.taileduml.diagram.edit.policies;

import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.emf.commands.core.commands.DuplicateEObjectsCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.DuplicateElementsRequest;

import edu.ustb.sei.mde.taileduml.diagram.edit.commands.ActivityCreateCommand;
import edu.ustb.sei.mde.taileduml.diagram.edit.commands.PackageCreateCommand;
import edu.ustb.sei.mde.taileduml.diagram.edit.commands.PrimitiveTypeCreateCommand;
import edu.ustb.sei.mde.taileduml.diagram.edit.commands.VariationActivityFragmentCreateCommand;
import edu.ustb.sei.mde.taileduml.diagram.providers.TUMLElementTypes;

/**
 * @generated
 */
public class ModelItemSemanticEditPolicy extends TUMLBaseItemSemanticEditPolicy {

	/**
	 * @generated
	 */
	public ModelItemSemanticEditPolicy() {
		super(TUMLElementTypes.Model_1000);
	}

	/**
	 * @generated
	 */
	protected Command getCreateCommand(CreateElementRequest req) {
		if (TUMLElementTypes.Package_2001 == req.getElementType()) {
			return getGEFWrapper(new PackageCreateCommand(req));
		}
		if (TUMLElementTypes.Activity_2002 == req.getElementType()) {
			return getGEFWrapper(new ActivityCreateCommand(req));
		}
		if (TUMLElementTypes.PrimitiveType_2003 == req.getElementType()) {
			return getGEFWrapper(new PrimitiveTypeCreateCommand(req));
		}
		if (TUMLElementTypes.VariationActivityFragment_2004 == req
				.getElementType()) {
			return getGEFWrapper(new VariationActivityFragmentCreateCommand(req));
		}
		return super.getCreateCommand(req);
	}

	/**
	 * @generated
	 */
	protected Command getDuplicateCommand(DuplicateElementsRequest req) {
		TransactionalEditingDomain editingDomain = ((IGraphicalEditPart) getHost())
				.getEditingDomain();
		return getGEFWrapper(new DuplicateAnythingCommand(editingDomain, req));
	}

	/**
	 * @generated
	 */
	private static class DuplicateAnythingCommand extends
			DuplicateEObjectsCommand {

		/**
		 * @generated
		 */
		public DuplicateAnythingCommand(
				TransactionalEditingDomain editingDomain,
				DuplicateElementsRequest req) {
			super(editingDomain, req.getLabel(), req
					.getElementsToBeDuplicated(), req
					.getAllDuplicatedElementsMap());
		}

	}

}
