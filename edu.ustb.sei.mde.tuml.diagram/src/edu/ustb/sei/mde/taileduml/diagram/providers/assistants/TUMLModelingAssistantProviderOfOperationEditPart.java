package edu.ustb.sei.mde.taileduml.diagram.providers.assistants;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;

import edu.ustb.sei.mde.taileduml.diagram.edit.parts.OperationEditPart;
import edu.ustb.sei.mde.taileduml.diagram.providers.TUMLElementTypes;
import edu.ustb.sei.mde.taileduml.diagram.providers.TUMLModelingAssistantProvider;

/**
 * @generated
 */
public class TUMLModelingAssistantProviderOfOperationEditPart extends
		TUMLModelingAssistantProvider {

	/**
	 * @generated
	 */
	@Override
	public List<IElementType> getRelTypesOnTarget(IAdaptable target) {
		IGraphicalEditPart targetEditPart = (IGraphicalEditPart) target
				.getAdapter(IGraphicalEditPart.class);
		return doGetRelTypesOnTarget((OperationEditPart) targetEditPart);
	}

	/**
	 * @generated
	 */
	public List<IElementType> doGetRelTypesOnTarget(OperationEditPart target) {
		List<IElementType> types = new ArrayList<IElementType>(2);
		types.add(TUMLElementTypes.ActivityRelatedOperation_4007);
		types.add(TUMLElementTypes.CallActionMethod_4008);
		return types;
	}

	/**
	 * @generated
	 */
	@Override
	public List<IElementType> getTypesForSource(IAdaptable target,
			IElementType relationshipType) {
		IGraphicalEditPart targetEditPart = (IGraphicalEditPart) target
				.getAdapter(IGraphicalEditPart.class);
		return doGetTypesForSource((OperationEditPart) targetEditPart,
				relationshipType);
	}

	/**
	 * @generated
	 */
	public List<IElementType> doGetTypesForSource(OperationEditPart target,
			IElementType relationshipType) {
		List<IElementType> types = new ArrayList<IElementType>();
		if (relationshipType == TUMLElementTypes.ActivityRelatedOperation_4007) {
			types.add(TUMLElementTypes.Activity_2002);
		} else if (relationshipType == TUMLElementTypes.CallActionMethod_4008) {
			types.add(TUMLElementTypes.CallAction_3027);
		}
		return types;
	}

}
