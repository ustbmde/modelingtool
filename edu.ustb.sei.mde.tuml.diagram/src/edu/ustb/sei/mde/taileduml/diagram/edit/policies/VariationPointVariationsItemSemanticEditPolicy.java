package edu.ustb.sei.mde.taileduml.diagram.edit.policies;

import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.emf.type.core.commands.DestroyReferenceCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.DestroyReferenceRequest;

import edu.ustb.sei.mde.taileduml.TailedUMLPackage;
import edu.ustb.sei.mde.taileduml.diagram.providers.TUMLElementTypes;
import edu.ustb.sei.mde.taileduml.vxmodel.VxModelPackage;

/**
 * @generated
 */
public class VariationPointVariationsItemSemanticEditPolicy extends
		TUMLBaseItemSemanticEditPolicy {

	/**
	 * @generated
	 */
	public VariationPointVariationsItemSemanticEditPolicy() {
		super(TUMLElementTypes.VariationPointVariations_4010);
	}

	/**
	 * @generated NOT
	 */
	protected Command getDestroyReferenceCommand(DestroyReferenceRequest req) {
		req.setContainingFeature(VxModelPackage.eINSTANCE
				.getVariationPoint_Variations());
		return getGEFWrapper(new DestroyReferenceCommand(req));
	}

}
