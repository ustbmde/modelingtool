package edu.ustb.sei.mde.taileduml.diagram.preferences;

import org.eclipse.gmf.runtime.diagram.ui.preferences.PrintingPreferencePage;

import edu.ustb.sei.mde.taileduml.diagram.part.TUMLDiagramEditorPlugin;

/**
 * @generated
 */
public class DiagramPrintingPreferencePage extends PrintingPreferencePage {

	/**
	 * @generated
	 */
	public DiagramPrintingPreferencePage() {
		setPreferenceStore(TUMLDiagramEditorPlugin.getInstance()
				.getPreferenceStore());
	}
}
