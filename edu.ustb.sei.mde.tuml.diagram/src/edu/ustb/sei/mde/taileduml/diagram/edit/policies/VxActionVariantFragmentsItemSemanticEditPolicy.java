package edu.ustb.sei.mde.taileduml.diagram.edit.policies;

import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.emf.type.core.commands.DestroyReferenceCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.DestroyReferenceRequest;

import edu.ustb.sei.mde.taileduml.diagram.providers.TUMLElementTypes;
import edu.ustb.sei.mde.taileduml.vxactivity.VxActivityPackage;

/**
 * @generated
 */
public class VxActionVariantFragmentsItemSemanticEditPolicy extends
		TUMLBaseItemSemanticEditPolicy {

	/**
	 * @generated
	 */
	public VxActionVariantFragmentsItemSemanticEditPolicy() {
		super(TUMLElementTypes.VxActionVariantFragments_4009);
	}

	/**
	 * @generated NOT
	 */
	protected Command getDestroyReferenceCommand(DestroyReferenceRequest req) {
		req.setContainingFeature(VxActivityPackage.eINSTANCE
				.getVxAction_VariantFragments());
		return getGEFWrapper(new DestroyReferenceCommand(req));
	}

}
