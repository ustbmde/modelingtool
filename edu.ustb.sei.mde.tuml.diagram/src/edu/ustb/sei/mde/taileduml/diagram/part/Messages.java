package edu.ustb.sei.mde.taileduml.diagram.part;

import org.eclipse.osgi.util.NLS;

/**
 * @generated
 */
public class Messages extends NLS {

	/**
	 * @generated
	 */
	static {
		NLS.initializeMessages("messages", Messages.class); //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	private Messages() {
	}

	/**
	 * @generated
	 */
	public static String TUMLCreationWizardTitle;

	/**
	 * @generated
	 */
	public static String TUMLCreationWizard_DiagramModelFilePageTitle;

	/**
	 * @generated
	 */
	public static String TUMLCreationWizard_DiagramModelFilePageDescription;

	/**
	 * @generated
	 */
	public static String TUMLCreationWizard_DomainModelFilePageTitle;

	/**
	 * @generated
	 */
	public static String TUMLCreationWizard_DomainModelFilePageDescription;

	/**
	 * @generated
	 */
	public static String TUMLCreationWizardOpenEditorError;

	/**
	 * @generated
	 */
	public static String TUMLCreationWizardCreationError;

	/**
	 * @generated
	 */
	public static String TUMLCreationWizardPageExtensionError;

	/**
	 * @generated
	 */
	public static String TUMLDiagramEditorUtil_OpenModelResourceErrorDialogTitle;

	/**
	 * @generated
	 */
	public static String TUMLDiagramEditorUtil_OpenModelResourceErrorDialogMessage;

	/**
	 * @generated
	 */
	public static String TUMLDiagramEditorUtil_CreateDiagramProgressTask;

	/**
	 * @generated
	 */
	public static String TUMLDiagramEditorUtil_CreateDiagramCommandLabel;

	/**
	 * @generated
	 */
	public static String TUMLDocumentProvider_isModifiable;

	/**
	 * @generated
	 */
	public static String TUMLDocumentProvider_handleElementContentChanged;

	/**
	 * @generated
	 */
	public static String TUMLDocumentProvider_IncorrectInputError;

	/**
	 * @generated
	 */
	public static String TUMLDocumentProvider_NoDiagramInResourceError;

	/**
	 * @generated
	 */
	public static String TUMLDocumentProvider_DiagramLoadingError;

	/**
	 * @generated
	 */
	public static String TUMLDocumentProvider_UnsynchronizedFileSaveError;

	/**
	 * @generated
	 */
	public static String TUMLDocumentProvider_SaveDiagramTask;

	/**
	 * @generated
	 */
	public static String TUMLDocumentProvider_SaveNextResourceTask;

	/**
	 * @generated
	 */
	public static String TUMLDocumentProvider_SaveAsOperation;

	/**
	 * @generated
	 */
	public static String InitDiagramFile_ResourceErrorDialogTitle;

	/**
	 * @generated
	 */
	public static String InitDiagramFile_ResourceErrorDialogMessage;

	/**
	 * @generated
	 */
	public static String InitDiagramFile_WizardTitle;

	/**
	 * @generated
	 */
	public static String InitDiagramFile_OpenModelFileDialogTitle;

	/**
	 * @generated
	 */
	public static String TUMLNewDiagramFileWizard_CreationPageName;

	/**
	 * @generated
	 */
	public static String TUMLNewDiagramFileWizard_CreationPageTitle;

	/**
	 * @generated
	 */
	public static String TUMLNewDiagramFileWizard_CreationPageDescription;

	/**
	 * @generated
	 */
	public static String TUMLNewDiagramFileWizard_RootSelectionPageName;

	/**
	 * @generated
	 */
	public static String TUMLNewDiagramFileWizard_RootSelectionPageTitle;

	/**
	 * @generated
	 */
	public static String TUMLNewDiagramFileWizard_RootSelectionPageDescription;

	/**
	 * @generated
	 */
	public static String TUMLNewDiagramFileWizard_RootSelectionPageSelectionTitle;

	/**
	 * @generated
	 */
	public static String TUMLNewDiagramFileWizard_RootSelectionPageNoSelectionMessage;

	/**
	 * @generated
	 */
	public static String TUMLNewDiagramFileWizard_RootSelectionPageInvalidSelectionMessage;

	/**
	 * @generated
	 */
	public static String TUMLNewDiagramFileWizard_InitDiagramCommand;

	/**
	 * @generated
	 */
	public static String TUMLNewDiagramFileWizard_IncorrectRootError;

	/**
	 * @generated
	 */
	public static String TUMLDiagramEditor_SavingDeletedFile;

	/**
	 * @generated
	 */
	public static String TUMLDiagramEditor_SaveAsErrorTitle;

	/**
	 * @generated
	 */
	public static String TUMLDiagramEditor_SaveAsErrorMessage;

	/**
	 * @generated
	 */
	public static String TUMLDiagramEditor_SaveErrorTitle;

	/**
	 * @generated
	 */
	public static String TUMLDiagramEditor_SaveErrorMessage;

	/**
	 * @generated
	 */
	public static String TUMLElementChooserDialog_SelectModelElementTitle;

	/**
	 * @generated
	 */
	public static String ModelElementSelectionPageMessage;

	/**
	 * @generated
	 */
	public static String ValidateActionMessage;

	/**
	 * @generated
	 */
	public static String StandardTool1Group_title;

	/**
	 * @generated
	 */
	public static String ClassDiagram2Group_title;

	/**
	 * @generated
	 */
	public static String ActivityDiagram3Group_title;

	/**
	 * @generated
	 */
	public static String VxActivity4Group_title;

	/**
	 * @generated
	 */
	public static String VariationModel5Group_title;

	/**
	 * @generated
	 */
	public static String SelectTool_title;

	/**
	 * @generated
	 */
	public static String MarqueeTool_title;

	/**
	 * @generated
	 */
	public static String MarqueeTool_desc;

	/**
	 * @generated
	 */
	public static String Package1CreationTool_title;

	/**
	 * @generated
	 */
	public static String Package1CreationTool_desc;

	/**
	 * @generated
	 */
	public static String Class2CreationTool_title;

	/**
	 * @generated
	 */
	public static String Class2CreationTool_desc;

	/**
	 * @generated
	 */
	public static String Interface3CreationTool_title;

	/**
	 * @generated
	 */
	public static String Interface3CreationTool_desc;

	/**
	 * @generated
	 */
	public static String Property4CreationTool_title;

	/**
	 * @generated
	 */
	public static String Property4CreationTool_desc;

	/**
	 * @generated
	 */
	public static String Operation5CreationTool_title;

	/**
	 * @generated
	 */
	public static String Operation5CreationTool_desc;

	/**
	 * @generated
	 */
	public static String Parameter6CreationTool_title;

	/**
	 * @generated
	 */
	public static String Parameter6CreationTool_desc;

	/**
	 * @generated
	 */
	public static String Generalization7CreationTool_title;

	/**
	 * @generated
	 */
	public static String Generalization7CreationTool_desc;

	/**
	 * @generated
	 */
	public static String Realization8CreationTool_title;

	/**
	 * @generated
	 */
	public static String Realization8CreationTool_desc;

	/**
	 * @generated
	 */
	public static String Reference9CreationTool_title;

	/**
	 * @generated
	 */
	public static String Reference9CreationTool_desc;

	/**
	 * @generated
	 */
	public static String PrimitiveType10CreationTool_title;

	/**
	 * @generated
	 */
	public static String PrimitiveType10CreationTool_desc;

	/**
	 * @generated
	 */
	public static String Enumeration11CreationTool_title;

	/**
	 * @generated
	 */
	public static String Enumeration11CreationTool_desc;

	/**
	 * @generated
	 */
	public static String EnumerationLiteral12CreationTool_title;

	/**
	 * @generated
	 */
	public static String EnumerationLiteral12CreationTool_desc;

	/**
	 * @generated
	 */
	public static String Activity1CreationTool_title;

	/**
	 * @generated
	 */
	public static String Activity1CreationTool_desc;

	/**
	 * @generated
	 */
	public static String ActivityRelatedOperationLink2CreationTool_title;

	/**
	 * @generated
	 */
	public static String ActivityRelatedOperationLink2CreationTool_desc;

	/**
	 * @generated
	 */
	public static String CallAction3CreationTool_title;

	/**
	 * @generated
	 */
	public static String CallAction3CreationTool_desc;

	/**
	 * @generated
	 */
	public static String AssignAction4CreationTool_title;

	/**
	 * @generated
	 */
	public static String AssignAction4CreationTool_desc;

	/**
	 * @generated
	 */
	public static String EmptyAction5CreationTool_title;

	/**
	 * @generated
	 */
	public static String EmptyAction5CreationTool_desc;

	/**
	 * @generated
	 */
	public static String SendAction6CreationTool_title;

	/**
	 * @generated
	 */
	public static String SendAction6CreationTool_desc;

	/**
	 * @generated
	 */
	public static String AcceptAction7CreationTool_title;

	/**
	 * @generated
	 */
	public static String AcceptAction7CreationTool_desc;

	/**
	 * @generated
	 */
	public static String InitialNode8CreationTool_title;

	/**
	 * @generated
	 */
	public static String InitialNode8CreationTool_desc;

	/**
	 * @generated
	 */
	public static String FinalNode9CreationTool_title;

	/**
	 * @generated
	 */
	public static String FinalNode9CreationTool_desc;

	/**
	 * @generated
	 */
	public static String DecisionNode10CreationTool_title;

	/**
	 * @generated
	 */
	public static String DecisionNode10CreationTool_desc;

	/**
	 * @generated
	 */
	public static String MergeNode11CreationTool_title;

	/**
	 * @generated
	 */
	public static String MergeNode11CreationTool_desc;

	/**
	 * @generated
	 */
	public static String ForkNode12CreationTool_title;

	/**
	 * @generated
	 */
	public static String ForkNode12CreationTool_desc;

	/**
	 * @generated
	 */
	public static String JoinNode13CreationTool_title;

	/**
	 * @generated
	 */
	public static String JoinNode13CreationTool_desc;

	/**
	 * @generated
	 */
	public static String ObjectNode14CreationTool_title;

	/**
	 * @generated
	 */
	public static String ObjectNode14CreationTool_desc;

	/**
	 * @generated
	 */
	public static String ControlFlow15CreationTool_title;

	/**
	 * @generated
	 */
	public static String ControlFlow15CreationTool_desc;

	/**
	 * @generated
	 */
	public static String ObjectFlow16CreationTool_title;

	/**
	 * @generated
	 */
	public static String ObjectFlow16CreationTool_desc;

	/**
	 * @generated
	 */
	public static String CallOperationLink17CreationTool_title;

	/**
	 * @generated
	 */
	public static String VxAction1CreationTool_title;

	/**
	 * @generated
	 */
	public static String VxAction1CreationTool_desc;

	/**
	 * @generated
	 */
	public static String VariationActivityFragment2CreationTool_title;

	/**
	 * @generated
	 */
	public static String VariationActivityFragment2CreationTool_desc;

	/**
	 * @generated
	 */
	public static String RelatedVariantFragment3CreationTool_title;

	/**
	 * @generated
	 */
	public static String RelatedVariantFragment3CreationTool_desc;

	/**
	 * @generated
	 */
	public static String VariationPoint1CreationTool_title;

	/**
	 * @generated
	 */
	public static String VariationPoint1CreationTool_desc;

	/**
	 * @generated
	 */
	public static String Variation2CreationTool_title;

	/**
	 * @generated
	 */
	public static String Variation2CreationTool_desc;

	/**
	 * @generated
	 */
	public static String RelatedVariation3CreationTool_title;

	/**
	 * @generated
	 */
	public static String RelatedVariation3CreationTool_desc;

	/**
	 * @generated
	 */
	public static String Constraint4CreationTool_title;

	/**
	 * @generated
	 */
	public static String Constraint4CreationTool_desc;

	/**
	 * @generated
	 */
	public static String PackagePackageCompartmentEditPart_title;

	/**
	 * @generated
	 */
	public static String ClassClassAttributeCompartmentEditPart_title;

	/**
	 * @generated
	 */
	public static String ClassClassOperationCompartmentEditPart_title;

	/**
	 * @generated
	 */
	public static String EnumerationEnumerationCompartmentEditPart_title;

	/**
	 * @generated
	 */
	public static String InterfaceInterfaceOperationCompartmentEditPart_title;

	/**
	 * @generated
	 */
	public static String ActivityActivityCompartmentEditPart_title;

	/**
	 * @generated
	 */
	public static String VariationActivityFragmentVariationActivityFragmentCompartmentEditPart_title;

	/**
	 * @generated
	 */
	public static String CommandName_OpenDiagram;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Model_1000_links;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_VxActionVariantFragments_4009_target;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_VxActionVariantFragments_4009_source;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Interface_3006_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Interface_3006_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_CallAction_3027_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_CallAction_3027_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Constraint_4004_target;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Constraint_4004_source;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_ControlFlow_4005_target;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_ControlFlow_4005_source;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_VariationPointVariations_4010_target;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_VariationPointVariations_4010_source;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Reference_4003_target;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Reference_4003_source;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Realization_4001_target;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Realization_4001_source;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_ActivityFinalNode_3024_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_ActivityFinalNode_3024_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_ObjectFlow_4006_target;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_ObjectFlow_4006_source;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Operation_3003_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_VxAction_3030_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_VxAction_3030_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_JoinNode_3026_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_JoinNode_3026_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Variation_3016_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Variation_3016_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Class_3001_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Class_3001_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_VariationPoint_3015_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_VariationPoint_3015_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Operation_3007_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_ForkNode_3025_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_ForkNode_3025_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_ObjectNode_3031_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_ObjectNode_3031_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_AssignAction_3032_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_AssignAction_3032_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_EmptyAction_3033_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_EmptyAction_3033_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Generalization_4002_target;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Generalization_4002_source;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_ActivityRelatedOperation_4007_target;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_ActivityRelatedOperation_4007_source;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_SendAction_3028_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_SendAction_3028_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_DecisionNode_3021_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_DecisionNode_3021_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_VariationActivityFragment_2004_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_AcceptAction_3029_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_AcceptAction_3029_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_MergeNode_3022_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_MergeNode_3022_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_InitialNode_3023_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_InitialNode_3023_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Activity_2002_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_CallActionMethod_4008_target;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_CallActionMethod_4008_source;

	/**
	 * @generated
	 */
	public static String NavigatorActionProvider_OpenDiagramActionName;

	/**
	 * @generated
	 */
	public static String MessageFormatParser_InvalidInputError;

	/**
	 * @generated
	 */
	public static String TUMLModelingAssistantProviderTitle;

	/**
	 * @generated
	 */
	public static String TUMLModelingAssistantProviderMessage;

	//TODO: put accessor fields manually	
}
