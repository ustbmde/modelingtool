package edu.ustb.sei.mde.taileduml.diagram.providers;

import java.lang.ref.WeakReference;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPartFactory;
import org.eclipse.gmf.runtime.common.core.service.IOperation;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.diagram.ui.services.editpart.AbstractEditPartProvider;
import org.eclipse.gmf.runtime.diagram.ui.services.editpart.CreateGraphicEditPartOperation;
import org.eclipse.gmf.runtime.diagram.ui.services.editpart.IEditPartOperation;
import org.eclipse.gmf.runtime.notation.View;

import org.eclipse.gmf.tooling.runtime.providers.DefaultEditPartProvider;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.ModelEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.TUMLEditPartFactory;
import edu.ustb.sei.mde.taileduml.diagram.part.TUMLVisualIDRegistry;

/**
 * @generated
 */
public class TUMLEditPartProvider extends DefaultEditPartProvider {

	/**
	 * @generated
	 */
	public TUMLEditPartProvider() {
		super(new TUMLEditPartFactory(), TUMLVisualIDRegistry.TYPED_INSTANCE,
				ModelEditPart.MODEL_ID);
	}
}
