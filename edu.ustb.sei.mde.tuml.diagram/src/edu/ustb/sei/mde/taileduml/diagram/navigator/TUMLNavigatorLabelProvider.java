package edu.ustb.sei.mde.taileduml.diagram.navigator;

import org.eclipse.gmf.runtime.common.ui.services.parser.IParser;
import org.eclipse.gmf.runtime.common.ui.services.parser.ParserOptions;
import org.eclipse.gmf.runtime.emf.core.util.EObjectAdapter;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.jface.viewers.ITreePathLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.jface.viewers.ViewerLabel;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.IMemento;
import org.eclipse.ui.navigator.ICommonContentExtensionSite;
import org.eclipse.ui.navigator.ICommonLabelProvider;

import edu.ustb.sei.mde.taileduml.EmptyAction;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.AcceptActionEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.AcceptActionNameEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.ActivityEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.ActivityFinalNodeEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.ActivityNameEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.ActivityRelatedOperationEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.AssignActionEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.AssignActionNameEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.CallActionEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.CallActionMethodEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.CallActionNameEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.ClassEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.ClassNameEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.ConstraintEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.ConstraintTypeEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.ControlFlowConditionEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.ControlFlowEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.DecisionNodeEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.EmptyActionEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.EnumerationEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.EnumerationLiteralEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.EnumerationLiteralNameEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.EnumerationNameEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.ForkNodeEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.GeneralizationEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.InitialNodeEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.InterfaceEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.InterfaceNameEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.JoinNodeEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.MergeNodeEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.ModelEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.ObjectFlowEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.ObjectNodeEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.ObjectNodeNameEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.Operation2EditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.OperationEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.OperationName2EditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.OperationNameEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.PackageEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.PackageNameEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.PrimitiveTypeEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.PrimitiveTypeNameEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.PropertyEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.PropertyNameEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.RealizationEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.ReferenceEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.ReferenceNameEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.SendActionEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.SendActionNameEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.VariationActivityFragmentEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.VariationActivityFragmentNameEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.VariationEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.VariationNameEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.VariationPointEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.VariationPointNameEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.VariationPointVariationsEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.VxActionEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.VxActionNameEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.VxActionVariantFragmentsEditPart;
import edu.ustb.sei.mde.taileduml.diagram.part.TUMLDiagramEditorPlugin;
import edu.ustb.sei.mde.taileduml.diagram.part.TUMLVisualIDRegistry;
import edu.ustb.sei.mde.taileduml.diagram.providers.TUMLElementTypes;
import edu.ustb.sei.mde.taileduml.diagram.providers.TUMLParserProvider;

/**
 * @generated
 */
public class TUMLNavigatorLabelProvider extends LabelProvider implements
		ICommonLabelProvider, ITreePathLabelProvider {

	/**
	 * @generated
	 */
	static {
		TUMLDiagramEditorPlugin
				.getInstance()
				.getImageRegistry()
				.put("Navigator?UnknownElement", ImageDescriptor.getMissingImageDescriptor()); //$NON-NLS-1$
		TUMLDiagramEditorPlugin
				.getInstance()
				.getImageRegistry()
				.put("Navigator?ImageNotFound", ImageDescriptor.getMissingImageDescriptor()); //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	public void updateLabel(ViewerLabel label, TreePath elementPath) {
		Object element = elementPath.getLastSegment();
		if (element instanceof TUMLNavigatorItem
				&& !isOwnView(((TUMLNavigatorItem) element).getView())) {
			return;
		}
		label.setText(getText(element));
		label.setImage(getImage(element));
	}

	/**
	 * @generated
	 */
	public Image getImage(Object element) {
		if (element instanceof TUMLNavigatorGroup) {
			TUMLNavigatorGroup group = (TUMLNavigatorGroup) element;
			return TUMLDiagramEditorPlugin.getInstance().getBundledImage(
					group.getIcon());
		}

		if (element instanceof TUMLNavigatorItem) {
			TUMLNavigatorItem navigatorItem = (TUMLNavigatorItem) element;
			if (!isOwnView(navigatorItem.getView())) {
				return super.getImage(element);
			}
			return getImage(navigatorItem.getView());
		}

		return super.getImage(element);
	}

	/**
	 * @generated
	 */
	public Image getImage(View view) {
		switch (TUMLVisualIDRegistry.getVisualID(view)) {
		case ModelEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Diagram?edu.ustb.sei.mde.taileduml?Model", TUMLElementTypes.Model_1000); //$NON-NLS-1$
		case PackageEditPart.VISUAL_ID:
			return getImage(
					"Navigator?TopLevelNode?edu.ustb.sei.mde.taileduml?Package", TUMLElementTypes.Package_2001); //$NON-NLS-1$
		case ActivityEditPart.VISUAL_ID:
			return getImage(
					"Navigator?TopLevelNode?edu.ustb.sei.mde.taileduml?Activity", TUMLElementTypes.Activity_2002); //$NON-NLS-1$
		case PrimitiveTypeEditPart.VISUAL_ID:
			return getImage(
					"Navigator?TopLevelNode?edu.ustb.sei.mde.taileduml?PrimitiveType", TUMLElementTypes.PrimitiveType_2003); //$NON-NLS-1$
		case VariationActivityFragmentEditPart.VISUAL_ID:
			return getImage(
					"Navigator?TopLevelNode?edu.ustb.sei.mde.taileduml.vxactivity?VariationActivityFragment", TUMLElementTypes.VariationActivityFragment_2004); //$NON-NLS-1$
		case ClassEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?edu.ustb.sei.mde.taileduml?Class", TUMLElementTypes.Class_3001); //$NON-NLS-1$
		case PropertyEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?edu.ustb.sei.mde.taileduml?Property", TUMLElementTypes.Property_3002); //$NON-NLS-1$
		case OperationEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?edu.ustb.sei.mde.taileduml?Operation", TUMLElementTypes.Operation_3003); //$NON-NLS-1$
		case EnumerationEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?edu.ustb.sei.mde.taileduml?Enumeration", TUMLElementTypes.Enumeration_3004); //$NON-NLS-1$
		case EnumerationLiteralEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?edu.ustb.sei.mde.taileduml?EnumerationLiteral", TUMLElementTypes.EnumerationLiteral_3005); //$NON-NLS-1$
		case InterfaceEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?edu.ustb.sei.mde.taileduml?Interface", TUMLElementTypes.Interface_3006); //$NON-NLS-1$
		case Operation2EditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?edu.ustb.sei.mde.taileduml?Operation", TUMLElementTypes.Operation_3007); //$NON-NLS-1$
		case VariationPointEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?edu.ustb.sei.mde.taileduml.vxmodel?VariationPoint", TUMLElementTypes.VariationPoint_3015); //$NON-NLS-1$
		case VariationEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?edu.ustb.sei.mde.taileduml.vxmodel?Variation", TUMLElementTypes.Variation_3016); //$NON-NLS-1$
		case DecisionNodeEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?edu.ustb.sei.mde.taileduml?DecisionNode", TUMLElementTypes.DecisionNode_3021); //$NON-NLS-1$
		case MergeNodeEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?edu.ustb.sei.mde.taileduml?MergeNode", TUMLElementTypes.MergeNode_3022); //$NON-NLS-1$
		case InitialNodeEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?edu.ustb.sei.mde.taileduml?InitialNode", TUMLElementTypes.InitialNode_3023); //$NON-NLS-1$
		case ActivityFinalNodeEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?edu.ustb.sei.mde.taileduml?ActivityFinalNode", TUMLElementTypes.ActivityFinalNode_3024); //$NON-NLS-1$
		case ForkNodeEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?edu.ustb.sei.mde.taileduml?ForkNode", TUMLElementTypes.ForkNode_3025); //$NON-NLS-1$
		case JoinNodeEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?edu.ustb.sei.mde.taileduml?JoinNode", TUMLElementTypes.JoinNode_3026); //$NON-NLS-1$
		case CallActionEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?edu.ustb.sei.mde.taileduml?CallAction", TUMLElementTypes.CallAction_3027); //$NON-NLS-1$
		case SendActionEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?edu.ustb.sei.mde.taileduml?SendAction", TUMLElementTypes.SendAction_3028); //$NON-NLS-1$
		case AcceptActionEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?edu.ustb.sei.mde.taileduml?AcceptAction", TUMLElementTypes.AcceptAction_3029); //$NON-NLS-1$
		case VxActionEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?edu.ustb.sei.mde.taileduml.vxactivity?VxAction", TUMLElementTypes.VxAction_3030); //$NON-NLS-1$
		case ObjectNodeEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?edu.ustb.sei.mde.taileduml?ObjectNode", TUMLElementTypes.ObjectNode_3031); //$NON-NLS-1$
		case AssignActionEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?edu.ustb.sei.mde.taileduml?AssignAction", TUMLElementTypes.AssignAction_3032); //$NON-NLS-1$
		case EmptyActionEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?edu.ustb.sei.mde.taileduml?EmptyAction", TUMLElementTypes.EmptyAction_3033); //$NON-NLS-1$
		case RealizationEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Link?edu.ustb.sei.mde.taileduml?Realization", TUMLElementTypes.Realization_4001); //$NON-NLS-1$
		case GeneralizationEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Link?edu.ustb.sei.mde.taileduml?Generalization", TUMLElementTypes.Generalization_4002); //$NON-NLS-1$
		case ReferenceEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Link?edu.ustb.sei.mde.taileduml?Reference", TUMLElementTypes.Reference_4003); //$NON-NLS-1$
		case ConstraintEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Link?edu.ustb.sei.mde.taileduml.vxmodel?Constraint", TUMLElementTypes.Constraint_4004); //$NON-NLS-1$
		case ControlFlowEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Link?edu.ustb.sei.mde.taileduml?ControlFlow", TUMLElementTypes.ControlFlow_4005); //$NON-NLS-1$
		case ObjectFlowEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Link?edu.ustb.sei.mde.taileduml?ObjectFlow", TUMLElementTypes.ObjectFlow_4006); //$NON-NLS-1$
		case ActivityRelatedOperationEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Link?edu.ustb.sei.mde.taileduml?Activity?relatedOperation", TUMLElementTypes.ActivityRelatedOperation_4007); //$NON-NLS-1$
		case CallActionMethodEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Link?edu.ustb.sei.mde.taileduml?CallAction?method", TUMLElementTypes.CallActionMethod_4008); //$NON-NLS-1$
		case VxActionVariantFragmentsEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Link?edu.ustb.sei.mde.taileduml.vxactivity?VxAction?variantFragments", TUMLElementTypes.VxActionVariantFragments_4009); //$NON-NLS-1$
		case VariationPointVariationsEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Link?edu.ustb.sei.mde.taileduml.vxmodel?VariationPoint?variations", TUMLElementTypes.VariationPointVariations_4010); //$NON-NLS-1$
		}
		return getImage("Navigator?UnknownElement", null); //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	private Image getImage(String key, IElementType elementType) {
		ImageRegistry imageRegistry = TUMLDiagramEditorPlugin.getInstance()
				.getImageRegistry();
		Image image = imageRegistry.get(key);
		if (image == null && elementType != null
				&& TUMLElementTypes.isKnownElementType(elementType)) {
			image = TUMLElementTypes.getImage(elementType);
			imageRegistry.put(key, image);
		}

		if (image == null) {
			image = imageRegistry.get("Navigator?ImageNotFound"); //$NON-NLS-1$
			imageRegistry.put(key, image);
		}
		return image;
	}

	/**
	 * @generated
	 */
	public String getText(Object element) {
		if (element instanceof TUMLNavigatorGroup) {
			TUMLNavigatorGroup group = (TUMLNavigatorGroup) element;
			return group.getGroupName();
		}

		if (element instanceof TUMLNavigatorItem) {
			TUMLNavigatorItem navigatorItem = (TUMLNavigatorItem) element;
			if (!isOwnView(navigatorItem.getView())) {
				return null;
			}
			return getText(navigatorItem.getView());
		}

		return super.getText(element);
	}

	/**
	 * @generated
	 */
	public String getText(View view) {
		if (view.getElement() != null && view.getElement().eIsProxy()) {
			return getUnresolvedDomainElementProxyText(view);
		}
		switch (TUMLVisualIDRegistry.getVisualID(view)) {
		case ModelEditPart.VISUAL_ID:
			return getModel_1000Text(view);
		case PackageEditPart.VISUAL_ID:
			return getPackage_2001Text(view);
		case ActivityEditPart.VISUAL_ID:
			return getActivity_2002Text(view);
		case PrimitiveTypeEditPart.VISUAL_ID:
			return getPrimitiveType_2003Text(view);
		case VariationActivityFragmentEditPart.VISUAL_ID:
			return getVariationActivityFragment_2004Text(view);
		case ClassEditPart.VISUAL_ID:
			return getClass_3001Text(view);
		case PropertyEditPart.VISUAL_ID:
			return getProperty_3002Text(view);
		case OperationEditPart.VISUAL_ID:
			return getOperation_3003Text(view);
		case EnumerationEditPart.VISUAL_ID:
			return getEnumeration_3004Text(view);
		case EnumerationLiteralEditPart.VISUAL_ID:
			return getEnumerationLiteral_3005Text(view);
		case InterfaceEditPart.VISUAL_ID:
			return getInterface_3006Text(view);
		case Operation2EditPart.VISUAL_ID:
			return getOperation_3007Text(view);
		case VariationPointEditPart.VISUAL_ID:
			return getVariationPoint_3015Text(view);
		case VariationEditPart.VISUAL_ID:
			return getVariation_3016Text(view);
		case DecisionNodeEditPart.VISUAL_ID:
			return getDecisionNode_3021Text(view);
		case MergeNodeEditPart.VISUAL_ID:
			return getMergeNode_3022Text(view);
		case InitialNodeEditPart.VISUAL_ID:
			return getInitialNode_3023Text(view);
		case ActivityFinalNodeEditPart.VISUAL_ID:
			return getActivityFinalNode_3024Text(view);
		case ForkNodeEditPart.VISUAL_ID:
			return getForkNode_3025Text(view);
		case JoinNodeEditPart.VISUAL_ID:
			return getJoinNode_3026Text(view);
		case CallActionEditPart.VISUAL_ID:
			return getCallAction_3027Text(view);
		case SendActionEditPart.VISUAL_ID:
			return getSendAction_3028Text(view);
		case AcceptActionEditPart.VISUAL_ID:
			return getAcceptAction_3029Text(view);
		case VxActionEditPart.VISUAL_ID:
			return getVxAction_3030Text(view);
		case ObjectNodeEditPart.VISUAL_ID:
			return getObjectNode_3031Text(view);
		case AssignActionEditPart.VISUAL_ID:
			return getAssignAction_3032Text(view);
		case EmptyActionEditPart.VISUAL_ID:
			return getEmptyAction_3033Text(view);
		case RealizationEditPart.VISUAL_ID:
			return getRealization_4001Text(view);
		case GeneralizationEditPart.VISUAL_ID:
			return getGeneralization_4002Text(view);
		case ReferenceEditPart.VISUAL_ID:
			return getReference_4003Text(view);
		case ConstraintEditPart.VISUAL_ID:
			return getConstraint_4004Text(view);
		case ControlFlowEditPart.VISUAL_ID:
			return getControlFlow_4005Text(view);
		case ObjectFlowEditPart.VISUAL_ID:
			return getObjectFlow_4006Text(view);
		case ActivityRelatedOperationEditPart.VISUAL_ID:
			return getActivityRelatedOperation_4007Text(view);
		case CallActionMethodEditPart.VISUAL_ID:
			return getCallActionMethod_4008Text(view);
		case VxActionVariantFragmentsEditPart.VISUAL_ID:
			return getVxActionVariantFragments_4009Text(view);
		case VariationPointVariationsEditPart.VISUAL_ID:
			return getVariationPointVariations_4010Text(view);
		}
		return getUnknownElementText(view);
	}

	/**
	 * @generated
	 */
	private String getModel_1000Text(View view) {
		return ""; //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	private String getVxActionVariantFragments_4009Text(View view) {
		return ""; //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	private String getInterface_3006Text(View view) {
		IParser parser = TUMLParserProvider.getParser(
				TUMLElementTypes.Interface_3006,
				view.getElement() != null ? view.getElement() : view,
				TUMLVisualIDRegistry.getType(InterfaceNameEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			TUMLDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5007); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getCallAction_3027Text(View view) {
		IParser parser = TUMLParserProvider.getParser(
				TUMLElementTypes.CallAction_3027,
				view.getElement() != null ? view.getElement() : view,
				TUMLVisualIDRegistry.getType(CallActionNameEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			TUMLDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5021); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getConstraint_4004Text(View view) {
		IParser parser = TUMLParserProvider.getParser(
				TUMLElementTypes.Constraint_4004,
				view.getElement() != null ? view.getElement() : view,
				TUMLVisualIDRegistry.getType(ConstraintTypeEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			TUMLDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 6001); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getControlFlow_4005Text(View view) {
		IParser parser = TUMLParserProvider.getParser(
				TUMLElementTypes.ControlFlow_4005,
				view.getElement() != null ? view.getElement() : view,
				TUMLVisualIDRegistry
						.getType(ControlFlowConditionEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			TUMLDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 6002); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getVariationPointVariations_4010Text(View view) {
		return ""; //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	private String getReference_4003Text(View view) {
		IParser parser = TUMLParserProvider.getParser(
				TUMLElementTypes.Reference_4003,
				view.getElement() != null ? view.getElement() : view,
				TUMLVisualIDRegistry.getType(ReferenceNameEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			TUMLDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 6004); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getRealization_4001Text(View view) {
		return ""; //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	private String getActivityFinalNode_3024Text(View view) {
		return ""; //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	private String getObjectFlow_4006Text(View view) {
		return ""; //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	private String getOperation_3003Text(View view) {
		IParser parser = TUMLParserProvider.getParser(
				TUMLElementTypes.Operation_3003,
				view.getElement() != null ? view.getElement() : view,
				TUMLVisualIDRegistry.getType(OperationNameEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			TUMLDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5002); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getVxAction_3030Text(View view) {
		IParser parser = TUMLParserProvider.getParser(
				TUMLElementTypes.VxAction_3030,
				view.getElement() != null ? view.getElement() : view,
				TUMLVisualIDRegistry.getType(VxActionNameEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			TUMLDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5024); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getJoinNode_3026Text(View view) {
		return ""; //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	private String getVariation_3016Text(View view) {
		IParser parser = TUMLParserProvider.getParser(
				TUMLElementTypes.Variation_3016,
				view.getElement() != null ? view.getElement() : view,
				TUMLVisualIDRegistry.getType(VariationNameEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			TUMLDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5014); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getClass_3001Text(View view) {
		IParser parser = TUMLParserProvider.getParser(
				TUMLElementTypes.Class_3001,
				view.getElement() != null ? view.getElement() : view,
				TUMLVisualIDRegistry.getType(ClassNameEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			TUMLDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5003); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getProperty_3002Text(View view) {
		IParser parser = TUMLParserProvider.getParser(
				TUMLElementTypes.Property_3002,
				view.getElement() != null ? view.getElement() : view,
				TUMLVisualIDRegistry.getType(PropertyNameEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			TUMLDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5001); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getVariationPoint_3015Text(View view) {
		IParser parser = TUMLParserProvider.getParser(
				TUMLElementTypes.VariationPoint_3015,
				view.getElement() != null ? view.getElement() : view,
				TUMLVisualIDRegistry
						.getType(VariationPointNameEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			TUMLDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5013); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getEnumeration_3004Text(View view) {
		IParser parser = TUMLParserProvider
				.getParser(TUMLElementTypes.Enumeration_3004,
						view.getElement() != null ? view.getElement() : view,
						TUMLVisualIDRegistry
								.getType(EnumerationNameEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			TUMLDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5005); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getOperation_3007Text(View view) {
		IParser parser = TUMLParserProvider.getParser(
				TUMLElementTypes.Operation_3007,
				view.getElement() != null ? view.getElement() : view,
				TUMLVisualIDRegistry.getType(OperationName2EditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			TUMLDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5006); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getPackage_2001Text(View view) {
		IParser parser = TUMLParserProvider.getParser(
				TUMLElementTypes.Package_2001,
				view.getElement() != null ? view.getElement() : view,
				TUMLVisualIDRegistry.getType(PackageNameEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			TUMLDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5008); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getForkNode_3025Text(View view) {
		return ""; //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	private String getObjectNode_3031Text(View view) {
		IParser parser = TUMLParserProvider.getParser(
				TUMLElementTypes.ObjectNode_3031,
				view.getElement() != null ? view.getElement() : view,
				TUMLVisualIDRegistry.getType(ObjectNodeNameEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			TUMLDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5025); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getAssignAction_3032Text(View view) {
		IParser parser = TUMLParserProvider.getParser(
				TUMLElementTypes.AssignAction_3032,
				view.getElement() != null ? view.getElement() : view,
				TUMLVisualIDRegistry
						.getType(AssignActionNameEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			TUMLDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5026); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getEmptyAction_3033Text(View view) {
		EmptyAction domainModelElement = (EmptyAction) view.getElement();
		if (domainModelElement != null) {
			return domainModelElement.getName();
		} else {
			TUMLDiagramEditorPlugin.getInstance().logError(
					"No domain element for view with visualID = " + 3033); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getGeneralization_4002Text(View view) {
		return ""; //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	private String getActivityRelatedOperation_4007Text(View view) {
		return ""; //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	private String getSendAction_3028Text(View view) {
		IParser parser = TUMLParserProvider.getParser(
				TUMLElementTypes.SendAction_3028,
				view.getElement() != null ? view.getElement() : view,
				TUMLVisualIDRegistry.getType(SendActionNameEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			TUMLDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5022); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getDecisionNode_3021Text(View view) {
		return ""; //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	private String getVariationActivityFragment_2004Text(View view) {
		IParser parser = TUMLParserProvider
				.getParser(
						TUMLElementTypes.VariationActivityFragment_2004,
						view.getElement() != null ? view.getElement() : view,
						TUMLVisualIDRegistry
								.getType(VariationActivityFragmentNameEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			TUMLDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5020); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getAcceptAction_3029Text(View view) {
		IParser parser = TUMLParserProvider.getParser(
				TUMLElementTypes.AcceptAction_3029,
				view.getElement() != null ? view.getElement() : view,
				TUMLVisualIDRegistry
						.getType(AcceptActionNameEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			TUMLDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5023); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getEnumerationLiteral_3005Text(View view) {
		IParser parser = TUMLParserProvider.getParser(
				TUMLElementTypes.EnumerationLiteral_3005,
				view.getElement() != null ? view.getElement() : view,
				TUMLVisualIDRegistry
						.getType(EnumerationLiteralNameEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			TUMLDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5004); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getMergeNode_3022Text(View view) {
		return ""; //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	private String getInitialNode_3023Text(View view) {
		return ""; //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	private String getPrimitiveType_2003Text(View view) {
		IParser parser = TUMLParserProvider.getParser(
				TUMLElementTypes.PrimitiveType_2003,
				view.getElement() != null ? view.getElement() : view,
				TUMLVisualIDRegistry
						.getType(PrimitiveTypeNameEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			TUMLDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5010); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getActivity_2002Text(View view) {
		IParser parser = TUMLParserProvider.getParser(
				TUMLElementTypes.Activity_2002,
				view.getElement() != null ? view.getElement() : view,
				TUMLVisualIDRegistry.getType(ActivityNameEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			TUMLDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5009); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getCallActionMethod_4008Text(View view) {
		return ""; //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	private String getUnknownElementText(View view) {
		return "<UnknownElement Visual_ID = " + view.getType() + ">"; //$NON-NLS-1$  //$NON-NLS-2$
	}

	/**
	 * @generated
	 */
	private String getUnresolvedDomainElementProxyText(View view) {
		return "<Unresolved domain element Visual_ID = " + view.getType() + ">"; //$NON-NLS-1$  //$NON-NLS-2$
	}

	/**
	 * @generated
	 */
	public void init(ICommonContentExtensionSite aConfig) {
	}

	/**
	 * @generated
	 */
	public void restoreState(IMemento aMemento) {
	}

	/**
	 * @generated
	 */
	public void saveState(IMemento aMemento) {
	}

	/**
	 * @generated
	 */
	public String getDescription(Object anElement) {
		return null;
	}

	/**
	 * @generated
	 */
	private boolean isOwnView(View view) {
		return ModelEditPart.MODEL_ID.equals(TUMLVisualIDRegistry
				.getModelID(view));
	}

}
