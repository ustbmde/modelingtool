package edu.ustb.sei.mde.taileduml.diagram.edit.parts;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPartFactory;
import org.eclipse.gef.tools.CellEditorLocator;
import org.eclipse.gmf.runtime.diagram.ui.editparts.ITextAwareEditPart;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.gmf.tooling.runtime.directedit.locator.CellEditorLocatorAccess;

import edu.ustb.sei.mde.taileduml.diagram.part.TUMLVisualIDRegistry;

/**
 * @generated
 */
public class TUMLEditPartFactory implements EditPartFactory {

	/**
	 * @generated
	 */
	public EditPart createEditPart(EditPart context, Object model) {
		if (model instanceof View) {
			View view = (View) model;
			switch (TUMLVisualIDRegistry.getVisualID(view)) {

			case ModelEditPart.VISUAL_ID:
				return new ModelEditPart(view);

			case PackageEditPart.VISUAL_ID:
				return new PackageEditPart(view);

			case PackageNameEditPart.VISUAL_ID:
				return new PackageNameEditPart(view);

			case ActivityEditPart.VISUAL_ID:
				return new ActivityEditPart(view);

			case ActivityNameEditPart.VISUAL_ID:
				return new ActivityNameEditPart(view);

			case PrimitiveTypeEditPart.VISUAL_ID:
				return new PrimitiveTypeEditPart(view);

			case PrimitiveTypeNameEditPart.VISUAL_ID:
				return new PrimitiveTypeNameEditPart(view);

			case PrimitiveTypeInstanceClassEditPart.VISUAL_ID:
				return new PrimitiveTypeInstanceClassEditPart(view);

			case VariationActivityFragmentEditPart.VISUAL_ID:
				return new VariationActivityFragmentEditPart(view);

			case VariationActivityFragmentNameEditPart.VISUAL_ID:
				return new VariationActivityFragmentNameEditPart(view);

			case ClassEditPart.VISUAL_ID:
				return new ClassEditPart(view);

			case ClassNameEditPart.VISUAL_ID:
				return new ClassNameEditPart(view);

			case PropertyEditPart.VISUAL_ID:
				return new PropertyEditPart(view);

			case PropertyNameEditPart.VISUAL_ID:
				return new PropertyNameEditPart(view);

			case OperationEditPart.VISUAL_ID:
				return new OperationEditPart(view);

			case OperationNameEditPart.VISUAL_ID:
				return new OperationNameEditPart(view);

			case EnumerationEditPart.VISUAL_ID:
				return new EnumerationEditPart(view);

			case EnumerationNameEditPart.VISUAL_ID:
				return new EnumerationNameEditPart(view);

			case EnumerationLiteralEditPart.VISUAL_ID:
				return new EnumerationLiteralEditPart(view);

			case EnumerationLiteralNameEditPart.VISUAL_ID:
				return new EnumerationLiteralNameEditPart(view);

			case InterfaceEditPart.VISUAL_ID:
				return new InterfaceEditPart(view);

			case InterfaceNameEditPart.VISUAL_ID:
				return new InterfaceNameEditPart(view);

			case Operation2EditPart.VISUAL_ID:
				return new Operation2EditPart(view);

			case OperationName2EditPart.VISUAL_ID:
				return new OperationName2EditPart(view);

			case VariationPointEditPart.VISUAL_ID:
				return new VariationPointEditPart(view);

			case VariationPointNameEditPart.VISUAL_ID:
				return new VariationPointNameEditPart(view);

			case VariationEditPart.VISUAL_ID:
				return new VariationEditPart(view);

			case VariationNameEditPart.VISUAL_ID:
				return new VariationNameEditPart(view);

			case DecisionNodeEditPart.VISUAL_ID:
				return new DecisionNodeEditPart(view);

			case MergeNodeEditPart.VISUAL_ID:
				return new MergeNodeEditPart(view);

			case InitialNodeEditPart.VISUAL_ID:
				return new InitialNodeEditPart(view);

			case ActivityFinalNodeEditPart.VISUAL_ID:
				return new ActivityFinalNodeEditPart(view);

			case ForkNodeEditPart.VISUAL_ID:
				return new ForkNodeEditPart(view);

			case JoinNodeEditPart.VISUAL_ID:
				return new JoinNodeEditPart(view);

			case CallActionEditPart.VISUAL_ID:
				return new CallActionEditPart(view);

			case CallActionNameEditPart.VISUAL_ID:
				return new CallActionNameEditPart(view);

			case SendActionEditPart.VISUAL_ID:
				return new SendActionEditPart(view);

			case SendActionNameEditPart.VISUAL_ID:
				return new SendActionNameEditPart(view);

			case AcceptActionEditPart.VISUAL_ID:
				return new AcceptActionEditPart(view);

			case AcceptActionNameEditPart.VISUAL_ID:
				return new AcceptActionNameEditPart(view);

			case VxActionEditPart.VISUAL_ID:
				return new VxActionEditPart(view);

			case VxActionNameEditPart.VISUAL_ID:
				return new VxActionNameEditPart(view);

			case ObjectNodeEditPart.VISUAL_ID:
				return new ObjectNodeEditPart(view);

			case ObjectNodeNameEditPart.VISUAL_ID:
				return new ObjectNodeNameEditPart(view);

			case AssignActionEditPart.VISUAL_ID:
				return new AssignActionEditPart(view);

			case AssignActionNameEditPart.VISUAL_ID:
				return new AssignActionNameEditPart(view);

			case EmptyActionEditPart.VISUAL_ID:
				return new EmptyActionEditPart(view);

			case PackagePackageCompartmentEditPart.VISUAL_ID:
				return new PackagePackageCompartmentEditPart(view);

			case ClassClassAttributeCompartmentEditPart.VISUAL_ID:
				return new ClassClassAttributeCompartmentEditPart(view);

			case ClassClassOperationCompartmentEditPart.VISUAL_ID:
				return new ClassClassOperationCompartmentEditPart(view);

			case EnumerationEnumerationCompartmentEditPart.VISUAL_ID:
				return new EnumerationEnumerationCompartmentEditPart(view);

			case InterfaceInterfaceOperationCompartmentEditPart.VISUAL_ID:
				return new InterfaceInterfaceOperationCompartmentEditPart(view);

			case ActivityActivityCompartmentEditPart.VISUAL_ID:
				return new ActivityActivityCompartmentEditPart(view);

			case VariationActivityFragmentVariationActivityFragmentCompartmentEditPart.VISUAL_ID:
				return new VariationActivityFragmentVariationActivityFragmentCompartmentEditPart(
						view);

			case RealizationEditPart.VISUAL_ID:
				return new RealizationEditPart(view);

			case GeneralizationEditPart.VISUAL_ID:
				return new GeneralizationEditPart(view);

			case ReferenceEditPart.VISUAL_ID:
				return new ReferenceEditPart(view);

			case ReferenceNameEditPart.VISUAL_ID:
				return new ReferenceNameEditPart(view);

			case ConstraintEditPart.VISUAL_ID:
				return new ConstraintEditPart(view);

			case ConstraintTypeEditPart.VISUAL_ID:
				return new ConstraintTypeEditPart(view);

			case ControlFlowEditPart.VISUAL_ID:
				return new ControlFlowEditPart(view);

			case ControlFlowConditionEditPart.VISUAL_ID:
				return new ControlFlowConditionEditPart(view);

			case ObjectFlowEditPart.VISUAL_ID:
				return new ObjectFlowEditPart(view);

			case ActivityRelatedOperationEditPart.VISUAL_ID:
				return new ActivityRelatedOperationEditPart(view);

			case CallActionMethodEditPart.VISUAL_ID:
				return new CallActionMethodEditPart(view);

			case VxActionVariantFragmentsEditPart.VISUAL_ID:
				return new VxActionVariantFragmentsEditPart(view);

			case VariationPointVariationsEditPart.VISUAL_ID:
				return new VariationPointVariationsEditPart(view);

			}
		}
		return createUnrecognizedEditPart(context, model);
	}

	/**
	 * @generated
	 */
	private EditPart createUnrecognizedEditPart(EditPart context, Object model) {
		// Handle creation of unrecognized child node EditParts here
		return null;
	}

	/**
	 * @generated
	 */
	public static CellEditorLocator getTextCellEditorLocator(
			ITextAwareEditPart source) {
		return CellEditorLocatorAccess.INSTANCE
				.getTextCellEditorLocator(source);
	}

}
