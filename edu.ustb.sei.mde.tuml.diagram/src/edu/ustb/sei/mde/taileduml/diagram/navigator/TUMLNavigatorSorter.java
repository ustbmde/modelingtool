package edu.ustb.sei.mde.taileduml.diagram.navigator;

import org.eclipse.jface.viewers.ViewerSorter;

import edu.ustb.sei.mde.taileduml.diagram.part.TUMLVisualIDRegistry;

/**
 * @generated
 */
public class TUMLNavigatorSorter extends ViewerSorter {

	/**
	 * @generated
	 */
	private static final int GROUP_CATEGORY = 7009;

	/**
	 * @generated
	 */
	public int category(Object element) {
		if (element instanceof TUMLNavigatorItem) {
			TUMLNavigatorItem item = (TUMLNavigatorItem) element;
			return TUMLVisualIDRegistry.getVisualID(item.getView());
		}
		return GROUP_CATEGORY;
	}

}
