package edu.ustb.sei.mde.taileduml.diagram.part;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.eclipse.gef.Tool;
import org.eclipse.gef.palette.MarqueeToolEntry;
import org.eclipse.gef.palette.PaletteContainer;
import org.eclipse.gef.palette.PaletteDrawer;
import org.eclipse.gef.palette.PaletteEntry;
import org.eclipse.gef.palette.PaletteGroup;
import org.eclipse.gef.palette.PaletteRoot;
import org.eclipse.gef.palette.PanningSelectionToolEntry;
import org.eclipse.gef.palette.ToolEntry;
import org.eclipse.gmf.runtime.diagram.ui.tools.UnspecifiedTypeConnectionTool;
import org.eclipse.gmf.runtime.diagram.ui.tools.UnspecifiedTypeCreationTool;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;

import edu.ustb.sei.mde.taileduml.diagram.providers.TUMLElementTypes;

/**
 * @generated
 */
public class TUMLPaletteFactory {

	/**
	 * @generated
	 */
	public void fillPalette(PaletteRoot paletteRoot) {
		cleanStandardTools(paletteRoot);
		paletteRoot.add(createStandardTool1Group());
		paletteRoot.add(createClassDiagram2Group());
		paletteRoot.add(createActivityDiagram3Group());
		paletteRoot.add(createVxActivity4Group());
		paletteRoot.add(createVariationModel5Group());
	}

	/**
	 * Workaround for https://bugs.eclipse.org/bugs/show_bug.cgi?id=159289
	 * @generated
	 */
	private void cleanStandardTools(PaletteRoot paletteRoot) {
		for (Iterator it = paletteRoot.getChildren().iterator(); it.hasNext();) {
			PaletteEntry entry = (PaletteEntry) it.next();
			if (!"standardGroup".equals(entry.getId())) { //$NON-NLS-1$
				continue;
			}
			for (Iterator it2 = ((PaletteContainer) entry).getChildren()
					.iterator(); it2.hasNext();) {
				PaletteEntry entry2 = (PaletteEntry) it2.next();
				if ("zoomTool".equals(entry2.getId())) { //$NON-NLS-1$
					it2.remove();
				} else if ("noteStack".equals(entry2.getId())) { //$NON-NLS-1$
					it2.remove();
				} else if ("selectionTool".equals(entry2.getId())) { //$NON-NLS-1$
					it2.remove();
				}
				if (paletteRoot.getDefaultEntry() == entry2) {
					paletteRoot.setDefaultEntry(null);
				}
			}
		}
	}

	/**
	 * Creates "Standard Tool" palette tool group
	 * @generated
	 */
	private PaletteContainer createStandardTool1Group() {
		PaletteGroup paletteContainer = new PaletteGroup(
				Messages.StandardTool1Group_title);
		paletteContainer.setId("createStandardTool1Group"); //$NON-NLS-1$
		paletteContainer.add(createSelectTool());
		paletteContainer.add(createMarqueeTool());
		return paletteContainer;
	}

	/**
	 * Creates "Class Diagram" palette tool group
	 * @generated
	 */
	private PaletteContainer createClassDiagram2Group() {
		PaletteDrawer paletteContainer = new PaletteDrawer(
				Messages.ClassDiagram2Group_title);
		paletteContainer.setId("createClassDiagram2Group"); //$NON-NLS-1$
		paletteContainer.add(createPackage1CreationTool());
		paletteContainer.add(createClass2CreationTool());
		paletteContainer.add(createInterface3CreationTool());
		paletteContainer.add(createProperty4CreationTool());
		paletteContainer.add(createOperation5CreationTool());
		paletteContainer.add(createParameter6CreationTool());
		paletteContainer.add(createGeneralization7CreationTool());
		paletteContainer.add(createRealization8CreationTool());
		paletteContainer.add(createReference9CreationTool());
		paletteContainer.add(createPrimitiveType10CreationTool());
		paletteContainer.add(createEnumeration11CreationTool());
		paletteContainer.add(createEnumerationLiteral12CreationTool());
		return paletteContainer;
	}

	/**
	 * Creates "Activity Diagram" palette tool group
	 * @generated
	 */
	private PaletteContainer createActivityDiagram3Group() {
		PaletteDrawer paletteContainer = new PaletteDrawer(
				Messages.ActivityDiagram3Group_title);
		paletteContainer.setId("createActivityDiagram3Group"); //$NON-NLS-1$
		paletteContainer.add(createActivity1CreationTool());
		paletteContainer.add(createActivityRelatedOperationLink2CreationTool());
		paletteContainer.add(createCallAction3CreationTool());
		paletteContainer.add(createAssignAction4CreationTool());
		paletteContainer.add(createEmptyAction5CreationTool());
		paletteContainer.add(createSendAction6CreationTool());
		paletteContainer.add(createAcceptAction7CreationTool());
		paletteContainer.add(createInitialNode8CreationTool());
		paletteContainer.add(createFinalNode9CreationTool());
		paletteContainer.add(createDecisionNode10CreationTool());
		paletteContainer.add(createMergeNode11CreationTool());
		paletteContainer.add(createForkNode12CreationTool());
		paletteContainer.add(createJoinNode13CreationTool());
		paletteContainer.add(createObjectNode14CreationTool());
		paletteContainer.add(createControlFlow15CreationTool());
		paletteContainer.add(createObjectFlow16CreationTool());
		paletteContainer.add(createCallOperationLink17CreationTool());
		return paletteContainer;
	}

	/**
	 * Creates "VxActivity" palette tool group
	 * @generated
	 */
	private PaletteContainer createVxActivity4Group() {
		PaletteDrawer paletteContainer = new PaletteDrawer(
				Messages.VxActivity4Group_title);
		paletteContainer.setId("createVxActivity4Group"); //$NON-NLS-1$
		paletteContainer.add(createVxAction1CreationTool());
		paletteContainer.add(createVariationActivityFragment2CreationTool());
		paletteContainer.add(createRelatedVariantFragment3CreationTool());
		return paletteContainer;
	}

	/**
	 * Creates "Variation Model" palette tool group
	 * @generated
	 */
	private PaletteContainer createVariationModel5Group() {
		PaletteDrawer paletteContainer = new PaletteDrawer(
				Messages.VariationModel5Group_title);
		paletteContainer.setId("createVariationModel5Group"); //$NON-NLS-1$
		paletteContainer.add(createVariationPoint1CreationTool());
		paletteContainer.add(createVariation2CreationTool());
		paletteContainer.add(createRelatedVariation3CreationTool());
		paletteContainer.add(createConstraint4CreationTool());
		return paletteContainer;
	}

	/**
	 * @generated
	 */
	private ToolEntry createSelectTool() {
		PanningSelectionToolEntry entry = new PanningSelectionToolEntry();
		entry.setId("createSelectTool"); //$NON-NLS-1$
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createMarqueeTool() {
		MarqueeToolEntry entry = new MarqueeToolEntry();
		entry.setId("createMarqueeTool"); //$NON-NLS-1$
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createPackage1CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				Messages.Package1CreationTool_title,
				Messages.Package1CreationTool_desc,
				Collections.singletonList(TUMLElementTypes.Package_2001));
		entry.setId("createPackage1CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(TUMLElementTypes
				.getImageDescriptor(TUMLElementTypes.Package_2001));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createClass2CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				Messages.Class2CreationTool_title,
				Messages.Class2CreationTool_desc,
				Collections.singletonList(TUMLElementTypes.Class_3001));
		entry.setId("createClass2CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(TUMLElementTypes
				.getImageDescriptor(TUMLElementTypes.Class_3001));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createInterface3CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				Messages.Interface3CreationTool_title,
				Messages.Interface3CreationTool_desc,
				Collections.singletonList(TUMLElementTypes.Interface_3006));
		entry.setId("createInterface3CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(TUMLElementTypes
				.getImageDescriptor(TUMLElementTypes.Interface_3006));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createProperty4CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				Messages.Property4CreationTool_title,
				Messages.Property4CreationTool_desc,
				Collections.singletonList(TUMLElementTypes.Property_3002));
		entry.setId("createProperty4CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(TUMLElementTypes
				.getImageDescriptor(TUMLElementTypes.Property_3002));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createOperation5CreationTool() {
		ArrayList<IElementType> types = new ArrayList<IElementType>(2);
		types.add(TUMLElementTypes.Operation_3003);
		types.add(TUMLElementTypes.Operation_3007);
		NodeToolEntry entry = new NodeToolEntry(
				Messages.Operation5CreationTool_title,
				Messages.Operation5CreationTool_desc, types);
		entry.setId("createOperation5CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(TUMLElementTypes
				.getImageDescriptor(TUMLElementTypes.Operation_3003));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createParameter6CreationTool() {
		ToolEntry entry = new ToolEntry(Messages.Parameter6CreationTool_title,
				Messages.Parameter6CreationTool_desc, null, null) {
		};
		entry.setId("createParameter6CreationTool"); //$NON-NLS-1$
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createGeneralization7CreationTool() {
		LinkToolEntry entry = new LinkToolEntry(
				Messages.Generalization7CreationTool_title,
				Messages.Generalization7CreationTool_desc,
				Collections.singletonList(TUMLElementTypes.Generalization_4002));
		entry.setId("createGeneralization7CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(TUMLElementTypes
				.getImageDescriptor(TUMLElementTypes.Generalization_4002));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createRealization8CreationTool() {
		LinkToolEntry entry = new LinkToolEntry(
				Messages.Realization8CreationTool_title,
				Messages.Realization8CreationTool_desc,
				Collections.singletonList(TUMLElementTypes.Realization_4001));
		entry.setId("createRealization8CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(TUMLElementTypes
				.getImageDescriptor(TUMLElementTypes.Realization_4001));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createReference9CreationTool() {
		LinkToolEntry entry = new LinkToolEntry(
				Messages.Reference9CreationTool_title,
				Messages.Reference9CreationTool_desc,
				Collections.singletonList(TUMLElementTypes.Reference_4003));
		entry.setId("createReference9CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(TUMLElementTypes
				.getImageDescriptor(TUMLElementTypes.Reference_4003));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createPrimitiveType10CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				Messages.PrimitiveType10CreationTool_title,
				Messages.PrimitiveType10CreationTool_desc,
				Collections.singletonList(TUMLElementTypes.PrimitiveType_2003));
		entry.setId("createPrimitiveType10CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(TUMLElementTypes
				.getImageDescriptor(TUMLElementTypes.PrimitiveType_2003));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createEnumeration11CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				Messages.Enumeration11CreationTool_title,
				Messages.Enumeration11CreationTool_desc,
				Collections.singletonList(TUMLElementTypes.Enumeration_3004));
		entry.setId("createEnumeration11CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(TUMLElementTypes
				.getImageDescriptor(TUMLElementTypes.Enumeration_3004));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createEnumerationLiteral12CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				Messages.EnumerationLiteral12CreationTool_title,
				Messages.EnumerationLiteral12CreationTool_desc,
				Collections
						.singletonList(TUMLElementTypes.EnumerationLiteral_3005));
		entry.setId("createEnumerationLiteral12CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(TUMLElementTypes
				.getImageDescriptor(TUMLElementTypes.EnumerationLiteral_3005));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createActivity1CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				Messages.Activity1CreationTool_title,
				Messages.Activity1CreationTool_desc,
				Collections.singletonList(TUMLElementTypes.Activity_2002));
		entry.setId("createActivity1CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(TUMLElementTypes
				.getImageDescriptor(TUMLElementTypes.Activity_2002));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createActivityRelatedOperationLink2CreationTool() {
		LinkToolEntry entry = new LinkToolEntry(
				Messages.ActivityRelatedOperationLink2CreationTool_title,
				Messages.ActivityRelatedOperationLink2CreationTool_desc,
				Collections
						.singletonList(TUMLElementTypes.ActivityRelatedOperation_4007));
		entry.setId("createActivityRelatedOperationLink2CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(TUMLElementTypes
				.getImageDescriptor(TUMLElementTypes.ActivityRelatedOperation_4007));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createCallAction3CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				Messages.CallAction3CreationTool_title,
				Messages.CallAction3CreationTool_desc,
				Collections.singletonList(TUMLElementTypes.CallAction_3027));
		entry.setId("createCallAction3CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(TUMLElementTypes
				.getImageDescriptor(TUMLElementTypes.CallAction_3027));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createAssignAction4CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				Messages.AssignAction4CreationTool_title,
				Messages.AssignAction4CreationTool_desc,
				Collections.singletonList(TUMLElementTypes.AssignAction_3032));
		entry.setId("createAssignAction4CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(TUMLElementTypes
				.getImageDescriptor(TUMLElementTypes.AssignAction_3032));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createEmptyAction5CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				Messages.EmptyAction5CreationTool_title,
				Messages.EmptyAction5CreationTool_desc,
				Collections.singletonList(TUMLElementTypes.EmptyAction_3033));
		entry.setId("createEmptyAction5CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(TUMLElementTypes
				.getImageDescriptor(TUMLElementTypes.EmptyAction_3033));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createSendAction6CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				Messages.SendAction6CreationTool_title,
				Messages.SendAction6CreationTool_desc,
				Collections.singletonList(TUMLElementTypes.SendAction_3028));
		entry.setId("createSendAction6CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(TUMLElementTypes
				.getImageDescriptor(TUMLElementTypes.SendAction_3028));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createAcceptAction7CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				Messages.AcceptAction7CreationTool_title,
				Messages.AcceptAction7CreationTool_desc,
				Collections.singletonList(TUMLElementTypes.AcceptAction_3029));
		entry.setId("createAcceptAction7CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(TUMLElementTypes
				.getImageDescriptor(TUMLElementTypes.AcceptAction_3029));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createInitialNode8CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				Messages.InitialNode8CreationTool_title,
				Messages.InitialNode8CreationTool_desc,
				Collections.singletonList(TUMLElementTypes.InitialNode_3023));
		entry.setId("createInitialNode8CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(TUMLElementTypes
				.getImageDescriptor(TUMLElementTypes.InitialNode_3023));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createFinalNode9CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				Messages.FinalNode9CreationTool_title,
				Messages.FinalNode9CreationTool_desc,
				Collections
						.singletonList(TUMLElementTypes.ActivityFinalNode_3024));
		entry.setId("createFinalNode9CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(TUMLElementTypes
				.getImageDescriptor(TUMLElementTypes.ActivityFinalNode_3024));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createDecisionNode10CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				Messages.DecisionNode10CreationTool_title,
				Messages.DecisionNode10CreationTool_desc,
				Collections.singletonList(TUMLElementTypes.DecisionNode_3021));
		entry.setId("createDecisionNode10CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(TUMLElementTypes
				.getImageDescriptor(TUMLElementTypes.DecisionNode_3021));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createMergeNode11CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				Messages.MergeNode11CreationTool_title,
				Messages.MergeNode11CreationTool_desc,
				Collections.singletonList(TUMLElementTypes.MergeNode_3022));
		entry.setId("createMergeNode11CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(TUMLElementTypes
				.getImageDescriptor(TUMLElementTypes.MergeNode_3022));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createForkNode12CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				Messages.ForkNode12CreationTool_title,
				Messages.ForkNode12CreationTool_desc,
				Collections.singletonList(TUMLElementTypes.ForkNode_3025));
		entry.setId("createForkNode12CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(TUMLElementTypes
				.getImageDescriptor(TUMLElementTypes.ForkNode_3025));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createJoinNode13CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				Messages.JoinNode13CreationTool_title,
				Messages.JoinNode13CreationTool_desc,
				Collections.singletonList(TUMLElementTypes.JoinNode_3026));
		entry.setId("createJoinNode13CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(TUMLElementTypes
				.getImageDescriptor(TUMLElementTypes.JoinNode_3026));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createObjectNode14CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				Messages.ObjectNode14CreationTool_title,
				Messages.ObjectNode14CreationTool_desc,
				Collections.singletonList(TUMLElementTypes.ObjectNode_3031));
		entry.setId("createObjectNode14CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(TUMLElementTypes
				.getImageDescriptor(TUMLElementTypes.ObjectNode_3031));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createControlFlow15CreationTool() {
		LinkToolEntry entry = new LinkToolEntry(
				Messages.ControlFlow15CreationTool_title,
				Messages.ControlFlow15CreationTool_desc,
				Collections.singletonList(TUMLElementTypes.ControlFlow_4005));
		entry.setId("createControlFlow15CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(TUMLElementTypes
				.getImageDescriptor(TUMLElementTypes.ControlFlow_4005));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createObjectFlow16CreationTool() {
		LinkToolEntry entry = new LinkToolEntry(
				Messages.ObjectFlow16CreationTool_title,
				Messages.ObjectFlow16CreationTool_desc,
				Collections.singletonList(TUMLElementTypes.ObjectFlow_4006));
		entry.setId("createObjectFlow16CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(TUMLElementTypes
				.getImageDescriptor(TUMLElementTypes.ObjectFlow_4006));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createCallOperationLink17CreationTool() {
		LinkToolEntry entry = new LinkToolEntry(
				Messages.CallOperationLink17CreationTool_title, null,
				Collections
						.singletonList(TUMLElementTypes.CallActionMethod_4008));
		entry.setId("createCallOperationLink17CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(TUMLElementTypes
				.getImageDescriptor(TUMLElementTypes.CallActionMethod_4008));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createVxAction1CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				Messages.VxAction1CreationTool_title,
				Messages.VxAction1CreationTool_desc,
				Collections.singletonList(TUMLElementTypes.VxAction_3030));
		entry.setId("createVxAction1CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(TUMLElementTypes
				.getImageDescriptor(TUMLElementTypes.VxAction_3030));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createVariationActivityFragment2CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				Messages.VariationActivityFragment2CreationTool_title,
				Messages.VariationActivityFragment2CreationTool_desc,
				Collections
						.singletonList(TUMLElementTypes.VariationActivityFragment_2004));
		entry.setId("createVariationActivityFragment2CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(TUMLElementTypes
				.getImageDescriptor(TUMLElementTypes.VariationActivityFragment_2004));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createRelatedVariantFragment3CreationTool() {
		LinkToolEntry entry = new LinkToolEntry(
				Messages.RelatedVariantFragment3CreationTool_title,
				Messages.RelatedVariantFragment3CreationTool_desc,
				Collections
						.singletonList(TUMLElementTypes.VxActionVariantFragments_4009));
		entry.setId("createRelatedVariantFragment3CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(TUMLElementTypes
				.getImageDescriptor(TUMLElementTypes.VxActionVariantFragments_4009));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createVariationPoint1CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				Messages.VariationPoint1CreationTool_title,
				Messages.VariationPoint1CreationTool_desc,
				Collections.singletonList(TUMLElementTypes.VariationPoint_3015));
		entry.setId("createVariationPoint1CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(TUMLElementTypes
				.getImageDescriptor(TUMLElementTypes.VariationPoint_3015));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createVariation2CreationTool() {
		NodeToolEntry entry = new NodeToolEntry(
				Messages.Variation2CreationTool_title,
				Messages.Variation2CreationTool_desc,
				Collections.singletonList(TUMLElementTypes.Variation_3016));
		entry.setId("createVariation2CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(TUMLElementTypes
				.getImageDescriptor(TUMLElementTypes.Variation_3016));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createRelatedVariation3CreationTool() {
		LinkToolEntry entry = new LinkToolEntry(
				Messages.RelatedVariation3CreationTool_title,
				Messages.RelatedVariation3CreationTool_desc,
				Collections
						.singletonList(TUMLElementTypes.VariationPointVariations_4010));
		entry.setId("createRelatedVariation3CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(TUMLElementTypes
				.getImageDescriptor(TUMLElementTypes.VariationPointVariations_4010));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createConstraint4CreationTool() {
		LinkToolEntry entry = new LinkToolEntry(
				Messages.Constraint4CreationTool_title,
				Messages.Constraint4CreationTool_desc,
				Collections.singletonList(TUMLElementTypes.Constraint_4004));
		entry.setId("createConstraint4CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(TUMLElementTypes
				.getImageDescriptor(TUMLElementTypes.Constraint_4004));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private static class NodeToolEntry extends ToolEntry {

		/**
		 * @generated
		 */
		private final List<IElementType> elementTypes;

		/**
		 * @generated
		 */
		private NodeToolEntry(String title, String description,
				List<IElementType> elementTypes) {
			super(title, description, null, null);
			this.elementTypes = elementTypes;
		}

		/**
		 * @generated
		 */
		public Tool createTool() {
			Tool tool = new UnspecifiedTypeCreationTool(elementTypes);
			tool.setProperties(getToolProperties());
			return tool;
		}
	}

	/**
	 * @generated
	 */
	private static class LinkToolEntry extends ToolEntry {

		/**
		 * @generated
		 */
		private final List<IElementType> relationshipTypes;

		/**
		 * @generated
		 */
		private LinkToolEntry(String title, String description,
				List<IElementType> relationshipTypes) {
			super(title, description, null, null);
			this.relationshipTypes = relationshipTypes;
		}

		/**
		 * @generated
		 */
		public Tool createTool() {
			Tool tool = new UnspecifiedTypeConnectionTool(relationshipTypes);
			tool.setProperties(getToolProperties());
			return tool;
		}
	}
}
