package edu.ustb.sei.mde.taileduml.diagram.providers;

import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.ENamedElement;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.gmf.runtime.emf.type.core.ElementTypeRegistry;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.tooling.runtime.providers.DiagramElementTypeImages;
import org.eclipse.gmf.tooling.runtime.providers.DiagramElementTypes;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.swt.graphics.Image;

import edu.ustb.sei.mde.taileduml.TailedUMLPackage;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.AcceptActionEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.ActivityEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.ActivityFinalNodeEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.ActivityRelatedOperationEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.AssignActionEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.CallActionEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.CallActionMethodEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.ClassEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.ConstraintEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.ControlFlowEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.DecisionNodeEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.EmptyActionEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.EnumerationEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.EnumerationLiteralEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.ForkNodeEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.GeneralizationEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.InitialNodeEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.InterfaceEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.JoinNodeEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.MergeNodeEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.ModelEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.ObjectFlowEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.ObjectNodeEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.Operation2EditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.OperationEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.PackageEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.PrimitiveTypeEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.PropertyEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.RealizationEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.ReferenceEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.SendActionEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.VariationActivityFragmentEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.VariationEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.VariationPointEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.VariationPointVariationsEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.VxActionEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.VxActionVariantFragmentsEditPart;
import edu.ustb.sei.mde.taileduml.diagram.part.TUMLDiagramEditorPlugin;
import edu.ustb.sei.mde.taileduml.vxactivity.VxActivityPackage;
import edu.ustb.sei.mde.taileduml.vxmodel.VxModelPackage;

/**
 * @generated
 */
public class TUMLElementTypes {

	/**
	 * @generated
	 */
	private TUMLElementTypes() {
	}

	/**
	 * @generated
	 */
	private static Map<IElementType, ENamedElement> elements;

	/**
	 * @generated
	 */
	private static DiagramElementTypeImages elementTypeImages = new DiagramElementTypeImages(
			TUMLDiagramEditorPlugin.getInstance()
					.getItemProvidersAdapterFactory());

	/**
	 * @generated
	 */
	private static Set<IElementType> KNOWN_ELEMENT_TYPES;

	/**
	 * @generated
	 */
	public static final IElementType Model_1000 = getElementType("edu.ustb.sei.mde.tuml.diagram.Model_1000"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType Package_2001 = getElementType("edu.ustb.sei.mde.tuml.diagram.Package_2001"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType Activity_2002 = getElementType("edu.ustb.sei.mde.tuml.diagram.Activity_2002"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType PrimitiveType_2003 = getElementType("edu.ustb.sei.mde.tuml.diagram.PrimitiveType_2003"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType VariationActivityFragment_2004 = getElementType("edu.ustb.sei.mde.tuml.diagram.VariationActivityFragment_2004"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType Class_3001 = getElementType("edu.ustb.sei.mde.tuml.diagram.Class_3001"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType Property_3002 = getElementType("edu.ustb.sei.mde.tuml.diagram.Property_3002"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType Operation_3003 = getElementType("edu.ustb.sei.mde.tuml.diagram.Operation_3003"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType Enumeration_3004 = getElementType("edu.ustb.sei.mde.tuml.diagram.Enumeration_3004"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType EnumerationLiteral_3005 = getElementType("edu.ustb.sei.mde.tuml.diagram.EnumerationLiteral_3005"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType Interface_3006 = getElementType("edu.ustb.sei.mde.tuml.diagram.Interface_3006"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType Operation_3007 = getElementType("edu.ustb.sei.mde.tuml.diagram.Operation_3007"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType VariationPoint_3015 = getElementType("edu.ustb.sei.mde.tuml.diagram.VariationPoint_3015"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType Variation_3016 = getElementType("edu.ustb.sei.mde.tuml.diagram.Variation_3016"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType DecisionNode_3021 = getElementType("edu.ustb.sei.mde.tuml.diagram.DecisionNode_3021"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType MergeNode_3022 = getElementType("edu.ustb.sei.mde.tuml.diagram.MergeNode_3022"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType InitialNode_3023 = getElementType("edu.ustb.sei.mde.tuml.diagram.InitialNode_3023"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType ActivityFinalNode_3024 = getElementType("edu.ustb.sei.mde.tuml.diagram.ActivityFinalNode_3024"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType ForkNode_3025 = getElementType("edu.ustb.sei.mde.tuml.diagram.ForkNode_3025"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType JoinNode_3026 = getElementType("edu.ustb.sei.mde.tuml.diagram.JoinNode_3026"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType CallAction_3027 = getElementType("edu.ustb.sei.mde.tuml.diagram.CallAction_3027"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType SendAction_3028 = getElementType("edu.ustb.sei.mde.tuml.diagram.SendAction_3028"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType AcceptAction_3029 = getElementType("edu.ustb.sei.mde.tuml.diagram.AcceptAction_3029"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType VxAction_3030 = getElementType("edu.ustb.sei.mde.tuml.diagram.VxAction_3030"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType ObjectNode_3031 = getElementType("edu.ustb.sei.mde.tuml.diagram.ObjectNode_3031"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType AssignAction_3032 = getElementType("edu.ustb.sei.mde.tuml.diagram.AssignAction_3032"); //$NON-NLS-1$

	/**
	 * @generated
	 */
	public static final IElementType EmptyAction_3033 = getElementType("edu.ustb.sei.mde.tuml.diagram.EmptyAction_3033"); //$NON-NLS-1$

	/**
	 * @generated
	 */
	public static final IElementType Realization_4001 = getElementType("edu.ustb.sei.mde.tuml.diagram.Realization_4001"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType Generalization_4002 = getElementType("edu.ustb.sei.mde.tuml.diagram.Generalization_4002"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType Reference_4003 = getElementType("edu.ustb.sei.mde.tuml.diagram.Reference_4003"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType Constraint_4004 = getElementType("edu.ustb.sei.mde.tuml.diagram.Constraint_4004"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType ControlFlow_4005 = getElementType("edu.ustb.sei.mde.tuml.diagram.ControlFlow_4005"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType ObjectFlow_4006 = getElementType("edu.ustb.sei.mde.tuml.diagram.ObjectFlow_4006"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType ActivityRelatedOperation_4007 = getElementType("edu.ustb.sei.mde.tuml.diagram.ActivityRelatedOperation_4007"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType CallActionMethod_4008 = getElementType("edu.ustb.sei.mde.tuml.diagram.CallActionMethod_4008"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType VxActionVariantFragments_4009 = getElementType("edu.ustb.sei.mde.tuml.diagram.VxActionVariantFragments_4009"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType VariationPointVariations_4010 = getElementType("edu.ustb.sei.mde.tuml.diagram.VariationPointVariations_4010"); //$NON-NLS-1$

	/**
	 * @generated
	 */
	public static ImageDescriptor getImageDescriptor(ENamedElement element) {
		return elementTypeImages.getImageDescriptor(element);
	}

	/**
	 * @generated
	 */
	public static Image getImage(ENamedElement element) {
		return elementTypeImages.getImage(element);
	}

	/**
	 * @generated
	 */
	public static ImageDescriptor getImageDescriptor(IAdaptable hint) {
		return getImageDescriptor(getElement(hint));
	}

	/**
	 * @generated
	 */
	public static Image getImage(IAdaptable hint) {
		return getImage(getElement(hint));
	}

	/**
	 * Returns 'type' of the ecore object associated with the hint.
	 * 
	 * @generated
	 */
	public static ENamedElement getElement(IAdaptable hint) {
		Object type = hint.getAdapter(IElementType.class);
		if (elements == null) {
			elements = new IdentityHashMap<IElementType, ENamedElement>();

			elements.put(Model_1000, TailedUMLPackage.eINSTANCE.getModel());

			elements.put(Package_2001, TailedUMLPackage.eINSTANCE.getPackage());

			elements.put(Activity_2002,
					TailedUMLPackage.eINSTANCE.getActivity());

			elements.put(PrimitiveType_2003,
					TailedUMLPackage.eINSTANCE.getPrimitiveType());

			elements.put(VariationActivityFragment_2004,
					VxActivityPackage.eINSTANCE.getVariationActivityFragment());

			elements.put(Class_3001, TailedUMLPackage.eINSTANCE.getClass_());

			elements.put(Property_3002,
					TailedUMLPackage.eINSTANCE.getProperty());

			elements.put(Operation_3003,
					TailedUMLPackage.eINSTANCE.getOperation());

			elements.put(Enumeration_3004,
					TailedUMLPackage.eINSTANCE.getEnumeration());

			elements.put(EnumerationLiteral_3005,
					TailedUMLPackage.eINSTANCE.getEnumerationLiteral());

			elements.put(Interface_3006,
					TailedUMLPackage.eINSTANCE.getInterface());

			elements.put(Operation_3007,
					TailedUMLPackage.eINSTANCE.getOperation());

			elements.put(VariationPoint_3015,
					VxModelPackage.eINSTANCE.getVariationPoint());

			elements.put(Variation_3016,
					VxModelPackage.eINSTANCE.getVariation());

			elements.put(DecisionNode_3021,
					TailedUMLPackage.eINSTANCE.getDecisionNode());

			elements.put(MergeNode_3022,
					TailedUMLPackage.eINSTANCE.getMergeNode());

			elements.put(InitialNode_3023,
					TailedUMLPackage.eINSTANCE.getInitialNode());

			elements.put(ActivityFinalNode_3024,
					TailedUMLPackage.eINSTANCE.getActivityFinalNode());

			elements.put(ForkNode_3025,
					TailedUMLPackage.eINSTANCE.getForkNode());

			elements.put(JoinNode_3026,
					TailedUMLPackage.eINSTANCE.getJoinNode());

			elements.put(CallAction_3027,
					TailedUMLPackage.eINSTANCE.getCallAction());

			elements.put(SendAction_3028,
					TailedUMLPackage.eINSTANCE.getSendAction());

			elements.put(AcceptAction_3029,
					TailedUMLPackage.eINSTANCE.getAcceptAction());

			elements.put(VxAction_3030,
					VxActivityPackage.eINSTANCE.getVxAction());

			elements.put(ObjectNode_3031,
					TailedUMLPackage.eINSTANCE.getObjectNode());

			elements.put(AssignAction_3032,
					TailedUMLPackage.eINSTANCE.getAssignAction());

			elements.put(EmptyAction_3033,
					TailedUMLPackage.eINSTANCE.getEmptyAction());

			elements.put(Realization_4001,
					TailedUMLPackage.eINSTANCE.getRealization());

			elements.put(Generalization_4002,
					TailedUMLPackage.eINSTANCE.getGeneralization());

			elements.put(Reference_4003,
					TailedUMLPackage.eINSTANCE.getReference());

			elements.put(Constraint_4004,
					VxModelPackage.eINSTANCE.getConstraint());

			elements.put(ControlFlow_4005,
					TailedUMLPackage.eINSTANCE.getControlFlow());

			elements.put(ObjectFlow_4006,
					TailedUMLPackage.eINSTANCE.getObjectFlow());

			elements.put(ActivityRelatedOperation_4007,
					TailedUMLPackage.eINSTANCE.getActivity_RelatedOperation());

			elements.put(CallActionMethod_4008,
					TailedUMLPackage.eINSTANCE.getCallAction_Method());

			elements.put(VxActionVariantFragments_4009,
					VxActivityPackage.eINSTANCE.getVxAction_VariantFragments());

			elements.put(VariationPointVariations_4010,
					VxModelPackage.eINSTANCE.getVariationPoint_Variations());
		}
		return (ENamedElement) elements.get(type);
	}

	/**
	 * @generated
	 */
	private static IElementType getElementType(String id) {
		return ElementTypeRegistry.getInstance().getType(id);
	}

	/**
	 * @generated
	 */
	public static boolean isKnownElementType(IElementType elementType) {
		if (KNOWN_ELEMENT_TYPES == null) {
			KNOWN_ELEMENT_TYPES = new HashSet<IElementType>();
			KNOWN_ELEMENT_TYPES.add(Model_1000);
			KNOWN_ELEMENT_TYPES.add(Package_2001);
			KNOWN_ELEMENT_TYPES.add(Activity_2002);
			KNOWN_ELEMENT_TYPES.add(PrimitiveType_2003);
			KNOWN_ELEMENT_TYPES.add(VariationActivityFragment_2004);
			KNOWN_ELEMENT_TYPES.add(Class_3001);
			KNOWN_ELEMENT_TYPES.add(Property_3002);
			KNOWN_ELEMENT_TYPES.add(Operation_3003);
			KNOWN_ELEMENT_TYPES.add(Enumeration_3004);
			KNOWN_ELEMENT_TYPES.add(EnumerationLiteral_3005);
			KNOWN_ELEMENT_TYPES.add(Interface_3006);
			KNOWN_ELEMENT_TYPES.add(Operation_3007);
			KNOWN_ELEMENT_TYPES.add(VariationPoint_3015);
			KNOWN_ELEMENT_TYPES.add(Variation_3016);
			KNOWN_ELEMENT_TYPES.add(DecisionNode_3021);
			KNOWN_ELEMENT_TYPES.add(MergeNode_3022);
			KNOWN_ELEMENT_TYPES.add(InitialNode_3023);
			KNOWN_ELEMENT_TYPES.add(ActivityFinalNode_3024);
			KNOWN_ELEMENT_TYPES.add(ForkNode_3025);
			KNOWN_ELEMENT_TYPES.add(JoinNode_3026);
			KNOWN_ELEMENT_TYPES.add(CallAction_3027);
			KNOWN_ELEMENT_TYPES.add(SendAction_3028);
			KNOWN_ELEMENT_TYPES.add(AcceptAction_3029);
			KNOWN_ELEMENT_TYPES.add(VxAction_3030);
			KNOWN_ELEMENT_TYPES.add(ObjectNode_3031);
			KNOWN_ELEMENT_TYPES.add(AssignAction_3032);
			KNOWN_ELEMENT_TYPES.add(EmptyAction_3033);
			KNOWN_ELEMENT_TYPES.add(Realization_4001);
			KNOWN_ELEMENT_TYPES.add(Generalization_4002);
			KNOWN_ELEMENT_TYPES.add(Reference_4003);
			KNOWN_ELEMENT_TYPES.add(Constraint_4004);
			KNOWN_ELEMENT_TYPES.add(ControlFlow_4005);
			KNOWN_ELEMENT_TYPES.add(ObjectFlow_4006);
			KNOWN_ELEMENT_TYPES.add(ActivityRelatedOperation_4007);
			KNOWN_ELEMENT_TYPES.add(CallActionMethod_4008);
			KNOWN_ELEMENT_TYPES.add(VxActionVariantFragments_4009);
			KNOWN_ELEMENT_TYPES.add(VariationPointVariations_4010);
		}
		return KNOWN_ELEMENT_TYPES.contains(elementType);
	}

	/**
	 * @generated
	 */
	public static IElementType getElementType(int visualID) {
		switch (visualID) {
		case ModelEditPart.VISUAL_ID:
			return Model_1000;
		case PackageEditPart.VISUAL_ID:
			return Package_2001;
		case ActivityEditPart.VISUAL_ID:
			return Activity_2002;
		case PrimitiveTypeEditPart.VISUAL_ID:
			return PrimitiveType_2003;
		case VariationActivityFragmentEditPart.VISUAL_ID:
			return VariationActivityFragment_2004;
		case ClassEditPart.VISUAL_ID:
			return Class_3001;
		case PropertyEditPart.VISUAL_ID:
			return Property_3002;
		case OperationEditPart.VISUAL_ID:
			return Operation_3003;
		case EnumerationEditPart.VISUAL_ID:
			return Enumeration_3004;
		case EnumerationLiteralEditPart.VISUAL_ID:
			return EnumerationLiteral_3005;
		case InterfaceEditPart.VISUAL_ID:
			return Interface_3006;
		case Operation2EditPart.VISUAL_ID:
			return Operation_3007;
		case VariationPointEditPart.VISUAL_ID:
			return VariationPoint_3015;
		case VariationEditPart.VISUAL_ID:
			return Variation_3016;
		case DecisionNodeEditPart.VISUAL_ID:
			return DecisionNode_3021;
		case MergeNodeEditPart.VISUAL_ID:
			return MergeNode_3022;
		case InitialNodeEditPart.VISUAL_ID:
			return InitialNode_3023;
		case ActivityFinalNodeEditPart.VISUAL_ID:
			return ActivityFinalNode_3024;
		case ForkNodeEditPart.VISUAL_ID:
			return ForkNode_3025;
		case JoinNodeEditPart.VISUAL_ID:
			return JoinNode_3026;
		case CallActionEditPart.VISUAL_ID:
			return CallAction_3027;
		case SendActionEditPart.VISUAL_ID:
			return SendAction_3028;
		case AcceptActionEditPart.VISUAL_ID:
			return AcceptAction_3029;
		case VxActionEditPart.VISUAL_ID:
			return VxAction_3030;
		case ObjectNodeEditPart.VISUAL_ID:
			return ObjectNode_3031;
		case AssignActionEditPart.VISUAL_ID:
			return AssignAction_3032;
		case EmptyActionEditPart.VISUAL_ID:
			return EmptyAction_3033;
		case RealizationEditPart.VISUAL_ID:
			return Realization_4001;
		case GeneralizationEditPart.VISUAL_ID:
			return Generalization_4002;
		case ReferenceEditPart.VISUAL_ID:
			return Reference_4003;
		case ConstraintEditPart.VISUAL_ID:
			return Constraint_4004;
		case ControlFlowEditPart.VISUAL_ID:
			return ControlFlow_4005;
		case ObjectFlowEditPart.VISUAL_ID:
			return ObjectFlow_4006;
		case ActivityRelatedOperationEditPart.VISUAL_ID:
			return ActivityRelatedOperation_4007;
		case CallActionMethodEditPart.VISUAL_ID:
			return CallActionMethod_4008;
		case VxActionVariantFragmentsEditPart.VISUAL_ID:
			return VxActionVariantFragments_4009;
		case VariationPointVariationsEditPart.VISUAL_ID:
			return VariationPointVariations_4010;
		}
		return null;
	}

	/**
	 * @generated
	 */
	public static final DiagramElementTypes TYPED_INSTANCE = new DiagramElementTypes(
			elementTypeImages) {

		/**
		 * @generated
		 */
		@Override
		public boolean isKnownElementType(IElementType elementType) {
			return edu.ustb.sei.mde.taileduml.diagram.providers.TUMLElementTypes
					.isKnownElementType(elementType);
		}

		/**
		 * @generated
		 */
		@Override
		public IElementType getElementTypeForVisualId(int visualID) {
			return edu.ustb.sei.mde.taileduml.diagram.providers.TUMLElementTypes
					.getElementType(visualID);
		}

		/**
		 * @generated
		 */
		@Override
		public ENamedElement getDefiningNamedElement(
				IAdaptable elementTypeAdapter) {
			return edu.ustb.sei.mde.taileduml.diagram.providers.TUMLElementTypes
					.getElement(elementTypeAdapter);
		}
	};

}
