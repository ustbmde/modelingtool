package edu.ustb.sei.mde.taileduml.diagram.edit.policies;

import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;

import edu.ustb.sei.mde.taileduml.diagram.edit.commands.AcceptActionCreateCommand;
import edu.ustb.sei.mde.taileduml.diagram.edit.commands.ActivityFinalNodeCreateCommand;
import edu.ustb.sei.mde.taileduml.diagram.edit.commands.AssignActionCreateCommand;
import edu.ustb.sei.mde.taileduml.diagram.edit.commands.CallActionCreateCommand;
import edu.ustb.sei.mde.taileduml.diagram.edit.commands.DecisionNodeCreateCommand;
import edu.ustb.sei.mde.taileduml.diagram.edit.commands.EmptyActionCreateCommand;
import edu.ustb.sei.mde.taileduml.diagram.edit.commands.ForkNodeCreateCommand;
import edu.ustb.sei.mde.taileduml.diagram.edit.commands.InitialNodeCreateCommand;
import edu.ustb.sei.mde.taileduml.diagram.edit.commands.JoinNodeCreateCommand;
import edu.ustb.sei.mde.taileduml.diagram.edit.commands.MergeNodeCreateCommand;
import edu.ustb.sei.mde.taileduml.diagram.edit.commands.ObjectNodeCreateCommand;
import edu.ustb.sei.mde.taileduml.diagram.edit.commands.SendActionCreateCommand;
import edu.ustb.sei.mde.taileduml.diagram.edit.commands.VxActionCreateCommand;
import edu.ustb.sei.mde.taileduml.diagram.providers.TUMLElementTypes;

/**
 * @generated
 */
public class VariationActivityFragmentVariationActivityFragmentCompartmentItemSemanticEditPolicy
		extends TUMLBaseItemSemanticEditPolicy {

	/**
	 * @generated
	 */
	public VariationActivityFragmentVariationActivityFragmentCompartmentItemSemanticEditPolicy() {
		super(TUMLElementTypes.VariationActivityFragment_2004);
	}

	/**
	 * @generated
	 */
	protected Command getCreateCommand(CreateElementRequest req) {
		if (TUMLElementTypes.InitialNode_3023 == req.getElementType()) {
			return getGEFWrapper(new InitialNodeCreateCommand(req));
		}
		if (TUMLElementTypes.ActivityFinalNode_3024 == req.getElementType()) {
			return getGEFWrapper(new ActivityFinalNodeCreateCommand(req));
		}
		if (TUMLElementTypes.DecisionNode_3021 == req.getElementType()) {
			return getGEFWrapper(new DecisionNodeCreateCommand(req));
		}
		if (TUMLElementTypes.MergeNode_3022 == req.getElementType()) {
			return getGEFWrapper(new MergeNodeCreateCommand(req));
		}
		if (TUMLElementTypes.ForkNode_3025 == req.getElementType()) {
			return getGEFWrapper(new ForkNodeCreateCommand(req));
		}
		if (TUMLElementTypes.JoinNode_3026 == req.getElementType()) {
			return getGEFWrapper(new JoinNodeCreateCommand(req));
		}
		if (TUMLElementTypes.VxAction_3030 == req.getElementType()) {
			return getGEFWrapper(new VxActionCreateCommand(req));
		}
		if (TUMLElementTypes.SendAction_3028 == req.getElementType()) {
			return getGEFWrapper(new SendActionCreateCommand(req));
		}
		if (TUMLElementTypes.CallAction_3027 == req.getElementType()) {
			return getGEFWrapper(new CallActionCreateCommand(req));
		}
		if (TUMLElementTypes.AcceptAction_3029 == req.getElementType()) {
			return getGEFWrapper(new AcceptActionCreateCommand(req));
		}
		if (TUMLElementTypes.ObjectNode_3031 == req.getElementType()) {
			return getGEFWrapper(new ObjectNodeCreateCommand(req));
		}
		if (TUMLElementTypes.AssignAction_3032 == req.getElementType()) {
			return getGEFWrapper(new AssignActionCreateCommand(req));
		}
		if (TUMLElementTypes.EmptyAction_3033 == req.getElementType()) {
			return getGEFWrapper(new EmptyActionCreateCommand(req));
		}
		return super.getCreateCommand(req);
	}

}
