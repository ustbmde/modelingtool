package edu.ustb.sei.mde.taileduml.diagram.providers;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.gmf.runtime.common.core.service.AbstractProvider;
import org.eclipse.gmf.runtime.common.core.service.IOperation;
import org.eclipse.gmf.runtime.common.ui.services.icon.GetIconOperation;
import org.eclipse.gmf.runtime.common.ui.services.icon.IIconProvider;
import org.eclipse.gmf.tooling.runtime.providers.DefaultElementTypeIconProvider;
import org.eclipse.swt.graphics.Image;

/**
 * @generated
 */
public class TUMLIconProvider extends DefaultElementTypeIconProvider implements
		IIconProvider {

	/**
	 * @generated
	 */
	public TUMLIconProvider() {
		super(TUMLElementTypes.TYPED_INSTANCE);
	}
}
