package edu.ustb.sei.mde.taileduml.diagram.part;

import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.gmf.tooling.runtime.structure.DiagramStructure;

import edu.ustb.sei.mde.taileduml.Model;
import edu.ustb.sei.mde.taileduml.TailedUMLPackage;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.AcceptActionEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.AcceptActionNameEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.ActivityActivityCompartmentEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.ActivityEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.ActivityFinalNodeEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.ActivityNameEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.AssignActionEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.AssignActionNameEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.CallActionEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.CallActionNameEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.ClassClassAttributeCompartmentEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.ClassClassOperationCompartmentEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.ClassEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.ClassNameEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.ConstraintEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.ConstraintTypeEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.ControlFlowConditionEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.ControlFlowEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.DecisionNodeEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.EmptyActionEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.EnumerationEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.EnumerationEnumerationCompartmentEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.EnumerationLiteralEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.EnumerationLiteralNameEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.EnumerationNameEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.ForkNodeEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.GeneralizationEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.InitialNodeEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.InterfaceEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.InterfaceInterfaceOperationCompartmentEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.InterfaceNameEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.JoinNodeEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.MergeNodeEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.ModelEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.ObjectFlowEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.ObjectNodeEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.ObjectNodeNameEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.Operation2EditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.OperationEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.OperationName2EditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.OperationNameEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.PackageEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.PackageNameEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.PackagePackageCompartmentEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.PrimitiveTypeEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.PrimitiveTypeInstanceClassEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.PrimitiveTypeNameEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.PropertyEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.PropertyNameEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.RealizationEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.ReferenceEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.ReferenceNameEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.SendActionEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.SendActionNameEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.VariationActivityFragmentEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.VariationActivityFragmentNameEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.VariationActivityFragmentVariationActivityFragmentCompartmentEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.VariationEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.VariationNameEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.VariationPointEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.VariationPointNameEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.VxActionEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.VxActionNameEditPart;
import edu.ustb.sei.mde.taileduml.diagram.edit.parts.WrappingLabelEditPart;
import edu.ustb.sei.mde.taileduml.vxactivity.VxActivityPackage;
import edu.ustb.sei.mde.taileduml.vxmodel.VxModelPackage;

/**
 * This registry is used to determine which type of visual object should be
 * created for the corresponding Diagram, Node, ChildNode or Link represented
 * by a domain model object.
 * 
 * @generated
 */
public class TUMLVisualIDRegistry {

	/**
	 * @generated
	 */
	private static final String DEBUG_KEY = "edu.ustb.sei.mde.tuml.diagram/debug/visualID"; //$NON-NLS-1$

	/**
	 * @generated
	 */
	public static int getVisualID(View view) {
		if (view instanceof Diagram) {
			if (ModelEditPart.MODEL_ID.equals(view.getType())) {
				return ModelEditPart.VISUAL_ID;
			} else {
				return -1;
			}
		}
		return edu.ustb.sei.mde.taileduml.diagram.part.TUMLVisualIDRegistry
				.getVisualID(view.getType());
	}

	/**
	 * @generated
	 */
	public static String getModelID(View view) {
		View diagram = view.getDiagram();
		while (view != diagram) {
			EAnnotation annotation = view.getEAnnotation("Shortcut"); //$NON-NLS-1$
			if (annotation != null) {
				return (String) annotation.getDetails().get("modelID"); //$NON-NLS-1$
			}
			view = (View) view.eContainer();
		}
		return diagram != null ? diagram.getType() : null;
	}

	/**
	 * @generated
	 */
	public static int getVisualID(String type) {
		try {
			return Integer.parseInt(type);
		} catch (NumberFormatException e) {
			if (Boolean.TRUE.toString().equalsIgnoreCase(
					Platform.getDebugOption(DEBUG_KEY))) {
				TUMLDiagramEditorPlugin.getInstance().logError(
						"Unable to parse view type as a visualID number: "
								+ type);
			}
		}
		return -1;
	}

	/**
	 * @generated
	 */
	public static String getType(int visualID) {
		return Integer.toString(visualID);
	}

	/**
	 * @generated
	 */
	public static int getDiagramVisualID(EObject domainElement) {
		if (domainElement == null) {
			return -1;
		}
		if (TailedUMLPackage.eINSTANCE.getModel().isSuperTypeOf(
				domainElement.eClass())
				&& isDiagram((Model) domainElement)) {
			return ModelEditPart.VISUAL_ID;
		}
		return -1;
	}

	/**
	 * @generated
	 */
	public static int getNodeVisualID(View containerView, EObject domainElement) {
		if (domainElement == null) {
			return -1;
		}
		String containerModelID = edu.ustb.sei.mde.taileduml.diagram.part.TUMLVisualIDRegistry
				.getModelID(containerView);
		if (!ModelEditPart.MODEL_ID.equals(containerModelID)) {
			return -1;
		}
		int containerVisualID;
		if (ModelEditPart.MODEL_ID.equals(containerModelID)) {
			containerVisualID = edu.ustb.sei.mde.taileduml.diagram.part.TUMLVisualIDRegistry
					.getVisualID(containerView);
		} else {
			if (containerView instanceof Diagram) {
				containerVisualID = ModelEditPart.VISUAL_ID;
			} else {
				return -1;
			}
		}
		switch (containerVisualID) {
		case ModelEditPart.VISUAL_ID:
			if (TailedUMLPackage.eINSTANCE.getPackage().isSuperTypeOf(
					domainElement.eClass())) {
				return PackageEditPart.VISUAL_ID;
			}
			if (TailedUMLPackage.eINSTANCE.getActivity().isSuperTypeOf(
					domainElement.eClass())) {
				return ActivityEditPart.VISUAL_ID;
			}
			if (TailedUMLPackage.eINSTANCE.getPrimitiveType().isSuperTypeOf(
					domainElement.eClass())) {
				return PrimitiveTypeEditPart.VISUAL_ID;
			}
			if (VxActivityPackage.eINSTANCE.getVariationActivityFragment()
					.isSuperTypeOf(domainElement.eClass())) {
				return VariationActivityFragmentEditPart.VISUAL_ID;
			}
			break;
		case PackagePackageCompartmentEditPart.VISUAL_ID:
			if (TailedUMLPackage.eINSTANCE.getClass_().isSuperTypeOf(
					domainElement.eClass())) {
				return ClassEditPart.VISUAL_ID;
			}
			if (TailedUMLPackage.eINSTANCE.getEnumeration().isSuperTypeOf(
					domainElement.eClass())) {
				return EnumerationEditPart.VISUAL_ID;
			}
			if (TailedUMLPackage.eINSTANCE.getInterface().isSuperTypeOf(
					domainElement.eClass())) {
				return InterfaceEditPart.VISUAL_ID;
			}
			if (VxModelPackage.eINSTANCE.getVariationPoint().isSuperTypeOf(
					domainElement.eClass())) {
				return VariationPointEditPart.VISUAL_ID;
			}
			if (VxModelPackage.eINSTANCE.getVariation().isSuperTypeOf(
					domainElement.eClass())) {
				return VariationEditPart.VISUAL_ID;
			}
			break;
		case ClassClassAttributeCompartmentEditPart.VISUAL_ID:
			if (TailedUMLPackage.eINSTANCE.getProperty().isSuperTypeOf(
					domainElement.eClass())) {
				return PropertyEditPart.VISUAL_ID;
			}
			break;
		case ClassClassOperationCompartmentEditPart.VISUAL_ID:
			if (TailedUMLPackage.eINSTANCE.getOperation().isSuperTypeOf(
					domainElement.eClass())) {
				return OperationEditPart.VISUAL_ID;
			}
			break;
		case EnumerationEnumerationCompartmentEditPart.VISUAL_ID:
			if (TailedUMLPackage.eINSTANCE.getEnumerationLiteral()
					.isSuperTypeOf(domainElement.eClass())) {
				return EnumerationLiteralEditPart.VISUAL_ID;
			}
			break;
		case InterfaceInterfaceOperationCompartmentEditPart.VISUAL_ID:
			if (TailedUMLPackage.eINSTANCE.getOperation().isSuperTypeOf(
					domainElement.eClass())) {
				return Operation2EditPart.VISUAL_ID;
			}
			break;
		case ActivityActivityCompartmentEditPart.VISUAL_ID:
			if (TailedUMLPackage.eINSTANCE.getDecisionNode().isSuperTypeOf(
					domainElement.eClass())) {
				return DecisionNodeEditPart.VISUAL_ID;
			}
			if (TailedUMLPackage.eINSTANCE.getMergeNode().isSuperTypeOf(
					domainElement.eClass())) {
				return MergeNodeEditPart.VISUAL_ID;
			}
			if (TailedUMLPackage.eINSTANCE.getInitialNode().isSuperTypeOf(
					domainElement.eClass())) {
				return InitialNodeEditPart.VISUAL_ID;
			}
			if (TailedUMLPackage.eINSTANCE.getActivityFinalNode()
					.isSuperTypeOf(domainElement.eClass())) {
				return ActivityFinalNodeEditPart.VISUAL_ID;
			}
			if (TailedUMLPackage.eINSTANCE.getForkNode().isSuperTypeOf(
					domainElement.eClass())) {
				return ForkNodeEditPart.VISUAL_ID;
			}
			if (TailedUMLPackage.eINSTANCE.getJoinNode().isSuperTypeOf(
					domainElement.eClass())) {
				return JoinNodeEditPart.VISUAL_ID;
			}
			if (TailedUMLPackage.eINSTANCE.getCallAction().isSuperTypeOf(
					domainElement.eClass())) {
				return CallActionEditPart.VISUAL_ID;
			}
			if (TailedUMLPackage.eINSTANCE.getSendAction().isSuperTypeOf(
					domainElement.eClass())) {
				return SendActionEditPart.VISUAL_ID;
			}
			if (TailedUMLPackage.eINSTANCE.getAcceptAction().isSuperTypeOf(
					domainElement.eClass())) {
				return AcceptActionEditPart.VISUAL_ID;
			}
			if (VxActivityPackage.eINSTANCE.getVxAction().isSuperTypeOf(
					domainElement.eClass())) {
				return VxActionEditPart.VISUAL_ID;
			}
			if (TailedUMLPackage.eINSTANCE.getObjectNode().isSuperTypeOf(
					domainElement.eClass())) {
				return ObjectNodeEditPart.VISUAL_ID;
			}
			if (TailedUMLPackage.eINSTANCE.getAssignAction().isSuperTypeOf(
					domainElement.eClass())) {
				return AssignActionEditPart.VISUAL_ID;
			}
			if (TailedUMLPackage.eINSTANCE.getEmptyAction().isSuperTypeOf(
					domainElement.eClass())) {
				return EmptyActionEditPart.VISUAL_ID;
			}
			break;
		case VariationActivityFragmentVariationActivityFragmentCompartmentEditPart.VISUAL_ID:
			if (TailedUMLPackage.eINSTANCE.getInitialNode().isSuperTypeOf(
					domainElement.eClass())) {
				return InitialNodeEditPart.VISUAL_ID;
			}
			if (TailedUMLPackage.eINSTANCE.getActivityFinalNode()
					.isSuperTypeOf(domainElement.eClass())) {
				return ActivityFinalNodeEditPart.VISUAL_ID;
			}
			if (TailedUMLPackage.eINSTANCE.getDecisionNode().isSuperTypeOf(
					domainElement.eClass())) {
				return DecisionNodeEditPart.VISUAL_ID;
			}
			if (TailedUMLPackage.eINSTANCE.getMergeNode().isSuperTypeOf(
					domainElement.eClass())) {
				return MergeNodeEditPart.VISUAL_ID;
			}
			if (TailedUMLPackage.eINSTANCE.getForkNode().isSuperTypeOf(
					domainElement.eClass())) {
				return ForkNodeEditPart.VISUAL_ID;
			}
			if (TailedUMLPackage.eINSTANCE.getJoinNode().isSuperTypeOf(
					domainElement.eClass())) {
				return JoinNodeEditPart.VISUAL_ID;
			}
			if (VxActivityPackage.eINSTANCE.getVxAction().isSuperTypeOf(
					domainElement.eClass())) {
				return VxActionEditPart.VISUAL_ID;
			}
			if (TailedUMLPackage.eINSTANCE.getSendAction().isSuperTypeOf(
					domainElement.eClass())) {
				return SendActionEditPart.VISUAL_ID;
			}
			if (TailedUMLPackage.eINSTANCE.getCallAction().isSuperTypeOf(
					domainElement.eClass())) {
				return CallActionEditPart.VISUAL_ID;
			}
			if (TailedUMLPackage.eINSTANCE.getAcceptAction().isSuperTypeOf(
					domainElement.eClass())) {
				return AcceptActionEditPart.VISUAL_ID;
			}
			if (TailedUMLPackage.eINSTANCE.getObjectNode().isSuperTypeOf(
					domainElement.eClass())) {
				return ObjectNodeEditPart.VISUAL_ID;
			}
			if (TailedUMLPackage.eINSTANCE.getAssignAction().isSuperTypeOf(
					domainElement.eClass())) {
				return AssignActionEditPart.VISUAL_ID;
			}
			if (TailedUMLPackage.eINSTANCE.getEmptyAction().isSuperTypeOf(
					domainElement.eClass())) {
				return EmptyActionEditPart.VISUAL_ID;
			}
			break;
		}
		return -1;
	}

	/**
	 * @generated
	 */
	public static boolean canCreateNode(View containerView, int nodeVisualID) {
		String containerModelID = edu.ustb.sei.mde.taileduml.diagram.part.TUMLVisualIDRegistry
				.getModelID(containerView);
		if (!ModelEditPart.MODEL_ID.equals(containerModelID)) {
			return false;
		}
		int containerVisualID;
		if (ModelEditPart.MODEL_ID.equals(containerModelID)) {
			containerVisualID = edu.ustb.sei.mde.taileduml.diagram.part.TUMLVisualIDRegistry
					.getVisualID(containerView);
		} else {
			if (containerView instanceof Diagram) {
				containerVisualID = ModelEditPart.VISUAL_ID;
			} else {
				return false;
			}
		}
		switch (containerVisualID) {
		case ModelEditPart.VISUAL_ID:
			if (PackageEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (ActivityEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (PrimitiveTypeEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (VariationActivityFragmentEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case PackageEditPart.VISUAL_ID:
			if (PackageNameEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (PackagePackageCompartmentEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case ActivityEditPart.VISUAL_ID:
			if (ActivityNameEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (ActivityActivityCompartmentEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case PrimitiveTypeEditPart.VISUAL_ID:
			if (PrimitiveTypeNameEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (PrimitiveTypeInstanceClassEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case VariationActivityFragmentEditPart.VISUAL_ID:
			if (VariationActivityFragmentNameEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (VariationActivityFragmentVariationActivityFragmentCompartmentEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case ClassEditPart.VISUAL_ID:
			if (ClassNameEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (ClassClassAttributeCompartmentEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (ClassClassOperationCompartmentEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case PropertyEditPart.VISUAL_ID:
			if (PropertyNameEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case OperationEditPart.VISUAL_ID:
			if (OperationNameEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case EnumerationEditPart.VISUAL_ID:
			if (EnumerationNameEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (EnumerationEnumerationCompartmentEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case EnumerationLiteralEditPart.VISUAL_ID:
			if (EnumerationLiteralNameEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case InterfaceEditPart.VISUAL_ID:
			if (InterfaceNameEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (InterfaceInterfaceOperationCompartmentEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case Operation2EditPart.VISUAL_ID:
			if (OperationName2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case VariationPointEditPart.VISUAL_ID:
			if (VariationPointNameEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case VariationEditPart.VISUAL_ID:
			if (VariationNameEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case CallActionEditPart.VISUAL_ID:
			if (CallActionNameEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case SendActionEditPart.VISUAL_ID:
			if (SendActionNameEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case AcceptActionEditPart.VISUAL_ID:
			if (AcceptActionNameEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case VxActionEditPart.VISUAL_ID:
			if (VxActionNameEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case ObjectNodeEditPart.VISUAL_ID:
			if (ObjectNodeNameEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case AssignActionEditPart.VISUAL_ID:
			if (AssignActionNameEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case PackagePackageCompartmentEditPart.VISUAL_ID:
			if (ClassEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (EnumerationEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (InterfaceEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (VariationPointEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (VariationEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case ClassClassAttributeCompartmentEditPart.VISUAL_ID:
			if (PropertyEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case ClassClassOperationCompartmentEditPart.VISUAL_ID:
			if (OperationEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case EnumerationEnumerationCompartmentEditPart.VISUAL_ID:
			if (EnumerationLiteralEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case InterfaceInterfaceOperationCompartmentEditPart.VISUAL_ID:
			if (Operation2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case ActivityActivityCompartmentEditPart.VISUAL_ID:
			if (DecisionNodeEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (MergeNodeEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (InitialNodeEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (ActivityFinalNodeEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (ForkNodeEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (JoinNodeEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (CallActionEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (SendActionEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (AcceptActionEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (VxActionEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (ObjectNodeEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (AssignActionEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (EmptyActionEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case VariationActivityFragmentVariationActivityFragmentCompartmentEditPart.VISUAL_ID:
			if (InitialNodeEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (ActivityFinalNodeEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (DecisionNodeEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (MergeNodeEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (ForkNodeEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (JoinNodeEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (VxActionEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (SendActionEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (CallActionEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (AcceptActionEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (ObjectNodeEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (AssignActionEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (EmptyActionEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case ReferenceEditPart.VISUAL_ID:
			if (ReferenceNameEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case ConstraintEditPart.VISUAL_ID:
			if (ConstraintTypeEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case ControlFlowEditPart.VISUAL_ID:
			if (ControlFlowConditionEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		}
		return false;
	}

	/**
	 * @generated
	 */
	public static int getLinkWithClassVisualID(EObject domainElement) {
		if (domainElement == null) {
			return -1;
		}
		if (TailedUMLPackage.eINSTANCE.getRealization().isSuperTypeOf(
				domainElement.eClass())) {
			return RealizationEditPart.VISUAL_ID;
		}
		if (TailedUMLPackage.eINSTANCE.getGeneralization().isSuperTypeOf(
				domainElement.eClass())) {
			return GeneralizationEditPart.VISUAL_ID;
		}
		if (TailedUMLPackage.eINSTANCE.getReference().isSuperTypeOf(
				domainElement.eClass())) {
			return ReferenceEditPart.VISUAL_ID;
		}
		if (VxModelPackage.eINSTANCE.getConstraint().isSuperTypeOf(
				domainElement.eClass())) {
			return ConstraintEditPart.VISUAL_ID;
		}
		if (TailedUMLPackage.eINSTANCE.getControlFlow().isSuperTypeOf(
				domainElement.eClass())) {
			return ControlFlowEditPart.VISUAL_ID;
		}
		if (TailedUMLPackage.eINSTANCE.getObjectFlow().isSuperTypeOf(
				domainElement.eClass())) {
			return ObjectFlowEditPart.VISUAL_ID;
		}
		return -1;
	}

	/**
	 * User can change implementation of this method to handle some specific
	 * situations not covered by default logic.
	 * 
	 * @generated
	 */
	private static boolean isDiagram(Model element) {
		return true;
	}

	/**
	 * @generated
	 */
	public static boolean checkNodeVisualID(View containerView,
			EObject domainElement, int candidate) {
		if (candidate == -1) {
			//unrecognized id is always bad
			return false;
		}
		int basic = getNodeVisualID(containerView, domainElement);
		return basic == candidate;
	}

	/**
	 * @generated
	 */
	public static boolean isCompartmentVisualID(int visualID) {
		switch (visualID) {
		case PackagePackageCompartmentEditPart.VISUAL_ID:
		case ClassClassAttributeCompartmentEditPart.VISUAL_ID:
		case ClassClassOperationCompartmentEditPart.VISUAL_ID:
		case EnumerationEnumerationCompartmentEditPart.VISUAL_ID:
		case InterfaceInterfaceOperationCompartmentEditPart.VISUAL_ID:
		case ActivityActivityCompartmentEditPart.VISUAL_ID:
		case VariationActivityFragmentVariationActivityFragmentCompartmentEditPart.VISUAL_ID:
			return true;
		default:
			break;
		}
		return false;
	}

	/**
	 * @generated
	 */
	public static boolean isSemanticLeafVisualID(int visualID) {
		switch (visualID) {
		case ModelEditPart.VISUAL_ID:
			return false;
		case PrimitiveTypeEditPart.VISUAL_ID:
		case PropertyEditPart.VISUAL_ID:
		case OperationEditPart.VISUAL_ID:
		case EnumerationLiteralEditPart.VISUAL_ID:
		case Operation2EditPart.VISUAL_ID:
		case VariationPointEditPart.VISUAL_ID:
		case VariationEditPart.VISUAL_ID:
		case DecisionNodeEditPart.VISUAL_ID:
		case MergeNodeEditPart.VISUAL_ID:
		case InitialNodeEditPart.VISUAL_ID:
		case ActivityFinalNodeEditPart.VISUAL_ID:
		case ForkNodeEditPart.VISUAL_ID:
		case JoinNodeEditPart.VISUAL_ID:
		case CallActionEditPart.VISUAL_ID:
		case SendActionEditPart.VISUAL_ID:
		case AcceptActionEditPart.VISUAL_ID:
		case VxActionEditPart.VISUAL_ID:
		case ObjectNodeEditPart.VISUAL_ID:
		case AssignActionEditPart.VISUAL_ID:
		case EmptyActionEditPart.VISUAL_ID:
			return true;
		default:
			break;
		}
		return false;
	}

	/**
	 * @generated
	 */
	public static final DiagramStructure TYPED_INSTANCE = new DiagramStructure() {
		/**
		 * @generated
		 */
		@Override
		public int getVisualID(View view) {
			return edu.ustb.sei.mde.taileduml.diagram.part.TUMLVisualIDRegistry
					.getVisualID(view);
		}

		/**
		 * @generated
		 */
		@Override
		public String getModelID(View view) {
			return edu.ustb.sei.mde.taileduml.diagram.part.TUMLVisualIDRegistry
					.getModelID(view);
		}

		/**
		 * @generated
		 */
		@Override
		public int getNodeVisualID(View containerView, EObject domainElement) {
			return edu.ustb.sei.mde.taileduml.diagram.part.TUMLVisualIDRegistry
					.getNodeVisualID(containerView, domainElement);
		}

		/**
		 * @generated
		 */
		@Override
		public boolean checkNodeVisualID(View containerView,
				EObject domainElement, int candidate) {
			return edu.ustb.sei.mde.taileduml.diagram.part.TUMLVisualIDRegistry
					.checkNodeVisualID(containerView, domainElement, candidate);
		}

		/**
		 * @generated
		 */
		@Override
		public boolean isCompartmentVisualID(int visualID) {
			return edu.ustb.sei.mde.taileduml.diagram.part.TUMLVisualIDRegistry
					.isCompartmentVisualID(visualID);
		}

		/**
		 * @generated
		 */
		@Override
		public boolean isSemanticLeafVisualID(int visualID) {
			return edu.ustb.sei.mde.taileduml.diagram.part.TUMLVisualIDRegistry
					.isSemanticLeafVisualID(visualID);
		}
	};

}
