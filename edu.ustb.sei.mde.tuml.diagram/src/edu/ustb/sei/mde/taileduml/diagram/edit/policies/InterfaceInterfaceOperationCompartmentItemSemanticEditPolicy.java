package edu.ustb.sei.mde.taileduml.diagram.edit.policies;

import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;

import edu.ustb.sei.mde.taileduml.diagram.edit.commands.Operation2CreateCommand;
import edu.ustb.sei.mde.taileduml.diagram.providers.TUMLElementTypes;

/**
 * @generated
 */
public class InterfaceInterfaceOperationCompartmentItemSemanticEditPolicy
		extends TUMLBaseItemSemanticEditPolicy {

	/**
	 * @generated
	 */
	public InterfaceInterfaceOperationCompartmentItemSemanticEditPolicy() {
		super(TUMLElementTypes.Interface_3006);
	}

	/**
	 * @generated
	 */
	protected Command getCreateCommand(CreateElementRequest req) {
		if (TUMLElementTypes.Operation_3007 == req.getElementType()) {
			return getGEFWrapper(new Operation2CreateCommand(req));
		}
		return super.getCreateCommand(req);
	}

}
