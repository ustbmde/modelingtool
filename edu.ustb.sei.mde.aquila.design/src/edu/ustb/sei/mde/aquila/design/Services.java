package edu.ustb.sei.mde.aquila.design;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import org.eclipse.emf.codegen.ecore.genmodel.util.GenModelUtil;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

import edu.ustb.sei.mde.Aquila.AqActor;
import edu.ustb.sei.mde.Aquila.AqAttribute;
import edu.ustb.sei.mde.Aquila.AqClass;
import edu.ustb.sei.mde.Aquila.AqClassifier;
import edu.ustb.sei.mde.Aquila.AqOperation;
import edu.ustb.sei.mde.Aquila.AqParameter;
import edu.ustb.sei.mde.Aquila.AqReference;
import edu.ustb.sei.mde.Aquila.AqTransition;
import edu.ustb.sei.mde.Aquila.AquilaPackage;
import edu.ustb.sei.mde.Aquila.TypedElement;

/**
 * The services class used by VSM.
 */
public class Services {
    
    /**
    * See http://help.eclipse.org/neon/index.jsp?topic=%2Forg.eclipse.sirius.doc%2Fdoc%2Findex.html&cp=24 for documentation on how to write service methods.
    */
    public EObject myService(EObject self, String arg) {
       // TODO Auto-generated code
      return self;
    }
    
    static private List<EObject> allInstancesInModel(EObject self, EClass clazz) {
    	List<EObject> result = new ArrayList<>();
    	result.add(null);
    	self.eResource().getAllContents().forEachRemaining(e->{
    		if(clazz.isSuperTypeOf(e.eClass())) {
    			result.add(e);
    		}
    	});
    	return result;
    }
    
    public List<EObject> allClassifiersInModel(EObject self) {
    	return allInstancesInModel(self, AquilaPackage.Literals.AQ_CLASSIFIER);
    }
    
    public List<EObject> allDatatypesInModel(EObject self) {
    	return allInstancesInModel(self, AquilaPackage.Literals.AQ_DATA_TYPE);
    }
    
    public List<EObject> allClassesInModel(EObject self) {
    	return allInstancesInModel(self, AquilaPackage.Literals.AQ_CLASS);
    }
    
    public List<EObject> allCandidateOppositeReferences(AqReference ref) {
    	List<EObject> result = new ArrayList<>();
    	result.add(null);
    	ref.getReferenceType().getStructuralFeatures()
    			.stream().filter(e-> e instanceof AqReference && e.getType() == ref.eContainer())
    			.forEach(e->result.add(e));
    	
    	return result;
    }
    
    private void typedElementToString(StringBuilder builder, TypedElement self, Consumer<StringBuilder> insert) {
    	builder.append(self.getName() == null ? "NO NAME" : self.getName());
    	
    	if(insert!=null) {
    		insert.accept(builder);
    	}
    	
    	if(self.getType()!=null) {
    		builder.append(":");
    		builder.append(self.getType().getName());
    	}
    	
    	if(self.getUpperBound() != 1) {
    		builder.append("[");
    		if(self.getLowerBound() == self.getUpperBound()) {
    			builder.append(self.getLowerBound());
    		} else {
    			builder.append(self.getLowerBound());
    			builder.append("..");
    			builder.append(self.getUpperBound());
    		}
    		builder.append("]");
    	}
    }
    
    public String attributeToString(AqAttribute self) {
    	StringBuilder builder = new StringBuilder();
    	typedElementToString(builder, self, null);
    	return builder.toString();
    }
    
    public String operationToString(AqOperation self) {
    	StringBuilder builder = new StringBuilder();
    	typedElementToString(builder, self, sb->{
    		sb.append("(");
    		self.getParameters().forEach(p->{
    			if(p!=self.getParameters().get(0))
    				sb.append(",");
    			sb.append(p.getName());
    		});
    		sb.append(")");
    	});
    	return builder.toString();
    }
    
    public String parameterToString(AqParameter self) {
    	StringBuilder builder = new StringBuilder();
    	typedElementToString(builder, self, null);
    	return builder.toString();
    }
    
    public String transitionToString(AqTransition self) {
    	StringBuilder builder = new StringBuilder();
    	if(self.getTrigger()==null || self.getTrigger().isEmpty()) {}
    	else builder.append(self.getTrigger());
    	if(self.getGuard()==null || self.getGuard().isEmpty()) {}
    	else {
    		builder.append('[');
    		builder.append(self.getGuard());
    		builder.append(']');
    	}
    	
    	return builder.toString();
    }
    
    
    private boolean checkCircleInheritance(AqClass clazz, AqClass from) {
    	if(from.getSuperTypes().contains(clazz)) return false;
    	for(AqClass p : from.getSuperTypes()) {
    		if(!checkCircleInheritance(clazz, p)) return false;
    	}
    	return true;
    }
    public boolean checkCircleInheritance(AqClass clazz) {
    	return checkCircleInheritance(clazz, clazz);
    }
    
    private boolean checkCircleInheritance(AqActor clazz, AqActor from) {
    	if(from.getSuperActors().contains(clazz)) return false;
    	for(AqActor p : from.getSuperActors()) {
    		if(!checkCircleInheritance(clazz, p)) return false;
    	}
    	return true;
    }
    public boolean checkCircleInheritance(AqActor clazz) {
    	return checkCircleInheritance(clazz, clazz);
    }
 
}
