package edu.ustb.sei.mde.aquila.design;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;

import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.common.util.BasicMonitor;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IJavaModel;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.internal.core.JavaProject;
import org.eclipse.sirius.diagram.business.internal.metamodel.spec.DSemanticDiagramSpec;
import org.eclipse.sirius.tools.api.ui.IExternalJavaAction;
import org.eclipse.sirius.tools.internal.resource.ResourceSetUtil;

import edu.ustb.sei.mde.Aquila.Model;
import edu.ustb.sei.mde.Aquila.codegen.Aquila2java;

public class JavaCodeGeneration implements IExternalJavaAction {

	public JavaCodeGeneration() {
		// TODO Auto-generated constructor stub
	}

	@SuppressWarnings("restriction")
	@Override
	public boolean canExecute(Collection<? extends EObject> arg0) {
		if(arg0.size()!=1) return false;
		EObject obj = arg0.iterator().next();
		return obj instanceof DSemanticDiagramSpec; 
	}

	@SuppressWarnings("restriction")
	@Override
	public void execute(Collection<? extends EObject> arg0, Map<String, Object> arg1) {
		DSemanticDiagramSpec view = (DSemanticDiagramSpec) arg0.iterator().next();
		EObject element = view.getTarget();
		Model model = (Model) EcoreUtil.getRootContainer(element);
		Resource res = model.eResource();
		
		IWorkspaceRoot myWorkspaceRoot = ResourcesPlugin.getWorkspace().getRoot();
		try {
			IPath path = new Path(res.getURI().trimFileExtension().toPlatformString(true));
			IFolder folder = myWorkspaceRoot.getFolder(path);
			if(fixProjectNature(myWorkspaceRoot, folder.getProject(), folder)==false) return;
			
			Aquila2java generator;
			generator = new Aquila2java(model, folder.getLocation().toFile(), Collections.emptyList());
			generator.doGenerate(new BasicMonitor());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	protected boolean fixProjectNature(IWorkspaceRoot root, IProject project, IFolder folder) {
		if(project==null || project.exists()==false) return false;
		
		try {
			if(project.hasNature(JavaCore.NATURE_ID)==false) {
				IProjectDescription description = project.getDescription();
				description.setNatureIds(new String[] { JavaCore.NATURE_ID });
				project.setDescription(description, null);				
			}
			
			IJavaProject javaProject = JavaCore.create(project);
			IPackageFragmentRoot pkgRoot = javaProject.getPackageFragmentRoot(folder);
			if(pkgRoot.exists()==false) {
				if(folder.exists()==false) {
					folder.create(true, true, null);
				}
				
				IClasspathEntry[] oldPaths = javaProject.getRawClasspath();
				IClasspathEntry[] newPaths = Arrays.copyOf(oldPaths, oldPaths.length + 1);
				newPaths[oldPaths.length] = JavaCore.newSourceEntry(folder.getFullPath());
				javaProject.setRawClasspath(newPaths, null);
			}
			
			
		} catch (CoreException e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
	}

}
