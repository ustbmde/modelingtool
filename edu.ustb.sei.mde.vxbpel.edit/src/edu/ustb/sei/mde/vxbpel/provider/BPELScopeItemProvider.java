/**
 */
package edu.ustb.sei.mde.vxbpel.provider;


import edu.ustb.sei.mde.vxbpel.BPELScope;
import edu.ustb.sei.mde.vxbpel.VxBPELFactory;
import edu.ustb.sei.mde.vxbpel.VxBPELPackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link edu.ustb.sei.mde.vxbpel.BPELScope} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class BPELScopeItemProvider
	extends BPELActivityItemProvider
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BPELScopeItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addExitOnStandardFaultPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Exit On Standard Fault feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addExitOnStandardFaultPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_BPELScopeElement_exitOnStandardFault_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_BPELScopeElement_exitOnStandardFault_feature", "_UI_BPELScopeElement_type"),
				 VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__EXIT_ON_STANDARD_FAULT,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__PARTNER_LINKS);
			childrenFeatures.add(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__MESSAGE_EXCHANGES);
			childrenFeatures.add(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__VARIABLES);
			childrenFeatures.add(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__CORRELATION_SETS);
			childrenFeatures.add(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__FAULT_HANDLER_LIST);
			childrenFeatures.add(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__EVENT_HANDLER_LIST);
			childrenFeatures.add(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__ACTIVITY);
			childrenFeatures.add(VxBPELPackage.Literals.BPEL_SCOPE__COMPENSATION_HANDLER);
			childrenFeatures.add(VxBPELPackage.Literals.BPEL_SCOPE__TERMINATION_HANDLER);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns BPELScope.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/BPELScope"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((BPELScope)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_BPELScope_type") :
			getString("_UI_BPELScope_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(BPELScope.class)) {
			case VxBPELPackage.BPEL_SCOPE__EXIT_ON_STANDARD_FAULT:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case VxBPELPackage.BPEL_SCOPE__PARTNER_LINKS:
			case VxBPELPackage.BPEL_SCOPE__MESSAGE_EXCHANGES:
			case VxBPELPackage.BPEL_SCOPE__VARIABLES:
			case VxBPELPackage.BPEL_SCOPE__CORRELATION_SETS:
			case VxBPELPackage.BPEL_SCOPE__FAULT_HANDLER_LIST:
			case VxBPELPackage.BPEL_SCOPE__EVENT_HANDLER_LIST:
			case VxBPELPackage.BPEL_SCOPE__ACTIVITY:
			case VxBPELPackage.BPEL_SCOPE__COMPENSATION_HANDLER:
			case VxBPELPackage.BPEL_SCOPE__TERMINATION_HANDLER:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__PARTNER_LINKS,
				 VxBPELFactory.eINSTANCE.createBPELPartnerLink()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__MESSAGE_EXCHANGES,
				 VxBPELFactory.eINSTANCE.createBPELMessageExchange()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__VARIABLES,
				 VxBPELFactory.eINSTANCE.createBPELVariable()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__CORRELATION_SETS,
				 VxBPELFactory.eINSTANCE.createBPELCorrelationSet()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__FAULT_HANDLER_LIST,
				 VxBPELFactory.eINSTANCE.createXMLAttribute()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__FAULT_HANDLER_LIST,
				 VxBPELFactory.eINSTANCE.createBPELExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__FAULT_HANDLER_LIST,
				 VxBPELFactory.eINSTANCE.createBPELBoolExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__FAULT_HANDLER_LIST,
				 VxBPELFactory.eINSTANCE.createBPELDeadlineExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__FAULT_HANDLER_LIST,
				 VxBPELFactory.eINSTANCE.createBPELDurationExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__FAULT_HANDLER_LIST,
				 VxBPELFactory.eINSTANCE.createBPELUnsignedIntegerExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__FAULT_HANDLER_LIST,
				 VxBPELFactory.eINSTANCE.createBPELProcess()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__FAULT_HANDLER_LIST,
				 VxBPELFactory.eINSTANCE.createBPELImport()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__FAULT_HANDLER_LIST,
				 VxBPELFactory.eINSTANCE.createBPELPartnerLink()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__FAULT_HANDLER_LIST,
				 VxBPELFactory.eINSTANCE.createBPELMessageExchange()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__FAULT_HANDLER_LIST,
				 VxBPELFactory.eINSTANCE.createBPELVariable()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__FAULT_HANDLER_LIST,
				 VxBPELFactory.eINSTANCE.createBPELCorrelationSet()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__FAULT_HANDLER_LIST,
				 VxBPELFactory.eINSTANCE.createBPELTargetList()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__FAULT_HANDLER_LIST,
				 VxBPELFactory.eINSTANCE.createBPELTarget()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__FAULT_HANDLER_LIST,
				 VxBPELFactory.eINSTANCE.createBPELSourceList()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__FAULT_HANDLER_LIST,
				 VxBPELFactory.eINSTANCE.createBPELSource()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__FAULT_HANDLER_LIST,
				 VxBPELFactory.eINSTANCE.createBPELInvoke()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__FAULT_HANDLER_LIST,
				 VxBPELFactory.eINSTANCE.createBPELReceive()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__FAULT_HANDLER_LIST,
				 VxBPELFactory.eINSTANCE.createBPELReply()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__FAULT_HANDLER_LIST,
				 VxBPELFactory.eINSTANCE.createBPELAssign()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__FAULT_HANDLER_LIST,
				 VxBPELFactory.eINSTANCE.createBPELCopy()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__FAULT_HANDLER_LIST,
				 VxBPELFactory.eINSTANCE.createBPELCopyFrom()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__FAULT_HANDLER_LIST,
				 VxBPELFactory.eINSTANCE.createBPELCopyTo()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__FAULT_HANDLER_LIST,
				 VxBPELFactory.eINSTANCE.createBPELLiteral()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__FAULT_HANDLER_LIST,
				 VxBPELFactory.eINSTANCE.createBPELQuery()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__FAULT_HANDLER_LIST,
				 VxBPELFactory.eINSTANCE.createBPELThrow()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__FAULT_HANDLER_LIST,
				 VxBPELFactory.eINSTANCE.createBPELRethrow()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__FAULT_HANDLER_LIST,
				 VxBPELFactory.eINSTANCE.createBPELWait()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__FAULT_HANDLER_LIST,
				 VxBPELFactory.eINSTANCE.createBPELEmpty()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__FAULT_HANDLER_LIST,
				 VxBPELFactory.eINSTANCE.createBPELExit()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__FAULT_HANDLER_LIST,
				 VxBPELFactory.eINSTANCE.createBPELSequence()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__FAULT_HANDLER_LIST,
				 VxBPELFactory.eINSTANCE.createBPELIf()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__FAULT_HANDLER_LIST,
				 VxBPELFactory.eINSTANCE.createBPELElseIf()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__FAULT_HANDLER_LIST,
				 VxBPELFactory.eINSTANCE.createBPELWhile()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__FAULT_HANDLER_LIST,
				 VxBPELFactory.eINSTANCE.createBPELRepeatUntil()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__FAULT_HANDLER_LIST,
				 VxBPELFactory.eINSTANCE.createBPELPick()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__FAULT_HANDLER_LIST,
				 VxBPELFactory.eINSTANCE.createBPELFlow()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__FAULT_HANDLER_LIST,
				 VxBPELFactory.eINSTANCE.createBPELLink()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__FAULT_HANDLER_LIST,
				 VxBPELFactory.eINSTANCE.createBPELForEach()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__FAULT_HANDLER_LIST,
				 VxBPELFactory.eINSTANCE.createBPELScope()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__FAULT_HANDLER_LIST,
				 VxBPELFactory.eINSTANCE.createVxBPELVariationPoint()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__FAULT_HANDLER_LIST,
				 VxBPELFactory.eINSTANCE.createVxBPELVariant()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__FAULT_HANDLER_LIST,
				 VxBPELFactory.eINSTANCE.createVxBPELConfigurableVariationPoint()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__FAULT_HANDLER_LIST,
				 VxBPELFactory.eINSTANCE.createVxBPELConfigurableVariant()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__FAULT_HANDLER_LIST,
				 VxBPELFactory.eINSTANCE.createVxBPELVariationPointPChoice()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__FAULT_HANDLER_LIST,
				 VxBPELFactory.eINSTANCE.createVxBPELProcess()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__EVENT_HANDLER_LIST,
				 VxBPELFactory.eINSTANCE.createXMLAttribute()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__EVENT_HANDLER_LIST,
				 VxBPELFactory.eINSTANCE.createBPELExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__EVENT_HANDLER_LIST,
				 VxBPELFactory.eINSTANCE.createBPELBoolExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__EVENT_HANDLER_LIST,
				 VxBPELFactory.eINSTANCE.createBPELDeadlineExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__EVENT_HANDLER_LIST,
				 VxBPELFactory.eINSTANCE.createBPELDurationExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__EVENT_HANDLER_LIST,
				 VxBPELFactory.eINSTANCE.createBPELUnsignedIntegerExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__EVENT_HANDLER_LIST,
				 VxBPELFactory.eINSTANCE.createBPELProcess()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__EVENT_HANDLER_LIST,
				 VxBPELFactory.eINSTANCE.createBPELImport()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__EVENT_HANDLER_LIST,
				 VxBPELFactory.eINSTANCE.createBPELPartnerLink()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__EVENT_HANDLER_LIST,
				 VxBPELFactory.eINSTANCE.createBPELMessageExchange()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__EVENT_HANDLER_LIST,
				 VxBPELFactory.eINSTANCE.createBPELVariable()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__EVENT_HANDLER_LIST,
				 VxBPELFactory.eINSTANCE.createBPELCorrelationSet()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__EVENT_HANDLER_LIST,
				 VxBPELFactory.eINSTANCE.createBPELTargetList()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__EVENT_HANDLER_LIST,
				 VxBPELFactory.eINSTANCE.createBPELTarget()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__EVENT_HANDLER_LIST,
				 VxBPELFactory.eINSTANCE.createBPELSourceList()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__EVENT_HANDLER_LIST,
				 VxBPELFactory.eINSTANCE.createBPELSource()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__EVENT_HANDLER_LIST,
				 VxBPELFactory.eINSTANCE.createBPELInvoke()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__EVENT_HANDLER_LIST,
				 VxBPELFactory.eINSTANCE.createBPELReceive()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__EVENT_HANDLER_LIST,
				 VxBPELFactory.eINSTANCE.createBPELReply()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__EVENT_HANDLER_LIST,
				 VxBPELFactory.eINSTANCE.createBPELAssign()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__EVENT_HANDLER_LIST,
				 VxBPELFactory.eINSTANCE.createBPELCopy()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__EVENT_HANDLER_LIST,
				 VxBPELFactory.eINSTANCE.createBPELCopyFrom()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__EVENT_HANDLER_LIST,
				 VxBPELFactory.eINSTANCE.createBPELCopyTo()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__EVENT_HANDLER_LIST,
				 VxBPELFactory.eINSTANCE.createBPELLiteral()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__EVENT_HANDLER_LIST,
				 VxBPELFactory.eINSTANCE.createBPELQuery()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__EVENT_HANDLER_LIST,
				 VxBPELFactory.eINSTANCE.createBPELThrow()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__EVENT_HANDLER_LIST,
				 VxBPELFactory.eINSTANCE.createBPELRethrow()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__EVENT_HANDLER_LIST,
				 VxBPELFactory.eINSTANCE.createBPELWait()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__EVENT_HANDLER_LIST,
				 VxBPELFactory.eINSTANCE.createBPELEmpty()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__EVENT_HANDLER_LIST,
				 VxBPELFactory.eINSTANCE.createBPELExit()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__EVENT_HANDLER_LIST,
				 VxBPELFactory.eINSTANCE.createBPELSequence()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__EVENT_HANDLER_LIST,
				 VxBPELFactory.eINSTANCE.createBPELIf()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__EVENT_HANDLER_LIST,
				 VxBPELFactory.eINSTANCE.createBPELElseIf()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__EVENT_HANDLER_LIST,
				 VxBPELFactory.eINSTANCE.createBPELWhile()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__EVENT_HANDLER_LIST,
				 VxBPELFactory.eINSTANCE.createBPELRepeatUntil()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__EVENT_HANDLER_LIST,
				 VxBPELFactory.eINSTANCE.createBPELPick()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__EVENT_HANDLER_LIST,
				 VxBPELFactory.eINSTANCE.createBPELFlow()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__EVENT_HANDLER_LIST,
				 VxBPELFactory.eINSTANCE.createBPELLink()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__EVENT_HANDLER_LIST,
				 VxBPELFactory.eINSTANCE.createBPELForEach()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__EVENT_HANDLER_LIST,
				 VxBPELFactory.eINSTANCE.createBPELScope()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__EVENT_HANDLER_LIST,
				 VxBPELFactory.eINSTANCE.createVxBPELVariationPoint()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__EVENT_HANDLER_LIST,
				 VxBPELFactory.eINSTANCE.createVxBPELVariant()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__EVENT_HANDLER_LIST,
				 VxBPELFactory.eINSTANCE.createVxBPELConfigurableVariationPoint()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__EVENT_HANDLER_LIST,
				 VxBPELFactory.eINSTANCE.createVxBPELConfigurableVariant()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__EVENT_HANDLER_LIST,
				 VxBPELFactory.eINSTANCE.createVxBPELVariationPointPChoice()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__EVENT_HANDLER_LIST,
				 VxBPELFactory.eINSTANCE.createVxBPELProcess()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__ACTIVITY,
				 VxBPELFactory.eINSTANCE.createBPELInvoke()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__ACTIVITY,
				 VxBPELFactory.eINSTANCE.createBPELReceive()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__ACTIVITY,
				 VxBPELFactory.eINSTANCE.createBPELReply()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__ACTIVITY,
				 VxBPELFactory.eINSTANCE.createBPELAssign()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__ACTIVITY,
				 VxBPELFactory.eINSTANCE.createBPELThrow()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__ACTIVITY,
				 VxBPELFactory.eINSTANCE.createBPELRethrow()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__ACTIVITY,
				 VxBPELFactory.eINSTANCE.createBPELWait()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__ACTIVITY,
				 VxBPELFactory.eINSTANCE.createBPELEmpty()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__ACTIVITY,
				 VxBPELFactory.eINSTANCE.createBPELExit()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__ACTIVITY,
				 VxBPELFactory.eINSTANCE.createBPELSequence()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__ACTIVITY,
				 VxBPELFactory.eINSTANCE.createBPELIf()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__ACTIVITY,
				 VxBPELFactory.eINSTANCE.createBPELElseIf()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__ACTIVITY,
				 VxBPELFactory.eINSTANCE.createBPELWhile()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__ACTIVITY,
				 VxBPELFactory.eINSTANCE.createBPELRepeatUntil()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__ACTIVITY,
				 VxBPELFactory.eINSTANCE.createBPELPick()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__ACTIVITY,
				 VxBPELFactory.eINSTANCE.createBPELFlow()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__ACTIVITY,
				 VxBPELFactory.eINSTANCE.createBPELForEach()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__ACTIVITY,
				 VxBPELFactory.eINSTANCE.createBPELScope()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__ACTIVITY,
				 VxBPELFactory.eINSTANCE.createVxBPELVariationPoint()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE__COMPENSATION_HANDLER,
				 VxBPELFactory.eINSTANCE.createXMLAttribute()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE__COMPENSATION_HANDLER,
				 VxBPELFactory.eINSTANCE.createBPELExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE__COMPENSATION_HANDLER,
				 VxBPELFactory.eINSTANCE.createBPELBoolExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE__COMPENSATION_HANDLER,
				 VxBPELFactory.eINSTANCE.createBPELDeadlineExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE__COMPENSATION_HANDLER,
				 VxBPELFactory.eINSTANCE.createBPELDurationExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE__COMPENSATION_HANDLER,
				 VxBPELFactory.eINSTANCE.createBPELUnsignedIntegerExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE__COMPENSATION_HANDLER,
				 VxBPELFactory.eINSTANCE.createBPELProcess()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE__COMPENSATION_HANDLER,
				 VxBPELFactory.eINSTANCE.createBPELImport()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE__COMPENSATION_HANDLER,
				 VxBPELFactory.eINSTANCE.createBPELPartnerLink()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE__COMPENSATION_HANDLER,
				 VxBPELFactory.eINSTANCE.createBPELMessageExchange()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE__COMPENSATION_HANDLER,
				 VxBPELFactory.eINSTANCE.createBPELVariable()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE__COMPENSATION_HANDLER,
				 VxBPELFactory.eINSTANCE.createBPELCorrelationSet()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE__COMPENSATION_HANDLER,
				 VxBPELFactory.eINSTANCE.createBPELTargetList()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE__COMPENSATION_HANDLER,
				 VxBPELFactory.eINSTANCE.createBPELTarget()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE__COMPENSATION_HANDLER,
				 VxBPELFactory.eINSTANCE.createBPELSourceList()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE__COMPENSATION_HANDLER,
				 VxBPELFactory.eINSTANCE.createBPELSource()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE__COMPENSATION_HANDLER,
				 VxBPELFactory.eINSTANCE.createBPELInvoke()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE__COMPENSATION_HANDLER,
				 VxBPELFactory.eINSTANCE.createBPELReceive()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE__COMPENSATION_HANDLER,
				 VxBPELFactory.eINSTANCE.createBPELReply()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE__COMPENSATION_HANDLER,
				 VxBPELFactory.eINSTANCE.createBPELAssign()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE__COMPENSATION_HANDLER,
				 VxBPELFactory.eINSTANCE.createBPELCopy()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE__COMPENSATION_HANDLER,
				 VxBPELFactory.eINSTANCE.createBPELCopyFrom()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE__COMPENSATION_HANDLER,
				 VxBPELFactory.eINSTANCE.createBPELCopyTo()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE__COMPENSATION_HANDLER,
				 VxBPELFactory.eINSTANCE.createBPELLiteral()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE__COMPENSATION_HANDLER,
				 VxBPELFactory.eINSTANCE.createBPELQuery()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE__COMPENSATION_HANDLER,
				 VxBPELFactory.eINSTANCE.createBPELThrow()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE__COMPENSATION_HANDLER,
				 VxBPELFactory.eINSTANCE.createBPELRethrow()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE__COMPENSATION_HANDLER,
				 VxBPELFactory.eINSTANCE.createBPELWait()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE__COMPENSATION_HANDLER,
				 VxBPELFactory.eINSTANCE.createBPELEmpty()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE__COMPENSATION_HANDLER,
				 VxBPELFactory.eINSTANCE.createBPELExit()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE__COMPENSATION_HANDLER,
				 VxBPELFactory.eINSTANCE.createBPELSequence()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE__COMPENSATION_HANDLER,
				 VxBPELFactory.eINSTANCE.createBPELIf()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE__COMPENSATION_HANDLER,
				 VxBPELFactory.eINSTANCE.createBPELElseIf()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE__COMPENSATION_HANDLER,
				 VxBPELFactory.eINSTANCE.createBPELWhile()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE__COMPENSATION_HANDLER,
				 VxBPELFactory.eINSTANCE.createBPELRepeatUntil()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE__COMPENSATION_HANDLER,
				 VxBPELFactory.eINSTANCE.createBPELPick()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE__COMPENSATION_HANDLER,
				 VxBPELFactory.eINSTANCE.createBPELFlow()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE__COMPENSATION_HANDLER,
				 VxBPELFactory.eINSTANCE.createBPELLink()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE__COMPENSATION_HANDLER,
				 VxBPELFactory.eINSTANCE.createBPELForEach()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE__COMPENSATION_HANDLER,
				 VxBPELFactory.eINSTANCE.createBPELScope()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE__COMPENSATION_HANDLER,
				 VxBPELFactory.eINSTANCE.createVxBPELVariationPoint()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE__COMPENSATION_HANDLER,
				 VxBPELFactory.eINSTANCE.createVxBPELVariant()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE__COMPENSATION_HANDLER,
				 VxBPELFactory.eINSTANCE.createVxBPELConfigurableVariationPoint()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE__COMPENSATION_HANDLER,
				 VxBPELFactory.eINSTANCE.createVxBPELConfigurableVariant()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE__COMPENSATION_HANDLER,
				 VxBPELFactory.eINSTANCE.createVxBPELVariationPointPChoice()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE__COMPENSATION_HANDLER,
				 VxBPELFactory.eINSTANCE.createVxBPELProcess()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE__TERMINATION_HANDLER,
				 VxBPELFactory.eINSTANCE.createXMLAttribute()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE__TERMINATION_HANDLER,
				 VxBPELFactory.eINSTANCE.createBPELExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE__TERMINATION_HANDLER,
				 VxBPELFactory.eINSTANCE.createBPELBoolExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE__TERMINATION_HANDLER,
				 VxBPELFactory.eINSTANCE.createBPELDeadlineExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE__TERMINATION_HANDLER,
				 VxBPELFactory.eINSTANCE.createBPELDurationExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE__TERMINATION_HANDLER,
				 VxBPELFactory.eINSTANCE.createBPELUnsignedIntegerExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE__TERMINATION_HANDLER,
				 VxBPELFactory.eINSTANCE.createBPELProcess()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE__TERMINATION_HANDLER,
				 VxBPELFactory.eINSTANCE.createBPELImport()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE__TERMINATION_HANDLER,
				 VxBPELFactory.eINSTANCE.createBPELPartnerLink()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE__TERMINATION_HANDLER,
				 VxBPELFactory.eINSTANCE.createBPELMessageExchange()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE__TERMINATION_HANDLER,
				 VxBPELFactory.eINSTANCE.createBPELVariable()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE__TERMINATION_HANDLER,
				 VxBPELFactory.eINSTANCE.createBPELCorrelationSet()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE__TERMINATION_HANDLER,
				 VxBPELFactory.eINSTANCE.createBPELTargetList()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE__TERMINATION_HANDLER,
				 VxBPELFactory.eINSTANCE.createBPELTarget()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE__TERMINATION_HANDLER,
				 VxBPELFactory.eINSTANCE.createBPELSourceList()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE__TERMINATION_HANDLER,
				 VxBPELFactory.eINSTANCE.createBPELSource()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE__TERMINATION_HANDLER,
				 VxBPELFactory.eINSTANCE.createBPELInvoke()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE__TERMINATION_HANDLER,
				 VxBPELFactory.eINSTANCE.createBPELReceive()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE__TERMINATION_HANDLER,
				 VxBPELFactory.eINSTANCE.createBPELReply()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE__TERMINATION_HANDLER,
				 VxBPELFactory.eINSTANCE.createBPELAssign()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE__TERMINATION_HANDLER,
				 VxBPELFactory.eINSTANCE.createBPELCopy()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE__TERMINATION_HANDLER,
				 VxBPELFactory.eINSTANCE.createBPELCopyFrom()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE__TERMINATION_HANDLER,
				 VxBPELFactory.eINSTANCE.createBPELCopyTo()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE__TERMINATION_HANDLER,
				 VxBPELFactory.eINSTANCE.createBPELLiteral()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE__TERMINATION_HANDLER,
				 VxBPELFactory.eINSTANCE.createBPELQuery()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE__TERMINATION_HANDLER,
				 VxBPELFactory.eINSTANCE.createBPELThrow()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE__TERMINATION_HANDLER,
				 VxBPELFactory.eINSTANCE.createBPELRethrow()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE__TERMINATION_HANDLER,
				 VxBPELFactory.eINSTANCE.createBPELWait()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE__TERMINATION_HANDLER,
				 VxBPELFactory.eINSTANCE.createBPELEmpty()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE__TERMINATION_HANDLER,
				 VxBPELFactory.eINSTANCE.createBPELExit()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE__TERMINATION_HANDLER,
				 VxBPELFactory.eINSTANCE.createBPELSequence()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE__TERMINATION_HANDLER,
				 VxBPELFactory.eINSTANCE.createBPELIf()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE__TERMINATION_HANDLER,
				 VxBPELFactory.eINSTANCE.createBPELElseIf()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE__TERMINATION_HANDLER,
				 VxBPELFactory.eINSTANCE.createBPELWhile()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE__TERMINATION_HANDLER,
				 VxBPELFactory.eINSTANCE.createBPELRepeatUntil()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE__TERMINATION_HANDLER,
				 VxBPELFactory.eINSTANCE.createBPELPick()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE__TERMINATION_HANDLER,
				 VxBPELFactory.eINSTANCE.createBPELFlow()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE__TERMINATION_HANDLER,
				 VxBPELFactory.eINSTANCE.createBPELLink()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE__TERMINATION_HANDLER,
				 VxBPELFactory.eINSTANCE.createBPELForEach()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE__TERMINATION_HANDLER,
				 VxBPELFactory.eINSTANCE.createBPELScope()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE__TERMINATION_HANDLER,
				 VxBPELFactory.eINSTANCE.createVxBPELVariationPoint()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE__TERMINATION_HANDLER,
				 VxBPELFactory.eINSTANCE.createVxBPELVariant()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE__TERMINATION_HANDLER,
				 VxBPELFactory.eINSTANCE.createVxBPELConfigurableVariationPoint()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE__TERMINATION_HANDLER,
				 VxBPELFactory.eINSTANCE.createVxBPELConfigurableVariant()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE__TERMINATION_HANDLER,
				 VxBPELFactory.eINSTANCE.createVxBPELVariationPointPChoice()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_SCOPE__TERMINATION_HANDLER,
				 VxBPELFactory.eINSTANCE.createVxBPELProcess()));
	}

	/**
	 * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCreateChildText(Object owner, Object feature, Object child, Collection<?> selection) {
		Object childFeature = feature;
		Object childObject = child;

		boolean qualify =
			childFeature == VxBPELPackage.Literals.XML_ATTRIBUTE_CONTAINER__ATTRIBUTES ||
			childFeature == VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__FAULT_HANDLER_LIST ||
			childFeature == VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__EVENT_HANDLER_LIST ||
			childFeature == VxBPELPackage.Literals.BPEL_SCOPE__COMPENSATION_HANDLER ||
			childFeature == VxBPELPackage.Literals.BPEL_SCOPE__TERMINATION_HANDLER ||
			childFeature == VxBPELPackage.Literals.BPEL_ACTIVITY__TARGET_SET ||
			childFeature == VxBPELPackage.Literals.BPEL_ACTIVITY__SOURCE_SET ||
			childFeature == VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__PARTNER_LINKS ||
			childFeature == VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__MESSAGE_EXCHANGES ||
			childFeature == VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__VARIABLES ||
			childFeature == VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__CORRELATION_SETS ||
			childFeature == VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT__ACTIVITY;

		if (qualify) {
			return getString
				("_UI_CreateChild_text2",
				 new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

}
