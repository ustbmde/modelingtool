/**
 */
package edu.ustb.sei.mde.vxbpel.provider;


import edu.ustb.sei.mde.vxbpel.BPELWhile;
import edu.ustb.sei.mde.vxbpel.VxBPELFactory;
import edu.ustb.sei.mde.vxbpel.VxBPELPackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link edu.ustb.sei.mde.vxbpel.BPELWhile} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class BPELWhileItemProvider
	extends BPELActivityItemProvider
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BPELWhileItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

		}
		return itemPropertyDescriptors;
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(VxBPELPackage.Literals.BPEL_WHILE__CONDITION);
			childrenFeatures.add(VxBPELPackage.Literals.BPEL_WHILE__BODY_ACTIVITY);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns BPELWhile.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/BPELWhile"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((BPELWhile)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_BPELWhile_type") :
			getString("_UI_BPELWhile_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(BPELWhile.class)) {
			case VxBPELPackage.BPEL_WHILE__CONDITION:
			case VxBPELPackage.BPEL_WHILE__BODY_ACTIVITY:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_WHILE__CONDITION,
				 VxBPELFactory.eINSTANCE.createBPELBoolExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_WHILE__BODY_ACTIVITY,
				 VxBPELFactory.eINSTANCE.createBPELInvoke()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_WHILE__BODY_ACTIVITY,
				 VxBPELFactory.eINSTANCE.createBPELReceive()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_WHILE__BODY_ACTIVITY,
				 VxBPELFactory.eINSTANCE.createBPELReply()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_WHILE__BODY_ACTIVITY,
				 VxBPELFactory.eINSTANCE.createBPELAssign()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_WHILE__BODY_ACTIVITY,
				 VxBPELFactory.eINSTANCE.createBPELThrow()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_WHILE__BODY_ACTIVITY,
				 VxBPELFactory.eINSTANCE.createBPELRethrow()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_WHILE__BODY_ACTIVITY,
				 VxBPELFactory.eINSTANCE.createBPELWait()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_WHILE__BODY_ACTIVITY,
				 VxBPELFactory.eINSTANCE.createBPELEmpty()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_WHILE__BODY_ACTIVITY,
				 VxBPELFactory.eINSTANCE.createBPELExit()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_WHILE__BODY_ACTIVITY,
				 VxBPELFactory.eINSTANCE.createBPELSequence()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_WHILE__BODY_ACTIVITY,
				 VxBPELFactory.eINSTANCE.createBPELIf()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_WHILE__BODY_ACTIVITY,
				 VxBPELFactory.eINSTANCE.createBPELElseIf()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_WHILE__BODY_ACTIVITY,
				 VxBPELFactory.eINSTANCE.createBPELWhile()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_WHILE__BODY_ACTIVITY,
				 VxBPELFactory.eINSTANCE.createBPELRepeatUntil()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_WHILE__BODY_ACTIVITY,
				 VxBPELFactory.eINSTANCE.createBPELPick()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_WHILE__BODY_ACTIVITY,
				 VxBPELFactory.eINSTANCE.createBPELFlow()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_WHILE__BODY_ACTIVITY,
				 VxBPELFactory.eINSTANCE.createBPELForEach()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_WHILE__BODY_ACTIVITY,
				 VxBPELFactory.eINSTANCE.createBPELScope()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_WHILE__BODY_ACTIVITY,
				 VxBPELFactory.eINSTANCE.createVxBPELVariationPoint()));
	}

}
