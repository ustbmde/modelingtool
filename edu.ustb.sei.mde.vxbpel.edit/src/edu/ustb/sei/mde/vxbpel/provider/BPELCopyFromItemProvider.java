/**
 */
package edu.ustb.sei.mde.vxbpel.provider;


import edu.ustb.sei.mde.vxbpel.BPELCopyFrom;
import edu.ustb.sei.mde.vxbpel.VxBPELFactory;
import edu.ustb.sei.mde.vxbpel.VxBPELPackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link edu.ustb.sei.mde.vxbpel.BPELCopyFrom} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class BPELCopyFromItemProvider
	extends XMLAttributeContainerItemProvider
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BPELCopyFromItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addVariablePropertyDescriptor(object);
			addPartPropertyDescriptor(object);
			addPropertyPropertyDescriptor(object);
			addPartnerLinkPropertyDescriptor(object);
			addEndpointReferencePropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Variable feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addVariablePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_BPELCopyFrom_variable_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_BPELCopyFrom_variable_feature", "_UI_BPELCopyFrom_type"),
				 VxBPELPackage.Literals.BPEL_COPY_FROM__VARIABLE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Part feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addPartPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_BPELCopyFrom_part_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_BPELCopyFrom_part_feature", "_UI_BPELCopyFrom_type"),
				 VxBPELPackage.Literals.BPEL_COPY_FROM__PART,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Property feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addPropertyPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_BPELCopyFrom_property_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_BPELCopyFrom_property_feature", "_UI_BPELCopyFrom_type"),
				 VxBPELPackage.Literals.BPEL_COPY_FROM__PROPERTY,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Partner Link feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addPartnerLinkPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_BPELCopyFrom_partnerLink_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_BPELCopyFrom_partnerLink_feature", "_UI_BPELCopyFrom_type"),
				 VxBPELPackage.Literals.BPEL_COPY_FROM__PARTNER_LINK,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Endpoint Reference feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addEndpointReferencePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_BPELCopyFrom_endpointReference_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_BPELCopyFrom_endpointReference_feature", "_UI_BPELCopyFrom_type"),
				 VxBPELPackage.Literals.BPEL_COPY_FROM__ENDPOINT_REFERENCE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(VxBPELPackage.Literals.BPEL_COPY_FROM__EXPRESSION);
			childrenFeatures.add(VxBPELPackage.Literals.BPEL_COPY_FROM__QUERY);
			childrenFeatures.add(VxBPELPackage.Literals.BPEL_COPY_FROM__LITERAL);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns BPELCopyFrom.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/BPELCopyFrom"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((BPELCopyFrom)object).getVariable();
		return label == null || label.length() == 0 ?
			getString("_UI_BPELCopyFrom_type") :
			getString("_UI_BPELCopyFrom_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(BPELCopyFrom.class)) {
			case VxBPELPackage.BPEL_COPY_FROM__VARIABLE:
			case VxBPELPackage.BPEL_COPY_FROM__PART:
			case VxBPELPackage.BPEL_COPY_FROM__PROPERTY:
			case VxBPELPackage.BPEL_COPY_FROM__PARTNER_LINK:
			case VxBPELPackage.BPEL_COPY_FROM__ENDPOINT_REFERENCE:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case VxBPELPackage.BPEL_COPY_FROM__EXPRESSION:
			case VxBPELPackage.BPEL_COPY_FROM__QUERY:
			case VxBPELPackage.BPEL_COPY_FROM__LITERAL:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_COPY_FROM__EXPRESSION,
				 VxBPELFactory.eINSTANCE.createBPELExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_COPY_FROM__EXPRESSION,
				 VxBPELFactory.eINSTANCE.createBPELBoolExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_COPY_FROM__EXPRESSION,
				 VxBPELFactory.eINSTANCE.createBPELDeadlineExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_COPY_FROM__EXPRESSION,
				 VxBPELFactory.eINSTANCE.createBPELDurationExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_COPY_FROM__EXPRESSION,
				 VxBPELFactory.eINSTANCE.createBPELUnsignedIntegerExpression()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_COPY_FROM__QUERY,
				 VxBPELFactory.eINSTANCE.createBPELQuery()));

		newChildDescriptors.add
			(createChildParameter
				(VxBPELPackage.Literals.BPEL_COPY_FROM__LITERAL,
				 VxBPELFactory.eINSTANCE.createBPELLiteral()));
	}

}
