/**
 */
package edu.ustb.sei.mde.Aquila;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Aq Primitive Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.Aquila.AqPrimitiveType#getInstanceType <em>Instance Type</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.mde.Aquila.AquilaPackage#getAqPrimitiveType()
 * @model
 * @generated
 */
public interface AqPrimitiveType extends AqDataType {

	/**
	 * Returns the value of the '<em><b>Instance Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Instance Type</em>' attribute.
	 * @see #setInstanceType(String)
	 * @see edu.ustb.sei.mde.Aquila.AquilaPackage#getAqPrimitiveType_InstanceType()
	 * @model
	 * @generated
	 */
	String getInstanceType();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.Aquila.AqPrimitiveType#getInstanceType <em>Instance Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Instance Type</em>' attribute.
	 * @see #getInstanceType()
	 * @generated
	 */
	void setInstanceType(String value);
} // AqPrimitiveType
