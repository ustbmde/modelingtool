/**
 */
package edu.ustb.sei.mde.Aquila;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Aq Reference</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.Aquila.AqReference#isContainment <em>Containment</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.Aquila.AqReference#getReferenceType <em>Reference Type</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.Aquila.AqReference#getOpposite <em>Opposite</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.mde.Aquila.AquilaPackage#getAqReference()
 * @model
 * @generated
 */
public interface AqReference extends AqStructuralFeature {
	/**
	 * Returns the value of the '<em><b>Containment</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Containment</em>' attribute.
	 * @see #setContainment(boolean)
	 * @see edu.ustb.sei.mde.Aquila.AquilaPackage#getAqReference_Containment()
	 * @model
	 * @generated
	 */
	boolean isContainment();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.Aquila.AqReference#isContainment <em>Containment</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Containment</em>' attribute.
	 * @see #isContainment()
	 * @generated
	 */
	void setContainment(boolean value);

	/**
	 * Returns the value of the '<em><b>Reference Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reference Type</em>' reference.
	 * @see #setReferenceType(AqClass)
	 * @see edu.ustb.sei.mde.Aquila.AquilaPackage#getAqReference_ReferenceType()
	 * @model
	 * @generated
	 */
	AqClass getReferenceType();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.Aquila.AqReference#getReferenceType <em>Reference Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Reference Type</em>' reference.
	 * @see #getReferenceType()
	 * @generated
	 */
	void setReferenceType(AqClass value);

	/**
	 * Returns the value of the '<em><b>Opposite</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Opposite</em>' reference.
	 * @see #setOpposite(AqReference)
	 * @see edu.ustb.sei.mde.Aquila.AquilaPackage#getAqReference_Opposite()
	 * @model
	 * @generated
	 */
	AqReference getOpposite();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.Aquila.AqReference#getOpposite <em>Opposite</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Opposite</em>' reference.
	 * @see #getOpposite()
	 * @generated
	 */
	void setOpposite(AqReference value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	boolean isMajor();

} // AqReference
