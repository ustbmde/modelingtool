/**
 */
package edu.ustb.sei.mde.Aquila;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Aq Actor</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.Aquila.AqActor#getUseCases <em>Use Cases</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.Aquila.AqActor#getSuperActors <em>Super Actors</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.mde.Aquila.AquilaPackage#getAqActor()
 * @model
 * @generated
 */
public interface AqActor extends AqUseCaseElement {
	/**
	 * Returns the value of the '<em><b>Use Cases</b></em>' reference list.
	 * The list contents are of type {@link edu.ustb.sei.mde.Aquila.AqUseCase}.
	 * It is bidirectional and its opposite is '{@link edu.ustb.sei.mde.Aquila.AqUseCase#getActors <em>Actors</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Use Cases</em>' reference list.
	 * @see edu.ustb.sei.mde.Aquila.AquilaPackage#getAqActor_UseCases()
	 * @see edu.ustb.sei.mde.Aquila.AqUseCase#getActors
	 * @model opposite="actors" ordered="false"
	 * @generated
	 */
	EList<AqUseCase> getUseCases();

	/**
	 * Returns the value of the '<em><b>Super Actors</b></em>' reference list.
	 * The list contents are of type {@link edu.ustb.sei.mde.Aquila.AqActor}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Super Actors</em>' reference list.
	 * @see edu.ustb.sei.mde.Aquila.AquilaPackage#getAqActor_SuperActors()
	 * @model ordered="false"
	 * @generated
	 */
	EList<AqActor> getSuperActors();

} // AqActor
