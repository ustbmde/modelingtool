/**
 */
package edu.ustb.sei.mde.Aquila;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Aq Parameter</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.Aquila.AqParameter#getParameterType <em>Parameter Type</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.mde.Aquila.AquilaPackage#getAqParameter()
 * @model
 * @generated
 */
public interface AqParameter extends TypedElement {

	/**
	 * Returns the value of the '<em><b>Parameter Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parameter Type</em>' reference.
	 * @see #setParameterType(AqClassifier)
	 * @see edu.ustb.sei.mde.Aquila.AquilaPackage#getAqParameter_ParameterType()
	 * @model
	 * @generated
	 */
	AqClassifier getParameterType();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.Aquila.AqParameter#getParameterType <em>Parameter Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parameter Type</em>' reference.
	 * @see #getParameterType()
	 * @generated
	 */
	void setParameterType(AqClassifier value);
} // AqParameter
