/**
 */
package edu.ustb.sei.mde.Aquila;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Aq Composite State</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.ustb.sei.mde.Aquila.AquilaPackage#getAqCompositeState()
 * @model
 * @generated
 */
public interface AqCompositeState extends AqStateRegion, AqState {
} // AqCompositeState
