/**
 */
package edu.ustb.sei.mde.Aquila;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Aq Use Case Element</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.ustb.sei.mde.Aquila.AquilaPackage#getAqUseCaseElement()
 * @model abstract="true"
 * @generated
 */
public interface AqUseCaseElement extends NamedElement {
} // AqUseCaseElement
