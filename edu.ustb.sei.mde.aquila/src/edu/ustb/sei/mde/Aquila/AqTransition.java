/**
 */
package edu.ustb.sei.mde.Aquila;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Aq Transition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.Aquila.AqTransition#getTarget <em>Target</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.Aquila.AqTransition#getTrigger <em>Trigger</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.Aquila.AqTransition#getGuard <em>Guard</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.mde.Aquila.AquilaPackage#getAqTransition()
 * @model
 * @generated
 */
public interface AqTransition extends EObject {
	/**
	 * Returns the value of the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target</em>' reference.
	 * @see #setTarget(AqVertex)
	 * @see edu.ustb.sei.mde.Aquila.AquilaPackage#getAqTransition_Target()
	 * @model required="true"
	 * @generated
	 */
	AqVertex getTarget();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.Aquila.AqTransition#getTarget <em>Target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target</em>' reference.
	 * @see #getTarget()
	 * @generated
	 */
	void setTarget(AqVertex value);

	/**
	 * Returns the value of the '<em><b>Trigger</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Trigger</em>' attribute.
	 * @see #setTrigger(String)
	 * @see edu.ustb.sei.mde.Aquila.AquilaPackage#getAqTransition_Trigger()
	 * @model
	 * @generated
	 */
	String getTrigger();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.Aquila.AqTransition#getTrigger <em>Trigger</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Trigger</em>' attribute.
	 * @see #getTrigger()
	 * @generated
	 */
	void setTrigger(String value);

	/**
	 * Returns the value of the '<em><b>Guard</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Guard</em>' attribute.
	 * @see #setGuard(String)
	 * @see edu.ustb.sei.mde.Aquila.AquilaPackage#getAqTransition_Guard()
	 * @model
	 * @generated
	 */
	String getGuard();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.Aquila.AqTransition#getGuard <em>Guard</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Guard</em>' attribute.
	 * @see #getGuard()
	 * @generated
	 */
	void setGuard(String value);

} // AqTransition
