/**
 */
package edu.ustb.sei.mde.Aquila;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Aq Structural Feature</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.ustb.sei.mde.Aquila.AquilaPackage#getAqStructuralFeature()
 * @model abstract="true"
 * @generated
 */
public interface AqStructuralFeature extends NamedElement, TypedElement {
} // AqStructuralFeature
