/**
 */
package edu.ustb.sei.mde.Aquila;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Aq Class</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.Aquila.AqClass#getStructuralFeatures <em>Structural Features</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.Aquila.AqClass#getSuperTypes <em>Super Types</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.Aquila.AqClass#isInterface <em>Interface</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.Aquila.AqClass#getOperations <em>Operations</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.Aquila.AqClass#getDependencies <em>Dependencies</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.Aquila.AqClass#isAbstract <em>Abstract</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.mde.Aquila.AquilaPackage#getAqClass()
 * @model
 * @generated
 */
public interface AqClass extends AqClassifier {
	/**
	 * Returns the value of the '<em><b>Structural Features</b></em>' containment reference list.
	 * The list contents are of type {@link edu.ustb.sei.mde.Aquila.AqStructuralFeature}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Structural Features</em>' containment reference list.
	 * @see edu.ustb.sei.mde.Aquila.AquilaPackage#getAqClass_StructuralFeatures()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<AqStructuralFeature> getStructuralFeatures();

	/**
	 * Returns the value of the '<em><b>Super Types</b></em>' reference list.
	 * The list contents are of type {@link edu.ustb.sei.mde.Aquila.AqClass}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Super Types</em>' reference list.
	 * @see edu.ustb.sei.mde.Aquila.AquilaPackage#getAqClass_SuperTypes()
	 * @model ordered="false"
	 * @generated
	 */
	EList<AqClass> getSuperTypes();

	/**
	 * Returns the value of the '<em><b>Interface</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Interface</em>' attribute.
	 * @see #setInterface(boolean)
	 * @see edu.ustb.sei.mde.Aquila.AquilaPackage#getAqClass_Interface()
	 * @model default="false" required="true"
	 * @generated
	 */
	boolean isInterface();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.Aquila.AqClass#isInterface <em>Interface</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Interface</em>' attribute.
	 * @see #isInterface()
	 * @generated
	 */
	void setInterface(boolean value);

	/**
	 * Returns the value of the '<em><b>Operations</b></em>' containment reference list.
	 * The list contents are of type {@link edu.ustb.sei.mde.Aquila.AqOperation}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operations</em>' containment reference list.
	 * @see edu.ustb.sei.mde.Aquila.AquilaPackage#getAqClass_Operations()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<AqOperation> getOperations();

	/**
	 * Returns the value of the '<em><b>Dependencies</b></em>' containment reference list.
	 * The list contents are of type {@link edu.ustb.sei.mde.Aquila.AqDependency}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dependencies</em>' containment reference list.
	 * @see edu.ustb.sei.mde.Aquila.AquilaPackage#getAqClass_Dependencies()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<AqDependency> getDependencies();

	/**
	 * Returns the value of the '<em><b>Abstract</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Abstract</em>' attribute.
	 * @see #setAbstract(boolean)
	 * @see edu.ustb.sei.mde.Aquila.AquilaPackage#getAqClass_Abstract()
	 * @model default="false"
	 * @generated
	 */
	boolean isAbstract();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.Aquila.AqClass#isAbstract <em>Abstract</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Abstract</em>' attribute.
	 * @see #isAbstract()
	 * @generated
	 */
	void setAbstract(boolean value);

} // AqClass
