/**
 */
package edu.ustb.sei.mde.Aquila;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Aq State Region</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.Aquila.AqStateRegion#getVertices <em>Vertices</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.mde.Aquila.AquilaPackage#getAqStateRegion()
 * @model abstract="true"
 * @generated
 */
public interface AqStateRegion extends EObject {
	/**
	 * Returns the value of the '<em><b>Vertices</b></em>' containment reference list.
	 * The list contents are of type {@link edu.ustb.sei.mde.Aquila.AqVertex}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Vertices</em>' containment reference list.
	 * @see edu.ustb.sei.mde.Aquila.AquilaPackage#getAqStateRegion_Vertices()
	 * @model containment="true"
	 * @generated
	 */
	EList<AqVertex> getVertices();

} // AqStateRegion
