/**
 */
package edu.ustb.sei.mde.Aquila;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Aq State Machine</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.ustb.sei.mde.Aquila.AquilaPackage#getAqStateMachine()
 * @model
 * @generated
 */
public interface AqStateMachine extends AqStateRegion, NamedElement {
} // AqStateMachine
