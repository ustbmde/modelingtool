/**
 */
package edu.ustb.sei.mde.Aquila.util;

import edu.ustb.sei.mde.Aquila.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see edu.ustb.sei.mde.Aquila.AquilaPackage
 * @generated
 */
public class AquilaSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static AquilaPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AquilaSwitch() {
		if (modelPackage == null) {
			modelPackage = AquilaPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case AquilaPackage.MODEL: {
				Model model = (Model)theEObject;
				T result = caseModel(model);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AquilaPackage.AQ_USE_CASE_ELEMENT: {
				AqUseCaseElement aqUseCaseElement = (AqUseCaseElement)theEObject;
				T result = caseAqUseCaseElement(aqUseCaseElement);
				if (result == null) result = caseNamedElement(aqUseCaseElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AquilaPackage.NAMED_ELEMENT: {
				NamedElement namedElement = (NamedElement)theEObject;
				T result = caseNamedElement(namedElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AquilaPackage.AQ_CLASSIFIER: {
				AqClassifier aqClassifier = (AqClassifier)theEObject;
				T result = caseAqClassifier(aqClassifier);
				if (result == null) result = caseNamedElement(aqClassifier);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AquilaPackage.AQ_CLASS: {
				AqClass aqClass = (AqClass)theEObject;
				T result = caseAqClass(aqClass);
				if (result == null) result = caseAqClassifier(aqClass);
				if (result == null) result = caseNamedElement(aqClass);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AquilaPackage.AQ_DATA_TYPE: {
				AqDataType aqDataType = (AqDataType)theEObject;
				T result = caseAqDataType(aqDataType);
				if (result == null) result = caseAqClassifier(aqDataType);
				if (result == null) result = caseNamedElement(aqDataType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AquilaPackage.AQ_PRIMITIVE_TYPE: {
				AqPrimitiveType aqPrimitiveType = (AqPrimitiveType)theEObject;
				T result = caseAqPrimitiveType(aqPrimitiveType);
				if (result == null) result = caseAqDataType(aqPrimitiveType);
				if (result == null) result = caseAqClassifier(aqPrimitiveType);
				if (result == null) result = caseNamedElement(aqPrimitiveType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AquilaPackage.AQ_ENUM_TYPE: {
				AqEnumType aqEnumType = (AqEnumType)theEObject;
				T result = caseAqEnumType(aqEnumType);
				if (result == null) result = caseAqDataType(aqEnumType);
				if (result == null) result = caseAqClassifier(aqEnumType);
				if (result == null) result = caseNamedElement(aqEnumType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AquilaPackage.AQ_STRUCTURAL_FEATURE: {
				AqStructuralFeature aqStructuralFeature = (AqStructuralFeature)theEObject;
				T result = caseAqStructuralFeature(aqStructuralFeature);
				if (result == null) result = caseTypedElement(aqStructuralFeature);
				if (result == null) result = caseNamedElement(aqStructuralFeature);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AquilaPackage.AQ_ATTRIBUTE: {
				AqAttribute aqAttribute = (AqAttribute)theEObject;
				T result = caseAqAttribute(aqAttribute);
				if (result == null) result = caseAqStructuralFeature(aqAttribute);
				if (result == null) result = caseTypedElement(aqAttribute);
				if (result == null) result = caseNamedElement(aqAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AquilaPackage.AQ_REFERENCE: {
				AqReference aqReference = (AqReference)theEObject;
				T result = caseAqReference(aqReference);
				if (result == null) result = caseAqStructuralFeature(aqReference);
				if (result == null) result = caseTypedElement(aqReference);
				if (result == null) result = caseNamedElement(aqReference);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AquilaPackage.AQ_ENUM_LITERAL: {
				AqEnumLiteral aqEnumLiteral = (AqEnumLiteral)theEObject;
				T result = caseAqEnumLiteral(aqEnumLiteral);
				if (result == null) result = caseNamedElement(aqEnumLiteral);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AquilaPackage.AQ_OPERATION: {
				AqOperation aqOperation = (AqOperation)theEObject;
				T result = caseAqOperation(aqOperation);
				if (result == null) result = caseTypedElement(aqOperation);
				if (result == null) result = caseNamedElement(aqOperation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AquilaPackage.AQ_PARAMETER: {
				AqParameter aqParameter = (AqParameter)theEObject;
				T result = caseAqParameter(aqParameter);
				if (result == null) result = caseTypedElement(aqParameter);
				if (result == null) result = caseNamedElement(aqParameter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AquilaPackage.TYPED_ELEMENT: {
				TypedElement typedElement = (TypedElement)theEObject;
				T result = caseTypedElement(typedElement);
				if (result == null) result = caseNamedElement(typedElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AquilaPackage.AQ_USE_CASE: {
				AqUseCase aqUseCase = (AqUseCase)theEObject;
				T result = caseAqUseCase(aqUseCase);
				if (result == null) result = caseAqUseCaseElement(aqUseCase);
				if (result == null) result = caseNamedElement(aqUseCase);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AquilaPackage.AQ_ACTOR: {
				AqActor aqActor = (AqActor)theEObject;
				T result = caseAqActor(aqActor);
				if (result == null) result = caseAqUseCaseElement(aqActor);
				if (result == null) result = caseNamedElement(aqActor);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AquilaPackage.AQ_STATE: {
				AqState aqState = (AqState)theEObject;
				T result = caseAqState(aqState);
				if (result == null) result = caseNamedElement(aqState);
				if (result == null) result = caseAqVertex(aqState);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AquilaPackage.AQ_TRANSITION: {
				AqTransition aqTransition = (AqTransition)theEObject;
				T result = caseAqTransition(aqTransition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AquilaPackage.AQ_VERTEX: {
				AqVertex aqVertex = (AqVertex)theEObject;
				T result = caseAqVertex(aqVertex);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AquilaPackage.AQ_PSEUDO_STATE: {
				AqPseudoState aqPseudoState = (AqPseudoState)theEObject;
				T result = caseAqPseudoState(aqPseudoState);
				if (result == null) result = caseAqVertex(aqPseudoState);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AquilaPackage.AQ_STATE_REGION: {
				AqStateRegion aqStateRegion = (AqStateRegion)theEObject;
				T result = caseAqStateRegion(aqStateRegion);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AquilaPackage.AQ_COMPOSITE_STATE: {
				AqCompositeState aqCompositeState = (AqCompositeState)theEObject;
				T result = caseAqCompositeState(aqCompositeState);
				if (result == null) result = caseAqStateRegion(aqCompositeState);
				if (result == null) result = caseAqState(aqCompositeState);
				if (result == null) result = caseNamedElement(aqCompositeState);
				if (result == null) result = caseAqVertex(aqCompositeState);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AquilaPackage.AQ_STATE_MACHINE: {
				AqStateMachine aqStateMachine = (AqStateMachine)theEObject;
				T result = caseAqStateMachine(aqStateMachine);
				if (result == null) result = caseAqStateRegion(aqStateMachine);
				if (result == null) result = caseNamedElement(aqStateMachine);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case AquilaPackage.AQ_DEPENDENCY: {
				AqDependency aqDependency = (AqDependency)theEObject;
				T result = caseAqDependency(aqDependency);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Model</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Model</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseModel(Model object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Aq Use Case Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Aq Use Case Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAqUseCaseElement(AqUseCaseElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Named Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Named Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNamedElement(NamedElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Aq Classifier</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Aq Classifier</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAqClassifier(AqClassifier object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Aq Class</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Aq Class</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAqClass(AqClass object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Aq Data Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Aq Data Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAqDataType(AqDataType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Aq Primitive Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Aq Primitive Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAqPrimitiveType(AqPrimitiveType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Aq Enum Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Aq Enum Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAqEnumType(AqEnumType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Aq Structural Feature</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Aq Structural Feature</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAqStructuralFeature(AqStructuralFeature object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Aq Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Aq Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAqAttribute(AqAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Aq Reference</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Aq Reference</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAqReference(AqReference object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Aq Enum Literal</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Aq Enum Literal</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAqEnumLiteral(AqEnumLiteral object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Aq Operation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Aq Operation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAqOperation(AqOperation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Aq Parameter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Aq Parameter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAqParameter(AqParameter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Typed Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Typed Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTypedElement(TypedElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Aq Use Case</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Aq Use Case</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAqUseCase(AqUseCase object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Aq Actor</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Aq Actor</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAqActor(AqActor object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Aq State</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Aq State</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAqState(AqState object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Aq Transition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Aq Transition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAqTransition(AqTransition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Aq Vertex</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Aq Vertex</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAqVertex(AqVertex object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Aq Pseudo State</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Aq Pseudo State</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAqPseudoState(AqPseudoState object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Aq State Region</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Aq State Region</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAqStateRegion(AqStateRegion object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Aq Composite State</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Aq Composite State</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAqCompositeState(AqCompositeState object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Aq State Machine</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Aq State Machine</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAqStateMachine(AqStateMachine object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Aq Dependency</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Aq Dependency</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAqDependency(AqDependency object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //AquilaSwitch
