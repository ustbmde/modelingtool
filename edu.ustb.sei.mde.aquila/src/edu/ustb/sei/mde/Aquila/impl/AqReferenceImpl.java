/**
 */
package edu.ustb.sei.mde.Aquila.impl;

import edu.ustb.sei.mde.Aquila.AqClass;
import edu.ustb.sei.mde.Aquila.AqClassifier;
import edu.ustb.sei.mde.Aquila.AqReference;
import edu.ustb.sei.mde.Aquila.AquilaPackage;

import java.lang.reflect.InvocationTargetException;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Aq Reference</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.Aquila.impl.AqReferenceImpl#isContainment <em>Containment</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.Aquila.impl.AqReferenceImpl#getReferenceType <em>Reference Type</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.Aquila.impl.AqReferenceImpl#getOpposite <em>Opposite</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AqReferenceImpl extends AqStructuralFeatureImpl implements AqReference {
	/**
	 * The default value of the '{@link #isContainment() <em>Containment</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isContainment()
	 * @generated
	 * @ordered
	 */
	protected static final boolean CONTAINMENT_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isContainment() <em>Containment</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isContainment()
	 * @generated
	 * @ordered
	 */
	protected boolean containment = CONTAINMENT_EDEFAULT;

	/**
	 * The cached value of the '{@link #getReferenceType() <em>Reference Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReferenceType()
	 * @generated
	 * @ordered
	 */
	protected AqClass referenceType;

	/**
	 * The cached value of the '{@link #getOpposite() <em>Opposite</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOpposite()
	 * @generated
	 * @ordered
	 */
	protected AqReference opposite;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected AqReferenceImpl() {
		super();
		internalID = nextInternalID ++;
	}
	
	private int internalID;
	static private int nextInternalID = 0;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AquilaPackage.Literals.AQ_REFERENCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isContainment() {
		return containment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public void setContainment(boolean newContainment) {
		boolean oldContainment = containment;
		containment = newContainment;
		
		if(newContainment && opposite!=null) 
			opposite.setContainment(false);
		
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AquilaPackage.AQ_REFERENCE__CONTAINMENT, oldContainment, containment));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AqClass getReferenceType() {
		if (referenceType != null && referenceType.eIsProxy()) {
			InternalEObject oldReferenceType = (InternalEObject)referenceType;
			referenceType = (AqClass)eResolveProxy(oldReferenceType);
			if (referenceType != oldReferenceType) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, AquilaPackage.AQ_REFERENCE__REFERENCE_TYPE, oldReferenceType, referenceType));
			}
		}
		return referenceType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AqClass basicGetReferenceType() {
		return referenceType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public void setReferenceType(AqClass newReferenceType) {
		if(newReferenceType==null) return;
		
		AqClass oldReferenceType = referenceType;
		referenceType = newReferenceType;
		
		if(newReferenceType!=oldReferenceType && opposite!=null) {
			newReferenceType.getStructuralFeatures().add(opposite);
		}
		
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AquilaPackage.AQ_REFERENCE__REFERENCE_TYPE, oldReferenceType, referenceType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AqReference getOpposite() {
		if (opposite != null && opposite.eIsProxy()) {
			InternalEObject oldOpposite = (InternalEObject)opposite;
			opposite = (AqReference)eResolveProxy(oldOpposite);
			if (opposite != oldOpposite) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, AquilaPackage.AQ_REFERENCE__OPPOSITE, oldOpposite, opposite));
			}
		}
		return opposite;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AqReference basicGetOpposite() {
		return opposite;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public void setOpposite(AqReference newOpposite) {
		AqReference oldOpposite = opposite;
		opposite = newOpposite;
		
		if(oldOpposite != newOpposite) {
			if(oldOpposite!=null) {
				oldOpposite.setOpposite(null);
			}
			
			if(newOpposite != null) {
				newOpposite.setOpposite(this);
			}
		}
		
		if (eNotificationRequired()) {
			eNotify(new ENotificationImpl(this, Notification.SET, AquilaPackage.AQ_REFERENCE__OPPOSITE, oldOpposite, opposite));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public boolean isMajor() {
		if(getOpposite() == null) return true;
		if(isContainment() == getOpposite().isContainment()) 
			return this.internalID < ((AqReferenceImpl)getOpposite()).internalID;
		if(isContainment()) return true;
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AquilaPackage.AQ_REFERENCE__CONTAINMENT:
				return isContainment();
			case AquilaPackage.AQ_REFERENCE__REFERENCE_TYPE:
				if (resolve) return getReferenceType();
				return basicGetReferenceType();
			case AquilaPackage.AQ_REFERENCE__OPPOSITE:
				if (resolve) return getOpposite();
				return basicGetOpposite();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AquilaPackage.AQ_REFERENCE__CONTAINMENT:
				setContainment((Boolean)newValue);
				return;
			case AquilaPackage.AQ_REFERENCE__REFERENCE_TYPE:
				setReferenceType((AqClass)newValue);
				return;
			case AquilaPackage.AQ_REFERENCE__OPPOSITE:
				setOpposite((AqReference)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AquilaPackage.AQ_REFERENCE__CONTAINMENT:
				setContainment(CONTAINMENT_EDEFAULT);
				return;
			case AquilaPackage.AQ_REFERENCE__REFERENCE_TYPE:
				setReferenceType((AqClass)null);
				return;
			case AquilaPackage.AQ_REFERENCE__OPPOSITE:
				setOpposite((AqReference)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AquilaPackage.AQ_REFERENCE__CONTAINMENT:
				return containment != CONTAINMENT_EDEFAULT;
			case AquilaPackage.AQ_REFERENCE__REFERENCE_TYPE:
				return referenceType != null;
			case AquilaPackage.AQ_REFERENCE__OPPOSITE:
				return opposite != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case AquilaPackage.AQ_REFERENCE___IS_MAJOR:
				return isMajor();
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (containment: ");
		result.append(containment);
		result.append(')');
		return result.toString();
	}
	
	@Override
	public AqClassifier basicGetType() {
		return this.basicGetReferenceType();
	}

	@Override
	public void setType(AqClassifier newType) {
		if(newType instanceof AqClass)
			this.setReferenceType((AqClass) newType);
	}

} //AqReferenceImpl
