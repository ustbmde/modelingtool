/**
 */
package edu.ustb.sei.mde.Aquila.impl;

import edu.ustb.sei.mde.Aquila.AqCompositeState;
import edu.ustb.sei.mde.Aquila.AqState;
import edu.ustb.sei.mde.Aquila.AqTransition;
import edu.ustb.sei.mde.Aquila.AqVertex;
import edu.ustb.sei.mde.Aquila.AquilaPackage;

import edu.ustb.sei.mde.Aquila.NamedElement;
import java.util.Collection;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Aq Composite State</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.Aquila.impl.AqCompositeStateImpl#getName <em>Name</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.Aquila.impl.AqCompositeStateImpl#getTransitions <em>Transitions</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AqCompositeStateImpl extends AqStateRegionImpl implements AqCompositeState {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getTransitions() <em>Transitions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTransitions()
	 * @generated
	 * @ordered
	 */
	protected EList<AqTransition> transitions;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AqCompositeStateImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AquilaPackage.Literals.AQ_COMPOSITE_STATE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AquilaPackage.AQ_COMPOSITE_STATE__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<AqTransition> getTransitions() {
		if (transitions == null) {
			transitions = new EObjectContainmentEList<AqTransition>(AqTransition.class, this, AquilaPackage.AQ_COMPOSITE_STATE__TRANSITIONS);
		}
		return transitions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AquilaPackage.AQ_COMPOSITE_STATE__TRANSITIONS:
				return ((InternalEList<?>)getTransitions()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AquilaPackage.AQ_COMPOSITE_STATE__NAME:
				return getName();
			case AquilaPackage.AQ_COMPOSITE_STATE__TRANSITIONS:
				return getTransitions();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AquilaPackage.AQ_COMPOSITE_STATE__NAME:
				setName((String)newValue);
				return;
			case AquilaPackage.AQ_COMPOSITE_STATE__TRANSITIONS:
				getTransitions().clear();
				getTransitions().addAll((Collection<? extends AqTransition>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AquilaPackage.AQ_COMPOSITE_STATE__NAME:
				setName(NAME_EDEFAULT);
				return;
			case AquilaPackage.AQ_COMPOSITE_STATE__TRANSITIONS:
				getTransitions().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AquilaPackage.AQ_COMPOSITE_STATE__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case AquilaPackage.AQ_COMPOSITE_STATE__TRANSITIONS:
				return transitions != null && !transitions.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == NamedElement.class) {
			switch (derivedFeatureID) {
				case AquilaPackage.AQ_COMPOSITE_STATE__NAME: return AquilaPackage.NAMED_ELEMENT__NAME;
				default: return -1;
			}
		}
		if (baseClass == AqVertex.class) {
			switch (derivedFeatureID) {
				case AquilaPackage.AQ_COMPOSITE_STATE__TRANSITIONS: return AquilaPackage.AQ_VERTEX__TRANSITIONS;
				default: return -1;
			}
		}
		if (baseClass == AqState.class) {
			switch (derivedFeatureID) {
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == NamedElement.class) {
			switch (baseFeatureID) {
				case AquilaPackage.NAMED_ELEMENT__NAME: return AquilaPackage.AQ_COMPOSITE_STATE__NAME;
				default: return -1;
			}
		}
		if (baseClass == AqVertex.class) {
			switch (baseFeatureID) {
				case AquilaPackage.AQ_VERTEX__TRANSITIONS: return AquilaPackage.AQ_COMPOSITE_STATE__TRANSITIONS;
				default: return -1;
			}
		}
		if (baseClass == AqState.class) {
			switch (baseFeatureID) {
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //AqCompositeStateImpl
