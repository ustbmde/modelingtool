/**
 */
package edu.ustb.sei.mde.Aquila.impl;

import edu.ustb.sei.mde.Aquila.AqClass;
import edu.ustb.sei.mde.Aquila.AqDependency;
import edu.ustb.sei.mde.Aquila.AqOperation;
import edu.ustb.sei.mde.Aquila.AqStructuralFeature;
import edu.ustb.sei.mde.Aquila.AquilaPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Aq Class</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.Aquila.impl.AqClassImpl#getStructuralFeatures <em>Structural Features</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.Aquila.impl.AqClassImpl#getSuperTypes <em>Super Types</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.Aquila.impl.AqClassImpl#isInterface <em>Interface</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.Aquila.impl.AqClassImpl#getOperations <em>Operations</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.Aquila.impl.AqClassImpl#getDependencies <em>Dependencies</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.Aquila.impl.AqClassImpl#isAbstract <em>Abstract</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AqClassImpl extends AqClassifierImpl implements AqClass {
	/**
	 * The cached value of the '{@link #getStructuralFeatures() <em>Structural Features</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStructuralFeatures()
	 * @generated
	 * @ordered
	 */
	protected EList<AqStructuralFeature> structuralFeatures;

	/**
	 * The cached value of the '{@link #getSuperTypes() <em>Super Types</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSuperTypes()
	 * @generated
	 * @ordered
	 */
	protected EList<AqClass> superTypes;

	/**
	 * The default value of the '{@link #isInterface() <em>Interface</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isInterface()
	 * @generated
	 * @ordered
	 */
	protected static final boolean INTERFACE_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isInterface() <em>Interface</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isInterface()
	 * @generated
	 * @ordered
	 */
	protected boolean interface_ = INTERFACE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getOperations() <em>Operations</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperations()
	 * @generated
	 * @ordered
	 */
	protected EList<AqOperation> operations;

	/**
	 * The cached value of the '{@link #getDependencies() <em>Dependencies</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDependencies()
	 * @generated
	 * @ordered
	 */
	protected EList<AqDependency> dependencies;

	/**
	 * The default value of the '{@link #isAbstract() <em>Abstract</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isAbstract()
	 * @generated
	 * @ordered
	 */
	protected static final boolean ABSTRACT_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isAbstract() <em>Abstract</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isAbstract()
	 * @generated
	 * @ordered
	 */
	protected boolean abstract_ = ABSTRACT_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AqClassImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AquilaPackage.Literals.AQ_CLASS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<AqStructuralFeature> getStructuralFeatures() {
		if (structuralFeatures == null) {
			structuralFeatures = new EObjectContainmentEList<AqStructuralFeature>(AqStructuralFeature.class, this, AquilaPackage.AQ_CLASS__STRUCTURAL_FEATURES);
		}
		return structuralFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<AqClass> getSuperTypes() {
		if (superTypes == null) {
			superTypes = new EObjectResolvingEList<AqClass>(AqClass.class, this, AquilaPackage.AQ_CLASS__SUPER_TYPES);
		}
		return superTypes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isInterface() {
		return interface_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setInterface(boolean newInterface) {
		boolean oldInterface = interface_;
		interface_ = newInterface;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AquilaPackage.AQ_CLASS__INTERFACE, oldInterface, interface_));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<AqOperation> getOperations() {
		if (operations == null) {
			operations = new EObjectContainmentEList<AqOperation>(AqOperation.class, this, AquilaPackage.AQ_CLASS__OPERATIONS);
		}
		return operations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<AqDependency> getDependencies() {
		if (dependencies == null) {
			dependencies = new EObjectContainmentEList<AqDependency>(AqDependency.class, this, AquilaPackage.AQ_CLASS__DEPENDENCIES);
		}
		return dependencies;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean isAbstract() {
		return abstract_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setAbstract(boolean newAbstract) {
		boolean oldAbstract = abstract_;
		abstract_ = newAbstract;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AquilaPackage.AQ_CLASS__ABSTRACT, oldAbstract, abstract_));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AquilaPackage.AQ_CLASS__STRUCTURAL_FEATURES:
				return ((InternalEList<?>)getStructuralFeatures()).basicRemove(otherEnd, msgs);
			case AquilaPackage.AQ_CLASS__OPERATIONS:
				return ((InternalEList<?>)getOperations()).basicRemove(otherEnd, msgs);
			case AquilaPackage.AQ_CLASS__DEPENDENCIES:
				return ((InternalEList<?>)getDependencies()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AquilaPackage.AQ_CLASS__STRUCTURAL_FEATURES:
				return getStructuralFeatures();
			case AquilaPackage.AQ_CLASS__SUPER_TYPES:
				return getSuperTypes();
			case AquilaPackage.AQ_CLASS__INTERFACE:
				return isInterface();
			case AquilaPackage.AQ_CLASS__OPERATIONS:
				return getOperations();
			case AquilaPackage.AQ_CLASS__DEPENDENCIES:
				return getDependencies();
			case AquilaPackage.AQ_CLASS__ABSTRACT:
				return isAbstract();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AquilaPackage.AQ_CLASS__STRUCTURAL_FEATURES:
				getStructuralFeatures().clear();
				getStructuralFeatures().addAll((Collection<? extends AqStructuralFeature>)newValue);
				return;
			case AquilaPackage.AQ_CLASS__SUPER_TYPES:
				getSuperTypes().clear();
				getSuperTypes().addAll((Collection<? extends AqClass>)newValue);
				return;
			case AquilaPackage.AQ_CLASS__INTERFACE:
				setInterface((Boolean)newValue);
				return;
			case AquilaPackage.AQ_CLASS__OPERATIONS:
				getOperations().clear();
				getOperations().addAll((Collection<? extends AqOperation>)newValue);
				return;
			case AquilaPackage.AQ_CLASS__DEPENDENCIES:
				getDependencies().clear();
				getDependencies().addAll((Collection<? extends AqDependency>)newValue);
				return;
			case AquilaPackage.AQ_CLASS__ABSTRACT:
				setAbstract((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AquilaPackage.AQ_CLASS__STRUCTURAL_FEATURES:
				getStructuralFeatures().clear();
				return;
			case AquilaPackage.AQ_CLASS__SUPER_TYPES:
				getSuperTypes().clear();
				return;
			case AquilaPackage.AQ_CLASS__INTERFACE:
				setInterface(INTERFACE_EDEFAULT);
				return;
			case AquilaPackage.AQ_CLASS__OPERATIONS:
				getOperations().clear();
				return;
			case AquilaPackage.AQ_CLASS__DEPENDENCIES:
				getDependencies().clear();
				return;
			case AquilaPackage.AQ_CLASS__ABSTRACT:
				setAbstract(ABSTRACT_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AquilaPackage.AQ_CLASS__STRUCTURAL_FEATURES:
				return structuralFeatures != null && !structuralFeatures.isEmpty();
			case AquilaPackage.AQ_CLASS__SUPER_TYPES:
				return superTypes != null && !superTypes.isEmpty();
			case AquilaPackage.AQ_CLASS__INTERFACE:
				return interface_ != INTERFACE_EDEFAULT;
			case AquilaPackage.AQ_CLASS__OPERATIONS:
				return operations != null && !operations.isEmpty();
			case AquilaPackage.AQ_CLASS__DEPENDENCIES:
				return dependencies != null && !dependencies.isEmpty();
			case AquilaPackage.AQ_CLASS__ABSTRACT:
				return abstract_ != ABSTRACT_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (interface: ");
		result.append(interface_);
		result.append(", abstract: ");
		result.append(abstract_);
		result.append(')');
		return result.toString();
	}

} //AqClassImpl
