/**
 */
package edu.ustb.sei.mde.Aquila.impl;

import edu.ustb.sei.mde.Aquila.AqDataType;
import edu.ustb.sei.mde.Aquila.AquilaPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Aq Data Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class AqDataTypeImpl extends AqClassifierImpl implements AqDataType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AqDataTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AquilaPackage.Literals.AQ_DATA_TYPE;
	}

} //AqDataTypeImpl
