/**
 */
package edu.ustb.sei.mde.Aquila.impl;

import edu.ustb.sei.mde.Aquila.AqUseCaseElement;
import edu.ustb.sei.mde.Aquila.AquilaPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Aq Use Case Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class AqUseCaseElementImpl extends NamedElementImpl implements AqUseCaseElement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AqUseCaseElementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AquilaPackage.Literals.AQ_USE_CASE_ELEMENT;
	}

} //AqUseCaseElementImpl
