/**
 */
package edu.ustb.sei.mde.Aquila.impl;

import edu.ustb.sei.mde.Aquila.AqClassifier;
import edu.ustb.sei.mde.Aquila.AqStateMachine;
import edu.ustb.sei.mde.Aquila.AqUseCaseElement;
import edu.ustb.sei.mde.Aquila.AquilaPackage;
import edu.ustb.sei.mde.Aquila.Model;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.Aquila.impl.ModelImpl#getElements <em>Elements</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.Aquila.impl.ModelImpl#getUseCaseElements <em>Use Case Elements</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.Aquila.impl.ModelImpl#getStateMachines <em>State Machines</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.Aquila.impl.ModelImpl#getUUID <em>UUID</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.Aquila.impl.ModelImpl#getPackage <em>Package</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ModelImpl extends MinimalEObjectImpl.Container implements Model {
	/**
	 * The cached value of the '{@link #getElements() <em>Elements</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElements()
	 * @generated
	 * @ordered
	 */
	protected EList<AqClassifier> elements;

	/**
	 * The cached value of the '{@link #getUseCaseElements() <em>Use Case Elements</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUseCaseElements()
	 * @generated
	 * @ordered
	 */
	protected EList<AqUseCaseElement> useCaseElements;

	/**
	 * The cached value of the '{@link #getStateMachines() <em>State Machines</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStateMachines()
	 * @generated
	 * @ordered
	 */
	protected EList<AqStateMachine> stateMachines;

	/**
	 * The default value of the '{@link #getUUID() <em>UUID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUUID()
	 * @generated
	 * @ordered
	 */
	protected static final String UUID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getUUID() <em>UUID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUUID()
	 * @generated
	 * @ordered
	 */
	protected String uuid = UUID_EDEFAULT;

	/**
	 * The default value of the '{@link #getPackage() <em>Package</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPackage()
	 * @generated
	 * @ordered
	 */
	protected static final String PACKAGE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getPackage() <em>Package</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPackage()
	 * @generated
	 * @ordered
	 */
	protected String package_ = PACKAGE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected ModelImpl() {
		super();
		this.uuid = java.util.UUID.randomUUID().toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AquilaPackage.Literals.MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<AqClassifier> getElements() {
		if (elements == null) {
			elements = new EObjectContainmentEList<AqClassifier>(AqClassifier.class, this, AquilaPackage.MODEL__ELEMENTS);
		}
		return elements;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<AqUseCaseElement> getUseCaseElements() {
		if (useCaseElements == null) {
			useCaseElements = new EObjectContainmentEList<AqUseCaseElement>(AqUseCaseElement.class, this, AquilaPackage.MODEL__USE_CASE_ELEMENTS);
		}
		return useCaseElements;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<AqStateMachine> getStateMachines() {
		if (stateMachines == null) {
			stateMachines = new EObjectContainmentEList<AqStateMachine>(AqStateMachine.class, this, AquilaPackage.MODEL__STATE_MACHINES);
		}
		return stateMachines;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getUUID() {
		return uuid;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setUUID(String newUUID) {
		String oldUUID = uuid;
		uuid = newUUID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AquilaPackage.MODEL__UUID, oldUUID, uuid));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getPackage() {
		return package_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setPackage(String newPackage) {
		String oldPackage = package_;
		package_ = newPackage;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AquilaPackage.MODEL__PACKAGE, oldPackage, package_));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AquilaPackage.MODEL__ELEMENTS:
				return ((InternalEList<?>)getElements()).basicRemove(otherEnd, msgs);
			case AquilaPackage.MODEL__USE_CASE_ELEMENTS:
				return ((InternalEList<?>)getUseCaseElements()).basicRemove(otherEnd, msgs);
			case AquilaPackage.MODEL__STATE_MACHINES:
				return ((InternalEList<?>)getStateMachines()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AquilaPackage.MODEL__ELEMENTS:
				return getElements();
			case AquilaPackage.MODEL__USE_CASE_ELEMENTS:
				return getUseCaseElements();
			case AquilaPackage.MODEL__STATE_MACHINES:
				return getStateMachines();
			case AquilaPackage.MODEL__UUID:
				return getUUID();
			case AquilaPackage.MODEL__PACKAGE:
				return getPackage();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AquilaPackage.MODEL__ELEMENTS:
				getElements().clear();
				getElements().addAll((Collection<? extends AqClassifier>)newValue);
				return;
			case AquilaPackage.MODEL__USE_CASE_ELEMENTS:
				getUseCaseElements().clear();
				getUseCaseElements().addAll((Collection<? extends AqUseCaseElement>)newValue);
				return;
			case AquilaPackage.MODEL__STATE_MACHINES:
				getStateMachines().clear();
				getStateMachines().addAll((Collection<? extends AqStateMachine>)newValue);
				return;
			case AquilaPackage.MODEL__UUID:
				setUUID((String)newValue);
				return;
			case AquilaPackage.MODEL__PACKAGE:
				setPackage((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AquilaPackage.MODEL__ELEMENTS:
				getElements().clear();
				return;
			case AquilaPackage.MODEL__USE_CASE_ELEMENTS:
				getUseCaseElements().clear();
				return;
			case AquilaPackage.MODEL__STATE_MACHINES:
				getStateMachines().clear();
				return;
			case AquilaPackage.MODEL__UUID:
				setUUID(UUID_EDEFAULT);
				return;
			case AquilaPackage.MODEL__PACKAGE:
				setPackage(PACKAGE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AquilaPackage.MODEL__ELEMENTS:
				return elements != null && !elements.isEmpty();
			case AquilaPackage.MODEL__USE_CASE_ELEMENTS:
				return useCaseElements != null && !useCaseElements.isEmpty();
			case AquilaPackage.MODEL__STATE_MACHINES:
				return stateMachines != null && !stateMachines.isEmpty();
			case AquilaPackage.MODEL__UUID:
				return UUID_EDEFAULT == null ? uuid != null : !UUID_EDEFAULT.equals(uuid);
			case AquilaPackage.MODEL__PACKAGE:
				return PACKAGE_EDEFAULT == null ? package_ != null : !PACKAGE_EDEFAULT.equals(package_);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (UUID: ");
		result.append(uuid);
		result.append(", package: ");
		result.append(package_);
		result.append(')');
		return result.toString();
	}

} //ModelImpl
