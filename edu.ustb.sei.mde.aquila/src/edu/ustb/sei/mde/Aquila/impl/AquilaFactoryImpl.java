/**
 */
package edu.ustb.sei.mde.Aquila.impl;

import edu.ustb.sei.mde.Aquila.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class AquilaFactoryImpl extends EFactoryImpl implements AquilaFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static AquilaFactory init() {
		try {
			AquilaFactory theAquilaFactory = (AquilaFactory)EPackage.Registry.INSTANCE.getEFactory(AquilaPackage.eNS_URI);
			if (theAquilaFactory != null) {
				return theAquilaFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new AquilaFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AquilaFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case AquilaPackage.MODEL: return createModel();
			case AquilaPackage.AQ_CLASS: return createAqClass();
			case AquilaPackage.AQ_PRIMITIVE_TYPE: return createAqPrimitiveType();
			case AquilaPackage.AQ_ENUM_TYPE: return createAqEnumType();
			case AquilaPackage.AQ_ATTRIBUTE: return createAqAttribute();
			case AquilaPackage.AQ_REFERENCE: return createAqReference();
			case AquilaPackage.AQ_ENUM_LITERAL: return createAqEnumLiteral();
			case AquilaPackage.AQ_OPERATION: return createAqOperation();
			case AquilaPackage.AQ_PARAMETER: return createAqParameter();
			case AquilaPackage.AQ_USE_CASE: return createAqUseCase();
			case AquilaPackage.AQ_ACTOR: return createAqActor();
			case AquilaPackage.AQ_STATE: return createAqState();
			case AquilaPackage.AQ_TRANSITION: return createAqTransition();
			case AquilaPackage.AQ_PSEUDO_STATE: return createAqPseudoState();
			case AquilaPackage.AQ_COMPOSITE_STATE: return createAqCompositeState();
			case AquilaPackage.AQ_STATE_MACHINE: return createAqStateMachine();
			case AquilaPackage.AQ_DEPENDENCY: return createAqDependency();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case AquilaPackage.AQ_PSEUDO_STATE_KIND:
				return createAqPseudoStateKindFromString(eDataType, initialValue);
			case AquilaPackage.AQ_DEPENDENCY_KIND:
				return createAqDependencyKindFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case AquilaPackage.AQ_PSEUDO_STATE_KIND:
				return convertAqPseudoStateKindToString(eDataType, instanceValue);
			case AquilaPackage.AQ_DEPENDENCY_KIND:
				return convertAqDependencyKindToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Model createModel() {
		ModelImpl model = new ModelImpl();
		return model;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AqClass createAqClass() {
		AqClassImpl aqClass = new AqClassImpl();
		return aqClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AqPrimitiveType createAqPrimitiveType() {
		AqPrimitiveTypeImpl aqPrimitiveType = new AqPrimitiveTypeImpl();
		return aqPrimitiveType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AqEnumType createAqEnumType() {
		AqEnumTypeImpl aqEnumType = new AqEnumTypeImpl();
		return aqEnumType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AqAttribute createAqAttribute() {
		AqAttributeImpl aqAttribute = new AqAttributeImpl();
		return aqAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AqReference createAqReference() {
		AqReferenceImpl aqReference = new AqReferenceImpl();
		return aqReference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AqEnumLiteral createAqEnumLiteral() {
		AqEnumLiteralImpl aqEnumLiteral = new AqEnumLiteralImpl();
		return aqEnumLiteral;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AqOperation createAqOperation() {
		AqOperationImpl aqOperation = new AqOperationImpl();
		return aqOperation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AqParameter createAqParameter() {
		AqParameterImpl aqParameter = new AqParameterImpl();
		return aqParameter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AqUseCase createAqUseCase() {
		AqUseCaseImpl aqUseCase = new AqUseCaseImpl();
		return aqUseCase;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AqActor createAqActor() {
		AqActorImpl aqActor = new AqActorImpl();
		return aqActor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AqState createAqState() {
		AqStateImpl aqState = new AqStateImpl();
		return aqState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AqTransition createAqTransition() {
		AqTransitionImpl aqTransition = new AqTransitionImpl();
		return aqTransition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AqPseudoState createAqPseudoState() {
		AqPseudoStateImpl aqPseudoState = new AqPseudoStateImpl();
		return aqPseudoState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AqCompositeState createAqCompositeState() {
		AqCompositeStateImpl aqCompositeState = new AqCompositeStateImpl();
		return aqCompositeState;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AqStateMachine createAqStateMachine() {
		AqStateMachineImpl aqStateMachine = new AqStateMachineImpl();
		return aqStateMachine;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AqDependency createAqDependency() {
		AqDependencyImpl aqDependency = new AqDependencyImpl();
		return aqDependency;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AqPseudoStateKind createAqPseudoStateKindFromString(EDataType eDataType, String initialValue) {
		AqPseudoStateKind result = AqPseudoStateKind.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertAqPseudoStateKindToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AqDependencyKind createAqDependencyKindFromString(EDataType eDataType, String initialValue) {
		AqDependencyKind result = AqDependencyKind.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertAqDependencyKindToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AquilaPackage getAquilaPackage() {
		return (AquilaPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static AquilaPackage getPackage() {
		return AquilaPackage.eINSTANCE;
	}

} //AquilaFactoryImpl
