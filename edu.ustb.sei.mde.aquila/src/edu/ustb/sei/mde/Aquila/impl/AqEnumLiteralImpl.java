/**
 */
package edu.ustb.sei.mde.Aquila.impl;

import edu.ustb.sei.mde.Aquila.AqEnumLiteral;
import edu.ustb.sei.mde.Aquila.AquilaPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Aq Enum Literal</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class AqEnumLiteralImpl extends NamedElementImpl implements AqEnumLiteral {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AqEnumLiteralImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AquilaPackage.Literals.AQ_ENUM_LITERAL;
	}

} //AqEnumLiteralImpl
