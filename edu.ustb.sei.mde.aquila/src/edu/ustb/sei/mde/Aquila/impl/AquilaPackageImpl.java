/**
 */
package edu.ustb.sei.mde.Aquila.impl;

import edu.ustb.sei.mde.Aquila.AqActor;
import edu.ustb.sei.mde.Aquila.AqAttribute;
import edu.ustb.sei.mde.Aquila.AqClass;
import edu.ustb.sei.mde.Aquila.AqClassifier;
import edu.ustb.sei.mde.Aquila.AqCompositeState;
import edu.ustb.sei.mde.Aquila.AqDataType;
import edu.ustb.sei.mde.Aquila.AqDependency;
import edu.ustb.sei.mde.Aquila.AqDependencyKind;
import edu.ustb.sei.mde.Aquila.AqEnumLiteral;
import edu.ustb.sei.mde.Aquila.AqEnumType;
import edu.ustb.sei.mde.Aquila.AqOperation;
import edu.ustb.sei.mde.Aquila.AqParameter;
import edu.ustb.sei.mde.Aquila.AqPrimitiveType;
import edu.ustb.sei.mde.Aquila.AqPseudoState;
import edu.ustb.sei.mde.Aquila.AqPseudoStateKind;
import edu.ustb.sei.mde.Aquila.AqReference;
import edu.ustb.sei.mde.Aquila.AqState;
import edu.ustb.sei.mde.Aquila.AqStateMachine;
import edu.ustb.sei.mde.Aquila.AqStateRegion;
import edu.ustb.sei.mde.Aquila.AqStructuralFeature;
import edu.ustb.sei.mde.Aquila.AqTransition;
import edu.ustb.sei.mde.Aquila.AqUseCase;
import edu.ustb.sei.mde.Aquila.AqUseCaseElement;
import edu.ustb.sei.mde.Aquila.AqVertex;
import edu.ustb.sei.mde.Aquila.AquilaFactory;
import edu.ustb.sei.mde.Aquila.AquilaPackage;
import edu.ustb.sei.mde.Aquila.Model;
import edu.ustb.sei.mde.Aquila.NamedElement;
import edu.ustb.sei.mde.Aquila.TypedElement;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class AquilaPackageImpl extends EPackageImpl implements AquilaPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass modelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aqUseCaseElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass namedElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aqClassifierEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aqClassEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aqDataTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aqPrimitiveTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aqEnumTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aqStructuralFeatureEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aqAttributeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aqReferenceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aqEnumLiteralEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aqOperationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aqParameterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass typedElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aqUseCaseEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aqActorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aqStateEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aqTransitionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aqVertexEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aqPseudoStateEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aqStateRegionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aqCompositeStateEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aqStateMachineEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aqDependencyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum aqPseudoStateKindEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum aqDependencyKindEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see edu.ustb.sei.mde.Aquila.AquilaPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private AquilaPackageImpl() {
		super(eNS_URI, AquilaFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link AquilaPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static AquilaPackage init() {
		if (isInited) return (AquilaPackage)EPackage.Registry.INSTANCE.getEPackage(AquilaPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredAquilaPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		AquilaPackageImpl theAquilaPackage = registeredAquilaPackage instanceof AquilaPackageImpl ? (AquilaPackageImpl)registeredAquilaPackage : new AquilaPackageImpl();

		isInited = true;

		// Create package meta-data objects
		theAquilaPackage.createPackageContents();

		// Initialize created meta-data
		theAquilaPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theAquilaPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(AquilaPackage.eNS_URI, theAquilaPackage);
		return theAquilaPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getModel() {
		return modelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getModel_Elements() {
		return (EReference)modelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getModel_UseCaseElements() {
		return (EReference)modelEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getModel_StateMachines() {
		return (EReference)modelEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getModel_UUID() {
		return (EAttribute)modelEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getModel_Package() {
		return (EAttribute)modelEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getAqUseCaseElement() {
		return aqUseCaseElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getNamedElement() {
		return namedElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getNamedElement_Name() {
		return (EAttribute)namedElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getAqClassifier() {
		return aqClassifierEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getAqClass() {
		return aqClassEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getAqClass_StructuralFeatures() {
		return (EReference)aqClassEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getAqClass_SuperTypes() {
		return (EReference)aqClassEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getAqClass_Interface() {
		return (EAttribute)aqClassEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getAqClass_Operations() {
		return (EReference)aqClassEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getAqClass_Dependencies() {
		return (EReference)aqClassEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getAqClass_Abstract() {
		return (EAttribute)aqClassEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getAqDataType() {
		return aqDataTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getAqPrimitiveType() {
		return aqPrimitiveTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getAqPrimitiveType_InstanceType() {
		return (EAttribute)aqPrimitiveTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getAqEnumType() {
		return aqEnumTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getAqEnumType_Literals() {
		return (EReference)aqEnumTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getAqStructuralFeature() {
		return aqStructuralFeatureEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getAqAttribute() {
		return aqAttributeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getAqAttribute_AttributeType() {
		return (EReference)aqAttributeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getAqReference() {
		return aqReferenceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getAqReference_Containment() {
		return (EAttribute)aqReferenceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getAqReference_ReferenceType() {
		return (EReference)aqReferenceEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getAqReference_Opposite() {
		return (EReference)aqReferenceEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EOperation getAqReference__IsMajor() {
		return aqReferenceEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getAqEnumLiteral() {
		return aqEnumLiteralEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getAqOperation() {
		return aqOperationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getAqOperation_ReturnType() {
		return (EReference)aqOperationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getAqOperation_Parameters() {
		return (EReference)aqOperationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getAqParameter() {
		return aqParameterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getAqParameter_ParameterType() {
		return (EReference)aqParameterEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getTypedElement() {
		return typedElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTypedElement_LowerBound() {
		return (EAttribute)typedElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getTypedElement_UpperBound() {
		return (EAttribute)typedElementEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getTypedElement_Type() {
		return (EReference)typedElementEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getAqUseCase() {
		return aqUseCaseEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getAqUseCase_Desciption() {
		return (EAttribute)aqUseCaseEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getAqUseCase_Actors() {
		return (EReference)aqUseCaseEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getAqUseCase_Includes() {
		return (EReference)aqUseCaseEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getAqUseCase_Extends() {
		return (EReference)aqUseCaseEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getAqActor() {
		return aqActorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getAqActor_UseCases() {
		return (EReference)aqActorEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getAqActor_SuperActors() {
		return (EReference)aqActorEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getAqState() {
		return aqStateEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getAqTransition() {
		return aqTransitionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getAqTransition_Target() {
		return (EReference)aqTransitionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getAqTransition_Trigger() {
		return (EAttribute)aqTransitionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getAqTransition_Guard() {
		return (EAttribute)aqTransitionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getAqVertex() {
		return aqVertexEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getAqVertex_Transitions() {
		return (EReference)aqVertexEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getAqPseudoState() {
		return aqPseudoStateEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getAqPseudoState_Kind() {
		return (EAttribute)aqPseudoStateEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getAqStateRegion() {
		return aqStateRegionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getAqStateRegion_Vertices() {
		return (EReference)aqStateRegionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getAqCompositeState() {
		return aqCompositeStateEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getAqStateMachine() {
		return aqStateMachineEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EClass getAqDependency() {
		return aqDependencyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EReference getAqDependency_Target() {
		return (EReference)aqDependencyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EAttribute getAqDependency_Kind() {
		return (EAttribute)aqDependencyEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getAqPseudoStateKind() {
		return aqPseudoStateKindEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EEnum getAqDependencyKind() {
		return aqDependencyKindEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public AquilaFactory getAquilaFactory() {
		return (AquilaFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		modelEClass = createEClass(MODEL);
		createEReference(modelEClass, MODEL__ELEMENTS);
		createEReference(modelEClass, MODEL__USE_CASE_ELEMENTS);
		createEReference(modelEClass, MODEL__STATE_MACHINES);
		createEAttribute(modelEClass, MODEL__UUID);
		createEAttribute(modelEClass, MODEL__PACKAGE);

		aqUseCaseElementEClass = createEClass(AQ_USE_CASE_ELEMENT);

		namedElementEClass = createEClass(NAMED_ELEMENT);
		createEAttribute(namedElementEClass, NAMED_ELEMENT__NAME);

		aqClassifierEClass = createEClass(AQ_CLASSIFIER);

		aqClassEClass = createEClass(AQ_CLASS);
		createEReference(aqClassEClass, AQ_CLASS__STRUCTURAL_FEATURES);
		createEReference(aqClassEClass, AQ_CLASS__SUPER_TYPES);
		createEAttribute(aqClassEClass, AQ_CLASS__INTERFACE);
		createEReference(aqClassEClass, AQ_CLASS__OPERATIONS);
		createEReference(aqClassEClass, AQ_CLASS__DEPENDENCIES);
		createEAttribute(aqClassEClass, AQ_CLASS__ABSTRACT);

		aqDataTypeEClass = createEClass(AQ_DATA_TYPE);

		aqPrimitiveTypeEClass = createEClass(AQ_PRIMITIVE_TYPE);
		createEAttribute(aqPrimitiveTypeEClass, AQ_PRIMITIVE_TYPE__INSTANCE_TYPE);

		aqEnumTypeEClass = createEClass(AQ_ENUM_TYPE);
		createEReference(aqEnumTypeEClass, AQ_ENUM_TYPE__LITERALS);

		aqStructuralFeatureEClass = createEClass(AQ_STRUCTURAL_FEATURE);

		aqAttributeEClass = createEClass(AQ_ATTRIBUTE);
		createEReference(aqAttributeEClass, AQ_ATTRIBUTE__ATTRIBUTE_TYPE);

		aqReferenceEClass = createEClass(AQ_REFERENCE);
		createEAttribute(aqReferenceEClass, AQ_REFERENCE__CONTAINMENT);
		createEReference(aqReferenceEClass, AQ_REFERENCE__REFERENCE_TYPE);
		createEReference(aqReferenceEClass, AQ_REFERENCE__OPPOSITE);
		createEOperation(aqReferenceEClass, AQ_REFERENCE___IS_MAJOR);

		aqEnumLiteralEClass = createEClass(AQ_ENUM_LITERAL);

		aqOperationEClass = createEClass(AQ_OPERATION);
		createEReference(aqOperationEClass, AQ_OPERATION__RETURN_TYPE);
		createEReference(aqOperationEClass, AQ_OPERATION__PARAMETERS);

		aqParameterEClass = createEClass(AQ_PARAMETER);
		createEReference(aqParameterEClass, AQ_PARAMETER__PARAMETER_TYPE);

		typedElementEClass = createEClass(TYPED_ELEMENT);
		createEAttribute(typedElementEClass, TYPED_ELEMENT__LOWER_BOUND);
		createEAttribute(typedElementEClass, TYPED_ELEMENT__UPPER_BOUND);
		createEReference(typedElementEClass, TYPED_ELEMENT__TYPE);

		aqUseCaseEClass = createEClass(AQ_USE_CASE);
		createEAttribute(aqUseCaseEClass, AQ_USE_CASE__DESCIPTION);
		createEReference(aqUseCaseEClass, AQ_USE_CASE__ACTORS);
		createEReference(aqUseCaseEClass, AQ_USE_CASE__INCLUDES);
		createEReference(aqUseCaseEClass, AQ_USE_CASE__EXTENDS);

		aqActorEClass = createEClass(AQ_ACTOR);
		createEReference(aqActorEClass, AQ_ACTOR__USE_CASES);
		createEReference(aqActorEClass, AQ_ACTOR__SUPER_ACTORS);

		aqStateEClass = createEClass(AQ_STATE);

		aqTransitionEClass = createEClass(AQ_TRANSITION);
		createEReference(aqTransitionEClass, AQ_TRANSITION__TARGET);
		createEAttribute(aqTransitionEClass, AQ_TRANSITION__TRIGGER);
		createEAttribute(aqTransitionEClass, AQ_TRANSITION__GUARD);

		aqVertexEClass = createEClass(AQ_VERTEX);
		createEReference(aqVertexEClass, AQ_VERTEX__TRANSITIONS);

		aqPseudoStateEClass = createEClass(AQ_PSEUDO_STATE);
		createEAttribute(aqPseudoStateEClass, AQ_PSEUDO_STATE__KIND);

		aqStateRegionEClass = createEClass(AQ_STATE_REGION);
		createEReference(aqStateRegionEClass, AQ_STATE_REGION__VERTICES);

		aqCompositeStateEClass = createEClass(AQ_COMPOSITE_STATE);

		aqStateMachineEClass = createEClass(AQ_STATE_MACHINE);

		aqDependencyEClass = createEClass(AQ_DEPENDENCY);
		createEReference(aqDependencyEClass, AQ_DEPENDENCY__TARGET);
		createEAttribute(aqDependencyEClass, AQ_DEPENDENCY__KIND);

		// Create enums
		aqPseudoStateKindEEnum = createEEnum(AQ_PSEUDO_STATE_KIND);
		aqDependencyKindEEnum = createEEnum(AQ_DEPENDENCY_KIND);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		aqUseCaseElementEClass.getESuperTypes().add(this.getNamedElement());
		aqClassifierEClass.getESuperTypes().add(this.getNamedElement());
		aqClassEClass.getESuperTypes().add(this.getAqClassifier());
		aqDataTypeEClass.getESuperTypes().add(this.getAqClassifier());
		aqPrimitiveTypeEClass.getESuperTypes().add(this.getAqDataType());
		aqEnumTypeEClass.getESuperTypes().add(this.getAqDataType());
		aqStructuralFeatureEClass.getESuperTypes().add(this.getNamedElement());
		aqStructuralFeatureEClass.getESuperTypes().add(this.getTypedElement());
		aqAttributeEClass.getESuperTypes().add(this.getAqStructuralFeature());
		aqReferenceEClass.getESuperTypes().add(this.getAqStructuralFeature());
		aqEnumLiteralEClass.getESuperTypes().add(this.getNamedElement());
		aqOperationEClass.getESuperTypes().add(this.getTypedElement());
		aqParameterEClass.getESuperTypes().add(this.getTypedElement());
		typedElementEClass.getESuperTypes().add(this.getNamedElement());
		aqUseCaseEClass.getESuperTypes().add(this.getAqUseCaseElement());
		aqActorEClass.getESuperTypes().add(this.getAqUseCaseElement());
		aqStateEClass.getESuperTypes().add(this.getNamedElement());
		aqStateEClass.getESuperTypes().add(this.getAqVertex());
		aqPseudoStateEClass.getESuperTypes().add(this.getAqVertex());
		aqCompositeStateEClass.getESuperTypes().add(this.getAqStateRegion());
		aqCompositeStateEClass.getESuperTypes().add(this.getAqState());
		aqStateMachineEClass.getESuperTypes().add(this.getAqStateRegion());
		aqStateMachineEClass.getESuperTypes().add(this.getNamedElement());

		// Initialize classes, features, and operations; add parameters
		initEClass(modelEClass, Model.class, "Model", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getModel_Elements(), this.getAqClassifier(), null, "elements", null, 0, -1, Model.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getModel_UseCaseElements(), this.getAqUseCaseElement(), null, "useCaseElements", null, 0, -1, Model.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getModel_StateMachines(), this.getAqStateMachine(), null, "stateMachines", null, 0, -1, Model.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getModel_UUID(), ecorePackage.getEString(), "UUID", null, 0, 1, Model.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getModel_Package(), ecorePackage.getEString(), "package", null, 0, 1, Model.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(aqUseCaseElementEClass, AqUseCaseElement.class, "AqUseCaseElement", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(namedElementEClass, NamedElement.class, "NamedElement", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getNamedElement_Name(), ecorePackage.getEString(), "name", null, 0, 1, NamedElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(aqClassifierEClass, AqClassifier.class, "AqClassifier", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(aqClassEClass, AqClass.class, "AqClass", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAqClass_StructuralFeatures(), this.getAqStructuralFeature(), null, "structuralFeatures", null, 0, -1, AqClass.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getAqClass_SuperTypes(), this.getAqClass(), null, "superTypes", null, 0, -1, AqClass.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getAqClass_Interface(), ecorePackage.getEBoolean(), "interface", "false", 1, 1, AqClass.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAqClass_Operations(), this.getAqOperation(), null, "operations", null, 0, -1, AqClass.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getAqClass_Dependencies(), this.getAqDependency(), null, "dependencies", null, 0, -1, AqClass.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getAqClass_Abstract(), ecorePackage.getEBoolean(), "abstract", "false", 0, 1, AqClass.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(aqDataTypeEClass, AqDataType.class, "AqDataType", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(aqPrimitiveTypeEClass, AqPrimitiveType.class, "AqPrimitiveType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAqPrimitiveType_InstanceType(), ecorePackage.getEString(), "instanceType", null, 0, 1, AqPrimitiveType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(aqEnumTypeEClass, AqEnumType.class, "AqEnumType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAqEnumType_Literals(), this.getAqEnumLiteral(), null, "literals", null, 0, -1, AqEnumType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(aqStructuralFeatureEClass, AqStructuralFeature.class, "AqStructuralFeature", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(aqAttributeEClass, AqAttribute.class, "AqAttribute", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAqAttribute_AttributeType(), this.getAqDataType(), null, "attributeType", null, 0, 1, AqAttribute.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(aqReferenceEClass, AqReference.class, "AqReference", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAqReference_Containment(), ecorePackage.getEBoolean(), "containment", null, 0, 1, AqReference.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAqReference_ReferenceType(), this.getAqClass(), null, "referenceType", null, 0, 1, AqReference.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAqReference_Opposite(), this.getAqReference(), null, "opposite", null, 0, 1, AqReference.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEOperation(getAqReference__IsMajor(), ecorePackage.getEBoolean(), "isMajor", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(aqEnumLiteralEClass, AqEnumLiteral.class, "AqEnumLiteral", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(aqOperationEClass, AqOperation.class, "AqOperation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAqOperation_ReturnType(), this.getAqClassifier(), null, "returnType", null, 0, 1, AqOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAqOperation_Parameters(), this.getAqParameter(), null, "parameters", null, 0, -1, AqOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(aqParameterEClass, AqParameter.class, "AqParameter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAqParameter_ParameterType(), this.getAqClassifier(), null, "parameterType", null, 0, 1, AqParameter.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(typedElementEClass, TypedElement.class, "TypedElement", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTypedElement_LowerBound(), ecorePackage.getEInt(), "lowerBound", null, 0, 1, TypedElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTypedElement_UpperBound(), ecorePackage.getEInt(), "upperBound", null, 0, 1, TypedElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTypedElement_Type(), this.getAqClassifier(), null, "type", null, 0, 1, TypedElement.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEClass(aqUseCaseEClass, AqUseCase.class, "AqUseCase", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAqUseCase_Desciption(), ecorePackage.getEString(), "desciption", null, 0, 1, AqUseCase.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAqUseCase_Actors(), this.getAqActor(), this.getAqActor_UseCases(), "actors", null, 0, -1, AqUseCase.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getAqUseCase_Includes(), this.getAqUseCase(), null, "includes", null, 0, -1, AqUseCase.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getAqUseCase_Extends(), this.getAqUseCase(), null, "extends", null, 0, -1, AqUseCase.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(aqActorEClass, AqActor.class, "AqActor", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAqActor_UseCases(), this.getAqUseCase(), this.getAqUseCase_Actors(), "useCases", null, 0, -1, AqActor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getAqActor_SuperActors(), this.getAqActor(), null, "superActors", null, 0, -1, AqActor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(aqStateEClass, AqState.class, "AqState", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(aqTransitionEClass, AqTransition.class, "AqTransition", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAqTransition_Target(), this.getAqVertex(), null, "target", null, 1, 1, AqTransition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAqTransition_Trigger(), ecorePackage.getEString(), "trigger", null, 0, 1, AqTransition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAqTransition_Guard(), ecorePackage.getEString(), "guard", null, 0, 1, AqTransition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(aqVertexEClass, AqVertex.class, "AqVertex", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAqVertex_Transitions(), this.getAqTransition(), null, "transitions", null, 0, -1, AqVertex.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(aqPseudoStateEClass, AqPseudoState.class, "AqPseudoState", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAqPseudoState_Kind(), this.getAqPseudoStateKind(), "kind", null, 1, 1, AqPseudoState.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(aqStateRegionEClass, AqStateRegion.class, "AqStateRegion", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAqStateRegion_Vertices(), this.getAqVertex(), null, "vertices", null, 0, -1, AqStateRegion.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(aqCompositeStateEClass, AqCompositeState.class, "AqCompositeState", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(aqStateMachineEClass, AqStateMachine.class, "AqStateMachine", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(aqDependencyEClass, AqDependency.class, "AqDependency", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAqDependency_Target(), this.getAqClass(), null, "target", null, 1, 1, AqDependency.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAqDependency_Kind(), this.getAqDependencyKind(), "kind", null, 0, 1, AqDependency.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(aqPseudoStateKindEEnum, AqPseudoStateKind.class, "AqPseudoStateKind");
		addEEnumLiteral(aqPseudoStateKindEEnum, AqPseudoStateKind.INITIAL);
		addEEnumLiteral(aqPseudoStateKindEEnum, AqPseudoStateKind.FINAL);
		addEEnumLiteral(aqPseudoStateKindEEnum, AqPseudoStateKind.ENTRY);
		addEEnumLiteral(aqPseudoStateKindEEnum, AqPseudoStateKind.EXIT);
		addEEnumLiteral(aqPseudoStateKindEEnum, AqPseudoStateKind.SHALLOW_HISTORY);
		addEEnumLiteral(aqPseudoStateKindEEnum, AqPseudoStateKind.DEEP_HISTORY);

		initEEnum(aqDependencyKindEEnum, AqDependencyKind.class, "AqDependencyKind");
		addEEnumLiteral(aqDependencyKindEEnum, AqDependencyKind.USE);
		addEEnumLiteral(aqDependencyKindEEnum, AqDependencyKind.CALL);

		// Create resource
		createResource(eNS_URI);
	}

} //AquilaPackageImpl
