/**
 */
package edu.ustb.sei.mde.Aquila.impl;

import edu.ustb.sei.mde.Aquila.AqActor;
import edu.ustb.sei.mde.Aquila.AqUseCase;
import edu.ustb.sei.mde.Aquila.AquilaPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Aq Actor</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.Aquila.impl.AqActorImpl#getUseCases <em>Use Cases</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.Aquila.impl.AqActorImpl#getSuperActors <em>Super Actors</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AqActorImpl extends AqUseCaseElementImpl implements AqActor {
	/**
	 * The cached value of the '{@link #getUseCases() <em>Use Cases</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUseCases()
	 * @generated
	 * @ordered
	 */
	protected EList<AqUseCase> useCases;

	/**
	 * The cached value of the '{@link #getSuperActors() <em>Super Actors</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSuperActors()
	 * @generated
	 * @ordered
	 */
	protected EList<AqActor> superActors;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AqActorImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AquilaPackage.Literals.AQ_ACTOR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<AqUseCase> getUseCases() {
		if (useCases == null) {
			useCases = new EObjectWithInverseResolvingEList.ManyInverse<AqUseCase>(AqUseCase.class, this, AquilaPackage.AQ_ACTOR__USE_CASES, AquilaPackage.AQ_USE_CASE__ACTORS);
		}
		return useCases;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<AqActor> getSuperActors() {
		if (superActors == null) {
			superActors = new EObjectResolvingEList<AqActor>(AqActor.class, this, AquilaPackage.AQ_ACTOR__SUPER_ACTORS);
		}
		return superActors;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AquilaPackage.AQ_ACTOR__USE_CASES:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getUseCases()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AquilaPackage.AQ_ACTOR__USE_CASES:
				return ((InternalEList<?>)getUseCases()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AquilaPackage.AQ_ACTOR__USE_CASES:
				return getUseCases();
			case AquilaPackage.AQ_ACTOR__SUPER_ACTORS:
				return getSuperActors();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AquilaPackage.AQ_ACTOR__USE_CASES:
				getUseCases().clear();
				getUseCases().addAll((Collection<? extends AqUseCase>)newValue);
				return;
			case AquilaPackage.AQ_ACTOR__SUPER_ACTORS:
				getSuperActors().clear();
				getSuperActors().addAll((Collection<? extends AqActor>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AquilaPackage.AQ_ACTOR__USE_CASES:
				getUseCases().clear();
				return;
			case AquilaPackage.AQ_ACTOR__SUPER_ACTORS:
				getSuperActors().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AquilaPackage.AQ_ACTOR__USE_CASES:
				return useCases != null && !useCases.isEmpty();
			case AquilaPackage.AQ_ACTOR__SUPER_ACTORS:
				return superActors != null && !superActors.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //AqActorImpl
