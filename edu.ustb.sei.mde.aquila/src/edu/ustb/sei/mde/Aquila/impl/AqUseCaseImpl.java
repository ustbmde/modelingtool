/**
 */
package edu.ustb.sei.mde.Aquila.impl;

import edu.ustb.sei.mde.Aquila.AqActor;
import edu.ustb.sei.mde.Aquila.AqUseCase;
import edu.ustb.sei.mde.Aquila.AquilaPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Aq Use Case</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.Aquila.impl.AqUseCaseImpl#getDesciption <em>Desciption</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.Aquila.impl.AqUseCaseImpl#getActors <em>Actors</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.Aquila.impl.AqUseCaseImpl#getIncludes <em>Includes</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.Aquila.impl.AqUseCaseImpl#getExtends <em>Extends</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AqUseCaseImpl extends AqUseCaseElementImpl implements AqUseCase {
	/**
	 * The default value of the '{@link #getDesciption() <em>Desciption</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDesciption()
	 * @generated
	 * @ordered
	 */
	protected static final String DESCIPTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDesciption() <em>Desciption</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDesciption()
	 * @generated
	 * @ordered
	 */
	protected String desciption = DESCIPTION_EDEFAULT;

	/**
	 * The cached value of the '{@link #getActors() <em>Actors</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActors()
	 * @generated
	 * @ordered
	 */
	protected EList<AqActor> actors;

	/**
	 * The cached value of the '{@link #getIncludes() <em>Includes</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIncludes()
	 * @generated
	 * @ordered
	 */
	protected EList<AqUseCase> includes;

	/**
	 * The cached value of the '{@link #getExtends() <em>Extends</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExtends()
	 * @generated
	 * @ordered
	 */
	protected EList<AqUseCase> extends_;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AqUseCaseImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AquilaPackage.Literals.AQ_USE_CASE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getDesciption() {
		return desciption;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setDesciption(String newDesciption) {
		String oldDesciption = desciption;
		desciption = newDesciption;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AquilaPackage.AQ_USE_CASE__DESCIPTION, oldDesciption, desciption));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<AqActor> getActors() {
		if (actors == null) {
			actors = new EObjectWithInverseResolvingEList.ManyInverse<AqActor>(AqActor.class, this, AquilaPackage.AQ_USE_CASE__ACTORS, AquilaPackage.AQ_ACTOR__USE_CASES);
		}
		return actors;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<AqUseCase> getIncludes() {
		if (includes == null) {
			includes = new EObjectResolvingEList<AqUseCase>(AqUseCase.class, this, AquilaPackage.AQ_USE_CASE__INCLUDES);
		}
		return includes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<AqUseCase> getExtends() {
		if (extends_ == null) {
			extends_ = new EObjectResolvingEList<AqUseCase>(AqUseCase.class, this, AquilaPackage.AQ_USE_CASE__EXTENDS);
		}
		return extends_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AquilaPackage.AQ_USE_CASE__ACTORS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getActors()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case AquilaPackage.AQ_USE_CASE__ACTORS:
				return ((InternalEList<?>)getActors()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case AquilaPackage.AQ_USE_CASE__DESCIPTION:
				return getDesciption();
			case AquilaPackage.AQ_USE_CASE__ACTORS:
				return getActors();
			case AquilaPackage.AQ_USE_CASE__INCLUDES:
				return getIncludes();
			case AquilaPackage.AQ_USE_CASE__EXTENDS:
				return getExtends();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case AquilaPackage.AQ_USE_CASE__DESCIPTION:
				setDesciption((String)newValue);
				return;
			case AquilaPackage.AQ_USE_CASE__ACTORS:
				getActors().clear();
				getActors().addAll((Collection<? extends AqActor>)newValue);
				return;
			case AquilaPackage.AQ_USE_CASE__INCLUDES:
				getIncludes().clear();
				getIncludes().addAll((Collection<? extends AqUseCase>)newValue);
				return;
			case AquilaPackage.AQ_USE_CASE__EXTENDS:
				getExtends().clear();
				getExtends().addAll((Collection<? extends AqUseCase>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case AquilaPackage.AQ_USE_CASE__DESCIPTION:
				setDesciption(DESCIPTION_EDEFAULT);
				return;
			case AquilaPackage.AQ_USE_CASE__ACTORS:
				getActors().clear();
				return;
			case AquilaPackage.AQ_USE_CASE__INCLUDES:
				getIncludes().clear();
				return;
			case AquilaPackage.AQ_USE_CASE__EXTENDS:
				getExtends().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case AquilaPackage.AQ_USE_CASE__DESCIPTION:
				return DESCIPTION_EDEFAULT == null ? desciption != null : !DESCIPTION_EDEFAULT.equals(desciption);
			case AquilaPackage.AQ_USE_CASE__ACTORS:
				return actors != null && !actors.isEmpty();
			case AquilaPackage.AQ_USE_CASE__INCLUDES:
				return includes != null && !includes.isEmpty();
			case AquilaPackage.AQ_USE_CASE__EXTENDS:
				return extends_ != null && !extends_.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (desciption: ");
		result.append(desciption);
		result.append(')');
		return result.toString();
	}

} //AqUseCaseImpl
