/**
 */
package edu.ustb.sei.mde.Aquila;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see edu.ustb.sei.mde.Aquila.AquilaPackage
 * @generated
 */
public interface AquilaFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	AquilaFactory eINSTANCE = edu.ustb.sei.mde.Aquila.impl.AquilaFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Model</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Model</em>'.
	 * @generated
	 */
	Model createModel();

	/**
	 * Returns a new object of class '<em>Aq Class</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Aq Class</em>'.
	 * @generated
	 */
	AqClass createAqClass();

	/**
	 * Returns a new object of class '<em>Aq Primitive Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Aq Primitive Type</em>'.
	 * @generated
	 */
	AqPrimitiveType createAqPrimitiveType();

	/**
	 * Returns a new object of class '<em>Aq Enum Type</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Aq Enum Type</em>'.
	 * @generated
	 */
	AqEnumType createAqEnumType();

	/**
	 * Returns a new object of class '<em>Aq Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Aq Attribute</em>'.
	 * @generated
	 */
	AqAttribute createAqAttribute();

	/**
	 * Returns a new object of class '<em>Aq Reference</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Aq Reference</em>'.
	 * @generated
	 */
	AqReference createAqReference();

	/**
	 * Returns a new object of class '<em>Aq Enum Literal</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Aq Enum Literal</em>'.
	 * @generated
	 */
	AqEnumLiteral createAqEnumLiteral();

	/**
	 * Returns a new object of class '<em>Aq Operation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Aq Operation</em>'.
	 * @generated
	 */
	AqOperation createAqOperation();

	/**
	 * Returns a new object of class '<em>Aq Parameter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Aq Parameter</em>'.
	 * @generated
	 */
	AqParameter createAqParameter();

	/**
	 * Returns a new object of class '<em>Aq Use Case</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Aq Use Case</em>'.
	 * @generated
	 */
	AqUseCase createAqUseCase();

	/**
	 * Returns a new object of class '<em>Aq Actor</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Aq Actor</em>'.
	 * @generated
	 */
	AqActor createAqActor();

	/**
	 * Returns a new object of class '<em>Aq State</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Aq State</em>'.
	 * @generated
	 */
	AqState createAqState();

	/**
	 * Returns a new object of class '<em>Aq Transition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Aq Transition</em>'.
	 * @generated
	 */
	AqTransition createAqTransition();

	/**
	 * Returns a new object of class '<em>Aq Pseudo State</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Aq Pseudo State</em>'.
	 * @generated
	 */
	AqPseudoState createAqPseudoState();

	/**
	 * Returns a new object of class '<em>Aq Composite State</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Aq Composite State</em>'.
	 * @generated
	 */
	AqCompositeState createAqCompositeState();

	/**
	 * Returns a new object of class '<em>Aq State Machine</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Aq State Machine</em>'.
	 * @generated
	 */
	AqStateMachine createAqStateMachine();

	/**
	 * Returns a new object of class '<em>Aq Dependency</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Aq Dependency</em>'.
	 * @generated
	 */
	AqDependency createAqDependency();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	AquilaPackage getAquilaPackage();

} //AquilaFactory
