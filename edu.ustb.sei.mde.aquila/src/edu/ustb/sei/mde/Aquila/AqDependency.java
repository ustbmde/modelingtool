/**
 */
package edu.ustb.sei.mde.Aquila;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Aq Dependency</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.Aquila.AqDependency#getTarget <em>Target</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.Aquila.AqDependency#getKind <em>Kind</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.mde.Aquila.AquilaPackage#getAqDependency()
 * @model
 * @generated
 */
public interface AqDependency extends EObject {
	/**
	 * Returns the value of the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target</em>' reference.
	 * @see #setTarget(AqClass)
	 * @see edu.ustb.sei.mde.Aquila.AquilaPackage#getAqDependency_Target()
	 * @model required="true"
	 * @generated
	 */
	AqClass getTarget();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.Aquila.AqDependency#getTarget <em>Target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target</em>' reference.
	 * @see #getTarget()
	 * @generated
	 */
	void setTarget(AqClass value);

	/**
	 * Returns the value of the '<em><b>Kind</b></em>' attribute.
	 * The literals are from the enumeration {@link edu.ustb.sei.mde.Aquila.AqDependencyKind}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Kind</em>' attribute.
	 * @see edu.ustb.sei.mde.Aquila.AqDependencyKind
	 * @see #setKind(AqDependencyKind)
	 * @see edu.ustb.sei.mde.Aquila.AquilaPackage#getAqDependency_Kind()
	 * @model
	 * @generated
	 */
	AqDependencyKind getKind();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.Aquila.AqDependency#getKind <em>Kind</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Kind</em>' attribute.
	 * @see edu.ustb.sei.mde.Aquila.AqDependencyKind
	 * @see #getKind()
	 * @generated
	 */
	void setKind(AqDependencyKind value);

} // AqDependency
