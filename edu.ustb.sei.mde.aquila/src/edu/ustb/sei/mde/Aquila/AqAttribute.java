/**
 */
package edu.ustb.sei.mde.Aquila;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Aq Attribute</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.Aquila.AqAttribute#getAttributeType <em>Attribute Type</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.mde.Aquila.AquilaPackage#getAqAttribute()
 * @model
 * @generated
 */
public interface AqAttribute extends AqStructuralFeature {
	/**
	 * Returns the value of the '<em><b>Attribute Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attribute Type</em>' reference.
	 * @see #setAttributeType(AqDataType)
	 * @see edu.ustb.sei.mde.Aquila.AquilaPackage#getAqAttribute_AttributeType()
	 * @model
	 * @generated
	 */
	AqDataType getAttributeType();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.Aquila.AqAttribute#getAttributeType <em>Attribute Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Attribute Type</em>' reference.
	 * @see #getAttributeType()
	 * @generated
	 */
	void setAttributeType(AqDataType value);

} // AqAttribute
