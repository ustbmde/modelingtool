/**
 */
package edu.ustb.sei.mde.Aquila;

import org.eclipse.emf.common.util.EList;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Aq Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.Aquila.AqOperation#getReturnType <em>Return Type</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.Aquila.AqOperation#getParameters <em>Parameters</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.mde.Aquila.AquilaPackage#getAqOperation()
 * @model
 * @generated
 */
public interface AqOperation extends TypedElement {
	/**
	 * Returns the value of the '<em><b>Return Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Return Type</em>' reference.
	 * @see #setReturnType(AqClassifier)
	 * @see edu.ustb.sei.mde.Aquila.AquilaPackage#getAqOperation_ReturnType()
	 * @model
	 * @generated
	 */
	AqClassifier getReturnType();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.Aquila.AqOperation#getReturnType <em>Return Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Return Type</em>' reference.
	 * @see #getReturnType()
	 * @generated
	 */
	void setReturnType(AqClassifier value);

	/**
	 * Returns the value of the '<em><b>Parameters</b></em>' containment reference list.
	 * The list contents are of type {@link edu.ustb.sei.mde.Aquila.AqParameter}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parameters</em>' containment reference list.
	 * @see edu.ustb.sei.mde.Aquila.AquilaPackage#getAqOperation_Parameters()
	 * @model containment="true"
	 * @generated
	 */
	EList<AqParameter> getParameters();

} // AqOperation
