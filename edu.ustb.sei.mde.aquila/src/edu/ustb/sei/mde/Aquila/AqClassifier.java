/**
 */
package edu.ustb.sei.mde.Aquila;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Aq Classifier</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.ustb.sei.mde.Aquila.AquilaPackage#getAqClassifier()
 * @model abstract="true"
 * @generated
 */
public interface AqClassifier extends NamedElement {
} // AqClassifier
