/**
 */
package edu.ustb.sei.mde.Aquila;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Aq Pseudo State</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.Aquila.AqPseudoState#getKind <em>Kind</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.mde.Aquila.AquilaPackage#getAqPseudoState()
 * @model
 * @generated
 */
public interface AqPseudoState extends AqVertex {
	/**
	 * Returns the value of the '<em><b>Kind</b></em>' attribute.
	 * The literals are from the enumeration {@link edu.ustb.sei.mde.Aquila.AqPseudoStateKind}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Kind</em>' attribute.
	 * @see edu.ustb.sei.mde.Aquila.AqPseudoStateKind
	 * @see #isSetKind()
	 * @see #unsetKind()
	 * @see #setKind(AqPseudoStateKind)
	 * @see edu.ustb.sei.mde.Aquila.AquilaPackage#getAqPseudoState_Kind()
	 * @model unsettable="true" required="true"
	 * @generated
	 */
	AqPseudoStateKind getKind();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.Aquila.AqPseudoState#getKind <em>Kind</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Kind</em>' attribute.
	 * @see edu.ustb.sei.mde.Aquila.AqPseudoStateKind
	 * @see #isSetKind()
	 * @see #unsetKind()
	 * @see #getKind()
	 * @generated
	 */
	void setKind(AqPseudoStateKind value);

	/**
	 * Unsets the value of the '{@link edu.ustb.sei.mde.Aquila.AqPseudoState#getKind <em>Kind</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetKind()
	 * @see #getKind()
	 * @see #setKind(AqPseudoStateKind)
	 * @generated
	 */
	void unsetKind();

	/**
	 * Returns whether the value of the '{@link edu.ustb.sei.mde.Aquila.AqPseudoState#getKind <em>Kind</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Kind</em>' attribute is set.
	 * @see #unsetKind()
	 * @see #getKind()
	 * @see #setKind(AqPseudoStateKind)
	 * @generated
	 */
	boolean isSetKind();

} // AqPseudoState
