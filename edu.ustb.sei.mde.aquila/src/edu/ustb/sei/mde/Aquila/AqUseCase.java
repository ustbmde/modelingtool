/**
 */
package edu.ustb.sei.mde.Aquila;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Aq Use Case</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.Aquila.AqUseCase#getDesciption <em>Desciption</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.Aquila.AqUseCase#getActors <em>Actors</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.Aquila.AqUseCase#getIncludes <em>Includes</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.Aquila.AqUseCase#getExtends <em>Extends</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.mde.Aquila.AquilaPackage#getAqUseCase()
 * @model
 * @generated
 */
public interface AqUseCase extends AqUseCaseElement {
	/**
	 * Returns the value of the '<em><b>Desciption</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Desciption</em>' attribute.
	 * @see #setDesciption(String)
	 * @see edu.ustb.sei.mde.Aquila.AquilaPackage#getAqUseCase_Desciption()
	 * @model
	 * @generated
	 */
	String getDesciption();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.Aquila.AqUseCase#getDesciption <em>Desciption</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Desciption</em>' attribute.
	 * @see #getDesciption()
	 * @generated
	 */
	void setDesciption(String value);

	/**
	 * Returns the value of the '<em><b>Actors</b></em>' reference list.
	 * The list contents are of type {@link edu.ustb.sei.mde.Aquila.AqActor}.
	 * It is bidirectional and its opposite is '{@link edu.ustb.sei.mde.Aquila.AqActor#getUseCases <em>Use Cases</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Actors</em>' reference list.
	 * @see edu.ustb.sei.mde.Aquila.AquilaPackage#getAqUseCase_Actors()
	 * @see edu.ustb.sei.mde.Aquila.AqActor#getUseCases
	 * @model opposite="useCases" ordered="false"
	 * @generated
	 */
	EList<AqActor> getActors();

	/**
	 * Returns the value of the '<em><b>Includes</b></em>' reference list.
	 * The list contents are of type {@link edu.ustb.sei.mde.Aquila.AqUseCase}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Includes</em>' reference list.
	 * @see edu.ustb.sei.mde.Aquila.AquilaPackage#getAqUseCase_Includes()
	 * @model ordered="false"
	 * @generated
	 */
	EList<AqUseCase> getIncludes();

	/**
	 * Returns the value of the '<em><b>Extends</b></em>' reference list.
	 * The list contents are of type {@link edu.ustb.sei.mde.Aquila.AqUseCase}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Extends</em>' reference list.
	 * @see edu.ustb.sei.mde.Aquila.AquilaPackage#getAqUseCase_Extends()
	 * @model ordered="false"
	 * @generated
	 */
	EList<AqUseCase> getExtends();

} // AqUseCase
