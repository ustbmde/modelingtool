/**
 */
package edu.ustb.sei.mde.Aquila;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Aq Enum Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.Aquila.AqEnumType#getLiterals <em>Literals</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.mde.Aquila.AquilaPackage#getAqEnumType()
 * @model
 * @generated
 */
public interface AqEnumType extends AqDataType {
	/**
	 * Returns the value of the '<em><b>Literals</b></em>' containment reference list.
	 * The list contents are of type {@link edu.ustb.sei.mde.Aquila.AqEnumLiteral}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Literals</em>' containment reference list.
	 * @see edu.ustb.sei.mde.Aquila.AquilaPackage#getAqEnumType_Literals()
	 * @model containment="true"
	 * @generated
	 */
	EList<AqEnumLiteral> getLiterals();

} // AqEnumType
