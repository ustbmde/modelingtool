/**
 */
package edu.ustb.sei.mde.Aquila;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Aq Enum Literal</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.ustb.sei.mde.Aquila.AquilaPackage#getAqEnumLiteral()
 * @model
 * @generated
 */
public interface AqEnumLiteral extends NamedElement {
} // AqEnumLiteral
