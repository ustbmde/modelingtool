/**
 */
package edu.ustb.sei.mde.Aquila;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.Aquila.Model#getElements <em>Elements</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.Aquila.Model#getUseCaseElements <em>Use Case Elements</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.Aquila.Model#getStateMachines <em>State Machines</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.Aquila.Model#getUUID <em>UUID</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.Aquila.Model#getPackage <em>Package</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.mde.Aquila.AquilaPackage#getModel()
 * @model
 * @generated
 */
public interface Model extends EObject {
	/**
	 * Returns the value of the '<em><b>Elements</b></em>' containment reference list.
	 * The list contents are of type {@link edu.ustb.sei.mde.Aquila.AqClassifier}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Elements</em>' containment reference list.
	 * @see edu.ustb.sei.mde.Aquila.AquilaPackage#getModel_Elements()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<AqClassifier> getElements();

	/**
	 * Returns the value of the '<em><b>Use Case Elements</b></em>' containment reference list.
	 * The list contents are of type {@link edu.ustb.sei.mde.Aquila.AqUseCaseElement}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Use Case Elements</em>' containment reference list.
	 * @see edu.ustb.sei.mde.Aquila.AquilaPackage#getModel_UseCaseElements()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<AqUseCaseElement> getUseCaseElements();

	/**
	 * Returns the value of the '<em><b>State Machines</b></em>' containment reference list.
	 * The list contents are of type {@link edu.ustb.sei.mde.Aquila.AqStateMachine}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>State Machines</em>' containment reference list.
	 * @see edu.ustb.sei.mde.Aquila.AquilaPackage#getModel_StateMachines()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<AqStateMachine> getStateMachines();

	/**
	 * Returns the value of the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>UUID</em>' attribute.
	 * @see #setUUID(String)
	 * @see edu.ustb.sei.mde.Aquila.AquilaPackage#getModel_UUID()
	 * @model
	 * @generated
	 */
	String getUUID();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.Aquila.Model#getUUID <em>UUID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>UUID</em>' attribute.
	 * @see #getUUID()
	 * @generated
	 */
	void setUUID(String value);

	/**
	 * Returns the value of the '<em><b>Package</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Package</em>' attribute.
	 * @see #setPackage(String)
	 * @see edu.ustb.sei.mde.Aquila.AquilaPackage#getModel_Package()
	 * @model
	 * @generated
	 */
	String getPackage();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.Aquila.Model#getPackage <em>Package</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Package</em>' attribute.
	 * @see #getPackage()
	 * @generated
	 */
	void setPackage(String value);

} // Model
