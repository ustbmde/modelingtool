/**
 */
package edu.ustb.sei.mde.Aquila;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see edu.ustb.sei.mde.Aquila.AquilaFactory
 * @model kind="package"
 * @generated
 */
public interface AquilaPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "Aquila";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.ustb.edu.cn/sei/mde/Aquila";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "Aquila";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	AquilaPackage eINSTANCE = edu.ustb.sei.mde.Aquila.impl.AquilaPackageImpl.init();

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.Aquila.impl.ModelImpl <em>Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.Aquila.impl.ModelImpl
	 * @see edu.ustb.sei.mde.Aquila.impl.AquilaPackageImpl#getModel()
	 * @generated
	 */
	int MODEL = 0;

	/**
	 * The feature id for the '<em><b>Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL__ELEMENTS = 0;

	/**
	 * The feature id for the '<em><b>Use Case Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL__USE_CASE_ELEMENTS = 1;

	/**
	 * The feature id for the '<em><b>State Machines</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL__STATE_MACHINES = 2;

	/**
	 * The feature id for the '<em><b>UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL__UUID = 3;

	/**
	 * The feature id for the '<em><b>Package</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL__PACKAGE = 4;

	/**
	 * The number of structural features of the '<em>Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_FEATURE_COUNT = 5;

	/**
	 * The number of operations of the '<em>Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.Aquila.impl.NamedElementImpl <em>Named Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.Aquila.impl.NamedElementImpl
	 * @see edu.ustb.sei.mde.Aquila.impl.AquilaPackageImpl#getNamedElement()
	 * @generated
	 */
	int NAMED_ELEMENT = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ELEMENT__NAME = 0;

	/**
	 * The number of structural features of the '<em>Named Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ELEMENT_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Named Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ELEMENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.Aquila.impl.AqUseCaseElementImpl <em>Aq Use Case Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.Aquila.impl.AqUseCaseElementImpl
	 * @see edu.ustb.sei.mde.Aquila.impl.AquilaPackageImpl#getAqUseCaseElement()
	 * @generated
	 */
	int AQ_USE_CASE_ELEMENT = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_USE_CASE_ELEMENT__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The number of structural features of the '<em>Aq Use Case Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_USE_CASE_ELEMENT_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Aq Use Case Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_USE_CASE_ELEMENT_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.Aquila.impl.AqClassifierImpl <em>Aq Classifier</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.Aquila.impl.AqClassifierImpl
	 * @see edu.ustb.sei.mde.Aquila.impl.AquilaPackageImpl#getAqClassifier()
	 * @generated
	 */
	int AQ_CLASSIFIER = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_CLASSIFIER__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The number of structural features of the '<em>Aq Classifier</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_CLASSIFIER_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Aq Classifier</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_CLASSIFIER_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.Aquila.impl.AqClassImpl <em>Aq Class</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.Aquila.impl.AqClassImpl
	 * @see edu.ustb.sei.mde.Aquila.impl.AquilaPackageImpl#getAqClass()
	 * @generated
	 */
	int AQ_CLASS = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_CLASS__NAME = AQ_CLASSIFIER__NAME;

	/**
	 * The feature id for the '<em><b>Structural Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_CLASS__STRUCTURAL_FEATURES = AQ_CLASSIFIER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Super Types</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_CLASS__SUPER_TYPES = AQ_CLASSIFIER_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Interface</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_CLASS__INTERFACE = AQ_CLASSIFIER_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Operations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_CLASS__OPERATIONS = AQ_CLASSIFIER_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Dependencies</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_CLASS__DEPENDENCIES = AQ_CLASSIFIER_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Abstract</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_CLASS__ABSTRACT = AQ_CLASSIFIER_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>Aq Class</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_CLASS_FEATURE_COUNT = AQ_CLASSIFIER_FEATURE_COUNT + 6;

	/**
	 * The number of operations of the '<em>Aq Class</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_CLASS_OPERATION_COUNT = AQ_CLASSIFIER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.Aquila.impl.AqDataTypeImpl <em>Aq Data Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.Aquila.impl.AqDataTypeImpl
	 * @see edu.ustb.sei.mde.Aquila.impl.AquilaPackageImpl#getAqDataType()
	 * @generated
	 */
	int AQ_DATA_TYPE = 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_DATA_TYPE__NAME = AQ_CLASSIFIER__NAME;

	/**
	 * The number of structural features of the '<em>Aq Data Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_DATA_TYPE_FEATURE_COUNT = AQ_CLASSIFIER_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Aq Data Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_DATA_TYPE_OPERATION_COUNT = AQ_CLASSIFIER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.Aquila.impl.AqPrimitiveTypeImpl <em>Aq Primitive Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.Aquila.impl.AqPrimitiveTypeImpl
	 * @see edu.ustb.sei.mde.Aquila.impl.AquilaPackageImpl#getAqPrimitiveType()
	 * @generated
	 */
	int AQ_PRIMITIVE_TYPE = 6;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_PRIMITIVE_TYPE__NAME = AQ_DATA_TYPE__NAME;

	/**
	 * The feature id for the '<em><b>Instance Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_PRIMITIVE_TYPE__INSTANCE_TYPE = AQ_DATA_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Aq Primitive Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_PRIMITIVE_TYPE_FEATURE_COUNT = AQ_DATA_TYPE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Aq Primitive Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_PRIMITIVE_TYPE_OPERATION_COUNT = AQ_DATA_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.Aquila.impl.AqEnumTypeImpl <em>Aq Enum Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.Aquila.impl.AqEnumTypeImpl
	 * @see edu.ustb.sei.mde.Aquila.impl.AquilaPackageImpl#getAqEnumType()
	 * @generated
	 */
	int AQ_ENUM_TYPE = 7;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_ENUM_TYPE__NAME = AQ_DATA_TYPE__NAME;

	/**
	 * The feature id for the '<em><b>Literals</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_ENUM_TYPE__LITERALS = AQ_DATA_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Aq Enum Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_ENUM_TYPE_FEATURE_COUNT = AQ_DATA_TYPE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Aq Enum Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_ENUM_TYPE_OPERATION_COUNT = AQ_DATA_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.Aquila.impl.AqStructuralFeatureImpl <em>Aq Structural Feature</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.Aquila.impl.AqStructuralFeatureImpl
	 * @see edu.ustb.sei.mde.Aquila.impl.AquilaPackageImpl#getAqStructuralFeature()
	 * @generated
	 */
	int AQ_STRUCTURAL_FEATURE = 8;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_STRUCTURAL_FEATURE__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Lower Bound</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_STRUCTURAL_FEATURE__LOWER_BOUND = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Upper Bound</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_STRUCTURAL_FEATURE__UPPER_BOUND = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_STRUCTURAL_FEATURE__TYPE = NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Aq Structural Feature</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_STRUCTURAL_FEATURE_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Aq Structural Feature</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_STRUCTURAL_FEATURE_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.Aquila.impl.AqAttributeImpl <em>Aq Attribute</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.Aquila.impl.AqAttributeImpl
	 * @see edu.ustb.sei.mde.Aquila.impl.AquilaPackageImpl#getAqAttribute()
	 * @generated
	 */
	int AQ_ATTRIBUTE = 9;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_ATTRIBUTE__NAME = AQ_STRUCTURAL_FEATURE__NAME;

	/**
	 * The feature id for the '<em><b>Lower Bound</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_ATTRIBUTE__LOWER_BOUND = AQ_STRUCTURAL_FEATURE__LOWER_BOUND;

	/**
	 * The feature id for the '<em><b>Upper Bound</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_ATTRIBUTE__UPPER_BOUND = AQ_STRUCTURAL_FEATURE__UPPER_BOUND;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_ATTRIBUTE__TYPE = AQ_STRUCTURAL_FEATURE__TYPE;

	/**
	 * The feature id for the '<em><b>Attribute Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_ATTRIBUTE__ATTRIBUTE_TYPE = AQ_STRUCTURAL_FEATURE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Aq Attribute</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_ATTRIBUTE_FEATURE_COUNT = AQ_STRUCTURAL_FEATURE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Aq Attribute</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_ATTRIBUTE_OPERATION_COUNT = AQ_STRUCTURAL_FEATURE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.Aquila.impl.AqReferenceImpl <em>Aq Reference</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.Aquila.impl.AqReferenceImpl
	 * @see edu.ustb.sei.mde.Aquila.impl.AquilaPackageImpl#getAqReference()
	 * @generated
	 */
	int AQ_REFERENCE = 10;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_REFERENCE__NAME = AQ_STRUCTURAL_FEATURE__NAME;

	/**
	 * The feature id for the '<em><b>Lower Bound</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_REFERENCE__LOWER_BOUND = AQ_STRUCTURAL_FEATURE__LOWER_BOUND;

	/**
	 * The feature id for the '<em><b>Upper Bound</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_REFERENCE__UPPER_BOUND = AQ_STRUCTURAL_FEATURE__UPPER_BOUND;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_REFERENCE__TYPE = AQ_STRUCTURAL_FEATURE__TYPE;

	/**
	 * The feature id for the '<em><b>Containment</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_REFERENCE__CONTAINMENT = AQ_STRUCTURAL_FEATURE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Reference Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_REFERENCE__REFERENCE_TYPE = AQ_STRUCTURAL_FEATURE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Opposite</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_REFERENCE__OPPOSITE = AQ_STRUCTURAL_FEATURE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Aq Reference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_REFERENCE_FEATURE_COUNT = AQ_STRUCTURAL_FEATURE_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Is Major</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_REFERENCE___IS_MAJOR = AQ_STRUCTURAL_FEATURE_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>Aq Reference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_REFERENCE_OPERATION_COUNT = AQ_STRUCTURAL_FEATURE_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.Aquila.impl.AqEnumLiteralImpl <em>Aq Enum Literal</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.Aquila.impl.AqEnumLiteralImpl
	 * @see edu.ustb.sei.mde.Aquila.impl.AquilaPackageImpl#getAqEnumLiteral()
	 * @generated
	 */
	int AQ_ENUM_LITERAL = 11;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_ENUM_LITERAL__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The number of structural features of the '<em>Aq Enum Literal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_ENUM_LITERAL_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Aq Enum Literal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_ENUM_LITERAL_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.Aquila.impl.TypedElementImpl <em>Typed Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.Aquila.impl.TypedElementImpl
	 * @see edu.ustb.sei.mde.Aquila.impl.AquilaPackageImpl#getTypedElement()
	 * @generated
	 */
	int TYPED_ELEMENT = 14;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPED_ELEMENT__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Lower Bound</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPED_ELEMENT__LOWER_BOUND = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Upper Bound</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPED_ELEMENT__UPPER_BOUND = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPED_ELEMENT__TYPE = NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Typed Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPED_ELEMENT_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Typed Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPED_ELEMENT_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.Aquila.impl.AqOperationImpl <em>Aq Operation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.Aquila.impl.AqOperationImpl
	 * @see edu.ustb.sei.mde.Aquila.impl.AquilaPackageImpl#getAqOperation()
	 * @generated
	 */
	int AQ_OPERATION = 12;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_OPERATION__NAME = TYPED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Lower Bound</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_OPERATION__LOWER_BOUND = TYPED_ELEMENT__LOWER_BOUND;

	/**
	 * The feature id for the '<em><b>Upper Bound</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_OPERATION__UPPER_BOUND = TYPED_ELEMENT__UPPER_BOUND;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_OPERATION__TYPE = TYPED_ELEMENT__TYPE;

	/**
	 * The feature id for the '<em><b>Return Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_OPERATION__RETURN_TYPE = TYPED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_OPERATION__PARAMETERS = TYPED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Aq Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_OPERATION_FEATURE_COUNT = TYPED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Aq Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_OPERATION_OPERATION_COUNT = TYPED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.Aquila.impl.AqParameterImpl <em>Aq Parameter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.Aquila.impl.AqParameterImpl
	 * @see edu.ustb.sei.mde.Aquila.impl.AquilaPackageImpl#getAqParameter()
	 * @generated
	 */
	int AQ_PARAMETER = 13;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_PARAMETER__NAME = TYPED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Lower Bound</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_PARAMETER__LOWER_BOUND = TYPED_ELEMENT__LOWER_BOUND;

	/**
	 * The feature id for the '<em><b>Upper Bound</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_PARAMETER__UPPER_BOUND = TYPED_ELEMENT__UPPER_BOUND;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_PARAMETER__TYPE = TYPED_ELEMENT__TYPE;

	/**
	 * The feature id for the '<em><b>Parameter Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_PARAMETER__PARAMETER_TYPE = TYPED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Aq Parameter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_PARAMETER_FEATURE_COUNT = TYPED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Aq Parameter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_PARAMETER_OPERATION_COUNT = TYPED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.Aquila.impl.AqUseCaseImpl <em>Aq Use Case</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.Aquila.impl.AqUseCaseImpl
	 * @see edu.ustb.sei.mde.Aquila.impl.AquilaPackageImpl#getAqUseCase()
	 * @generated
	 */
	int AQ_USE_CASE = 15;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_USE_CASE__NAME = AQ_USE_CASE_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Desciption</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_USE_CASE__DESCIPTION = AQ_USE_CASE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Actors</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_USE_CASE__ACTORS = AQ_USE_CASE_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Includes</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_USE_CASE__INCLUDES = AQ_USE_CASE_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Extends</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_USE_CASE__EXTENDS = AQ_USE_CASE_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Aq Use Case</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_USE_CASE_FEATURE_COUNT = AQ_USE_CASE_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>Aq Use Case</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_USE_CASE_OPERATION_COUNT = AQ_USE_CASE_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.Aquila.impl.AqActorImpl <em>Aq Actor</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.Aquila.impl.AqActorImpl
	 * @see edu.ustb.sei.mde.Aquila.impl.AquilaPackageImpl#getAqActor()
	 * @generated
	 */
	int AQ_ACTOR = 16;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_ACTOR__NAME = AQ_USE_CASE_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Use Cases</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_ACTOR__USE_CASES = AQ_USE_CASE_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Super Actors</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_ACTOR__SUPER_ACTORS = AQ_USE_CASE_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Aq Actor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_ACTOR_FEATURE_COUNT = AQ_USE_CASE_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Aq Actor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_ACTOR_OPERATION_COUNT = AQ_USE_CASE_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.Aquila.impl.AqStateImpl <em>Aq State</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.Aquila.impl.AqStateImpl
	 * @see edu.ustb.sei.mde.Aquila.impl.AquilaPackageImpl#getAqState()
	 * @generated
	 */
	int AQ_STATE = 17;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_STATE__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Transitions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_STATE__TRANSITIONS = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Aq State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_STATE_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Aq State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_STATE_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.Aquila.impl.AqTransitionImpl <em>Aq Transition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.Aquila.impl.AqTransitionImpl
	 * @see edu.ustb.sei.mde.Aquila.impl.AquilaPackageImpl#getAqTransition()
	 * @generated
	 */
	int AQ_TRANSITION = 18;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_TRANSITION__TARGET = 0;

	/**
	 * The feature id for the '<em><b>Trigger</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_TRANSITION__TRIGGER = 1;

	/**
	 * The feature id for the '<em><b>Guard</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_TRANSITION__GUARD = 2;

	/**
	 * The number of structural features of the '<em>Aq Transition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_TRANSITION_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Aq Transition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_TRANSITION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.Aquila.impl.AqVertexImpl <em>Aq Vertex</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.Aquila.impl.AqVertexImpl
	 * @see edu.ustb.sei.mde.Aquila.impl.AquilaPackageImpl#getAqVertex()
	 * @generated
	 */
	int AQ_VERTEX = 19;

	/**
	 * The feature id for the '<em><b>Transitions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_VERTEX__TRANSITIONS = 0;

	/**
	 * The number of structural features of the '<em>Aq Vertex</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_VERTEX_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Aq Vertex</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_VERTEX_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.Aquila.impl.AqPseudoStateImpl <em>Aq Pseudo State</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.Aquila.impl.AqPseudoStateImpl
	 * @see edu.ustb.sei.mde.Aquila.impl.AquilaPackageImpl#getAqPseudoState()
	 * @generated
	 */
	int AQ_PSEUDO_STATE = 20;

	/**
	 * The feature id for the '<em><b>Transitions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_PSEUDO_STATE__TRANSITIONS = AQ_VERTEX__TRANSITIONS;

	/**
	 * The feature id for the '<em><b>Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_PSEUDO_STATE__KIND = AQ_VERTEX_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Aq Pseudo State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_PSEUDO_STATE_FEATURE_COUNT = AQ_VERTEX_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Aq Pseudo State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_PSEUDO_STATE_OPERATION_COUNT = AQ_VERTEX_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.Aquila.impl.AqStateRegionImpl <em>Aq State Region</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.Aquila.impl.AqStateRegionImpl
	 * @see edu.ustb.sei.mde.Aquila.impl.AquilaPackageImpl#getAqStateRegion()
	 * @generated
	 */
	int AQ_STATE_REGION = 21;

	/**
	 * The feature id for the '<em><b>Vertices</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_STATE_REGION__VERTICES = 0;

	/**
	 * The number of structural features of the '<em>Aq State Region</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_STATE_REGION_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Aq State Region</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_STATE_REGION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.Aquila.impl.AqCompositeStateImpl <em>Aq Composite State</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.Aquila.impl.AqCompositeStateImpl
	 * @see edu.ustb.sei.mde.Aquila.impl.AquilaPackageImpl#getAqCompositeState()
	 * @generated
	 */
	int AQ_COMPOSITE_STATE = 22;

	/**
	 * The feature id for the '<em><b>Vertices</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_COMPOSITE_STATE__VERTICES = AQ_STATE_REGION__VERTICES;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_COMPOSITE_STATE__NAME = AQ_STATE_REGION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Transitions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_COMPOSITE_STATE__TRANSITIONS = AQ_STATE_REGION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Aq Composite State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_COMPOSITE_STATE_FEATURE_COUNT = AQ_STATE_REGION_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Aq Composite State</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_COMPOSITE_STATE_OPERATION_COUNT = AQ_STATE_REGION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.Aquila.impl.AqStateMachineImpl <em>Aq State Machine</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.Aquila.impl.AqStateMachineImpl
	 * @see edu.ustb.sei.mde.Aquila.impl.AquilaPackageImpl#getAqStateMachine()
	 * @generated
	 */
	int AQ_STATE_MACHINE = 23;

	/**
	 * The feature id for the '<em><b>Vertices</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_STATE_MACHINE__VERTICES = AQ_STATE_REGION__VERTICES;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_STATE_MACHINE__NAME = AQ_STATE_REGION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Aq State Machine</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_STATE_MACHINE_FEATURE_COUNT = AQ_STATE_REGION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Aq State Machine</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_STATE_MACHINE_OPERATION_COUNT = AQ_STATE_REGION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.Aquila.impl.AqDependencyImpl <em>Aq Dependency</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.Aquila.impl.AqDependencyImpl
	 * @see edu.ustb.sei.mde.Aquila.impl.AquilaPackageImpl#getAqDependency()
	 * @generated
	 */
	int AQ_DEPENDENCY = 24;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_DEPENDENCY__TARGET = 0;

	/**
	 * The feature id for the '<em><b>Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_DEPENDENCY__KIND = 1;

	/**
	 * The number of structural features of the '<em>Aq Dependency</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_DEPENDENCY_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Aq Dependency</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AQ_DEPENDENCY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.Aquila.AqPseudoStateKind <em>Aq Pseudo State Kind</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.Aquila.AqPseudoStateKind
	 * @see edu.ustb.sei.mde.Aquila.impl.AquilaPackageImpl#getAqPseudoStateKind()
	 * @generated
	 */
	int AQ_PSEUDO_STATE_KIND = 25;


	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.Aquila.AqDependencyKind <em>Aq Dependency Kind</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.Aquila.AqDependencyKind
	 * @see edu.ustb.sei.mde.Aquila.impl.AquilaPackageImpl#getAqDependencyKind()
	 * @generated
	 */
	int AQ_DEPENDENCY_KIND = 26;


	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.Aquila.Model <em>Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Model</em>'.
	 * @see edu.ustb.sei.mde.Aquila.Model
	 * @generated
	 */
	EClass getModel();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.ustb.sei.mde.Aquila.Model#getElements <em>Elements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Elements</em>'.
	 * @see edu.ustb.sei.mde.Aquila.Model#getElements()
	 * @see #getModel()
	 * @generated
	 */
	EReference getModel_Elements();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.ustb.sei.mde.Aquila.Model#getUseCaseElements <em>Use Case Elements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Use Case Elements</em>'.
	 * @see edu.ustb.sei.mde.Aquila.Model#getUseCaseElements()
	 * @see #getModel()
	 * @generated
	 */
	EReference getModel_UseCaseElements();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.ustb.sei.mde.Aquila.Model#getStateMachines <em>State Machines</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>State Machines</em>'.
	 * @see edu.ustb.sei.mde.Aquila.Model#getStateMachines()
	 * @see #getModel()
	 * @generated
	 */
	EReference getModel_StateMachines();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.Aquila.Model#getUUID <em>UUID</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>UUID</em>'.
	 * @see edu.ustb.sei.mde.Aquila.Model#getUUID()
	 * @see #getModel()
	 * @generated
	 */
	EAttribute getModel_UUID();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.Aquila.Model#getPackage <em>Package</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Package</em>'.
	 * @see edu.ustb.sei.mde.Aquila.Model#getPackage()
	 * @see #getModel()
	 * @generated
	 */
	EAttribute getModel_Package();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.Aquila.AqUseCaseElement <em>Aq Use Case Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Aq Use Case Element</em>'.
	 * @see edu.ustb.sei.mde.Aquila.AqUseCaseElement
	 * @generated
	 */
	EClass getAqUseCaseElement();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.Aquila.NamedElement <em>Named Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Named Element</em>'.
	 * @see edu.ustb.sei.mde.Aquila.NamedElement
	 * @generated
	 */
	EClass getNamedElement();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.Aquila.NamedElement#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see edu.ustb.sei.mde.Aquila.NamedElement#getName()
	 * @see #getNamedElement()
	 * @generated
	 */
	EAttribute getNamedElement_Name();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.Aquila.AqClassifier <em>Aq Classifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Aq Classifier</em>'.
	 * @see edu.ustb.sei.mde.Aquila.AqClassifier
	 * @generated
	 */
	EClass getAqClassifier();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.Aquila.AqClass <em>Aq Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Aq Class</em>'.
	 * @see edu.ustb.sei.mde.Aquila.AqClass
	 * @generated
	 */
	EClass getAqClass();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.ustb.sei.mde.Aquila.AqClass#getStructuralFeatures <em>Structural Features</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Structural Features</em>'.
	 * @see edu.ustb.sei.mde.Aquila.AqClass#getStructuralFeatures()
	 * @see #getAqClass()
	 * @generated
	 */
	EReference getAqClass_StructuralFeatures();

	/**
	 * Returns the meta object for the reference list '{@link edu.ustb.sei.mde.Aquila.AqClass#getSuperTypes <em>Super Types</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Super Types</em>'.
	 * @see edu.ustb.sei.mde.Aquila.AqClass#getSuperTypes()
	 * @see #getAqClass()
	 * @generated
	 */
	EReference getAqClass_SuperTypes();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.Aquila.AqClass#isInterface <em>Interface</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Interface</em>'.
	 * @see edu.ustb.sei.mde.Aquila.AqClass#isInterface()
	 * @see #getAqClass()
	 * @generated
	 */
	EAttribute getAqClass_Interface();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.ustb.sei.mde.Aquila.AqClass#getOperations <em>Operations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Operations</em>'.
	 * @see edu.ustb.sei.mde.Aquila.AqClass#getOperations()
	 * @see #getAqClass()
	 * @generated
	 */
	EReference getAqClass_Operations();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.ustb.sei.mde.Aquila.AqClass#getDependencies <em>Dependencies</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Dependencies</em>'.
	 * @see edu.ustb.sei.mde.Aquila.AqClass#getDependencies()
	 * @see #getAqClass()
	 * @generated
	 */
	EReference getAqClass_Dependencies();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.Aquila.AqClass#isAbstract <em>Abstract</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Abstract</em>'.
	 * @see edu.ustb.sei.mde.Aquila.AqClass#isAbstract()
	 * @see #getAqClass()
	 * @generated
	 */
	EAttribute getAqClass_Abstract();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.Aquila.AqDataType <em>Aq Data Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Aq Data Type</em>'.
	 * @see edu.ustb.sei.mde.Aquila.AqDataType
	 * @generated
	 */
	EClass getAqDataType();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.Aquila.AqPrimitiveType <em>Aq Primitive Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Aq Primitive Type</em>'.
	 * @see edu.ustb.sei.mde.Aquila.AqPrimitiveType
	 * @generated
	 */
	EClass getAqPrimitiveType();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.Aquila.AqPrimitiveType#getInstanceType <em>Instance Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Instance Type</em>'.
	 * @see edu.ustb.sei.mde.Aquila.AqPrimitiveType#getInstanceType()
	 * @see #getAqPrimitiveType()
	 * @generated
	 */
	EAttribute getAqPrimitiveType_InstanceType();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.Aquila.AqEnumType <em>Aq Enum Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Aq Enum Type</em>'.
	 * @see edu.ustb.sei.mde.Aquila.AqEnumType
	 * @generated
	 */
	EClass getAqEnumType();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.ustb.sei.mde.Aquila.AqEnumType#getLiterals <em>Literals</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Literals</em>'.
	 * @see edu.ustb.sei.mde.Aquila.AqEnumType#getLiterals()
	 * @see #getAqEnumType()
	 * @generated
	 */
	EReference getAqEnumType_Literals();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.Aquila.AqStructuralFeature <em>Aq Structural Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Aq Structural Feature</em>'.
	 * @see edu.ustb.sei.mde.Aquila.AqStructuralFeature
	 * @generated
	 */
	EClass getAqStructuralFeature();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.Aquila.AqAttribute <em>Aq Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Aq Attribute</em>'.
	 * @see edu.ustb.sei.mde.Aquila.AqAttribute
	 * @generated
	 */
	EClass getAqAttribute();

	/**
	 * Returns the meta object for the reference '{@link edu.ustb.sei.mde.Aquila.AqAttribute#getAttributeType <em>Attribute Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Attribute Type</em>'.
	 * @see edu.ustb.sei.mde.Aquila.AqAttribute#getAttributeType()
	 * @see #getAqAttribute()
	 * @generated
	 */
	EReference getAqAttribute_AttributeType();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.Aquila.AqReference <em>Aq Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Aq Reference</em>'.
	 * @see edu.ustb.sei.mde.Aquila.AqReference
	 * @generated
	 */
	EClass getAqReference();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.Aquila.AqReference#isContainment <em>Containment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Containment</em>'.
	 * @see edu.ustb.sei.mde.Aquila.AqReference#isContainment()
	 * @see #getAqReference()
	 * @generated
	 */
	EAttribute getAqReference_Containment();

	/**
	 * Returns the meta object for the reference '{@link edu.ustb.sei.mde.Aquila.AqReference#getReferenceType <em>Reference Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Reference Type</em>'.
	 * @see edu.ustb.sei.mde.Aquila.AqReference#getReferenceType()
	 * @see #getAqReference()
	 * @generated
	 */
	EReference getAqReference_ReferenceType();

	/**
	 * Returns the meta object for the reference '{@link edu.ustb.sei.mde.Aquila.AqReference#getOpposite <em>Opposite</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Opposite</em>'.
	 * @see edu.ustb.sei.mde.Aquila.AqReference#getOpposite()
	 * @see #getAqReference()
	 * @generated
	 */
	EReference getAqReference_Opposite();

	/**
	 * Returns the meta object for the '{@link edu.ustb.sei.mde.Aquila.AqReference#isMajor() <em>Is Major</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Major</em>' operation.
	 * @see edu.ustb.sei.mde.Aquila.AqReference#isMajor()
	 * @generated
	 */
	EOperation getAqReference__IsMajor();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.Aquila.AqEnumLiteral <em>Aq Enum Literal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Aq Enum Literal</em>'.
	 * @see edu.ustb.sei.mde.Aquila.AqEnumLiteral
	 * @generated
	 */
	EClass getAqEnumLiteral();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.Aquila.AqOperation <em>Aq Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Aq Operation</em>'.
	 * @see edu.ustb.sei.mde.Aquila.AqOperation
	 * @generated
	 */
	EClass getAqOperation();

	/**
	 * Returns the meta object for the reference '{@link edu.ustb.sei.mde.Aquila.AqOperation#getReturnType <em>Return Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Return Type</em>'.
	 * @see edu.ustb.sei.mde.Aquila.AqOperation#getReturnType()
	 * @see #getAqOperation()
	 * @generated
	 */
	EReference getAqOperation_ReturnType();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.ustb.sei.mde.Aquila.AqOperation#getParameters <em>Parameters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Parameters</em>'.
	 * @see edu.ustb.sei.mde.Aquila.AqOperation#getParameters()
	 * @see #getAqOperation()
	 * @generated
	 */
	EReference getAqOperation_Parameters();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.Aquila.AqParameter <em>Aq Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Aq Parameter</em>'.
	 * @see edu.ustb.sei.mde.Aquila.AqParameter
	 * @generated
	 */
	EClass getAqParameter();

	/**
	 * Returns the meta object for the reference '{@link edu.ustb.sei.mde.Aquila.AqParameter#getParameterType <em>Parameter Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Parameter Type</em>'.
	 * @see edu.ustb.sei.mde.Aquila.AqParameter#getParameterType()
	 * @see #getAqParameter()
	 * @generated
	 */
	EReference getAqParameter_ParameterType();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.Aquila.TypedElement <em>Typed Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Typed Element</em>'.
	 * @see edu.ustb.sei.mde.Aquila.TypedElement
	 * @generated
	 */
	EClass getTypedElement();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.Aquila.TypedElement#getLowerBound <em>Lower Bound</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Lower Bound</em>'.
	 * @see edu.ustb.sei.mde.Aquila.TypedElement#getLowerBound()
	 * @see #getTypedElement()
	 * @generated
	 */
	EAttribute getTypedElement_LowerBound();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.Aquila.TypedElement#getUpperBound <em>Upper Bound</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Upper Bound</em>'.
	 * @see edu.ustb.sei.mde.Aquila.TypedElement#getUpperBound()
	 * @see #getTypedElement()
	 * @generated
	 */
	EAttribute getTypedElement_UpperBound();

	/**
	 * Returns the meta object for the reference '{@link edu.ustb.sei.mde.Aquila.TypedElement#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Type</em>'.
	 * @see edu.ustb.sei.mde.Aquila.TypedElement#getType()
	 * @see #getTypedElement()
	 * @generated
	 */
	EReference getTypedElement_Type();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.Aquila.AqUseCase <em>Aq Use Case</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Aq Use Case</em>'.
	 * @see edu.ustb.sei.mde.Aquila.AqUseCase
	 * @generated
	 */
	EClass getAqUseCase();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.Aquila.AqUseCase#getDesciption <em>Desciption</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Desciption</em>'.
	 * @see edu.ustb.sei.mde.Aquila.AqUseCase#getDesciption()
	 * @see #getAqUseCase()
	 * @generated
	 */
	EAttribute getAqUseCase_Desciption();

	/**
	 * Returns the meta object for the reference list '{@link edu.ustb.sei.mde.Aquila.AqUseCase#getActors <em>Actors</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Actors</em>'.
	 * @see edu.ustb.sei.mde.Aquila.AqUseCase#getActors()
	 * @see #getAqUseCase()
	 * @generated
	 */
	EReference getAqUseCase_Actors();

	/**
	 * Returns the meta object for the reference list '{@link edu.ustb.sei.mde.Aquila.AqUseCase#getIncludes <em>Includes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Includes</em>'.
	 * @see edu.ustb.sei.mde.Aquila.AqUseCase#getIncludes()
	 * @see #getAqUseCase()
	 * @generated
	 */
	EReference getAqUseCase_Includes();

	/**
	 * Returns the meta object for the reference list '{@link edu.ustb.sei.mde.Aquila.AqUseCase#getExtends <em>Extends</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Extends</em>'.
	 * @see edu.ustb.sei.mde.Aquila.AqUseCase#getExtends()
	 * @see #getAqUseCase()
	 * @generated
	 */
	EReference getAqUseCase_Extends();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.Aquila.AqActor <em>Aq Actor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Aq Actor</em>'.
	 * @see edu.ustb.sei.mde.Aquila.AqActor
	 * @generated
	 */
	EClass getAqActor();

	/**
	 * Returns the meta object for the reference list '{@link edu.ustb.sei.mde.Aquila.AqActor#getUseCases <em>Use Cases</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Use Cases</em>'.
	 * @see edu.ustb.sei.mde.Aquila.AqActor#getUseCases()
	 * @see #getAqActor()
	 * @generated
	 */
	EReference getAqActor_UseCases();

	/**
	 * Returns the meta object for the reference list '{@link edu.ustb.sei.mde.Aquila.AqActor#getSuperActors <em>Super Actors</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Super Actors</em>'.
	 * @see edu.ustb.sei.mde.Aquila.AqActor#getSuperActors()
	 * @see #getAqActor()
	 * @generated
	 */
	EReference getAqActor_SuperActors();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.Aquila.AqState <em>Aq State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Aq State</em>'.
	 * @see edu.ustb.sei.mde.Aquila.AqState
	 * @generated
	 */
	EClass getAqState();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.Aquila.AqTransition <em>Aq Transition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Aq Transition</em>'.
	 * @see edu.ustb.sei.mde.Aquila.AqTransition
	 * @generated
	 */
	EClass getAqTransition();

	/**
	 * Returns the meta object for the reference '{@link edu.ustb.sei.mde.Aquila.AqTransition#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target</em>'.
	 * @see edu.ustb.sei.mde.Aquila.AqTransition#getTarget()
	 * @see #getAqTransition()
	 * @generated
	 */
	EReference getAqTransition_Target();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.Aquila.AqTransition#getTrigger <em>Trigger</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Trigger</em>'.
	 * @see edu.ustb.sei.mde.Aquila.AqTransition#getTrigger()
	 * @see #getAqTransition()
	 * @generated
	 */
	EAttribute getAqTransition_Trigger();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.Aquila.AqTransition#getGuard <em>Guard</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Guard</em>'.
	 * @see edu.ustb.sei.mde.Aquila.AqTransition#getGuard()
	 * @see #getAqTransition()
	 * @generated
	 */
	EAttribute getAqTransition_Guard();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.Aquila.AqVertex <em>Aq Vertex</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Aq Vertex</em>'.
	 * @see edu.ustb.sei.mde.Aquila.AqVertex
	 * @generated
	 */
	EClass getAqVertex();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.ustb.sei.mde.Aquila.AqVertex#getTransitions <em>Transitions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Transitions</em>'.
	 * @see edu.ustb.sei.mde.Aquila.AqVertex#getTransitions()
	 * @see #getAqVertex()
	 * @generated
	 */
	EReference getAqVertex_Transitions();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.Aquila.AqPseudoState <em>Aq Pseudo State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Aq Pseudo State</em>'.
	 * @see edu.ustb.sei.mde.Aquila.AqPseudoState
	 * @generated
	 */
	EClass getAqPseudoState();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.Aquila.AqPseudoState#getKind <em>Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Kind</em>'.
	 * @see edu.ustb.sei.mde.Aquila.AqPseudoState#getKind()
	 * @see #getAqPseudoState()
	 * @generated
	 */
	EAttribute getAqPseudoState_Kind();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.Aquila.AqStateRegion <em>Aq State Region</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Aq State Region</em>'.
	 * @see edu.ustb.sei.mde.Aquila.AqStateRegion
	 * @generated
	 */
	EClass getAqStateRegion();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.ustb.sei.mde.Aquila.AqStateRegion#getVertices <em>Vertices</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Vertices</em>'.
	 * @see edu.ustb.sei.mde.Aquila.AqStateRegion#getVertices()
	 * @see #getAqStateRegion()
	 * @generated
	 */
	EReference getAqStateRegion_Vertices();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.Aquila.AqCompositeState <em>Aq Composite State</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Aq Composite State</em>'.
	 * @see edu.ustb.sei.mde.Aquila.AqCompositeState
	 * @generated
	 */
	EClass getAqCompositeState();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.Aquila.AqStateMachine <em>Aq State Machine</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Aq State Machine</em>'.
	 * @see edu.ustb.sei.mde.Aquila.AqStateMachine
	 * @generated
	 */
	EClass getAqStateMachine();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.Aquila.AqDependency <em>Aq Dependency</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Aq Dependency</em>'.
	 * @see edu.ustb.sei.mde.Aquila.AqDependency
	 * @generated
	 */
	EClass getAqDependency();

	/**
	 * Returns the meta object for the reference '{@link edu.ustb.sei.mde.Aquila.AqDependency#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target</em>'.
	 * @see edu.ustb.sei.mde.Aquila.AqDependency#getTarget()
	 * @see #getAqDependency()
	 * @generated
	 */
	EReference getAqDependency_Target();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.Aquila.AqDependency#getKind <em>Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Kind</em>'.
	 * @see edu.ustb.sei.mde.Aquila.AqDependency#getKind()
	 * @see #getAqDependency()
	 * @generated
	 */
	EAttribute getAqDependency_Kind();

	/**
	 * Returns the meta object for enum '{@link edu.ustb.sei.mde.Aquila.AqPseudoStateKind <em>Aq Pseudo State Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Aq Pseudo State Kind</em>'.
	 * @see edu.ustb.sei.mde.Aquila.AqPseudoStateKind
	 * @generated
	 */
	EEnum getAqPseudoStateKind();

	/**
	 * Returns the meta object for enum '{@link edu.ustb.sei.mde.Aquila.AqDependencyKind <em>Aq Dependency Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Aq Dependency Kind</em>'.
	 * @see edu.ustb.sei.mde.Aquila.AqDependencyKind
	 * @generated
	 */
	EEnum getAqDependencyKind();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	AquilaFactory getAquilaFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.Aquila.impl.ModelImpl <em>Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.Aquila.impl.ModelImpl
		 * @see edu.ustb.sei.mde.Aquila.impl.AquilaPackageImpl#getModel()
		 * @generated
		 */
		EClass MODEL = eINSTANCE.getModel();

		/**
		 * The meta object literal for the '<em><b>Elements</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MODEL__ELEMENTS = eINSTANCE.getModel_Elements();

		/**
		 * The meta object literal for the '<em><b>Use Case Elements</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MODEL__USE_CASE_ELEMENTS = eINSTANCE.getModel_UseCaseElements();

		/**
		 * The meta object literal for the '<em><b>State Machines</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MODEL__STATE_MACHINES = eINSTANCE.getModel_StateMachines();

		/**
		 * The meta object literal for the '<em><b>UUID</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MODEL__UUID = eINSTANCE.getModel_UUID();

		/**
		 * The meta object literal for the '<em><b>Package</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MODEL__PACKAGE = eINSTANCE.getModel_Package();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.Aquila.impl.AqUseCaseElementImpl <em>Aq Use Case Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.Aquila.impl.AqUseCaseElementImpl
		 * @see edu.ustb.sei.mde.Aquila.impl.AquilaPackageImpl#getAqUseCaseElement()
		 * @generated
		 */
		EClass AQ_USE_CASE_ELEMENT = eINSTANCE.getAqUseCaseElement();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.Aquila.impl.NamedElementImpl <em>Named Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.Aquila.impl.NamedElementImpl
		 * @see edu.ustb.sei.mde.Aquila.impl.AquilaPackageImpl#getNamedElement()
		 * @generated
		 */
		EClass NAMED_ELEMENT = eINSTANCE.getNamedElement();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NAMED_ELEMENT__NAME = eINSTANCE.getNamedElement_Name();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.Aquila.impl.AqClassifierImpl <em>Aq Classifier</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.Aquila.impl.AqClassifierImpl
		 * @see edu.ustb.sei.mde.Aquila.impl.AquilaPackageImpl#getAqClassifier()
		 * @generated
		 */
		EClass AQ_CLASSIFIER = eINSTANCE.getAqClassifier();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.Aquila.impl.AqClassImpl <em>Aq Class</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.Aquila.impl.AqClassImpl
		 * @see edu.ustb.sei.mde.Aquila.impl.AquilaPackageImpl#getAqClass()
		 * @generated
		 */
		EClass AQ_CLASS = eINSTANCE.getAqClass();

		/**
		 * The meta object literal for the '<em><b>Structural Features</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AQ_CLASS__STRUCTURAL_FEATURES = eINSTANCE.getAqClass_StructuralFeatures();

		/**
		 * The meta object literal for the '<em><b>Super Types</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AQ_CLASS__SUPER_TYPES = eINSTANCE.getAqClass_SuperTypes();

		/**
		 * The meta object literal for the '<em><b>Interface</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AQ_CLASS__INTERFACE = eINSTANCE.getAqClass_Interface();

		/**
		 * The meta object literal for the '<em><b>Operations</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AQ_CLASS__OPERATIONS = eINSTANCE.getAqClass_Operations();

		/**
		 * The meta object literal for the '<em><b>Dependencies</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AQ_CLASS__DEPENDENCIES = eINSTANCE.getAqClass_Dependencies();

		/**
		 * The meta object literal for the '<em><b>Abstract</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AQ_CLASS__ABSTRACT = eINSTANCE.getAqClass_Abstract();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.Aquila.impl.AqDataTypeImpl <em>Aq Data Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.Aquila.impl.AqDataTypeImpl
		 * @see edu.ustb.sei.mde.Aquila.impl.AquilaPackageImpl#getAqDataType()
		 * @generated
		 */
		EClass AQ_DATA_TYPE = eINSTANCE.getAqDataType();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.Aquila.impl.AqPrimitiveTypeImpl <em>Aq Primitive Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.Aquila.impl.AqPrimitiveTypeImpl
		 * @see edu.ustb.sei.mde.Aquila.impl.AquilaPackageImpl#getAqPrimitiveType()
		 * @generated
		 */
		EClass AQ_PRIMITIVE_TYPE = eINSTANCE.getAqPrimitiveType();

		/**
		 * The meta object literal for the '<em><b>Instance Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AQ_PRIMITIVE_TYPE__INSTANCE_TYPE = eINSTANCE.getAqPrimitiveType_InstanceType();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.Aquila.impl.AqEnumTypeImpl <em>Aq Enum Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.Aquila.impl.AqEnumTypeImpl
		 * @see edu.ustb.sei.mde.Aquila.impl.AquilaPackageImpl#getAqEnumType()
		 * @generated
		 */
		EClass AQ_ENUM_TYPE = eINSTANCE.getAqEnumType();

		/**
		 * The meta object literal for the '<em><b>Literals</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AQ_ENUM_TYPE__LITERALS = eINSTANCE.getAqEnumType_Literals();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.Aquila.impl.AqStructuralFeatureImpl <em>Aq Structural Feature</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.Aquila.impl.AqStructuralFeatureImpl
		 * @see edu.ustb.sei.mde.Aquila.impl.AquilaPackageImpl#getAqStructuralFeature()
		 * @generated
		 */
		EClass AQ_STRUCTURAL_FEATURE = eINSTANCE.getAqStructuralFeature();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.Aquila.impl.AqAttributeImpl <em>Aq Attribute</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.Aquila.impl.AqAttributeImpl
		 * @see edu.ustb.sei.mde.Aquila.impl.AquilaPackageImpl#getAqAttribute()
		 * @generated
		 */
		EClass AQ_ATTRIBUTE = eINSTANCE.getAqAttribute();

		/**
		 * The meta object literal for the '<em><b>Attribute Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AQ_ATTRIBUTE__ATTRIBUTE_TYPE = eINSTANCE.getAqAttribute_AttributeType();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.Aquila.impl.AqReferenceImpl <em>Aq Reference</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.Aquila.impl.AqReferenceImpl
		 * @see edu.ustb.sei.mde.Aquila.impl.AquilaPackageImpl#getAqReference()
		 * @generated
		 */
		EClass AQ_REFERENCE = eINSTANCE.getAqReference();

		/**
		 * The meta object literal for the '<em><b>Containment</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AQ_REFERENCE__CONTAINMENT = eINSTANCE.getAqReference_Containment();

		/**
		 * The meta object literal for the '<em><b>Reference Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AQ_REFERENCE__REFERENCE_TYPE = eINSTANCE.getAqReference_ReferenceType();

		/**
		 * The meta object literal for the '<em><b>Opposite</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AQ_REFERENCE__OPPOSITE = eINSTANCE.getAqReference_Opposite();

		/**
		 * The meta object literal for the '<em><b>Is Major</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation AQ_REFERENCE___IS_MAJOR = eINSTANCE.getAqReference__IsMajor();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.Aquila.impl.AqEnumLiteralImpl <em>Aq Enum Literal</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.Aquila.impl.AqEnumLiteralImpl
		 * @see edu.ustb.sei.mde.Aquila.impl.AquilaPackageImpl#getAqEnumLiteral()
		 * @generated
		 */
		EClass AQ_ENUM_LITERAL = eINSTANCE.getAqEnumLiteral();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.Aquila.impl.AqOperationImpl <em>Aq Operation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.Aquila.impl.AqOperationImpl
		 * @see edu.ustb.sei.mde.Aquila.impl.AquilaPackageImpl#getAqOperation()
		 * @generated
		 */
		EClass AQ_OPERATION = eINSTANCE.getAqOperation();

		/**
		 * The meta object literal for the '<em><b>Return Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AQ_OPERATION__RETURN_TYPE = eINSTANCE.getAqOperation_ReturnType();

		/**
		 * The meta object literal for the '<em><b>Parameters</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AQ_OPERATION__PARAMETERS = eINSTANCE.getAqOperation_Parameters();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.Aquila.impl.AqParameterImpl <em>Aq Parameter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.Aquila.impl.AqParameterImpl
		 * @see edu.ustb.sei.mde.Aquila.impl.AquilaPackageImpl#getAqParameter()
		 * @generated
		 */
		EClass AQ_PARAMETER = eINSTANCE.getAqParameter();

		/**
		 * The meta object literal for the '<em><b>Parameter Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AQ_PARAMETER__PARAMETER_TYPE = eINSTANCE.getAqParameter_ParameterType();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.Aquila.impl.TypedElementImpl <em>Typed Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.Aquila.impl.TypedElementImpl
		 * @see edu.ustb.sei.mde.Aquila.impl.AquilaPackageImpl#getTypedElement()
		 * @generated
		 */
		EClass TYPED_ELEMENT = eINSTANCE.getTypedElement();

		/**
		 * The meta object literal for the '<em><b>Lower Bound</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TYPED_ELEMENT__LOWER_BOUND = eINSTANCE.getTypedElement_LowerBound();

		/**
		 * The meta object literal for the '<em><b>Upper Bound</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TYPED_ELEMENT__UPPER_BOUND = eINSTANCE.getTypedElement_UpperBound();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TYPED_ELEMENT__TYPE = eINSTANCE.getTypedElement_Type();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.Aquila.impl.AqUseCaseImpl <em>Aq Use Case</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.Aquila.impl.AqUseCaseImpl
		 * @see edu.ustb.sei.mde.Aquila.impl.AquilaPackageImpl#getAqUseCase()
		 * @generated
		 */
		EClass AQ_USE_CASE = eINSTANCE.getAqUseCase();

		/**
		 * The meta object literal for the '<em><b>Desciption</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AQ_USE_CASE__DESCIPTION = eINSTANCE.getAqUseCase_Desciption();

		/**
		 * The meta object literal for the '<em><b>Actors</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AQ_USE_CASE__ACTORS = eINSTANCE.getAqUseCase_Actors();

		/**
		 * The meta object literal for the '<em><b>Includes</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AQ_USE_CASE__INCLUDES = eINSTANCE.getAqUseCase_Includes();

		/**
		 * The meta object literal for the '<em><b>Extends</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AQ_USE_CASE__EXTENDS = eINSTANCE.getAqUseCase_Extends();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.Aquila.impl.AqActorImpl <em>Aq Actor</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.Aquila.impl.AqActorImpl
		 * @see edu.ustb.sei.mde.Aquila.impl.AquilaPackageImpl#getAqActor()
		 * @generated
		 */
		EClass AQ_ACTOR = eINSTANCE.getAqActor();

		/**
		 * The meta object literal for the '<em><b>Use Cases</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AQ_ACTOR__USE_CASES = eINSTANCE.getAqActor_UseCases();

		/**
		 * The meta object literal for the '<em><b>Super Actors</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AQ_ACTOR__SUPER_ACTORS = eINSTANCE.getAqActor_SuperActors();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.Aquila.impl.AqStateImpl <em>Aq State</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.Aquila.impl.AqStateImpl
		 * @see edu.ustb.sei.mde.Aquila.impl.AquilaPackageImpl#getAqState()
		 * @generated
		 */
		EClass AQ_STATE = eINSTANCE.getAqState();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.Aquila.impl.AqTransitionImpl <em>Aq Transition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.Aquila.impl.AqTransitionImpl
		 * @see edu.ustb.sei.mde.Aquila.impl.AquilaPackageImpl#getAqTransition()
		 * @generated
		 */
		EClass AQ_TRANSITION = eINSTANCE.getAqTransition();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AQ_TRANSITION__TARGET = eINSTANCE.getAqTransition_Target();

		/**
		 * The meta object literal for the '<em><b>Trigger</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AQ_TRANSITION__TRIGGER = eINSTANCE.getAqTransition_Trigger();

		/**
		 * The meta object literal for the '<em><b>Guard</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AQ_TRANSITION__GUARD = eINSTANCE.getAqTransition_Guard();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.Aquila.impl.AqVertexImpl <em>Aq Vertex</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.Aquila.impl.AqVertexImpl
		 * @see edu.ustb.sei.mde.Aquila.impl.AquilaPackageImpl#getAqVertex()
		 * @generated
		 */
		EClass AQ_VERTEX = eINSTANCE.getAqVertex();

		/**
		 * The meta object literal for the '<em><b>Transitions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AQ_VERTEX__TRANSITIONS = eINSTANCE.getAqVertex_Transitions();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.Aquila.impl.AqPseudoStateImpl <em>Aq Pseudo State</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.Aquila.impl.AqPseudoStateImpl
		 * @see edu.ustb.sei.mde.Aquila.impl.AquilaPackageImpl#getAqPseudoState()
		 * @generated
		 */
		EClass AQ_PSEUDO_STATE = eINSTANCE.getAqPseudoState();

		/**
		 * The meta object literal for the '<em><b>Kind</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AQ_PSEUDO_STATE__KIND = eINSTANCE.getAqPseudoState_Kind();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.Aquila.impl.AqStateRegionImpl <em>Aq State Region</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.Aquila.impl.AqStateRegionImpl
		 * @see edu.ustb.sei.mde.Aquila.impl.AquilaPackageImpl#getAqStateRegion()
		 * @generated
		 */
		EClass AQ_STATE_REGION = eINSTANCE.getAqStateRegion();

		/**
		 * The meta object literal for the '<em><b>Vertices</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AQ_STATE_REGION__VERTICES = eINSTANCE.getAqStateRegion_Vertices();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.Aquila.impl.AqCompositeStateImpl <em>Aq Composite State</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.Aquila.impl.AqCompositeStateImpl
		 * @see edu.ustb.sei.mde.Aquila.impl.AquilaPackageImpl#getAqCompositeState()
		 * @generated
		 */
		EClass AQ_COMPOSITE_STATE = eINSTANCE.getAqCompositeState();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.Aquila.impl.AqStateMachineImpl <em>Aq State Machine</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.Aquila.impl.AqStateMachineImpl
		 * @see edu.ustb.sei.mde.Aquila.impl.AquilaPackageImpl#getAqStateMachine()
		 * @generated
		 */
		EClass AQ_STATE_MACHINE = eINSTANCE.getAqStateMachine();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.Aquila.impl.AqDependencyImpl <em>Aq Dependency</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.Aquila.impl.AqDependencyImpl
		 * @see edu.ustb.sei.mde.Aquila.impl.AquilaPackageImpl#getAqDependency()
		 * @generated
		 */
		EClass AQ_DEPENDENCY = eINSTANCE.getAqDependency();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AQ_DEPENDENCY__TARGET = eINSTANCE.getAqDependency_Target();

		/**
		 * The meta object literal for the '<em><b>Kind</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AQ_DEPENDENCY__KIND = eINSTANCE.getAqDependency_Kind();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.Aquila.AqPseudoStateKind <em>Aq Pseudo State Kind</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.Aquila.AqPseudoStateKind
		 * @see edu.ustb.sei.mde.Aquila.impl.AquilaPackageImpl#getAqPseudoStateKind()
		 * @generated
		 */
		EEnum AQ_PSEUDO_STATE_KIND = eINSTANCE.getAqPseudoStateKind();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.Aquila.AqDependencyKind <em>Aq Dependency Kind</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.Aquila.AqDependencyKind
		 * @see edu.ustb.sei.mde.Aquila.impl.AquilaPackageImpl#getAqDependencyKind()
		 * @generated
		 */
		EEnum AQ_DEPENDENCY_KIND = eINSTANCE.getAqDependencyKind();

	}

} //AquilaPackage
