/**
 */
package edu.ustb.sei.mde.Aquila;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Aq Data Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.ustb.sei.mde.Aquila.AquilaPackage#getAqDataType()
 * @model abstract="true"
 * @generated
 */
public interface AqDataType extends AqClassifier {
} // AqDataType
