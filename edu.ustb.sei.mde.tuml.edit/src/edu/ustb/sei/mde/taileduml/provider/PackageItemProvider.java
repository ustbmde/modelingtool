/**
 */
package edu.ustb.sei.mde.taileduml.provider;


import edu.ustb.sei.mde.taileduml.TailedUMLFactory;
import edu.ustb.sei.mde.taileduml.TailedUMLPackage;


import edu.ustb.sei.mde.taileduml.vxmodel.VxModelFactory;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link edu.ustb.sei.mde.taileduml.Package} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class PackageItemProvider
	extends NamedElementItemProvider
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PackageItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

		}
		return itemPropertyDescriptors;
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(TailedUMLPackage.Literals.PACKAGE__CLASSIFIERS);
			childrenFeatures.add(TailedUMLPackage.Literals.PACKAGE__RELATIONSHIPS);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns Package.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/Package"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((edu.ustb.sei.mde.taileduml.Package)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_Package_type") :
			getString("_UI_Package_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(edu.ustb.sei.mde.taileduml.Package.class)) {
			case TailedUMLPackage.PACKAGE__CLASSIFIERS:
			case TailedUMLPackage.PACKAGE__RELATIONSHIPS:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(TailedUMLPackage.Literals.PACKAGE__CLASSIFIERS,
				 TailedUMLFactory.eINSTANCE.createClass()));

		newChildDescriptors.add
			(createChildParameter
				(TailedUMLPackage.Literals.PACKAGE__CLASSIFIERS,
				 TailedUMLFactory.eINSTANCE.createInterface()));

		newChildDescriptors.add
			(createChildParameter
				(TailedUMLPackage.Literals.PACKAGE__CLASSIFIERS,
				 TailedUMLFactory.eINSTANCE.createPrimitiveType()));

		newChildDescriptors.add
			(createChildParameter
				(TailedUMLPackage.Literals.PACKAGE__CLASSIFIERS,
				 TailedUMLFactory.eINSTANCE.createEnumeration()));

		newChildDescriptors.add
			(createChildParameter
				(TailedUMLPackage.Literals.PACKAGE__CLASSIFIERS,
				 VxModelFactory.eINSTANCE.createVariationPoint()));

		newChildDescriptors.add
			(createChildParameter
				(TailedUMLPackage.Literals.PACKAGE__CLASSIFIERS,
				 VxModelFactory.eINSTANCE.createVariation()));

		newChildDescriptors.add
			(createChildParameter
				(TailedUMLPackage.Literals.PACKAGE__RELATIONSHIPS,
				 TailedUMLFactory.eINSTANCE.createGeneralization()));

		newChildDescriptors.add
			(createChildParameter
				(TailedUMLPackage.Literals.PACKAGE__RELATIONSHIPS,
				 TailedUMLFactory.eINSTANCE.createRealization()));

		newChildDescriptors.add
			(createChildParameter
				(TailedUMLPackage.Literals.PACKAGE__RELATIONSHIPS,
				 TailedUMLFactory.eINSTANCE.createReference()));

		newChildDescriptors.add
			(createChildParameter
				(TailedUMLPackage.Literals.PACKAGE__RELATIONSHIPS,
				 VxModelFactory.eINSTANCE.createConstraint()));
	}

}
