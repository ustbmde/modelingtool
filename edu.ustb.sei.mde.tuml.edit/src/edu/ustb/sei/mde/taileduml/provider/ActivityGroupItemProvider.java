/**
 */
package edu.ustb.sei.mde.taileduml.provider;


import edu.ustb.sei.mde.taileduml.ActivityGroup;
import edu.ustb.sei.mde.taileduml.TailedUMLFactory;
import edu.ustb.sei.mde.taileduml.TailedUMLPackage;

import edu.ustb.sei.mde.taileduml.vxactivity.VxActivityFactory;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link edu.ustb.sei.mde.taileduml.ActivityGroup} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class ActivityGroupItemProvider
	extends NamedElementItemProvider
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActivityGroupItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

		}
		return itemPropertyDescriptors;
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(TailedUMLPackage.Literals.ACTIVITY_GROUP__NODES);
			childrenFeatures.add(TailedUMLPackage.Literals.ACTIVITY_GROUP__EDGES);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((ActivityGroup)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_ActivityGroup_type") :
			getString("_UI_ActivityGroup_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(ActivityGroup.class)) {
			case TailedUMLPackage.ACTIVITY_GROUP__NODES:
			case TailedUMLPackage.ACTIVITY_GROUP__EDGES:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(TailedUMLPackage.Literals.ACTIVITY_GROUP__NODES,
				 TailedUMLFactory.eINSTANCE.createCallAction()));

		newChildDescriptors.add
			(createChildParameter
				(TailedUMLPackage.Literals.ACTIVITY_GROUP__NODES,
				 TailedUMLFactory.eINSTANCE.createEmptyAction()));

		newChildDescriptors.add
			(createChildParameter
				(TailedUMLPackage.Literals.ACTIVITY_GROUP__NODES,
				 TailedUMLFactory.eINSTANCE.createAssignAction()));

		newChildDescriptors.add
			(createChildParameter
				(TailedUMLPackage.Literals.ACTIVITY_GROUP__NODES,
				 TailedUMLFactory.eINSTANCE.createSendAction()));

		newChildDescriptors.add
			(createChildParameter
				(TailedUMLPackage.Literals.ACTIVITY_GROUP__NODES,
				 TailedUMLFactory.eINSTANCE.createAcceptAction()));

		newChildDescriptors.add
			(createChildParameter
				(TailedUMLPackage.Literals.ACTIVITY_GROUP__NODES,
				 TailedUMLFactory.eINSTANCE.createObjectNode()));

		newChildDescriptors.add
			(createChildParameter
				(TailedUMLPackage.Literals.ACTIVITY_GROUP__NODES,
				 TailedUMLFactory.eINSTANCE.createDecisionNode()));

		newChildDescriptors.add
			(createChildParameter
				(TailedUMLPackage.Literals.ACTIVITY_GROUP__NODES,
				 TailedUMLFactory.eINSTANCE.createMergeNode()));

		newChildDescriptors.add
			(createChildParameter
				(TailedUMLPackage.Literals.ACTIVITY_GROUP__NODES,
				 TailedUMLFactory.eINSTANCE.createForkNode()));

		newChildDescriptors.add
			(createChildParameter
				(TailedUMLPackage.Literals.ACTIVITY_GROUP__NODES,
				 TailedUMLFactory.eINSTANCE.createJoinNode()));

		newChildDescriptors.add
			(createChildParameter
				(TailedUMLPackage.Literals.ACTIVITY_GROUP__NODES,
				 TailedUMLFactory.eINSTANCE.createFlowFinalNode()));

		newChildDescriptors.add
			(createChildParameter
				(TailedUMLPackage.Literals.ACTIVITY_GROUP__NODES,
				 TailedUMLFactory.eINSTANCE.createActivityFinalNode()));

		newChildDescriptors.add
			(createChildParameter
				(TailedUMLPackage.Literals.ACTIVITY_GROUP__NODES,
				 TailedUMLFactory.eINSTANCE.createInitialNode()));

		newChildDescriptors.add
			(createChildParameter
				(TailedUMLPackage.Literals.ACTIVITY_GROUP__NODES,
				 VxActivityFactory.eINSTANCE.createVxAction()));

		newChildDescriptors.add
			(createChildParameter
				(TailedUMLPackage.Literals.ACTIVITY_GROUP__EDGES,
				 TailedUMLFactory.eINSTANCE.createControlFlow()));

		newChildDescriptors.add
			(createChildParameter
				(TailedUMLPackage.Literals.ACTIVITY_GROUP__EDGES,
				 TailedUMLFactory.eINSTANCE.createObjectFlow()));
	}

}
