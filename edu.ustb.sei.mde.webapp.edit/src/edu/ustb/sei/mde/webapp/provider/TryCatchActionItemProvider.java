/**
 */
package edu.ustb.sei.mde.webapp.provider;


import edu.ustb.sei.mde.webapp.TryCatchAction;
import edu.ustb.sei.mde.webapp.WebAppFactory;
import edu.ustb.sei.mde.webapp.WebAppPackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link edu.ustb.sei.mde.webapp.TryCatchAction} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class TryCatchActionItemProvider
	extends ActionItemProvider
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TryCatchActionItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

		}
		return itemPropertyDescriptors;
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(WebAppPackage.Literals.TRY_CATCH_ACTION__TRY_PART);
			childrenFeatures.add(WebAppPackage.Literals.TRY_CATCH_ACTION__CATCH_PARTS);
			childrenFeatures.add(WebAppPackage.Literals.TRY_CATCH_ACTION__FINAL_PART);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns TryCatchAction.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/TryCatchAction"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public String getText(Object object) {
		return "try";
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(TryCatchAction.class)) {
			case WebAppPackage.TRY_CATCH_ACTION__TRY_PART:
			case WebAppPackage.TRY_CATCH_ACTION__CATCH_PARTS:
			case WebAppPackage.TRY_CATCH_ACTION__FINAL_PART:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(WebAppPackage.Literals.TRY_CATCH_ACTION__TRY_PART,
				 WebAppFactory.eINSTANCE.createCodeAction()));

		newChildDescriptors.add
			(createChildParameter
				(WebAppPackage.Literals.TRY_CATCH_ACTION__TRY_PART,
				 WebAppFactory.eINSTANCE.createCallAction()));

		newChildDescriptors.add
			(createChildParameter
				(WebAppPackage.Literals.TRY_CATCH_ACTION__TRY_PART,
				 WebAppFactory.eINSTANCE.createLetAction()));

		newChildDescriptors.add
			(createChildParameter
				(WebAppPackage.Literals.TRY_CATCH_ACTION__TRY_PART,
				 WebAppFactory.eINSTANCE.createForAction()));

		newChildDescriptors.add
			(createChildParameter
				(WebAppPackage.Literals.TRY_CATCH_ACTION__TRY_PART,
				 WebAppFactory.eINSTANCE.createIfAction()));

		newChildDescriptors.add
			(createChildParameter
				(WebAppPackage.Literals.TRY_CATCH_ACTION__TRY_PART,
				 WebAppFactory.eINSTANCE.createSequenceAction()));

		newChildDescriptors.add
			(createChildParameter
				(WebAppPackage.Literals.TRY_CATCH_ACTION__TRY_PART,
				 WebAppFactory.eINSTANCE.createTryCatchAction()));

		newChildDescriptors.add
			(createChildParameter
				(WebAppPackage.Literals.TRY_CATCH_ACTION__TRY_PART,
				 WebAppFactory.eINSTANCE.createJQueryAjaxAction()));

		newChildDescriptors.add
			(createChildParameter
				(WebAppPackage.Literals.TRY_CATCH_ACTION__TRY_PART,
				 WebAppFactory.eINSTANCE.createJQueryEventAction()));

		newChildDescriptors.add
			(createChildParameter
				(WebAppPackage.Literals.TRY_CATCH_ACTION__TRY_PART,
				 WebAppFactory.eINSTANCE.createSQLSelectAction()));

		newChildDescriptors.add
			(createChildParameter
				(WebAppPackage.Literals.TRY_CATCH_ACTION__TRY_PART,
				 WebAppFactory.eINSTANCE.createSQLInsertAction()));

		newChildDescriptors.add
			(createChildParameter
				(WebAppPackage.Literals.TRY_CATCH_ACTION__TRY_PART,
				 WebAppFactory.eINSTANCE.createSQLUpdateAction()));

		newChildDescriptors.add
			(createChildParameter
				(WebAppPackage.Literals.TRY_CATCH_ACTION__TRY_PART,
				 WebAppFactory.eINSTANCE.createSQLDeleteAction()));

		newChildDescriptors.add
			(createChildParameter
				(WebAppPackage.Literals.TRY_CATCH_ACTION__TRY_PART,
				 WebAppFactory.eINSTANCE.createSQLExecutionAction()));

		newChildDescriptors.add
			(createChildParameter
				(WebAppPackage.Literals.TRY_CATCH_ACTION__TRY_PART,
				 WebAppFactory.eINSTANCE.createSQLResultSetIterationAction()));

		newChildDescriptors.add
			(createChildParameter
				(WebAppPackage.Literals.TRY_CATCH_ACTION__TRY_PART,
				 WebAppFactory.eINSTANCE.createServerPageTemplateAction()));

		newChildDescriptors.add
			(createChildParameter
				(WebAppPackage.Literals.TRY_CATCH_ACTION__TRY_PART,
				 WebAppFactory.eINSTANCE.createServerPageExpressionAction()));

		newChildDescriptors.add
			(createChildParameter
				(WebAppPackage.Literals.TRY_CATCH_ACTION__TRY_PART,
				 WebAppFactory.eINSTANCE.createServerPageStructuredTemplateAction()));

		newChildDescriptors.add
			(createChildParameter
				(WebAppPackage.Literals.TRY_CATCH_ACTION__CATCH_PARTS,
				 WebAppFactory.eINSTANCE.createCatchPart()));

		newChildDescriptors.add
			(createChildParameter
				(WebAppPackage.Literals.TRY_CATCH_ACTION__FINAL_PART,
				 WebAppFactory.eINSTANCE.createCodeAction()));

		newChildDescriptors.add
			(createChildParameter
				(WebAppPackage.Literals.TRY_CATCH_ACTION__FINAL_PART,
				 WebAppFactory.eINSTANCE.createCallAction()));

		newChildDescriptors.add
			(createChildParameter
				(WebAppPackage.Literals.TRY_CATCH_ACTION__FINAL_PART,
				 WebAppFactory.eINSTANCE.createLetAction()));

		newChildDescriptors.add
			(createChildParameter
				(WebAppPackage.Literals.TRY_CATCH_ACTION__FINAL_PART,
				 WebAppFactory.eINSTANCE.createForAction()));

		newChildDescriptors.add
			(createChildParameter
				(WebAppPackage.Literals.TRY_CATCH_ACTION__FINAL_PART,
				 WebAppFactory.eINSTANCE.createIfAction()));

		newChildDescriptors.add
			(createChildParameter
				(WebAppPackage.Literals.TRY_CATCH_ACTION__FINAL_PART,
				 WebAppFactory.eINSTANCE.createSequenceAction()));

		newChildDescriptors.add
			(createChildParameter
				(WebAppPackage.Literals.TRY_CATCH_ACTION__FINAL_PART,
				 WebAppFactory.eINSTANCE.createTryCatchAction()));

		newChildDescriptors.add
			(createChildParameter
				(WebAppPackage.Literals.TRY_CATCH_ACTION__FINAL_PART,
				 WebAppFactory.eINSTANCE.createJQueryAjaxAction()));

		newChildDescriptors.add
			(createChildParameter
				(WebAppPackage.Literals.TRY_CATCH_ACTION__FINAL_PART,
				 WebAppFactory.eINSTANCE.createJQueryEventAction()));

		newChildDescriptors.add
			(createChildParameter
				(WebAppPackage.Literals.TRY_CATCH_ACTION__FINAL_PART,
				 WebAppFactory.eINSTANCE.createSQLSelectAction()));

		newChildDescriptors.add
			(createChildParameter
				(WebAppPackage.Literals.TRY_CATCH_ACTION__FINAL_PART,
				 WebAppFactory.eINSTANCE.createSQLInsertAction()));

		newChildDescriptors.add
			(createChildParameter
				(WebAppPackage.Literals.TRY_CATCH_ACTION__FINAL_PART,
				 WebAppFactory.eINSTANCE.createSQLUpdateAction()));

		newChildDescriptors.add
			(createChildParameter
				(WebAppPackage.Literals.TRY_CATCH_ACTION__FINAL_PART,
				 WebAppFactory.eINSTANCE.createSQLDeleteAction()));

		newChildDescriptors.add
			(createChildParameter
				(WebAppPackage.Literals.TRY_CATCH_ACTION__FINAL_PART,
				 WebAppFactory.eINSTANCE.createSQLExecutionAction()));

		newChildDescriptors.add
			(createChildParameter
				(WebAppPackage.Literals.TRY_CATCH_ACTION__FINAL_PART,
				 WebAppFactory.eINSTANCE.createSQLResultSetIterationAction()));

		newChildDescriptors.add
			(createChildParameter
				(WebAppPackage.Literals.TRY_CATCH_ACTION__FINAL_PART,
				 WebAppFactory.eINSTANCE.createServerPageTemplateAction()));

		newChildDescriptors.add
			(createChildParameter
				(WebAppPackage.Literals.TRY_CATCH_ACTION__FINAL_PART,
				 WebAppFactory.eINSTANCE.createServerPageExpressionAction()));

		newChildDescriptors.add
			(createChildParameter
				(WebAppPackage.Literals.TRY_CATCH_ACTION__FINAL_PART,
				 WebAppFactory.eINSTANCE.createServerPageStructuredTemplateAction()));
	}

	/**
	 * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCreateChildText(Object owner, Object feature, Object child, Collection<?> selection) {
		Object childFeature = feature;
		Object childObject = child;

		boolean qualify =
			childFeature == WebAppPackage.Literals.TRY_CATCH_ACTION__TRY_PART ||
			childFeature == WebAppPackage.Literals.TRY_CATCH_ACTION__FINAL_PART;

		if (qualify) {
			return getString
				("_UI_CreateChild_text2",
				 new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

}
