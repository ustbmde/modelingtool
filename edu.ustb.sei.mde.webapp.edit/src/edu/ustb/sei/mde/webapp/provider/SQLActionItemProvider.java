/**
 */
package edu.ustb.sei.mde.webapp.provider;


import edu.ustb.sei.mde.webapp.SQLAction;
import edu.ustb.sei.mde.webapp.WebAppFactory;
import edu.ustb.sei.mde.webapp.WebAppPackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link edu.ustb.sei.mde.webapp.SQLAction} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class SQLActionItemProvider
	extends ActionItemProvider
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SQLActionItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addDatabasePropertyDescriptor(object);
			addSqlNamePropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Database feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDatabasePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_SQLAction_database_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_SQLAction_database_feature", "_UI_SQLAction_type"),
				 WebAppPackage.Literals.SQL_ACTION__DATABASE,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Sql Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSqlNamePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_SQLAction_sqlName_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_SQLAction_sqlName_feature", "_UI_SQLAction_type"),
				 WebAppPackage.Literals.SQL_ACTION__SQL_NAME,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(WebAppPackage.Literals.SQL_ACTION__HANDLING_ACTION);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((SQLAction)object).getSqlName();
		return label == null || label.length() == 0 ?
			getString("_UI_SQLAction_type") :
			getString("_UI_SQLAction_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(SQLAction.class)) {
			case WebAppPackage.SQL_ACTION__SQL_NAME:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case WebAppPackage.SQL_ACTION__HANDLING_ACTION:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(WebAppPackage.Literals.SQL_ACTION__HANDLING_ACTION,
				 WebAppFactory.eINSTANCE.createCodeAction()));

		newChildDescriptors.add
			(createChildParameter
				(WebAppPackage.Literals.SQL_ACTION__HANDLING_ACTION,
				 WebAppFactory.eINSTANCE.createCallAction()));

		newChildDescriptors.add
			(createChildParameter
				(WebAppPackage.Literals.SQL_ACTION__HANDLING_ACTION,
				 WebAppFactory.eINSTANCE.createLetAction()));

		newChildDescriptors.add
			(createChildParameter
				(WebAppPackage.Literals.SQL_ACTION__HANDLING_ACTION,
				 WebAppFactory.eINSTANCE.createForAction()));

		newChildDescriptors.add
			(createChildParameter
				(WebAppPackage.Literals.SQL_ACTION__HANDLING_ACTION,
				 WebAppFactory.eINSTANCE.createIfAction()));

		newChildDescriptors.add
			(createChildParameter
				(WebAppPackage.Literals.SQL_ACTION__HANDLING_ACTION,
				 WebAppFactory.eINSTANCE.createSequenceAction()));

		newChildDescriptors.add
			(createChildParameter
				(WebAppPackage.Literals.SQL_ACTION__HANDLING_ACTION,
				 WebAppFactory.eINSTANCE.createTryCatchAction()));

		newChildDescriptors.add
			(createChildParameter
				(WebAppPackage.Literals.SQL_ACTION__HANDLING_ACTION,
				 WebAppFactory.eINSTANCE.createJQueryAjaxAction()));

		newChildDescriptors.add
			(createChildParameter
				(WebAppPackage.Literals.SQL_ACTION__HANDLING_ACTION,
				 WebAppFactory.eINSTANCE.createJQueryEventAction()));

		newChildDescriptors.add
			(createChildParameter
				(WebAppPackage.Literals.SQL_ACTION__HANDLING_ACTION,
				 WebAppFactory.eINSTANCE.createSQLSelectAction()));

		newChildDescriptors.add
			(createChildParameter
				(WebAppPackage.Literals.SQL_ACTION__HANDLING_ACTION,
				 WebAppFactory.eINSTANCE.createSQLInsertAction()));

		newChildDescriptors.add
			(createChildParameter
				(WebAppPackage.Literals.SQL_ACTION__HANDLING_ACTION,
				 WebAppFactory.eINSTANCE.createSQLUpdateAction()));

		newChildDescriptors.add
			(createChildParameter
				(WebAppPackage.Literals.SQL_ACTION__HANDLING_ACTION,
				 WebAppFactory.eINSTANCE.createSQLDeleteAction()));

		newChildDescriptors.add
			(createChildParameter
				(WebAppPackage.Literals.SQL_ACTION__HANDLING_ACTION,
				 WebAppFactory.eINSTANCE.createSQLExecutionAction()));

		newChildDescriptors.add
			(createChildParameter
				(WebAppPackage.Literals.SQL_ACTION__HANDLING_ACTION,
				 WebAppFactory.eINSTANCE.createSQLResultSetIterationAction()));

		newChildDescriptors.add
			(createChildParameter
				(WebAppPackage.Literals.SQL_ACTION__HANDLING_ACTION,
				 WebAppFactory.eINSTANCE.createServerPageTemplateAction()));

		newChildDescriptors.add
			(createChildParameter
				(WebAppPackage.Literals.SQL_ACTION__HANDLING_ACTION,
				 WebAppFactory.eINSTANCE.createServerPageExpressionAction()));

		newChildDescriptors.add
			(createChildParameter
				(WebAppPackage.Literals.SQL_ACTION__HANDLING_ACTION,
				 WebAppFactory.eINSTANCE.createServerPageStructuredTemplateAction()));
	}

}
