/**
 */
package edu.ustb.sei.mde.webapp.provider;


import edu.ustb.sei.mde.webapp.WebAppFactory;
import edu.ustb.sei.mde.webapp.WebAppPackage;
import edu.ustb.sei.mde.webapp.WebApplication;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link edu.ustb.sei.mde.webapp.WebApplication} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class WebApplicationItemProvider
	extends NamedElementItemProvider
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WebApplicationItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addDatabaseUtilClassPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Database Util Class feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDatabaseUtilClassPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_WebApplication_databaseUtilClass_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_WebApplication_databaseUtilClass_feature", "_UI_WebApplication_type"),
				 WebAppPackage.Literals.WEB_APPLICATION__DATABASE_UTIL_CLASS,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(WebAppPackage.Literals.WEB_APPLICATION__DATABASES);
			childrenFeatures.add(WebAppPackage.Literals.WEB_APPLICATION__INTERFACES);
			childrenFeatures.add(WebAppPackage.Literals.WEB_APPLICATION__PAGE_COMPONENTS);
			childrenFeatures.add(WebAppPackage.Literals.WEB_APPLICATION__SERVER_COMPONENTS);
			childrenFeatures.add(WebAppPackage.Literals.WEB_APPLICATION__CLASS_COMPONENTS);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns WebApplication.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/WebApplication"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((WebApplication)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_WebApplication_type") :
			getString("_UI_WebApplication_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(WebApplication.class)) {
			case WebAppPackage.WEB_APPLICATION__DATABASE_UTIL_CLASS:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case WebAppPackage.WEB_APPLICATION__DATABASES:
			case WebAppPackage.WEB_APPLICATION__INTERFACES:
			case WebAppPackage.WEB_APPLICATION__PAGE_COMPONENTS:
			case WebAppPackage.WEB_APPLICATION__SERVER_COMPONENTS:
			case WebAppPackage.WEB_APPLICATION__CLASS_COMPONENTS:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(WebAppPackage.Literals.WEB_APPLICATION__DATABASES,
				 WebAppFactory.eINSTANCE.createDatabase()));

		newChildDescriptors.add
			(createChildParameter
				(WebAppPackage.Literals.WEB_APPLICATION__INTERFACES,
				 WebAppFactory.eINSTANCE.createInterface()));

		newChildDescriptors.add
			(createChildParameter
				(WebAppPackage.Literals.WEB_APPLICATION__PAGE_COMPONENTS,
				 WebAppFactory.eINSTANCE.createPageComponent()));

		newChildDescriptors.add
			(createChildParameter
				(WebAppPackage.Literals.WEB_APPLICATION__SERVER_COMPONENTS,
				 WebAppFactory.eINSTANCE.createServerComponent()));

		newChildDescriptors.add
			(createChildParameter
				(WebAppPackage.Literals.WEB_APPLICATION__CLASS_COMPONENTS,
				 WebAppFactory.eINSTANCE.createClassComponent()));
	}

}
