/**
 */
package edu.ustb.sei.mde.webapp.provider;


import edu.ustb.sei.mde.webapp.SQLResultSetIterationAction;
import edu.ustb.sei.mde.webapp.WebAppFactory;
import edu.ustb.sei.mde.webapp.WebAppPackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link edu.ustb.sei.mde.webapp.SQLResultSetIterationAction} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class SQLResultSetIterationActionItemProvider
	extends ActionItemProvider
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SQLResultSetIterationActionItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

		}
		return itemPropertyDescriptors;
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(WebAppPackage.Literals.SQL_RESULT_SET_ITERATION_ACTION__ACTION);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns SQLResultSetIterationAction.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/SQLResultSetIterationAction"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public String getText(Object object) {
		return "iterate result";
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(SQLResultSetIterationAction.class)) {
			case WebAppPackage.SQL_RESULT_SET_ITERATION_ACTION__ACTION:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(WebAppPackage.Literals.SQL_RESULT_SET_ITERATION_ACTION__ACTION,
				 WebAppFactory.eINSTANCE.createCodeAction()));

		newChildDescriptors.add
			(createChildParameter
				(WebAppPackage.Literals.SQL_RESULT_SET_ITERATION_ACTION__ACTION,
				 WebAppFactory.eINSTANCE.createCallAction()));

		newChildDescriptors.add
			(createChildParameter
				(WebAppPackage.Literals.SQL_RESULT_SET_ITERATION_ACTION__ACTION,
				 WebAppFactory.eINSTANCE.createLetAction()));

		newChildDescriptors.add
			(createChildParameter
				(WebAppPackage.Literals.SQL_RESULT_SET_ITERATION_ACTION__ACTION,
				 WebAppFactory.eINSTANCE.createForAction()));

		newChildDescriptors.add
			(createChildParameter
				(WebAppPackage.Literals.SQL_RESULT_SET_ITERATION_ACTION__ACTION,
				 WebAppFactory.eINSTANCE.createIfAction()));

		newChildDescriptors.add
			(createChildParameter
				(WebAppPackage.Literals.SQL_RESULT_SET_ITERATION_ACTION__ACTION,
				 WebAppFactory.eINSTANCE.createSequenceAction()));

		newChildDescriptors.add
			(createChildParameter
				(WebAppPackage.Literals.SQL_RESULT_SET_ITERATION_ACTION__ACTION,
				 WebAppFactory.eINSTANCE.createTryCatchAction()));

		newChildDescriptors.add
			(createChildParameter
				(WebAppPackage.Literals.SQL_RESULT_SET_ITERATION_ACTION__ACTION,
				 WebAppFactory.eINSTANCE.createJQueryAjaxAction()));

		newChildDescriptors.add
			(createChildParameter
				(WebAppPackage.Literals.SQL_RESULT_SET_ITERATION_ACTION__ACTION,
				 WebAppFactory.eINSTANCE.createJQueryEventAction()));

		newChildDescriptors.add
			(createChildParameter
				(WebAppPackage.Literals.SQL_RESULT_SET_ITERATION_ACTION__ACTION,
				 WebAppFactory.eINSTANCE.createSQLSelectAction()));

		newChildDescriptors.add
			(createChildParameter
				(WebAppPackage.Literals.SQL_RESULT_SET_ITERATION_ACTION__ACTION,
				 WebAppFactory.eINSTANCE.createSQLInsertAction()));

		newChildDescriptors.add
			(createChildParameter
				(WebAppPackage.Literals.SQL_RESULT_SET_ITERATION_ACTION__ACTION,
				 WebAppFactory.eINSTANCE.createSQLUpdateAction()));

		newChildDescriptors.add
			(createChildParameter
				(WebAppPackage.Literals.SQL_RESULT_SET_ITERATION_ACTION__ACTION,
				 WebAppFactory.eINSTANCE.createSQLDeleteAction()));

		newChildDescriptors.add
			(createChildParameter
				(WebAppPackage.Literals.SQL_RESULT_SET_ITERATION_ACTION__ACTION,
				 WebAppFactory.eINSTANCE.createSQLExecutionAction()));

		newChildDescriptors.add
			(createChildParameter
				(WebAppPackage.Literals.SQL_RESULT_SET_ITERATION_ACTION__ACTION,
				 WebAppFactory.eINSTANCE.createSQLResultSetIterationAction()));

		newChildDescriptors.add
			(createChildParameter
				(WebAppPackage.Literals.SQL_RESULT_SET_ITERATION_ACTION__ACTION,
				 WebAppFactory.eINSTANCE.createServerPageTemplateAction()));

		newChildDescriptors.add
			(createChildParameter
				(WebAppPackage.Literals.SQL_RESULT_SET_ITERATION_ACTION__ACTION,
				 WebAppFactory.eINSTANCE.createServerPageExpressionAction()));

		newChildDescriptors.add
			(createChildParameter
				(WebAppPackage.Literals.SQL_RESULT_SET_ITERATION_ACTION__ACTION,
				 WebAppFactory.eINSTANCE.createServerPageStructuredTemplateAction()));
	}

}
