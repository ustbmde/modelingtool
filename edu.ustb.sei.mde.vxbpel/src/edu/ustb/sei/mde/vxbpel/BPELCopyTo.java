/**
 */
package edu.ustb.sei.mde.vxbpel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>BPEL Copy To</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.BPELCopyTo#getVariable <em>Variable</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.BPELCopyTo#getPart <em>Part</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.BPELCopyTo#getProperty <em>Property</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.BPELCopyTo#getPartnerLink <em>Partner Link</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.BPELCopyTo#getExpression <em>Expression</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.BPELCopyTo#getQuery <em>Query</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELCopyTo()
 * @model
 * @generated
 */
public interface BPELCopyTo extends XMLAttributeContainer {
	/**
	 * Returns the value of the '<em><b>Variable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Variable</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Variable</em>' attribute.
	 * @see #setVariable(String)
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELCopyTo_Variable()
	 * @model
	 * @generated
	 */
	String getVariable();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.vxbpel.BPELCopyTo#getVariable <em>Variable</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Variable</em>' attribute.
	 * @see #getVariable()
	 * @generated
	 */
	void setVariable(String value);

	/**
	 * Returns the value of the '<em><b>Part</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Part</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Part</em>' attribute.
	 * @see #setPart(String)
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELCopyTo_Part()
	 * @model
	 * @generated
	 */
	String getPart();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.vxbpel.BPELCopyTo#getPart <em>Part</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Part</em>' attribute.
	 * @see #getPart()
	 * @generated
	 */
	void setPart(String value);

	/**
	 * Returns the value of the '<em><b>Property</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Property</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Property</em>' attribute.
	 * @see #setProperty(String)
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELCopyTo_Property()
	 * @model
	 * @generated
	 */
	String getProperty();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.vxbpel.BPELCopyTo#getProperty <em>Property</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Property</em>' attribute.
	 * @see #getProperty()
	 * @generated
	 */
	void setProperty(String value);

	/**
	 * Returns the value of the '<em><b>Partner Link</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Partner Link</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Partner Link</em>' attribute.
	 * @see #setPartnerLink(String)
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELCopyTo_PartnerLink()
	 * @model
	 * @generated
	 */
	String getPartnerLink();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.vxbpel.BPELCopyTo#getPartnerLink <em>Partner Link</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Partner Link</em>' attribute.
	 * @see #getPartnerLink()
	 * @generated
	 */
	void setPartnerLink(String value);

	/**
	 * Returns the value of the '<em><b>Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Expression</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Expression</em>' containment reference.
	 * @see #setExpression(BPELExpression)
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELCopyTo_Expression()
	 * @model containment="true"
	 * @generated
	 */
	BPELExpression getExpression();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.vxbpel.BPELCopyTo#getExpression <em>Expression</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Expression</em>' containment reference.
	 * @see #getExpression()
	 * @generated
	 */
	void setExpression(BPELExpression value);

	/**
	 * Returns the value of the '<em><b>Query</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Query</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Query</em>' containment reference.
	 * @see #setQuery(BPELQuery)
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELCopyTo_Query()
	 * @model containment="true"
	 * @generated
	 */
	BPELQuery getQuery();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.vxbpel.BPELCopyTo#getQuery <em>Query</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Query</em>' containment reference.
	 * @see #getQuery()
	 * @generated
	 */
	void setQuery(BPELQuery value);

} // BPELCopyTo
