/**
 */
package edu.ustb.sei.mde.vxbpel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>BPEL Repeat Until</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.BPELRepeatUntil#getBodyActivity <em>Body Activity</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.BPELRepeatUntil#getCondition <em>Condition</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELRepeatUntil()
 * @model
 * @generated
 */
public interface BPELRepeatUntil extends BPELActivity {
	/**
	 * Returns the value of the '<em><b>Body Activity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Body Activity</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Body Activity</em>' containment reference.
	 * @see #setBodyActivity(BPELActivity)
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELRepeatUntil_BodyActivity()
	 * @model containment="true" required="true"
	 * @generated
	 */
	BPELActivity getBodyActivity();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.vxbpel.BPELRepeatUntil#getBodyActivity <em>Body Activity</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Body Activity</em>' containment reference.
	 * @see #getBodyActivity()
	 * @generated
	 */
	void setBodyActivity(BPELActivity value);

	/**
	 * Returns the value of the '<em><b>Condition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Condition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Condition</em>' containment reference.
	 * @see #setCondition(BPELBoolExpression)
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELRepeatUntil_Condition()
	 * @model containment="true" required="true"
	 * @generated
	 */
	BPELBoolExpression getCondition();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.vxbpel.BPELRepeatUntil#getCondition <em>Condition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Condition</em>' containment reference.
	 * @see #getCondition()
	 * @generated
	 */
	void setCondition(BPELBoolExpression value);

} // BPELRepeatUntil
