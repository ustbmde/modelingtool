/**
 */
package edu.ustb.sei.mde.vxbpel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Variation Point PChoice</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.VxBPELVariationPointPChoice#getVpname <em>Vpname</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.VxBPELVariationPointPChoice#getVariant <em>Variant</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getVxBPELVariationPointPChoice()
 * @model
 * @generated
 */
public interface VxBPELVariationPointPChoice extends EObject {
	/**
	 * Returns the value of the '<em><b>Vpname</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Vpname</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Vpname</em>' attribute.
	 * @see #setVpname(String)
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getVxBPELVariationPointPChoice_Vpname()
	 * @model
	 * @generated
	 */
	String getVpname();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.vxbpel.VxBPELVariationPointPChoice#getVpname <em>Vpname</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Vpname</em>' attribute.
	 * @see #getVpname()
	 * @generated
	 */
	void setVpname(String value);

	/**
	 * Returns the value of the '<em><b>Variant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Variant</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Variant</em>' attribute.
	 * @see #setVariant(String)
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getVxBPELVariationPointPChoice_Variant()
	 * @model
	 * @generated
	 */
	String getVariant();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.vxbpel.VxBPELVariationPointPChoice#getVariant <em>Variant</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Variant</em>' attribute.
	 * @see #getVariant()
	 * @generated
	 */
	void setVariant(String value);

} // VxBPELVariationPointPChoice
