/**
 */
package edu.ustb.sei.mde.vxbpel;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>BPEL If</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.BPELIf#getCondition <em>Condition</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.BPELIf#getThenActivity <em>Then Activity</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.BPELIf#getElseIfActivity <em>Else If Activity</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.BPELIf#getElseActivity <em>Else Activity</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELIf()
 * @model
 * @generated
 */
public interface BPELIf extends BPELActivity {
	/**
	 * Returns the value of the '<em><b>Condition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Condition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Condition</em>' containment reference.
	 * @see #setCondition(BPELBoolExpression)
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELIf_Condition()
	 * @model containment="true" required="true"
	 * @generated
	 */
	BPELBoolExpression getCondition();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.vxbpel.BPELIf#getCondition <em>Condition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Condition</em>' containment reference.
	 * @see #getCondition()
	 * @generated
	 */
	void setCondition(BPELBoolExpression value);

	/**
	 * Returns the value of the '<em><b>Then Activity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Then Activity</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Then Activity</em>' containment reference.
	 * @see #setThenActivity(BPELActivity)
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELIf_ThenActivity()
	 * @model containment="true" required="true"
	 * @generated
	 */
	BPELActivity getThenActivity();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.vxbpel.BPELIf#getThenActivity <em>Then Activity</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Then Activity</em>' containment reference.
	 * @see #getThenActivity()
	 * @generated
	 */
	void setThenActivity(BPELActivity value);

	/**
	 * Returns the value of the '<em><b>Else If Activity</b></em>' containment reference list.
	 * The list contents are of type {@link edu.ustb.sei.mde.vxbpel.BPELElseIf}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Else If Activity</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Else If Activity</em>' containment reference list.
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELIf_ElseIfActivity()
	 * @model containment="true"
	 * @generated
	 */
	EList<BPELElseIf> getElseIfActivity();

	/**
	 * Returns the value of the '<em><b>Else Activity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Else Activity</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Else Activity</em>' containment reference.
	 * @see #setElseActivity(BPELActivity)
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELIf_ElseActivity()
	 * @model containment="true"
	 * @generated
	 */
	BPELActivity getElseActivity();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.vxbpel.BPELIf#getElseActivity <em>Else Activity</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Else Activity</em>' containment reference.
	 * @see #getElseActivity()
	 * @generated
	 */
	void setElseActivity(BPELActivity value);

} // BPELIf
