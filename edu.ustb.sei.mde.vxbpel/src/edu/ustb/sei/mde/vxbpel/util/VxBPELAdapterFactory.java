/**
 */
package edu.ustb.sei.mde.vxbpel.util;

import edu.ustb.sei.mde.vxbpel.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage
 * @generated
 */
public class VxBPELAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static VxBPELPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VxBPELAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = VxBPELPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VxBPELSwitch<Adapter> modelSwitch =
		new VxBPELSwitch<Adapter>() {
			@Override
			public Adapter caseNamedElement(NamedElement object) {
				return createNamedElementAdapter();
			}
			@Override
			public Adapter caseXMLAttributeContainer(XMLAttributeContainer object) {
				return createXMLAttributeContainerAdapter();
			}
			@Override
			public Adapter caseXMLAttribute(XMLAttribute object) {
				return createXMLAttributeAdapter();
			}
			@Override
			public Adapter caseBPELExpression(BPELExpression object) {
				return createBPELExpressionAdapter();
			}
			@Override
			public Adapter caseBPELBoolExpression(BPELBoolExpression object) {
				return createBPELBoolExpressionAdapter();
			}
			@Override
			public Adapter caseBPELDeadlineExpression(BPELDeadlineExpression object) {
				return createBPELDeadlineExpressionAdapter();
			}
			@Override
			public Adapter caseBPELDurationExpression(BPELDurationExpression object) {
				return createBPELDurationExpressionAdapter();
			}
			@Override
			public Adapter caseBPELUnsignedIntegerExpression(BPELUnsignedIntegerExpression object) {
				return createBPELUnsignedIntegerExpressionAdapter();
			}
			@Override
			public Adapter caseBPELScopeElement(BPELScopeElement object) {
				return createBPELScopeElementAdapter();
			}
			@Override
			public Adapter caseBPELProcess(BPELProcess object) {
				return createBPELProcessAdapter();
			}
			@Override
			public Adapter caseBPELImport(BPELImport object) {
				return createBPELImportAdapter();
			}
			@Override
			public Adapter caseBPELPartnerLink(BPELPartnerLink object) {
				return createBPELPartnerLinkAdapter();
			}
			@Override
			public Adapter caseBPELMessageExchange(BPELMessageExchange object) {
				return createBPELMessageExchangeAdapter();
			}
			@Override
			public Adapter caseBPELVariable(BPELVariable object) {
				return createBPELVariableAdapter();
			}
			@Override
			public Adapter caseBPELCorrelationSet(BPELCorrelationSet object) {
				return createBPELCorrelationSetAdapter();
			}
			@Override
			public Adapter caseBPELActivity(BPELActivity object) {
				return createBPELActivityAdapter();
			}
			@Override
			public Adapter caseBPELTargetList(BPELTargetList object) {
				return createBPELTargetListAdapter();
			}
			@Override
			public Adapter caseBPELTarget(BPELTarget object) {
				return createBPELTargetAdapter();
			}
			@Override
			public Adapter caseBPELSourceList(BPELSourceList object) {
				return createBPELSourceListAdapter();
			}
			@Override
			public Adapter caseBPELSource(BPELSource object) {
				return createBPELSourceAdapter();
			}
			@Override
			public Adapter caseBPELInvoke(BPELInvoke object) {
				return createBPELInvokeAdapter();
			}
			@Override
			public Adapter caseBPELReceive(BPELReceive object) {
				return createBPELReceiveAdapter();
			}
			@Override
			public Adapter caseBPELReply(BPELReply object) {
				return createBPELReplyAdapter();
			}
			@Override
			public Adapter caseBPELAssign(BPELAssign object) {
				return createBPELAssignAdapter();
			}
			@Override
			public Adapter caseBPELAssignOperation(BPELAssignOperation object) {
				return createBPELAssignOperationAdapter();
			}
			@Override
			public Adapter caseBPELCopy(BPELCopy object) {
				return createBPELCopyAdapter();
			}
			@Override
			public Adapter caseBPELCopyFrom(BPELCopyFrom object) {
				return createBPELCopyFromAdapter();
			}
			@Override
			public Adapter caseBPELCopyTo(BPELCopyTo object) {
				return createBPELCopyToAdapter();
			}
			@Override
			public Adapter caseBPELLiteral(BPELLiteral object) {
				return createBPELLiteralAdapter();
			}
			@Override
			public Adapter caseBPELQuery(BPELQuery object) {
				return createBPELQueryAdapter();
			}
			@Override
			public Adapter caseBPELThrow(BPELThrow object) {
				return createBPELThrowAdapter();
			}
			@Override
			public Adapter caseBPELRethrow(BPELRethrow object) {
				return createBPELRethrowAdapter();
			}
			@Override
			public Adapter caseBPELWait(BPELWait object) {
				return createBPELWaitAdapter();
			}
			@Override
			public Adapter caseBPELEmpty(BPELEmpty object) {
				return createBPELEmptyAdapter();
			}
			@Override
			public Adapter caseBPELExit(BPELExit object) {
				return createBPELExitAdapter();
			}
			@Override
			public Adapter caseBPELSequence(BPELSequence object) {
				return createBPELSequenceAdapter();
			}
			@Override
			public Adapter caseBPELIf(BPELIf object) {
				return createBPELIfAdapter();
			}
			@Override
			public Adapter caseBPELElseIf(BPELElseIf object) {
				return createBPELElseIfAdapter();
			}
			@Override
			public Adapter caseBPELWhile(BPELWhile object) {
				return createBPELWhileAdapter();
			}
			@Override
			public Adapter caseBPELRepeatUntil(BPELRepeatUntil object) {
				return createBPELRepeatUntilAdapter();
			}
			@Override
			public Adapter caseBPELPick(BPELPick object) {
				return createBPELPickAdapter();
			}
			@Override
			public Adapter caseBPELFlow(BPELFlow object) {
				return createBPELFlowAdapter();
			}
			@Override
			public Adapter caseBPELLink(BPELLink object) {
				return createBPELLinkAdapter();
			}
			@Override
			public Adapter caseBPELForEach(BPELForEach object) {
				return createBPELForEachAdapter();
			}
			@Override
			public Adapter caseBPELScope(BPELScope object) {
				return createBPELScopeAdapter();
			}
			@Override
			public Adapter caseVxBPELVariationPoint(VxBPELVariationPoint object) {
				return createVxBPELVariationPointAdapter();
			}
			@Override
			public Adapter caseVxBPELVariant(VxBPELVariant object) {
				return createVxBPELVariantAdapter();
			}
			@Override
			public Adapter caseVxBPELConfigurableVariationPoint(VxBPELConfigurableVariationPoint object) {
				return createVxBPELConfigurableVariationPointAdapter();
			}
			@Override
			public Adapter caseVxBPELConfigurableVariant(VxBPELConfigurableVariant object) {
				return createVxBPELConfigurableVariantAdapter();
			}
			@Override
			public Adapter caseVxBPELVariationPointPChoice(VxBPELVariationPointPChoice object) {
				return createVxBPELVariationPointPChoiceAdapter();
			}
			@Override
			public Adapter caseVxBPELProcess(VxBPELProcess object) {
				return createVxBPELProcessAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link edu.ustb.sei.mde.vxbpel.NamedElement <em>Named Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ustb.sei.mde.vxbpel.NamedElement
	 * @generated
	 */
	public Adapter createNamedElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ustb.sei.mde.vxbpel.XMLAttributeContainer <em>XML Attribute Container</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ustb.sei.mde.vxbpel.XMLAttributeContainer
	 * @generated
	 */
	public Adapter createXMLAttributeContainerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ustb.sei.mde.vxbpel.XMLAttribute <em>XML Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ustb.sei.mde.vxbpel.XMLAttribute
	 * @generated
	 */
	public Adapter createXMLAttributeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ustb.sei.mde.vxbpel.BPELExpression <em>BPEL Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ustb.sei.mde.vxbpel.BPELExpression
	 * @generated
	 */
	public Adapter createBPELExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ustb.sei.mde.vxbpel.BPELBoolExpression <em>BPEL Bool Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ustb.sei.mde.vxbpel.BPELBoolExpression
	 * @generated
	 */
	public Adapter createBPELBoolExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ustb.sei.mde.vxbpel.BPELDeadlineExpression <em>BPEL Deadline Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ustb.sei.mde.vxbpel.BPELDeadlineExpression
	 * @generated
	 */
	public Adapter createBPELDeadlineExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ustb.sei.mde.vxbpel.BPELDurationExpression <em>BPEL Duration Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ustb.sei.mde.vxbpel.BPELDurationExpression
	 * @generated
	 */
	public Adapter createBPELDurationExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ustb.sei.mde.vxbpel.BPELUnsignedIntegerExpression <em>BPEL Unsigned Integer Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ustb.sei.mde.vxbpel.BPELUnsignedIntegerExpression
	 * @generated
	 */
	public Adapter createBPELUnsignedIntegerExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ustb.sei.mde.vxbpel.BPELScopeElement <em>BPEL Scope Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ustb.sei.mde.vxbpel.BPELScopeElement
	 * @generated
	 */
	public Adapter createBPELScopeElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ustb.sei.mde.vxbpel.BPELProcess <em>BPEL Process</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ustb.sei.mde.vxbpel.BPELProcess
	 * @generated
	 */
	public Adapter createBPELProcessAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ustb.sei.mde.vxbpel.BPELImport <em>BPEL Import</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ustb.sei.mde.vxbpel.BPELImport
	 * @generated
	 */
	public Adapter createBPELImportAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ustb.sei.mde.vxbpel.BPELPartnerLink <em>BPEL Partner Link</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ustb.sei.mde.vxbpel.BPELPartnerLink
	 * @generated
	 */
	public Adapter createBPELPartnerLinkAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ustb.sei.mde.vxbpel.BPELMessageExchange <em>BPEL Message Exchange</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ustb.sei.mde.vxbpel.BPELMessageExchange
	 * @generated
	 */
	public Adapter createBPELMessageExchangeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ustb.sei.mde.vxbpel.BPELVariable <em>BPEL Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ustb.sei.mde.vxbpel.BPELVariable
	 * @generated
	 */
	public Adapter createBPELVariableAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ustb.sei.mde.vxbpel.BPELCorrelationSet <em>BPEL Correlation Set</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ustb.sei.mde.vxbpel.BPELCorrelationSet
	 * @generated
	 */
	public Adapter createBPELCorrelationSetAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ustb.sei.mde.vxbpel.BPELActivity <em>BPEL Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ustb.sei.mde.vxbpel.BPELActivity
	 * @generated
	 */
	public Adapter createBPELActivityAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ustb.sei.mde.vxbpel.BPELTargetList <em>BPEL Target List</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ustb.sei.mde.vxbpel.BPELTargetList
	 * @generated
	 */
	public Adapter createBPELTargetListAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ustb.sei.mde.vxbpel.BPELTarget <em>BPEL Target</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ustb.sei.mde.vxbpel.BPELTarget
	 * @generated
	 */
	public Adapter createBPELTargetAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ustb.sei.mde.vxbpel.BPELSourceList <em>BPEL Source List</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ustb.sei.mde.vxbpel.BPELSourceList
	 * @generated
	 */
	public Adapter createBPELSourceListAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ustb.sei.mde.vxbpel.BPELSource <em>BPEL Source</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ustb.sei.mde.vxbpel.BPELSource
	 * @generated
	 */
	public Adapter createBPELSourceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ustb.sei.mde.vxbpel.BPELInvoke <em>BPEL Invoke</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ustb.sei.mde.vxbpel.BPELInvoke
	 * @generated
	 */
	public Adapter createBPELInvokeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ustb.sei.mde.vxbpel.BPELReceive <em>BPEL Receive</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ustb.sei.mde.vxbpel.BPELReceive
	 * @generated
	 */
	public Adapter createBPELReceiveAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ustb.sei.mde.vxbpel.BPELReply <em>BPEL Reply</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ustb.sei.mde.vxbpel.BPELReply
	 * @generated
	 */
	public Adapter createBPELReplyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ustb.sei.mde.vxbpel.BPELAssign <em>BPEL Assign</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ustb.sei.mde.vxbpel.BPELAssign
	 * @generated
	 */
	public Adapter createBPELAssignAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ustb.sei.mde.vxbpel.BPELAssignOperation <em>BPEL Assign Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ustb.sei.mde.vxbpel.BPELAssignOperation
	 * @generated
	 */
	public Adapter createBPELAssignOperationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ustb.sei.mde.vxbpel.BPELCopy <em>BPEL Copy</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ustb.sei.mde.vxbpel.BPELCopy
	 * @generated
	 */
	public Adapter createBPELCopyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ustb.sei.mde.vxbpel.BPELCopyFrom <em>BPEL Copy From</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ustb.sei.mde.vxbpel.BPELCopyFrom
	 * @generated
	 */
	public Adapter createBPELCopyFromAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ustb.sei.mde.vxbpel.BPELCopyTo <em>BPEL Copy To</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ustb.sei.mde.vxbpel.BPELCopyTo
	 * @generated
	 */
	public Adapter createBPELCopyToAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ustb.sei.mde.vxbpel.BPELLiteral <em>BPEL Literal</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ustb.sei.mde.vxbpel.BPELLiteral
	 * @generated
	 */
	public Adapter createBPELLiteralAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ustb.sei.mde.vxbpel.BPELQuery <em>BPEL Query</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ustb.sei.mde.vxbpel.BPELQuery
	 * @generated
	 */
	public Adapter createBPELQueryAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ustb.sei.mde.vxbpel.BPELThrow <em>BPEL Throw</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ustb.sei.mde.vxbpel.BPELThrow
	 * @generated
	 */
	public Adapter createBPELThrowAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ustb.sei.mde.vxbpel.BPELRethrow <em>BPEL Rethrow</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ustb.sei.mde.vxbpel.BPELRethrow
	 * @generated
	 */
	public Adapter createBPELRethrowAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ustb.sei.mde.vxbpel.BPELWait <em>BPEL Wait</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ustb.sei.mde.vxbpel.BPELWait
	 * @generated
	 */
	public Adapter createBPELWaitAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ustb.sei.mde.vxbpel.BPELEmpty <em>BPEL Empty</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ustb.sei.mde.vxbpel.BPELEmpty
	 * @generated
	 */
	public Adapter createBPELEmptyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ustb.sei.mde.vxbpel.BPELExit <em>BPEL Exit</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ustb.sei.mde.vxbpel.BPELExit
	 * @generated
	 */
	public Adapter createBPELExitAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ustb.sei.mde.vxbpel.BPELSequence <em>BPEL Sequence</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ustb.sei.mde.vxbpel.BPELSequence
	 * @generated
	 */
	public Adapter createBPELSequenceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ustb.sei.mde.vxbpel.BPELIf <em>BPEL If</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ustb.sei.mde.vxbpel.BPELIf
	 * @generated
	 */
	public Adapter createBPELIfAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ustb.sei.mde.vxbpel.BPELElseIf <em>BPEL Else If</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ustb.sei.mde.vxbpel.BPELElseIf
	 * @generated
	 */
	public Adapter createBPELElseIfAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ustb.sei.mde.vxbpel.BPELWhile <em>BPEL While</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ustb.sei.mde.vxbpel.BPELWhile
	 * @generated
	 */
	public Adapter createBPELWhileAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ustb.sei.mde.vxbpel.BPELRepeatUntil <em>BPEL Repeat Until</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ustb.sei.mde.vxbpel.BPELRepeatUntil
	 * @generated
	 */
	public Adapter createBPELRepeatUntilAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ustb.sei.mde.vxbpel.BPELPick <em>BPEL Pick</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ustb.sei.mde.vxbpel.BPELPick
	 * @generated
	 */
	public Adapter createBPELPickAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ustb.sei.mde.vxbpel.BPELFlow <em>BPEL Flow</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ustb.sei.mde.vxbpel.BPELFlow
	 * @generated
	 */
	public Adapter createBPELFlowAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ustb.sei.mde.vxbpel.BPELLink <em>BPEL Link</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ustb.sei.mde.vxbpel.BPELLink
	 * @generated
	 */
	public Adapter createBPELLinkAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ustb.sei.mde.vxbpel.BPELForEach <em>BPEL For Each</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ustb.sei.mde.vxbpel.BPELForEach
	 * @generated
	 */
	public Adapter createBPELForEachAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ustb.sei.mde.vxbpel.BPELScope <em>BPEL Scope</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ustb.sei.mde.vxbpel.BPELScope
	 * @generated
	 */
	public Adapter createBPELScopeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ustb.sei.mde.vxbpel.VxBPELVariationPoint <em>Variation Point</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELVariationPoint
	 * @generated
	 */
	public Adapter createVxBPELVariationPointAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ustb.sei.mde.vxbpel.VxBPELVariant <em>Variant</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELVariant
	 * @generated
	 */
	public Adapter createVxBPELVariantAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ustb.sei.mde.vxbpel.VxBPELConfigurableVariationPoint <em>Configurable Variation Point</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELConfigurableVariationPoint
	 * @generated
	 */
	public Adapter createVxBPELConfigurableVariationPointAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ustb.sei.mde.vxbpel.VxBPELConfigurableVariant <em>Configurable Variant</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELConfigurableVariant
	 * @generated
	 */
	public Adapter createVxBPELConfigurableVariantAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ustb.sei.mde.vxbpel.VxBPELVariationPointPChoice <em>Variation Point PChoice</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELVariationPointPChoice
	 * @generated
	 */
	public Adapter createVxBPELVariationPointPChoiceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link edu.ustb.sei.mde.vxbpel.VxBPELProcess <em>Process</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELProcess
	 * @generated
	 */
	public Adapter createVxBPELProcessAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //VxBPELAdapterFactory
