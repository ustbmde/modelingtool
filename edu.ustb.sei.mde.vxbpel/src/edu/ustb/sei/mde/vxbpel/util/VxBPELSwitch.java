/**
 */
package edu.ustb.sei.mde.vxbpel.util;

import edu.ustb.sei.mde.vxbpel.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage
 * @generated
 */
public class VxBPELSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static VxBPELPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VxBPELSwitch() {
		if (modelPackage == null) {
			modelPackage = VxBPELPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @parameter ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case VxBPELPackage.NAMED_ELEMENT: {
				NamedElement namedElement = (NamedElement)theEObject;
				T result = caseNamedElement(namedElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VxBPELPackage.XML_ATTRIBUTE_CONTAINER: {
				XMLAttributeContainer xmlAttributeContainer = (XMLAttributeContainer)theEObject;
				T result = caseXMLAttributeContainer(xmlAttributeContainer);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VxBPELPackage.XML_ATTRIBUTE: {
				XMLAttribute xmlAttribute = (XMLAttribute)theEObject;
				T result = caseXMLAttribute(xmlAttribute);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VxBPELPackage.BPEL_EXPRESSION: {
				BPELExpression bpelExpression = (BPELExpression)theEObject;
				T result = caseBPELExpression(bpelExpression);
				if (result == null) result = caseXMLAttributeContainer(bpelExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VxBPELPackage.BPEL_BOOL_EXPRESSION: {
				BPELBoolExpression bpelBoolExpression = (BPELBoolExpression)theEObject;
				T result = caseBPELBoolExpression(bpelBoolExpression);
				if (result == null) result = caseBPELExpression(bpelBoolExpression);
				if (result == null) result = caseXMLAttributeContainer(bpelBoolExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VxBPELPackage.BPEL_DEADLINE_EXPRESSION: {
				BPELDeadlineExpression bpelDeadlineExpression = (BPELDeadlineExpression)theEObject;
				T result = caseBPELDeadlineExpression(bpelDeadlineExpression);
				if (result == null) result = caseBPELExpression(bpelDeadlineExpression);
				if (result == null) result = caseXMLAttributeContainer(bpelDeadlineExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VxBPELPackage.BPEL_DURATION_EXPRESSION: {
				BPELDurationExpression bpelDurationExpression = (BPELDurationExpression)theEObject;
				T result = caseBPELDurationExpression(bpelDurationExpression);
				if (result == null) result = caseBPELExpression(bpelDurationExpression);
				if (result == null) result = caseXMLAttributeContainer(bpelDurationExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VxBPELPackage.BPEL_UNSIGNED_INTEGER_EXPRESSION: {
				BPELUnsignedIntegerExpression bpelUnsignedIntegerExpression = (BPELUnsignedIntegerExpression)theEObject;
				T result = caseBPELUnsignedIntegerExpression(bpelUnsignedIntegerExpression);
				if (result == null) result = caseBPELExpression(bpelUnsignedIntegerExpression);
				if (result == null) result = caseXMLAttributeContainer(bpelUnsignedIntegerExpression);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VxBPELPackage.BPEL_SCOPE_ELEMENT: {
				BPELScopeElement bpelScopeElement = (BPELScopeElement)theEObject;
				T result = caseBPELScopeElement(bpelScopeElement);
				if (result == null) result = caseXMLAttributeContainer(bpelScopeElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VxBPELPackage.BPEL_PROCESS: {
				BPELProcess bpelProcess = (BPELProcess)theEObject;
				T result = caseBPELProcess(bpelProcess);
				if (result == null) result = caseNamedElement(bpelProcess);
				if (result == null) result = caseBPELScopeElement(bpelProcess);
				if (result == null) result = caseXMLAttributeContainer(bpelProcess);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VxBPELPackage.BPEL_IMPORT: {
				BPELImport bpelImport = (BPELImport)theEObject;
				T result = caseBPELImport(bpelImport);
				if (result == null) result = caseXMLAttributeContainer(bpelImport);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VxBPELPackage.BPEL_PARTNER_LINK: {
				BPELPartnerLink bpelPartnerLink = (BPELPartnerLink)theEObject;
				T result = caseBPELPartnerLink(bpelPartnerLink);
				if (result == null) result = caseXMLAttributeContainer(bpelPartnerLink);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VxBPELPackage.BPEL_MESSAGE_EXCHANGE: {
				BPELMessageExchange bpelMessageExchange = (BPELMessageExchange)theEObject;
				T result = caseBPELMessageExchange(bpelMessageExchange);
				if (result == null) result = caseXMLAttributeContainer(bpelMessageExchange);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VxBPELPackage.BPEL_VARIABLE: {
				BPELVariable bpelVariable = (BPELVariable)theEObject;
				T result = caseBPELVariable(bpelVariable);
				if (result == null) result = caseXMLAttributeContainer(bpelVariable);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VxBPELPackage.BPEL_CORRELATION_SET: {
				BPELCorrelationSet bpelCorrelationSet = (BPELCorrelationSet)theEObject;
				T result = caseBPELCorrelationSet(bpelCorrelationSet);
				if (result == null) result = caseXMLAttributeContainer(bpelCorrelationSet);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VxBPELPackage.BPEL_ACTIVITY: {
				BPELActivity bpelActivity = (BPELActivity)theEObject;
				T result = caseBPELActivity(bpelActivity);
				if (result == null) result = caseNamedElement(bpelActivity);
				if (result == null) result = caseXMLAttributeContainer(bpelActivity);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VxBPELPackage.BPEL_TARGET_LIST: {
				BPELTargetList bpelTargetList = (BPELTargetList)theEObject;
				T result = caseBPELTargetList(bpelTargetList);
				if (result == null) result = caseXMLAttributeContainer(bpelTargetList);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VxBPELPackage.BPEL_TARGET: {
				BPELTarget bpelTarget = (BPELTarget)theEObject;
				T result = caseBPELTarget(bpelTarget);
				if (result == null) result = caseXMLAttributeContainer(bpelTarget);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VxBPELPackage.BPEL_SOURCE_LIST: {
				BPELSourceList bpelSourceList = (BPELSourceList)theEObject;
				T result = caseBPELSourceList(bpelSourceList);
				if (result == null) result = caseXMLAttributeContainer(bpelSourceList);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VxBPELPackage.BPEL_SOURCE: {
				BPELSource bpelSource = (BPELSource)theEObject;
				T result = caseBPELSource(bpelSource);
				if (result == null) result = caseXMLAttributeContainer(bpelSource);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VxBPELPackage.BPEL_INVOKE: {
				BPELInvoke bpelInvoke = (BPELInvoke)theEObject;
				T result = caseBPELInvoke(bpelInvoke);
				if (result == null) result = caseBPELActivity(bpelInvoke);
				if (result == null) result = caseNamedElement(bpelInvoke);
				if (result == null) result = caseXMLAttributeContainer(bpelInvoke);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VxBPELPackage.BPEL_RECEIVE: {
				BPELReceive bpelReceive = (BPELReceive)theEObject;
				T result = caseBPELReceive(bpelReceive);
				if (result == null) result = caseBPELActivity(bpelReceive);
				if (result == null) result = caseNamedElement(bpelReceive);
				if (result == null) result = caseXMLAttributeContainer(bpelReceive);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VxBPELPackage.BPEL_REPLY: {
				BPELReply bpelReply = (BPELReply)theEObject;
				T result = caseBPELReply(bpelReply);
				if (result == null) result = caseBPELActivity(bpelReply);
				if (result == null) result = caseNamedElement(bpelReply);
				if (result == null) result = caseXMLAttributeContainer(bpelReply);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VxBPELPackage.BPEL_ASSIGN: {
				BPELAssign bpelAssign = (BPELAssign)theEObject;
				T result = caseBPELAssign(bpelAssign);
				if (result == null) result = caseBPELActivity(bpelAssign);
				if (result == null) result = caseNamedElement(bpelAssign);
				if (result == null) result = caseXMLAttributeContainer(bpelAssign);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VxBPELPackage.BPEL_ASSIGN_OPERATION: {
				BPELAssignOperation bpelAssignOperation = (BPELAssignOperation)theEObject;
				T result = caseBPELAssignOperation(bpelAssignOperation);
				if (result == null) result = caseXMLAttributeContainer(bpelAssignOperation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VxBPELPackage.BPEL_COPY: {
				BPELCopy bpelCopy = (BPELCopy)theEObject;
				T result = caseBPELCopy(bpelCopy);
				if (result == null) result = caseBPELAssignOperation(bpelCopy);
				if (result == null) result = caseXMLAttributeContainer(bpelCopy);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VxBPELPackage.BPEL_COPY_FROM: {
				BPELCopyFrom bpelCopyFrom = (BPELCopyFrom)theEObject;
				T result = caseBPELCopyFrom(bpelCopyFrom);
				if (result == null) result = caseXMLAttributeContainer(bpelCopyFrom);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VxBPELPackage.BPEL_COPY_TO: {
				BPELCopyTo bpelCopyTo = (BPELCopyTo)theEObject;
				T result = caseBPELCopyTo(bpelCopyTo);
				if (result == null) result = caseXMLAttributeContainer(bpelCopyTo);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VxBPELPackage.BPEL_LITERAL: {
				BPELLiteral bpelLiteral = (BPELLiteral)theEObject;
				T result = caseBPELLiteral(bpelLiteral);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VxBPELPackage.BPEL_QUERY: {
				BPELQuery bpelQuery = (BPELQuery)theEObject;
				T result = caseBPELQuery(bpelQuery);
				if (result == null) result = caseXMLAttributeContainer(bpelQuery);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VxBPELPackage.BPEL_THROW: {
				BPELThrow bpelThrow = (BPELThrow)theEObject;
				T result = caseBPELThrow(bpelThrow);
				if (result == null) result = caseBPELActivity(bpelThrow);
				if (result == null) result = caseNamedElement(bpelThrow);
				if (result == null) result = caseXMLAttributeContainer(bpelThrow);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VxBPELPackage.BPEL_RETHROW: {
				BPELRethrow bpelRethrow = (BPELRethrow)theEObject;
				T result = caseBPELRethrow(bpelRethrow);
				if (result == null) result = caseBPELActivity(bpelRethrow);
				if (result == null) result = caseNamedElement(bpelRethrow);
				if (result == null) result = caseXMLAttributeContainer(bpelRethrow);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VxBPELPackage.BPEL_WAIT: {
				BPELWait bpelWait = (BPELWait)theEObject;
				T result = caseBPELWait(bpelWait);
				if (result == null) result = caseBPELActivity(bpelWait);
				if (result == null) result = caseNamedElement(bpelWait);
				if (result == null) result = caseXMLAttributeContainer(bpelWait);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VxBPELPackage.BPEL_EMPTY: {
				BPELEmpty bpelEmpty = (BPELEmpty)theEObject;
				T result = caseBPELEmpty(bpelEmpty);
				if (result == null) result = caseBPELActivity(bpelEmpty);
				if (result == null) result = caseNamedElement(bpelEmpty);
				if (result == null) result = caseXMLAttributeContainer(bpelEmpty);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VxBPELPackage.BPEL_EXIT: {
				BPELExit bpelExit = (BPELExit)theEObject;
				T result = caseBPELExit(bpelExit);
				if (result == null) result = caseBPELActivity(bpelExit);
				if (result == null) result = caseNamedElement(bpelExit);
				if (result == null) result = caseXMLAttributeContainer(bpelExit);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VxBPELPackage.BPEL_SEQUENCE: {
				BPELSequence bpelSequence = (BPELSequence)theEObject;
				T result = caseBPELSequence(bpelSequence);
				if (result == null) result = caseBPELActivity(bpelSequence);
				if (result == null) result = caseNamedElement(bpelSequence);
				if (result == null) result = caseXMLAttributeContainer(bpelSequence);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VxBPELPackage.BPEL_IF: {
				BPELIf bpelIf = (BPELIf)theEObject;
				T result = caseBPELIf(bpelIf);
				if (result == null) result = caseBPELActivity(bpelIf);
				if (result == null) result = caseNamedElement(bpelIf);
				if (result == null) result = caseXMLAttributeContainer(bpelIf);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VxBPELPackage.BPEL_ELSE_IF: {
				BPELElseIf bpelElseIf = (BPELElseIf)theEObject;
				T result = caseBPELElseIf(bpelElseIf);
				if (result == null) result = caseBPELActivity(bpelElseIf);
				if (result == null) result = caseNamedElement(bpelElseIf);
				if (result == null) result = caseXMLAttributeContainer(bpelElseIf);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VxBPELPackage.BPEL_WHILE: {
				BPELWhile bpelWhile = (BPELWhile)theEObject;
				T result = caseBPELWhile(bpelWhile);
				if (result == null) result = caseBPELActivity(bpelWhile);
				if (result == null) result = caseNamedElement(bpelWhile);
				if (result == null) result = caseXMLAttributeContainer(bpelWhile);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VxBPELPackage.BPEL_REPEAT_UNTIL: {
				BPELRepeatUntil bpelRepeatUntil = (BPELRepeatUntil)theEObject;
				T result = caseBPELRepeatUntil(bpelRepeatUntil);
				if (result == null) result = caseBPELActivity(bpelRepeatUntil);
				if (result == null) result = caseNamedElement(bpelRepeatUntil);
				if (result == null) result = caseXMLAttributeContainer(bpelRepeatUntil);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VxBPELPackage.BPEL_PICK: {
				BPELPick bpelPick = (BPELPick)theEObject;
				T result = caseBPELPick(bpelPick);
				if (result == null) result = caseBPELActivity(bpelPick);
				if (result == null) result = caseNamedElement(bpelPick);
				if (result == null) result = caseXMLAttributeContainer(bpelPick);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VxBPELPackage.BPEL_FLOW: {
				BPELFlow bpelFlow = (BPELFlow)theEObject;
				T result = caseBPELFlow(bpelFlow);
				if (result == null) result = caseBPELActivity(bpelFlow);
				if (result == null) result = caseNamedElement(bpelFlow);
				if (result == null) result = caseXMLAttributeContainer(bpelFlow);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VxBPELPackage.BPEL_LINK: {
				BPELLink bpelLink = (BPELLink)theEObject;
				T result = caseBPELLink(bpelLink);
				if (result == null) result = caseNamedElement(bpelLink);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VxBPELPackage.BPEL_FOR_EACH: {
				BPELForEach bpelForEach = (BPELForEach)theEObject;
				T result = caseBPELForEach(bpelForEach);
				if (result == null) result = caseBPELActivity(bpelForEach);
				if (result == null) result = caseNamedElement(bpelForEach);
				if (result == null) result = caseXMLAttributeContainer(bpelForEach);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VxBPELPackage.BPEL_SCOPE: {
				BPELScope bpelScope = (BPELScope)theEObject;
				T result = caseBPELScope(bpelScope);
				if (result == null) result = caseBPELActivity(bpelScope);
				if (result == null) result = caseBPELScopeElement(bpelScope);
				if (result == null) result = caseNamedElement(bpelScope);
				if (result == null) result = caseXMLAttributeContainer(bpelScope);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VxBPELPackage.VX_BPEL_VARIATION_POINT: {
				VxBPELVariationPoint vxBPELVariationPoint = (VxBPELVariationPoint)theEObject;
				T result = caseVxBPELVariationPoint(vxBPELVariationPoint);
				if (result == null) result = caseBPELActivity(vxBPELVariationPoint);
				if (result == null) result = caseNamedElement(vxBPELVariationPoint);
				if (result == null) result = caseXMLAttributeContainer(vxBPELVariationPoint);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VxBPELPackage.VX_BPEL_VARIANT: {
				VxBPELVariant vxBPELVariant = (VxBPELVariant)theEObject;
				T result = caseVxBPELVariant(vxBPELVariant);
				if (result == null) result = caseNamedElement(vxBPELVariant);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VxBPELPackage.VX_BPEL_CONFIGURABLE_VARIATION_POINT: {
				VxBPELConfigurableVariationPoint vxBPELConfigurableVariationPoint = (VxBPELConfigurableVariationPoint)theEObject;
				T result = caseVxBPELConfigurableVariationPoint(vxBPELConfigurableVariationPoint);
				if (result == null) result = caseXMLAttributeContainer(vxBPELConfigurableVariationPoint);
				if (result == null) result = caseNamedElement(vxBPELConfigurableVariationPoint);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VxBPELPackage.VX_BPEL_CONFIGURABLE_VARIANT: {
				VxBPELConfigurableVariant vxBPELConfigurableVariant = (VxBPELConfigurableVariant)theEObject;
				T result = caseVxBPELConfigurableVariant(vxBPELConfigurableVariant);
				if (result == null) result = caseNamedElement(vxBPELConfigurableVariant);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VxBPELPackage.VX_BPEL_VARIATION_POINT_PCHOICE: {
				VxBPELVariationPointPChoice vxBPELVariationPointPChoice = (VxBPELVariationPointPChoice)theEObject;
				T result = caseVxBPELVariationPointPChoice(vxBPELVariationPointPChoice);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case VxBPELPackage.VX_BPEL_PROCESS: {
				VxBPELProcess vxBPELProcess = (VxBPELProcess)theEObject;
				T result = caseVxBPELProcess(vxBPELProcess);
				if (result == null) result = caseBPELProcess(vxBPELProcess);
				if (result == null) result = caseNamedElement(vxBPELProcess);
				if (result == null) result = caseBPELScopeElement(vxBPELProcess);
				if (result == null) result = caseXMLAttributeContainer(vxBPELProcess);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Named Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Named Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNamedElement(NamedElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>XML Attribute Container</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>XML Attribute Container</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseXMLAttributeContainer(XMLAttributeContainer object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>XML Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>XML Attribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseXMLAttribute(XMLAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>BPEL Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>BPEL Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBPELExpression(BPELExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>BPEL Bool Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>BPEL Bool Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBPELBoolExpression(BPELBoolExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>BPEL Deadline Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>BPEL Deadline Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBPELDeadlineExpression(BPELDeadlineExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>BPEL Duration Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>BPEL Duration Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBPELDurationExpression(BPELDurationExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>BPEL Unsigned Integer Expression</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>BPEL Unsigned Integer Expression</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBPELUnsignedIntegerExpression(BPELUnsignedIntegerExpression object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>BPEL Scope Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>BPEL Scope Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBPELScopeElement(BPELScopeElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>BPEL Process</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>BPEL Process</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBPELProcess(BPELProcess object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>BPEL Import</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>BPEL Import</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBPELImport(BPELImport object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>BPEL Partner Link</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>BPEL Partner Link</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBPELPartnerLink(BPELPartnerLink object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>BPEL Message Exchange</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>BPEL Message Exchange</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBPELMessageExchange(BPELMessageExchange object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>BPEL Variable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>BPEL Variable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBPELVariable(BPELVariable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>BPEL Correlation Set</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>BPEL Correlation Set</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBPELCorrelationSet(BPELCorrelationSet object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>BPEL Activity</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>BPEL Activity</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBPELActivity(BPELActivity object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>BPEL Target List</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>BPEL Target List</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBPELTargetList(BPELTargetList object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>BPEL Target</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>BPEL Target</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBPELTarget(BPELTarget object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>BPEL Source List</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>BPEL Source List</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBPELSourceList(BPELSourceList object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>BPEL Source</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>BPEL Source</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBPELSource(BPELSource object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>BPEL Invoke</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>BPEL Invoke</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBPELInvoke(BPELInvoke object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>BPEL Receive</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>BPEL Receive</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBPELReceive(BPELReceive object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>BPEL Reply</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>BPEL Reply</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBPELReply(BPELReply object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>BPEL Assign</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>BPEL Assign</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBPELAssign(BPELAssign object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>BPEL Assign Operation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>BPEL Assign Operation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBPELAssignOperation(BPELAssignOperation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>BPEL Copy</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>BPEL Copy</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBPELCopy(BPELCopy object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>BPEL Copy From</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>BPEL Copy From</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBPELCopyFrom(BPELCopyFrom object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>BPEL Copy To</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>BPEL Copy To</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBPELCopyTo(BPELCopyTo object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>BPEL Literal</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>BPEL Literal</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBPELLiteral(BPELLiteral object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>BPEL Query</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>BPEL Query</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBPELQuery(BPELQuery object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>BPEL Throw</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>BPEL Throw</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBPELThrow(BPELThrow object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>BPEL Rethrow</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>BPEL Rethrow</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBPELRethrow(BPELRethrow object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>BPEL Wait</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>BPEL Wait</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBPELWait(BPELWait object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>BPEL Empty</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>BPEL Empty</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBPELEmpty(BPELEmpty object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>BPEL Exit</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>BPEL Exit</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBPELExit(BPELExit object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>BPEL Sequence</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>BPEL Sequence</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBPELSequence(BPELSequence object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>BPEL If</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>BPEL If</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBPELIf(BPELIf object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>BPEL Else If</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>BPEL Else If</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBPELElseIf(BPELElseIf object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>BPEL While</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>BPEL While</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBPELWhile(BPELWhile object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>BPEL Repeat Until</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>BPEL Repeat Until</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBPELRepeatUntil(BPELRepeatUntil object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>BPEL Pick</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>BPEL Pick</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBPELPick(BPELPick object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>BPEL Flow</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>BPEL Flow</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBPELFlow(BPELFlow object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>BPEL Link</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>BPEL Link</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBPELLink(BPELLink object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>BPEL For Each</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>BPEL For Each</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBPELForEach(BPELForEach object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>BPEL Scope</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>BPEL Scope</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBPELScope(BPELScope object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Variation Point</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Variation Point</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVxBPELVariationPoint(VxBPELVariationPoint object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Variant</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Variant</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVxBPELVariant(VxBPELVariant object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Configurable Variation Point</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Configurable Variation Point</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVxBPELConfigurableVariationPoint(VxBPELConfigurableVariationPoint object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Configurable Variant</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Configurable Variant</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVxBPELConfigurableVariant(VxBPELConfigurableVariant object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Variation Point PChoice</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Variation Point PChoice</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVxBPELVariationPointPChoice(VxBPELVariationPointPChoice object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Process</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Process</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVxBPELProcess(VxBPELProcess object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //VxBPELSwitch
