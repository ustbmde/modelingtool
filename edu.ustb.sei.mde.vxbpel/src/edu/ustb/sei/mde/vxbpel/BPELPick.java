/**
 */
package edu.ustb.sei.mde.vxbpel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>BPEL Pick</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELPick()
 * @model
 * @generated
 */
public interface BPELPick extends BPELActivity {
} // BPELPick
