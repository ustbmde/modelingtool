/**
 */
package edu.ustb.sei.mde.vxbpel;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage
 * @generated
 */
public interface VxBPELFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	VxBPELFactory eINSTANCE = edu.ustb.sei.mde.vxbpel.impl.VxBPELFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>XML Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>XML Attribute</em>'.
	 * @generated
	 */
	XMLAttribute createXMLAttribute();

	/**
	 * Returns a new object of class '<em>BPEL Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>BPEL Expression</em>'.
	 * @generated
	 */
	BPELExpression createBPELExpression();

	/**
	 * Returns a new object of class '<em>BPEL Bool Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>BPEL Bool Expression</em>'.
	 * @generated
	 */
	BPELBoolExpression createBPELBoolExpression();

	/**
	 * Returns a new object of class '<em>BPEL Deadline Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>BPEL Deadline Expression</em>'.
	 * @generated
	 */
	BPELDeadlineExpression createBPELDeadlineExpression();

	/**
	 * Returns a new object of class '<em>BPEL Duration Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>BPEL Duration Expression</em>'.
	 * @generated
	 */
	BPELDurationExpression createBPELDurationExpression();

	/**
	 * Returns a new object of class '<em>BPEL Unsigned Integer Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>BPEL Unsigned Integer Expression</em>'.
	 * @generated
	 */
	BPELUnsignedIntegerExpression createBPELUnsignedIntegerExpression();

	/**
	 * Returns a new object of class '<em>BPEL Process</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>BPEL Process</em>'.
	 * @generated
	 */
	BPELProcess createBPELProcess();

	/**
	 * Returns a new object of class '<em>BPEL Import</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>BPEL Import</em>'.
	 * @generated
	 */
	BPELImport createBPELImport();

	/**
	 * Returns a new object of class '<em>BPEL Partner Link</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>BPEL Partner Link</em>'.
	 * @generated
	 */
	BPELPartnerLink createBPELPartnerLink();

	/**
	 * Returns a new object of class '<em>BPEL Message Exchange</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>BPEL Message Exchange</em>'.
	 * @generated
	 */
	BPELMessageExchange createBPELMessageExchange();

	/**
	 * Returns a new object of class '<em>BPEL Variable</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>BPEL Variable</em>'.
	 * @generated
	 */
	BPELVariable createBPELVariable();

	/**
	 * Returns a new object of class '<em>BPEL Correlation Set</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>BPEL Correlation Set</em>'.
	 * @generated
	 */
	BPELCorrelationSet createBPELCorrelationSet();

	/**
	 * Returns a new object of class '<em>BPEL Target List</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>BPEL Target List</em>'.
	 * @generated
	 */
	BPELTargetList createBPELTargetList();

	/**
	 * Returns a new object of class '<em>BPEL Target</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>BPEL Target</em>'.
	 * @generated
	 */
	BPELTarget createBPELTarget();

	/**
	 * Returns a new object of class '<em>BPEL Source List</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>BPEL Source List</em>'.
	 * @generated
	 */
	BPELSourceList createBPELSourceList();

	/**
	 * Returns a new object of class '<em>BPEL Source</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>BPEL Source</em>'.
	 * @generated
	 */
	BPELSource createBPELSource();

	/**
	 * Returns a new object of class '<em>BPEL Invoke</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>BPEL Invoke</em>'.
	 * @generated
	 */
	BPELInvoke createBPELInvoke();

	/**
	 * Returns a new object of class '<em>BPEL Receive</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>BPEL Receive</em>'.
	 * @generated
	 */
	BPELReceive createBPELReceive();

	/**
	 * Returns a new object of class '<em>BPEL Reply</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>BPEL Reply</em>'.
	 * @generated
	 */
	BPELReply createBPELReply();

	/**
	 * Returns a new object of class '<em>BPEL Assign</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>BPEL Assign</em>'.
	 * @generated
	 */
	BPELAssign createBPELAssign();

	/**
	 * Returns a new object of class '<em>BPEL Copy</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>BPEL Copy</em>'.
	 * @generated
	 */
	BPELCopy createBPELCopy();

	/**
	 * Returns a new object of class '<em>BPEL Copy From</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>BPEL Copy From</em>'.
	 * @generated
	 */
	BPELCopyFrom createBPELCopyFrom();

	/**
	 * Returns a new object of class '<em>BPEL Copy To</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>BPEL Copy To</em>'.
	 * @generated
	 */
	BPELCopyTo createBPELCopyTo();

	/**
	 * Returns a new object of class '<em>BPEL Literal</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>BPEL Literal</em>'.
	 * @generated
	 */
	BPELLiteral createBPELLiteral();

	/**
	 * Returns a new object of class '<em>BPEL Query</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>BPEL Query</em>'.
	 * @generated
	 */
	BPELQuery createBPELQuery();

	/**
	 * Returns a new object of class '<em>BPEL Throw</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>BPEL Throw</em>'.
	 * @generated
	 */
	BPELThrow createBPELThrow();

	/**
	 * Returns a new object of class '<em>BPEL Rethrow</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>BPEL Rethrow</em>'.
	 * @generated
	 */
	BPELRethrow createBPELRethrow();

	/**
	 * Returns a new object of class '<em>BPEL Wait</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>BPEL Wait</em>'.
	 * @generated
	 */
	BPELWait createBPELWait();

	/**
	 * Returns a new object of class '<em>BPEL Empty</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>BPEL Empty</em>'.
	 * @generated
	 */
	BPELEmpty createBPELEmpty();

	/**
	 * Returns a new object of class '<em>BPEL Exit</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>BPEL Exit</em>'.
	 * @generated
	 */
	BPELExit createBPELExit();

	/**
	 * Returns a new object of class '<em>BPEL Sequence</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>BPEL Sequence</em>'.
	 * @generated
	 */
	BPELSequence createBPELSequence();

	/**
	 * Returns a new object of class '<em>BPEL If</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>BPEL If</em>'.
	 * @generated
	 */
	BPELIf createBPELIf();

	/**
	 * Returns a new object of class '<em>BPEL Else If</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>BPEL Else If</em>'.
	 * @generated
	 */
	BPELElseIf createBPELElseIf();

	/**
	 * Returns a new object of class '<em>BPEL While</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>BPEL While</em>'.
	 * @generated
	 */
	BPELWhile createBPELWhile();

	/**
	 * Returns a new object of class '<em>BPEL Repeat Until</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>BPEL Repeat Until</em>'.
	 * @generated
	 */
	BPELRepeatUntil createBPELRepeatUntil();

	/**
	 * Returns a new object of class '<em>BPEL Pick</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>BPEL Pick</em>'.
	 * @generated
	 */
	BPELPick createBPELPick();

	/**
	 * Returns a new object of class '<em>BPEL Flow</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>BPEL Flow</em>'.
	 * @generated
	 */
	BPELFlow createBPELFlow();

	/**
	 * Returns a new object of class '<em>BPEL Link</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>BPEL Link</em>'.
	 * @generated
	 */
	BPELLink createBPELLink();

	/**
	 * Returns a new object of class '<em>BPEL For Each</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>BPEL For Each</em>'.
	 * @generated
	 */
	BPELForEach createBPELForEach();

	/**
	 * Returns a new object of class '<em>BPEL Scope</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>BPEL Scope</em>'.
	 * @generated
	 */
	BPELScope createBPELScope();

	/**
	 * Returns a new object of class '<em>Variation Point</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Variation Point</em>'.
	 * @generated
	 */
	VxBPELVariationPoint createVxBPELVariationPoint();

	/**
	 * Returns a new object of class '<em>Variant</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Variant</em>'.
	 * @generated
	 */
	VxBPELVariant createVxBPELVariant();

	/**
	 * Returns a new object of class '<em>Configurable Variation Point</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Configurable Variation Point</em>'.
	 * @generated
	 */
	VxBPELConfigurableVariationPoint createVxBPELConfigurableVariationPoint();

	/**
	 * Returns a new object of class '<em>Configurable Variant</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Configurable Variant</em>'.
	 * @generated
	 */
	VxBPELConfigurableVariant createVxBPELConfigurableVariant();

	/**
	 * Returns a new object of class '<em>Variation Point PChoice</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Variation Point PChoice</em>'.
	 * @generated
	 */
	VxBPELVariationPointPChoice createVxBPELVariationPointPChoice();

	/**
	 * Returns a new object of class '<em>Process</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Process</em>'.
	 * @generated
	 */
	VxBPELProcess createVxBPELProcess();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	VxBPELPackage getVxBPELPackage();

} //VxBPELFactory
