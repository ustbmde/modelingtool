/**
 */
package edu.ustb.sei.mde.vxbpel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>BPEL Rethrow</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELRethrow()
 * @model
 * @generated
 */
public interface BPELRethrow extends BPELActivity {
} // BPELRethrow
