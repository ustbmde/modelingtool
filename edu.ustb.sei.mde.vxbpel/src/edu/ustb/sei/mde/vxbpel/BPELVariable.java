/**
 */
package edu.ustb.sei.mde.vxbpel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>BPEL Variable</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.BPELVariable#getName <em>Name</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.BPELVariable#getMessageType <em>Message Type</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.BPELVariable#getType <em>Type</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.BPELVariable#getElement <em>Element</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.BPELVariable#getFromSpec <em>From Spec</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELVariable()
 * @model
 * @generated
 */
public interface BPELVariable extends XMLAttributeContainer {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELVariable_Name()
	 * @model required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.vxbpel.BPELVariable#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Message Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Message Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Message Type</em>' attribute.
	 * @see #setMessageType(String)
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELVariable_MessageType()
	 * @model
	 * @generated
	 */
	String getMessageType();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.vxbpel.BPELVariable#getMessageType <em>Message Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Message Type</em>' attribute.
	 * @see #getMessageType()
	 * @generated
	 */
	void setMessageType(String value);

	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see #setType(String)
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELVariable_Type()
	 * @model
	 * @generated
	 */
	String getType();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.vxbpel.BPELVariable#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see #getType()
	 * @generated
	 */
	void setType(String value);

	/**
	 * Returns the value of the '<em><b>Element</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Element</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Element</em>' attribute.
	 * @see #setElement(String)
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELVariable_Element()
	 * @model
	 * @generated
	 */
	String getElement();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.vxbpel.BPELVariable#getElement <em>Element</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Element</em>' attribute.
	 * @see #getElement()
	 * @generated
	 */
	void setElement(String value);

	/**
	 * Returns the value of the '<em><b>From Spec</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>From Spec</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>From Spec</em>' attribute.
	 * @see #setFromSpec(String)
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELVariable_FromSpec()
	 * @model
	 * @generated
	 */
	String getFromSpec();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.vxbpel.BPELVariable#getFromSpec <em>From Spec</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>From Spec</em>' attribute.
	 * @see #getFromSpec()
	 * @generated
	 */
	void setFromSpec(String value);

} // BPELVariable
