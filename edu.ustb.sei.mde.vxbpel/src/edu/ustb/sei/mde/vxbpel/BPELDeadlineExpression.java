/**
 */
package edu.ustb.sei.mde.vxbpel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>BPEL Deadline Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELDeadlineExpression()
 * @model
 * @generated
 */
public interface BPELDeadlineExpression extends BPELExpression {
} // BPELDeadlineExpression
