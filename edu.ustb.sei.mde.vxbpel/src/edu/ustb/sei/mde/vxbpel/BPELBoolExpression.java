/**
 */
package edu.ustb.sei.mde.vxbpel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>BPEL Bool Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELBoolExpression()
 * @model
 * @generated
 */
public interface BPELBoolExpression extends BPELExpression {
} // BPELBoolExpression
