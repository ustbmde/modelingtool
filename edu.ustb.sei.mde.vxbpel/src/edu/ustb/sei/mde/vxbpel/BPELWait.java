/**
 */
package edu.ustb.sei.mde.vxbpel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>BPEL Wait</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.BPELWait#getDeadlineExpression <em>Deadline Expression</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.BPELWait#getDurationExpression <em>Duration Expression</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELWait()
 * @model
 * @generated
 */
public interface BPELWait extends BPELActivity {
	/**
	 * Returns the value of the '<em><b>Deadline Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Deadline Expression</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Deadline Expression</em>' containment reference.
	 * @see #setDeadlineExpression(BPELDeadlineExpression)
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELWait_DeadlineExpression()
	 * @model containment="true"
	 * @generated
	 */
	BPELDeadlineExpression getDeadlineExpression();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.vxbpel.BPELWait#getDeadlineExpression <em>Deadline Expression</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Deadline Expression</em>' containment reference.
	 * @see #getDeadlineExpression()
	 * @generated
	 */
	void setDeadlineExpression(BPELDeadlineExpression value);

	/**
	 * Returns the value of the '<em><b>Duration Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Duration Expression</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Duration Expression</em>' containment reference.
	 * @see #setDurationExpression(BPELDurationExpression)
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELWait_DurationExpression()
	 * @model containment="true"
	 * @generated
	 */
	BPELDurationExpression getDurationExpression();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.vxbpel.BPELWait#getDurationExpression <em>Duration Expression</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Duration Expression</em>' containment reference.
	 * @see #getDurationExpression()
	 * @generated
	 */
	void setDurationExpression(BPELDurationExpression value);

} // BPELWait
