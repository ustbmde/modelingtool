/**
 */
package edu.ustb.sei.mde.vxbpel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>BPEL Unsigned Integer Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELUnsignedIntegerExpression()
 * @model
 * @generated
 */
public interface BPELUnsignedIntegerExpression extends BPELExpression {
} // BPELUnsignedIntegerExpression
