/**
 */
package edu.ustb.sei.mde.vxbpel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>BPEL Invoke</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.BPELInvoke#getPartnerLink <em>Partner Link</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.BPELInvoke#getPortType <em>Port Type</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.BPELInvoke#getOperation <em>Operation</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.BPELInvoke#getInputVariable <em>Input Variable</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.BPELInvoke#getOutputVariable <em>Output Variable</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELInvoke()
 * @model
 * @generated
 */
public interface BPELInvoke extends BPELActivity {
	/**
	 * Returns the value of the '<em><b>Partner Link</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Partner Link</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Partner Link</em>' attribute.
	 * @see #setPartnerLink(String)
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELInvoke_PartnerLink()
	 * @model required="true"
	 * @generated
	 */
	String getPartnerLink();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.vxbpel.BPELInvoke#getPartnerLink <em>Partner Link</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Partner Link</em>' attribute.
	 * @see #getPartnerLink()
	 * @generated
	 */
	void setPartnerLink(String value);

	/**
	 * Returns the value of the '<em><b>Port Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Port Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Port Type</em>' attribute.
	 * @see #setPortType(String)
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELInvoke_PortType()
	 * @model
	 * @generated
	 */
	String getPortType();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.vxbpel.BPELInvoke#getPortType <em>Port Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Port Type</em>' attribute.
	 * @see #getPortType()
	 * @generated
	 */
	void setPortType(String value);

	/**
	 * Returns the value of the '<em><b>Operation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operation</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operation</em>' attribute.
	 * @see #setOperation(String)
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELInvoke_Operation()
	 * @model required="true"
	 * @generated
	 */
	String getOperation();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.vxbpel.BPELInvoke#getOperation <em>Operation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operation</em>' attribute.
	 * @see #getOperation()
	 * @generated
	 */
	void setOperation(String value);

	/**
	 * Returns the value of the '<em><b>Input Variable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Input Variable</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Input Variable</em>' attribute.
	 * @see #setInputVariable(String)
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELInvoke_InputVariable()
	 * @model
	 * @generated
	 */
	String getInputVariable();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.vxbpel.BPELInvoke#getInputVariable <em>Input Variable</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Input Variable</em>' attribute.
	 * @see #getInputVariable()
	 * @generated
	 */
	void setInputVariable(String value);

	/**
	 * Returns the value of the '<em><b>Output Variable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Output Variable</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Output Variable</em>' attribute.
	 * @see #setOutputVariable(String)
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELInvoke_OutputVariable()
	 * @model
	 * @generated
	 */
	String getOutputVariable();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.vxbpel.BPELInvoke#getOutputVariable <em>Output Variable</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Output Variable</em>' attribute.
	 * @see #getOutputVariable()
	 * @generated
	 */
	void setOutputVariable(String value);

} // BPELInvoke
