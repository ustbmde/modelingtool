/**
 */
package edu.ustb.sei.mde.vxbpel;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>BPEL Scope Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.BPELScopeElement#getExitOnStandardFault <em>Exit On Standard Fault</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.BPELScopeElement#getPartnerLinks <em>Partner Links</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.BPELScopeElement#getMessageExchanges <em>Message Exchanges</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.BPELScopeElement#getVariables <em>Variables</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.BPELScopeElement#getCorrelationSets <em>Correlation Sets</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.BPELScopeElement#getFaultHandlerList <em>Fault Handler List</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.BPELScopeElement#getEventHandlerList <em>Event Handler List</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.BPELScopeElement#getActivity <em>Activity</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELScopeElement()
 * @model abstract="true"
 * @generated
 */
public interface BPELScopeElement extends XMLAttributeContainer {
	/**
	 * Returns the value of the '<em><b>Exit On Standard Fault</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Exit On Standard Fault</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Exit On Standard Fault</em>' attribute.
	 * @see #setExitOnStandardFault(String)
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELScopeElement_ExitOnStandardFault()
	 * @model
	 * @generated
	 */
	String getExitOnStandardFault();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.vxbpel.BPELScopeElement#getExitOnStandardFault <em>Exit On Standard Fault</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Exit On Standard Fault</em>' attribute.
	 * @see #getExitOnStandardFault()
	 * @generated
	 */
	void setExitOnStandardFault(String value);

	/**
	 * Returns the value of the '<em><b>Partner Links</b></em>' containment reference list.
	 * The list contents are of type {@link edu.ustb.sei.mde.vxbpel.BPELPartnerLink}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Partner Links</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Partner Links</em>' containment reference list.
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELScopeElement_PartnerLinks()
	 * @model containment="true"
	 * @generated
	 */
	EList<BPELPartnerLink> getPartnerLinks();

	/**
	 * Returns the value of the '<em><b>Message Exchanges</b></em>' containment reference list.
	 * The list contents are of type {@link edu.ustb.sei.mde.vxbpel.BPELMessageExchange}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Message Exchanges</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Message Exchanges</em>' containment reference list.
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELScopeElement_MessageExchanges()
	 * @model containment="true"
	 * @generated
	 */
	EList<BPELMessageExchange> getMessageExchanges();

	/**
	 * Returns the value of the '<em><b>Variables</b></em>' containment reference list.
	 * The list contents are of type {@link edu.ustb.sei.mde.vxbpel.BPELVariable}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Variables</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Variables</em>' containment reference list.
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELScopeElement_Variables()
	 * @model containment="true"
	 * @generated
	 */
	EList<BPELVariable> getVariables();

	/**
	 * Returns the value of the '<em><b>Correlation Sets</b></em>' containment reference list.
	 * The list contents are of type {@link edu.ustb.sei.mde.vxbpel.BPELCorrelationSet}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Correlation Sets</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Correlation Sets</em>' containment reference list.
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELScopeElement_CorrelationSets()
	 * @model containment="true"
	 * @generated
	 */
	EList<BPELCorrelationSet> getCorrelationSets();

	/**
	 * Returns the value of the '<em><b>Fault Handler List</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.EObject}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Fault Handler List</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Fault Handler List</em>' containment reference list.
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELScopeElement_FaultHandlerList()
	 * @model containment="true"
	 * @generated
	 */
	EList<EObject> getFaultHandlerList();

	/**
	 * Returns the value of the '<em><b>Event Handler List</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.EObject}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Event Handler List</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Event Handler List</em>' containment reference list.
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELScopeElement_EventHandlerList()
	 * @model containment="true"
	 * @generated
	 */
	EList<EObject> getEventHandlerList();

	/**
	 * Returns the value of the '<em><b>Activity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Activity</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Activity</em>' containment reference.
	 * @see #setActivity(BPELActivity)
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELScopeElement_Activity()
	 * @model containment="true" required="true"
	 * @generated
	 */
	BPELActivity getActivity();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.vxbpel.BPELScopeElement#getActivity <em>Activity</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Activity</em>' containment reference.
	 * @see #getActivity()
	 * @generated
	 */
	void setActivity(BPELActivity value);

} // BPELScopeElement
