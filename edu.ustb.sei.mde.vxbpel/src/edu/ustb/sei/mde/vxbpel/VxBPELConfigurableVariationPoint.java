/**
 */
package edu.ustb.sei.mde.vxbpel;

import org.eclipse.emf.common.util.EList;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Configurable Variation Point</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.VxBPELConfigurableVariationPoint#getId <em>Id</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.VxBPELConfigurableVariationPoint#getDefaultVariant <em>Default Variant</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.VxBPELConfigurableVariationPoint#getVariants <em>Variants</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getVxBPELConfigurableVariationPoint()
 * @model
 * @generated
 */
public interface VxBPELConfigurableVariationPoint extends XMLAttributeContainer, NamedElement {
	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(String)
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getVxBPELConfigurableVariationPoint_Id()
	 * @model
	 * @generated
	 */
	String getId();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.vxbpel.VxBPELConfigurableVariationPoint#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(String value);

	/**
	 * Returns the value of the '<em><b>Default Variant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Default Variant</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Default Variant</em>' attribute.
	 * @see #setDefaultVariant(String)
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getVxBPELConfigurableVariationPoint_DefaultVariant()
	 * @model
	 * @generated
	 */
	String getDefaultVariant();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.vxbpel.VxBPELConfigurableVariationPoint#getDefaultVariant <em>Default Variant</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Default Variant</em>' attribute.
	 * @see #getDefaultVariant()
	 * @generated
	 */
	void setDefaultVariant(String value);

	/**
	 * Returns the value of the '<em><b>Variants</b></em>' containment reference list.
	 * The list contents are of type {@link edu.ustb.sei.mde.vxbpel.VxBPELConfigurableVariant}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Variants</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Variants</em>' containment reference list.
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getVxBPELConfigurableVariationPoint_Variants()
	 * @model containment="true"
	 * @generated
	 */
	EList<VxBPELConfigurableVariant> getVariants();

} // VxBPELConfigurableVariationPoint
