/**
 */
package edu.ustb.sei.mde.vxbpel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>BPEL Copy</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.BPELCopy#getFromSpec <em>From Spec</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.BPELCopy#getToSpec <em>To Spec</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELCopy()
 * @model
 * @generated
 */
public interface BPELCopy extends BPELAssignOperation {
	/**
	 * Returns the value of the '<em><b>From Spec</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>From Spec</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>From Spec</em>' containment reference.
	 * @see #setFromSpec(BPELCopyFrom)
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELCopy_FromSpec()
	 * @model containment="true" required="true"
	 * @generated
	 */
	BPELCopyFrom getFromSpec();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.vxbpel.BPELCopy#getFromSpec <em>From Spec</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>From Spec</em>' containment reference.
	 * @see #getFromSpec()
	 * @generated
	 */
	void setFromSpec(BPELCopyFrom value);

	/**
	 * Returns the value of the '<em><b>To Spec</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>To Spec</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>To Spec</em>' containment reference.
	 * @see #setToSpec(BPELCopyTo)
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELCopy_ToSpec()
	 * @model containment="true" required="true"
	 * @generated
	 */
	BPELCopyTo getToSpec();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.vxbpel.BPELCopy#getToSpec <em>To Spec</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>To Spec</em>' containment reference.
	 * @see #getToSpec()
	 * @generated
	 */
	void setToSpec(BPELCopyTo value);

} // BPELCopy
