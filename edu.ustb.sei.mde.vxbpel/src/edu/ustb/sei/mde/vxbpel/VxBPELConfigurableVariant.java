/**
 */
package edu.ustb.sei.mde.vxbpel;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Configurable Variant</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.VxBPELConfigurableVariant#getVariantInfo <em>Variant Info</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.VxBPELConfigurableVariant#getRationale <em>Rationale</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.VxBPELConfigurableVariant#getRequiredVariants <em>Required Variants</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getVxBPELConfigurableVariant()
 * @model
 * @generated
 */
public interface VxBPELConfigurableVariant extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Variant Info</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Variant Info</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Variant Info</em>' attribute.
	 * @see #setVariantInfo(String)
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getVxBPELConfigurableVariant_VariantInfo()
	 * @model
	 * @generated
	 */
	String getVariantInfo();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.vxbpel.VxBPELConfigurableVariant#getVariantInfo <em>Variant Info</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Variant Info</em>' attribute.
	 * @see #getVariantInfo()
	 * @generated
	 */
	void setVariantInfo(String value);

	/**
	 * Returns the value of the '<em><b>Rationale</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Rationale</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rationale</em>' attribute.
	 * @see #setRationale(String)
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getVxBPELConfigurableVariant_Rationale()
	 * @model
	 * @generated
	 */
	String getRationale();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.vxbpel.VxBPELConfigurableVariant#getRationale <em>Rationale</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Rationale</em>' attribute.
	 * @see #getRationale()
	 * @generated
	 */
	void setRationale(String value);

	/**
	 * Returns the value of the '<em><b>Required Variants</b></em>' containment reference list.
	 * The list contents are of type {@link edu.ustb.sei.mde.vxbpel.VxBPELVariationPointPChoice}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Required Variants</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Required Variants</em>' containment reference list.
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getVxBPELConfigurableVariant_RequiredVariants()
	 * @model containment="true"
	 * @generated
	 */
	EList<VxBPELVariationPointPChoice> getRequiredVariants();

} // VxBPELConfigurableVariant
