/**
 */
package edu.ustb.sei.mde.vxbpel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>BPEL Activity</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.BPELActivity#getTargetSet <em>Target Set</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.BPELActivity#getSourceSet <em>Source Set</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELActivity()
 * @model abstract="true"
 * @generated
 */
public interface BPELActivity extends NamedElement, XMLAttributeContainer {
	/**
	 * Returns the value of the '<em><b>Target Set</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target Set</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target Set</em>' containment reference.
	 * @see #setTargetSet(BPELTargetList)
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELActivity_TargetSet()
	 * @model containment="true"
	 * @generated
	 */
	BPELTargetList getTargetSet();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.vxbpel.BPELActivity#getTargetSet <em>Target Set</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target Set</em>' containment reference.
	 * @see #getTargetSet()
	 * @generated
	 */
	void setTargetSet(BPELTargetList value);

	/**
	 * Returns the value of the '<em><b>Source Set</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source Set</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source Set</em>' containment reference.
	 * @see #setSourceSet(BPELSourceList)
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELActivity_SourceSet()
	 * @model containment="true"
	 * @generated
	 */
	BPELSourceList getSourceSet();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.vxbpel.BPELActivity#getSourceSet <em>Source Set</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source Set</em>' containment reference.
	 * @see #getSourceSet()
	 * @generated
	 */
	void setSourceSet(BPELSourceList value);

} // BPELActivity
