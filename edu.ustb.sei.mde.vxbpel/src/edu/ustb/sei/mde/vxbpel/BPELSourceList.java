/**
 */
package edu.ustb.sei.mde.vxbpel;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>BPEL Source List</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.BPELSourceList#getSources <em>Sources</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELSourceList()
 * @model
 * @generated
 */
public interface BPELSourceList extends XMLAttributeContainer {
	/**
	 * Returns the value of the '<em><b>Sources</b></em>' containment reference list.
	 * The list contents are of type {@link edu.ustb.sei.mde.vxbpel.BPELSource}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sources</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sources</em>' containment reference list.
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELSourceList_Sources()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<BPELSource> getSources();

} // BPELSourceList
