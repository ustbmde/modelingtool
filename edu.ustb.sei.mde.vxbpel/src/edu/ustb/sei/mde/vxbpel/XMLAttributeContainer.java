/**
 */
package edu.ustb.sei.mde.vxbpel;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>XML Attribute Container</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.XMLAttributeContainer#getAttributes <em>Attributes</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getXMLAttributeContainer()
 * @model abstract="true"
 * @generated
 */
public interface XMLAttributeContainer extends EObject {
	/**
	 * Returns the value of the '<em><b>Attributes</b></em>' containment reference list.
	 * The list contents are of type {@link edu.ustb.sei.mde.vxbpel.XMLAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Attributes</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attributes</em>' containment reference list.
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getXMLAttributeContainer_Attributes()
	 * @model containment="true"
	 * @generated
	 */
	EList<XMLAttribute> getAttributes();

} // XMLAttributeContainer
