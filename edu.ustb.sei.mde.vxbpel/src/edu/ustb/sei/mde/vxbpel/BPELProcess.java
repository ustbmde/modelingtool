/**
 */
package edu.ustb.sei.mde.vxbpel;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>BPEL Process</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.BPELProcess#getTargetNamespace <em>Target Namespace</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.BPELProcess#getQueryLanguage <em>Query Language</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.BPELProcess#getExpressionLanguage <em>Expression Language</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.BPELProcess#getImports <em>Imports</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELProcess()
 * @model
 * @generated
 */
public interface BPELProcess extends NamedElement, BPELScopeElement {
	/**
	 * Returns the value of the '<em><b>Target Namespace</b></em>' attribute.
	 * The default value is <code>"urn:oasis:names:tc:wsbpel:2.0:sublang:xpath1.0"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target Namespace</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target Namespace</em>' attribute.
	 * @see #setTargetNamespace(String)
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELProcess_TargetNamespace()
	 * @model default="urn:oasis:names:tc:wsbpel:2.0:sublang:xpath1.0" required="true"
	 * @generated
	 */
	String getTargetNamespace();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.vxbpel.BPELProcess#getTargetNamespace <em>Target Namespace</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target Namespace</em>' attribute.
	 * @see #getTargetNamespace()
	 * @generated
	 */
	void setTargetNamespace(String value);

	/**
	 * Returns the value of the '<em><b>Query Language</b></em>' attribute.
	 * The default value is <code>"urn:oasis:names:tc:wsbpel:2.0:sublang:xpath1.0"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Query Language</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Query Language</em>' attribute.
	 * @see #setQueryLanguage(String)
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELProcess_QueryLanguage()
	 * @model default="urn:oasis:names:tc:wsbpel:2.0:sublang:xpath1.0"
	 * @generated
	 */
	String getQueryLanguage();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.vxbpel.BPELProcess#getQueryLanguage <em>Query Language</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Query Language</em>' attribute.
	 * @see #getQueryLanguage()
	 * @generated
	 */
	void setQueryLanguage(String value);

	/**
	 * Returns the value of the '<em><b>Expression Language</b></em>' attribute.
	 * The default value is <code>"urn:oasis:names:tc:wsbpel:2.0:sublang:xpath1.0"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Expression Language</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Expression Language</em>' attribute.
	 * @see #setExpressionLanguage(String)
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELProcess_ExpressionLanguage()
	 * @model default="urn:oasis:names:tc:wsbpel:2.0:sublang:xpath1.0"
	 * @generated
	 */
	String getExpressionLanguage();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.vxbpel.BPELProcess#getExpressionLanguage <em>Expression Language</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Expression Language</em>' attribute.
	 * @see #getExpressionLanguage()
	 * @generated
	 */
	void setExpressionLanguage(String value);

	/**
	 * Returns the value of the '<em><b>Imports</b></em>' containment reference list.
	 * The list contents are of type {@link edu.ustb.sei.mde.vxbpel.BPELImport}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Imports</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Imports</em>' containment reference list.
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELProcess_Imports()
	 * @model containment="true"
	 * @generated
	 */
	EList<BPELImport> getImports();

} // BPELProcess
