/**
 */
package edu.ustb.sei.mde.vxbpel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>BPEL Copy From</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.BPELCopyFrom#getVariable <em>Variable</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.BPELCopyFrom#getPart <em>Part</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.BPELCopyFrom#getProperty <em>Property</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.BPELCopyFrom#getPartnerLink <em>Partner Link</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.BPELCopyFrom#getEndpointReference <em>Endpoint Reference</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.BPELCopyFrom#getExpression <em>Expression</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.BPELCopyFrom#getQuery <em>Query</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.BPELCopyFrom#getLiteral <em>Literal</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELCopyFrom()
 * @model
 * @generated
 */
public interface BPELCopyFrom extends XMLAttributeContainer {
	/**
	 * Returns the value of the '<em><b>Variable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Variable</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Variable</em>' attribute.
	 * @see #setVariable(String)
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELCopyFrom_Variable()
	 * @model
	 * @generated
	 */
	String getVariable();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.vxbpel.BPELCopyFrom#getVariable <em>Variable</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Variable</em>' attribute.
	 * @see #getVariable()
	 * @generated
	 */
	void setVariable(String value);

	/**
	 * Returns the value of the '<em><b>Part</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Part</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Part</em>' attribute.
	 * @see #setPart(String)
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELCopyFrom_Part()
	 * @model
	 * @generated
	 */
	String getPart();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.vxbpel.BPELCopyFrom#getPart <em>Part</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Part</em>' attribute.
	 * @see #getPart()
	 * @generated
	 */
	void setPart(String value);

	/**
	 * Returns the value of the '<em><b>Property</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Property</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Property</em>' attribute.
	 * @see #setProperty(String)
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELCopyFrom_Property()
	 * @model
	 * @generated
	 */
	String getProperty();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.vxbpel.BPELCopyFrom#getProperty <em>Property</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Property</em>' attribute.
	 * @see #getProperty()
	 * @generated
	 */
	void setProperty(String value);

	/**
	 * Returns the value of the '<em><b>Partner Link</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Partner Link</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Partner Link</em>' attribute.
	 * @see #setPartnerLink(String)
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELCopyFrom_PartnerLink()
	 * @model
	 * @generated
	 */
	String getPartnerLink();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.vxbpel.BPELCopyFrom#getPartnerLink <em>Partner Link</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Partner Link</em>' attribute.
	 * @see #getPartnerLink()
	 * @generated
	 */
	void setPartnerLink(String value);

	/**
	 * Returns the value of the '<em><b>Endpoint Reference</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Endpoint Reference</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Endpoint Reference</em>' attribute.
	 * @see #setEndpointReference(String)
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELCopyFrom_EndpointReference()
	 * @model
	 * @generated
	 */
	String getEndpointReference();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.vxbpel.BPELCopyFrom#getEndpointReference <em>Endpoint Reference</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Endpoint Reference</em>' attribute.
	 * @see #getEndpointReference()
	 * @generated
	 */
	void setEndpointReference(String value);

	/**
	 * Returns the value of the '<em><b>Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Expression</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Expression</em>' containment reference.
	 * @see #setExpression(BPELExpression)
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELCopyFrom_Expression()
	 * @model containment="true"
	 * @generated
	 */
	BPELExpression getExpression();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.vxbpel.BPELCopyFrom#getExpression <em>Expression</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Expression</em>' containment reference.
	 * @see #getExpression()
	 * @generated
	 */
	void setExpression(BPELExpression value);

	/**
	 * Returns the value of the '<em><b>Query</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Query</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Query</em>' containment reference.
	 * @see #setQuery(BPELQuery)
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELCopyFrom_Query()
	 * @model containment="true"
	 * @generated
	 */
	BPELQuery getQuery();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.vxbpel.BPELCopyFrom#getQuery <em>Query</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Query</em>' containment reference.
	 * @see #getQuery()
	 * @generated
	 */
	void setQuery(BPELQuery value);

	/**
	 * Returns the value of the '<em><b>Literal</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Literal</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Literal</em>' containment reference.
	 * @see #setLiteral(BPELLiteral)
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELCopyFrom_Literal()
	 * @model containment="true"
	 * @generated
	 */
	BPELLiteral getLiteral();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.vxbpel.BPELCopyFrom#getLiteral <em>Literal</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Literal</em>' containment reference.
	 * @see #getLiteral()
	 * @generated
	 */
	void setLiteral(BPELLiteral value);

} // BPELCopyFrom
