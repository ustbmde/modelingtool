/**
 */
package edu.ustb.sei.mde.vxbpel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>BPEL Else If</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.BPELElseIf#getCondition <em>Condition</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.BPELElseIf#getThenActivity <em>Then Activity</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELElseIf()
 * @model
 * @generated
 */
public interface BPELElseIf extends BPELActivity {
	/**
	 * Returns the value of the '<em><b>Condition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Condition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Condition</em>' containment reference.
	 * @see #setCondition(BPELBoolExpression)
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELElseIf_Condition()
	 * @model containment="true" required="true"
	 * @generated
	 */
	BPELBoolExpression getCondition();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.vxbpel.BPELElseIf#getCondition <em>Condition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Condition</em>' containment reference.
	 * @see #getCondition()
	 * @generated
	 */
	void setCondition(BPELBoolExpression value);

	/**
	 * Returns the value of the '<em><b>Then Activity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Then Activity</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Then Activity</em>' containment reference.
	 * @see #setThenActivity(BPELActivity)
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELElseIf_ThenActivity()
	 * @model containment="true" required="true"
	 * @generated
	 */
	BPELActivity getThenActivity();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.vxbpel.BPELElseIf#getThenActivity <em>Then Activity</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Then Activity</em>' containment reference.
	 * @see #getThenActivity()
	 * @generated
	 */
	void setThenActivity(BPELActivity value);

} // BPELElseIf
