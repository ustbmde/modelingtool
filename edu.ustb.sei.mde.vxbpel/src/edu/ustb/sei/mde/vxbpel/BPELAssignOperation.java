/**
 */
package edu.ustb.sei.mde.vxbpel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>BPEL Assign Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELAssignOperation()
 * @model abstract="true"
 * @generated
 */
public interface BPELAssignOperation extends XMLAttributeContainer {
} // BPELAssignOperation
