/**
 */
package edu.ustb.sei.mde.vxbpel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>BPEL Partner Link</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.BPELPartnerLink#getName <em>Name</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.BPELPartnerLink#getPartnerLinkType <em>Partner Link Type</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.BPELPartnerLink#getMyRole <em>My Role</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.BPELPartnerLink#getPartnerRole <em>Partner Role</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.BPELPartnerLink#getInitializePartnerRole <em>Initialize Partner Role</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELPartnerLink()
 * @model
 * @generated
 */
public interface BPELPartnerLink extends XMLAttributeContainer {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELPartnerLink_Name()
	 * @model required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.vxbpel.BPELPartnerLink#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Partner Link Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Partner Link Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Partner Link Type</em>' attribute.
	 * @see #setPartnerLinkType(String)
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELPartnerLink_PartnerLinkType()
	 * @model required="true"
	 * @generated
	 */
	String getPartnerLinkType();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.vxbpel.BPELPartnerLink#getPartnerLinkType <em>Partner Link Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Partner Link Type</em>' attribute.
	 * @see #getPartnerLinkType()
	 * @generated
	 */
	void setPartnerLinkType(String value);

	/**
	 * Returns the value of the '<em><b>My Role</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>My Role</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>My Role</em>' attribute.
	 * @see #setMyRole(String)
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELPartnerLink_MyRole()
	 * @model
	 * @generated
	 */
	String getMyRole();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.vxbpel.BPELPartnerLink#getMyRole <em>My Role</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>My Role</em>' attribute.
	 * @see #getMyRole()
	 * @generated
	 */
	void setMyRole(String value);

	/**
	 * Returns the value of the '<em><b>Partner Role</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Partner Role</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Partner Role</em>' attribute.
	 * @see #setPartnerRole(String)
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELPartnerLink_PartnerRole()
	 * @model
	 * @generated
	 */
	String getPartnerRole();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.vxbpel.BPELPartnerLink#getPartnerRole <em>Partner Role</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Partner Role</em>' attribute.
	 * @see #getPartnerRole()
	 * @generated
	 */
	void setPartnerRole(String value);

	/**
	 * Returns the value of the '<em><b>Initialize Partner Role</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Initialize Partner Role</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Initialize Partner Role</em>' attribute.
	 * @see #setInitializePartnerRole(String)
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELPartnerLink_InitializePartnerRole()
	 * @model
	 * @generated
	 */
	String getInitializePartnerRole();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.vxbpel.BPELPartnerLink#getInitializePartnerRole <em>Initialize Partner Role</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Initialize Partner Role</em>' attribute.
	 * @see #getInitializePartnerRole()
	 * @generated
	 */
	void setInitializePartnerRole(String value);

} // BPELPartnerLink
