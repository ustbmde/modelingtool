/**
 */
package edu.ustb.sei.mde.vxbpel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>BPEL Scope</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.BPELScope#getCompensationHandler <em>Compensation Handler</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.BPELScope#getTerminationHandler <em>Termination Handler</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELScope()
 * @model
 * @generated
 */
public interface BPELScope extends BPELActivity, NamedElement, BPELScopeElement {
	/**
	 * Returns the value of the '<em><b>Compensation Handler</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Compensation Handler</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Compensation Handler</em>' containment reference.
	 * @see #setCompensationHandler(EObject)
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELScope_CompensationHandler()
	 * @model containment="true"
	 * @generated
	 */
	EObject getCompensationHandler();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.vxbpel.BPELScope#getCompensationHandler <em>Compensation Handler</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Compensation Handler</em>' containment reference.
	 * @see #getCompensationHandler()
	 * @generated
	 */
	void setCompensationHandler(EObject value);

	/**
	 * Returns the value of the '<em><b>Termination Handler</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Termination Handler</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Termination Handler</em>' containment reference.
	 * @see #setTerminationHandler(EObject)
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELScope_TerminationHandler()
	 * @model containment="true"
	 * @generated
	 */
	EObject getTerminationHandler();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.vxbpel.BPELScope#getTerminationHandler <em>Termination Handler</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Termination Handler</em>' containment reference.
	 * @see #getTerminationHandler()
	 * @generated
	 */
	void setTerminationHandler(EObject value);

} // BPELScope
