/**
 */
package edu.ustb.sei.mde.vxbpel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>BPEL Source</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.BPELSource#getLinkName <em>Link Name</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.BPELSource#getTransitionCondition <em>Transition Condition</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELSource()
 * @model
 * @generated
 */
public interface BPELSource extends XMLAttributeContainer {
	/**
	 * Returns the value of the '<em><b>Link Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Link Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Link Name</em>' attribute.
	 * @see #setLinkName(String)
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELSource_LinkName()
	 * @model required="true"
	 * @generated
	 */
	String getLinkName();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.vxbpel.BPELSource#getLinkName <em>Link Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Link Name</em>' attribute.
	 * @see #getLinkName()
	 * @generated
	 */
	void setLinkName(String value);

	/**
	 * Returns the value of the '<em><b>Transition Condition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Transition Condition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Transition Condition</em>' containment reference.
	 * @see #setTransitionCondition(BPELBoolExpression)
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELSource_TransitionCondition()
	 * @model containment="true"
	 * @generated
	 */
	BPELBoolExpression getTransitionCondition();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.vxbpel.BPELSource#getTransitionCondition <em>Transition Condition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Transition Condition</em>' containment reference.
	 * @see #getTransitionCondition()
	 * @generated
	 */
	void setTransitionCondition(BPELBoolExpression value);

} // BPELSource
