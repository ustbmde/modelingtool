/**
 */
package edu.ustb.sei.mde.vxbpel.impl;

import edu.ustb.sei.mde.vxbpel.BPELActivity;
import edu.ustb.sei.mde.vxbpel.BPELCorrelationSet;
import edu.ustb.sei.mde.vxbpel.BPELMessageExchange;
import edu.ustb.sei.mde.vxbpel.BPELPartnerLink;
import edu.ustb.sei.mde.vxbpel.BPELScopeElement;
import edu.ustb.sei.mde.vxbpel.BPELVariable;
import edu.ustb.sei.mde.vxbpel.VxBPELPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>BPEL Scope Element</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.impl.BPELScopeElementImpl#getExitOnStandardFault <em>Exit On Standard Fault</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.impl.BPELScopeElementImpl#getPartnerLinks <em>Partner Links</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.impl.BPELScopeElementImpl#getMessageExchanges <em>Message Exchanges</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.impl.BPELScopeElementImpl#getVariables <em>Variables</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.impl.BPELScopeElementImpl#getCorrelationSets <em>Correlation Sets</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.impl.BPELScopeElementImpl#getFaultHandlerList <em>Fault Handler List</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.impl.BPELScopeElementImpl#getEventHandlerList <em>Event Handler List</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.impl.BPELScopeElementImpl#getActivity <em>Activity</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public abstract class BPELScopeElementImpl extends XMLAttributeContainerImpl implements BPELScopeElement {
	/**
	 * The default value of the '{@link #getExitOnStandardFault() <em>Exit On Standard Fault</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExitOnStandardFault()
	 * @generated
	 * @ordered
	 */
	protected static final String EXIT_ON_STANDARD_FAULT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getExitOnStandardFault() <em>Exit On Standard Fault</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExitOnStandardFault()
	 * @generated
	 * @ordered
	 */
	protected String exitOnStandardFault = EXIT_ON_STANDARD_FAULT_EDEFAULT;

	/**
	 * The cached value of the '{@link #getPartnerLinks() <em>Partner Links</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPartnerLinks()
	 * @generated
	 * @ordered
	 */
	protected EList<BPELPartnerLink> partnerLinks;

	/**
	 * The cached value of the '{@link #getMessageExchanges() <em>Message Exchanges</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMessageExchanges()
	 * @generated
	 * @ordered
	 */
	protected EList<BPELMessageExchange> messageExchanges;

	/**
	 * The cached value of the '{@link #getVariables() <em>Variables</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVariables()
	 * @generated
	 * @ordered
	 */
	protected EList<BPELVariable> variables;

	/**
	 * The cached value of the '{@link #getCorrelationSets() <em>Correlation Sets</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCorrelationSets()
	 * @generated
	 * @ordered
	 */
	protected EList<BPELCorrelationSet> correlationSets;

	/**
	 * The cached value of the '{@link #getFaultHandlerList() <em>Fault Handler List</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFaultHandlerList()
	 * @generated
	 * @ordered
	 */
	protected EList<EObject> faultHandlerList;

	/**
	 * The cached value of the '{@link #getEventHandlerList() <em>Event Handler List</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEventHandlerList()
	 * @generated
	 * @ordered
	 */
	protected EList<EObject> eventHandlerList;

	/**
	 * The cached value of the '{@link #getActivity() <em>Activity</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActivity()
	 * @generated
	 * @ordered
	 */
	protected BPELActivity activity;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BPELScopeElementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return VxBPELPackage.Literals.BPEL_SCOPE_ELEMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getExitOnStandardFault() {
		return exitOnStandardFault;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExitOnStandardFault(String newExitOnStandardFault) {
		String oldExitOnStandardFault = exitOnStandardFault;
		exitOnStandardFault = newExitOnStandardFault;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VxBPELPackage.BPEL_SCOPE_ELEMENT__EXIT_ON_STANDARD_FAULT, oldExitOnStandardFault, exitOnStandardFault));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<BPELPartnerLink> getPartnerLinks() {
		if (partnerLinks == null) {
			partnerLinks = new EObjectContainmentEList<BPELPartnerLink>(BPELPartnerLink.class, this, VxBPELPackage.BPEL_SCOPE_ELEMENT__PARTNER_LINKS);
		}
		return partnerLinks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<BPELMessageExchange> getMessageExchanges() {
		if (messageExchanges == null) {
			messageExchanges = new EObjectContainmentEList<BPELMessageExchange>(BPELMessageExchange.class, this, VxBPELPackage.BPEL_SCOPE_ELEMENT__MESSAGE_EXCHANGES);
		}
		return messageExchanges;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<BPELVariable> getVariables() {
		if (variables == null) {
			variables = new EObjectContainmentEList<BPELVariable>(BPELVariable.class, this, VxBPELPackage.BPEL_SCOPE_ELEMENT__VARIABLES);
		}
		return variables;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<BPELCorrelationSet> getCorrelationSets() {
		if (correlationSets == null) {
			correlationSets = new EObjectContainmentEList<BPELCorrelationSet>(BPELCorrelationSet.class, this, VxBPELPackage.BPEL_SCOPE_ELEMENT__CORRELATION_SETS);
		}
		return correlationSets;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EObject> getFaultHandlerList() {
		if (faultHandlerList == null) {
			faultHandlerList = new EObjectContainmentEList<EObject>(EObject.class, this, VxBPELPackage.BPEL_SCOPE_ELEMENT__FAULT_HANDLER_LIST);
		}
		return faultHandlerList;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EObject> getEventHandlerList() {
		if (eventHandlerList == null) {
			eventHandlerList = new EObjectContainmentEList<EObject>(EObject.class, this, VxBPELPackage.BPEL_SCOPE_ELEMENT__EVENT_HANDLER_LIST);
		}
		return eventHandlerList;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BPELActivity getActivity() {
		return activity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetActivity(BPELActivity newActivity, NotificationChain msgs) {
		BPELActivity oldActivity = activity;
		activity = newActivity;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, VxBPELPackage.BPEL_SCOPE_ELEMENT__ACTIVITY, oldActivity, newActivity);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setActivity(BPELActivity newActivity) {
		if (newActivity != activity) {
			NotificationChain msgs = null;
			if (activity != null)
				msgs = ((InternalEObject)activity).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - VxBPELPackage.BPEL_SCOPE_ELEMENT__ACTIVITY, null, msgs);
			if (newActivity != null)
				msgs = ((InternalEObject)newActivity).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - VxBPELPackage.BPEL_SCOPE_ELEMENT__ACTIVITY, null, msgs);
			msgs = basicSetActivity(newActivity, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VxBPELPackage.BPEL_SCOPE_ELEMENT__ACTIVITY, newActivity, newActivity));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case VxBPELPackage.BPEL_SCOPE_ELEMENT__PARTNER_LINKS:
				return ((InternalEList<?>)getPartnerLinks()).basicRemove(otherEnd, msgs);
			case VxBPELPackage.BPEL_SCOPE_ELEMENT__MESSAGE_EXCHANGES:
				return ((InternalEList<?>)getMessageExchanges()).basicRemove(otherEnd, msgs);
			case VxBPELPackage.BPEL_SCOPE_ELEMENT__VARIABLES:
				return ((InternalEList<?>)getVariables()).basicRemove(otherEnd, msgs);
			case VxBPELPackage.BPEL_SCOPE_ELEMENT__CORRELATION_SETS:
				return ((InternalEList<?>)getCorrelationSets()).basicRemove(otherEnd, msgs);
			case VxBPELPackage.BPEL_SCOPE_ELEMENT__FAULT_HANDLER_LIST:
				return ((InternalEList<?>)getFaultHandlerList()).basicRemove(otherEnd, msgs);
			case VxBPELPackage.BPEL_SCOPE_ELEMENT__EVENT_HANDLER_LIST:
				return ((InternalEList<?>)getEventHandlerList()).basicRemove(otherEnd, msgs);
			case VxBPELPackage.BPEL_SCOPE_ELEMENT__ACTIVITY:
				return basicSetActivity(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case VxBPELPackage.BPEL_SCOPE_ELEMENT__EXIT_ON_STANDARD_FAULT:
				return getExitOnStandardFault();
			case VxBPELPackage.BPEL_SCOPE_ELEMENT__PARTNER_LINKS:
				return getPartnerLinks();
			case VxBPELPackage.BPEL_SCOPE_ELEMENT__MESSAGE_EXCHANGES:
				return getMessageExchanges();
			case VxBPELPackage.BPEL_SCOPE_ELEMENT__VARIABLES:
				return getVariables();
			case VxBPELPackage.BPEL_SCOPE_ELEMENT__CORRELATION_SETS:
				return getCorrelationSets();
			case VxBPELPackage.BPEL_SCOPE_ELEMENT__FAULT_HANDLER_LIST:
				return getFaultHandlerList();
			case VxBPELPackage.BPEL_SCOPE_ELEMENT__EVENT_HANDLER_LIST:
				return getEventHandlerList();
			case VxBPELPackage.BPEL_SCOPE_ELEMENT__ACTIVITY:
				return getActivity();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case VxBPELPackage.BPEL_SCOPE_ELEMENT__EXIT_ON_STANDARD_FAULT:
				setExitOnStandardFault((String)newValue);
				return;
			case VxBPELPackage.BPEL_SCOPE_ELEMENT__PARTNER_LINKS:
				getPartnerLinks().clear();
				getPartnerLinks().addAll((Collection<? extends BPELPartnerLink>)newValue);
				return;
			case VxBPELPackage.BPEL_SCOPE_ELEMENT__MESSAGE_EXCHANGES:
				getMessageExchanges().clear();
				getMessageExchanges().addAll((Collection<? extends BPELMessageExchange>)newValue);
				return;
			case VxBPELPackage.BPEL_SCOPE_ELEMENT__VARIABLES:
				getVariables().clear();
				getVariables().addAll((Collection<? extends BPELVariable>)newValue);
				return;
			case VxBPELPackage.BPEL_SCOPE_ELEMENT__CORRELATION_SETS:
				getCorrelationSets().clear();
				getCorrelationSets().addAll((Collection<? extends BPELCorrelationSet>)newValue);
				return;
			case VxBPELPackage.BPEL_SCOPE_ELEMENT__FAULT_HANDLER_LIST:
				getFaultHandlerList().clear();
				getFaultHandlerList().addAll((Collection<? extends EObject>)newValue);
				return;
			case VxBPELPackage.BPEL_SCOPE_ELEMENT__EVENT_HANDLER_LIST:
				getEventHandlerList().clear();
				getEventHandlerList().addAll((Collection<? extends EObject>)newValue);
				return;
			case VxBPELPackage.BPEL_SCOPE_ELEMENT__ACTIVITY:
				setActivity((BPELActivity)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case VxBPELPackage.BPEL_SCOPE_ELEMENT__EXIT_ON_STANDARD_FAULT:
				setExitOnStandardFault(EXIT_ON_STANDARD_FAULT_EDEFAULT);
				return;
			case VxBPELPackage.BPEL_SCOPE_ELEMENT__PARTNER_LINKS:
				getPartnerLinks().clear();
				return;
			case VxBPELPackage.BPEL_SCOPE_ELEMENT__MESSAGE_EXCHANGES:
				getMessageExchanges().clear();
				return;
			case VxBPELPackage.BPEL_SCOPE_ELEMENT__VARIABLES:
				getVariables().clear();
				return;
			case VxBPELPackage.BPEL_SCOPE_ELEMENT__CORRELATION_SETS:
				getCorrelationSets().clear();
				return;
			case VxBPELPackage.BPEL_SCOPE_ELEMENT__FAULT_HANDLER_LIST:
				getFaultHandlerList().clear();
				return;
			case VxBPELPackage.BPEL_SCOPE_ELEMENT__EVENT_HANDLER_LIST:
				getEventHandlerList().clear();
				return;
			case VxBPELPackage.BPEL_SCOPE_ELEMENT__ACTIVITY:
				setActivity((BPELActivity)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case VxBPELPackage.BPEL_SCOPE_ELEMENT__EXIT_ON_STANDARD_FAULT:
				return EXIT_ON_STANDARD_FAULT_EDEFAULT == null ? exitOnStandardFault != null : !EXIT_ON_STANDARD_FAULT_EDEFAULT.equals(exitOnStandardFault);
			case VxBPELPackage.BPEL_SCOPE_ELEMENT__PARTNER_LINKS:
				return partnerLinks != null && !partnerLinks.isEmpty();
			case VxBPELPackage.BPEL_SCOPE_ELEMENT__MESSAGE_EXCHANGES:
				return messageExchanges != null && !messageExchanges.isEmpty();
			case VxBPELPackage.BPEL_SCOPE_ELEMENT__VARIABLES:
				return variables != null && !variables.isEmpty();
			case VxBPELPackage.BPEL_SCOPE_ELEMENT__CORRELATION_SETS:
				return correlationSets != null && !correlationSets.isEmpty();
			case VxBPELPackage.BPEL_SCOPE_ELEMENT__FAULT_HANDLER_LIST:
				return faultHandlerList != null && !faultHandlerList.isEmpty();
			case VxBPELPackage.BPEL_SCOPE_ELEMENT__EVENT_HANDLER_LIST:
				return eventHandlerList != null && !eventHandlerList.isEmpty();
			case VxBPELPackage.BPEL_SCOPE_ELEMENT__ACTIVITY:
				return activity != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (exitOnStandardFault: ");
		result.append(exitOnStandardFault);
		result.append(')');
		return result.toString();
	}

} //BPELScopeElementImpl
