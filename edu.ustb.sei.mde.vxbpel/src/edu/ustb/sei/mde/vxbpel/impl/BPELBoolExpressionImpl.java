/**
 */
package edu.ustb.sei.mde.vxbpel.impl;

import edu.ustb.sei.mde.vxbpel.BPELBoolExpression;
import edu.ustb.sei.mde.vxbpel.VxBPELPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>BPEL Bool Expression</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class BPELBoolExpressionImpl extends BPELExpressionImpl implements BPELBoolExpression {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BPELBoolExpressionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return VxBPELPackage.Literals.BPEL_BOOL_EXPRESSION;
	}

} //BPELBoolExpressionImpl
