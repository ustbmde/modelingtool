/**
 */
package edu.ustb.sei.mde.vxbpel.impl;

import edu.ustb.sei.mde.vxbpel.BPELDeadlineExpression;
import edu.ustb.sei.mde.vxbpel.BPELDurationExpression;
import edu.ustb.sei.mde.vxbpel.BPELWait;
import edu.ustb.sei.mde.vxbpel.VxBPELPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>BPEL Wait</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.impl.BPELWaitImpl#getDeadlineExpression <em>Deadline Expression</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.impl.BPELWaitImpl#getDurationExpression <em>Duration Expression</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class BPELWaitImpl extends BPELActivityImpl implements BPELWait {
	/**
	 * The cached value of the '{@link #getDeadlineExpression() <em>Deadline Expression</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDeadlineExpression()
	 * @generated
	 * @ordered
	 */
	protected BPELDeadlineExpression deadlineExpression;

	/**
	 * The cached value of the '{@link #getDurationExpression() <em>Duration Expression</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDurationExpression()
	 * @generated
	 * @ordered
	 */
	protected BPELDurationExpression durationExpression;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BPELWaitImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return VxBPELPackage.Literals.BPEL_WAIT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BPELDeadlineExpression getDeadlineExpression() {
		return deadlineExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDeadlineExpression(BPELDeadlineExpression newDeadlineExpression, NotificationChain msgs) {
		BPELDeadlineExpression oldDeadlineExpression = deadlineExpression;
		deadlineExpression = newDeadlineExpression;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, VxBPELPackage.BPEL_WAIT__DEADLINE_EXPRESSION, oldDeadlineExpression, newDeadlineExpression);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDeadlineExpression(BPELDeadlineExpression newDeadlineExpression) {
		if (newDeadlineExpression != deadlineExpression) {
			NotificationChain msgs = null;
			if (deadlineExpression != null)
				msgs = ((InternalEObject)deadlineExpression).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - VxBPELPackage.BPEL_WAIT__DEADLINE_EXPRESSION, null, msgs);
			if (newDeadlineExpression != null)
				msgs = ((InternalEObject)newDeadlineExpression).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - VxBPELPackage.BPEL_WAIT__DEADLINE_EXPRESSION, null, msgs);
			msgs = basicSetDeadlineExpression(newDeadlineExpression, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VxBPELPackage.BPEL_WAIT__DEADLINE_EXPRESSION, newDeadlineExpression, newDeadlineExpression));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BPELDurationExpression getDurationExpression() {
		return durationExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDurationExpression(BPELDurationExpression newDurationExpression, NotificationChain msgs) {
		BPELDurationExpression oldDurationExpression = durationExpression;
		durationExpression = newDurationExpression;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, VxBPELPackage.BPEL_WAIT__DURATION_EXPRESSION, oldDurationExpression, newDurationExpression);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDurationExpression(BPELDurationExpression newDurationExpression) {
		if (newDurationExpression != durationExpression) {
			NotificationChain msgs = null;
			if (durationExpression != null)
				msgs = ((InternalEObject)durationExpression).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - VxBPELPackage.BPEL_WAIT__DURATION_EXPRESSION, null, msgs);
			if (newDurationExpression != null)
				msgs = ((InternalEObject)newDurationExpression).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - VxBPELPackage.BPEL_WAIT__DURATION_EXPRESSION, null, msgs);
			msgs = basicSetDurationExpression(newDurationExpression, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VxBPELPackage.BPEL_WAIT__DURATION_EXPRESSION, newDurationExpression, newDurationExpression));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case VxBPELPackage.BPEL_WAIT__DEADLINE_EXPRESSION:
				return basicSetDeadlineExpression(null, msgs);
			case VxBPELPackage.BPEL_WAIT__DURATION_EXPRESSION:
				return basicSetDurationExpression(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case VxBPELPackage.BPEL_WAIT__DEADLINE_EXPRESSION:
				return getDeadlineExpression();
			case VxBPELPackage.BPEL_WAIT__DURATION_EXPRESSION:
				return getDurationExpression();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case VxBPELPackage.BPEL_WAIT__DEADLINE_EXPRESSION:
				setDeadlineExpression((BPELDeadlineExpression)newValue);
				return;
			case VxBPELPackage.BPEL_WAIT__DURATION_EXPRESSION:
				setDurationExpression((BPELDurationExpression)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case VxBPELPackage.BPEL_WAIT__DEADLINE_EXPRESSION:
				setDeadlineExpression((BPELDeadlineExpression)null);
				return;
			case VxBPELPackage.BPEL_WAIT__DURATION_EXPRESSION:
				setDurationExpression((BPELDurationExpression)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case VxBPELPackage.BPEL_WAIT__DEADLINE_EXPRESSION:
				return deadlineExpression != null;
			case VxBPELPackage.BPEL_WAIT__DURATION_EXPRESSION:
				return durationExpression != null;
		}
		return super.eIsSet(featureID);
	}

} //BPELWaitImpl
