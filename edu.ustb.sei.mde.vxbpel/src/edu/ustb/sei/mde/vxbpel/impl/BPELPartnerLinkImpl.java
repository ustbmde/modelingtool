/**
 */
package edu.ustb.sei.mde.vxbpel.impl;

import edu.ustb.sei.mde.vxbpel.BPELPartnerLink;
import edu.ustb.sei.mde.vxbpel.VxBPELPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>BPEL Partner Link</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.impl.BPELPartnerLinkImpl#getName <em>Name</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.impl.BPELPartnerLinkImpl#getPartnerLinkType <em>Partner Link Type</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.impl.BPELPartnerLinkImpl#getMyRole <em>My Role</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.impl.BPELPartnerLinkImpl#getPartnerRole <em>Partner Role</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.impl.BPELPartnerLinkImpl#getInitializePartnerRole <em>Initialize Partner Role</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class BPELPartnerLinkImpl extends XMLAttributeContainerImpl implements BPELPartnerLink {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getPartnerLinkType() <em>Partner Link Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPartnerLinkType()
	 * @generated
	 * @ordered
	 */
	protected static final String PARTNER_LINK_TYPE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getPartnerLinkType() <em>Partner Link Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPartnerLinkType()
	 * @generated
	 * @ordered
	 */
	protected String partnerLinkType = PARTNER_LINK_TYPE_EDEFAULT;

	/**
	 * The default value of the '{@link #getMyRole() <em>My Role</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMyRole()
	 * @generated
	 * @ordered
	 */
	protected static final String MY_ROLE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getMyRole() <em>My Role</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMyRole()
	 * @generated
	 * @ordered
	 */
	protected String myRole = MY_ROLE_EDEFAULT;

	/**
	 * The default value of the '{@link #getPartnerRole() <em>Partner Role</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPartnerRole()
	 * @generated
	 * @ordered
	 */
	protected static final String PARTNER_ROLE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getPartnerRole() <em>Partner Role</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPartnerRole()
	 * @generated
	 * @ordered
	 */
	protected String partnerRole = PARTNER_ROLE_EDEFAULT;

	/**
	 * The default value of the '{@link #getInitializePartnerRole() <em>Initialize Partner Role</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInitializePartnerRole()
	 * @generated
	 * @ordered
	 */
	protected static final String INITIALIZE_PARTNER_ROLE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getInitializePartnerRole() <em>Initialize Partner Role</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInitializePartnerRole()
	 * @generated
	 * @ordered
	 */
	protected String initializePartnerRole = INITIALIZE_PARTNER_ROLE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BPELPartnerLinkImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return VxBPELPackage.Literals.BPEL_PARTNER_LINK;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VxBPELPackage.BPEL_PARTNER_LINK__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getPartnerLinkType() {
		return partnerLinkType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPartnerLinkType(String newPartnerLinkType) {
		String oldPartnerLinkType = partnerLinkType;
		partnerLinkType = newPartnerLinkType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VxBPELPackage.BPEL_PARTNER_LINK__PARTNER_LINK_TYPE, oldPartnerLinkType, partnerLinkType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getMyRole() {
		return myRole;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMyRole(String newMyRole) {
		String oldMyRole = myRole;
		myRole = newMyRole;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VxBPELPackage.BPEL_PARTNER_LINK__MY_ROLE, oldMyRole, myRole));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getPartnerRole() {
		return partnerRole;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPartnerRole(String newPartnerRole) {
		String oldPartnerRole = partnerRole;
		partnerRole = newPartnerRole;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VxBPELPackage.BPEL_PARTNER_LINK__PARTNER_ROLE, oldPartnerRole, partnerRole));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getInitializePartnerRole() {
		return initializePartnerRole;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInitializePartnerRole(String newInitializePartnerRole) {
		String oldInitializePartnerRole = initializePartnerRole;
		initializePartnerRole = newInitializePartnerRole;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VxBPELPackage.BPEL_PARTNER_LINK__INITIALIZE_PARTNER_ROLE, oldInitializePartnerRole, initializePartnerRole));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case VxBPELPackage.BPEL_PARTNER_LINK__NAME:
				return getName();
			case VxBPELPackage.BPEL_PARTNER_LINK__PARTNER_LINK_TYPE:
				return getPartnerLinkType();
			case VxBPELPackage.BPEL_PARTNER_LINK__MY_ROLE:
				return getMyRole();
			case VxBPELPackage.BPEL_PARTNER_LINK__PARTNER_ROLE:
				return getPartnerRole();
			case VxBPELPackage.BPEL_PARTNER_LINK__INITIALIZE_PARTNER_ROLE:
				return getInitializePartnerRole();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case VxBPELPackage.BPEL_PARTNER_LINK__NAME:
				setName((String)newValue);
				return;
			case VxBPELPackage.BPEL_PARTNER_LINK__PARTNER_LINK_TYPE:
				setPartnerLinkType((String)newValue);
				return;
			case VxBPELPackage.BPEL_PARTNER_LINK__MY_ROLE:
				setMyRole((String)newValue);
				return;
			case VxBPELPackage.BPEL_PARTNER_LINK__PARTNER_ROLE:
				setPartnerRole((String)newValue);
				return;
			case VxBPELPackage.BPEL_PARTNER_LINK__INITIALIZE_PARTNER_ROLE:
				setInitializePartnerRole((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case VxBPELPackage.BPEL_PARTNER_LINK__NAME:
				setName(NAME_EDEFAULT);
				return;
			case VxBPELPackage.BPEL_PARTNER_LINK__PARTNER_LINK_TYPE:
				setPartnerLinkType(PARTNER_LINK_TYPE_EDEFAULT);
				return;
			case VxBPELPackage.BPEL_PARTNER_LINK__MY_ROLE:
				setMyRole(MY_ROLE_EDEFAULT);
				return;
			case VxBPELPackage.BPEL_PARTNER_LINK__PARTNER_ROLE:
				setPartnerRole(PARTNER_ROLE_EDEFAULT);
				return;
			case VxBPELPackage.BPEL_PARTNER_LINK__INITIALIZE_PARTNER_ROLE:
				setInitializePartnerRole(INITIALIZE_PARTNER_ROLE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case VxBPELPackage.BPEL_PARTNER_LINK__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case VxBPELPackage.BPEL_PARTNER_LINK__PARTNER_LINK_TYPE:
				return PARTNER_LINK_TYPE_EDEFAULT == null ? partnerLinkType != null : !PARTNER_LINK_TYPE_EDEFAULT.equals(partnerLinkType);
			case VxBPELPackage.BPEL_PARTNER_LINK__MY_ROLE:
				return MY_ROLE_EDEFAULT == null ? myRole != null : !MY_ROLE_EDEFAULT.equals(myRole);
			case VxBPELPackage.BPEL_PARTNER_LINK__PARTNER_ROLE:
				return PARTNER_ROLE_EDEFAULT == null ? partnerRole != null : !PARTNER_ROLE_EDEFAULT.equals(partnerRole);
			case VxBPELPackage.BPEL_PARTNER_LINK__INITIALIZE_PARTNER_ROLE:
				return INITIALIZE_PARTNER_ROLE_EDEFAULT == null ? initializePartnerRole != null : !INITIALIZE_PARTNER_ROLE_EDEFAULT.equals(initializePartnerRole);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", partnerLinkType: ");
		result.append(partnerLinkType);
		result.append(", myRole: ");
		result.append(myRole);
		result.append(", partnerRole: ");
		result.append(partnerRole);
		result.append(", initializePartnerRole: ");
		result.append(initializePartnerRole);
		result.append(')');
		return result.toString();
	}

} //BPELPartnerLinkImpl
