/**
 */
package edu.ustb.sei.mde.vxbpel.impl;

import edu.ustb.sei.mde.vxbpel.BPELActivity;
import edu.ustb.sei.mde.vxbpel.BPELBoolExpression;
import edu.ustb.sei.mde.vxbpel.BPELWhile;
import edu.ustb.sei.mde.vxbpel.VxBPELPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>BPEL While</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.impl.BPELWhileImpl#getCondition <em>Condition</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.impl.BPELWhileImpl#getBodyActivity <em>Body Activity</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class BPELWhileImpl extends BPELActivityImpl implements BPELWhile {
	/**
	 * The cached value of the '{@link #getCondition() <em>Condition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCondition()
	 * @generated
	 * @ordered
	 */
	protected BPELBoolExpression condition;

	/**
	 * The cached value of the '{@link #getBodyActivity() <em>Body Activity</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBodyActivity()
	 * @generated
	 * @ordered
	 */
	protected BPELActivity bodyActivity;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BPELWhileImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return VxBPELPackage.Literals.BPEL_WHILE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BPELBoolExpression getCondition() {
		return condition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCondition(BPELBoolExpression newCondition, NotificationChain msgs) {
		BPELBoolExpression oldCondition = condition;
		condition = newCondition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, VxBPELPackage.BPEL_WHILE__CONDITION, oldCondition, newCondition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCondition(BPELBoolExpression newCondition) {
		if (newCondition != condition) {
			NotificationChain msgs = null;
			if (condition != null)
				msgs = ((InternalEObject)condition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - VxBPELPackage.BPEL_WHILE__CONDITION, null, msgs);
			if (newCondition != null)
				msgs = ((InternalEObject)newCondition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - VxBPELPackage.BPEL_WHILE__CONDITION, null, msgs);
			msgs = basicSetCondition(newCondition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VxBPELPackage.BPEL_WHILE__CONDITION, newCondition, newCondition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BPELActivity getBodyActivity() {
		return bodyActivity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetBodyActivity(BPELActivity newBodyActivity, NotificationChain msgs) {
		BPELActivity oldBodyActivity = bodyActivity;
		bodyActivity = newBodyActivity;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, VxBPELPackage.BPEL_WHILE__BODY_ACTIVITY, oldBodyActivity, newBodyActivity);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBodyActivity(BPELActivity newBodyActivity) {
		if (newBodyActivity != bodyActivity) {
			NotificationChain msgs = null;
			if (bodyActivity != null)
				msgs = ((InternalEObject)bodyActivity).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - VxBPELPackage.BPEL_WHILE__BODY_ACTIVITY, null, msgs);
			if (newBodyActivity != null)
				msgs = ((InternalEObject)newBodyActivity).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - VxBPELPackage.BPEL_WHILE__BODY_ACTIVITY, null, msgs);
			msgs = basicSetBodyActivity(newBodyActivity, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VxBPELPackage.BPEL_WHILE__BODY_ACTIVITY, newBodyActivity, newBodyActivity));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case VxBPELPackage.BPEL_WHILE__CONDITION:
				return basicSetCondition(null, msgs);
			case VxBPELPackage.BPEL_WHILE__BODY_ACTIVITY:
				return basicSetBodyActivity(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case VxBPELPackage.BPEL_WHILE__CONDITION:
				return getCondition();
			case VxBPELPackage.BPEL_WHILE__BODY_ACTIVITY:
				return getBodyActivity();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case VxBPELPackage.BPEL_WHILE__CONDITION:
				setCondition((BPELBoolExpression)newValue);
				return;
			case VxBPELPackage.BPEL_WHILE__BODY_ACTIVITY:
				setBodyActivity((BPELActivity)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case VxBPELPackage.BPEL_WHILE__CONDITION:
				setCondition((BPELBoolExpression)null);
				return;
			case VxBPELPackage.BPEL_WHILE__BODY_ACTIVITY:
				setBodyActivity((BPELActivity)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case VxBPELPackage.BPEL_WHILE__CONDITION:
				return condition != null;
			case VxBPELPackage.BPEL_WHILE__BODY_ACTIVITY:
				return bodyActivity != null;
		}
		return super.eIsSet(featureID);
	}

} //BPELWhileImpl
