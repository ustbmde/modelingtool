/**
 */
package edu.ustb.sei.mde.vxbpel.impl;

import edu.ustb.sei.mde.vxbpel.BPELActivity;
import edu.ustb.sei.mde.vxbpel.BPELFlow;
import edu.ustb.sei.mde.vxbpel.BPELLink;
import edu.ustb.sei.mde.vxbpel.VxBPELPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>BPEL Flow</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.impl.BPELFlowImpl#getLinks <em>Links</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.impl.BPELFlowImpl#getBodyActivities <em>Body Activities</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class BPELFlowImpl extends BPELActivityImpl implements BPELFlow {
	/**
	 * The cached value of the '{@link #getLinks() <em>Links</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLinks()
	 * @generated
	 * @ordered
	 */
	protected EList<BPELLink> links;

	/**
	 * The cached value of the '{@link #getBodyActivities() <em>Body Activities</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBodyActivities()
	 * @generated
	 * @ordered
	 */
	protected EList<BPELActivity> bodyActivities;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BPELFlowImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return VxBPELPackage.Literals.BPEL_FLOW;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<BPELLink> getLinks() {
		if (links == null) {
			links = new EObjectContainmentEList<BPELLink>(BPELLink.class, this, VxBPELPackage.BPEL_FLOW__LINKS);
		}
		return links;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<BPELActivity> getBodyActivities() {
		if (bodyActivities == null) {
			bodyActivities = new EObjectContainmentEList<BPELActivity>(BPELActivity.class, this, VxBPELPackage.BPEL_FLOW__BODY_ACTIVITIES);
		}
		return bodyActivities;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case VxBPELPackage.BPEL_FLOW__LINKS:
				return ((InternalEList<?>)getLinks()).basicRemove(otherEnd, msgs);
			case VxBPELPackage.BPEL_FLOW__BODY_ACTIVITIES:
				return ((InternalEList<?>)getBodyActivities()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case VxBPELPackage.BPEL_FLOW__LINKS:
				return getLinks();
			case VxBPELPackage.BPEL_FLOW__BODY_ACTIVITIES:
				return getBodyActivities();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case VxBPELPackage.BPEL_FLOW__LINKS:
				getLinks().clear();
				getLinks().addAll((Collection<? extends BPELLink>)newValue);
				return;
			case VxBPELPackage.BPEL_FLOW__BODY_ACTIVITIES:
				getBodyActivities().clear();
				getBodyActivities().addAll((Collection<? extends BPELActivity>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case VxBPELPackage.BPEL_FLOW__LINKS:
				getLinks().clear();
				return;
			case VxBPELPackage.BPEL_FLOW__BODY_ACTIVITIES:
				getBodyActivities().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case VxBPELPackage.BPEL_FLOW__LINKS:
				return links != null && !links.isEmpty();
			case VxBPELPackage.BPEL_FLOW__BODY_ACTIVITIES:
				return bodyActivities != null && !bodyActivities.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //BPELFlowImpl
