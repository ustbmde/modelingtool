/**
 */
package edu.ustb.sei.mde.vxbpel.impl;

import edu.ustb.sei.mde.vxbpel.BPELActivity;
import edu.ustb.sei.mde.vxbpel.BPELAssign;
import edu.ustb.sei.mde.vxbpel.BPELAssignOperation;
import edu.ustb.sei.mde.vxbpel.BPELBoolExpression;
import edu.ustb.sei.mde.vxbpel.BPELCopy;
import edu.ustb.sei.mde.vxbpel.BPELCopyFrom;
import edu.ustb.sei.mde.vxbpel.BPELCopyTo;
import edu.ustb.sei.mde.vxbpel.BPELCorrelationSet;
import edu.ustb.sei.mde.vxbpel.BPELDeadlineExpression;
import edu.ustb.sei.mde.vxbpel.BPELDurationExpression;
import edu.ustb.sei.mde.vxbpel.BPELElseIf;
import edu.ustb.sei.mde.vxbpel.BPELEmpty;
import edu.ustb.sei.mde.vxbpel.BPELExit;
import edu.ustb.sei.mde.vxbpel.BPELExpression;
import edu.ustb.sei.mde.vxbpel.BPELFlow;
import edu.ustb.sei.mde.vxbpel.BPELForEach;
import edu.ustb.sei.mde.vxbpel.BPELIf;
import edu.ustb.sei.mde.vxbpel.BPELImport;
import edu.ustb.sei.mde.vxbpel.BPELInvoke;
import edu.ustb.sei.mde.vxbpel.BPELLink;
import edu.ustb.sei.mde.vxbpel.BPELLiteral;
import edu.ustb.sei.mde.vxbpel.BPELMessageExchange;
import edu.ustb.sei.mde.vxbpel.BPELPartnerLink;
import edu.ustb.sei.mde.vxbpel.BPELPick;
import edu.ustb.sei.mde.vxbpel.BPELProcess;
import edu.ustb.sei.mde.vxbpel.BPELQuery;
import edu.ustb.sei.mde.vxbpel.BPELReceive;
import edu.ustb.sei.mde.vxbpel.BPELRepeatUntil;
import edu.ustb.sei.mde.vxbpel.BPELReply;
import edu.ustb.sei.mde.vxbpel.BPELRethrow;
import edu.ustb.sei.mde.vxbpel.BPELScope;
import edu.ustb.sei.mde.vxbpel.BPELScopeElement;
import edu.ustb.sei.mde.vxbpel.BPELSequence;
import edu.ustb.sei.mde.vxbpel.BPELSource;
import edu.ustb.sei.mde.vxbpel.BPELSourceList;
import edu.ustb.sei.mde.vxbpel.BPELTarget;
import edu.ustb.sei.mde.vxbpel.BPELTargetList;
import edu.ustb.sei.mde.vxbpel.BPELThrow;
import edu.ustb.sei.mde.vxbpel.BPELUnsignedIntegerExpression;
import edu.ustb.sei.mde.vxbpel.BPELVariable;
import edu.ustb.sei.mde.vxbpel.BPELWait;
import edu.ustb.sei.mde.vxbpel.BPELWhile;
import edu.ustb.sei.mde.vxbpel.NamedElement;
import edu.ustb.sei.mde.vxbpel.VxBPELConfigurableVariant;
import edu.ustb.sei.mde.vxbpel.VxBPELConfigurableVariationPoint;
import edu.ustb.sei.mde.vxbpel.VxBPELFactory;
import edu.ustb.sei.mde.vxbpel.VxBPELPackage;
import edu.ustb.sei.mde.vxbpel.VxBPELProcess;
import edu.ustb.sei.mde.vxbpel.VxBPELVariant;
import edu.ustb.sei.mde.vxbpel.VxBPELVariationPoint;
import edu.ustb.sei.mde.vxbpel.VxBPELVariationPointPChoice;
import edu.ustb.sei.mde.vxbpel.XMLAttribute;
import edu.ustb.sei.mde.vxbpel.XMLAttributeContainer;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class VxBPELPackageImpl extends EPackageImpl implements VxBPELPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass namedElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass xmlAttributeContainerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass xmlAttributeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass bpelExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass bpelBoolExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass bpelDeadlineExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass bpelDurationExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass bpelUnsignedIntegerExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass bpelScopeElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass bpelProcessEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass bpelImportEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass bpelPartnerLinkEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass bpelMessageExchangeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass bpelVariableEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass bpelCorrelationSetEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass bpelActivityEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass bpelTargetListEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass bpelTargetEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass bpelSourceListEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass bpelSourceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass bpelInvokeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass bpelReceiveEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass bpelReplyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass bpelAssignEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass bpelAssignOperationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass bpelCopyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass bpelCopyFromEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass bpelCopyToEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass bpelLiteralEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass bpelQueryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass bpelThrowEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass bpelRethrowEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass bpelWaitEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass bpelEmptyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass bpelExitEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass bpelSequenceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass bpelIfEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass bpelElseIfEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass bpelWhileEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass bpelRepeatUntilEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass bpelPickEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass bpelFlowEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass bpelLinkEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass bpelForEachEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass bpelScopeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass vxBPELVariationPointEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass vxBPELVariantEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass vxBPELConfigurableVariationPointEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass vxBPELConfigurableVariantEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass vxBPELVariationPointPChoiceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass vxBPELProcessEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private VxBPELPackageImpl() {
		super(eNS_URI, VxBPELFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link VxBPELPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static VxBPELPackage init() {
		if (isInited) return (VxBPELPackage)EPackage.Registry.INSTANCE.getEPackage(VxBPELPackage.eNS_URI);

		// Obtain or create and register package
		VxBPELPackageImpl theVxBPELPackage = (VxBPELPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof VxBPELPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new VxBPELPackageImpl());

		isInited = true;

		// Create package meta-data objects
		theVxBPELPackage.createPackageContents();

		// Initialize created meta-data
		theVxBPELPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theVxBPELPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(VxBPELPackage.eNS_URI, theVxBPELPackage);
		return theVxBPELPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNamedElement() {
		return namedElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getNamedElement_Name() {
		return (EAttribute)namedElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getXMLAttributeContainer() {
		return xmlAttributeContainerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getXMLAttributeContainer_Attributes() {
		return (EReference)xmlAttributeContainerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getXMLAttribute() {
		return xmlAttributeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getXMLAttribute_Name() {
		return (EAttribute)xmlAttributeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getXMLAttribute_Value() {
		return (EAttribute)xmlAttributeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBPELExpression() {
		return bpelExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBPELExpression_ExpressionLanguage() {
		return (EAttribute)bpelExpressionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBPELExpression_Expression() {
		return (EAttribute)bpelExpressionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBPELBoolExpression() {
		return bpelBoolExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBPELDeadlineExpression() {
		return bpelDeadlineExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBPELDurationExpression() {
		return bpelDurationExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBPELUnsignedIntegerExpression() {
		return bpelUnsignedIntegerExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBPELScopeElement() {
		return bpelScopeElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBPELScopeElement_ExitOnStandardFault() {
		return (EAttribute)bpelScopeElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBPELScopeElement_PartnerLinks() {
		return (EReference)bpelScopeElementEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBPELScopeElement_MessageExchanges() {
		return (EReference)bpelScopeElementEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBPELScopeElement_Variables() {
		return (EReference)bpelScopeElementEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBPELScopeElement_CorrelationSets() {
		return (EReference)bpelScopeElementEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBPELScopeElement_FaultHandlerList() {
		return (EReference)bpelScopeElementEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBPELScopeElement_EventHandlerList() {
		return (EReference)bpelScopeElementEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBPELScopeElement_Activity() {
		return (EReference)bpelScopeElementEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBPELProcess() {
		return bpelProcessEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBPELProcess_TargetNamespace() {
		return (EAttribute)bpelProcessEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBPELProcess_QueryLanguage() {
		return (EAttribute)bpelProcessEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBPELProcess_ExpressionLanguage() {
		return (EAttribute)bpelProcessEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBPELProcess_Imports() {
		return (EReference)bpelProcessEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBPELImport() {
		return bpelImportEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBPELImport_Namespace() {
		return (EAttribute)bpelImportEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBPELImport_Location() {
		return (EAttribute)bpelImportEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBPELImport_ImportType() {
		return (EAttribute)bpelImportEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBPELPartnerLink() {
		return bpelPartnerLinkEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBPELPartnerLink_Name() {
		return (EAttribute)bpelPartnerLinkEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBPELPartnerLink_PartnerLinkType() {
		return (EAttribute)bpelPartnerLinkEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBPELPartnerLink_MyRole() {
		return (EAttribute)bpelPartnerLinkEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBPELPartnerLink_PartnerRole() {
		return (EAttribute)bpelPartnerLinkEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBPELPartnerLink_InitializePartnerRole() {
		return (EAttribute)bpelPartnerLinkEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBPELMessageExchange() {
		return bpelMessageExchangeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBPELMessageExchange_Name() {
		return (EAttribute)bpelMessageExchangeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBPELVariable() {
		return bpelVariableEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBPELVariable_Name() {
		return (EAttribute)bpelVariableEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBPELVariable_MessageType() {
		return (EAttribute)bpelVariableEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBPELVariable_Type() {
		return (EAttribute)bpelVariableEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBPELVariable_Element() {
		return (EAttribute)bpelVariableEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBPELVariable_FromSpec() {
		return (EAttribute)bpelVariableEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBPELCorrelationSet() {
		return bpelCorrelationSetEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBPELCorrelationSet_Name() {
		return (EAttribute)bpelCorrelationSetEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBPELCorrelationSet_Properties() {
		return (EAttribute)bpelCorrelationSetEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBPELActivity() {
		return bpelActivityEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBPELActivity_TargetSet() {
		return (EReference)bpelActivityEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBPELActivity_SourceSet() {
		return (EReference)bpelActivityEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBPELTargetList() {
		return bpelTargetListEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBPELTargetList_JoinCondition() {
		return (EReference)bpelTargetListEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBPELTargetList_Targets() {
		return (EReference)bpelTargetListEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBPELTarget() {
		return bpelTargetEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBPELTarget_LinkName() {
		return (EAttribute)bpelTargetEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBPELSourceList() {
		return bpelSourceListEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBPELSourceList_Sources() {
		return (EReference)bpelSourceListEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBPELSource() {
		return bpelSourceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBPELSource_LinkName() {
		return (EAttribute)bpelSourceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBPELSource_TransitionCondition() {
		return (EReference)bpelSourceEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBPELInvoke() {
		return bpelInvokeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBPELInvoke_PartnerLink() {
		return (EAttribute)bpelInvokeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBPELInvoke_PortType() {
		return (EAttribute)bpelInvokeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBPELInvoke_Operation() {
		return (EAttribute)bpelInvokeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBPELInvoke_InputVariable() {
		return (EAttribute)bpelInvokeEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBPELInvoke_OutputVariable() {
		return (EAttribute)bpelInvokeEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBPELReceive() {
		return bpelReceiveEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBPELReceive_PartnerLink() {
		return (EAttribute)bpelReceiveEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBPELReceive_PortType() {
		return (EAttribute)bpelReceiveEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBPELReceive_Operation() {
		return (EAttribute)bpelReceiveEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBPELReceive_Variable() {
		return (EAttribute)bpelReceiveEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBPELReceive_CreateInstance() {
		return (EAttribute)bpelReceiveEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBPELReceive_MessageExchange() {
		return (EAttribute)bpelReceiveEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBPELReply() {
		return bpelReplyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBPELReply_PartnerLink() {
		return (EAttribute)bpelReplyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBPELReply_PortType() {
		return (EAttribute)bpelReplyEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBPELReply_Operation() {
		return (EAttribute)bpelReplyEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBPELReply_Variable() {
		return (EAttribute)bpelReplyEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBPELReply_FaultName() {
		return (EAttribute)bpelReplyEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBPELReply_MessageExchange() {
		return (EAttribute)bpelReplyEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBPELAssign() {
		return bpelAssignEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBPELAssign_Validate() {
		return (EAttribute)bpelAssignEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBPELAssign_Parts() {
		return (EReference)bpelAssignEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBPELAssignOperation() {
		return bpelAssignOperationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBPELCopy() {
		return bpelCopyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBPELCopy_FromSpec() {
		return (EReference)bpelCopyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBPELCopy_ToSpec() {
		return (EReference)bpelCopyEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBPELCopyFrom() {
		return bpelCopyFromEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBPELCopyFrom_Variable() {
		return (EAttribute)bpelCopyFromEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBPELCopyFrom_Part() {
		return (EAttribute)bpelCopyFromEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBPELCopyFrom_Property() {
		return (EAttribute)bpelCopyFromEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBPELCopyFrom_PartnerLink() {
		return (EAttribute)bpelCopyFromEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBPELCopyFrom_EndpointReference() {
		return (EAttribute)bpelCopyFromEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBPELCopyFrom_Expression() {
		return (EReference)bpelCopyFromEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBPELCopyFrom_Query() {
		return (EReference)bpelCopyFromEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBPELCopyFrom_Literal() {
		return (EReference)bpelCopyFromEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBPELCopyTo() {
		return bpelCopyToEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBPELCopyTo_Variable() {
		return (EAttribute)bpelCopyToEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBPELCopyTo_Part() {
		return (EAttribute)bpelCopyToEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBPELCopyTo_Property() {
		return (EAttribute)bpelCopyToEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBPELCopyTo_PartnerLink() {
		return (EAttribute)bpelCopyToEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBPELCopyTo_Expression() {
		return (EReference)bpelCopyToEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBPELCopyTo_Query() {
		return (EReference)bpelCopyToEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBPELLiteral() {
		return bpelLiteralEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBPELLiteral_Type() {
		return (EAttribute)bpelLiteralEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBPELLiteral_Literal() {
		return (EAttribute)bpelLiteralEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBPELQuery() {
		return bpelQueryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBPELQuery_QueryLanguage() {
		return (EAttribute)bpelQueryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBPELQuery_Content() {
		return (EAttribute)bpelQueryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBPELThrow() {
		return bpelThrowEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBPELThrow_FaultName() {
		return (EAttribute)bpelThrowEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBPELThrow_FaultVariable() {
		return (EAttribute)bpelThrowEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBPELRethrow() {
		return bpelRethrowEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBPELWait() {
		return bpelWaitEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBPELWait_DeadlineExpression() {
		return (EReference)bpelWaitEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBPELWait_DurationExpression() {
		return (EReference)bpelWaitEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBPELEmpty() {
		return bpelEmptyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBPELExit() {
		return bpelExitEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBPELSequence() {
		return bpelSequenceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBPELSequence_Activities() {
		return (EReference)bpelSequenceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBPELIf() {
		return bpelIfEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBPELIf_Condition() {
		return (EReference)bpelIfEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBPELIf_ThenActivity() {
		return (EReference)bpelIfEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBPELIf_ElseIfActivity() {
		return (EReference)bpelIfEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBPELIf_ElseActivity() {
		return (EReference)bpelIfEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBPELElseIf() {
		return bpelElseIfEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBPELElseIf_Condition() {
		return (EReference)bpelElseIfEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBPELElseIf_ThenActivity() {
		return (EReference)bpelElseIfEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBPELWhile() {
		return bpelWhileEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBPELWhile_Condition() {
		return (EReference)bpelWhileEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBPELWhile_BodyActivity() {
		return (EReference)bpelWhileEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBPELRepeatUntil() {
		return bpelRepeatUntilEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBPELRepeatUntil_BodyActivity() {
		return (EReference)bpelRepeatUntilEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBPELRepeatUntil_Condition() {
		return (EReference)bpelRepeatUntilEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBPELPick() {
		return bpelPickEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBPELFlow() {
		return bpelFlowEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBPELFlow_Links() {
		return (EReference)bpelFlowEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBPELFlow_BodyActivities() {
		return (EReference)bpelFlowEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBPELLink() {
		return bpelLinkEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBPELForEach() {
		return bpelForEachEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBPELForEach_StartCounterValue() {
		return (EReference)bpelForEachEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBPELForEach_FinalCounterValue() {
		return (EReference)bpelForEachEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBPELForEach_CompletionCondition() {
		return (EReference)bpelForEachEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBPELForEach_Scope() {
		return (EReference)bpelForEachEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBPELScope() {
		return bpelScopeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBPELScope_CompensationHandler() {
		return (EReference)bpelScopeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBPELScope_TerminationHandler() {
		return (EReference)bpelScopeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVxBPELVariationPoint() {
		return vxBPELVariationPointEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getVxBPELVariationPoint_Variants() {
		return (EReference)vxBPELVariationPointEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVxBPELVariant() {
		return vxBPELVariantEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getVxBPELVariant_BpelCode() {
		return (EReference)vxBPELVariantEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVxBPELConfigurableVariationPoint() {
		return vxBPELConfigurableVariationPointEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getVxBPELConfigurableVariationPoint_Id() {
		return (EAttribute)vxBPELConfigurableVariationPointEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getVxBPELConfigurableVariationPoint_DefaultVariant() {
		return (EAttribute)vxBPELConfigurableVariationPointEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getVxBPELConfigurableVariationPoint_Variants() {
		return (EReference)vxBPELConfigurableVariationPointEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVxBPELConfigurableVariant() {
		return vxBPELConfigurableVariantEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getVxBPELConfigurableVariant_VariantInfo() {
		return (EAttribute)vxBPELConfigurableVariantEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getVxBPELConfigurableVariant_Rationale() {
		return (EAttribute)vxBPELConfigurableVariantEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getVxBPELConfigurableVariant_RequiredVariants() {
		return (EReference)vxBPELConfigurableVariantEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVxBPELVariationPointPChoice() {
		return vxBPELVariationPointPChoiceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getVxBPELVariationPointPChoice_Vpname() {
		return (EAttribute)vxBPELVariationPointPChoiceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getVxBPELVariationPointPChoice_Variant() {
		return (EAttribute)vxBPELVariationPointPChoiceEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVxBPELProcess() {
		return vxBPELProcessEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getVxBPELProcess_ConfigurableVariationPoints() {
		return (EReference)vxBPELProcessEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VxBPELFactory getVxBPELFactory() {
		return (VxBPELFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		namedElementEClass = createEClass(NAMED_ELEMENT);
		createEAttribute(namedElementEClass, NAMED_ELEMENT__NAME);

		xmlAttributeContainerEClass = createEClass(XML_ATTRIBUTE_CONTAINER);
		createEReference(xmlAttributeContainerEClass, XML_ATTRIBUTE_CONTAINER__ATTRIBUTES);

		xmlAttributeEClass = createEClass(XML_ATTRIBUTE);
		createEAttribute(xmlAttributeEClass, XML_ATTRIBUTE__NAME);
		createEAttribute(xmlAttributeEClass, XML_ATTRIBUTE__VALUE);

		bpelExpressionEClass = createEClass(BPEL_EXPRESSION);
		createEAttribute(bpelExpressionEClass, BPEL_EXPRESSION__EXPRESSION_LANGUAGE);
		createEAttribute(bpelExpressionEClass, BPEL_EXPRESSION__EXPRESSION);

		bpelBoolExpressionEClass = createEClass(BPEL_BOOL_EXPRESSION);

		bpelDeadlineExpressionEClass = createEClass(BPEL_DEADLINE_EXPRESSION);

		bpelDurationExpressionEClass = createEClass(BPEL_DURATION_EXPRESSION);

		bpelUnsignedIntegerExpressionEClass = createEClass(BPEL_UNSIGNED_INTEGER_EXPRESSION);

		bpelScopeElementEClass = createEClass(BPEL_SCOPE_ELEMENT);
		createEAttribute(bpelScopeElementEClass, BPEL_SCOPE_ELEMENT__EXIT_ON_STANDARD_FAULT);
		createEReference(bpelScopeElementEClass, BPEL_SCOPE_ELEMENT__PARTNER_LINKS);
		createEReference(bpelScopeElementEClass, BPEL_SCOPE_ELEMENT__MESSAGE_EXCHANGES);
		createEReference(bpelScopeElementEClass, BPEL_SCOPE_ELEMENT__VARIABLES);
		createEReference(bpelScopeElementEClass, BPEL_SCOPE_ELEMENT__CORRELATION_SETS);
		createEReference(bpelScopeElementEClass, BPEL_SCOPE_ELEMENT__FAULT_HANDLER_LIST);
		createEReference(bpelScopeElementEClass, BPEL_SCOPE_ELEMENT__EVENT_HANDLER_LIST);
		createEReference(bpelScopeElementEClass, BPEL_SCOPE_ELEMENT__ACTIVITY);

		bpelProcessEClass = createEClass(BPEL_PROCESS);
		createEAttribute(bpelProcessEClass, BPEL_PROCESS__TARGET_NAMESPACE);
		createEAttribute(bpelProcessEClass, BPEL_PROCESS__QUERY_LANGUAGE);
		createEAttribute(bpelProcessEClass, BPEL_PROCESS__EXPRESSION_LANGUAGE);
		createEReference(bpelProcessEClass, BPEL_PROCESS__IMPORTS);

		bpelImportEClass = createEClass(BPEL_IMPORT);
		createEAttribute(bpelImportEClass, BPEL_IMPORT__NAMESPACE);
		createEAttribute(bpelImportEClass, BPEL_IMPORT__LOCATION);
		createEAttribute(bpelImportEClass, BPEL_IMPORT__IMPORT_TYPE);

		bpelPartnerLinkEClass = createEClass(BPEL_PARTNER_LINK);
		createEAttribute(bpelPartnerLinkEClass, BPEL_PARTNER_LINK__NAME);
		createEAttribute(bpelPartnerLinkEClass, BPEL_PARTNER_LINK__PARTNER_LINK_TYPE);
		createEAttribute(bpelPartnerLinkEClass, BPEL_PARTNER_LINK__MY_ROLE);
		createEAttribute(bpelPartnerLinkEClass, BPEL_PARTNER_LINK__PARTNER_ROLE);
		createEAttribute(bpelPartnerLinkEClass, BPEL_PARTNER_LINK__INITIALIZE_PARTNER_ROLE);

		bpelMessageExchangeEClass = createEClass(BPEL_MESSAGE_EXCHANGE);
		createEAttribute(bpelMessageExchangeEClass, BPEL_MESSAGE_EXCHANGE__NAME);

		bpelVariableEClass = createEClass(BPEL_VARIABLE);
		createEAttribute(bpelVariableEClass, BPEL_VARIABLE__NAME);
		createEAttribute(bpelVariableEClass, BPEL_VARIABLE__MESSAGE_TYPE);
		createEAttribute(bpelVariableEClass, BPEL_VARIABLE__TYPE);
		createEAttribute(bpelVariableEClass, BPEL_VARIABLE__ELEMENT);
		createEAttribute(bpelVariableEClass, BPEL_VARIABLE__FROM_SPEC);

		bpelCorrelationSetEClass = createEClass(BPEL_CORRELATION_SET);
		createEAttribute(bpelCorrelationSetEClass, BPEL_CORRELATION_SET__NAME);
		createEAttribute(bpelCorrelationSetEClass, BPEL_CORRELATION_SET__PROPERTIES);

		bpelActivityEClass = createEClass(BPEL_ACTIVITY);
		createEReference(bpelActivityEClass, BPEL_ACTIVITY__TARGET_SET);
		createEReference(bpelActivityEClass, BPEL_ACTIVITY__SOURCE_SET);

		bpelTargetListEClass = createEClass(BPEL_TARGET_LIST);
		createEReference(bpelTargetListEClass, BPEL_TARGET_LIST__JOIN_CONDITION);
		createEReference(bpelTargetListEClass, BPEL_TARGET_LIST__TARGETS);

		bpelTargetEClass = createEClass(BPEL_TARGET);
		createEAttribute(bpelTargetEClass, BPEL_TARGET__LINK_NAME);

		bpelSourceListEClass = createEClass(BPEL_SOURCE_LIST);
		createEReference(bpelSourceListEClass, BPEL_SOURCE_LIST__SOURCES);

		bpelSourceEClass = createEClass(BPEL_SOURCE);
		createEAttribute(bpelSourceEClass, BPEL_SOURCE__LINK_NAME);
		createEReference(bpelSourceEClass, BPEL_SOURCE__TRANSITION_CONDITION);

		bpelInvokeEClass = createEClass(BPEL_INVOKE);
		createEAttribute(bpelInvokeEClass, BPEL_INVOKE__PARTNER_LINK);
		createEAttribute(bpelInvokeEClass, BPEL_INVOKE__PORT_TYPE);
		createEAttribute(bpelInvokeEClass, BPEL_INVOKE__OPERATION);
		createEAttribute(bpelInvokeEClass, BPEL_INVOKE__INPUT_VARIABLE);
		createEAttribute(bpelInvokeEClass, BPEL_INVOKE__OUTPUT_VARIABLE);

		bpelReceiveEClass = createEClass(BPEL_RECEIVE);
		createEAttribute(bpelReceiveEClass, BPEL_RECEIVE__PARTNER_LINK);
		createEAttribute(bpelReceiveEClass, BPEL_RECEIVE__PORT_TYPE);
		createEAttribute(bpelReceiveEClass, BPEL_RECEIVE__OPERATION);
		createEAttribute(bpelReceiveEClass, BPEL_RECEIVE__VARIABLE);
		createEAttribute(bpelReceiveEClass, BPEL_RECEIVE__CREATE_INSTANCE);
		createEAttribute(bpelReceiveEClass, BPEL_RECEIVE__MESSAGE_EXCHANGE);

		bpelReplyEClass = createEClass(BPEL_REPLY);
		createEAttribute(bpelReplyEClass, BPEL_REPLY__PARTNER_LINK);
		createEAttribute(bpelReplyEClass, BPEL_REPLY__PORT_TYPE);
		createEAttribute(bpelReplyEClass, BPEL_REPLY__OPERATION);
		createEAttribute(bpelReplyEClass, BPEL_REPLY__VARIABLE);
		createEAttribute(bpelReplyEClass, BPEL_REPLY__FAULT_NAME);
		createEAttribute(bpelReplyEClass, BPEL_REPLY__MESSAGE_EXCHANGE);

		bpelAssignEClass = createEClass(BPEL_ASSIGN);
		createEAttribute(bpelAssignEClass, BPEL_ASSIGN__VALIDATE);
		createEReference(bpelAssignEClass, BPEL_ASSIGN__PARTS);

		bpelAssignOperationEClass = createEClass(BPEL_ASSIGN_OPERATION);

		bpelCopyEClass = createEClass(BPEL_COPY);
		createEReference(bpelCopyEClass, BPEL_COPY__FROM_SPEC);
		createEReference(bpelCopyEClass, BPEL_COPY__TO_SPEC);

		bpelCopyFromEClass = createEClass(BPEL_COPY_FROM);
		createEAttribute(bpelCopyFromEClass, BPEL_COPY_FROM__VARIABLE);
		createEAttribute(bpelCopyFromEClass, BPEL_COPY_FROM__PART);
		createEAttribute(bpelCopyFromEClass, BPEL_COPY_FROM__PROPERTY);
		createEAttribute(bpelCopyFromEClass, BPEL_COPY_FROM__PARTNER_LINK);
		createEAttribute(bpelCopyFromEClass, BPEL_COPY_FROM__ENDPOINT_REFERENCE);
		createEReference(bpelCopyFromEClass, BPEL_COPY_FROM__EXPRESSION);
		createEReference(bpelCopyFromEClass, BPEL_COPY_FROM__QUERY);
		createEReference(bpelCopyFromEClass, BPEL_COPY_FROM__LITERAL);

		bpelCopyToEClass = createEClass(BPEL_COPY_TO);
		createEAttribute(bpelCopyToEClass, BPEL_COPY_TO__VARIABLE);
		createEAttribute(bpelCopyToEClass, BPEL_COPY_TO__PART);
		createEAttribute(bpelCopyToEClass, BPEL_COPY_TO__PROPERTY);
		createEAttribute(bpelCopyToEClass, BPEL_COPY_TO__PARTNER_LINK);
		createEReference(bpelCopyToEClass, BPEL_COPY_TO__EXPRESSION);
		createEReference(bpelCopyToEClass, BPEL_COPY_TO__QUERY);

		bpelLiteralEClass = createEClass(BPEL_LITERAL);
		createEAttribute(bpelLiteralEClass, BPEL_LITERAL__TYPE);
		createEAttribute(bpelLiteralEClass, BPEL_LITERAL__LITERAL);

		bpelQueryEClass = createEClass(BPEL_QUERY);
		createEAttribute(bpelQueryEClass, BPEL_QUERY__QUERY_LANGUAGE);
		createEAttribute(bpelQueryEClass, BPEL_QUERY__CONTENT);

		bpelThrowEClass = createEClass(BPEL_THROW);
		createEAttribute(bpelThrowEClass, BPEL_THROW__FAULT_NAME);
		createEAttribute(bpelThrowEClass, BPEL_THROW__FAULT_VARIABLE);

		bpelRethrowEClass = createEClass(BPEL_RETHROW);

		bpelWaitEClass = createEClass(BPEL_WAIT);
		createEReference(bpelWaitEClass, BPEL_WAIT__DEADLINE_EXPRESSION);
		createEReference(bpelWaitEClass, BPEL_WAIT__DURATION_EXPRESSION);

		bpelEmptyEClass = createEClass(BPEL_EMPTY);

		bpelExitEClass = createEClass(BPEL_EXIT);

		bpelSequenceEClass = createEClass(BPEL_SEQUENCE);
		createEReference(bpelSequenceEClass, BPEL_SEQUENCE__ACTIVITIES);

		bpelIfEClass = createEClass(BPEL_IF);
		createEReference(bpelIfEClass, BPEL_IF__CONDITION);
		createEReference(bpelIfEClass, BPEL_IF__THEN_ACTIVITY);
		createEReference(bpelIfEClass, BPEL_IF__ELSE_IF_ACTIVITY);
		createEReference(bpelIfEClass, BPEL_IF__ELSE_ACTIVITY);

		bpelElseIfEClass = createEClass(BPEL_ELSE_IF);
		createEReference(bpelElseIfEClass, BPEL_ELSE_IF__CONDITION);
		createEReference(bpelElseIfEClass, BPEL_ELSE_IF__THEN_ACTIVITY);

		bpelWhileEClass = createEClass(BPEL_WHILE);
		createEReference(bpelWhileEClass, BPEL_WHILE__CONDITION);
		createEReference(bpelWhileEClass, BPEL_WHILE__BODY_ACTIVITY);

		bpelRepeatUntilEClass = createEClass(BPEL_REPEAT_UNTIL);
		createEReference(bpelRepeatUntilEClass, BPEL_REPEAT_UNTIL__BODY_ACTIVITY);
		createEReference(bpelRepeatUntilEClass, BPEL_REPEAT_UNTIL__CONDITION);

		bpelPickEClass = createEClass(BPEL_PICK);

		bpelFlowEClass = createEClass(BPEL_FLOW);
		createEReference(bpelFlowEClass, BPEL_FLOW__LINKS);
		createEReference(bpelFlowEClass, BPEL_FLOW__BODY_ACTIVITIES);

		bpelLinkEClass = createEClass(BPEL_LINK);

		bpelForEachEClass = createEClass(BPEL_FOR_EACH);
		createEReference(bpelForEachEClass, BPEL_FOR_EACH__START_COUNTER_VALUE);
		createEReference(bpelForEachEClass, BPEL_FOR_EACH__FINAL_COUNTER_VALUE);
		createEReference(bpelForEachEClass, BPEL_FOR_EACH__COMPLETION_CONDITION);
		createEReference(bpelForEachEClass, BPEL_FOR_EACH__SCOPE);

		bpelScopeEClass = createEClass(BPEL_SCOPE);
		createEReference(bpelScopeEClass, BPEL_SCOPE__COMPENSATION_HANDLER);
		createEReference(bpelScopeEClass, BPEL_SCOPE__TERMINATION_HANDLER);

		vxBPELVariationPointEClass = createEClass(VX_BPEL_VARIATION_POINT);
		createEReference(vxBPELVariationPointEClass, VX_BPEL_VARIATION_POINT__VARIANTS);

		vxBPELVariantEClass = createEClass(VX_BPEL_VARIANT);
		createEReference(vxBPELVariantEClass, VX_BPEL_VARIANT__BPEL_CODE);

		vxBPELConfigurableVariationPointEClass = createEClass(VX_BPEL_CONFIGURABLE_VARIATION_POINT);
		createEAttribute(vxBPELConfigurableVariationPointEClass, VX_BPEL_CONFIGURABLE_VARIATION_POINT__ID);
		createEAttribute(vxBPELConfigurableVariationPointEClass, VX_BPEL_CONFIGURABLE_VARIATION_POINT__DEFAULT_VARIANT);
		createEReference(vxBPELConfigurableVariationPointEClass, VX_BPEL_CONFIGURABLE_VARIATION_POINT__VARIANTS);

		vxBPELConfigurableVariantEClass = createEClass(VX_BPEL_CONFIGURABLE_VARIANT);
		createEAttribute(vxBPELConfigurableVariantEClass, VX_BPEL_CONFIGURABLE_VARIANT__VARIANT_INFO);
		createEAttribute(vxBPELConfigurableVariantEClass, VX_BPEL_CONFIGURABLE_VARIANT__RATIONALE);
		createEReference(vxBPELConfigurableVariantEClass, VX_BPEL_CONFIGURABLE_VARIANT__REQUIRED_VARIANTS);

		vxBPELVariationPointPChoiceEClass = createEClass(VX_BPEL_VARIATION_POINT_PCHOICE);
		createEAttribute(vxBPELVariationPointPChoiceEClass, VX_BPEL_VARIATION_POINT_PCHOICE__VPNAME);
		createEAttribute(vxBPELVariationPointPChoiceEClass, VX_BPEL_VARIATION_POINT_PCHOICE__VARIANT);

		vxBPELProcessEClass = createEClass(VX_BPEL_PROCESS);
		createEReference(vxBPELProcessEClass, VX_BPEL_PROCESS__CONFIGURABLE_VARIATION_POINTS);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		bpelExpressionEClass.getESuperTypes().add(this.getXMLAttributeContainer());
		bpelBoolExpressionEClass.getESuperTypes().add(this.getBPELExpression());
		bpelDeadlineExpressionEClass.getESuperTypes().add(this.getBPELExpression());
		bpelDurationExpressionEClass.getESuperTypes().add(this.getBPELExpression());
		bpelUnsignedIntegerExpressionEClass.getESuperTypes().add(this.getBPELExpression());
		bpelScopeElementEClass.getESuperTypes().add(this.getXMLAttributeContainer());
		bpelProcessEClass.getESuperTypes().add(this.getNamedElement());
		bpelProcessEClass.getESuperTypes().add(this.getBPELScopeElement());
		bpelImportEClass.getESuperTypes().add(this.getXMLAttributeContainer());
		bpelPartnerLinkEClass.getESuperTypes().add(this.getXMLAttributeContainer());
		bpelMessageExchangeEClass.getESuperTypes().add(this.getXMLAttributeContainer());
		bpelVariableEClass.getESuperTypes().add(this.getXMLAttributeContainer());
		bpelCorrelationSetEClass.getESuperTypes().add(this.getXMLAttributeContainer());
		bpelActivityEClass.getESuperTypes().add(this.getNamedElement());
		bpelActivityEClass.getESuperTypes().add(this.getXMLAttributeContainer());
		bpelTargetListEClass.getESuperTypes().add(this.getXMLAttributeContainer());
		bpelTargetEClass.getESuperTypes().add(this.getXMLAttributeContainer());
		bpelSourceListEClass.getESuperTypes().add(this.getXMLAttributeContainer());
		bpelSourceEClass.getESuperTypes().add(this.getXMLAttributeContainer());
		bpelInvokeEClass.getESuperTypes().add(this.getBPELActivity());
		bpelReceiveEClass.getESuperTypes().add(this.getBPELActivity());
		bpelReplyEClass.getESuperTypes().add(this.getBPELActivity());
		bpelAssignEClass.getESuperTypes().add(this.getBPELActivity());
		bpelAssignOperationEClass.getESuperTypes().add(this.getXMLAttributeContainer());
		bpelCopyEClass.getESuperTypes().add(this.getBPELAssignOperation());
		bpelCopyFromEClass.getESuperTypes().add(this.getXMLAttributeContainer());
		bpelCopyToEClass.getESuperTypes().add(this.getXMLAttributeContainer());
		bpelQueryEClass.getESuperTypes().add(this.getXMLAttributeContainer());
		bpelThrowEClass.getESuperTypes().add(this.getBPELActivity());
		bpelRethrowEClass.getESuperTypes().add(this.getBPELActivity());
		bpelWaitEClass.getESuperTypes().add(this.getBPELActivity());
		bpelEmptyEClass.getESuperTypes().add(this.getBPELActivity());
		bpelExitEClass.getESuperTypes().add(this.getBPELActivity());
		bpelSequenceEClass.getESuperTypes().add(this.getBPELActivity());
		bpelIfEClass.getESuperTypes().add(this.getBPELActivity());
		bpelElseIfEClass.getESuperTypes().add(this.getBPELActivity());
		bpelWhileEClass.getESuperTypes().add(this.getBPELActivity());
		bpelRepeatUntilEClass.getESuperTypes().add(this.getBPELActivity());
		bpelPickEClass.getESuperTypes().add(this.getBPELActivity());
		bpelFlowEClass.getESuperTypes().add(this.getBPELActivity());
		bpelLinkEClass.getESuperTypes().add(this.getNamedElement());
		bpelForEachEClass.getESuperTypes().add(this.getBPELActivity());
		bpelScopeEClass.getESuperTypes().add(this.getBPELActivity());
		bpelScopeEClass.getESuperTypes().add(this.getNamedElement());
		bpelScopeEClass.getESuperTypes().add(this.getBPELScopeElement());
		vxBPELVariationPointEClass.getESuperTypes().add(this.getBPELActivity());
		vxBPELVariantEClass.getESuperTypes().add(this.getNamedElement());
		vxBPELConfigurableVariationPointEClass.getESuperTypes().add(this.getXMLAttributeContainer());
		vxBPELConfigurableVariationPointEClass.getESuperTypes().add(this.getNamedElement());
		vxBPELConfigurableVariantEClass.getESuperTypes().add(this.getNamedElement());
		vxBPELProcessEClass.getESuperTypes().add(this.getBPELProcess());

		// Initialize classes, features, and operations; add parameters
		initEClass(namedElementEClass, NamedElement.class, "NamedElement", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getNamedElement_Name(), ecorePackage.getEString(), "name", null, 1, 1, NamedElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(xmlAttributeContainerEClass, XMLAttributeContainer.class, "XMLAttributeContainer", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getXMLAttributeContainer_Attributes(), this.getXMLAttribute(), null, "attributes", null, 0, -1, XMLAttributeContainer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(xmlAttributeEClass, XMLAttribute.class, "XMLAttribute", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getXMLAttribute_Name(), ecorePackage.getEString(), "name", null, 0, 1, XMLAttribute.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getXMLAttribute_Value(), ecorePackage.getEString(), "value", null, 0, 1, XMLAttribute.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(bpelExpressionEClass, BPELExpression.class, "BPELExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getBPELExpression_ExpressionLanguage(), ecorePackage.getEString(), "expressionLanguage", null, 0, 1, BPELExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBPELExpression_Expression(), ecorePackage.getEString(), "expression", null, 1, 1, BPELExpression.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(bpelBoolExpressionEClass, BPELBoolExpression.class, "BPELBoolExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(bpelDeadlineExpressionEClass, BPELDeadlineExpression.class, "BPELDeadlineExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(bpelDurationExpressionEClass, BPELDurationExpression.class, "BPELDurationExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(bpelUnsignedIntegerExpressionEClass, BPELUnsignedIntegerExpression.class, "BPELUnsignedIntegerExpression", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(bpelScopeElementEClass, BPELScopeElement.class, "BPELScopeElement", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getBPELScopeElement_ExitOnStandardFault(), ecorePackage.getEString(), "exitOnStandardFault", null, 0, 1, BPELScopeElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBPELScopeElement_PartnerLinks(), this.getBPELPartnerLink(), null, "partnerLinks", null, 0, -1, BPELScopeElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBPELScopeElement_MessageExchanges(), this.getBPELMessageExchange(), null, "messageExchanges", null, 0, -1, BPELScopeElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBPELScopeElement_Variables(), this.getBPELVariable(), null, "variables", null, 0, -1, BPELScopeElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBPELScopeElement_CorrelationSets(), this.getBPELCorrelationSet(), null, "correlationSets", null, 0, -1, BPELScopeElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBPELScopeElement_FaultHandlerList(), ecorePackage.getEObject(), null, "faultHandlerList", null, 0, -1, BPELScopeElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBPELScopeElement_EventHandlerList(), ecorePackage.getEObject(), null, "eventHandlerList", null, 0, -1, BPELScopeElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBPELScopeElement_Activity(), this.getBPELActivity(), null, "activity", null, 1, 1, BPELScopeElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(bpelProcessEClass, BPELProcess.class, "BPELProcess", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getBPELProcess_TargetNamespace(), ecorePackage.getEString(), "targetNamespace", "urn:oasis:names:tc:wsbpel:2.0:sublang:xpath1.0", 1, 1, BPELProcess.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBPELProcess_QueryLanguage(), ecorePackage.getEString(), "queryLanguage", "urn:oasis:names:tc:wsbpel:2.0:sublang:xpath1.0", 0, 1, BPELProcess.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBPELProcess_ExpressionLanguage(), ecorePackage.getEString(), "expressionLanguage", "urn:oasis:names:tc:wsbpel:2.0:sublang:xpath1.0", 0, 1, BPELProcess.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBPELProcess_Imports(), this.getBPELImport(), null, "imports", null, 0, -1, BPELProcess.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(bpelImportEClass, BPELImport.class, "BPELImport", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getBPELImport_Namespace(), ecorePackage.getEString(), "namespace", null, 0, 1, BPELImport.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBPELImport_Location(), ecorePackage.getEString(), "location", null, 0, 1, BPELImport.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBPELImport_ImportType(), ecorePackage.getEString(), "importType", null, 1, 1, BPELImport.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(bpelPartnerLinkEClass, BPELPartnerLink.class, "BPELPartnerLink", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getBPELPartnerLink_Name(), ecorePackage.getEString(), "name", null, 1, 1, BPELPartnerLink.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBPELPartnerLink_PartnerLinkType(), ecorePackage.getEString(), "partnerLinkType", null, 1, 1, BPELPartnerLink.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBPELPartnerLink_MyRole(), ecorePackage.getEString(), "myRole", null, 0, 1, BPELPartnerLink.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBPELPartnerLink_PartnerRole(), ecorePackage.getEString(), "partnerRole", null, 0, 1, BPELPartnerLink.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBPELPartnerLink_InitializePartnerRole(), ecorePackage.getEString(), "initializePartnerRole", null, 0, 1, BPELPartnerLink.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(bpelMessageExchangeEClass, BPELMessageExchange.class, "BPELMessageExchange", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getBPELMessageExchange_Name(), ecorePackage.getEString(), "name", null, 1, 1, BPELMessageExchange.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(bpelVariableEClass, BPELVariable.class, "BPELVariable", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getBPELVariable_Name(), ecorePackage.getEString(), "name", null, 1, 1, BPELVariable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBPELVariable_MessageType(), ecorePackage.getEString(), "messageType", null, 0, 1, BPELVariable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBPELVariable_Type(), ecorePackage.getEString(), "type", null, 0, 1, BPELVariable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBPELVariable_Element(), ecorePackage.getEString(), "element", null, 0, 1, BPELVariable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBPELVariable_FromSpec(), ecorePackage.getEString(), "fromSpec", null, 0, 1, BPELVariable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(bpelCorrelationSetEClass, BPELCorrelationSet.class, "BPELCorrelationSet", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getBPELCorrelationSet_Name(), ecorePackage.getEString(), "name", null, 1, 1, BPELCorrelationSet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBPELCorrelationSet_Properties(), ecorePackage.getEString(), "properties", null, 1, 1, BPELCorrelationSet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(bpelActivityEClass, BPELActivity.class, "BPELActivity", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBPELActivity_TargetSet(), this.getBPELTargetList(), null, "targetSet", null, 0, 1, BPELActivity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBPELActivity_SourceSet(), this.getBPELSourceList(), null, "sourceSet", null, 0, 1, BPELActivity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(bpelTargetListEClass, BPELTargetList.class, "BPELTargetList", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBPELTargetList_JoinCondition(), this.getBPELBoolExpression(), null, "joinCondition", null, 0, 1, BPELTargetList.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBPELTargetList_Targets(), this.getBPELTarget(), null, "targets", null, 1, -1, BPELTargetList.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(bpelTargetEClass, BPELTarget.class, "BPELTarget", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getBPELTarget_LinkName(), ecorePackage.getEString(), "linkName", null, 1, 1, BPELTarget.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(bpelSourceListEClass, BPELSourceList.class, "BPELSourceList", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBPELSourceList_Sources(), this.getBPELSource(), null, "sources", null, 1, -1, BPELSourceList.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(bpelSourceEClass, BPELSource.class, "BPELSource", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getBPELSource_LinkName(), ecorePackage.getEString(), "linkName", null, 1, 1, BPELSource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBPELSource_TransitionCondition(), this.getBPELBoolExpression(), null, "transitionCondition", null, 0, 1, BPELSource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(bpelInvokeEClass, BPELInvoke.class, "BPELInvoke", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getBPELInvoke_PartnerLink(), ecorePackage.getEString(), "partnerLink", null, 1, 1, BPELInvoke.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBPELInvoke_PortType(), ecorePackage.getEString(), "portType", null, 0, 1, BPELInvoke.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBPELInvoke_Operation(), ecorePackage.getEString(), "operation", null, 1, 1, BPELInvoke.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBPELInvoke_InputVariable(), ecorePackage.getEString(), "inputVariable", null, 0, 1, BPELInvoke.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBPELInvoke_OutputVariable(), ecorePackage.getEString(), "outputVariable", null, 0, 1, BPELInvoke.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(bpelReceiveEClass, BPELReceive.class, "BPELReceive", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getBPELReceive_PartnerLink(), ecorePackage.getEString(), "partnerLink", null, 1, 1, BPELReceive.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBPELReceive_PortType(), ecorePackage.getEString(), "portType", null, 0, 1, BPELReceive.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBPELReceive_Operation(), ecorePackage.getEString(), "operation", null, 1, 1, BPELReceive.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBPELReceive_Variable(), ecorePackage.getEString(), "variable", null, 0, 1, BPELReceive.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBPELReceive_CreateInstance(), ecorePackage.getEString(), "createInstance", null, 0, 1, BPELReceive.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBPELReceive_MessageExchange(), ecorePackage.getEString(), "messageExchange", null, 0, 1, BPELReceive.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(bpelReplyEClass, BPELReply.class, "BPELReply", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getBPELReply_PartnerLink(), ecorePackage.getEString(), "partnerLink", null, 1, 1, BPELReply.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBPELReply_PortType(), ecorePackage.getEString(), "portType", null, 0, 1, BPELReply.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBPELReply_Operation(), ecorePackage.getEString(), "operation", null, 1, 1, BPELReply.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBPELReply_Variable(), ecorePackage.getEString(), "variable", null, 0, 1, BPELReply.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBPELReply_FaultName(), ecorePackage.getEString(), "faultName", null, 0, 1, BPELReply.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBPELReply_MessageExchange(), ecorePackage.getEString(), "messageExchange", null, 0, 1, BPELReply.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(bpelAssignEClass, BPELAssign.class, "BPELAssign", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getBPELAssign_Validate(), ecorePackage.getEString(), "validate", null, 0, 1, BPELAssign.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBPELAssign_Parts(), this.getBPELAssignOperation(), null, "parts", null, 1, -1, BPELAssign.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(bpelAssignOperationEClass, BPELAssignOperation.class, "BPELAssignOperation", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(bpelCopyEClass, BPELCopy.class, "BPELCopy", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBPELCopy_FromSpec(), this.getBPELCopyFrom(), null, "fromSpec", null, 1, 1, BPELCopy.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBPELCopy_ToSpec(), this.getBPELCopyTo(), null, "toSpec", null, 1, 1, BPELCopy.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(bpelCopyFromEClass, BPELCopyFrom.class, "BPELCopyFrom", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getBPELCopyFrom_Variable(), ecorePackage.getEString(), "variable", null, 0, 1, BPELCopyFrom.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBPELCopyFrom_Part(), ecorePackage.getEString(), "part", null, 0, 1, BPELCopyFrom.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBPELCopyFrom_Property(), ecorePackage.getEString(), "property", null, 0, 1, BPELCopyFrom.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBPELCopyFrom_PartnerLink(), ecorePackage.getEString(), "partnerLink", null, 0, 1, BPELCopyFrom.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBPELCopyFrom_EndpointReference(), ecorePackage.getEString(), "endpointReference", null, 0, 1, BPELCopyFrom.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBPELCopyFrom_Expression(), this.getBPELExpression(), null, "expression", null, 0, 1, BPELCopyFrom.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBPELCopyFrom_Query(), this.getBPELQuery(), null, "query", null, 0, 1, BPELCopyFrom.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBPELCopyFrom_Literal(), this.getBPELLiteral(), null, "literal", null, 0, 1, BPELCopyFrom.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(bpelCopyToEClass, BPELCopyTo.class, "BPELCopyTo", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getBPELCopyTo_Variable(), ecorePackage.getEString(), "variable", null, 0, 1, BPELCopyTo.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBPELCopyTo_Part(), ecorePackage.getEString(), "part", null, 0, 1, BPELCopyTo.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBPELCopyTo_Property(), ecorePackage.getEString(), "property", null, 0, 1, BPELCopyTo.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBPELCopyTo_PartnerLink(), ecorePackage.getEString(), "partnerLink", null, 0, 1, BPELCopyTo.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBPELCopyTo_Expression(), this.getBPELExpression(), null, "expression", null, 0, 1, BPELCopyTo.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBPELCopyTo_Query(), this.getBPELQuery(), null, "query", null, 0, 1, BPELCopyTo.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(bpelLiteralEClass, BPELLiteral.class, "BPELLiteral", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getBPELLiteral_Type(), ecorePackage.getEString(), "type", null, 0, 1, BPELLiteral.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBPELLiteral_Literal(), ecorePackage.getEString(), "literal", null, 0, 1, BPELLiteral.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(bpelQueryEClass, BPELQuery.class, "BPELQuery", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getBPELQuery_QueryLanguage(), ecorePackage.getEString(), "queryLanguage", null, 0, 1, BPELQuery.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBPELQuery_Content(), ecorePackage.getEString(), "content", null, 1, 1, BPELQuery.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(bpelThrowEClass, BPELThrow.class, "BPELThrow", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getBPELThrow_FaultName(), ecorePackage.getEString(), "faultName", null, 1, 1, BPELThrow.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBPELThrow_FaultVariable(), ecorePackage.getEString(), "faultVariable", null, 0, 1, BPELThrow.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(bpelRethrowEClass, BPELRethrow.class, "BPELRethrow", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(bpelWaitEClass, BPELWait.class, "BPELWait", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBPELWait_DeadlineExpression(), this.getBPELDeadlineExpression(), null, "deadlineExpression", null, 0, 1, BPELWait.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBPELWait_DurationExpression(), this.getBPELDurationExpression(), null, "durationExpression", null, 0, 1, BPELWait.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(bpelEmptyEClass, BPELEmpty.class, "BPELEmpty", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(bpelExitEClass, BPELExit.class, "BPELExit", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(bpelSequenceEClass, BPELSequence.class, "BPELSequence", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBPELSequence_Activities(), this.getBPELActivity(), null, "activities", null, 1, -1, BPELSequence.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(bpelIfEClass, BPELIf.class, "BPELIf", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBPELIf_Condition(), this.getBPELBoolExpression(), null, "condition", null, 1, 1, BPELIf.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBPELIf_ThenActivity(), this.getBPELActivity(), null, "thenActivity", null, 1, 1, BPELIf.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBPELIf_ElseIfActivity(), this.getBPELElseIf(), null, "elseIfActivity", null, 0, -1, BPELIf.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBPELIf_ElseActivity(), this.getBPELActivity(), null, "elseActivity", null, 0, 1, BPELIf.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(bpelElseIfEClass, BPELElseIf.class, "BPELElseIf", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBPELElseIf_Condition(), this.getBPELBoolExpression(), null, "condition", null, 1, 1, BPELElseIf.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBPELElseIf_ThenActivity(), this.getBPELActivity(), null, "thenActivity", null, 1, 1, BPELElseIf.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(bpelWhileEClass, BPELWhile.class, "BPELWhile", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBPELWhile_Condition(), this.getBPELBoolExpression(), null, "condition", null, 1, 1, BPELWhile.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBPELWhile_BodyActivity(), this.getBPELActivity(), null, "bodyActivity", null, 1, 1, BPELWhile.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(bpelRepeatUntilEClass, BPELRepeatUntil.class, "BPELRepeatUntil", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBPELRepeatUntil_BodyActivity(), this.getBPELActivity(), null, "bodyActivity", null, 1, 1, BPELRepeatUntil.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBPELRepeatUntil_Condition(), this.getBPELBoolExpression(), null, "condition", null, 1, 1, BPELRepeatUntil.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(bpelPickEClass, BPELPick.class, "BPELPick", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(bpelFlowEClass, BPELFlow.class, "BPELFlow", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBPELFlow_Links(), this.getBPELLink(), null, "links", null, 0, -1, BPELFlow.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBPELFlow_BodyActivities(), this.getBPELActivity(), null, "bodyActivities", null, 0, -1, BPELFlow.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(bpelLinkEClass, BPELLink.class, "BPELLink", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(bpelForEachEClass, BPELForEach.class, "BPELForEach", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBPELForEach_StartCounterValue(), this.getBPELUnsignedIntegerExpression(), null, "startCounterValue", null, 1, 1, BPELForEach.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBPELForEach_FinalCounterValue(), this.getBPELUnsignedIntegerExpression(), null, "finalCounterValue", null, 1, 1, BPELForEach.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBPELForEach_CompletionCondition(), this.getBPELUnsignedIntegerExpression(), null, "completionCondition", null, 0, 1, BPELForEach.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBPELForEach_Scope(), this.getBPELScope(), null, "scope", null, 1, 1, BPELForEach.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(bpelScopeEClass, BPELScope.class, "BPELScope", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBPELScope_CompensationHandler(), ecorePackage.getEObject(), null, "compensationHandler", null, 0, 1, BPELScope.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBPELScope_TerminationHandler(), ecorePackage.getEObject(), null, "terminationHandler", null, 0, 1, BPELScope.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(vxBPELVariationPointEClass, VxBPELVariationPoint.class, "VxBPELVariationPoint", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getVxBPELVariationPoint_Variants(), this.getVxBPELVariant(), null, "variants", null, 0, -1, VxBPELVariationPoint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(vxBPELVariantEClass, VxBPELVariant.class, "VxBPELVariant", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getVxBPELVariant_BpelCode(), this.getBPELActivity(), null, "bpelCode", null, 1, 1, VxBPELVariant.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(vxBPELConfigurableVariationPointEClass, VxBPELConfigurableVariationPoint.class, "VxBPELConfigurableVariationPoint", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getVxBPELConfigurableVariationPoint_Id(), ecorePackage.getEString(), "id", null, 0, 1, VxBPELConfigurableVariationPoint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getVxBPELConfigurableVariationPoint_DefaultVariant(), ecorePackage.getEString(), "defaultVariant", null, 0, 1, VxBPELConfigurableVariationPoint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getVxBPELConfigurableVariationPoint_Variants(), this.getVxBPELConfigurableVariant(), null, "variants", null, 0, -1, VxBPELConfigurableVariationPoint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(vxBPELConfigurableVariantEClass, VxBPELConfigurableVariant.class, "VxBPELConfigurableVariant", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getVxBPELConfigurableVariant_VariantInfo(), ecorePackage.getEString(), "variantInfo", null, 0, 1, VxBPELConfigurableVariant.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getVxBPELConfigurableVariant_Rationale(), ecorePackage.getEString(), "rationale", null, 0, 1, VxBPELConfigurableVariant.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getVxBPELConfigurableVariant_RequiredVariants(), this.getVxBPELVariationPointPChoice(), null, "requiredVariants", null, 0, -1, VxBPELConfigurableVariant.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(vxBPELVariationPointPChoiceEClass, VxBPELVariationPointPChoice.class, "VxBPELVariationPointPChoice", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getVxBPELVariationPointPChoice_Vpname(), ecorePackage.getEString(), "vpname", null, 0, 1, VxBPELVariationPointPChoice.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getVxBPELVariationPointPChoice_Variant(), ecorePackage.getEString(), "variant", null, 0, 1, VxBPELVariationPointPChoice.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(vxBPELProcessEClass, VxBPELProcess.class, "VxBPELProcess", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getVxBPELProcess_ConfigurableVariationPoints(), this.getVxBPELConfigurableVariationPoint(), null, "configurableVariationPoints", null, 0, -1, VxBPELProcess.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //VxBPELPackageImpl
