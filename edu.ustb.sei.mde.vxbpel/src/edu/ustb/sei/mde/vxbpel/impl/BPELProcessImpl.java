/**
 */
package edu.ustb.sei.mde.vxbpel.impl;

import edu.ustb.sei.mde.vxbpel.BPELActivity;
import edu.ustb.sei.mde.vxbpel.BPELCorrelationSet;
import edu.ustb.sei.mde.vxbpel.BPELImport;
import edu.ustb.sei.mde.vxbpel.BPELMessageExchange;
import edu.ustb.sei.mde.vxbpel.BPELPartnerLink;
import edu.ustb.sei.mde.vxbpel.BPELProcess;
import edu.ustb.sei.mde.vxbpel.BPELScopeElement;
import edu.ustb.sei.mde.vxbpel.BPELVariable;
import edu.ustb.sei.mde.vxbpel.VxBPELPackage;
import edu.ustb.sei.mde.vxbpel.XMLAttribute;
import edu.ustb.sei.mde.vxbpel.XMLAttributeContainer;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>BPEL Process</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.impl.BPELProcessImpl#getAttributes <em>Attributes</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.impl.BPELProcessImpl#getExitOnStandardFault <em>Exit On Standard Fault</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.impl.BPELProcessImpl#getPartnerLinks <em>Partner Links</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.impl.BPELProcessImpl#getMessageExchanges <em>Message Exchanges</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.impl.BPELProcessImpl#getVariables <em>Variables</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.impl.BPELProcessImpl#getCorrelationSets <em>Correlation Sets</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.impl.BPELProcessImpl#getFaultHandlerList <em>Fault Handler List</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.impl.BPELProcessImpl#getEventHandlerList <em>Event Handler List</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.impl.BPELProcessImpl#getActivity <em>Activity</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.impl.BPELProcessImpl#getTargetNamespace <em>Target Namespace</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.impl.BPELProcessImpl#getQueryLanguage <em>Query Language</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.impl.BPELProcessImpl#getExpressionLanguage <em>Expression Language</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.impl.BPELProcessImpl#getImports <em>Imports</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class BPELProcessImpl extends NamedElementImpl implements BPELProcess {
	/**
	 * The cached value of the '{@link #getAttributes() <em>Attributes</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAttributes()
	 * @generated
	 * @ordered
	 */
	protected EList<XMLAttribute> attributes;

	/**
	 * The default value of the '{@link #getExitOnStandardFault() <em>Exit On Standard Fault</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExitOnStandardFault()
	 * @generated
	 * @ordered
	 */
	protected static final String EXIT_ON_STANDARD_FAULT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getExitOnStandardFault() <em>Exit On Standard Fault</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExitOnStandardFault()
	 * @generated
	 * @ordered
	 */
	protected String exitOnStandardFault = EXIT_ON_STANDARD_FAULT_EDEFAULT;

	/**
	 * The cached value of the '{@link #getPartnerLinks() <em>Partner Links</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPartnerLinks()
	 * @generated
	 * @ordered
	 */
	protected EList<BPELPartnerLink> partnerLinks;

	/**
	 * The cached value of the '{@link #getMessageExchanges() <em>Message Exchanges</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMessageExchanges()
	 * @generated
	 * @ordered
	 */
	protected EList<BPELMessageExchange> messageExchanges;

	/**
	 * The cached value of the '{@link #getVariables() <em>Variables</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVariables()
	 * @generated
	 * @ordered
	 */
	protected EList<BPELVariable> variables;

	/**
	 * The cached value of the '{@link #getCorrelationSets() <em>Correlation Sets</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCorrelationSets()
	 * @generated
	 * @ordered
	 */
	protected EList<BPELCorrelationSet> correlationSets;

	/**
	 * The cached value of the '{@link #getFaultHandlerList() <em>Fault Handler List</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFaultHandlerList()
	 * @generated
	 * @ordered
	 */
	protected EList<EObject> faultHandlerList;

	/**
	 * The cached value of the '{@link #getEventHandlerList() <em>Event Handler List</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEventHandlerList()
	 * @generated
	 * @ordered
	 */
	protected EList<EObject> eventHandlerList;

	/**
	 * The cached value of the '{@link #getActivity() <em>Activity</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActivity()
	 * @generated
	 * @ordered
	 */
	protected BPELActivity activity;

	/**
	 * The default value of the '{@link #getTargetNamespace() <em>Target Namespace</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTargetNamespace()
	 * @generated
	 * @ordered
	 */
	protected static final String TARGET_NAMESPACE_EDEFAULT = "urn:oasis:names:tc:wsbpel:2.0:sublang:xpath1.0";

	/**
	 * The cached value of the '{@link #getTargetNamespace() <em>Target Namespace</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTargetNamespace()
	 * @generated
	 * @ordered
	 */
	protected String targetNamespace = TARGET_NAMESPACE_EDEFAULT;

	/**
	 * The default value of the '{@link #getQueryLanguage() <em>Query Language</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQueryLanguage()
	 * @generated
	 * @ordered
	 */
	protected static final String QUERY_LANGUAGE_EDEFAULT = "urn:oasis:names:tc:wsbpel:2.0:sublang:xpath1.0";

	/**
	 * The cached value of the '{@link #getQueryLanguage() <em>Query Language</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getQueryLanguage()
	 * @generated
	 * @ordered
	 */
	protected String queryLanguage = QUERY_LANGUAGE_EDEFAULT;

	/**
	 * The default value of the '{@link #getExpressionLanguage() <em>Expression Language</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExpressionLanguage()
	 * @generated
	 * @ordered
	 */
	protected static final String EXPRESSION_LANGUAGE_EDEFAULT = "urn:oasis:names:tc:wsbpel:2.0:sublang:xpath1.0";

	/**
	 * The cached value of the '{@link #getExpressionLanguage() <em>Expression Language</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExpressionLanguage()
	 * @generated
	 * @ordered
	 */
	protected String expressionLanguage = EXPRESSION_LANGUAGE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getImports() <em>Imports</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getImports()
	 * @generated
	 * @ordered
	 */
	protected EList<BPELImport> imports;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BPELProcessImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return VxBPELPackage.Literals.BPEL_PROCESS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<XMLAttribute> getAttributes() {
		if (attributes == null) {
			attributes = new EObjectContainmentEList<XMLAttribute>(XMLAttribute.class, this, VxBPELPackage.BPEL_PROCESS__ATTRIBUTES);
		}
		return attributes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getExitOnStandardFault() {
		return exitOnStandardFault;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExitOnStandardFault(String newExitOnStandardFault) {
		String oldExitOnStandardFault = exitOnStandardFault;
		exitOnStandardFault = newExitOnStandardFault;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VxBPELPackage.BPEL_PROCESS__EXIT_ON_STANDARD_FAULT, oldExitOnStandardFault, exitOnStandardFault));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<BPELPartnerLink> getPartnerLinks() {
		if (partnerLinks == null) {
			partnerLinks = new EObjectContainmentEList<BPELPartnerLink>(BPELPartnerLink.class, this, VxBPELPackage.BPEL_PROCESS__PARTNER_LINKS);
		}
		return partnerLinks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<BPELMessageExchange> getMessageExchanges() {
		if (messageExchanges == null) {
			messageExchanges = new EObjectContainmentEList<BPELMessageExchange>(BPELMessageExchange.class, this, VxBPELPackage.BPEL_PROCESS__MESSAGE_EXCHANGES);
		}
		return messageExchanges;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<BPELVariable> getVariables() {
		if (variables == null) {
			variables = new EObjectContainmentEList<BPELVariable>(BPELVariable.class, this, VxBPELPackage.BPEL_PROCESS__VARIABLES);
		}
		return variables;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<BPELCorrelationSet> getCorrelationSets() {
		if (correlationSets == null) {
			correlationSets = new EObjectContainmentEList<BPELCorrelationSet>(BPELCorrelationSet.class, this, VxBPELPackage.BPEL_PROCESS__CORRELATION_SETS);
		}
		return correlationSets;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EObject> getFaultHandlerList() {
		if (faultHandlerList == null) {
			faultHandlerList = new EObjectContainmentEList<EObject>(EObject.class, this, VxBPELPackage.BPEL_PROCESS__FAULT_HANDLER_LIST);
		}
		return faultHandlerList;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EObject> getEventHandlerList() {
		if (eventHandlerList == null) {
			eventHandlerList = new EObjectContainmentEList<EObject>(EObject.class, this, VxBPELPackage.BPEL_PROCESS__EVENT_HANDLER_LIST);
		}
		return eventHandlerList;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BPELActivity getActivity() {
		return activity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetActivity(BPELActivity newActivity, NotificationChain msgs) {
		BPELActivity oldActivity = activity;
		activity = newActivity;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, VxBPELPackage.BPEL_PROCESS__ACTIVITY, oldActivity, newActivity);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setActivity(BPELActivity newActivity) {
		if (newActivity != activity) {
			NotificationChain msgs = null;
			if (activity != null)
				msgs = ((InternalEObject)activity).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - VxBPELPackage.BPEL_PROCESS__ACTIVITY, null, msgs);
			if (newActivity != null)
				msgs = ((InternalEObject)newActivity).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - VxBPELPackage.BPEL_PROCESS__ACTIVITY, null, msgs);
			msgs = basicSetActivity(newActivity, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VxBPELPackage.BPEL_PROCESS__ACTIVITY, newActivity, newActivity));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getTargetNamespace() {
		return targetNamespace;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTargetNamespace(String newTargetNamespace) {
		String oldTargetNamespace = targetNamespace;
		targetNamespace = newTargetNamespace;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VxBPELPackage.BPEL_PROCESS__TARGET_NAMESPACE, oldTargetNamespace, targetNamespace));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getQueryLanguage() {
		return queryLanguage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setQueryLanguage(String newQueryLanguage) {
		String oldQueryLanguage = queryLanguage;
		queryLanguage = newQueryLanguage;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VxBPELPackage.BPEL_PROCESS__QUERY_LANGUAGE, oldQueryLanguage, queryLanguage));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getExpressionLanguage() {
		return expressionLanguage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExpressionLanguage(String newExpressionLanguage) {
		String oldExpressionLanguage = expressionLanguage;
		expressionLanguage = newExpressionLanguage;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VxBPELPackage.BPEL_PROCESS__EXPRESSION_LANGUAGE, oldExpressionLanguage, expressionLanguage));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<BPELImport> getImports() {
		if (imports == null) {
			imports = new EObjectContainmentEList<BPELImport>(BPELImport.class, this, VxBPELPackage.BPEL_PROCESS__IMPORTS);
		}
		return imports;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case VxBPELPackage.BPEL_PROCESS__ATTRIBUTES:
				return ((InternalEList<?>)getAttributes()).basicRemove(otherEnd, msgs);
			case VxBPELPackage.BPEL_PROCESS__PARTNER_LINKS:
				return ((InternalEList<?>)getPartnerLinks()).basicRemove(otherEnd, msgs);
			case VxBPELPackage.BPEL_PROCESS__MESSAGE_EXCHANGES:
				return ((InternalEList<?>)getMessageExchanges()).basicRemove(otherEnd, msgs);
			case VxBPELPackage.BPEL_PROCESS__VARIABLES:
				return ((InternalEList<?>)getVariables()).basicRemove(otherEnd, msgs);
			case VxBPELPackage.BPEL_PROCESS__CORRELATION_SETS:
				return ((InternalEList<?>)getCorrelationSets()).basicRemove(otherEnd, msgs);
			case VxBPELPackage.BPEL_PROCESS__FAULT_HANDLER_LIST:
				return ((InternalEList<?>)getFaultHandlerList()).basicRemove(otherEnd, msgs);
			case VxBPELPackage.BPEL_PROCESS__EVENT_HANDLER_LIST:
				return ((InternalEList<?>)getEventHandlerList()).basicRemove(otherEnd, msgs);
			case VxBPELPackage.BPEL_PROCESS__ACTIVITY:
				return basicSetActivity(null, msgs);
			case VxBPELPackage.BPEL_PROCESS__IMPORTS:
				return ((InternalEList<?>)getImports()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case VxBPELPackage.BPEL_PROCESS__ATTRIBUTES:
				return getAttributes();
			case VxBPELPackage.BPEL_PROCESS__EXIT_ON_STANDARD_FAULT:
				return getExitOnStandardFault();
			case VxBPELPackage.BPEL_PROCESS__PARTNER_LINKS:
				return getPartnerLinks();
			case VxBPELPackage.BPEL_PROCESS__MESSAGE_EXCHANGES:
				return getMessageExchanges();
			case VxBPELPackage.BPEL_PROCESS__VARIABLES:
				return getVariables();
			case VxBPELPackage.BPEL_PROCESS__CORRELATION_SETS:
				return getCorrelationSets();
			case VxBPELPackage.BPEL_PROCESS__FAULT_HANDLER_LIST:
				return getFaultHandlerList();
			case VxBPELPackage.BPEL_PROCESS__EVENT_HANDLER_LIST:
				return getEventHandlerList();
			case VxBPELPackage.BPEL_PROCESS__ACTIVITY:
				return getActivity();
			case VxBPELPackage.BPEL_PROCESS__TARGET_NAMESPACE:
				return getTargetNamespace();
			case VxBPELPackage.BPEL_PROCESS__QUERY_LANGUAGE:
				return getQueryLanguage();
			case VxBPELPackage.BPEL_PROCESS__EXPRESSION_LANGUAGE:
				return getExpressionLanguage();
			case VxBPELPackage.BPEL_PROCESS__IMPORTS:
				return getImports();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case VxBPELPackage.BPEL_PROCESS__ATTRIBUTES:
				getAttributes().clear();
				getAttributes().addAll((Collection<? extends XMLAttribute>)newValue);
				return;
			case VxBPELPackage.BPEL_PROCESS__EXIT_ON_STANDARD_FAULT:
				setExitOnStandardFault((String)newValue);
				return;
			case VxBPELPackage.BPEL_PROCESS__PARTNER_LINKS:
				getPartnerLinks().clear();
				getPartnerLinks().addAll((Collection<? extends BPELPartnerLink>)newValue);
				return;
			case VxBPELPackage.BPEL_PROCESS__MESSAGE_EXCHANGES:
				getMessageExchanges().clear();
				getMessageExchanges().addAll((Collection<? extends BPELMessageExchange>)newValue);
				return;
			case VxBPELPackage.BPEL_PROCESS__VARIABLES:
				getVariables().clear();
				getVariables().addAll((Collection<? extends BPELVariable>)newValue);
				return;
			case VxBPELPackage.BPEL_PROCESS__CORRELATION_SETS:
				getCorrelationSets().clear();
				getCorrelationSets().addAll((Collection<? extends BPELCorrelationSet>)newValue);
				return;
			case VxBPELPackage.BPEL_PROCESS__FAULT_HANDLER_LIST:
				getFaultHandlerList().clear();
				getFaultHandlerList().addAll((Collection<? extends EObject>)newValue);
				return;
			case VxBPELPackage.BPEL_PROCESS__EVENT_HANDLER_LIST:
				getEventHandlerList().clear();
				getEventHandlerList().addAll((Collection<? extends EObject>)newValue);
				return;
			case VxBPELPackage.BPEL_PROCESS__ACTIVITY:
				setActivity((BPELActivity)newValue);
				return;
			case VxBPELPackage.BPEL_PROCESS__TARGET_NAMESPACE:
				setTargetNamespace((String)newValue);
				return;
			case VxBPELPackage.BPEL_PROCESS__QUERY_LANGUAGE:
				setQueryLanguage((String)newValue);
				return;
			case VxBPELPackage.BPEL_PROCESS__EXPRESSION_LANGUAGE:
				setExpressionLanguage((String)newValue);
				return;
			case VxBPELPackage.BPEL_PROCESS__IMPORTS:
				getImports().clear();
				getImports().addAll((Collection<? extends BPELImport>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case VxBPELPackage.BPEL_PROCESS__ATTRIBUTES:
				getAttributes().clear();
				return;
			case VxBPELPackage.BPEL_PROCESS__EXIT_ON_STANDARD_FAULT:
				setExitOnStandardFault(EXIT_ON_STANDARD_FAULT_EDEFAULT);
				return;
			case VxBPELPackage.BPEL_PROCESS__PARTNER_LINKS:
				getPartnerLinks().clear();
				return;
			case VxBPELPackage.BPEL_PROCESS__MESSAGE_EXCHANGES:
				getMessageExchanges().clear();
				return;
			case VxBPELPackage.BPEL_PROCESS__VARIABLES:
				getVariables().clear();
				return;
			case VxBPELPackage.BPEL_PROCESS__CORRELATION_SETS:
				getCorrelationSets().clear();
				return;
			case VxBPELPackage.BPEL_PROCESS__FAULT_HANDLER_LIST:
				getFaultHandlerList().clear();
				return;
			case VxBPELPackage.BPEL_PROCESS__EVENT_HANDLER_LIST:
				getEventHandlerList().clear();
				return;
			case VxBPELPackage.BPEL_PROCESS__ACTIVITY:
				setActivity((BPELActivity)null);
				return;
			case VxBPELPackage.BPEL_PROCESS__TARGET_NAMESPACE:
				setTargetNamespace(TARGET_NAMESPACE_EDEFAULT);
				return;
			case VxBPELPackage.BPEL_PROCESS__QUERY_LANGUAGE:
				setQueryLanguage(QUERY_LANGUAGE_EDEFAULT);
				return;
			case VxBPELPackage.BPEL_PROCESS__EXPRESSION_LANGUAGE:
				setExpressionLanguage(EXPRESSION_LANGUAGE_EDEFAULT);
				return;
			case VxBPELPackage.BPEL_PROCESS__IMPORTS:
				getImports().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case VxBPELPackage.BPEL_PROCESS__ATTRIBUTES:
				return attributes != null && !attributes.isEmpty();
			case VxBPELPackage.BPEL_PROCESS__EXIT_ON_STANDARD_FAULT:
				return EXIT_ON_STANDARD_FAULT_EDEFAULT == null ? exitOnStandardFault != null : !EXIT_ON_STANDARD_FAULT_EDEFAULT.equals(exitOnStandardFault);
			case VxBPELPackage.BPEL_PROCESS__PARTNER_LINKS:
				return partnerLinks != null && !partnerLinks.isEmpty();
			case VxBPELPackage.BPEL_PROCESS__MESSAGE_EXCHANGES:
				return messageExchanges != null && !messageExchanges.isEmpty();
			case VxBPELPackage.BPEL_PROCESS__VARIABLES:
				return variables != null && !variables.isEmpty();
			case VxBPELPackage.BPEL_PROCESS__CORRELATION_SETS:
				return correlationSets != null && !correlationSets.isEmpty();
			case VxBPELPackage.BPEL_PROCESS__FAULT_HANDLER_LIST:
				return faultHandlerList != null && !faultHandlerList.isEmpty();
			case VxBPELPackage.BPEL_PROCESS__EVENT_HANDLER_LIST:
				return eventHandlerList != null && !eventHandlerList.isEmpty();
			case VxBPELPackage.BPEL_PROCESS__ACTIVITY:
				return activity != null;
			case VxBPELPackage.BPEL_PROCESS__TARGET_NAMESPACE:
				return TARGET_NAMESPACE_EDEFAULT == null ? targetNamespace != null : !TARGET_NAMESPACE_EDEFAULT.equals(targetNamespace);
			case VxBPELPackage.BPEL_PROCESS__QUERY_LANGUAGE:
				return QUERY_LANGUAGE_EDEFAULT == null ? queryLanguage != null : !QUERY_LANGUAGE_EDEFAULT.equals(queryLanguage);
			case VxBPELPackage.BPEL_PROCESS__EXPRESSION_LANGUAGE:
				return EXPRESSION_LANGUAGE_EDEFAULT == null ? expressionLanguage != null : !EXPRESSION_LANGUAGE_EDEFAULT.equals(expressionLanguage);
			case VxBPELPackage.BPEL_PROCESS__IMPORTS:
				return imports != null && !imports.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == XMLAttributeContainer.class) {
			switch (derivedFeatureID) {
				case VxBPELPackage.BPEL_PROCESS__ATTRIBUTES: return VxBPELPackage.XML_ATTRIBUTE_CONTAINER__ATTRIBUTES;
				default: return -1;
			}
		}
		if (baseClass == BPELScopeElement.class) {
			switch (derivedFeatureID) {
				case VxBPELPackage.BPEL_PROCESS__EXIT_ON_STANDARD_FAULT: return VxBPELPackage.BPEL_SCOPE_ELEMENT__EXIT_ON_STANDARD_FAULT;
				case VxBPELPackage.BPEL_PROCESS__PARTNER_LINKS: return VxBPELPackage.BPEL_SCOPE_ELEMENT__PARTNER_LINKS;
				case VxBPELPackage.BPEL_PROCESS__MESSAGE_EXCHANGES: return VxBPELPackage.BPEL_SCOPE_ELEMENT__MESSAGE_EXCHANGES;
				case VxBPELPackage.BPEL_PROCESS__VARIABLES: return VxBPELPackage.BPEL_SCOPE_ELEMENT__VARIABLES;
				case VxBPELPackage.BPEL_PROCESS__CORRELATION_SETS: return VxBPELPackage.BPEL_SCOPE_ELEMENT__CORRELATION_SETS;
				case VxBPELPackage.BPEL_PROCESS__FAULT_HANDLER_LIST: return VxBPELPackage.BPEL_SCOPE_ELEMENT__FAULT_HANDLER_LIST;
				case VxBPELPackage.BPEL_PROCESS__EVENT_HANDLER_LIST: return VxBPELPackage.BPEL_SCOPE_ELEMENT__EVENT_HANDLER_LIST;
				case VxBPELPackage.BPEL_PROCESS__ACTIVITY: return VxBPELPackage.BPEL_SCOPE_ELEMENT__ACTIVITY;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == XMLAttributeContainer.class) {
			switch (baseFeatureID) {
				case VxBPELPackage.XML_ATTRIBUTE_CONTAINER__ATTRIBUTES: return VxBPELPackage.BPEL_PROCESS__ATTRIBUTES;
				default: return -1;
			}
		}
		if (baseClass == BPELScopeElement.class) {
			switch (baseFeatureID) {
				case VxBPELPackage.BPEL_SCOPE_ELEMENT__EXIT_ON_STANDARD_FAULT: return VxBPELPackage.BPEL_PROCESS__EXIT_ON_STANDARD_FAULT;
				case VxBPELPackage.BPEL_SCOPE_ELEMENT__PARTNER_LINKS: return VxBPELPackage.BPEL_PROCESS__PARTNER_LINKS;
				case VxBPELPackage.BPEL_SCOPE_ELEMENT__MESSAGE_EXCHANGES: return VxBPELPackage.BPEL_PROCESS__MESSAGE_EXCHANGES;
				case VxBPELPackage.BPEL_SCOPE_ELEMENT__VARIABLES: return VxBPELPackage.BPEL_PROCESS__VARIABLES;
				case VxBPELPackage.BPEL_SCOPE_ELEMENT__CORRELATION_SETS: return VxBPELPackage.BPEL_PROCESS__CORRELATION_SETS;
				case VxBPELPackage.BPEL_SCOPE_ELEMENT__FAULT_HANDLER_LIST: return VxBPELPackage.BPEL_PROCESS__FAULT_HANDLER_LIST;
				case VxBPELPackage.BPEL_SCOPE_ELEMENT__EVENT_HANDLER_LIST: return VxBPELPackage.BPEL_PROCESS__EVENT_HANDLER_LIST;
				case VxBPELPackage.BPEL_SCOPE_ELEMENT__ACTIVITY: return VxBPELPackage.BPEL_PROCESS__ACTIVITY;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (exitOnStandardFault: ");
		result.append(exitOnStandardFault);
		result.append(", targetNamespace: ");
		result.append(targetNamespace);
		result.append(", queryLanguage: ");
		result.append(queryLanguage);
		result.append(", expressionLanguage: ");
		result.append(expressionLanguage);
		result.append(')');
		return result.toString();
	}

} //BPELProcessImpl
