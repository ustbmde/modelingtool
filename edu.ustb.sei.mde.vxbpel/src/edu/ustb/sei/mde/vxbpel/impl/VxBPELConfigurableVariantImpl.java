/**
 */
package edu.ustb.sei.mde.vxbpel.impl;

import edu.ustb.sei.mde.vxbpel.VxBPELConfigurableVariant;
import edu.ustb.sei.mde.vxbpel.VxBPELPackage;
import edu.ustb.sei.mde.vxbpel.VxBPELVariationPointPChoice;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Configurable Variant</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.impl.VxBPELConfigurableVariantImpl#getVariantInfo <em>Variant Info</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.impl.VxBPELConfigurableVariantImpl#getRationale <em>Rationale</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.impl.VxBPELConfigurableVariantImpl#getRequiredVariants <em>Required Variants</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class VxBPELConfigurableVariantImpl extends NamedElementImpl implements VxBPELConfigurableVariant {
	/**
	 * The default value of the '{@link #getVariantInfo() <em>Variant Info</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVariantInfo()
	 * @generated
	 * @ordered
	 */
	protected static final String VARIANT_INFO_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getVariantInfo() <em>Variant Info</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVariantInfo()
	 * @generated
	 * @ordered
	 */
	protected String variantInfo = VARIANT_INFO_EDEFAULT;

	/**
	 * The default value of the '{@link #getRationale() <em>Rationale</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRationale()
	 * @generated
	 * @ordered
	 */
	protected static final String RATIONALE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getRationale() <em>Rationale</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRationale()
	 * @generated
	 * @ordered
	 */
	protected String rationale = RATIONALE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getRequiredVariants() <em>Required Variants</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRequiredVariants()
	 * @generated
	 * @ordered
	 */
	protected EList<VxBPELVariationPointPChoice> requiredVariants;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VxBPELConfigurableVariantImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return VxBPELPackage.Literals.VX_BPEL_CONFIGURABLE_VARIANT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getVariantInfo() {
		return variantInfo;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVariantInfo(String newVariantInfo) {
		String oldVariantInfo = variantInfo;
		variantInfo = newVariantInfo;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VxBPELPackage.VX_BPEL_CONFIGURABLE_VARIANT__VARIANT_INFO, oldVariantInfo, variantInfo));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getRationale() {
		return rationale;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRationale(String newRationale) {
		String oldRationale = rationale;
		rationale = newRationale;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VxBPELPackage.VX_BPEL_CONFIGURABLE_VARIANT__RATIONALE, oldRationale, rationale));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<VxBPELVariationPointPChoice> getRequiredVariants() {
		if (requiredVariants == null) {
			requiredVariants = new EObjectContainmentEList<VxBPELVariationPointPChoice>(VxBPELVariationPointPChoice.class, this, VxBPELPackage.VX_BPEL_CONFIGURABLE_VARIANT__REQUIRED_VARIANTS);
		}
		return requiredVariants;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case VxBPELPackage.VX_BPEL_CONFIGURABLE_VARIANT__REQUIRED_VARIANTS:
				return ((InternalEList<?>)getRequiredVariants()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case VxBPELPackage.VX_BPEL_CONFIGURABLE_VARIANT__VARIANT_INFO:
				return getVariantInfo();
			case VxBPELPackage.VX_BPEL_CONFIGURABLE_VARIANT__RATIONALE:
				return getRationale();
			case VxBPELPackage.VX_BPEL_CONFIGURABLE_VARIANT__REQUIRED_VARIANTS:
				return getRequiredVariants();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case VxBPELPackage.VX_BPEL_CONFIGURABLE_VARIANT__VARIANT_INFO:
				setVariantInfo((String)newValue);
				return;
			case VxBPELPackage.VX_BPEL_CONFIGURABLE_VARIANT__RATIONALE:
				setRationale((String)newValue);
				return;
			case VxBPELPackage.VX_BPEL_CONFIGURABLE_VARIANT__REQUIRED_VARIANTS:
				getRequiredVariants().clear();
				getRequiredVariants().addAll((Collection<? extends VxBPELVariationPointPChoice>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case VxBPELPackage.VX_BPEL_CONFIGURABLE_VARIANT__VARIANT_INFO:
				setVariantInfo(VARIANT_INFO_EDEFAULT);
				return;
			case VxBPELPackage.VX_BPEL_CONFIGURABLE_VARIANT__RATIONALE:
				setRationale(RATIONALE_EDEFAULT);
				return;
			case VxBPELPackage.VX_BPEL_CONFIGURABLE_VARIANT__REQUIRED_VARIANTS:
				getRequiredVariants().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case VxBPELPackage.VX_BPEL_CONFIGURABLE_VARIANT__VARIANT_INFO:
				return VARIANT_INFO_EDEFAULT == null ? variantInfo != null : !VARIANT_INFO_EDEFAULT.equals(variantInfo);
			case VxBPELPackage.VX_BPEL_CONFIGURABLE_VARIANT__RATIONALE:
				return RATIONALE_EDEFAULT == null ? rationale != null : !RATIONALE_EDEFAULT.equals(rationale);
			case VxBPELPackage.VX_BPEL_CONFIGURABLE_VARIANT__REQUIRED_VARIANTS:
				return requiredVariants != null && !requiredVariants.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (variantInfo: ");
		result.append(variantInfo);
		result.append(", rationale: ");
		result.append(rationale);
		result.append(')');
		return result.toString();
	}

} //VxBPELConfigurableVariantImpl
