/**
 */
package edu.ustb.sei.mde.vxbpel.impl;

import edu.ustb.sei.mde.vxbpel.BPELLink;
import edu.ustb.sei.mde.vxbpel.VxBPELPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>BPEL Link</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class BPELLinkImpl extends NamedElementImpl implements BPELLink {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BPELLinkImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return VxBPELPackage.Literals.BPEL_LINK;
	}

} //BPELLinkImpl
