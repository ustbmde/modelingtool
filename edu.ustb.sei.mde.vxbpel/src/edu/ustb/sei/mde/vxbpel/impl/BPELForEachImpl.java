/**
 */
package edu.ustb.sei.mde.vxbpel.impl;

import edu.ustb.sei.mde.vxbpel.BPELForEach;
import edu.ustb.sei.mde.vxbpel.BPELScope;
import edu.ustb.sei.mde.vxbpel.BPELUnsignedIntegerExpression;
import edu.ustb.sei.mde.vxbpel.VxBPELPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>BPEL For Each</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.impl.BPELForEachImpl#getStartCounterValue <em>Start Counter Value</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.impl.BPELForEachImpl#getFinalCounterValue <em>Final Counter Value</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.impl.BPELForEachImpl#getCompletionCondition <em>Completion Condition</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.impl.BPELForEachImpl#getScope <em>Scope</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class BPELForEachImpl extends BPELActivityImpl implements BPELForEach {
	/**
	 * The cached value of the '{@link #getStartCounterValue() <em>Start Counter Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStartCounterValue()
	 * @generated
	 * @ordered
	 */
	protected BPELUnsignedIntegerExpression startCounterValue;

	/**
	 * The cached value of the '{@link #getFinalCounterValue() <em>Final Counter Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFinalCounterValue()
	 * @generated
	 * @ordered
	 */
	protected BPELUnsignedIntegerExpression finalCounterValue;

	/**
	 * The cached value of the '{@link #getCompletionCondition() <em>Completion Condition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCompletionCondition()
	 * @generated
	 * @ordered
	 */
	protected BPELUnsignedIntegerExpression completionCondition;

	/**
	 * The cached value of the '{@link #getScope() <em>Scope</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScope()
	 * @generated
	 * @ordered
	 */
	protected BPELScope scope;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BPELForEachImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return VxBPELPackage.Literals.BPEL_FOR_EACH;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BPELUnsignedIntegerExpression getStartCounterValue() {
		return startCounterValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetStartCounterValue(BPELUnsignedIntegerExpression newStartCounterValue, NotificationChain msgs) {
		BPELUnsignedIntegerExpression oldStartCounterValue = startCounterValue;
		startCounterValue = newStartCounterValue;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, VxBPELPackage.BPEL_FOR_EACH__START_COUNTER_VALUE, oldStartCounterValue, newStartCounterValue);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStartCounterValue(BPELUnsignedIntegerExpression newStartCounterValue) {
		if (newStartCounterValue != startCounterValue) {
			NotificationChain msgs = null;
			if (startCounterValue != null)
				msgs = ((InternalEObject)startCounterValue).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - VxBPELPackage.BPEL_FOR_EACH__START_COUNTER_VALUE, null, msgs);
			if (newStartCounterValue != null)
				msgs = ((InternalEObject)newStartCounterValue).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - VxBPELPackage.BPEL_FOR_EACH__START_COUNTER_VALUE, null, msgs);
			msgs = basicSetStartCounterValue(newStartCounterValue, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VxBPELPackage.BPEL_FOR_EACH__START_COUNTER_VALUE, newStartCounterValue, newStartCounterValue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BPELUnsignedIntegerExpression getFinalCounterValue() {
		return finalCounterValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFinalCounterValue(BPELUnsignedIntegerExpression newFinalCounterValue, NotificationChain msgs) {
		BPELUnsignedIntegerExpression oldFinalCounterValue = finalCounterValue;
		finalCounterValue = newFinalCounterValue;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, VxBPELPackage.BPEL_FOR_EACH__FINAL_COUNTER_VALUE, oldFinalCounterValue, newFinalCounterValue);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFinalCounterValue(BPELUnsignedIntegerExpression newFinalCounterValue) {
		if (newFinalCounterValue != finalCounterValue) {
			NotificationChain msgs = null;
			if (finalCounterValue != null)
				msgs = ((InternalEObject)finalCounterValue).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - VxBPELPackage.BPEL_FOR_EACH__FINAL_COUNTER_VALUE, null, msgs);
			if (newFinalCounterValue != null)
				msgs = ((InternalEObject)newFinalCounterValue).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - VxBPELPackage.BPEL_FOR_EACH__FINAL_COUNTER_VALUE, null, msgs);
			msgs = basicSetFinalCounterValue(newFinalCounterValue, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VxBPELPackage.BPEL_FOR_EACH__FINAL_COUNTER_VALUE, newFinalCounterValue, newFinalCounterValue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BPELUnsignedIntegerExpression getCompletionCondition() {
		return completionCondition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCompletionCondition(BPELUnsignedIntegerExpression newCompletionCondition, NotificationChain msgs) {
		BPELUnsignedIntegerExpression oldCompletionCondition = completionCondition;
		completionCondition = newCompletionCondition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, VxBPELPackage.BPEL_FOR_EACH__COMPLETION_CONDITION, oldCompletionCondition, newCompletionCondition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCompletionCondition(BPELUnsignedIntegerExpression newCompletionCondition) {
		if (newCompletionCondition != completionCondition) {
			NotificationChain msgs = null;
			if (completionCondition != null)
				msgs = ((InternalEObject)completionCondition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - VxBPELPackage.BPEL_FOR_EACH__COMPLETION_CONDITION, null, msgs);
			if (newCompletionCondition != null)
				msgs = ((InternalEObject)newCompletionCondition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - VxBPELPackage.BPEL_FOR_EACH__COMPLETION_CONDITION, null, msgs);
			msgs = basicSetCompletionCondition(newCompletionCondition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VxBPELPackage.BPEL_FOR_EACH__COMPLETION_CONDITION, newCompletionCondition, newCompletionCondition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BPELScope getScope() {
		return scope;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetScope(BPELScope newScope, NotificationChain msgs) {
		BPELScope oldScope = scope;
		scope = newScope;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, VxBPELPackage.BPEL_FOR_EACH__SCOPE, oldScope, newScope);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setScope(BPELScope newScope) {
		if (newScope != scope) {
			NotificationChain msgs = null;
			if (scope != null)
				msgs = ((InternalEObject)scope).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - VxBPELPackage.BPEL_FOR_EACH__SCOPE, null, msgs);
			if (newScope != null)
				msgs = ((InternalEObject)newScope).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - VxBPELPackage.BPEL_FOR_EACH__SCOPE, null, msgs);
			msgs = basicSetScope(newScope, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VxBPELPackage.BPEL_FOR_EACH__SCOPE, newScope, newScope));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case VxBPELPackage.BPEL_FOR_EACH__START_COUNTER_VALUE:
				return basicSetStartCounterValue(null, msgs);
			case VxBPELPackage.BPEL_FOR_EACH__FINAL_COUNTER_VALUE:
				return basicSetFinalCounterValue(null, msgs);
			case VxBPELPackage.BPEL_FOR_EACH__COMPLETION_CONDITION:
				return basicSetCompletionCondition(null, msgs);
			case VxBPELPackage.BPEL_FOR_EACH__SCOPE:
				return basicSetScope(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case VxBPELPackage.BPEL_FOR_EACH__START_COUNTER_VALUE:
				return getStartCounterValue();
			case VxBPELPackage.BPEL_FOR_EACH__FINAL_COUNTER_VALUE:
				return getFinalCounterValue();
			case VxBPELPackage.BPEL_FOR_EACH__COMPLETION_CONDITION:
				return getCompletionCondition();
			case VxBPELPackage.BPEL_FOR_EACH__SCOPE:
				return getScope();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case VxBPELPackage.BPEL_FOR_EACH__START_COUNTER_VALUE:
				setStartCounterValue((BPELUnsignedIntegerExpression)newValue);
				return;
			case VxBPELPackage.BPEL_FOR_EACH__FINAL_COUNTER_VALUE:
				setFinalCounterValue((BPELUnsignedIntegerExpression)newValue);
				return;
			case VxBPELPackage.BPEL_FOR_EACH__COMPLETION_CONDITION:
				setCompletionCondition((BPELUnsignedIntegerExpression)newValue);
				return;
			case VxBPELPackage.BPEL_FOR_EACH__SCOPE:
				setScope((BPELScope)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case VxBPELPackage.BPEL_FOR_EACH__START_COUNTER_VALUE:
				setStartCounterValue((BPELUnsignedIntegerExpression)null);
				return;
			case VxBPELPackage.BPEL_FOR_EACH__FINAL_COUNTER_VALUE:
				setFinalCounterValue((BPELUnsignedIntegerExpression)null);
				return;
			case VxBPELPackage.BPEL_FOR_EACH__COMPLETION_CONDITION:
				setCompletionCondition((BPELUnsignedIntegerExpression)null);
				return;
			case VxBPELPackage.BPEL_FOR_EACH__SCOPE:
				setScope((BPELScope)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case VxBPELPackage.BPEL_FOR_EACH__START_COUNTER_VALUE:
				return startCounterValue != null;
			case VxBPELPackage.BPEL_FOR_EACH__FINAL_COUNTER_VALUE:
				return finalCounterValue != null;
			case VxBPELPackage.BPEL_FOR_EACH__COMPLETION_CONDITION:
				return completionCondition != null;
			case VxBPELPackage.BPEL_FOR_EACH__SCOPE:
				return scope != null;
		}
		return super.eIsSet(featureID);
	}

} //BPELForEachImpl
