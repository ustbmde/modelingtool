/**
 */
package edu.ustb.sei.mde.vxbpel.impl;

import edu.ustb.sei.mde.vxbpel.NamedElement;
import edu.ustb.sei.mde.vxbpel.VxBPELConfigurableVariant;
import edu.ustb.sei.mde.vxbpel.VxBPELConfigurableVariationPoint;
import edu.ustb.sei.mde.vxbpel.VxBPELPackage;

import java.util.Collection;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Configurable Variation Point</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.impl.VxBPELConfigurableVariationPointImpl#getName <em>Name</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.impl.VxBPELConfigurableVariationPointImpl#getId <em>Id</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.impl.VxBPELConfigurableVariationPointImpl#getDefaultVariant <em>Default Variant</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.impl.VxBPELConfigurableVariationPointImpl#getVariants <em>Variants</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class VxBPELConfigurableVariationPointImpl extends XMLAttributeContainerImpl implements VxBPELConfigurableVariationPoint {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected String id = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getDefaultVariant() <em>Default Variant</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDefaultVariant()
	 * @generated
	 * @ordered
	 */
	protected static final String DEFAULT_VARIANT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDefaultVariant() <em>Default Variant</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDefaultVariant()
	 * @generated
	 * @ordered
	 */
	protected String defaultVariant = DEFAULT_VARIANT_EDEFAULT;

	/**
	 * The cached value of the '{@link #getVariants() <em>Variants</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVariants()
	 * @generated
	 * @ordered
	 */
	protected EList<VxBPELConfigurableVariant> variants;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VxBPELConfigurableVariationPointImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return VxBPELPackage.Literals.VX_BPEL_CONFIGURABLE_VARIATION_POINT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VxBPELPackage.VX_BPEL_CONFIGURABLE_VARIATION_POINT__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setId(String newId) {
		String oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VxBPELPackage.VX_BPEL_CONFIGURABLE_VARIATION_POINT__ID, oldId, id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDefaultVariant() {
		return defaultVariant;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDefaultVariant(String newDefaultVariant) {
		String oldDefaultVariant = defaultVariant;
		defaultVariant = newDefaultVariant;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VxBPELPackage.VX_BPEL_CONFIGURABLE_VARIATION_POINT__DEFAULT_VARIANT, oldDefaultVariant, defaultVariant));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<VxBPELConfigurableVariant> getVariants() {
		if (variants == null) {
			variants = new EObjectContainmentEList<VxBPELConfigurableVariant>(VxBPELConfigurableVariant.class, this, VxBPELPackage.VX_BPEL_CONFIGURABLE_VARIATION_POINT__VARIANTS);
		}
		return variants;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case VxBPELPackage.VX_BPEL_CONFIGURABLE_VARIATION_POINT__VARIANTS:
				return ((InternalEList<?>)getVariants()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case VxBPELPackage.VX_BPEL_CONFIGURABLE_VARIATION_POINT__NAME:
				return getName();
			case VxBPELPackage.VX_BPEL_CONFIGURABLE_VARIATION_POINT__ID:
				return getId();
			case VxBPELPackage.VX_BPEL_CONFIGURABLE_VARIATION_POINT__DEFAULT_VARIANT:
				return getDefaultVariant();
			case VxBPELPackage.VX_BPEL_CONFIGURABLE_VARIATION_POINT__VARIANTS:
				return getVariants();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case VxBPELPackage.VX_BPEL_CONFIGURABLE_VARIATION_POINT__NAME:
				setName((String)newValue);
				return;
			case VxBPELPackage.VX_BPEL_CONFIGURABLE_VARIATION_POINT__ID:
				setId((String)newValue);
				return;
			case VxBPELPackage.VX_BPEL_CONFIGURABLE_VARIATION_POINT__DEFAULT_VARIANT:
				setDefaultVariant((String)newValue);
				return;
			case VxBPELPackage.VX_BPEL_CONFIGURABLE_VARIATION_POINT__VARIANTS:
				getVariants().clear();
				getVariants().addAll((Collection<? extends VxBPELConfigurableVariant>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case VxBPELPackage.VX_BPEL_CONFIGURABLE_VARIATION_POINT__NAME:
				setName(NAME_EDEFAULT);
				return;
			case VxBPELPackage.VX_BPEL_CONFIGURABLE_VARIATION_POINT__ID:
				setId(ID_EDEFAULT);
				return;
			case VxBPELPackage.VX_BPEL_CONFIGURABLE_VARIATION_POINT__DEFAULT_VARIANT:
				setDefaultVariant(DEFAULT_VARIANT_EDEFAULT);
				return;
			case VxBPELPackage.VX_BPEL_CONFIGURABLE_VARIATION_POINT__VARIANTS:
				getVariants().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case VxBPELPackage.VX_BPEL_CONFIGURABLE_VARIATION_POINT__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case VxBPELPackage.VX_BPEL_CONFIGURABLE_VARIATION_POINT__ID:
				return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
			case VxBPELPackage.VX_BPEL_CONFIGURABLE_VARIATION_POINT__DEFAULT_VARIANT:
				return DEFAULT_VARIANT_EDEFAULT == null ? defaultVariant != null : !DEFAULT_VARIANT_EDEFAULT.equals(defaultVariant);
			case VxBPELPackage.VX_BPEL_CONFIGURABLE_VARIATION_POINT__VARIANTS:
				return variants != null && !variants.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == NamedElement.class) {
			switch (derivedFeatureID) {
				case VxBPELPackage.VX_BPEL_CONFIGURABLE_VARIATION_POINT__NAME: return VxBPELPackage.NAMED_ELEMENT__NAME;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == NamedElement.class) {
			switch (baseFeatureID) {
				case VxBPELPackage.NAMED_ELEMENT__NAME: return VxBPELPackage.VX_BPEL_CONFIGURABLE_VARIATION_POINT__NAME;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", id: ");
		result.append(id);
		result.append(", defaultVariant: ");
		result.append(defaultVariant);
		result.append(')');
		return result.toString();
	}

} //VxBPELConfigurableVariationPointImpl
