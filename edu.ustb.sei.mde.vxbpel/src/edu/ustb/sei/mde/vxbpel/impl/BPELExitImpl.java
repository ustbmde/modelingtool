/**
 */
package edu.ustb.sei.mde.vxbpel.impl;

import edu.ustb.sei.mde.vxbpel.BPELExit;
import edu.ustb.sei.mde.vxbpel.VxBPELPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>BPEL Exit</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class BPELExitImpl extends BPELActivityImpl implements BPELExit {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BPELExitImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return VxBPELPackage.Literals.BPEL_EXIT;
	}

} //BPELExitImpl
