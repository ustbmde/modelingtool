/**
 */
package edu.ustb.sei.mde.vxbpel.impl;

import edu.ustb.sei.mde.vxbpel.VxBPELConfigurableVariationPoint;
import edu.ustb.sei.mde.vxbpel.VxBPELPackage;
import edu.ustb.sei.mde.vxbpel.VxBPELProcess;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Process</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.impl.VxBPELProcessImpl#getConfigurableVariationPoints <em>Configurable Variation Points</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class VxBPELProcessImpl extends BPELProcessImpl implements VxBPELProcess {
	/**
	 * The cached value of the '{@link #getConfigurableVariationPoints() <em>Configurable Variation Points</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConfigurableVariationPoints()
	 * @generated
	 * @ordered
	 */
	protected EList<VxBPELConfigurableVariationPoint> configurableVariationPoints;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VxBPELProcessImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return VxBPELPackage.Literals.VX_BPEL_PROCESS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<VxBPELConfigurableVariationPoint> getConfigurableVariationPoints() {
		if (configurableVariationPoints == null) {
			configurableVariationPoints = new EObjectContainmentEList<VxBPELConfigurableVariationPoint>(VxBPELConfigurableVariationPoint.class, this, VxBPELPackage.VX_BPEL_PROCESS__CONFIGURABLE_VARIATION_POINTS);
		}
		return configurableVariationPoints;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case VxBPELPackage.VX_BPEL_PROCESS__CONFIGURABLE_VARIATION_POINTS:
				return ((InternalEList<?>)getConfigurableVariationPoints()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case VxBPELPackage.VX_BPEL_PROCESS__CONFIGURABLE_VARIATION_POINTS:
				return getConfigurableVariationPoints();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case VxBPELPackage.VX_BPEL_PROCESS__CONFIGURABLE_VARIATION_POINTS:
				getConfigurableVariationPoints().clear();
				getConfigurableVariationPoints().addAll((Collection<? extends VxBPELConfigurableVariationPoint>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case VxBPELPackage.VX_BPEL_PROCESS__CONFIGURABLE_VARIATION_POINTS:
				getConfigurableVariationPoints().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case VxBPELPackage.VX_BPEL_PROCESS__CONFIGURABLE_VARIATION_POINTS:
				return configurableVariationPoints != null && !configurableVariationPoints.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //VxBPELProcessImpl
