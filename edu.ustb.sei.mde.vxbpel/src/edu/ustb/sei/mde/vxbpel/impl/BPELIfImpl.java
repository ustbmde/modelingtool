/**
 */
package edu.ustb.sei.mde.vxbpel.impl;

import edu.ustb.sei.mde.vxbpel.BPELActivity;
import edu.ustb.sei.mde.vxbpel.BPELBoolExpression;
import edu.ustb.sei.mde.vxbpel.BPELElseIf;
import edu.ustb.sei.mde.vxbpel.BPELIf;
import edu.ustb.sei.mde.vxbpel.VxBPELPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>BPEL If</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.impl.BPELIfImpl#getCondition <em>Condition</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.impl.BPELIfImpl#getThenActivity <em>Then Activity</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.impl.BPELIfImpl#getElseIfActivity <em>Else If Activity</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.impl.BPELIfImpl#getElseActivity <em>Else Activity</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class BPELIfImpl extends BPELActivityImpl implements BPELIf {
	/**
	 * The cached value of the '{@link #getCondition() <em>Condition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCondition()
	 * @generated
	 * @ordered
	 */
	protected BPELBoolExpression condition;

	/**
	 * The cached value of the '{@link #getThenActivity() <em>Then Activity</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getThenActivity()
	 * @generated
	 * @ordered
	 */
	protected BPELActivity thenActivity;

	/**
	 * The cached value of the '{@link #getElseIfActivity() <em>Else If Activity</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElseIfActivity()
	 * @generated
	 * @ordered
	 */
	protected EList<BPELElseIf> elseIfActivity;

	/**
	 * The cached value of the '{@link #getElseActivity() <em>Else Activity</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElseActivity()
	 * @generated
	 * @ordered
	 */
	protected BPELActivity elseActivity;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BPELIfImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return VxBPELPackage.Literals.BPEL_IF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BPELBoolExpression getCondition() {
		return condition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCondition(BPELBoolExpression newCondition, NotificationChain msgs) {
		BPELBoolExpression oldCondition = condition;
		condition = newCondition;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, VxBPELPackage.BPEL_IF__CONDITION, oldCondition, newCondition);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCondition(BPELBoolExpression newCondition) {
		if (newCondition != condition) {
			NotificationChain msgs = null;
			if (condition != null)
				msgs = ((InternalEObject)condition).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - VxBPELPackage.BPEL_IF__CONDITION, null, msgs);
			if (newCondition != null)
				msgs = ((InternalEObject)newCondition).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - VxBPELPackage.BPEL_IF__CONDITION, null, msgs);
			msgs = basicSetCondition(newCondition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VxBPELPackage.BPEL_IF__CONDITION, newCondition, newCondition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BPELActivity getThenActivity() {
		return thenActivity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetThenActivity(BPELActivity newThenActivity, NotificationChain msgs) {
		BPELActivity oldThenActivity = thenActivity;
		thenActivity = newThenActivity;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, VxBPELPackage.BPEL_IF__THEN_ACTIVITY, oldThenActivity, newThenActivity);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setThenActivity(BPELActivity newThenActivity) {
		if (newThenActivity != thenActivity) {
			NotificationChain msgs = null;
			if (thenActivity != null)
				msgs = ((InternalEObject)thenActivity).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - VxBPELPackage.BPEL_IF__THEN_ACTIVITY, null, msgs);
			if (newThenActivity != null)
				msgs = ((InternalEObject)newThenActivity).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - VxBPELPackage.BPEL_IF__THEN_ACTIVITY, null, msgs);
			msgs = basicSetThenActivity(newThenActivity, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VxBPELPackage.BPEL_IF__THEN_ACTIVITY, newThenActivity, newThenActivity));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<BPELElseIf> getElseIfActivity() {
		if (elseIfActivity == null) {
			elseIfActivity = new EObjectContainmentEList<BPELElseIf>(BPELElseIf.class, this, VxBPELPackage.BPEL_IF__ELSE_IF_ACTIVITY);
		}
		return elseIfActivity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BPELActivity getElseActivity() {
		return elseActivity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetElseActivity(BPELActivity newElseActivity, NotificationChain msgs) {
		BPELActivity oldElseActivity = elseActivity;
		elseActivity = newElseActivity;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, VxBPELPackage.BPEL_IF__ELSE_ACTIVITY, oldElseActivity, newElseActivity);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setElseActivity(BPELActivity newElseActivity) {
		if (newElseActivity != elseActivity) {
			NotificationChain msgs = null;
			if (elseActivity != null)
				msgs = ((InternalEObject)elseActivity).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - VxBPELPackage.BPEL_IF__ELSE_ACTIVITY, null, msgs);
			if (newElseActivity != null)
				msgs = ((InternalEObject)newElseActivity).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - VxBPELPackage.BPEL_IF__ELSE_ACTIVITY, null, msgs);
			msgs = basicSetElseActivity(newElseActivity, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VxBPELPackage.BPEL_IF__ELSE_ACTIVITY, newElseActivity, newElseActivity));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case VxBPELPackage.BPEL_IF__CONDITION:
				return basicSetCondition(null, msgs);
			case VxBPELPackage.BPEL_IF__THEN_ACTIVITY:
				return basicSetThenActivity(null, msgs);
			case VxBPELPackage.BPEL_IF__ELSE_IF_ACTIVITY:
				return ((InternalEList<?>)getElseIfActivity()).basicRemove(otherEnd, msgs);
			case VxBPELPackage.BPEL_IF__ELSE_ACTIVITY:
				return basicSetElseActivity(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case VxBPELPackage.BPEL_IF__CONDITION:
				return getCondition();
			case VxBPELPackage.BPEL_IF__THEN_ACTIVITY:
				return getThenActivity();
			case VxBPELPackage.BPEL_IF__ELSE_IF_ACTIVITY:
				return getElseIfActivity();
			case VxBPELPackage.BPEL_IF__ELSE_ACTIVITY:
				return getElseActivity();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case VxBPELPackage.BPEL_IF__CONDITION:
				setCondition((BPELBoolExpression)newValue);
				return;
			case VxBPELPackage.BPEL_IF__THEN_ACTIVITY:
				setThenActivity((BPELActivity)newValue);
				return;
			case VxBPELPackage.BPEL_IF__ELSE_IF_ACTIVITY:
				getElseIfActivity().clear();
				getElseIfActivity().addAll((Collection<? extends BPELElseIf>)newValue);
				return;
			case VxBPELPackage.BPEL_IF__ELSE_ACTIVITY:
				setElseActivity((BPELActivity)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case VxBPELPackage.BPEL_IF__CONDITION:
				setCondition((BPELBoolExpression)null);
				return;
			case VxBPELPackage.BPEL_IF__THEN_ACTIVITY:
				setThenActivity((BPELActivity)null);
				return;
			case VxBPELPackage.BPEL_IF__ELSE_IF_ACTIVITY:
				getElseIfActivity().clear();
				return;
			case VxBPELPackage.BPEL_IF__ELSE_ACTIVITY:
				setElseActivity((BPELActivity)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case VxBPELPackage.BPEL_IF__CONDITION:
				return condition != null;
			case VxBPELPackage.BPEL_IF__THEN_ACTIVITY:
				return thenActivity != null;
			case VxBPELPackage.BPEL_IF__ELSE_IF_ACTIVITY:
				return elseIfActivity != null && !elseIfActivity.isEmpty();
			case VxBPELPackage.BPEL_IF__ELSE_ACTIVITY:
				return elseActivity != null;
		}
		return super.eIsSet(featureID);
	}

} //BPELIfImpl
