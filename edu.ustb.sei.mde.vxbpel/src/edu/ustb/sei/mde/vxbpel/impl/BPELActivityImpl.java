/**
 */
package edu.ustb.sei.mde.vxbpel.impl;

import edu.ustb.sei.mde.vxbpel.BPELActivity;
import edu.ustb.sei.mde.vxbpel.BPELSourceList;
import edu.ustb.sei.mde.vxbpel.BPELTargetList;
import edu.ustb.sei.mde.vxbpel.VxBPELPackage;
import edu.ustb.sei.mde.vxbpel.XMLAttribute;
import edu.ustb.sei.mde.vxbpel.XMLAttributeContainer;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>BPEL Activity</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.impl.BPELActivityImpl#getAttributes <em>Attributes</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.impl.BPELActivityImpl#getTargetSet <em>Target Set</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.impl.BPELActivityImpl#getSourceSet <em>Source Set</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public abstract class BPELActivityImpl extends NamedElementImpl implements BPELActivity {
	/**
	 * The cached value of the '{@link #getAttributes() <em>Attributes</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAttributes()
	 * @generated
	 * @ordered
	 */
	protected EList<XMLAttribute> attributes;

	/**
	 * The cached value of the '{@link #getTargetSet() <em>Target Set</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTargetSet()
	 * @generated
	 * @ordered
	 */
	protected BPELTargetList targetSet;

	/**
	 * The cached value of the '{@link #getSourceSet() <em>Source Set</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSourceSet()
	 * @generated
	 * @ordered
	 */
	protected BPELSourceList sourceSet;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BPELActivityImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return VxBPELPackage.Literals.BPEL_ACTIVITY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<XMLAttribute> getAttributes() {
		if (attributes == null) {
			attributes = new EObjectContainmentEList<XMLAttribute>(XMLAttribute.class, this, VxBPELPackage.BPEL_ACTIVITY__ATTRIBUTES);
		}
		return attributes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BPELTargetList getTargetSet() {
		return targetSet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTargetSet(BPELTargetList newTargetSet, NotificationChain msgs) {
		BPELTargetList oldTargetSet = targetSet;
		targetSet = newTargetSet;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, VxBPELPackage.BPEL_ACTIVITY__TARGET_SET, oldTargetSet, newTargetSet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTargetSet(BPELTargetList newTargetSet) {
		if (newTargetSet != targetSet) {
			NotificationChain msgs = null;
			if (targetSet != null)
				msgs = ((InternalEObject)targetSet).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - VxBPELPackage.BPEL_ACTIVITY__TARGET_SET, null, msgs);
			if (newTargetSet != null)
				msgs = ((InternalEObject)newTargetSet).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - VxBPELPackage.BPEL_ACTIVITY__TARGET_SET, null, msgs);
			msgs = basicSetTargetSet(newTargetSet, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VxBPELPackage.BPEL_ACTIVITY__TARGET_SET, newTargetSet, newTargetSet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BPELSourceList getSourceSet() {
		return sourceSet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSourceSet(BPELSourceList newSourceSet, NotificationChain msgs) {
		BPELSourceList oldSourceSet = sourceSet;
		sourceSet = newSourceSet;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, VxBPELPackage.BPEL_ACTIVITY__SOURCE_SET, oldSourceSet, newSourceSet);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSourceSet(BPELSourceList newSourceSet) {
		if (newSourceSet != sourceSet) {
			NotificationChain msgs = null;
			if (sourceSet != null)
				msgs = ((InternalEObject)sourceSet).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - VxBPELPackage.BPEL_ACTIVITY__SOURCE_SET, null, msgs);
			if (newSourceSet != null)
				msgs = ((InternalEObject)newSourceSet).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - VxBPELPackage.BPEL_ACTIVITY__SOURCE_SET, null, msgs);
			msgs = basicSetSourceSet(newSourceSet, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VxBPELPackage.BPEL_ACTIVITY__SOURCE_SET, newSourceSet, newSourceSet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case VxBPELPackage.BPEL_ACTIVITY__ATTRIBUTES:
				return ((InternalEList<?>)getAttributes()).basicRemove(otherEnd, msgs);
			case VxBPELPackage.BPEL_ACTIVITY__TARGET_SET:
				return basicSetTargetSet(null, msgs);
			case VxBPELPackage.BPEL_ACTIVITY__SOURCE_SET:
				return basicSetSourceSet(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case VxBPELPackage.BPEL_ACTIVITY__ATTRIBUTES:
				return getAttributes();
			case VxBPELPackage.BPEL_ACTIVITY__TARGET_SET:
				return getTargetSet();
			case VxBPELPackage.BPEL_ACTIVITY__SOURCE_SET:
				return getSourceSet();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case VxBPELPackage.BPEL_ACTIVITY__ATTRIBUTES:
				getAttributes().clear();
				getAttributes().addAll((Collection<? extends XMLAttribute>)newValue);
				return;
			case VxBPELPackage.BPEL_ACTIVITY__TARGET_SET:
				setTargetSet((BPELTargetList)newValue);
				return;
			case VxBPELPackage.BPEL_ACTIVITY__SOURCE_SET:
				setSourceSet((BPELSourceList)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case VxBPELPackage.BPEL_ACTIVITY__ATTRIBUTES:
				getAttributes().clear();
				return;
			case VxBPELPackage.BPEL_ACTIVITY__TARGET_SET:
				setTargetSet((BPELTargetList)null);
				return;
			case VxBPELPackage.BPEL_ACTIVITY__SOURCE_SET:
				setSourceSet((BPELSourceList)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case VxBPELPackage.BPEL_ACTIVITY__ATTRIBUTES:
				return attributes != null && !attributes.isEmpty();
			case VxBPELPackage.BPEL_ACTIVITY__TARGET_SET:
				return targetSet != null;
			case VxBPELPackage.BPEL_ACTIVITY__SOURCE_SET:
				return sourceSet != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == XMLAttributeContainer.class) {
			switch (derivedFeatureID) {
				case VxBPELPackage.BPEL_ACTIVITY__ATTRIBUTES: return VxBPELPackage.XML_ATTRIBUTE_CONTAINER__ATTRIBUTES;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == XMLAttributeContainer.class) {
			switch (baseFeatureID) {
				case VxBPELPackage.XML_ATTRIBUTE_CONTAINER__ATTRIBUTES: return VxBPELPackage.BPEL_ACTIVITY__ATTRIBUTES;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

} //BPELActivityImpl
