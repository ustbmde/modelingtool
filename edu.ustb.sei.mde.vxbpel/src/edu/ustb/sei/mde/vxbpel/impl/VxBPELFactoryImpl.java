/**
 */
package edu.ustb.sei.mde.vxbpel.impl;

import edu.ustb.sei.mde.vxbpel.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class VxBPELFactoryImpl extends EFactoryImpl implements VxBPELFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static VxBPELFactory init() {
		try {
			VxBPELFactory theVxBPELFactory = (VxBPELFactory)EPackage.Registry.INSTANCE.getEFactory(VxBPELPackage.eNS_URI);
			if (theVxBPELFactory != null) {
				return theVxBPELFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new VxBPELFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VxBPELFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case VxBPELPackage.XML_ATTRIBUTE: return createXMLAttribute();
			case VxBPELPackage.BPEL_EXPRESSION: return createBPELExpression();
			case VxBPELPackage.BPEL_BOOL_EXPRESSION: return createBPELBoolExpression();
			case VxBPELPackage.BPEL_DEADLINE_EXPRESSION: return createBPELDeadlineExpression();
			case VxBPELPackage.BPEL_DURATION_EXPRESSION: return createBPELDurationExpression();
			case VxBPELPackage.BPEL_UNSIGNED_INTEGER_EXPRESSION: return createBPELUnsignedIntegerExpression();
			case VxBPELPackage.BPEL_PROCESS: return createBPELProcess();
			case VxBPELPackage.BPEL_IMPORT: return createBPELImport();
			case VxBPELPackage.BPEL_PARTNER_LINK: return createBPELPartnerLink();
			case VxBPELPackage.BPEL_MESSAGE_EXCHANGE: return createBPELMessageExchange();
			case VxBPELPackage.BPEL_VARIABLE: return createBPELVariable();
			case VxBPELPackage.BPEL_CORRELATION_SET: return createBPELCorrelationSet();
			case VxBPELPackage.BPEL_TARGET_LIST: return createBPELTargetList();
			case VxBPELPackage.BPEL_TARGET: return createBPELTarget();
			case VxBPELPackage.BPEL_SOURCE_LIST: return createBPELSourceList();
			case VxBPELPackage.BPEL_SOURCE: return createBPELSource();
			case VxBPELPackage.BPEL_INVOKE: return createBPELInvoke();
			case VxBPELPackage.BPEL_RECEIVE: return createBPELReceive();
			case VxBPELPackage.BPEL_REPLY: return createBPELReply();
			case VxBPELPackage.BPEL_ASSIGN: return createBPELAssign();
			case VxBPELPackage.BPEL_COPY: return createBPELCopy();
			case VxBPELPackage.BPEL_COPY_FROM: return createBPELCopyFrom();
			case VxBPELPackage.BPEL_COPY_TO: return createBPELCopyTo();
			case VxBPELPackage.BPEL_LITERAL: return createBPELLiteral();
			case VxBPELPackage.BPEL_QUERY: return createBPELQuery();
			case VxBPELPackage.BPEL_THROW: return createBPELThrow();
			case VxBPELPackage.BPEL_RETHROW: return createBPELRethrow();
			case VxBPELPackage.BPEL_WAIT: return createBPELWait();
			case VxBPELPackage.BPEL_EMPTY: return createBPELEmpty();
			case VxBPELPackage.BPEL_EXIT: return createBPELExit();
			case VxBPELPackage.BPEL_SEQUENCE: return createBPELSequence();
			case VxBPELPackage.BPEL_IF: return createBPELIf();
			case VxBPELPackage.BPEL_ELSE_IF: return createBPELElseIf();
			case VxBPELPackage.BPEL_WHILE: return createBPELWhile();
			case VxBPELPackage.BPEL_REPEAT_UNTIL: return createBPELRepeatUntil();
			case VxBPELPackage.BPEL_PICK: return createBPELPick();
			case VxBPELPackage.BPEL_FLOW: return createBPELFlow();
			case VxBPELPackage.BPEL_LINK: return createBPELLink();
			case VxBPELPackage.BPEL_FOR_EACH: return createBPELForEach();
			case VxBPELPackage.BPEL_SCOPE: return createBPELScope();
			case VxBPELPackage.VX_BPEL_VARIATION_POINT: return createVxBPELVariationPoint();
			case VxBPELPackage.VX_BPEL_VARIANT: return createVxBPELVariant();
			case VxBPELPackage.VX_BPEL_CONFIGURABLE_VARIATION_POINT: return createVxBPELConfigurableVariationPoint();
			case VxBPELPackage.VX_BPEL_CONFIGURABLE_VARIANT: return createVxBPELConfigurableVariant();
			case VxBPELPackage.VX_BPEL_VARIATION_POINT_PCHOICE: return createVxBPELVariationPointPChoice();
			case VxBPELPackage.VX_BPEL_PROCESS: return createVxBPELProcess();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XMLAttribute createXMLAttribute() {
		XMLAttributeImpl xmlAttribute = new XMLAttributeImpl();
		return xmlAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BPELExpression createBPELExpression() {
		BPELExpressionImpl bpelExpression = new BPELExpressionImpl();
		return bpelExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BPELBoolExpression createBPELBoolExpression() {
		BPELBoolExpressionImpl bpelBoolExpression = new BPELBoolExpressionImpl();
		return bpelBoolExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BPELDeadlineExpression createBPELDeadlineExpression() {
		BPELDeadlineExpressionImpl bpelDeadlineExpression = new BPELDeadlineExpressionImpl();
		return bpelDeadlineExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BPELDurationExpression createBPELDurationExpression() {
		BPELDurationExpressionImpl bpelDurationExpression = new BPELDurationExpressionImpl();
		return bpelDurationExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BPELUnsignedIntegerExpression createBPELUnsignedIntegerExpression() {
		BPELUnsignedIntegerExpressionImpl bpelUnsignedIntegerExpression = new BPELUnsignedIntegerExpressionImpl();
		return bpelUnsignedIntegerExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BPELProcess createBPELProcess() {
		BPELProcessImpl bpelProcess = new BPELProcessImpl();
		return bpelProcess;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BPELImport createBPELImport() {
		BPELImportImpl bpelImport = new BPELImportImpl();
		return bpelImport;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BPELPartnerLink createBPELPartnerLink() {
		BPELPartnerLinkImpl bpelPartnerLink = new BPELPartnerLinkImpl();
		return bpelPartnerLink;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BPELMessageExchange createBPELMessageExchange() {
		BPELMessageExchangeImpl bpelMessageExchange = new BPELMessageExchangeImpl();
		return bpelMessageExchange;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BPELVariable createBPELVariable() {
		BPELVariableImpl bpelVariable = new BPELVariableImpl();
		return bpelVariable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BPELCorrelationSet createBPELCorrelationSet() {
		BPELCorrelationSetImpl bpelCorrelationSet = new BPELCorrelationSetImpl();
		return bpelCorrelationSet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BPELTargetList createBPELTargetList() {
		BPELTargetListImpl bpelTargetList = new BPELTargetListImpl();
		return bpelTargetList;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BPELTarget createBPELTarget() {
		BPELTargetImpl bpelTarget = new BPELTargetImpl();
		return bpelTarget;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BPELSourceList createBPELSourceList() {
		BPELSourceListImpl bpelSourceList = new BPELSourceListImpl();
		return bpelSourceList;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BPELSource createBPELSource() {
		BPELSourceImpl bpelSource = new BPELSourceImpl();
		return bpelSource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BPELInvoke createBPELInvoke() {
		BPELInvokeImpl bpelInvoke = new BPELInvokeImpl();
		return bpelInvoke;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BPELReceive createBPELReceive() {
		BPELReceiveImpl bpelReceive = new BPELReceiveImpl();
		return bpelReceive;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BPELReply createBPELReply() {
		BPELReplyImpl bpelReply = new BPELReplyImpl();
		return bpelReply;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BPELAssign createBPELAssign() {
		BPELAssignImpl bpelAssign = new BPELAssignImpl();
		return bpelAssign;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BPELCopy createBPELCopy() {
		BPELCopyImpl bpelCopy = new BPELCopyImpl();
		return bpelCopy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BPELCopyFrom createBPELCopyFrom() {
		BPELCopyFromImpl bpelCopyFrom = new BPELCopyFromImpl();
		return bpelCopyFrom;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BPELCopyTo createBPELCopyTo() {
		BPELCopyToImpl bpelCopyTo = new BPELCopyToImpl();
		return bpelCopyTo;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BPELLiteral createBPELLiteral() {
		BPELLiteralImpl bpelLiteral = new BPELLiteralImpl();
		return bpelLiteral;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BPELQuery createBPELQuery() {
		BPELQueryImpl bpelQuery = new BPELQueryImpl();
		return bpelQuery;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BPELThrow createBPELThrow() {
		BPELThrowImpl bpelThrow = new BPELThrowImpl();
		return bpelThrow;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BPELRethrow createBPELRethrow() {
		BPELRethrowImpl bpelRethrow = new BPELRethrowImpl();
		return bpelRethrow;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BPELWait createBPELWait() {
		BPELWaitImpl bpelWait = new BPELWaitImpl();
		return bpelWait;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BPELEmpty createBPELEmpty() {
		BPELEmptyImpl bpelEmpty = new BPELEmptyImpl();
		return bpelEmpty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BPELExit createBPELExit() {
		BPELExitImpl bpelExit = new BPELExitImpl();
		return bpelExit;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BPELSequence createBPELSequence() {
		BPELSequenceImpl bpelSequence = new BPELSequenceImpl();
		return bpelSequence;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BPELIf createBPELIf() {
		BPELIfImpl bpelIf = new BPELIfImpl();
		return bpelIf;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BPELElseIf createBPELElseIf() {
		BPELElseIfImpl bpelElseIf = new BPELElseIfImpl();
		return bpelElseIf;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BPELWhile createBPELWhile() {
		BPELWhileImpl bpelWhile = new BPELWhileImpl();
		return bpelWhile;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BPELRepeatUntil createBPELRepeatUntil() {
		BPELRepeatUntilImpl bpelRepeatUntil = new BPELRepeatUntilImpl();
		return bpelRepeatUntil;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BPELPick createBPELPick() {
		BPELPickImpl bpelPick = new BPELPickImpl();
		return bpelPick;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BPELFlow createBPELFlow() {
		BPELFlowImpl bpelFlow = new BPELFlowImpl();
		return bpelFlow;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BPELLink createBPELLink() {
		BPELLinkImpl bpelLink = new BPELLinkImpl();
		return bpelLink;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BPELForEach createBPELForEach() {
		BPELForEachImpl bpelForEach = new BPELForEachImpl();
		return bpelForEach;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BPELScope createBPELScope() {
		BPELScopeImpl bpelScope = new BPELScopeImpl();
		return bpelScope;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VxBPELVariationPoint createVxBPELVariationPoint() {
		VxBPELVariationPointImpl vxBPELVariationPoint = new VxBPELVariationPointImpl();
		return vxBPELVariationPoint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VxBPELVariant createVxBPELVariant() {
		VxBPELVariantImpl vxBPELVariant = new VxBPELVariantImpl();
		return vxBPELVariant;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VxBPELConfigurableVariationPoint createVxBPELConfigurableVariationPoint() {
		VxBPELConfigurableVariationPointImpl vxBPELConfigurableVariationPoint = new VxBPELConfigurableVariationPointImpl();
		return vxBPELConfigurableVariationPoint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VxBPELConfigurableVariant createVxBPELConfigurableVariant() {
		VxBPELConfigurableVariantImpl vxBPELConfigurableVariant = new VxBPELConfigurableVariantImpl();
		return vxBPELConfigurableVariant;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VxBPELVariationPointPChoice createVxBPELVariationPointPChoice() {
		VxBPELVariationPointPChoiceImpl vxBPELVariationPointPChoice = new VxBPELVariationPointPChoiceImpl();
		return vxBPELVariationPointPChoice;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VxBPELProcess createVxBPELProcess() {
		VxBPELProcessImpl vxBPELProcess = new VxBPELProcessImpl();
		return vxBPELProcess;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VxBPELPackage getVxBPELPackage() {
		return (VxBPELPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static VxBPELPackage getPackage() {
		return VxBPELPackage.eINSTANCE;
	}

} //VxBPELFactoryImpl
