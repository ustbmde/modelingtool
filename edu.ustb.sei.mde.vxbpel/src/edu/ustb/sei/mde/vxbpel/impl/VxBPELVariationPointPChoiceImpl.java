/**
 */
package edu.ustb.sei.mde.vxbpel.impl;

import edu.ustb.sei.mde.vxbpel.VxBPELPackage;
import edu.ustb.sei.mde.vxbpel.VxBPELVariationPointPChoice;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Variation Point PChoice</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.impl.VxBPELVariationPointPChoiceImpl#getVpname <em>Vpname</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.impl.VxBPELVariationPointPChoiceImpl#getVariant <em>Variant</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class VxBPELVariationPointPChoiceImpl extends MinimalEObjectImpl.Container implements VxBPELVariationPointPChoice {
	/**
	 * The default value of the '{@link #getVpname() <em>Vpname</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVpname()
	 * @generated
	 * @ordered
	 */
	protected static final String VPNAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getVpname() <em>Vpname</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVpname()
	 * @generated
	 * @ordered
	 */
	protected String vpname = VPNAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getVariant() <em>Variant</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVariant()
	 * @generated
	 * @ordered
	 */
	protected static final String VARIANT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getVariant() <em>Variant</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVariant()
	 * @generated
	 * @ordered
	 */
	protected String variant = VARIANT_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VxBPELVariationPointPChoiceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return VxBPELPackage.Literals.VX_BPEL_VARIATION_POINT_PCHOICE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getVpname() {
		return vpname;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVpname(String newVpname) {
		String oldVpname = vpname;
		vpname = newVpname;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VxBPELPackage.VX_BPEL_VARIATION_POINT_PCHOICE__VPNAME, oldVpname, vpname));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getVariant() {
		return variant;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVariant(String newVariant) {
		String oldVariant = variant;
		variant = newVariant;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VxBPELPackage.VX_BPEL_VARIATION_POINT_PCHOICE__VARIANT, oldVariant, variant));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case VxBPELPackage.VX_BPEL_VARIATION_POINT_PCHOICE__VPNAME:
				return getVpname();
			case VxBPELPackage.VX_BPEL_VARIATION_POINT_PCHOICE__VARIANT:
				return getVariant();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case VxBPELPackage.VX_BPEL_VARIATION_POINT_PCHOICE__VPNAME:
				setVpname((String)newValue);
				return;
			case VxBPELPackage.VX_BPEL_VARIATION_POINT_PCHOICE__VARIANT:
				setVariant((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case VxBPELPackage.VX_BPEL_VARIATION_POINT_PCHOICE__VPNAME:
				setVpname(VPNAME_EDEFAULT);
				return;
			case VxBPELPackage.VX_BPEL_VARIATION_POINT_PCHOICE__VARIANT:
				setVariant(VARIANT_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case VxBPELPackage.VX_BPEL_VARIATION_POINT_PCHOICE__VPNAME:
				return VPNAME_EDEFAULT == null ? vpname != null : !VPNAME_EDEFAULT.equals(vpname);
			case VxBPELPackage.VX_BPEL_VARIATION_POINT_PCHOICE__VARIANT:
				return VARIANT_EDEFAULT == null ? variant != null : !VARIANT_EDEFAULT.equals(variant);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (vpname: ");
		result.append(vpname);
		result.append(", variant: ");
		result.append(variant);
		result.append(')');
		return result.toString();
	}

} //VxBPELVariationPointPChoiceImpl
