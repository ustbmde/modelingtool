/**
 */
package edu.ustb.sei.mde.vxbpel.impl;

import edu.ustb.sei.mde.vxbpel.BPELCopy;
import edu.ustb.sei.mde.vxbpel.BPELCopyFrom;
import edu.ustb.sei.mde.vxbpel.BPELCopyTo;
import edu.ustb.sei.mde.vxbpel.VxBPELPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>BPEL Copy</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.impl.BPELCopyImpl#getFromSpec <em>From Spec</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.impl.BPELCopyImpl#getToSpec <em>To Spec</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class BPELCopyImpl extends BPELAssignOperationImpl implements BPELCopy {
	/**
	 * The cached value of the '{@link #getFromSpec() <em>From Spec</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFromSpec()
	 * @generated
	 * @ordered
	 */
	protected BPELCopyFrom fromSpec;

	/**
	 * The cached value of the '{@link #getToSpec() <em>To Spec</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getToSpec()
	 * @generated
	 * @ordered
	 */
	protected BPELCopyTo toSpec;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BPELCopyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return VxBPELPackage.Literals.BPEL_COPY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BPELCopyFrom getFromSpec() {
		return fromSpec;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFromSpec(BPELCopyFrom newFromSpec, NotificationChain msgs) {
		BPELCopyFrom oldFromSpec = fromSpec;
		fromSpec = newFromSpec;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, VxBPELPackage.BPEL_COPY__FROM_SPEC, oldFromSpec, newFromSpec);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFromSpec(BPELCopyFrom newFromSpec) {
		if (newFromSpec != fromSpec) {
			NotificationChain msgs = null;
			if (fromSpec != null)
				msgs = ((InternalEObject)fromSpec).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - VxBPELPackage.BPEL_COPY__FROM_SPEC, null, msgs);
			if (newFromSpec != null)
				msgs = ((InternalEObject)newFromSpec).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - VxBPELPackage.BPEL_COPY__FROM_SPEC, null, msgs);
			msgs = basicSetFromSpec(newFromSpec, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VxBPELPackage.BPEL_COPY__FROM_SPEC, newFromSpec, newFromSpec));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BPELCopyTo getToSpec() {
		return toSpec;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetToSpec(BPELCopyTo newToSpec, NotificationChain msgs) {
		BPELCopyTo oldToSpec = toSpec;
		toSpec = newToSpec;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, VxBPELPackage.BPEL_COPY__TO_SPEC, oldToSpec, newToSpec);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setToSpec(BPELCopyTo newToSpec) {
		if (newToSpec != toSpec) {
			NotificationChain msgs = null;
			if (toSpec != null)
				msgs = ((InternalEObject)toSpec).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - VxBPELPackage.BPEL_COPY__TO_SPEC, null, msgs);
			if (newToSpec != null)
				msgs = ((InternalEObject)newToSpec).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - VxBPELPackage.BPEL_COPY__TO_SPEC, null, msgs);
			msgs = basicSetToSpec(newToSpec, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VxBPELPackage.BPEL_COPY__TO_SPEC, newToSpec, newToSpec));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case VxBPELPackage.BPEL_COPY__FROM_SPEC:
				return basicSetFromSpec(null, msgs);
			case VxBPELPackage.BPEL_COPY__TO_SPEC:
				return basicSetToSpec(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case VxBPELPackage.BPEL_COPY__FROM_SPEC:
				return getFromSpec();
			case VxBPELPackage.BPEL_COPY__TO_SPEC:
				return getToSpec();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case VxBPELPackage.BPEL_COPY__FROM_SPEC:
				setFromSpec((BPELCopyFrom)newValue);
				return;
			case VxBPELPackage.BPEL_COPY__TO_SPEC:
				setToSpec((BPELCopyTo)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case VxBPELPackage.BPEL_COPY__FROM_SPEC:
				setFromSpec((BPELCopyFrom)null);
				return;
			case VxBPELPackage.BPEL_COPY__TO_SPEC:
				setToSpec((BPELCopyTo)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case VxBPELPackage.BPEL_COPY__FROM_SPEC:
				return fromSpec != null;
			case VxBPELPackage.BPEL_COPY__TO_SPEC:
				return toSpec != null;
		}
		return super.eIsSet(featureID);
	}

} //BPELCopyImpl
