/**
 */
package edu.ustb.sei.mde.vxbpel.impl;

import edu.ustb.sei.mde.vxbpel.BPELActivity;
import edu.ustb.sei.mde.vxbpel.VxBPELPackage;
import edu.ustb.sei.mde.vxbpel.VxBPELVariant;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Variant</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.impl.VxBPELVariantImpl#getBpelCode <em>Bpel Code</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class VxBPELVariantImpl extends NamedElementImpl implements VxBPELVariant {
	/**
	 * The cached value of the '{@link #getBpelCode() <em>Bpel Code</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBpelCode()
	 * @generated
	 * @ordered
	 */
	protected BPELActivity bpelCode;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VxBPELVariantImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return VxBPELPackage.Literals.VX_BPEL_VARIANT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BPELActivity getBpelCode() {
		return bpelCode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetBpelCode(BPELActivity newBpelCode, NotificationChain msgs) {
		BPELActivity oldBpelCode = bpelCode;
		bpelCode = newBpelCode;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, VxBPELPackage.VX_BPEL_VARIANT__BPEL_CODE, oldBpelCode, newBpelCode);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBpelCode(BPELActivity newBpelCode) {
		if (newBpelCode != bpelCode) {
			NotificationChain msgs = null;
			if (bpelCode != null)
				msgs = ((InternalEObject)bpelCode).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - VxBPELPackage.VX_BPEL_VARIANT__BPEL_CODE, null, msgs);
			if (newBpelCode != null)
				msgs = ((InternalEObject)newBpelCode).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - VxBPELPackage.VX_BPEL_VARIANT__BPEL_CODE, null, msgs);
			msgs = basicSetBpelCode(newBpelCode, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VxBPELPackage.VX_BPEL_VARIANT__BPEL_CODE, newBpelCode, newBpelCode));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case VxBPELPackage.VX_BPEL_VARIANT__BPEL_CODE:
				return basicSetBpelCode(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case VxBPELPackage.VX_BPEL_VARIANT__BPEL_CODE:
				return getBpelCode();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case VxBPELPackage.VX_BPEL_VARIANT__BPEL_CODE:
				setBpelCode((BPELActivity)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case VxBPELPackage.VX_BPEL_VARIANT__BPEL_CODE:
				setBpelCode((BPELActivity)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case VxBPELPackage.VX_BPEL_VARIANT__BPEL_CODE:
				return bpelCode != null;
		}
		return super.eIsSet(featureID);
	}

} //VxBPELVariantImpl
