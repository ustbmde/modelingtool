/**
 */
package edu.ustb.sei.mde.vxbpel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Variant</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.VxBPELVariant#getBpelCode <em>Bpel Code</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getVxBPELVariant()
 * @model
 * @generated
 */
public interface VxBPELVariant extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Bpel Code</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Bpel Code</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Bpel Code</em>' containment reference.
	 * @see #setBpelCode(BPELActivity)
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getVxBPELVariant_BpelCode()
	 * @model containment="true" required="true"
	 * @generated
	 */
	BPELActivity getBpelCode();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.vxbpel.VxBPELVariant#getBpelCode <em>Bpel Code</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Bpel Code</em>' containment reference.
	 * @see #getBpelCode()
	 * @generated
	 */
	void setBpelCode(BPELActivity value);

} // VxBPELVariant
