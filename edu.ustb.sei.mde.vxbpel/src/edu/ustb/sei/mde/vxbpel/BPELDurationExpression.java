/**
 */
package edu.ustb.sei.mde.vxbpel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>BPEL Duration Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELDurationExpression()
 * @model
 * @generated
 */
public interface BPELDurationExpression extends BPELExpression {
} // BPELDurationExpression
