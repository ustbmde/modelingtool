/**
 */
package edu.ustb.sei.mde.vxbpel;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>BPEL Sequence</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.BPELSequence#getActivities <em>Activities</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELSequence()
 * @model
 * @generated
 */
public interface BPELSequence extends BPELActivity {
	/**
	 * Returns the value of the '<em><b>Activities</b></em>' containment reference list.
	 * The list contents are of type {@link edu.ustb.sei.mde.vxbpel.BPELActivity}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Activities</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Activities</em>' containment reference list.
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELSequence_Activities()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<BPELActivity> getActivities();

} // BPELSequence
