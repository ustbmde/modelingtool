/**
 */
package edu.ustb.sei.mde.vxbpel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>BPEL Exit</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELExit()
 * @model
 * @generated
 */
public interface BPELExit extends BPELActivity {
} // BPELExit
