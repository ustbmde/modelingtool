/**
 */
package edu.ustb.sei.mde.vxbpel;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Process</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.VxBPELProcess#getConfigurableVariationPoints <em>Configurable Variation Points</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getVxBPELProcess()
 * @model
 * @generated
 */
public interface VxBPELProcess extends BPELProcess {
	/**
	 * Returns the value of the '<em><b>Configurable Variation Points</b></em>' containment reference list.
	 * The list contents are of type {@link edu.ustb.sei.mde.vxbpel.VxBPELConfigurableVariationPoint}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Configurable Variation Points</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Configurable Variation Points</em>' containment reference list.
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getVxBPELProcess_ConfigurableVariationPoints()
	 * @model containment="true"
	 * @generated
	 */
	EList<VxBPELConfigurableVariationPoint> getConfigurableVariationPoints();

} // VxBPELProcess
