/**
 */
package edu.ustb.sei.mde.vxbpel;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>BPEL Target List</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.BPELTargetList#getJoinCondition <em>Join Condition</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.BPELTargetList#getTargets <em>Targets</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELTargetList()
 * @model
 * @generated
 */
public interface BPELTargetList extends XMLAttributeContainer {
	/**
	 * Returns the value of the '<em><b>Join Condition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Join Condition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Join Condition</em>' containment reference.
	 * @see #setJoinCondition(BPELBoolExpression)
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELTargetList_JoinCondition()
	 * @model containment="true"
	 * @generated
	 */
	BPELBoolExpression getJoinCondition();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.vxbpel.BPELTargetList#getJoinCondition <em>Join Condition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Join Condition</em>' containment reference.
	 * @see #getJoinCondition()
	 * @generated
	 */
	void setJoinCondition(BPELBoolExpression value);

	/**
	 * Returns the value of the '<em><b>Targets</b></em>' containment reference list.
	 * The list contents are of type {@link edu.ustb.sei.mde.vxbpel.BPELTarget}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Targets</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Targets</em>' containment reference list.
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELTargetList_Targets()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<BPELTarget> getTargets();

} // BPELTargetList
