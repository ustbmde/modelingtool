/**
 */
package edu.ustb.sei.mde.vxbpel;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see edu.ustb.sei.mde.vxbpel.VxBPELFactory
 * @model kind="package"
 * @generated
 */
public interface VxBPELPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "vxbpel";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "edu.ustb.sei.mde.vxbpel";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "vxbpel";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	VxBPELPackage eINSTANCE = edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl.init();

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.vxbpel.impl.NamedElementImpl <em>Named Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.vxbpel.impl.NamedElementImpl
	 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getNamedElement()
	 * @generated
	 */
	int NAMED_ELEMENT = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ELEMENT__NAME = 0;

	/**
	 * The number of structural features of the '<em>Named Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ELEMENT_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Named Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ELEMENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.vxbpel.impl.XMLAttributeContainerImpl <em>XML Attribute Container</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.vxbpel.impl.XMLAttributeContainerImpl
	 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getXMLAttributeContainer()
	 * @generated
	 */
	int XML_ATTRIBUTE_CONTAINER = 1;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_ATTRIBUTE_CONTAINER__ATTRIBUTES = 0;

	/**
	 * The number of structural features of the '<em>XML Attribute Container</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_ATTRIBUTE_CONTAINER_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>XML Attribute Container</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_ATTRIBUTE_CONTAINER_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.vxbpel.impl.XMLAttributeImpl <em>XML Attribute</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.vxbpel.impl.XMLAttributeImpl
	 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getXMLAttribute()
	 * @generated
	 */
	int XML_ATTRIBUTE = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_ATTRIBUTE__NAME = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_ATTRIBUTE__VALUE = 1;

	/**
	 * The number of structural features of the '<em>XML Attribute</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_ATTRIBUTE_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>XML Attribute</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XML_ATTRIBUTE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.vxbpel.impl.BPELExpressionImpl <em>BPEL Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.vxbpel.impl.BPELExpressionImpl
	 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getBPELExpression()
	 * @generated
	 */
	int BPEL_EXPRESSION = 3;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_EXPRESSION__ATTRIBUTES = XML_ATTRIBUTE_CONTAINER__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Expression Language</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_EXPRESSION__EXPRESSION_LANGUAGE = XML_ATTRIBUTE_CONTAINER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Expression</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_EXPRESSION__EXPRESSION = XML_ATTRIBUTE_CONTAINER_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>BPEL Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_EXPRESSION_FEATURE_COUNT = XML_ATTRIBUTE_CONTAINER_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>BPEL Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_EXPRESSION_OPERATION_COUNT = XML_ATTRIBUTE_CONTAINER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.vxbpel.impl.BPELBoolExpressionImpl <em>BPEL Bool Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.vxbpel.impl.BPELBoolExpressionImpl
	 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getBPELBoolExpression()
	 * @generated
	 */
	int BPEL_BOOL_EXPRESSION = 4;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_BOOL_EXPRESSION__ATTRIBUTES = BPEL_EXPRESSION__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Expression Language</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_BOOL_EXPRESSION__EXPRESSION_LANGUAGE = BPEL_EXPRESSION__EXPRESSION_LANGUAGE;

	/**
	 * The feature id for the '<em><b>Expression</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_BOOL_EXPRESSION__EXPRESSION = BPEL_EXPRESSION__EXPRESSION;

	/**
	 * The number of structural features of the '<em>BPEL Bool Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_BOOL_EXPRESSION_FEATURE_COUNT = BPEL_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>BPEL Bool Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_BOOL_EXPRESSION_OPERATION_COUNT = BPEL_EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.vxbpel.impl.BPELDeadlineExpressionImpl <em>BPEL Deadline Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.vxbpel.impl.BPELDeadlineExpressionImpl
	 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getBPELDeadlineExpression()
	 * @generated
	 */
	int BPEL_DEADLINE_EXPRESSION = 5;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_DEADLINE_EXPRESSION__ATTRIBUTES = BPEL_EXPRESSION__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Expression Language</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_DEADLINE_EXPRESSION__EXPRESSION_LANGUAGE = BPEL_EXPRESSION__EXPRESSION_LANGUAGE;

	/**
	 * The feature id for the '<em><b>Expression</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_DEADLINE_EXPRESSION__EXPRESSION = BPEL_EXPRESSION__EXPRESSION;

	/**
	 * The number of structural features of the '<em>BPEL Deadline Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_DEADLINE_EXPRESSION_FEATURE_COUNT = BPEL_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>BPEL Deadline Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_DEADLINE_EXPRESSION_OPERATION_COUNT = BPEL_EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.vxbpel.impl.BPELDurationExpressionImpl <em>BPEL Duration Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.vxbpel.impl.BPELDurationExpressionImpl
	 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getBPELDurationExpression()
	 * @generated
	 */
	int BPEL_DURATION_EXPRESSION = 6;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_DURATION_EXPRESSION__ATTRIBUTES = BPEL_EXPRESSION__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Expression Language</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_DURATION_EXPRESSION__EXPRESSION_LANGUAGE = BPEL_EXPRESSION__EXPRESSION_LANGUAGE;

	/**
	 * The feature id for the '<em><b>Expression</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_DURATION_EXPRESSION__EXPRESSION = BPEL_EXPRESSION__EXPRESSION;

	/**
	 * The number of structural features of the '<em>BPEL Duration Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_DURATION_EXPRESSION_FEATURE_COUNT = BPEL_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>BPEL Duration Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_DURATION_EXPRESSION_OPERATION_COUNT = BPEL_EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.vxbpel.impl.BPELUnsignedIntegerExpressionImpl <em>BPEL Unsigned Integer Expression</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.vxbpel.impl.BPELUnsignedIntegerExpressionImpl
	 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getBPELUnsignedIntegerExpression()
	 * @generated
	 */
	int BPEL_UNSIGNED_INTEGER_EXPRESSION = 7;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_UNSIGNED_INTEGER_EXPRESSION__ATTRIBUTES = BPEL_EXPRESSION__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Expression Language</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_UNSIGNED_INTEGER_EXPRESSION__EXPRESSION_LANGUAGE = BPEL_EXPRESSION__EXPRESSION_LANGUAGE;

	/**
	 * The feature id for the '<em><b>Expression</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_UNSIGNED_INTEGER_EXPRESSION__EXPRESSION = BPEL_EXPRESSION__EXPRESSION;

	/**
	 * The number of structural features of the '<em>BPEL Unsigned Integer Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_UNSIGNED_INTEGER_EXPRESSION_FEATURE_COUNT = BPEL_EXPRESSION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>BPEL Unsigned Integer Expression</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_UNSIGNED_INTEGER_EXPRESSION_OPERATION_COUNT = BPEL_EXPRESSION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.vxbpel.impl.BPELScopeElementImpl <em>BPEL Scope Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.vxbpel.impl.BPELScopeElementImpl
	 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getBPELScopeElement()
	 * @generated
	 */
	int BPEL_SCOPE_ELEMENT = 8;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_SCOPE_ELEMENT__ATTRIBUTES = XML_ATTRIBUTE_CONTAINER__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Exit On Standard Fault</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_SCOPE_ELEMENT__EXIT_ON_STANDARD_FAULT = XML_ATTRIBUTE_CONTAINER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Partner Links</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_SCOPE_ELEMENT__PARTNER_LINKS = XML_ATTRIBUTE_CONTAINER_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Message Exchanges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_SCOPE_ELEMENT__MESSAGE_EXCHANGES = XML_ATTRIBUTE_CONTAINER_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Variables</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_SCOPE_ELEMENT__VARIABLES = XML_ATTRIBUTE_CONTAINER_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Correlation Sets</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_SCOPE_ELEMENT__CORRELATION_SETS = XML_ATTRIBUTE_CONTAINER_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Fault Handler List</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_SCOPE_ELEMENT__FAULT_HANDLER_LIST = XML_ATTRIBUTE_CONTAINER_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Event Handler List</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_SCOPE_ELEMENT__EVENT_HANDLER_LIST = XML_ATTRIBUTE_CONTAINER_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Activity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_SCOPE_ELEMENT__ACTIVITY = XML_ATTRIBUTE_CONTAINER_FEATURE_COUNT + 7;

	/**
	 * The number of structural features of the '<em>BPEL Scope Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_SCOPE_ELEMENT_FEATURE_COUNT = XML_ATTRIBUTE_CONTAINER_FEATURE_COUNT + 8;

	/**
	 * The number of operations of the '<em>BPEL Scope Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_SCOPE_ELEMENT_OPERATION_COUNT = XML_ATTRIBUTE_CONTAINER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.vxbpel.impl.BPELProcessImpl <em>BPEL Process</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.vxbpel.impl.BPELProcessImpl
	 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getBPELProcess()
	 * @generated
	 */
	int BPEL_PROCESS = 9;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_PROCESS__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_PROCESS__ATTRIBUTES = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Exit On Standard Fault</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_PROCESS__EXIT_ON_STANDARD_FAULT = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Partner Links</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_PROCESS__PARTNER_LINKS = NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Message Exchanges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_PROCESS__MESSAGE_EXCHANGES = NAMED_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Variables</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_PROCESS__VARIABLES = NAMED_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Correlation Sets</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_PROCESS__CORRELATION_SETS = NAMED_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Fault Handler List</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_PROCESS__FAULT_HANDLER_LIST = NAMED_ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Event Handler List</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_PROCESS__EVENT_HANDLER_LIST = NAMED_ELEMENT_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Activity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_PROCESS__ACTIVITY = NAMED_ELEMENT_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Target Namespace</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_PROCESS__TARGET_NAMESPACE = NAMED_ELEMENT_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Query Language</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_PROCESS__QUERY_LANGUAGE = NAMED_ELEMENT_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>Expression Language</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_PROCESS__EXPRESSION_LANGUAGE = NAMED_ELEMENT_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>Imports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_PROCESS__IMPORTS = NAMED_ELEMENT_FEATURE_COUNT + 12;

	/**
	 * The number of structural features of the '<em>BPEL Process</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_PROCESS_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 13;

	/**
	 * The number of operations of the '<em>BPEL Process</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_PROCESS_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.vxbpel.impl.BPELImportImpl <em>BPEL Import</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.vxbpel.impl.BPELImportImpl
	 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getBPELImport()
	 * @generated
	 */
	int BPEL_IMPORT = 10;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_IMPORT__ATTRIBUTES = XML_ATTRIBUTE_CONTAINER__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Namespace</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_IMPORT__NAMESPACE = XML_ATTRIBUTE_CONTAINER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_IMPORT__LOCATION = XML_ATTRIBUTE_CONTAINER_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Import Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_IMPORT__IMPORT_TYPE = XML_ATTRIBUTE_CONTAINER_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>BPEL Import</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_IMPORT_FEATURE_COUNT = XML_ATTRIBUTE_CONTAINER_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>BPEL Import</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_IMPORT_OPERATION_COUNT = XML_ATTRIBUTE_CONTAINER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.vxbpel.impl.BPELPartnerLinkImpl <em>BPEL Partner Link</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.vxbpel.impl.BPELPartnerLinkImpl
	 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getBPELPartnerLink()
	 * @generated
	 */
	int BPEL_PARTNER_LINK = 11;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_PARTNER_LINK__ATTRIBUTES = XML_ATTRIBUTE_CONTAINER__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_PARTNER_LINK__NAME = XML_ATTRIBUTE_CONTAINER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Partner Link Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_PARTNER_LINK__PARTNER_LINK_TYPE = XML_ATTRIBUTE_CONTAINER_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>My Role</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_PARTNER_LINK__MY_ROLE = XML_ATTRIBUTE_CONTAINER_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Partner Role</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_PARTNER_LINK__PARTNER_ROLE = XML_ATTRIBUTE_CONTAINER_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Initialize Partner Role</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_PARTNER_LINK__INITIALIZE_PARTNER_ROLE = XML_ATTRIBUTE_CONTAINER_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>BPEL Partner Link</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_PARTNER_LINK_FEATURE_COUNT = XML_ATTRIBUTE_CONTAINER_FEATURE_COUNT + 5;

	/**
	 * The number of operations of the '<em>BPEL Partner Link</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_PARTNER_LINK_OPERATION_COUNT = XML_ATTRIBUTE_CONTAINER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.vxbpel.impl.BPELMessageExchangeImpl <em>BPEL Message Exchange</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.vxbpel.impl.BPELMessageExchangeImpl
	 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getBPELMessageExchange()
	 * @generated
	 */
	int BPEL_MESSAGE_EXCHANGE = 12;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_MESSAGE_EXCHANGE__ATTRIBUTES = XML_ATTRIBUTE_CONTAINER__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_MESSAGE_EXCHANGE__NAME = XML_ATTRIBUTE_CONTAINER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>BPEL Message Exchange</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_MESSAGE_EXCHANGE_FEATURE_COUNT = XML_ATTRIBUTE_CONTAINER_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>BPEL Message Exchange</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_MESSAGE_EXCHANGE_OPERATION_COUNT = XML_ATTRIBUTE_CONTAINER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.vxbpel.impl.BPELVariableImpl <em>BPEL Variable</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.vxbpel.impl.BPELVariableImpl
	 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getBPELVariable()
	 * @generated
	 */
	int BPEL_VARIABLE = 13;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_VARIABLE__ATTRIBUTES = XML_ATTRIBUTE_CONTAINER__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_VARIABLE__NAME = XML_ATTRIBUTE_CONTAINER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Message Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_VARIABLE__MESSAGE_TYPE = XML_ATTRIBUTE_CONTAINER_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_VARIABLE__TYPE = XML_ATTRIBUTE_CONTAINER_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Element</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_VARIABLE__ELEMENT = XML_ATTRIBUTE_CONTAINER_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>From Spec</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_VARIABLE__FROM_SPEC = XML_ATTRIBUTE_CONTAINER_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>BPEL Variable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_VARIABLE_FEATURE_COUNT = XML_ATTRIBUTE_CONTAINER_FEATURE_COUNT + 5;

	/**
	 * The number of operations of the '<em>BPEL Variable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_VARIABLE_OPERATION_COUNT = XML_ATTRIBUTE_CONTAINER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.vxbpel.impl.BPELCorrelationSetImpl <em>BPEL Correlation Set</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.vxbpel.impl.BPELCorrelationSetImpl
	 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getBPELCorrelationSet()
	 * @generated
	 */
	int BPEL_CORRELATION_SET = 14;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_CORRELATION_SET__ATTRIBUTES = XML_ATTRIBUTE_CONTAINER__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_CORRELATION_SET__NAME = XML_ATTRIBUTE_CONTAINER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Properties</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_CORRELATION_SET__PROPERTIES = XML_ATTRIBUTE_CONTAINER_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>BPEL Correlation Set</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_CORRELATION_SET_FEATURE_COUNT = XML_ATTRIBUTE_CONTAINER_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>BPEL Correlation Set</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_CORRELATION_SET_OPERATION_COUNT = XML_ATTRIBUTE_CONTAINER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.vxbpel.impl.BPELActivityImpl <em>BPEL Activity</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.vxbpel.impl.BPELActivityImpl
	 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getBPELActivity()
	 * @generated
	 */
	int BPEL_ACTIVITY = 15;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_ACTIVITY__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_ACTIVITY__ATTRIBUTES = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Target Set</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_ACTIVITY__TARGET_SET = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Source Set</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_ACTIVITY__SOURCE_SET = NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>BPEL Activity</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_ACTIVITY_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>BPEL Activity</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_ACTIVITY_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.vxbpel.impl.BPELTargetListImpl <em>BPEL Target List</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.vxbpel.impl.BPELTargetListImpl
	 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getBPELTargetList()
	 * @generated
	 */
	int BPEL_TARGET_LIST = 16;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_TARGET_LIST__ATTRIBUTES = XML_ATTRIBUTE_CONTAINER__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Join Condition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_TARGET_LIST__JOIN_CONDITION = XML_ATTRIBUTE_CONTAINER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Targets</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_TARGET_LIST__TARGETS = XML_ATTRIBUTE_CONTAINER_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>BPEL Target List</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_TARGET_LIST_FEATURE_COUNT = XML_ATTRIBUTE_CONTAINER_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>BPEL Target List</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_TARGET_LIST_OPERATION_COUNT = XML_ATTRIBUTE_CONTAINER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.vxbpel.impl.BPELTargetImpl <em>BPEL Target</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.vxbpel.impl.BPELTargetImpl
	 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getBPELTarget()
	 * @generated
	 */
	int BPEL_TARGET = 17;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_TARGET__ATTRIBUTES = XML_ATTRIBUTE_CONTAINER__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Link Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_TARGET__LINK_NAME = XML_ATTRIBUTE_CONTAINER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>BPEL Target</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_TARGET_FEATURE_COUNT = XML_ATTRIBUTE_CONTAINER_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>BPEL Target</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_TARGET_OPERATION_COUNT = XML_ATTRIBUTE_CONTAINER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.vxbpel.impl.BPELSourceListImpl <em>BPEL Source List</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.vxbpel.impl.BPELSourceListImpl
	 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getBPELSourceList()
	 * @generated
	 */
	int BPEL_SOURCE_LIST = 18;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_SOURCE_LIST__ATTRIBUTES = XML_ATTRIBUTE_CONTAINER__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sources</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_SOURCE_LIST__SOURCES = XML_ATTRIBUTE_CONTAINER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>BPEL Source List</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_SOURCE_LIST_FEATURE_COUNT = XML_ATTRIBUTE_CONTAINER_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>BPEL Source List</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_SOURCE_LIST_OPERATION_COUNT = XML_ATTRIBUTE_CONTAINER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.vxbpel.impl.BPELSourceImpl <em>BPEL Source</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.vxbpel.impl.BPELSourceImpl
	 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getBPELSource()
	 * @generated
	 */
	int BPEL_SOURCE = 19;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_SOURCE__ATTRIBUTES = XML_ATTRIBUTE_CONTAINER__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Link Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_SOURCE__LINK_NAME = XML_ATTRIBUTE_CONTAINER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Transition Condition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_SOURCE__TRANSITION_CONDITION = XML_ATTRIBUTE_CONTAINER_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>BPEL Source</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_SOURCE_FEATURE_COUNT = XML_ATTRIBUTE_CONTAINER_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>BPEL Source</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_SOURCE_OPERATION_COUNT = XML_ATTRIBUTE_CONTAINER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.vxbpel.impl.BPELInvokeImpl <em>BPEL Invoke</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.vxbpel.impl.BPELInvokeImpl
	 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getBPELInvoke()
	 * @generated
	 */
	int BPEL_INVOKE = 20;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_INVOKE__NAME = BPEL_ACTIVITY__NAME;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_INVOKE__ATTRIBUTES = BPEL_ACTIVITY__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Target Set</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_INVOKE__TARGET_SET = BPEL_ACTIVITY__TARGET_SET;

	/**
	 * The feature id for the '<em><b>Source Set</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_INVOKE__SOURCE_SET = BPEL_ACTIVITY__SOURCE_SET;

	/**
	 * The feature id for the '<em><b>Partner Link</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_INVOKE__PARTNER_LINK = BPEL_ACTIVITY_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Port Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_INVOKE__PORT_TYPE = BPEL_ACTIVITY_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Operation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_INVOKE__OPERATION = BPEL_ACTIVITY_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Input Variable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_INVOKE__INPUT_VARIABLE = BPEL_ACTIVITY_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Output Variable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_INVOKE__OUTPUT_VARIABLE = BPEL_ACTIVITY_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>BPEL Invoke</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_INVOKE_FEATURE_COUNT = BPEL_ACTIVITY_FEATURE_COUNT + 5;

	/**
	 * The number of operations of the '<em>BPEL Invoke</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_INVOKE_OPERATION_COUNT = BPEL_ACTIVITY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.vxbpel.impl.BPELReceiveImpl <em>BPEL Receive</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.vxbpel.impl.BPELReceiveImpl
	 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getBPELReceive()
	 * @generated
	 */
	int BPEL_RECEIVE = 21;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_RECEIVE__NAME = BPEL_ACTIVITY__NAME;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_RECEIVE__ATTRIBUTES = BPEL_ACTIVITY__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Target Set</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_RECEIVE__TARGET_SET = BPEL_ACTIVITY__TARGET_SET;

	/**
	 * The feature id for the '<em><b>Source Set</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_RECEIVE__SOURCE_SET = BPEL_ACTIVITY__SOURCE_SET;

	/**
	 * The feature id for the '<em><b>Partner Link</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_RECEIVE__PARTNER_LINK = BPEL_ACTIVITY_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Port Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_RECEIVE__PORT_TYPE = BPEL_ACTIVITY_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Operation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_RECEIVE__OPERATION = BPEL_ACTIVITY_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Variable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_RECEIVE__VARIABLE = BPEL_ACTIVITY_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Create Instance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_RECEIVE__CREATE_INSTANCE = BPEL_ACTIVITY_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Message Exchange</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_RECEIVE__MESSAGE_EXCHANGE = BPEL_ACTIVITY_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>BPEL Receive</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_RECEIVE_FEATURE_COUNT = BPEL_ACTIVITY_FEATURE_COUNT + 6;

	/**
	 * The number of operations of the '<em>BPEL Receive</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_RECEIVE_OPERATION_COUNT = BPEL_ACTIVITY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.vxbpel.impl.BPELReplyImpl <em>BPEL Reply</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.vxbpel.impl.BPELReplyImpl
	 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getBPELReply()
	 * @generated
	 */
	int BPEL_REPLY = 22;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_REPLY__NAME = BPEL_ACTIVITY__NAME;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_REPLY__ATTRIBUTES = BPEL_ACTIVITY__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Target Set</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_REPLY__TARGET_SET = BPEL_ACTIVITY__TARGET_SET;

	/**
	 * The feature id for the '<em><b>Source Set</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_REPLY__SOURCE_SET = BPEL_ACTIVITY__SOURCE_SET;

	/**
	 * The feature id for the '<em><b>Partner Link</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_REPLY__PARTNER_LINK = BPEL_ACTIVITY_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Port Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_REPLY__PORT_TYPE = BPEL_ACTIVITY_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Operation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_REPLY__OPERATION = BPEL_ACTIVITY_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Variable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_REPLY__VARIABLE = BPEL_ACTIVITY_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Fault Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_REPLY__FAULT_NAME = BPEL_ACTIVITY_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Message Exchange</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_REPLY__MESSAGE_EXCHANGE = BPEL_ACTIVITY_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>BPEL Reply</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_REPLY_FEATURE_COUNT = BPEL_ACTIVITY_FEATURE_COUNT + 6;

	/**
	 * The number of operations of the '<em>BPEL Reply</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_REPLY_OPERATION_COUNT = BPEL_ACTIVITY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.vxbpel.impl.BPELAssignImpl <em>BPEL Assign</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.vxbpel.impl.BPELAssignImpl
	 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getBPELAssign()
	 * @generated
	 */
	int BPEL_ASSIGN = 23;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_ASSIGN__NAME = BPEL_ACTIVITY__NAME;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_ASSIGN__ATTRIBUTES = BPEL_ACTIVITY__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Target Set</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_ASSIGN__TARGET_SET = BPEL_ACTIVITY__TARGET_SET;

	/**
	 * The feature id for the '<em><b>Source Set</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_ASSIGN__SOURCE_SET = BPEL_ACTIVITY__SOURCE_SET;

	/**
	 * The feature id for the '<em><b>Validate</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_ASSIGN__VALIDATE = BPEL_ACTIVITY_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Parts</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_ASSIGN__PARTS = BPEL_ACTIVITY_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>BPEL Assign</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_ASSIGN_FEATURE_COUNT = BPEL_ACTIVITY_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>BPEL Assign</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_ASSIGN_OPERATION_COUNT = BPEL_ACTIVITY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.vxbpel.impl.BPELAssignOperationImpl <em>BPEL Assign Operation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.vxbpel.impl.BPELAssignOperationImpl
	 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getBPELAssignOperation()
	 * @generated
	 */
	int BPEL_ASSIGN_OPERATION = 24;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_ASSIGN_OPERATION__ATTRIBUTES = XML_ATTRIBUTE_CONTAINER__ATTRIBUTES;

	/**
	 * The number of structural features of the '<em>BPEL Assign Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_ASSIGN_OPERATION_FEATURE_COUNT = XML_ATTRIBUTE_CONTAINER_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>BPEL Assign Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_ASSIGN_OPERATION_OPERATION_COUNT = XML_ATTRIBUTE_CONTAINER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.vxbpel.impl.BPELCopyImpl <em>BPEL Copy</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.vxbpel.impl.BPELCopyImpl
	 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getBPELCopy()
	 * @generated
	 */
	int BPEL_COPY = 25;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_COPY__ATTRIBUTES = BPEL_ASSIGN_OPERATION__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>From Spec</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_COPY__FROM_SPEC = BPEL_ASSIGN_OPERATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>To Spec</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_COPY__TO_SPEC = BPEL_ASSIGN_OPERATION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>BPEL Copy</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_COPY_FEATURE_COUNT = BPEL_ASSIGN_OPERATION_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>BPEL Copy</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_COPY_OPERATION_COUNT = BPEL_ASSIGN_OPERATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.vxbpel.impl.BPELCopyFromImpl <em>BPEL Copy From</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.vxbpel.impl.BPELCopyFromImpl
	 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getBPELCopyFrom()
	 * @generated
	 */
	int BPEL_COPY_FROM = 26;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_COPY_FROM__ATTRIBUTES = XML_ATTRIBUTE_CONTAINER__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Variable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_COPY_FROM__VARIABLE = XML_ATTRIBUTE_CONTAINER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Part</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_COPY_FROM__PART = XML_ATTRIBUTE_CONTAINER_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Property</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_COPY_FROM__PROPERTY = XML_ATTRIBUTE_CONTAINER_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Partner Link</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_COPY_FROM__PARTNER_LINK = XML_ATTRIBUTE_CONTAINER_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Endpoint Reference</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_COPY_FROM__ENDPOINT_REFERENCE = XML_ATTRIBUTE_CONTAINER_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_COPY_FROM__EXPRESSION = XML_ATTRIBUTE_CONTAINER_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Query</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_COPY_FROM__QUERY = XML_ATTRIBUTE_CONTAINER_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Literal</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_COPY_FROM__LITERAL = XML_ATTRIBUTE_CONTAINER_FEATURE_COUNT + 7;

	/**
	 * The number of structural features of the '<em>BPEL Copy From</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_COPY_FROM_FEATURE_COUNT = XML_ATTRIBUTE_CONTAINER_FEATURE_COUNT + 8;

	/**
	 * The number of operations of the '<em>BPEL Copy From</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_COPY_FROM_OPERATION_COUNT = XML_ATTRIBUTE_CONTAINER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.vxbpel.impl.BPELCopyToImpl <em>BPEL Copy To</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.vxbpel.impl.BPELCopyToImpl
	 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getBPELCopyTo()
	 * @generated
	 */
	int BPEL_COPY_TO = 27;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_COPY_TO__ATTRIBUTES = XML_ATTRIBUTE_CONTAINER__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Variable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_COPY_TO__VARIABLE = XML_ATTRIBUTE_CONTAINER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Part</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_COPY_TO__PART = XML_ATTRIBUTE_CONTAINER_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Property</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_COPY_TO__PROPERTY = XML_ATTRIBUTE_CONTAINER_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Partner Link</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_COPY_TO__PARTNER_LINK = XML_ATTRIBUTE_CONTAINER_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_COPY_TO__EXPRESSION = XML_ATTRIBUTE_CONTAINER_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Query</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_COPY_TO__QUERY = XML_ATTRIBUTE_CONTAINER_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>BPEL Copy To</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_COPY_TO_FEATURE_COUNT = XML_ATTRIBUTE_CONTAINER_FEATURE_COUNT + 6;

	/**
	 * The number of operations of the '<em>BPEL Copy To</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_COPY_TO_OPERATION_COUNT = XML_ATTRIBUTE_CONTAINER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.vxbpel.impl.BPELLiteralImpl <em>BPEL Literal</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.vxbpel.impl.BPELLiteralImpl
	 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getBPELLiteral()
	 * @generated
	 */
	int BPEL_LITERAL = 28;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_LITERAL__TYPE = 0;

	/**
	 * The feature id for the '<em><b>Literal</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_LITERAL__LITERAL = 1;

	/**
	 * The number of structural features of the '<em>BPEL Literal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_LITERAL_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>BPEL Literal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_LITERAL_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.vxbpel.impl.BPELQueryImpl <em>BPEL Query</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.vxbpel.impl.BPELQueryImpl
	 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getBPELQuery()
	 * @generated
	 */
	int BPEL_QUERY = 29;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_QUERY__ATTRIBUTES = XML_ATTRIBUTE_CONTAINER__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Query Language</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_QUERY__QUERY_LANGUAGE = XML_ATTRIBUTE_CONTAINER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Content</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_QUERY__CONTENT = XML_ATTRIBUTE_CONTAINER_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>BPEL Query</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_QUERY_FEATURE_COUNT = XML_ATTRIBUTE_CONTAINER_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>BPEL Query</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_QUERY_OPERATION_COUNT = XML_ATTRIBUTE_CONTAINER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.vxbpel.impl.BPELThrowImpl <em>BPEL Throw</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.vxbpel.impl.BPELThrowImpl
	 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getBPELThrow()
	 * @generated
	 */
	int BPEL_THROW = 30;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_THROW__NAME = BPEL_ACTIVITY__NAME;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_THROW__ATTRIBUTES = BPEL_ACTIVITY__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Target Set</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_THROW__TARGET_SET = BPEL_ACTIVITY__TARGET_SET;

	/**
	 * The feature id for the '<em><b>Source Set</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_THROW__SOURCE_SET = BPEL_ACTIVITY__SOURCE_SET;

	/**
	 * The feature id for the '<em><b>Fault Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_THROW__FAULT_NAME = BPEL_ACTIVITY_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Fault Variable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_THROW__FAULT_VARIABLE = BPEL_ACTIVITY_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>BPEL Throw</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_THROW_FEATURE_COUNT = BPEL_ACTIVITY_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>BPEL Throw</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_THROW_OPERATION_COUNT = BPEL_ACTIVITY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.vxbpel.impl.BPELRethrowImpl <em>BPEL Rethrow</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.vxbpel.impl.BPELRethrowImpl
	 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getBPELRethrow()
	 * @generated
	 */
	int BPEL_RETHROW = 31;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_RETHROW__NAME = BPEL_ACTIVITY__NAME;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_RETHROW__ATTRIBUTES = BPEL_ACTIVITY__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Target Set</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_RETHROW__TARGET_SET = BPEL_ACTIVITY__TARGET_SET;

	/**
	 * The feature id for the '<em><b>Source Set</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_RETHROW__SOURCE_SET = BPEL_ACTIVITY__SOURCE_SET;

	/**
	 * The number of structural features of the '<em>BPEL Rethrow</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_RETHROW_FEATURE_COUNT = BPEL_ACTIVITY_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>BPEL Rethrow</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_RETHROW_OPERATION_COUNT = BPEL_ACTIVITY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.vxbpel.impl.BPELWaitImpl <em>BPEL Wait</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.vxbpel.impl.BPELWaitImpl
	 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getBPELWait()
	 * @generated
	 */
	int BPEL_WAIT = 32;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_WAIT__NAME = BPEL_ACTIVITY__NAME;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_WAIT__ATTRIBUTES = BPEL_ACTIVITY__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Target Set</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_WAIT__TARGET_SET = BPEL_ACTIVITY__TARGET_SET;

	/**
	 * The feature id for the '<em><b>Source Set</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_WAIT__SOURCE_SET = BPEL_ACTIVITY__SOURCE_SET;

	/**
	 * The feature id for the '<em><b>Deadline Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_WAIT__DEADLINE_EXPRESSION = BPEL_ACTIVITY_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Duration Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_WAIT__DURATION_EXPRESSION = BPEL_ACTIVITY_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>BPEL Wait</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_WAIT_FEATURE_COUNT = BPEL_ACTIVITY_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>BPEL Wait</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_WAIT_OPERATION_COUNT = BPEL_ACTIVITY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.vxbpel.impl.BPELEmptyImpl <em>BPEL Empty</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.vxbpel.impl.BPELEmptyImpl
	 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getBPELEmpty()
	 * @generated
	 */
	int BPEL_EMPTY = 33;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_EMPTY__NAME = BPEL_ACTIVITY__NAME;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_EMPTY__ATTRIBUTES = BPEL_ACTIVITY__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Target Set</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_EMPTY__TARGET_SET = BPEL_ACTIVITY__TARGET_SET;

	/**
	 * The feature id for the '<em><b>Source Set</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_EMPTY__SOURCE_SET = BPEL_ACTIVITY__SOURCE_SET;

	/**
	 * The number of structural features of the '<em>BPEL Empty</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_EMPTY_FEATURE_COUNT = BPEL_ACTIVITY_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>BPEL Empty</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_EMPTY_OPERATION_COUNT = BPEL_ACTIVITY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.vxbpel.impl.BPELExitImpl <em>BPEL Exit</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.vxbpel.impl.BPELExitImpl
	 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getBPELExit()
	 * @generated
	 */
	int BPEL_EXIT = 34;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_EXIT__NAME = BPEL_ACTIVITY__NAME;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_EXIT__ATTRIBUTES = BPEL_ACTIVITY__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Target Set</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_EXIT__TARGET_SET = BPEL_ACTIVITY__TARGET_SET;

	/**
	 * The feature id for the '<em><b>Source Set</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_EXIT__SOURCE_SET = BPEL_ACTIVITY__SOURCE_SET;

	/**
	 * The number of structural features of the '<em>BPEL Exit</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_EXIT_FEATURE_COUNT = BPEL_ACTIVITY_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>BPEL Exit</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_EXIT_OPERATION_COUNT = BPEL_ACTIVITY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.vxbpel.impl.BPELSequenceImpl <em>BPEL Sequence</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.vxbpel.impl.BPELSequenceImpl
	 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getBPELSequence()
	 * @generated
	 */
	int BPEL_SEQUENCE = 35;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_SEQUENCE__NAME = BPEL_ACTIVITY__NAME;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_SEQUENCE__ATTRIBUTES = BPEL_ACTIVITY__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Target Set</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_SEQUENCE__TARGET_SET = BPEL_ACTIVITY__TARGET_SET;

	/**
	 * The feature id for the '<em><b>Source Set</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_SEQUENCE__SOURCE_SET = BPEL_ACTIVITY__SOURCE_SET;

	/**
	 * The feature id for the '<em><b>Activities</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_SEQUENCE__ACTIVITIES = BPEL_ACTIVITY_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>BPEL Sequence</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_SEQUENCE_FEATURE_COUNT = BPEL_ACTIVITY_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>BPEL Sequence</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_SEQUENCE_OPERATION_COUNT = BPEL_ACTIVITY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.vxbpel.impl.BPELIfImpl <em>BPEL If</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.vxbpel.impl.BPELIfImpl
	 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getBPELIf()
	 * @generated
	 */
	int BPEL_IF = 36;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_IF__NAME = BPEL_ACTIVITY__NAME;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_IF__ATTRIBUTES = BPEL_ACTIVITY__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Target Set</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_IF__TARGET_SET = BPEL_ACTIVITY__TARGET_SET;

	/**
	 * The feature id for the '<em><b>Source Set</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_IF__SOURCE_SET = BPEL_ACTIVITY__SOURCE_SET;

	/**
	 * The feature id for the '<em><b>Condition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_IF__CONDITION = BPEL_ACTIVITY_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Then Activity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_IF__THEN_ACTIVITY = BPEL_ACTIVITY_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Else If Activity</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_IF__ELSE_IF_ACTIVITY = BPEL_ACTIVITY_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Else Activity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_IF__ELSE_ACTIVITY = BPEL_ACTIVITY_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>BPEL If</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_IF_FEATURE_COUNT = BPEL_ACTIVITY_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>BPEL If</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_IF_OPERATION_COUNT = BPEL_ACTIVITY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.vxbpel.impl.BPELElseIfImpl <em>BPEL Else If</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.vxbpel.impl.BPELElseIfImpl
	 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getBPELElseIf()
	 * @generated
	 */
	int BPEL_ELSE_IF = 37;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_ELSE_IF__NAME = BPEL_ACTIVITY__NAME;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_ELSE_IF__ATTRIBUTES = BPEL_ACTIVITY__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Target Set</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_ELSE_IF__TARGET_SET = BPEL_ACTIVITY__TARGET_SET;

	/**
	 * The feature id for the '<em><b>Source Set</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_ELSE_IF__SOURCE_SET = BPEL_ACTIVITY__SOURCE_SET;

	/**
	 * The feature id for the '<em><b>Condition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_ELSE_IF__CONDITION = BPEL_ACTIVITY_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Then Activity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_ELSE_IF__THEN_ACTIVITY = BPEL_ACTIVITY_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>BPEL Else If</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_ELSE_IF_FEATURE_COUNT = BPEL_ACTIVITY_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>BPEL Else If</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_ELSE_IF_OPERATION_COUNT = BPEL_ACTIVITY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.vxbpel.impl.BPELWhileImpl <em>BPEL While</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.vxbpel.impl.BPELWhileImpl
	 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getBPELWhile()
	 * @generated
	 */
	int BPEL_WHILE = 38;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_WHILE__NAME = BPEL_ACTIVITY__NAME;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_WHILE__ATTRIBUTES = BPEL_ACTIVITY__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Target Set</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_WHILE__TARGET_SET = BPEL_ACTIVITY__TARGET_SET;

	/**
	 * The feature id for the '<em><b>Source Set</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_WHILE__SOURCE_SET = BPEL_ACTIVITY__SOURCE_SET;

	/**
	 * The feature id for the '<em><b>Condition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_WHILE__CONDITION = BPEL_ACTIVITY_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Body Activity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_WHILE__BODY_ACTIVITY = BPEL_ACTIVITY_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>BPEL While</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_WHILE_FEATURE_COUNT = BPEL_ACTIVITY_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>BPEL While</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_WHILE_OPERATION_COUNT = BPEL_ACTIVITY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.vxbpel.impl.BPELRepeatUntilImpl <em>BPEL Repeat Until</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.vxbpel.impl.BPELRepeatUntilImpl
	 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getBPELRepeatUntil()
	 * @generated
	 */
	int BPEL_REPEAT_UNTIL = 39;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_REPEAT_UNTIL__NAME = BPEL_ACTIVITY__NAME;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_REPEAT_UNTIL__ATTRIBUTES = BPEL_ACTIVITY__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Target Set</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_REPEAT_UNTIL__TARGET_SET = BPEL_ACTIVITY__TARGET_SET;

	/**
	 * The feature id for the '<em><b>Source Set</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_REPEAT_UNTIL__SOURCE_SET = BPEL_ACTIVITY__SOURCE_SET;

	/**
	 * The feature id for the '<em><b>Body Activity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_REPEAT_UNTIL__BODY_ACTIVITY = BPEL_ACTIVITY_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Condition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_REPEAT_UNTIL__CONDITION = BPEL_ACTIVITY_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>BPEL Repeat Until</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_REPEAT_UNTIL_FEATURE_COUNT = BPEL_ACTIVITY_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>BPEL Repeat Until</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_REPEAT_UNTIL_OPERATION_COUNT = BPEL_ACTIVITY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.vxbpel.impl.BPELPickImpl <em>BPEL Pick</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.vxbpel.impl.BPELPickImpl
	 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getBPELPick()
	 * @generated
	 */
	int BPEL_PICK = 40;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_PICK__NAME = BPEL_ACTIVITY__NAME;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_PICK__ATTRIBUTES = BPEL_ACTIVITY__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Target Set</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_PICK__TARGET_SET = BPEL_ACTIVITY__TARGET_SET;

	/**
	 * The feature id for the '<em><b>Source Set</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_PICK__SOURCE_SET = BPEL_ACTIVITY__SOURCE_SET;

	/**
	 * The number of structural features of the '<em>BPEL Pick</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_PICK_FEATURE_COUNT = BPEL_ACTIVITY_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>BPEL Pick</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_PICK_OPERATION_COUNT = BPEL_ACTIVITY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.vxbpel.impl.BPELFlowImpl <em>BPEL Flow</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.vxbpel.impl.BPELFlowImpl
	 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getBPELFlow()
	 * @generated
	 */
	int BPEL_FLOW = 41;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_FLOW__NAME = BPEL_ACTIVITY__NAME;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_FLOW__ATTRIBUTES = BPEL_ACTIVITY__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Target Set</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_FLOW__TARGET_SET = BPEL_ACTIVITY__TARGET_SET;

	/**
	 * The feature id for the '<em><b>Source Set</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_FLOW__SOURCE_SET = BPEL_ACTIVITY__SOURCE_SET;

	/**
	 * The feature id for the '<em><b>Links</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_FLOW__LINKS = BPEL_ACTIVITY_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Body Activities</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_FLOW__BODY_ACTIVITIES = BPEL_ACTIVITY_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>BPEL Flow</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_FLOW_FEATURE_COUNT = BPEL_ACTIVITY_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>BPEL Flow</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_FLOW_OPERATION_COUNT = BPEL_ACTIVITY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.vxbpel.impl.BPELLinkImpl <em>BPEL Link</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.vxbpel.impl.BPELLinkImpl
	 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getBPELLink()
	 * @generated
	 */
	int BPEL_LINK = 42;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_LINK__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The number of structural features of the '<em>BPEL Link</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_LINK_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>BPEL Link</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_LINK_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.vxbpel.impl.BPELForEachImpl <em>BPEL For Each</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.vxbpel.impl.BPELForEachImpl
	 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getBPELForEach()
	 * @generated
	 */
	int BPEL_FOR_EACH = 43;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_FOR_EACH__NAME = BPEL_ACTIVITY__NAME;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_FOR_EACH__ATTRIBUTES = BPEL_ACTIVITY__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Target Set</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_FOR_EACH__TARGET_SET = BPEL_ACTIVITY__TARGET_SET;

	/**
	 * The feature id for the '<em><b>Source Set</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_FOR_EACH__SOURCE_SET = BPEL_ACTIVITY__SOURCE_SET;

	/**
	 * The feature id for the '<em><b>Start Counter Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_FOR_EACH__START_COUNTER_VALUE = BPEL_ACTIVITY_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Final Counter Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_FOR_EACH__FINAL_COUNTER_VALUE = BPEL_ACTIVITY_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Completion Condition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_FOR_EACH__COMPLETION_CONDITION = BPEL_ACTIVITY_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Scope</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_FOR_EACH__SCOPE = BPEL_ACTIVITY_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>BPEL For Each</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_FOR_EACH_FEATURE_COUNT = BPEL_ACTIVITY_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>BPEL For Each</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_FOR_EACH_OPERATION_COUNT = BPEL_ACTIVITY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.vxbpel.impl.BPELScopeImpl <em>BPEL Scope</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.vxbpel.impl.BPELScopeImpl
	 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getBPELScope()
	 * @generated
	 */
	int BPEL_SCOPE = 44;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_SCOPE__NAME = BPEL_ACTIVITY__NAME;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_SCOPE__ATTRIBUTES = BPEL_ACTIVITY__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Target Set</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_SCOPE__TARGET_SET = BPEL_ACTIVITY__TARGET_SET;

	/**
	 * The feature id for the '<em><b>Source Set</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_SCOPE__SOURCE_SET = BPEL_ACTIVITY__SOURCE_SET;

	/**
	 * The feature id for the '<em><b>Exit On Standard Fault</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_SCOPE__EXIT_ON_STANDARD_FAULT = BPEL_ACTIVITY_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Partner Links</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_SCOPE__PARTNER_LINKS = BPEL_ACTIVITY_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Message Exchanges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_SCOPE__MESSAGE_EXCHANGES = BPEL_ACTIVITY_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Variables</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_SCOPE__VARIABLES = BPEL_ACTIVITY_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Correlation Sets</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_SCOPE__CORRELATION_SETS = BPEL_ACTIVITY_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Fault Handler List</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_SCOPE__FAULT_HANDLER_LIST = BPEL_ACTIVITY_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Event Handler List</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_SCOPE__EVENT_HANDLER_LIST = BPEL_ACTIVITY_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Activity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_SCOPE__ACTIVITY = BPEL_ACTIVITY_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Compensation Handler</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_SCOPE__COMPENSATION_HANDLER = BPEL_ACTIVITY_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Termination Handler</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_SCOPE__TERMINATION_HANDLER = BPEL_ACTIVITY_FEATURE_COUNT + 9;

	/**
	 * The number of structural features of the '<em>BPEL Scope</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_SCOPE_FEATURE_COUNT = BPEL_ACTIVITY_FEATURE_COUNT + 10;

	/**
	 * The number of operations of the '<em>BPEL Scope</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BPEL_SCOPE_OPERATION_COUNT = BPEL_ACTIVITY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.vxbpel.impl.VxBPELVariationPointImpl <em>Variation Point</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELVariationPointImpl
	 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getVxBPELVariationPoint()
	 * @generated
	 */
	int VX_BPEL_VARIATION_POINT = 45;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VX_BPEL_VARIATION_POINT__NAME = BPEL_ACTIVITY__NAME;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VX_BPEL_VARIATION_POINT__ATTRIBUTES = BPEL_ACTIVITY__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Target Set</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VX_BPEL_VARIATION_POINT__TARGET_SET = BPEL_ACTIVITY__TARGET_SET;

	/**
	 * The feature id for the '<em><b>Source Set</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VX_BPEL_VARIATION_POINT__SOURCE_SET = BPEL_ACTIVITY__SOURCE_SET;

	/**
	 * The feature id for the '<em><b>Variants</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VX_BPEL_VARIATION_POINT__VARIANTS = BPEL_ACTIVITY_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Variation Point</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VX_BPEL_VARIATION_POINT_FEATURE_COUNT = BPEL_ACTIVITY_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Variation Point</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VX_BPEL_VARIATION_POINT_OPERATION_COUNT = BPEL_ACTIVITY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.vxbpel.impl.VxBPELVariantImpl <em>Variant</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELVariantImpl
	 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getVxBPELVariant()
	 * @generated
	 */
	int VX_BPEL_VARIANT = 46;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VX_BPEL_VARIANT__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Bpel Code</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VX_BPEL_VARIANT__BPEL_CODE = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Variant</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VX_BPEL_VARIANT_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Variant</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VX_BPEL_VARIANT_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.vxbpel.impl.VxBPELConfigurableVariationPointImpl <em>Configurable Variation Point</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELConfigurableVariationPointImpl
	 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getVxBPELConfigurableVariationPoint()
	 * @generated
	 */
	int VX_BPEL_CONFIGURABLE_VARIATION_POINT = 47;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VX_BPEL_CONFIGURABLE_VARIATION_POINT__ATTRIBUTES = XML_ATTRIBUTE_CONTAINER__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VX_BPEL_CONFIGURABLE_VARIATION_POINT__NAME = XML_ATTRIBUTE_CONTAINER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VX_BPEL_CONFIGURABLE_VARIATION_POINT__ID = XML_ATTRIBUTE_CONTAINER_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Default Variant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VX_BPEL_CONFIGURABLE_VARIATION_POINT__DEFAULT_VARIANT = XML_ATTRIBUTE_CONTAINER_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Variants</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VX_BPEL_CONFIGURABLE_VARIATION_POINT__VARIANTS = XML_ATTRIBUTE_CONTAINER_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Configurable Variation Point</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VX_BPEL_CONFIGURABLE_VARIATION_POINT_FEATURE_COUNT = XML_ATTRIBUTE_CONTAINER_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>Configurable Variation Point</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VX_BPEL_CONFIGURABLE_VARIATION_POINT_OPERATION_COUNT = XML_ATTRIBUTE_CONTAINER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.vxbpel.impl.VxBPELConfigurableVariantImpl <em>Configurable Variant</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELConfigurableVariantImpl
	 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getVxBPELConfigurableVariant()
	 * @generated
	 */
	int VX_BPEL_CONFIGURABLE_VARIANT = 48;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VX_BPEL_CONFIGURABLE_VARIANT__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Variant Info</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VX_BPEL_CONFIGURABLE_VARIANT__VARIANT_INFO = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Rationale</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VX_BPEL_CONFIGURABLE_VARIANT__RATIONALE = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Required Variants</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VX_BPEL_CONFIGURABLE_VARIANT__REQUIRED_VARIANTS = NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Configurable Variant</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VX_BPEL_CONFIGURABLE_VARIANT_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Configurable Variant</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VX_BPEL_CONFIGURABLE_VARIANT_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.vxbpel.impl.VxBPELVariationPointPChoiceImpl <em>Variation Point PChoice</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELVariationPointPChoiceImpl
	 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getVxBPELVariationPointPChoice()
	 * @generated
	 */
	int VX_BPEL_VARIATION_POINT_PCHOICE = 49;

	/**
	 * The feature id for the '<em><b>Vpname</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VX_BPEL_VARIATION_POINT_PCHOICE__VPNAME = 0;

	/**
	 * The feature id for the '<em><b>Variant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VX_BPEL_VARIATION_POINT_PCHOICE__VARIANT = 1;

	/**
	 * The number of structural features of the '<em>Variation Point PChoice</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VX_BPEL_VARIATION_POINT_PCHOICE_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Variation Point PChoice</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VX_BPEL_VARIATION_POINT_PCHOICE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.vxbpel.impl.VxBPELProcessImpl <em>Process</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELProcessImpl
	 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getVxBPELProcess()
	 * @generated
	 */
	int VX_BPEL_PROCESS = 50;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VX_BPEL_PROCESS__NAME = BPEL_PROCESS__NAME;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VX_BPEL_PROCESS__ATTRIBUTES = BPEL_PROCESS__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Exit On Standard Fault</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VX_BPEL_PROCESS__EXIT_ON_STANDARD_FAULT = BPEL_PROCESS__EXIT_ON_STANDARD_FAULT;

	/**
	 * The feature id for the '<em><b>Partner Links</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VX_BPEL_PROCESS__PARTNER_LINKS = BPEL_PROCESS__PARTNER_LINKS;

	/**
	 * The feature id for the '<em><b>Message Exchanges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VX_BPEL_PROCESS__MESSAGE_EXCHANGES = BPEL_PROCESS__MESSAGE_EXCHANGES;

	/**
	 * The feature id for the '<em><b>Variables</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VX_BPEL_PROCESS__VARIABLES = BPEL_PROCESS__VARIABLES;

	/**
	 * The feature id for the '<em><b>Correlation Sets</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VX_BPEL_PROCESS__CORRELATION_SETS = BPEL_PROCESS__CORRELATION_SETS;

	/**
	 * The feature id for the '<em><b>Fault Handler List</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VX_BPEL_PROCESS__FAULT_HANDLER_LIST = BPEL_PROCESS__FAULT_HANDLER_LIST;

	/**
	 * The feature id for the '<em><b>Event Handler List</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VX_BPEL_PROCESS__EVENT_HANDLER_LIST = BPEL_PROCESS__EVENT_HANDLER_LIST;

	/**
	 * The feature id for the '<em><b>Activity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VX_BPEL_PROCESS__ACTIVITY = BPEL_PROCESS__ACTIVITY;

	/**
	 * The feature id for the '<em><b>Target Namespace</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VX_BPEL_PROCESS__TARGET_NAMESPACE = BPEL_PROCESS__TARGET_NAMESPACE;

	/**
	 * The feature id for the '<em><b>Query Language</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VX_BPEL_PROCESS__QUERY_LANGUAGE = BPEL_PROCESS__QUERY_LANGUAGE;

	/**
	 * The feature id for the '<em><b>Expression Language</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VX_BPEL_PROCESS__EXPRESSION_LANGUAGE = BPEL_PROCESS__EXPRESSION_LANGUAGE;

	/**
	 * The feature id for the '<em><b>Imports</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VX_BPEL_PROCESS__IMPORTS = BPEL_PROCESS__IMPORTS;

	/**
	 * The feature id for the '<em><b>Configurable Variation Points</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VX_BPEL_PROCESS__CONFIGURABLE_VARIATION_POINTS = BPEL_PROCESS_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Process</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VX_BPEL_PROCESS_FEATURE_COUNT = BPEL_PROCESS_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Process</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VX_BPEL_PROCESS_OPERATION_COUNT = BPEL_PROCESS_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.vxbpel.NamedElement <em>Named Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Named Element</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.NamedElement
	 * @generated
	 */
	EClass getNamedElement();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.vxbpel.NamedElement#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.NamedElement#getName()
	 * @see #getNamedElement()
	 * @generated
	 */
	EAttribute getNamedElement_Name();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.vxbpel.XMLAttributeContainer <em>XML Attribute Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>XML Attribute Container</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.XMLAttributeContainer
	 * @generated
	 */
	EClass getXMLAttributeContainer();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.ustb.sei.mde.vxbpel.XMLAttributeContainer#getAttributes <em>Attributes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Attributes</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.XMLAttributeContainer#getAttributes()
	 * @see #getXMLAttributeContainer()
	 * @generated
	 */
	EReference getXMLAttributeContainer_Attributes();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.vxbpel.XMLAttribute <em>XML Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>XML Attribute</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.XMLAttribute
	 * @generated
	 */
	EClass getXMLAttribute();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.vxbpel.XMLAttribute#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.XMLAttribute#getName()
	 * @see #getXMLAttribute()
	 * @generated
	 */
	EAttribute getXMLAttribute_Name();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.vxbpel.XMLAttribute#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.XMLAttribute#getValue()
	 * @see #getXMLAttribute()
	 * @generated
	 */
	EAttribute getXMLAttribute_Value();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.vxbpel.BPELExpression <em>BPEL Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>BPEL Expression</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELExpression
	 * @generated
	 */
	EClass getBPELExpression();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.vxbpel.BPELExpression#getExpressionLanguage <em>Expression Language</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Expression Language</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELExpression#getExpressionLanguage()
	 * @see #getBPELExpression()
	 * @generated
	 */
	EAttribute getBPELExpression_ExpressionLanguage();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.vxbpel.BPELExpression#getExpression <em>Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Expression</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELExpression#getExpression()
	 * @see #getBPELExpression()
	 * @generated
	 */
	EAttribute getBPELExpression_Expression();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.vxbpel.BPELBoolExpression <em>BPEL Bool Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>BPEL Bool Expression</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELBoolExpression
	 * @generated
	 */
	EClass getBPELBoolExpression();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.vxbpel.BPELDeadlineExpression <em>BPEL Deadline Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>BPEL Deadline Expression</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELDeadlineExpression
	 * @generated
	 */
	EClass getBPELDeadlineExpression();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.vxbpel.BPELDurationExpression <em>BPEL Duration Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>BPEL Duration Expression</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELDurationExpression
	 * @generated
	 */
	EClass getBPELDurationExpression();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.vxbpel.BPELUnsignedIntegerExpression <em>BPEL Unsigned Integer Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>BPEL Unsigned Integer Expression</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELUnsignedIntegerExpression
	 * @generated
	 */
	EClass getBPELUnsignedIntegerExpression();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.vxbpel.BPELScopeElement <em>BPEL Scope Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>BPEL Scope Element</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELScopeElement
	 * @generated
	 */
	EClass getBPELScopeElement();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.vxbpel.BPELScopeElement#getExitOnStandardFault <em>Exit On Standard Fault</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Exit On Standard Fault</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELScopeElement#getExitOnStandardFault()
	 * @see #getBPELScopeElement()
	 * @generated
	 */
	EAttribute getBPELScopeElement_ExitOnStandardFault();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.ustb.sei.mde.vxbpel.BPELScopeElement#getPartnerLinks <em>Partner Links</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Partner Links</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELScopeElement#getPartnerLinks()
	 * @see #getBPELScopeElement()
	 * @generated
	 */
	EReference getBPELScopeElement_PartnerLinks();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.ustb.sei.mde.vxbpel.BPELScopeElement#getMessageExchanges <em>Message Exchanges</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Message Exchanges</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELScopeElement#getMessageExchanges()
	 * @see #getBPELScopeElement()
	 * @generated
	 */
	EReference getBPELScopeElement_MessageExchanges();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.ustb.sei.mde.vxbpel.BPELScopeElement#getVariables <em>Variables</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Variables</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELScopeElement#getVariables()
	 * @see #getBPELScopeElement()
	 * @generated
	 */
	EReference getBPELScopeElement_Variables();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.ustb.sei.mde.vxbpel.BPELScopeElement#getCorrelationSets <em>Correlation Sets</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Correlation Sets</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELScopeElement#getCorrelationSets()
	 * @see #getBPELScopeElement()
	 * @generated
	 */
	EReference getBPELScopeElement_CorrelationSets();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.ustb.sei.mde.vxbpel.BPELScopeElement#getFaultHandlerList <em>Fault Handler List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Fault Handler List</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELScopeElement#getFaultHandlerList()
	 * @see #getBPELScopeElement()
	 * @generated
	 */
	EReference getBPELScopeElement_FaultHandlerList();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.ustb.sei.mde.vxbpel.BPELScopeElement#getEventHandlerList <em>Event Handler List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Event Handler List</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELScopeElement#getEventHandlerList()
	 * @see #getBPELScopeElement()
	 * @generated
	 */
	EReference getBPELScopeElement_EventHandlerList();

	/**
	 * Returns the meta object for the containment reference '{@link edu.ustb.sei.mde.vxbpel.BPELScopeElement#getActivity <em>Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Activity</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELScopeElement#getActivity()
	 * @see #getBPELScopeElement()
	 * @generated
	 */
	EReference getBPELScopeElement_Activity();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.vxbpel.BPELProcess <em>BPEL Process</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>BPEL Process</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELProcess
	 * @generated
	 */
	EClass getBPELProcess();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.vxbpel.BPELProcess#getTargetNamespace <em>Target Namespace</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Target Namespace</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELProcess#getTargetNamespace()
	 * @see #getBPELProcess()
	 * @generated
	 */
	EAttribute getBPELProcess_TargetNamespace();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.vxbpel.BPELProcess#getQueryLanguage <em>Query Language</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Query Language</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELProcess#getQueryLanguage()
	 * @see #getBPELProcess()
	 * @generated
	 */
	EAttribute getBPELProcess_QueryLanguage();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.vxbpel.BPELProcess#getExpressionLanguage <em>Expression Language</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Expression Language</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELProcess#getExpressionLanguage()
	 * @see #getBPELProcess()
	 * @generated
	 */
	EAttribute getBPELProcess_ExpressionLanguage();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.ustb.sei.mde.vxbpel.BPELProcess#getImports <em>Imports</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Imports</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELProcess#getImports()
	 * @see #getBPELProcess()
	 * @generated
	 */
	EReference getBPELProcess_Imports();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.vxbpel.BPELImport <em>BPEL Import</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>BPEL Import</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELImport
	 * @generated
	 */
	EClass getBPELImport();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.vxbpel.BPELImport#getNamespace <em>Namespace</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Namespace</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELImport#getNamespace()
	 * @see #getBPELImport()
	 * @generated
	 */
	EAttribute getBPELImport_Namespace();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.vxbpel.BPELImport#getLocation <em>Location</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Location</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELImport#getLocation()
	 * @see #getBPELImport()
	 * @generated
	 */
	EAttribute getBPELImport_Location();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.vxbpel.BPELImport#getImportType <em>Import Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Import Type</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELImport#getImportType()
	 * @see #getBPELImport()
	 * @generated
	 */
	EAttribute getBPELImport_ImportType();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.vxbpel.BPELPartnerLink <em>BPEL Partner Link</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>BPEL Partner Link</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELPartnerLink
	 * @generated
	 */
	EClass getBPELPartnerLink();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.vxbpel.BPELPartnerLink#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELPartnerLink#getName()
	 * @see #getBPELPartnerLink()
	 * @generated
	 */
	EAttribute getBPELPartnerLink_Name();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.vxbpel.BPELPartnerLink#getPartnerLinkType <em>Partner Link Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Partner Link Type</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELPartnerLink#getPartnerLinkType()
	 * @see #getBPELPartnerLink()
	 * @generated
	 */
	EAttribute getBPELPartnerLink_PartnerLinkType();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.vxbpel.BPELPartnerLink#getMyRole <em>My Role</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>My Role</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELPartnerLink#getMyRole()
	 * @see #getBPELPartnerLink()
	 * @generated
	 */
	EAttribute getBPELPartnerLink_MyRole();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.vxbpel.BPELPartnerLink#getPartnerRole <em>Partner Role</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Partner Role</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELPartnerLink#getPartnerRole()
	 * @see #getBPELPartnerLink()
	 * @generated
	 */
	EAttribute getBPELPartnerLink_PartnerRole();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.vxbpel.BPELPartnerLink#getInitializePartnerRole <em>Initialize Partner Role</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Initialize Partner Role</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELPartnerLink#getInitializePartnerRole()
	 * @see #getBPELPartnerLink()
	 * @generated
	 */
	EAttribute getBPELPartnerLink_InitializePartnerRole();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.vxbpel.BPELMessageExchange <em>BPEL Message Exchange</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>BPEL Message Exchange</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELMessageExchange
	 * @generated
	 */
	EClass getBPELMessageExchange();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.vxbpel.BPELMessageExchange#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELMessageExchange#getName()
	 * @see #getBPELMessageExchange()
	 * @generated
	 */
	EAttribute getBPELMessageExchange_Name();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.vxbpel.BPELVariable <em>BPEL Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>BPEL Variable</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELVariable
	 * @generated
	 */
	EClass getBPELVariable();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.vxbpel.BPELVariable#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELVariable#getName()
	 * @see #getBPELVariable()
	 * @generated
	 */
	EAttribute getBPELVariable_Name();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.vxbpel.BPELVariable#getMessageType <em>Message Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Message Type</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELVariable#getMessageType()
	 * @see #getBPELVariable()
	 * @generated
	 */
	EAttribute getBPELVariable_MessageType();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.vxbpel.BPELVariable#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELVariable#getType()
	 * @see #getBPELVariable()
	 * @generated
	 */
	EAttribute getBPELVariable_Type();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.vxbpel.BPELVariable#getElement <em>Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Element</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELVariable#getElement()
	 * @see #getBPELVariable()
	 * @generated
	 */
	EAttribute getBPELVariable_Element();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.vxbpel.BPELVariable#getFromSpec <em>From Spec</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>From Spec</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELVariable#getFromSpec()
	 * @see #getBPELVariable()
	 * @generated
	 */
	EAttribute getBPELVariable_FromSpec();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.vxbpel.BPELCorrelationSet <em>BPEL Correlation Set</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>BPEL Correlation Set</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELCorrelationSet
	 * @generated
	 */
	EClass getBPELCorrelationSet();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.vxbpel.BPELCorrelationSet#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELCorrelationSet#getName()
	 * @see #getBPELCorrelationSet()
	 * @generated
	 */
	EAttribute getBPELCorrelationSet_Name();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.vxbpel.BPELCorrelationSet#getProperties <em>Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Properties</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELCorrelationSet#getProperties()
	 * @see #getBPELCorrelationSet()
	 * @generated
	 */
	EAttribute getBPELCorrelationSet_Properties();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.vxbpel.BPELActivity <em>BPEL Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>BPEL Activity</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELActivity
	 * @generated
	 */
	EClass getBPELActivity();

	/**
	 * Returns the meta object for the containment reference '{@link edu.ustb.sei.mde.vxbpel.BPELActivity#getTargetSet <em>Target Set</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Target Set</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELActivity#getTargetSet()
	 * @see #getBPELActivity()
	 * @generated
	 */
	EReference getBPELActivity_TargetSet();

	/**
	 * Returns the meta object for the containment reference '{@link edu.ustb.sei.mde.vxbpel.BPELActivity#getSourceSet <em>Source Set</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Source Set</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELActivity#getSourceSet()
	 * @see #getBPELActivity()
	 * @generated
	 */
	EReference getBPELActivity_SourceSet();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.vxbpel.BPELTargetList <em>BPEL Target List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>BPEL Target List</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELTargetList
	 * @generated
	 */
	EClass getBPELTargetList();

	/**
	 * Returns the meta object for the containment reference '{@link edu.ustb.sei.mde.vxbpel.BPELTargetList#getJoinCondition <em>Join Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Join Condition</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELTargetList#getJoinCondition()
	 * @see #getBPELTargetList()
	 * @generated
	 */
	EReference getBPELTargetList_JoinCondition();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.ustb.sei.mde.vxbpel.BPELTargetList#getTargets <em>Targets</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Targets</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELTargetList#getTargets()
	 * @see #getBPELTargetList()
	 * @generated
	 */
	EReference getBPELTargetList_Targets();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.vxbpel.BPELTarget <em>BPEL Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>BPEL Target</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELTarget
	 * @generated
	 */
	EClass getBPELTarget();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.vxbpel.BPELTarget#getLinkName <em>Link Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Link Name</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELTarget#getLinkName()
	 * @see #getBPELTarget()
	 * @generated
	 */
	EAttribute getBPELTarget_LinkName();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.vxbpel.BPELSourceList <em>BPEL Source List</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>BPEL Source List</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELSourceList
	 * @generated
	 */
	EClass getBPELSourceList();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.ustb.sei.mde.vxbpel.BPELSourceList#getSources <em>Sources</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Sources</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELSourceList#getSources()
	 * @see #getBPELSourceList()
	 * @generated
	 */
	EReference getBPELSourceList_Sources();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.vxbpel.BPELSource <em>BPEL Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>BPEL Source</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELSource
	 * @generated
	 */
	EClass getBPELSource();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.vxbpel.BPELSource#getLinkName <em>Link Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Link Name</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELSource#getLinkName()
	 * @see #getBPELSource()
	 * @generated
	 */
	EAttribute getBPELSource_LinkName();

	/**
	 * Returns the meta object for the containment reference '{@link edu.ustb.sei.mde.vxbpel.BPELSource#getTransitionCondition <em>Transition Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Transition Condition</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELSource#getTransitionCondition()
	 * @see #getBPELSource()
	 * @generated
	 */
	EReference getBPELSource_TransitionCondition();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.vxbpel.BPELInvoke <em>BPEL Invoke</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>BPEL Invoke</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELInvoke
	 * @generated
	 */
	EClass getBPELInvoke();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.vxbpel.BPELInvoke#getPartnerLink <em>Partner Link</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Partner Link</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELInvoke#getPartnerLink()
	 * @see #getBPELInvoke()
	 * @generated
	 */
	EAttribute getBPELInvoke_PartnerLink();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.vxbpel.BPELInvoke#getPortType <em>Port Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Port Type</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELInvoke#getPortType()
	 * @see #getBPELInvoke()
	 * @generated
	 */
	EAttribute getBPELInvoke_PortType();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.vxbpel.BPELInvoke#getOperation <em>Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Operation</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELInvoke#getOperation()
	 * @see #getBPELInvoke()
	 * @generated
	 */
	EAttribute getBPELInvoke_Operation();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.vxbpel.BPELInvoke#getInputVariable <em>Input Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Input Variable</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELInvoke#getInputVariable()
	 * @see #getBPELInvoke()
	 * @generated
	 */
	EAttribute getBPELInvoke_InputVariable();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.vxbpel.BPELInvoke#getOutputVariable <em>Output Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Output Variable</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELInvoke#getOutputVariable()
	 * @see #getBPELInvoke()
	 * @generated
	 */
	EAttribute getBPELInvoke_OutputVariable();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.vxbpel.BPELReceive <em>BPEL Receive</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>BPEL Receive</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELReceive
	 * @generated
	 */
	EClass getBPELReceive();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.vxbpel.BPELReceive#getPartnerLink <em>Partner Link</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Partner Link</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELReceive#getPartnerLink()
	 * @see #getBPELReceive()
	 * @generated
	 */
	EAttribute getBPELReceive_PartnerLink();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.vxbpel.BPELReceive#getPortType <em>Port Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Port Type</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELReceive#getPortType()
	 * @see #getBPELReceive()
	 * @generated
	 */
	EAttribute getBPELReceive_PortType();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.vxbpel.BPELReceive#getOperation <em>Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Operation</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELReceive#getOperation()
	 * @see #getBPELReceive()
	 * @generated
	 */
	EAttribute getBPELReceive_Operation();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.vxbpel.BPELReceive#getVariable <em>Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Variable</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELReceive#getVariable()
	 * @see #getBPELReceive()
	 * @generated
	 */
	EAttribute getBPELReceive_Variable();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.vxbpel.BPELReceive#getCreateInstance <em>Create Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Create Instance</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELReceive#getCreateInstance()
	 * @see #getBPELReceive()
	 * @generated
	 */
	EAttribute getBPELReceive_CreateInstance();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.vxbpel.BPELReceive#getMessageExchange <em>Message Exchange</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Message Exchange</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELReceive#getMessageExchange()
	 * @see #getBPELReceive()
	 * @generated
	 */
	EAttribute getBPELReceive_MessageExchange();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.vxbpel.BPELReply <em>BPEL Reply</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>BPEL Reply</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELReply
	 * @generated
	 */
	EClass getBPELReply();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.vxbpel.BPELReply#getPartnerLink <em>Partner Link</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Partner Link</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELReply#getPartnerLink()
	 * @see #getBPELReply()
	 * @generated
	 */
	EAttribute getBPELReply_PartnerLink();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.vxbpel.BPELReply#getPortType <em>Port Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Port Type</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELReply#getPortType()
	 * @see #getBPELReply()
	 * @generated
	 */
	EAttribute getBPELReply_PortType();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.vxbpel.BPELReply#getOperation <em>Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Operation</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELReply#getOperation()
	 * @see #getBPELReply()
	 * @generated
	 */
	EAttribute getBPELReply_Operation();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.vxbpel.BPELReply#getVariable <em>Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Variable</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELReply#getVariable()
	 * @see #getBPELReply()
	 * @generated
	 */
	EAttribute getBPELReply_Variable();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.vxbpel.BPELReply#getFaultName <em>Fault Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Fault Name</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELReply#getFaultName()
	 * @see #getBPELReply()
	 * @generated
	 */
	EAttribute getBPELReply_FaultName();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.vxbpel.BPELReply#getMessageExchange <em>Message Exchange</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Message Exchange</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELReply#getMessageExchange()
	 * @see #getBPELReply()
	 * @generated
	 */
	EAttribute getBPELReply_MessageExchange();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.vxbpel.BPELAssign <em>BPEL Assign</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>BPEL Assign</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELAssign
	 * @generated
	 */
	EClass getBPELAssign();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.vxbpel.BPELAssign#getValidate <em>Validate</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Validate</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELAssign#getValidate()
	 * @see #getBPELAssign()
	 * @generated
	 */
	EAttribute getBPELAssign_Validate();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.ustb.sei.mde.vxbpel.BPELAssign#getParts <em>Parts</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Parts</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELAssign#getParts()
	 * @see #getBPELAssign()
	 * @generated
	 */
	EReference getBPELAssign_Parts();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.vxbpel.BPELAssignOperation <em>BPEL Assign Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>BPEL Assign Operation</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELAssignOperation
	 * @generated
	 */
	EClass getBPELAssignOperation();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.vxbpel.BPELCopy <em>BPEL Copy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>BPEL Copy</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELCopy
	 * @generated
	 */
	EClass getBPELCopy();

	/**
	 * Returns the meta object for the containment reference '{@link edu.ustb.sei.mde.vxbpel.BPELCopy#getFromSpec <em>From Spec</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>From Spec</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELCopy#getFromSpec()
	 * @see #getBPELCopy()
	 * @generated
	 */
	EReference getBPELCopy_FromSpec();

	/**
	 * Returns the meta object for the containment reference '{@link edu.ustb.sei.mde.vxbpel.BPELCopy#getToSpec <em>To Spec</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>To Spec</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELCopy#getToSpec()
	 * @see #getBPELCopy()
	 * @generated
	 */
	EReference getBPELCopy_ToSpec();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.vxbpel.BPELCopyFrom <em>BPEL Copy From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>BPEL Copy From</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELCopyFrom
	 * @generated
	 */
	EClass getBPELCopyFrom();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.vxbpel.BPELCopyFrom#getVariable <em>Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Variable</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELCopyFrom#getVariable()
	 * @see #getBPELCopyFrom()
	 * @generated
	 */
	EAttribute getBPELCopyFrom_Variable();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.vxbpel.BPELCopyFrom#getPart <em>Part</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Part</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELCopyFrom#getPart()
	 * @see #getBPELCopyFrom()
	 * @generated
	 */
	EAttribute getBPELCopyFrom_Part();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.vxbpel.BPELCopyFrom#getProperty <em>Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Property</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELCopyFrom#getProperty()
	 * @see #getBPELCopyFrom()
	 * @generated
	 */
	EAttribute getBPELCopyFrom_Property();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.vxbpel.BPELCopyFrom#getPartnerLink <em>Partner Link</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Partner Link</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELCopyFrom#getPartnerLink()
	 * @see #getBPELCopyFrom()
	 * @generated
	 */
	EAttribute getBPELCopyFrom_PartnerLink();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.vxbpel.BPELCopyFrom#getEndpointReference <em>Endpoint Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Endpoint Reference</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELCopyFrom#getEndpointReference()
	 * @see #getBPELCopyFrom()
	 * @generated
	 */
	EAttribute getBPELCopyFrom_EndpointReference();

	/**
	 * Returns the meta object for the containment reference '{@link edu.ustb.sei.mde.vxbpel.BPELCopyFrom#getExpression <em>Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Expression</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELCopyFrom#getExpression()
	 * @see #getBPELCopyFrom()
	 * @generated
	 */
	EReference getBPELCopyFrom_Expression();

	/**
	 * Returns the meta object for the containment reference '{@link edu.ustb.sei.mde.vxbpel.BPELCopyFrom#getQuery <em>Query</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Query</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELCopyFrom#getQuery()
	 * @see #getBPELCopyFrom()
	 * @generated
	 */
	EReference getBPELCopyFrom_Query();

	/**
	 * Returns the meta object for the containment reference '{@link edu.ustb.sei.mde.vxbpel.BPELCopyFrom#getLiteral <em>Literal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Literal</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELCopyFrom#getLiteral()
	 * @see #getBPELCopyFrom()
	 * @generated
	 */
	EReference getBPELCopyFrom_Literal();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.vxbpel.BPELCopyTo <em>BPEL Copy To</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>BPEL Copy To</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELCopyTo
	 * @generated
	 */
	EClass getBPELCopyTo();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.vxbpel.BPELCopyTo#getVariable <em>Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Variable</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELCopyTo#getVariable()
	 * @see #getBPELCopyTo()
	 * @generated
	 */
	EAttribute getBPELCopyTo_Variable();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.vxbpel.BPELCopyTo#getPart <em>Part</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Part</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELCopyTo#getPart()
	 * @see #getBPELCopyTo()
	 * @generated
	 */
	EAttribute getBPELCopyTo_Part();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.vxbpel.BPELCopyTo#getProperty <em>Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Property</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELCopyTo#getProperty()
	 * @see #getBPELCopyTo()
	 * @generated
	 */
	EAttribute getBPELCopyTo_Property();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.vxbpel.BPELCopyTo#getPartnerLink <em>Partner Link</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Partner Link</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELCopyTo#getPartnerLink()
	 * @see #getBPELCopyTo()
	 * @generated
	 */
	EAttribute getBPELCopyTo_PartnerLink();

	/**
	 * Returns the meta object for the containment reference '{@link edu.ustb.sei.mde.vxbpel.BPELCopyTo#getExpression <em>Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Expression</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELCopyTo#getExpression()
	 * @see #getBPELCopyTo()
	 * @generated
	 */
	EReference getBPELCopyTo_Expression();

	/**
	 * Returns the meta object for the containment reference '{@link edu.ustb.sei.mde.vxbpel.BPELCopyTo#getQuery <em>Query</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Query</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELCopyTo#getQuery()
	 * @see #getBPELCopyTo()
	 * @generated
	 */
	EReference getBPELCopyTo_Query();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.vxbpel.BPELLiteral <em>BPEL Literal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>BPEL Literal</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELLiteral
	 * @generated
	 */
	EClass getBPELLiteral();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.vxbpel.BPELLiteral#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELLiteral#getType()
	 * @see #getBPELLiteral()
	 * @generated
	 */
	EAttribute getBPELLiteral_Type();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.vxbpel.BPELLiteral#getLiteral <em>Literal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Literal</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELLiteral#getLiteral()
	 * @see #getBPELLiteral()
	 * @generated
	 */
	EAttribute getBPELLiteral_Literal();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.vxbpel.BPELQuery <em>BPEL Query</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>BPEL Query</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELQuery
	 * @generated
	 */
	EClass getBPELQuery();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.vxbpel.BPELQuery#getQueryLanguage <em>Query Language</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Query Language</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELQuery#getQueryLanguage()
	 * @see #getBPELQuery()
	 * @generated
	 */
	EAttribute getBPELQuery_QueryLanguage();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.vxbpel.BPELQuery#getContent <em>Content</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Content</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELQuery#getContent()
	 * @see #getBPELQuery()
	 * @generated
	 */
	EAttribute getBPELQuery_Content();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.vxbpel.BPELThrow <em>BPEL Throw</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>BPEL Throw</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELThrow
	 * @generated
	 */
	EClass getBPELThrow();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.vxbpel.BPELThrow#getFaultName <em>Fault Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Fault Name</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELThrow#getFaultName()
	 * @see #getBPELThrow()
	 * @generated
	 */
	EAttribute getBPELThrow_FaultName();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.vxbpel.BPELThrow#getFaultVariable <em>Fault Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Fault Variable</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELThrow#getFaultVariable()
	 * @see #getBPELThrow()
	 * @generated
	 */
	EAttribute getBPELThrow_FaultVariable();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.vxbpel.BPELRethrow <em>BPEL Rethrow</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>BPEL Rethrow</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELRethrow
	 * @generated
	 */
	EClass getBPELRethrow();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.vxbpel.BPELWait <em>BPEL Wait</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>BPEL Wait</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELWait
	 * @generated
	 */
	EClass getBPELWait();

	/**
	 * Returns the meta object for the containment reference '{@link edu.ustb.sei.mde.vxbpel.BPELWait#getDeadlineExpression <em>Deadline Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Deadline Expression</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELWait#getDeadlineExpression()
	 * @see #getBPELWait()
	 * @generated
	 */
	EReference getBPELWait_DeadlineExpression();

	/**
	 * Returns the meta object for the containment reference '{@link edu.ustb.sei.mde.vxbpel.BPELWait#getDurationExpression <em>Duration Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Duration Expression</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELWait#getDurationExpression()
	 * @see #getBPELWait()
	 * @generated
	 */
	EReference getBPELWait_DurationExpression();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.vxbpel.BPELEmpty <em>BPEL Empty</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>BPEL Empty</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELEmpty
	 * @generated
	 */
	EClass getBPELEmpty();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.vxbpel.BPELExit <em>BPEL Exit</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>BPEL Exit</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELExit
	 * @generated
	 */
	EClass getBPELExit();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.vxbpel.BPELSequence <em>BPEL Sequence</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>BPEL Sequence</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELSequence
	 * @generated
	 */
	EClass getBPELSequence();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.ustb.sei.mde.vxbpel.BPELSequence#getActivities <em>Activities</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Activities</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELSequence#getActivities()
	 * @see #getBPELSequence()
	 * @generated
	 */
	EReference getBPELSequence_Activities();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.vxbpel.BPELIf <em>BPEL If</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>BPEL If</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELIf
	 * @generated
	 */
	EClass getBPELIf();

	/**
	 * Returns the meta object for the containment reference '{@link edu.ustb.sei.mde.vxbpel.BPELIf#getCondition <em>Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Condition</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELIf#getCondition()
	 * @see #getBPELIf()
	 * @generated
	 */
	EReference getBPELIf_Condition();

	/**
	 * Returns the meta object for the containment reference '{@link edu.ustb.sei.mde.vxbpel.BPELIf#getThenActivity <em>Then Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Then Activity</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELIf#getThenActivity()
	 * @see #getBPELIf()
	 * @generated
	 */
	EReference getBPELIf_ThenActivity();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.ustb.sei.mde.vxbpel.BPELIf#getElseIfActivity <em>Else If Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Else If Activity</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELIf#getElseIfActivity()
	 * @see #getBPELIf()
	 * @generated
	 */
	EReference getBPELIf_ElseIfActivity();

	/**
	 * Returns the meta object for the containment reference '{@link edu.ustb.sei.mde.vxbpel.BPELIf#getElseActivity <em>Else Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Else Activity</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELIf#getElseActivity()
	 * @see #getBPELIf()
	 * @generated
	 */
	EReference getBPELIf_ElseActivity();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.vxbpel.BPELElseIf <em>BPEL Else If</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>BPEL Else If</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELElseIf
	 * @generated
	 */
	EClass getBPELElseIf();

	/**
	 * Returns the meta object for the containment reference '{@link edu.ustb.sei.mde.vxbpel.BPELElseIf#getCondition <em>Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Condition</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELElseIf#getCondition()
	 * @see #getBPELElseIf()
	 * @generated
	 */
	EReference getBPELElseIf_Condition();

	/**
	 * Returns the meta object for the containment reference '{@link edu.ustb.sei.mde.vxbpel.BPELElseIf#getThenActivity <em>Then Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Then Activity</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELElseIf#getThenActivity()
	 * @see #getBPELElseIf()
	 * @generated
	 */
	EReference getBPELElseIf_ThenActivity();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.vxbpel.BPELWhile <em>BPEL While</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>BPEL While</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELWhile
	 * @generated
	 */
	EClass getBPELWhile();

	/**
	 * Returns the meta object for the containment reference '{@link edu.ustb.sei.mde.vxbpel.BPELWhile#getCondition <em>Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Condition</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELWhile#getCondition()
	 * @see #getBPELWhile()
	 * @generated
	 */
	EReference getBPELWhile_Condition();

	/**
	 * Returns the meta object for the containment reference '{@link edu.ustb.sei.mde.vxbpel.BPELWhile#getBodyActivity <em>Body Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Body Activity</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELWhile#getBodyActivity()
	 * @see #getBPELWhile()
	 * @generated
	 */
	EReference getBPELWhile_BodyActivity();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.vxbpel.BPELRepeatUntil <em>BPEL Repeat Until</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>BPEL Repeat Until</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELRepeatUntil
	 * @generated
	 */
	EClass getBPELRepeatUntil();

	/**
	 * Returns the meta object for the containment reference '{@link edu.ustb.sei.mde.vxbpel.BPELRepeatUntil#getBodyActivity <em>Body Activity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Body Activity</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELRepeatUntil#getBodyActivity()
	 * @see #getBPELRepeatUntil()
	 * @generated
	 */
	EReference getBPELRepeatUntil_BodyActivity();

	/**
	 * Returns the meta object for the containment reference '{@link edu.ustb.sei.mde.vxbpel.BPELRepeatUntil#getCondition <em>Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Condition</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELRepeatUntil#getCondition()
	 * @see #getBPELRepeatUntil()
	 * @generated
	 */
	EReference getBPELRepeatUntil_Condition();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.vxbpel.BPELPick <em>BPEL Pick</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>BPEL Pick</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELPick
	 * @generated
	 */
	EClass getBPELPick();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.vxbpel.BPELFlow <em>BPEL Flow</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>BPEL Flow</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELFlow
	 * @generated
	 */
	EClass getBPELFlow();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.ustb.sei.mde.vxbpel.BPELFlow#getLinks <em>Links</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Links</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELFlow#getLinks()
	 * @see #getBPELFlow()
	 * @generated
	 */
	EReference getBPELFlow_Links();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.ustb.sei.mde.vxbpel.BPELFlow#getBodyActivities <em>Body Activities</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Body Activities</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELFlow#getBodyActivities()
	 * @see #getBPELFlow()
	 * @generated
	 */
	EReference getBPELFlow_BodyActivities();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.vxbpel.BPELLink <em>BPEL Link</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>BPEL Link</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELLink
	 * @generated
	 */
	EClass getBPELLink();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.vxbpel.BPELForEach <em>BPEL For Each</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>BPEL For Each</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELForEach
	 * @generated
	 */
	EClass getBPELForEach();

	/**
	 * Returns the meta object for the containment reference '{@link edu.ustb.sei.mde.vxbpel.BPELForEach#getStartCounterValue <em>Start Counter Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Start Counter Value</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELForEach#getStartCounterValue()
	 * @see #getBPELForEach()
	 * @generated
	 */
	EReference getBPELForEach_StartCounterValue();

	/**
	 * Returns the meta object for the containment reference '{@link edu.ustb.sei.mde.vxbpel.BPELForEach#getFinalCounterValue <em>Final Counter Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Final Counter Value</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELForEach#getFinalCounterValue()
	 * @see #getBPELForEach()
	 * @generated
	 */
	EReference getBPELForEach_FinalCounterValue();

	/**
	 * Returns the meta object for the containment reference '{@link edu.ustb.sei.mde.vxbpel.BPELForEach#getCompletionCondition <em>Completion Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Completion Condition</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELForEach#getCompletionCondition()
	 * @see #getBPELForEach()
	 * @generated
	 */
	EReference getBPELForEach_CompletionCondition();

	/**
	 * Returns the meta object for the containment reference '{@link edu.ustb.sei.mde.vxbpel.BPELForEach#getScope <em>Scope</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Scope</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELForEach#getScope()
	 * @see #getBPELForEach()
	 * @generated
	 */
	EReference getBPELForEach_Scope();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.vxbpel.BPELScope <em>BPEL Scope</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>BPEL Scope</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELScope
	 * @generated
	 */
	EClass getBPELScope();

	/**
	 * Returns the meta object for the containment reference '{@link edu.ustb.sei.mde.vxbpel.BPELScope#getCompensationHandler <em>Compensation Handler</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Compensation Handler</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELScope#getCompensationHandler()
	 * @see #getBPELScope()
	 * @generated
	 */
	EReference getBPELScope_CompensationHandler();

	/**
	 * Returns the meta object for the containment reference '{@link edu.ustb.sei.mde.vxbpel.BPELScope#getTerminationHandler <em>Termination Handler</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Termination Handler</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.BPELScope#getTerminationHandler()
	 * @see #getBPELScope()
	 * @generated
	 */
	EReference getBPELScope_TerminationHandler();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.vxbpel.VxBPELVariationPoint <em>Variation Point</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Variation Point</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELVariationPoint
	 * @generated
	 */
	EClass getVxBPELVariationPoint();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.ustb.sei.mde.vxbpel.VxBPELVariationPoint#getVariants <em>Variants</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Variants</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELVariationPoint#getVariants()
	 * @see #getVxBPELVariationPoint()
	 * @generated
	 */
	EReference getVxBPELVariationPoint_Variants();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.vxbpel.VxBPELVariant <em>Variant</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Variant</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELVariant
	 * @generated
	 */
	EClass getVxBPELVariant();

	/**
	 * Returns the meta object for the containment reference '{@link edu.ustb.sei.mde.vxbpel.VxBPELVariant#getBpelCode <em>Bpel Code</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Bpel Code</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELVariant#getBpelCode()
	 * @see #getVxBPELVariant()
	 * @generated
	 */
	EReference getVxBPELVariant_BpelCode();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.vxbpel.VxBPELConfigurableVariationPoint <em>Configurable Variation Point</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Configurable Variation Point</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELConfigurableVariationPoint
	 * @generated
	 */
	EClass getVxBPELConfigurableVariationPoint();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.vxbpel.VxBPELConfigurableVariationPoint#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELConfigurableVariationPoint#getId()
	 * @see #getVxBPELConfigurableVariationPoint()
	 * @generated
	 */
	EAttribute getVxBPELConfigurableVariationPoint_Id();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.vxbpel.VxBPELConfigurableVariationPoint#getDefaultVariant <em>Default Variant</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Default Variant</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELConfigurableVariationPoint#getDefaultVariant()
	 * @see #getVxBPELConfigurableVariationPoint()
	 * @generated
	 */
	EAttribute getVxBPELConfigurableVariationPoint_DefaultVariant();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.ustb.sei.mde.vxbpel.VxBPELConfigurableVariationPoint#getVariants <em>Variants</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Variants</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELConfigurableVariationPoint#getVariants()
	 * @see #getVxBPELConfigurableVariationPoint()
	 * @generated
	 */
	EReference getVxBPELConfigurableVariationPoint_Variants();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.vxbpel.VxBPELConfigurableVariant <em>Configurable Variant</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Configurable Variant</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELConfigurableVariant
	 * @generated
	 */
	EClass getVxBPELConfigurableVariant();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.vxbpel.VxBPELConfigurableVariant#getVariantInfo <em>Variant Info</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Variant Info</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELConfigurableVariant#getVariantInfo()
	 * @see #getVxBPELConfigurableVariant()
	 * @generated
	 */
	EAttribute getVxBPELConfigurableVariant_VariantInfo();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.vxbpel.VxBPELConfigurableVariant#getRationale <em>Rationale</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Rationale</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELConfigurableVariant#getRationale()
	 * @see #getVxBPELConfigurableVariant()
	 * @generated
	 */
	EAttribute getVxBPELConfigurableVariant_Rationale();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.ustb.sei.mde.vxbpel.VxBPELConfigurableVariant#getRequiredVariants <em>Required Variants</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Required Variants</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELConfigurableVariant#getRequiredVariants()
	 * @see #getVxBPELConfigurableVariant()
	 * @generated
	 */
	EReference getVxBPELConfigurableVariant_RequiredVariants();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.vxbpel.VxBPELVariationPointPChoice <em>Variation Point PChoice</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Variation Point PChoice</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELVariationPointPChoice
	 * @generated
	 */
	EClass getVxBPELVariationPointPChoice();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.vxbpel.VxBPELVariationPointPChoice#getVpname <em>Vpname</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Vpname</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELVariationPointPChoice#getVpname()
	 * @see #getVxBPELVariationPointPChoice()
	 * @generated
	 */
	EAttribute getVxBPELVariationPointPChoice_Vpname();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.vxbpel.VxBPELVariationPointPChoice#getVariant <em>Variant</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Variant</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELVariationPointPChoice#getVariant()
	 * @see #getVxBPELVariationPointPChoice()
	 * @generated
	 */
	EAttribute getVxBPELVariationPointPChoice_Variant();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.vxbpel.VxBPELProcess <em>Process</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Process</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELProcess
	 * @generated
	 */
	EClass getVxBPELProcess();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.ustb.sei.mde.vxbpel.VxBPELProcess#getConfigurableVariationPoints <em>Configurable Variation Points</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Configurable Variation Points</em>'.
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELProcess#getConfigurableVariationPoints()
	 * @see #getVxBPELProcess()
	 * @generated
	 */
	EReference getVxBPELProcess_ConfigurableVariationPoints();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	VxBPELFactory getVxBPELFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.vxbpel.impl.NamedElementImpl <em>Named Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.vxbpel.impl.NamedElementImpl
		 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getNamedElement()
		 * @generated
		 */
		EClass NAMED_ELEMENT = eINSTANCE.getNamedElement();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NAMED_ELEMENT__NAME = eINSTANCE.getNamedElement_Name();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.vxbpel.impl.XMLAttributeContainerImpl <em>XML Attribute Container</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.vxbpel.impl.XMLAttributeContainerImpl
		 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getXMLAttributeContainer()
		 * @generated
		 */
		EClass XML_ATTRIBUTE_CONTAINER = eINSTANCE.getXMLAttributeContainer();

		/**
		 * The meta object literal for the '<em><b>Attributes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference XML_ATTRIBUTE_CONTAINER__ATTRIBUTES = eINSTANCE.getXMLAttributeContainer_Attributes();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.vxbpel.impl.XMLAttributeImpl <em>XML Attribute</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.vxbpel.impl.XMLAttributeImpl
		 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getXMLAttribute()
		 * @generated
		 */
		EClass XML_ATTRIBUTE = eINSTANCE.getXMLAttribute();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute XML_ATTRIBUTE__NAME = eINSTANCE.getXMLAttribute_Name();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute XML_ATTRIBUTE__VALUE = eINSTANCE.getXMLAttribute_Value();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.vxbpel.impl.BPELExpressionImpl <em>BPEL Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.vxbpel.impl.BPELExpressionImpl
		 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getBPELExpression()
		 * @generated
		 */
		EClass BPEL_EXPRESSION = eINSTANCE.getBPELExpression();

		/**
		 * The meta object literal for the '<em><b>Expression Language</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BPEL_EXPRESSION__EXPRESSION_LANGUAGE = eINSTANCE.getBPELExpression_ExpressionLanguage();

		/**
		 * The meta object literal for the '<em><b>Expression</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BPEL_EXPRESSION__EXPRESSION = eINSTANCE.getBPELExpression_Expression();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.vxbpel.impl.BPELBoolExpressionImpl <em>BPEL Bool Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.vxbpel.impl.BPELBoolExpressionImpl
		 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getBPELBoolExpression()
		 * @generated
		 */
		EClass BPEL_BOOL_EXPRESSION = eINSTANCE.getBPELBoolExpression();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.vxbpel.impl.BPELDeadlineExpressionImpl <em>BPEL Deadline Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.vxbpel.impl.BPELDeadlineExpressionImpl
		 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getBPELDeadlineExpression()
		 * @generated
		 */
		EClass BPEL_DEADLINE_EXPRESSION = eINSTANCE.getBPELDeadlineExpression();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.vxbpel.impl.BPELDurationExpressionImpl <em>BPEL Duration Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.vxbpel.impl.BPELDurationExpressionImpl
		 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getBPELDurationExpression()
		 * @generated
		 */
		EClass BPEL_DURATION_EXPRESSION = eINSTANCE.getBPELDurationExpression();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.vxbpel.impl.BPELUnsignedIntegerExpressionImpl <em>BPEL Unsigned Integer Expression</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.vxbpel.impl.BPELUnsignedIntegerExpressionImpl
		 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getBPELUnsignedIntegerExpression()
		 * @generated
		 */
		EClass BPEL_UNSIGNED_INTEGER_EXPRESSION = eINSTANCE.getBPELUnsignedIntegerExpression();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.vxbpel.impl.BPELScopeElementImpl <em>BPEL Scope Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.vxbpel.impl.BPELScopeElementImpl
		 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getBPELScopeElement()
		 * @generated
		 */
		EClass BPEL_SCOPE_ELEMENT = eINSTANCE.getBPELScopeElement();

		/**
		 * The meta object literal for the '<em><b>Exit On Standard Fault</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BPEL_SCOPE_ELEMENT__EXIT_ON_STANDARD_FAULT = eINSTANCE.getBPELScopeElement_ExitOnStandardFault();

		/**
		 * The meta object literal for the '<em><b>Partner Links</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BPEL_SCOPE_ELEMENT__PARTNER_LINKS = eINSTANCE.getBPELScopeElement_PartnerLinks();

		/**
		 * The meta object literal for the '<em><b>Message Exchanges</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BPEL_SCOPE_ELEMENT__MESSAGE_EXCHANGES = eINSTANCE.getBPELScopeElement_MessageExchanges();

		/**
		 * The meta object literal for the '<em><b>Variables</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BPEL_SCOPE_ELEMENT__VARIABLES = eINSTANCE.getBPELScopeElement_Variables();

		/**
		 * The meta object literal for the '<em><b>Correlation Sets</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BPEL_SCOPE_ELEMENT__CORRELATION_SETS = eINSTANCE.getBPELScopeElement_CorrelationSets();

		/**
		 * The meta object literal for the '<em><b>Fault Handler List</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BPEL_SCOPE_ELEMENT__FAULT_HANDLER_LIST = eINSTANCE.getBPELScopeElement_FaultHandlerList();

		/**
		 * The meta object literal for the '<em><b>Event Handler List</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BPEL_SCOPE_ELEMENT__EVENT_HANDLER_LIST = eINSTANCE.getBPELScopeElement_EventHandlerList();

		/**
		 * The meta object literal for the '<em><b>Activity</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BPEL_SCOPE_ELEMENT__ACTIVITY = eINSTANCE.getBPELScopeElement_Activity();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.vxbpel.impl.BPELProcessImpl <em>BPEL Process</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.vxbpel.impl.BPELProcessImpl
		 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getBPELProcess()
		 * @generated
		 */
		EClass BPEL_PROCESS = eINSTANCE.getBPELProcess();

		/**
		 * The meta object literal for the '<em><b>Target Namespace</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BPEL_PROCESS__TARGET_NAMESPACE = eINSTANCE.getBPELProcess_TargetNamespace();

		/**
		 * The meta object literal for the '<em><b>Query Language</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BPEL_PROCESS__QUERY_LANGUAGE = eINSTANCE.getBPELProcess_QueryLanguage();

		/**
		 * The meta object literal for the '<em><b>Expression Language</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BPEL_PROCESS__EXPRESSION_LANGUAGE = eINSTANCE.getBPELProcess_ExpressionLanguage();

		/**
		 * The meta object literal for the '<em><b>Imports</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BPEL_PROCESS__IMPORTS = eINSTANCE.getBPELProcess_Imports();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.vxbpel.impl.BPELImportImpl <em>BPEL Import</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.vxbpel.impl.BPELImportImpl
		 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getBPELImport()
		 * @generated
		 */
		EClass BPEL_IMPORT = eINSTANCE.getBPELImport();

		/**
		 * The meta object literal for the '<em><b>Namespace</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BPEL_IMPORT__NAMESPACE = eINSTANCE.getBPELImport_Namespace();

		/**
		 * The meta object literal for the '<em><b>Location</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BPEL_IMPORT__LOCATION = eINSTANCE.getBPELImport_Location();

		/**
		 * The meta object literal for the '<em><b>Import Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BPEL_IMPORT__IMPORT_TYPE = eINSTANCE.getBPELImport_ImportType();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.vxbpel.impl.BPELPartnerLinkImpl <em>BPEL Partner Link</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.vxbpel.impl.BPELPartnerLinkImpl
		 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getBPELPartnerLink()
		 * @generated
		 */
		EClass BPEL_PARTNER_LINK = eINSTANCE.getBPELPartnerLink();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BPEL_PARTNER_LINK__NAME = eINSTANCE.getBPELPartnerLink_Name();

		/**
		 * The meta object literal for the '<em><b>Partner Link Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BPEL_PARTNER_LINK__PARTNER_LINK_TYPE = eINSTANCE.getBPELPartnerLink_PartnerLinkType();

		/**
		 * The meta object literal for the '<em><b>My Role</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BPEL_PARTNER_LINK__MY_ROLE = eINSTANCE.getBPELPartnerLink_MyRole();

		/**
		 * The meta object literal for the '<em><b>Partner Role</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BPEL_PARTNER_LINK__PARTNER_ROLE = eINSTANCE.getBPELPartnerLink_PartnerRole();

		/**
		 * The meta object literal for the '<em><b>Initialize Partner Role</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BPEL_PARTNER_LINK__INITIALIZE_PARTNER_ROLE = eINSTANCE.getBPELPartnerLink_InitializePartnerRole();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.vxbpel.impl.BPELMessageExchangeImpl <em>BPEL Message Exchange</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.vxbpel.impl.BPELMessageExchangeImpl
		 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getBPELMessageExchange()
		 * @generated
		 */
		EClass BPEL_MESSAGE_EXCHANGE = eINSTANCE.getBPELMessageExchange();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BPEL_MESSAGE_EXCHANGE__NAME = eINSTANCE.getBPELMessageExchange_Name();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.vxbpel.impl.BPELVariableImpl <em>BPEL Variable</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.vxbpel.impl.BPELVariableImpl
		 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getBPELVariable()
		 * @generated
		 */
		EClass BPEL_VARIABLE = eINSTANCE.getBPELVariable();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BPEL_VARIABLE__NAME = eINSTANCE.getBPELVariable_Name();

		/**
		 * The meta object literal for the '<em><b>Message Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BPEL_VARIABLE__MESSAGE_TYPE = eINSTANCE.getBPELVariable_MessageType();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BPEL_VARIABLE__TYPE = eINSTANCE.getBPELVariable_Type();

		/**
		 * The meta object literal for the '<em><b>Element</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BPEL_VARIABLE__ELEMENT = eINSTANCE.getBPELVariable_Element();

		/**
		 * The meta object literal for the '<em><b>From Spec</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BPEL_VARIABLE__FROM_SPEC = eINSTANCE.getBPELVariable_FromSpec();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.vxbpel.impl.BPELCorrelationSetImpl <em>BPEL Correlation Set</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.vxbpel.impl.BPELCorrelationSetImpl
		 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getBPELCorrelationSet()
		 * @generated
		 */
		EClass BPEL_CORRELATION_SET = eINSTANCE.getBPELCorrelationSet();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BPEL_CORRELATION_SET__NAME = eINSTANCE.getBPELCorrelationSet_Name();

		/**
		 * The meta object literal for the '<em><b>Properties</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BPEL_CORRELATION_SET__PROPERTIES = eINSTANCE.getBPELCorrelationSet_Properties();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.vxbpel.impl.BPELActivityImpl <em>BPEL Activity</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.vxbpel.impl.BPELActivityImpl
		 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getBPELActivity()
		 * @generated
		 */
		EClass BPEL_ACTIVITY = eINSTANCE.getBPELActivity();

		/**
		 * The meta object literal for the '<em><b>Target Set</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BPEL_ACTIVITY__TARGET_SET = eINSTANCE.getBPELActivity_TargetSet();

		/**
		 * The meta object literal for the '<em><b>Source Set</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BPEL_ACTIVITY__SOURCE_SET = eINSTANCE.getBPELActivity_SourceSet();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.vxbpel.impl.BPELTargetListImpl <em>BPEL Target List</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.vxbpel.impl.BPELTargetListImpl
		 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getBPELTargetList()
		 * @generated
		 */
		EClass BPEL_TARGET_LIST = eINSTANCE.getBPELTargetList();

		/**
		 * The meta object literal for the '<em><b>Join Condition</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BPEL_TARGET_LIST__JOIN_CONDITION = eINSTANCE.getBPELTargetList_JoinCondition();

		/**
		 * The meta object literal for the '<em><b>Targets</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BPEL_TARGET_LIST__TARGETS = eINSTANCE.getBPELTargetList_Targets();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.vxbpel.impl.BPELTargetImpl <em>BPEL Target</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.vxbpel.impl.BPELTargetImpl
		 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getBPELTarget()
		 * @generated
		 */
		EClass BPEL_TARGET = eINSTANCE.getBPELTarget();

		/**
		 * The meta object literal for the '<em><b>Link Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BPEL_TARGET__LINK_NAME = eINSTANCE.getBPELTarget_LinkName();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.vxbpel.impl.BPELSourceListImpl <em>BPEL Source List</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.vxbpel.impl.BPELSourceListImpl
		 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getBPELSourceList()
		 * @generated
		 */
		EClass BPEL_SOURCE_LIST = eINSTANCE.getBPELSourceList();

		/**
		 * The meta object literal for the '<em><b>Sources</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BPEL_SOURCE_LIST__SOURCES = eINSTANCE.getBPELSourceList_Sources();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.vxbpel.impl.BPELSourceImpl <em>BPEL Source</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.vxbpel.impl.BPELSourceImpl
		 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getBPELSource()
		 * @generated
		 */
		EClass BPEL_SOURCE = eINSTANCE.getBPELSource();

		/**
		 * The meta object literal for the '<em><b>Link Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BPEL_SOURCE__LINK_NAME = eINSTANCE.getBPELSource_LinkName();

		/**
		 * The meta object literal for the '<em><b>Transition Condition</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BPEL_SOURCE__TRANSITION_CONDITION = eINSTANCE.getBPELSource_TransitionCondition();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.vxbpel.impl.BPELInvokeImpl <em>BPEL Invoke</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.vxbpel.impl.BPELInvokeImpl
		 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getBPELInvoke()
		 * @generated
		 */
		EClass BPEL_INVOKE = eINSTANCE.getBPELInvoke();

		/**
		 * The meta object literal for the '<em><b>Partner Link</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BPEL_INVOKE__PARTNER_LINK = eINSTANCE.getBPELInvoke_PartnerLink();

		/**
		 * The meta object literal for the '<em><b>Port Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BPEL_INVOKE__PORT_TYPE = eINSTANCE.getBPELInvoke_PortType();

		/**
		 * The meta object literal for the '<em><b>Operation</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BPEL_INVOKE__OPERATION = eINSTANCE.getBPELInvoke_Operation();

		/**
		 * The meta object literal for the '<em><b>Input Variable</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BPEL_INVOKE__INPUT_VARIABLE = eINSTANCE.getBPELInvoke_InputVariable();

		/**
		 * The meta object literal for the '<em><b>Output Variable</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BPEL_INVOKE__OUTPUT_VARIABLE = eINSTANCE.getBPELInvoke_OutputVariable();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.vxbpel.impl.BPELReceiveImpl <em>BPEL Receive</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.vxbpel.impl.BPELReceiveImpl
		 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getBPELReceive()
		 * @generated
		 */
		EClass BPEL_RECEIVE = eINSTANCE.getBPELReceive();

		/**
		 * The meta object literal for the '<em><b>Partner Link</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BPEL_RECEIVE__PARTNER_LINK = eINSTANCE.getBPELReceive_PartnerLink();

		/**
		 * The meta object literal for the '<em><b>Port Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BPEL_RECEIVE__PORT_TYPE = eINSTANCE.getBPELReceive_PortType();

		/**
		 * The meta object literal for the '<em><b>Operation</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BPEL_RECEIVE__OPERATION = eINSTANCE.getBPELReceive_Operation();

		/**
		 * The meta object literal for the '<em><b>Variable</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BPEL_RECEIVE__VARIABLE = eINSTANCE.getBPELReceive_Variable();

		/**
		 * The meta object literal for the '<em><b>Create Instance</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BPEL_RECEIVE__CREATE_INSTANCE = eINSTANCE.getBPELReceive_CreateInstance();

		/**
		 * The meta object literal for the '<em><b>Message Exchange</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BPEL_RECEIVE__MESSAGE_EXCHANGE = eINSTANCE.getBPELReceive_MessageExchange();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.vxbpel.impl.BPELReplyImpl <em>BPEL Reply</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.vxbpel.impl.BPELReplyImpl
		 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getBPELReply()
		 * @generated
		 */
		EClass BPEL_REPLY = eINSTANCE.getBPELReply();

		/**
		 * The meta object literal for the '<em><b>Partner Link</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BPEL_REPLY__PARTNER_LINK = eINSTANCE.getBPELReply_PartnerLink();

		/**
		 * The meta object literal for the '<em><b>Port Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BPEL_REPLY__PORT_TYPE = eINSTANCE.getBPELReply_PortType();

		/**
		 * The meta object literal for the '<em><b>Operation</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BPEL_REPLY__OPERATION = eINSTANCE.getBPELReply_Operation();

		/**
		 * The meta object literal for the '<em><b>Variable</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BPEL_REPLY__VARIABLE = eINSTANCE.getBPELReply_Variable();

		/**
		 * The meta object literal for the '<em><b>Fault Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BPEL_REPLY__FAULT_NAME = eINSTANCE.getBPELReply_FaultName();

		/**
		 * The meta object literal for the '<em><b>Message Exchange</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BPEL_REPLY__MESSAGE_EXCHANGE = eINSTANCE.getBPELReply_MessageExchange();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.vxbpel.impl.BPELAssignImpl <em>BPEL Assign</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.vxbpel.impl.BPELAssignImpl
		 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getBPELAssign()
		 * @generated
		 */
		EClass BPEL_ASSIGN = eINSTANCE.getBPELAssign();

		/**
		 * The meta object literal for the '<em><b>Validate</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BPEL_ASSIGN__VALIDATE = eINSTANCE.getBPELAssign_Validate();

		/**
		 * The meta object literal for the '<em><b>Parts</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BPEL_ASSIGN__PARTS = eINSTANCE.getBPELAssign_Parts();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.vxbpel.impl.BPELAssignOperationImpl <em>BPEL Assign Operation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.vxbpel.impl.BPELAssignOperationImpl
		 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getBPELAssignOperation()
		 * @generated
		 */
		EClass BPEL_ASSIGN_OPERATION = eINSTANCE.getBPELAssignOperation();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.vxbpel.impl.BPELCopyImpl <em>BPEL Copy</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.vxbpel.impl.BPELCopyImpl
		 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getBPELCopy()
		 * @generated
		 */
		EClass BPEL_COPY = eINSTANCE.getBPELCopy();

		/**
		 * The meta object literal for the '<em><b>From Spec</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BPEL_COPY__FROM_SPEC = eINSTANCE.getBPELCopy_FromSpec();

		/**
		 * The meta object literal for the '<em><b>To Spec</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BPEL_COPY__TO_SPEC = eINSTANCE.getBPELCopy_ToSpec();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.vxbpel.impl.BPELCopyFromImpl <em>BPEL Copy From</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.vxbpel.impl.BPELCopyFromImpl
		 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getBPELCopyFrom()
		 * @generated
		 */
		EClass BPEL_COPY_FROM = eINSTANCE.getBPELCopyFrom();

		/**
		 * The meta object literal for the '<em><b>Variable</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BPEL_COPY_FROM__VARIABLE = eINSTANCE.getBPELCopyFrom_Variable();

		/**
		 * The meta object literal for the '<em><b>Part</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BPEL_COPY_FROM__PART = eINSTANCE.getBPELCopyFrom_Part();

		/**
		 * The meta object literal for the '<em><b>Property</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BPEL_COPY_FROM__PROPERTY = eINSTANCE.getBPELCopyFrom_Property();

		/**
		 * The meta object literal for the '<em><b>Partner Link</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BPEL_COPY_FROM__PARTNER_LINK = eINSTANCE.getBPELCopyFrom_PartnerLink();

		/**
		 * The meta object literal for the '<em><b>Endpoint Reference</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BPEL_COPY_FROM__ENDPOINT_REFERENCE = eINSTANCE.getBPELCopyFrom_EndpointReference();

		/**
		 * The meta object literal for the '<em><b>Expression</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BPEL_COPY_FROM__EXPRESSION = eINSTANCE.getBPELCopyFrom_Expression();

		/**
		 * The meta object literal for the '<em><b>Query</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BPEL_COPY_FROM__QUERY = eINSTANCE.getBPELCopyFrom_Query();

		/**
		 * The meta object literal for the '<em><b>Literal</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BPEL_COPY_FROM__LITERAL = eINSTANCE.getBPELCopyFrom_Literal();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.vxbpel.impl.BPELCopyToImpl <em>BPEL Copy To</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.vxbpel.impl.BPELCopyToImpl
		 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getBPELCopyTo()
		 * @generated
		 */
		EClass BPEL_COPY_TO = eINSTANCE.getBPELCopyTo();

		/**
		 * The meta object literal for the '<em><b>Variable</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BPEL_COPY_TO__VARIABLE = eINSTANCE.getBPELCopyTo_Variable();

		/**
		 * The meta object literal for the '<em><b>Part</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BPEL_COPY_TO__PART = eINSTANCE.getBPELCopyTo_Part();

		/**
		 * The meta object literal for the '<em><b>Property</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BPEL_COPY_TO__PROPERTY = eINSTANCE.getBPELCopyTo_Property();

		/**
		 * The meta object literal for the '<em><b>Partner Link</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BPEL_COPY_TO__PARTNER_LINK = eINSTANCE.getBPELCopyTo_PartnerLink();

		/**
		 * The meta object literal for the '<em><b>Expression</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BPEL_COPY_TO__EXPRESSION = eINSTANCE.getBPELCopyTo_Expression();

		/**
		 * The meta object literal for the '<em><b>Query</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BPEL_COPY_TO__QUERY = eINSTANCE.getBPELCopyTo_Query();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.vxbpel.impl.BPELLiteralImpl <em>BPEL Literal</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.vxbpel.impl.BPELLiteralImpl
		 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getBPELLiteral()
		 * @generated
		 */
		EClass BPEL_LITERAL = eINSTANCE.getBPELLiteral();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BPEL_LITERAL__TYPE = eINSTANCE.getBPELLiteral_Type();

		/**
		 * The meta object literal for the '<em><b>Literal</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BPEL_LITERAL__LITERAL = eINSTANCE.getBPELLiteral_Literal();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.vxbpel.impl.BPELQueryImpl <em>BPEL Query</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.vxbpel.impl.BPELQueryImpl
		 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getBPELQuery()
		 * @generated
		 */
		EClass BPEL_QUERY = eINSTANCE.getBPELQuery();

		/**
		 * The meta object literal for the '<em><b>Query Language</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BPEL_QUERY__QUERY_LANGUAGE = eINSTANCE.getBPELQuery_QueryLanguage();

		/**
		 * The meta object literal for the '<em><b>Content</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BPEL_QUERY__CONTENT = eINSTANCE.getBPELQuery_Content();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.vxbpel.impl.BPELThrowImpl <em>BPEL Throw</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.vxbpel.impl.BPELThrowImpl
		 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getBPELThrow()
		 * @generated
		 */
		EClass BPEL_THROW = eINSTANCE.getBPELThrow();

		/**
		 * The meta object literal for the '<em><b>Fault Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BPEL_THROW__FAULT_NAME = eINSTANCE.getBPELThrow_FaultName();

		/**
		 * The meta object literal for the '<em><b>Fault Variable</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BPEL_THROW__FAULT_VARIABLE = eINSTANCE.getBPELThrow_FaultVariable();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.vxbpel.impl.BPELRethrowImpl <em>BPEL Rethrow</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.vxbpel.impl.BPELRethrowImpl
		 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getBPELRethrow()
		 * @generated
		 */
		EClass BPEL_RETHROW = eINSTANCE.getBPELRethrow();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.vxbpel.impl.BPELWaitImpl <em>BPEL Wait</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.vxbpel.impl.BPELWaitImpl
		 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getBPELWait()
		 * @generated
		 */
		EClass BPEL_WAIT = eINSTANCE.getBPELWait();

		/**
		 * The meta object literal for the '<em><b>Deadline Expression</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BPEL_WAIT__DEADLINE_EXPRESSION = eINSTANCE.getBPELWait_DeadlineExpression();

		/**
		 * The meta object literal for the '<em><b>Duration Expression</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BPEL_WAIT__DURATION_EXPRESSION = eINSTANCE.getBPELWait_DurationExpression();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.vxbpel.impl.BPELEmptyImpl <em>BPEL Empty</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.vxbpel.impl.BPELEmptyImpl
		 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getBPELEmpty()
		 * @generated
		 */
		EClass BPEL_EMPTY = eINSTANCE.getBPELEmpty();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.vxbpel.impl.BPELExitImpl <em>BPEL Exit</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.vxbpel.impl.BPELExitImpl
		 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getBPELExit()
		 * @generated
		 */
		EClass BPEL_EXIT = eINSTANCE.getBPELExit();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.vxbpel.impl.BPELSequenceImpl <em>BPEL Sequence</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.vxbpel.impl.BPELSequenceImpl
		 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getBPELSequence()
		 * @generated
		 */
		EClass BPEL_SEQUENCE = eINSTANCE.getBPELSequence();

		/**
		 * The meta object literal for the '<em><b>Activities</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BPEL_SEQUENCE__ACTIVITIES = eINSTANCE.getBPELSequence_Activities();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.vxbpel.impl.BPELIfImpl <em>BPEL If</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.vxbpel.impl.BPELIfImpl
		 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getBPELIf()
		 * @generated
		 */
		EClass BPEL_IF = eINSTANCE.getBPELIf();

		/**
		 * The meta object literal for the '<em><b>Condition</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BPEL_IF__CONDITION = eINSTANCE.getBPELIf_Condition();

		/**
		 * The meta object literal for the '<em><b>Then Activity</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BPEL_IF__THEN_ACTIVITY = eINSTANCE.getBPELIf_ThenActivity();

		/**
		 * The meta object literal for the '<em><b>Else If Activity</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BPEL_IF__ELSE_IF_ACTIVITY = eINSTANCE.getBPELIf_ElseIfActivity();

		/**
		 * The meta object literal for the '<em><b>Else Activity</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BPEL_IF__ELSE_ACTIVITY = eINSTANCE.getBPELIf_ElseActivity();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.vxbpel.impl.BPELElseIfImpl <em>BPEL Else If</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.vxbpel.impl.BPELElseIfImpl
		 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getBPELElseIf()
		 * @generated
		 */
		EClass BPEL_ELSE_IF = eINSTANCE.getBPELElseIf();

		/**
		 * The meta object literal for the '<em><b>Condition</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BPEL_ELSE_IF__CONDITION = eINSTANCE.getBPELElseIf_Condition();

		/**
		 * The meta object literal for the '<em><b>Then Activity</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BPEL_ELSE_IF__THEN_ACTIVITY = eINSTANCE.getBPELElseIf_ThenActivity();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.vxbpel.impl.BPELWhileImpl <em>BPEL While</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.vxbpel.impl.BPELWhileImpl
		 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getBPELWhile()
		 * @generated
		 */
		EClass BPEL_WHILE = eINSTANCE.getBPELWhile();

		/**
		 * The meta object literal for the '<em><b>Condition</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BPEL_WHILE__CONDITION = eINSTANCE.getBPELWhile_Condition();

		/**
		 * The meta object literal for the '<em><b>Body Activity</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BPEL_WHILE__BODY_ACTIVITY = eINSTANCE.getBPELWhile_BodyActivity();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.vxbpel.impl.BPELRepeatUntilImpl <em>BPEL Repeat Until</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.vxbpel.impl.BPELRepeatUntilImpl
		 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getBPELRepeatUntil()
		 * @generated
		 */
		EClass BPEL_REPEAT_UNTIL = eINSTANCE.getBPELRepeatUntil();

		/**
		 * The meta object literal for the '<em><b>Body Activity</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BPEL_REPEAT_UNTIL__BODY_ACTIVITY = eINSTANCE.getBPELRepeatUntil_BodyActivity();

		/**
		 * The meta object literal for the '<em><b>Condition</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BPEL_REPEAT_UNTIL__CONDITION = eINSTANCE.getBPELRepeatUntil_Condition();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.vxbpel.impl.BPELPickImpl <em>BPEL Pick</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.vxbpel.impl.BPELPickImpl
		 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getBPELPick()
		 * @generated
		 */
		EClass BPEL_PICK = eINSTANCE.getBPELPick();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.vxbpel.impl.BPELFlowImpl <em>BPEL Flow</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.vxbpel.impl.BPELFlowImpl
		 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getBPELFlow()
		 * @generated
		 */
		EClass BPEL_FLOW = eINSTANCE.getBPELFlow();

		/**
		 * The meta object literal for the '<em><b>Links</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BPEL_FLOW__LINKS = eINSTANCE.getBPELFlow_Links();

		/**
		 * The meta object literal for the '<em><b>Body Activities</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BPEL_FLOW__BODY_ACTIVITIES = eINSTANCE.getBPELFlow_BodyActivities();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.vxbpel.impl.BPELLinkImpl <em>BPEL Link</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.vxbpel.impl.BPELLinkImpl
		 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getBPELLink()
		 * @generated
		 */
		EClass BPEL_LINK = eINSTANCE.getBPELLink();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.vxbpel.impl.BPELForEachImpl <em>BPEL For Each</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.vxbpel.impl.BPELForEachImpl
		 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getBPELForEach()
		 * @generated
		 */
		EClass BPEL_FOR_EACH = eINSTANCE.getBPELForEach();

		/**
		 * The meta object literal for the '<em><b>Start Counter Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BPEL_FOR_EACH__START_COUNTER_VALUE = eINSTANCE.getBPELForEach_StartCounterValue();

		/**
		 * The meta object literal for the '<em><b>Final Counter Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BPEL_FOR_EACH__FINAL_COUNTER_VALUE = eINSTANCE.getBPELForEach_FinalCounterValue();

		/**
		 * The meta object literal for the '<em><b>Completion Condition</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BPEL_FOR_EACH__COMPLETION_CONDITION = eINSTANCE.getBPELForEach_CompletionCondition();

		/**
		 * The meta object literal for the '<em><b>Scope</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BPEL_FOR_EACH__SCOPE = eINSTANCE.getBPELForEach_Scope();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.vxbpel.impl.BPELScopeImpl <em>BPEL Scope</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.vxbpel.impl.BPELScopeImpl
		 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getBPELScope()
		 * @generated
		 */
		EClass BPEL_SCOPE = eINSTANCE.getBPELScope();

		/**
		 * The meta object literal for the '<em><b>Compensation Handler</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BPEL_SCOPE__COMPENSATION_HANDLER = eINSTANCE.getBPELScope_CompensationHandler();

		/**
		 * The meta object literal for the '<em><b>Termination Handler</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BPEL_SCOPE__TERMINATION_HANDLER = eINSTANCE.getBPELScope_TerminationHandler();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.vxbpel.impl.VxBPELVariationPointImpl <em>Variation Point</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELVariationPointImpl
		 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getVxBPELVariationPoint()
		 * @generated
		 */
		EClass VX_BPEL_VARIATION_POINT = eINSTANCE.getVxBPELVariationPoint();

		/**
		 * The meta object literal for the '<em><b>Variants</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VX_BPEL_VARIATION_POINT__VARIANTS = eINSTANCE.getVxBPELVariationPoint_Variants();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.vxbpel.impl.VxBPELVariantImpl <em>Variant</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELVariantImpl
		 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getVxBPELVariant()
		 * @generated
		 */
		EClass VX_BPEL_VARIANT = eINSTANCE.getVxBPELVariant();

		/**
		 * The meta object literal for the '<em><b>Bpel Code</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VX_BPEL_VARIANT__BPEL_CODE = eINSTANCE.getVxBPELVariant_BpelCode();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.vxbpel.impl.VxBPELConfigurableVariationPointImpl <em>Configurable Variation Point</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELConfigurableVariationPointImpl
		 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getVxBPELConfigurableVariationPoint()
		 * @generated
		 */
		EClass VX_BPEL_CONFIGURABLE_VARIATION_POINT = eINSTANCE.getVxBPELConfigurableVariationPoint();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VX_BPEL_CONFIGURABLE_VARIATION_POINT__ID = eINSTANCE.getVxBPELConfigurableVariationPoint_Id();

		/**
		 * The meta object literal for the '<em><b>Default Variant</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VX_BPEL_CONFIGURABLE_VARIATION_POINT__DEFAULT_VARIANT = eINSTANCE.getVxBPELConfigurableVariationPoint_DefaultVariant();

		/**
		 * The meta object literal for the '<em><b>Variants</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VX_BPEL_CONFIGURABLE_VARIATION_POINT__VARIANTS = eINSTANCE.getVxBPELConfigurableVariationPoint_Variants();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.vxbpel.impl.VxBPELConfigurableVariantImpl <em>Configurable Variant</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELConfigurableVariantImpl
		 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getVxBPELConfigurableVariant()
		 * @generated
		 */
		EClass VX_BPEL_CONFIGURABLE_VARIANT = eINSTANCE.getVxBPELConfigurableVariant();

		/**
		 * The meta object literal for the '<em><b>Variant Info</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VX_BPEL_CONFIGURABLE_VARIANT__VARIANT_INFO = eINSTANCE.getVxBPELConfigurableVariant_VariantInfo();

		/**
		 * The meta object literal for the '<em><b>Rationale</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VX_BPEL_CONFIGURABLE_VARIANT__RATIONALE = eINSTANCE.getVxBPELConfigurableVariant_Rationale();

		/**
		 * The meta object literal for the '<em><b>Required Variants</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VX_BPEL_CONFIGURABLE_VARIANT__REQUIRED_VARIANTS = eINSTANCE.getVxBPELConfigurableVariant_RequiredVariants();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.vxbpel.impl.VxBPELVariationPointPChoiceImpl <em>Variation Point PChoice</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELVariationPointPChoiceImpl
		 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getVxBPELVariationPointPChoice()
		 * @generated
		 */
		EClass VX_BPEL_VARIATION_POINT_PCHOICE = eINSTANCE.getVxBPELVariationPointPChoice();

		/**
		 * The meta object literal for the '<em><b>Vpname</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VX_BPEL_VARIATION_POINT_PCHOICE__VPNAME = eINSTANCE.getVxBPELVariationPointPChoice_Vpname();

		/**
		 * The meta object literal for the '<em><b>Variant</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VX_BPEL_VARIATION_POINT_PCHOICE__VARIANT = eINSTANCE.getVxBPELVariationPointPChoice_Variant();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.vxbpel.impl.VxBPELProcessImpl <em>Process</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELProcessImpl
		 * @see edu.ustb.sei.mde.vxbpel.impl.VxBPELPackageImpl#getVxBPELProcess()
		 * @generated
		 */
		EClass VX_BPEL_PROCESS = eINSTANCE.getVxBPELProcess();

		/**
		 * The meta object literal for the '<em><b>Configurable Variation Points</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VX_BPEL_PROCESS__CONFIGURABLE_VARIATION_POINTS = eINSTANCE.getVxBPELProcess_ConfigurableVariationPoints();

	}

} //VxBPELPackage
