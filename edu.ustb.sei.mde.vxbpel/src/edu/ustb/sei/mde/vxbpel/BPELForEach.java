/**
 */
package edu.ustb.sei.mde.vxbpel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>BPEL For Each</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.BPELForEach#getStartCounterValue <em>Start Counter Value</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.BPELForEach#getFinalCounterValue <em>Final Counter Value</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.BPELForEach#getCompletionCondition <em>Completion Condition</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.vxbpel.BPELForEach#getScope <em>Scope</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELForEach()
 * @model
 * @generated
 */
public interface BPELForEach extends BPELActivity {
	/**
	 * Returns the value of the '<em><b>Start Counter Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Start Counter Value</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Start Counter Value</em>' containment reference.
	 * @see #setStartCounterValue(BPELUnsignedIntegerExpression)
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELForEach_StartCounterValue()
	 * @model containment="true" required="true"
	 * @generated
	 */
	BPELUnsignedIntegerExpression getStartCounterValue();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.vxbpel.BPELForEach#getStartCounterValue <em>Start Counter Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Start Counter Value</em>' containment reference.
	 * @see #getStartCounterValue()
	 * @generated
	 */
	void setStartCounterValue(BPELUnsignedIntegerExpression value);

	/**
	 * Returns the value of the '<em><b>Final Counter Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Final Counter Value</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Final Counter Value</em>' containment reference.
	 * @see #setFinalCounterValue(BPELUnsignedIntegerExpression)
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELForEach_FinalCounterValue()
	 * @model containment="true" required="true"
	 * @generated
	 */
	BPELUnsignedIntegerExpression getFinalCounterValue();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.vxbpel.BPELForEach#getFinalCounterValue <em>Final Counter Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Final Counter Value</em>' containment reference.
	 * @see #getFinalCounterValue()
	 * @generated
	 */
	void setFinalCounterValue(BPELUnsignedIntegerExpression value);

	/**
	 * Returns the value of the '<em><b>Completion Condition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Completion Condition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Completion Condition</em>' containment reference.
	 * @see #setCompletionCondition(BPELUnsignedIntegerExpression)
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELForEach_CompletionCondition()
	 * @model containment="true"
	 * @generated
	 */
	BPELUnsignedIntegerExpression getCompletionCondition();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.vxbpel.BPELForEach#getCompletionCondition <em>Completion Condition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Completion Condition</em>' containment reference.
	 * @see #getCompletionCondition()
	 * @generated
	 */
	void setCompletionCondition(BPELUnsignedIntegerExpression value);

	/**
	 * Returns the value of the '<em><b>Scope</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scope</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scope</em>' containment reference.
	 * @see #setScope(BPELScope)
	 * @see edu.ustb.sei.mde.vxbpel.VxBPELPackage#getBPELForEach_Scope()
	 * @model containment="true" required="true"
	 * @generated
	 */
	BPELScope getScope();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.vxbpel.BPELForEach#getScope <em>Scope</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Scope</em>' containment reference.
	 * @see #getScope()
	 * @generated
	 */
	void setScope(BPELScope value);

} // BPELForEach
