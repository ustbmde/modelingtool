/**
 */
package edu.ustb.sei.mde.mql.impl;

import edu.ustb.sei.mde.mql.MQLAction;
import edu.ustb.sei.mde.mql.MQLModel;
import edu.ustb.sei.mde.mql.MQLPackage;

import java.util.Collection;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.mql.impl.MQLModelImpl#getActions <em>Actions</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.mql.impl.MQLModelImpl#getDatamodel <em>Datamodel</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class MQLModelImpl extends MinimalEObjectImpl.Container implements MQLModel {
	/**
	 * The cached value of the '{@link #getActions() <em>Actions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActions()
	 * @generated
	 * @ordered
	 */
	protected EList<MQLAction> actions;

	/**
	 * The cached value of the '{@link #getDatamodel() <em>Datamodel</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDatamodel()
	 * @generated
	 * @ordered
	 */
	protected EPackage datamodel;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MQLModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MQLPackage.Literals.MQL_MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MQLAction> getActions() {
		if (actions == null) {
			actions = new EObjectContainmentEList<MQLAction>(MQLAction.class, this, MQLPackage.MQL_MODEL__ACTIONS);
		}
		return actions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EPackage getDatamodel() {
		if (datamodel != null && datamodel.eIsProxy()) {
			InternalEObject oldDatamodel = (InternalEObject)datamodel;
			datamodel = (EPackage)eResolveProxy(oldDatamodel);
			if (datamodel != oldDatamodel) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, MQLPackage.MQL_MODEL__DATAMODEL, oldDatamodel, datamodel));
			}
		}
		return datamodel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EPackage basicGetDatamodel() {
		return datamodel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDatamodel(EPackage newDatamodel) {
		EPackage oldDatamodel = datamodel;
		datamodel = newDatamodel;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MQLPackage.MQL_MODEL__DATAMODEL, oldDatamodel, datamodel));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case MQLPackage.MQL_MODEL__ACTIONS:
				return ((InternalEList<?>)getActions()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case MQLPackage.MQL_MODEL__ACTIONS:
				return getActions();
			case MQLPackage.MQL_MODEL__DATAMODEL:
				if (resolve) return getDatamodel();
				return basicGetDatamodel();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case MQLPackage.MQL_MODEL__ACTIONS:
				getActions().clear();
				getActions().addAll((Collection<? extends MQLAction>)newValue);
				return;
			case MQLPackage.MQL_MODEL__DATAMODEL:
				setDatamodel((EPackage)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case MQLPackage.MQL_MODEL__ACTIONS:
				getActions().clear();
				return;
			case MQLPackage.MQL_MODEL__DATAMODEL:
				setDatamodel((EPackage)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case MQLPackage.MQL_MODEL__ACTIONS:
				return actions != null && !actions.isEmpty();
			case MQLPackage.MQL_MODEL__DATAMODEL:
				return datamodel != null;
		}
		return super.eIsSet(featureID);
	}

} //MQLModelImpl
