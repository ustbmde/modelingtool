/**
 */
package edu.ustb.sei.mde.mql;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Action</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.mql.MQLAction#getParameters <em>Parameters</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.ustb.sei.mde.mql.MQLPackage#getMQLAction()
 * @model abstract="true"
 * @generated
 */
public interface MQLAction extends EObject {
	/**
	 * Returns the value of the '<em><b>Parameters</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parameters</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parameters</em>' attribute list.
	 * @see edu.ustb.sei.mde.mql.MQLPackage#getMQLAction_Parameters()
	 * @model
	 * @generated
	 */
	EList<String> getParameters();

} // MQLAction
