/**
 */
package edu.ustb.sei.mde.mql;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Entity Node</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.mql.EntityNode#getAttributes <em>Attributes</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.mql.EntityNode#getEntity <em>Entity</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.ustb.sei.mde.mql.MQLPackage#getEntityNode()
 * @model
 * @generated
 */
public interface EntityNode extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Entity</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Entity</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Entity</em>' reference.
	 * @see #setEntity(EClass)
	 * @see edu.ustb.sei.mde.mql.MQLPackage#getEntityNode_Entity()
	 * @model
	 * @generated
	 */
	EClass getEntity();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.mql.EntityNode#getEntity <em>Entity</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Entity</em>' reference.
	 * @see #getEntity()
	 * @generated
	 */
	void setEntity(EClass value);

	/**
	 * Returns the value of the '<em><b>Attributes</b></em>' containment reference list.
	 * The list contents are of type {@link edu.ustb.sei.mde.mql.AttributeNode}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Attributes</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attributes</em>' containment reference list.
	 * @see edu.ustb.sei.mde.mql.MQLPackage#getEntityNode_Attributes()
	 * @model containment="true"
	 * @generated
	 */
	EList<AttributeNode> getAttributes();

} // EntityNode
