/**
 */
package edu.ustb.sei.mde.mql;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Delete</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.mql.Delete#getTargets <em>Targets</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.ustb.sei.mde.mql.MQLPackage#getDelete()
 * @model
 * @generated
 */
public interface Delete extends NamedElement, MQLAction {
	/**
	 * Returns the value of the '<em><b>Targets</b></em>' containment reference list.
	 * The list contents are of type {@link edu.ustb.sei.mde.mql.EntityNode}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Targets</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Targets</em>' containment reference list.
	 * @see edu.ustb.sei.mde.mql.MQLPackage#getDelete_Targets()
	 * @model containment="true"
	 * @generated
	 */
	EList<EntityNode> getTargets();

} // Delete
