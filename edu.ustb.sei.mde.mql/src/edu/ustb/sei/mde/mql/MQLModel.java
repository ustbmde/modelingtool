/**
 */
package edu.ustb.sei.mde.mql;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.mql.MQLModel#getActions <em>Actions</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.mql.MQLModel#getDatamodel <em>Datamodel</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.ustb.sei.mde.mql.MQLPackage#getMQLModel()
 * @model
 * @generated
 */
public interface MQLModel extends EObject {

	/**
	 * Returns the value of the '<em><b>Actions</b></em>' containment reference list.
	 * The list contents are of type {@link edu.ustb.sei.mde.mql.MQLAction}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Actions</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Actions</em>' containment reference list.
	 * @see edu.ustb.sei.mde.mql.MQLPackage#getMQLModel_Actions()
	 * @model containment="true"
	 * @generated
	 */
	EList<MQLAction> getActions();

	/**
	 * Returns the value of the '<em><b>Datamodel</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Datamodel</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Datamodel</em>' reference.
	 * @see #setDatamodel(EPackage)
	 * @see edu.ustb.sei.mde.mql.MQLPackage#getMQLModel_Datamodel()
	 * @model
	 * @generated
	 */
	EPackage getDatamodel();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.mql.MQLModel#getDatamodel <em>Datamodel</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Datamodel</em>' reference.
	 * @see #getDatamodel()
	 * @generated
	 */
	void setDatamodel(EPackage value);
} // MQLModel
