/**
 */
package edu.ustb.sei.mde.mql;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Query</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.mql.Query#getSources <em>Sources</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.ustb.sei.mde.mql.MQLPackage#getQuery()
 * @model
 * @generated
 */
public interface Query extends NamedElement, MQLAction {
	/**
	 * Returns the value of the '<em><b>Sources</b></em>' containment reference list.
	 * The list contents are of type {@link edu.ustb.sei.mde.mql.EntityNode}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sources</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sources</em>' containment reference list.
	 * @see edu.ustb.sei.mde.mql.MQLPackage#getQuery_Sources()
	 * @model containment="true"
	 * @generated
	 */
	EList<EntityNode> getSources();

} // Query
