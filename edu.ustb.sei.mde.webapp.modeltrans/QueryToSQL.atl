-- @path SQL=/edu.ustb.sei.mde.webapp/model/webapp.ecore
-- @path QT=/edu.ustb.sei.mde.mql/model/mql.ecore

module QueryToSQL;
create OUT : WM from IN : QT, DB : TM;
uses TMLibrary;
--uses CommonUtil;

helper context QT!EntityNode def: getSourceName() : String = if self.name.oclIsUndefined() then self.entity.name else self.entity.name +' AS ' + self.name endif;
helper context QT!EntityNode def: getReferenceName() : String = if self.name.oclIsUndefined() then self.entity.name else self.name endif;
helper context QT!AttributeNode def: getFullName() : String = self.eContainer().getReferenceName() +'.'+self.attribute.name;
helper def: getOperationParameterName() : String = 'object';

rule PackageToDatabaseAndClassComponent {
	from 
		p : TM!EPackage(
			p.eAnnotations->select(s|s.source='database')->notEmpty()
		)
	to
		database : WM!Database(
			name <- p.name
		),
		classComponent:WM!ClassComponent(
			name <- p.getDBClassName(),
			imports <- 'edu.ustb.sei.commonutil.serialization.dobject.*;'
		)
	do {
		database.datamodel <- p;
	}
}

rule QueryToFunction {
	from q:QT!Query
	using {
		classComponent : WM!ClassComponent = thisModule.resolveTemp(q.eContainer().datamodel,'classComponent');
		database : WM!Database = thisModule.resolveTemp(q.eContainer().datamodel,'database');
		cona:Sequence(QT!AttributeNode) = q.sources->collect(e|e.attributes->select(a|a.direction=#input))->flatten();
        retatt:Sequence(QT!AttributeNode) = q.sources->collect(c|c.attributes->select(x|x.direction=#output))->flatten();
		retcon:Sequence(String) = retatt->collect(a|a.getFullName());
		resultName:String = q.name+'ResultList';
	}
	to 
		f:WM!ActionOperation(
			name <- q.name,
			action <- seq,
			type <- 'DynamicList',
			parameters <- if q.parameters->isEmpty() then Sequence{} else thisModule.genInputParameter(thisModule.getOperationParameterName()) endif
		),
		seq : WM!SequenceAction(
			actions <- q.parameters->collect(p|thisModule.genParameterInit(p)),
			actions <- rlstCon,
			actions <- sqlAction,
			actions <- retAction
		),
		rlstCon:WM!CodeAction(
			code <- resultName +'= new DynamicList();' 
		),
		retAction:WM!CodeAction(
			code <- 'return '+resultName+';' 
		),
		sqlAction:WM!SQLSelectAction(
			sqlName <- q.name+'Sql',
			tables <- q.sources->collect(c|c.getSourceName()),
			columns <- retcon,
			conditionCode <- thisModule.getQueryCondition(cona),
			handlingAction <- ritAction,
			database <- database
		),
		ritAction:WM!SQLResultSetIterationAction(
			action <- itseq
		),
		itseq:WM!SequenceAction(
			actions <- Sequence{newObject}->union(retatt->collect(e|thisModule.genGetAttributeAction(e)))->union(Sequence{addObject})
		),
		newObject:WM!CodeAction(
			code <- 'DynamicObject obj = new DynamicObject();'
		),
		addObject:WM!CodeAction(
			code <- resultName+'.add(obj);'
		)
	do {
		classComponent.operations <- f;
	}
}

lazy rule genInputParameter {
	from pn : String
	to
		para : WM!Parameter (
			name <- thisModule.getOperationParameterName(),
			type <- 'DynamicObject'
		)
}

lazy rule genGetAttributeAction {
	from attr : QT!AttributeNode
	to l:WM!CodeAction(
		code <- if attr.attribute.eType = QT!EString then
		        'obj.put(\"'+attr.getFullName()+'\", '+attr.eContainer().eContainer().name+'.getString(\"'+attr.getFullName()+'\"));'
		        else 
		        	if attr.attribute.eType = QT!EInt then
		        	'obj.put(\"'+attr.getFullName()+'\", '+attr.eContainer().eContainer().name+'.getInt(\"'+attr.getFullName()+'\"));'
		        	else 
		        		if attr.attribute.eType = QT!ELong then
		        		'obj.put(\"'+attr.getFullName()+'\", '+attr.eContainer().eContainer().name+'.getLong(\"'+attr.getFullName()+'\"));'
		        		else 
		        			if attr.attribute.eType = QT!EBoolean then
		        			'obj.put(\"'+attr.getFullName()+'\", '+attr.eContainer().eContainer().name+'.getBoolean(\"'+attr.getFullName()+'\"));'
		        			else 
		        				if attr.attribute.eType = QT!EDate then
		        				'obj.put(\"'+attr.getFullName()+'\", '+attr.eContainer().eContainer().name+'.getDate(\"'+attr.getFullName()+'\"));'
		        				else 
		        					if attr.attribute.eType = QT!EFloat then
		        					'obj.put(\"'+attr.getFullName()+'\", '+attr.eContainer().eContainer().name+'.getFloat(\"'+attr.getFullName()+'\"));'
		        					else
		        						if attr.attribute.eType = QT!EDouble then
		        						'obj.put(\"'+attr.getFullName()+'\", '+attr.eContainer().eContainer().name+'.getDouble(\"'+attr.getFullName()+'\"));'
		        						else
		        							if attr.attribute.eType = QT!EString then
		        							'obj.put(\"'+attr.getFullName()+'\", '+attr.eContainer().eContainer().name+'.getString(\"'+attr.getFullName()+'\"));'
		        							else ''
											endif
										endif
									endif
								endif
							endif
						endif
					endif
				endif
	)
}

helper def: getQueryCondition(s:Sequence(QT!AttributeNode)) : String = 
	if s->isEmpty() then ''
	else
		let f:QT!AttributeNode = s->first() in
		let remainder:Sequence(QT!AttributeNode) = s->excluding(f) in
			'\"'+f.getFullName()+'=\"+'+f.value+(if remainder->isEmpty() then '' else '+\" and \"+' endif)+thisModule.getDeletionCondition(remainder)
	endif;

--lazy rule genParameter {
--	from pn:String
--	to p:WM!Parameter(
--		name <- pn,
--		type <- 'Object'
--		)
--}

lazy rule genParameterInit {
	from parameterName : String
	to 
		letOp : WM!CodeAction(
			code <- 'Object '+parameterName + ' = ' + thisModule.getOperationParameterName() +'.get(\"'+parameterName+'\");'
		)
}

rule InsertToFunction {
	
	from q:QT!Insert
	using {
			classComponent : WM!ClassComponent = thisModule.resolveTemp(q.eContainer().datamodel,'classComponent');
	}
	to 
		f:WM!ActionOperation(
			name <- q.name,
			action <- seq,
			type <- 'int',
			parameters <- if q.parameters->isEmpty() then Sequence{} else thisModule.genInputParameter(thisModule.getOperationParameterName()) endif
		),
		seq : WM!SequenceAction (
			actions <- q.parameters->collect(p|thisModule.genParameterInit(p)),
			actions <- q.targets->collect(e|thisModule.GenInsertAction(e)),
			actions <- ret
		),
		ret : WM!CodeAction(
			code <- 'return '+q.name+';'
		)
	do {
		classComponent.operations <- f;
	}
}

lazy rule GenInsertAction{
	from t:QT!EntityNode
	using {
		database : WM!Database = thisModule.resolveTemp(t.eContainer().eContainer().datamodel,'database');
	}
	to i:WM!SQLInsertAction(
		table <- t.entity.name,
		sqlName <- t.eContainer().name+'Sql',
		columns <- t.attributes->select(e|e.direction=#input)->collect(e|e.attribute.name),
		valueCodes <- thisModule.getInsertValues(t.attributes->asSequence()),
		database <- database
	)
}

helper def: getInsertValues(s:Sequence(QT!AttributeNode)) : Sequence(String) = 
	if s->isEmpty() then Sequence{}
	else
		let f:QT!AttributeNode = s->first() in
		let remainder:Sequence(QT!AttributeNode) = s->excluding(f) in
			Sequence{f.value}->union(thisModule.getInsertValues(remainder))
	endif;

rule DeleteToFunction {
	
	from q:QT!Delete
	using {
		classComponent : WM!ClassComponent = thisModule.resolveTemp(q.eContainer().datamodel,'classComponent');
	}
	to 
		f:WM!ActionOperation(
			name <- q.name,
			action <- seq,
			type <- 'int',
			parameters <- if q.parameters->isEmpty() then Sequence{} else thisModule.genInputParameter(thisModule.getOperationParameterName()) endif
		),
		seq : WM!SequenceAction (
			actions <- q.parameters->collect(p|thisModule.genParameterInit(p)),
			actions <- q.targets->collect(e|thisModule.GenDeletionAction(e)),
			actions <- ret
		),
		ret : WM!CodeAction(
			code <- 'return '+q.name+';'
		)
	do {
		classComponent.operations <- f;
	}
}
			
lazy rule GenDeletionAction{
	from t:QT!EntityNode
	using {
		database : WM!Database = thisModule.resolveTemp(t.eContainer().eContainer().datamodel,'database');
	}
	to d:WM!SQLDeleteAction(
		table <- t.entity.name,
		sqlName <- t.eContainer().name+'Sql',
		conditionCode <- thisModule.getDeletionCondition(t.attributes->asSequence()),
		database <- database
		)
}
			
helper def: getDeletionCondition(s:Sequence(QT!AttributeNode)) : String = 
	if s->isEmpty() then ''
	else
		let f:QT!AttributeNode = s->first() in
		let remainder:Sequence(QT!AttributeNode) = s->excluding(f) in
			'\"'+f.getFullName()+'=\"+'+f.value+(if remainder->isEmpty() then '' else '+\" and \"+' endif)+thisModule.getDeletionCondition(remainder)
	endif;

		
rule UpdateToFunction {
	
	from q:QT!Update
	using {
		classComponent : WM!ClassComponent = thisModule.resolveTemp(q.eContainer().datamodel,'classComponent');
	}
	to 
		f:WM!ActionOperation(
			name <- q.name,
			action <- seq,
			type <- 'int',
			parameters <- if q.parameters->isEmpty() then Sequence{} else thisModule.genInputParameter(thisModule.getOperationParameterName()) endif
		),
		seq : WM!SequenceAction (
			actions <- q.parameters->collect(p|thisModule.genParameterInit(p)),
			actions <- q.targets->collect(e|thisModule.GenUpdateAction(e)),
			actions <- ret
		),
		ret : WM!CodeAction(
			code <- 'return '+q.name+';'
		)
	do {
		classComponent.operations <- f;
	}
}

lazy rule GenUpdateAction{
	from t:QT!EntityNode
	using {
		attrs:Sequence(QT!AttributeNode) = t.attributes->select(e|e.direction=#input);
		cona:Sequence(QT!AttributeNode) = t.attributes->select(e|e.direction=#output);
		database : WM!Database = thisModule.resolveTemp(t.eContainer().eContainer().datamodel,'database');
	}
	to d:WM!SQLUpdateAction(
		table <- t.entity.name,
		columns <- attrs->collect(e|e.attribute.name),
		valueCodes <- attrs->collect(e|e.value),
		sqlName <- t.eContainer().name+'Sql',
		conditionCode <- thisModule.getUpdateCondition(cona),
		database <- database
		)
}

helper def: getUpdateCondition(s:Sequence(QT!AttributeNode)) : String = 
	if s->isEmpty() then ''
	else
		let f:QT!AttributeNode = s->first() in
		let remainder:Sequence(QT!AttributeNode) = s->excluding(f) in
			'\"'+f.getFullName()+'=\"+'+f.value+(if remainder->isEmpty() then '' else '+\" and \"+' endif)+thisModule.getDeletionCondition(remainder)
	endif;
		

rule EntityToModelDeleteSQL {
	from entity : TM!EClass (entity.hasAnnotation('table'))
	using {
		classComponent : WM!ClassComponent = thisModule.resolveTemp(entity.ePackage,'classComponent');
		delName : String = entity.name+'DeleteByID';
	}
	to
	f:WM!ActionOperation(
		name <- entity.name+'ModelDelete',
		parameters <- p,
		action <- seq
		),
	p :WM!Parameter(
		name <- 'id',
		type <- 'String'
		),
	seq : WM!SequenceAction(
		actions <- typeVar
		),
	typeVar : WM!CodeAction(
		code <- 'final String entityType = "'+entity.name+'";'
		),
	eDel : WM!CallAction (
		invocationTarget <- WM!OperationRealization.allInstances()->select(s|s.name=delName)->first(),
		parameterCodes <- 'id'
		)
	
	do {
		classComponent.operations <- f;
		-- for each crossing reference, call Reference ListDelete
		for( ref in entity.crossingReferences) {
			thisModule.genCallReferenceListDelete(ref,seq);
		}
		-- for each containing reference, call Reference ListDelete
		for( ref in entity.containingReferences) {
			thisModule.genCallReferenceListDelete(ref,seq);
		}
		-- for each containment reference, call ContainmentReference ModelDelete
		for( ref in entity.containmentReferences) {
			 thisModule.genCallContainmentReferenceModelDelete(ref,seq);
		}
		
		
		-- call element IDDelete
		seq.actions <- eDel;
	}
}

rule genCallReferenceListDelete(ref:TM!EAttribute, seq : TM!SequenceAction) {
	using {
		selfColumn : TM!EAttribute = ref.opposite;
		selfTypeColumn : TM!EAttribute = selfColumn.getTypeColumn();
		typeColumn : TM!EAttribute = ref.getTypeColumn();
		refClass : TM!EClass = ref.eContainingClass;
		delName : String = refClass.name+'DeleteBy'+selfColumn.name;
	}
	to
	call : WM!CallAction (
		invocationTarget <- WM!OperationRealization.allInstances()->select(s|s.name=delName)->first(),
		parameterCodes <- 'id',
		parameterCodes <- 'entityType'
		)
	do {
		seq.actions <- call;
	}
}

rule genCallContainmentReferenceModelDelete(ref:TM!EAttribute, seq : TM!SequenceAction) {
	using {
		selfColumn : TM!EAttribute = ref.opposite;
		selfTypeColumn : TM!EAttribute = selfColumn.getTypeColumn();
		typeColumn : TM!EAttribute = ref.getTypeColumn();
		refClass : TM!EClass = ref.eContainingClass;
	}
	to
	call : WM!CallAction (
		invocationTarget <- ref,
		parameterCodes <- 'id',
		parameterCodes <- 'entityType'
		)
	do {
		seq.actions <- call;
	}
}

rule ContainmentReferenceToModelDeleteSQL {
	from ref : TM!EAttribute(
		(ref.hasAnnotation('navigable') or ref.hasAnnotation('unnavigable')) and
		ref.eContainingClass.getAnnotationValue('reference','containment')='true' and
		ref.eContainingClass.getAnnotationValue('reference','container')<>ref.name
		)
	using {
		selfColumn : TM!EAttribute = ref.opposite;
		selfTypeColumn : TM!EAttribute = selfColumn.getTypeColumn();
		typeColumn : TM!EAttribute = ref.getTypeColumn();
		refClass : TM!EClass = ref.eContainingClass;

		queryName : String = refClass.name+'ListQueryBy'+selfColumn.name;
		queryResult : String = refClass.name+'_'+ref.name+'Results';
		queryIterator : String = queryResult+'Iterator';

		classComponent : WM!ClassComponent = thisModule.resolveTemp(ref.eContainingClass.ePackage,'classComponent');
	}
	to
	containmentReferenceModelDelete:WM!ActionOperation(
		name <- refClass.name+'_'+ref.name+'ModelDelete',
		parameters <- id,
		parameters <- type,
		action <- seq
		),
	type :WM!Parameter(
		name <- 'type',
		type <- 'String'
		),
	id :WM!Parameter(
		name <- 'id',
		type <- 'String'
		),
	seq : WM!SequenceAction(
		actions <- l,
		actions <- forEach
		),
	l : WM!LetAction (
		leftCode <- 'DynamicList '+queryResult,
		right <- qr
		),
	qr : WM!CallAction(
		invocationTarget <- WM!OperationRealization.allInstances()->select(s|s.name=queryName)->first(),
		parameterCodes <- 'id',
		parameterCodes <- 'type'
		),
	forEach : WM!ForAction (
		conditionCode <- 'DynamicObject '+queryIterator +':'+queryResult,
		bodyAction <- bodySeq
		),
	bodySeq : WM!SequenceAction(
		actions <- idVar,
		actions <- typeVar
		),
	idVar : WM!CodeAction (
		code <- 'String result_id = '+queryIterator+'.dGet("'+ref.name+'");'
		),
	typeVar : WM!CodeAction (
		code <- 'String result_type = '+queryIterator+'.dGet("'+typeColumn.name+'");'
		)
	do {
		--delete element
		thisModule.genComtainmentReferenceDeletion(ref.getTypeColumn().getReferredClasses(),1,'result_id','result_type',bodySeq);
		classComponent.operations <- containmentReferenceModelDelete;
	}
}

rule genComtainmentReferenceDeletion(cls:Sequence(TM!Class), i : Integer,id:String, type:String, seq:WM!SequenceAction) {
	using {
		current : TM!EClass = cls->at(i);
	}
	to
	ifA : WM!IfAction (
		conditionCode <- type+'.equals("'+current.name+'")',
		thenAction <- thenA
		),
	thenA : WM!CallAction(
		invocationTarget <- current,
		parameterCodes <- id
		)
	do {
		if(i=1) {
			seq.actions <- ifA;
		}
		if(i<cls->size()) {
			ifA.elseAction <- thisModule.genComtainmentReferenceDeletion(cls,i+1,id,type);
		}
	}
}

