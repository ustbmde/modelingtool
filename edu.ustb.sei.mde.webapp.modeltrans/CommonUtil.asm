<?xml version = '1.0' encoding = 'ISO-8859-1' ?>
<asm version="1.0" name="0">
	<cp>
		<constant value="CommonUtil"/>
		<constant value="main"/>
		<constant value="A"/>
		<constant value="self"/>
		<constant value="getSourceName"/>
		<constant value="MQT!EntityNode;"/>
		<constant value="0"/>
		<constant value="name"/>
		<constant value="J.oclIsUndefined():J"/>
		<constant value="13"/>
		<constant value="entity"/>
		<constant value=" AS "/>
		<constant value="J.+(J):J"/>
		<constant value="16"/>
		<constant value="3:65-3:69"/>
		<constant value="3:65-3:74"/>
		<constant value="3:65-3:91"/>
		<constant value="3:119-3:123"/>
		<constant value="3:119-3:130"/>
		<constant value="3:119-3:135"/>
		<constant value="3:137-3:143"/>
		<constant value="3:119-3:143"/>
		<constant value="3:146-3:150"/>
		<constant value="3:146-3:155"/>
		<constant value="3:119-3:155"/>
		<constant value="3:97-3:101"/>
		<constant value="3:97-3:108"/>
		<constant value="3:97-3:113"/>
		<constant value="3:62-3:161"/>
		<constant value="getReferenceName"/>
		<constant value="7"/>
		<constant value="10"/>
		<constant value="4:68-4:72"/>
		<constant value="4:68-4:77"/>
		<constant value="4:68-4:94"/>
		<constant value="4:122-4:126"/>
		<constant value="4:122-4:131"/>
		<constant value="4:100-4:104"/>
		<constant value="4:100-4:111"/>
		<constant value="4:100-4:116"/>
		<constant value="4:65-4:137"/>
		<constant value="getFullName"/>
		<constant value="MQT!AttributeNode;"/>
		<constant value="J.eContainer():J"/>
		<constant value="J.getReferenceName():J"/>
		<constant value="."/>
		<constant value="attribute"/>
		<constant value="5:63-5:67"/>
		<constant value="5:63-5:80"/>
		<constant value="5:63-5:99"/>
		<constant value="5:101-5:104"/>
		<constant value="5:63-5:104"/>
		<constant value="5:105-5:109"/>
		<constant value="5:105-5:119"/>
		<constant value="5:105-5:124"/>
		<constant value="5:63-5:124"/>
	</cp>
	<operation name="1">
		<context type="2"/>
		<parameters>
		</parameters>
		<code>
		</code>
		<linenumbertable>
		</linenumbertable>
		<localvariabletable>
			<lve slot="0" name="3" begin="0" end="-1"/>
		</localvariabletable>
	</operation>
	<operation name="4">
		<context type="5"/>
		<parameters>
		</parameters>
		<code>
			<load arg="6"/>
			<get arg="7"/>
			<call arg="8"/>
			<if arg="9"/>
			<load arg="6"/>
			<get arg="10"/>
			<get arg="7"/>
			<push arg="11"/>
			<call arg="12"/>
			<load arg="6"/>
			<get arg="7"/>
			<call arg="12"/>
			<goto arg="13"/>
			<load arg="6"/>
			<get arg="10"/>
			<get arg="7"/>
		</code>
		<linenumbertable>
			<lne id="14" begin="0" end="0"/>
			<lne id="15" begin="0" end="1"/>
			<lne id="16" begin="0" end="2"/>
			<lne id="17" begin="4" end="4"/>
			<lne id="18" begin="4" end="5"/>
			<lne id="19" begin="4" end="6"/>
			<lne id="20" begin="7" end="7"/>
			<lne id="21" begin="4" end="8"/>
			<lne id="22" begin="9" end="9"/>
			<lne id="23" begin="9" end="10"/>
			<lne id="24" begin="4" end="11"/>
			<lne id="25" begin="13" end="13"/>
			<lne id="26" begin="13" end="14"/>
			<lne id="27" begin="13" end="15"/>
			<lne id="28" begin="0" end="15"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="0" name="3" begin="0" end="15"/>
		</localvariabletable>
	</operation>
	<operation name="29">
		<context type="5"/>
		<parameters>
		</parameters>
		<code>
			<load arg="6"/>
			<get arg="7"/>
			<call arg="8"/>
			<if arg="30"/>
			<load arg="6"/>
			<get arg="7"/>
			<goto arg="31"/>
			<load arg="6"/>
			<get arg="10"/>
			<get arg="7"/>
		</code>
		<linenumbertable>
			<lne id="32" begin="0" end="0"/>
			<lne id="33" begin="0" end="1"/>
			<lne id="34" begin="0" end="2"/>
			<lne id="35" begin="4" end="4"/>
			<lne id="36" begin="4" end="5"/>
			<lne id="37" begin="7" end="7"/>
			<lne id="38" begin="7" end="8"/>
			<lne id="39" begin="7" end="9"/>
			<lne id="40" begin="0" end="9"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="0" name="3" begin="0" end="9"/>
		</localvariabletable>
	</operation>
	<operation name="41">
		<context type="42"/>
		<parameters>
		</parameters>
		<code>
			<load arg="6"/>
			<call arg="43"/>
			<call arg="44"/>
			<push arg="45"/>
			<call arg="12"/>
			<load arg="6"/>
			<get arg="46"/>
			<get arg="7"/>
			<call arg="12"/>
		</code>
		<linenumbertable>
			<lne id="47" begin="0" end="0"/>
			<lne id="48" begin="0" end="1"/>
			<lne id="49" begin="0" end="2"/>
			<lne id="50" begin="3" end="3"/>
			<lne id="51" begin="0" end="4"/>
			<lne id="52" begin="5" end="5"/>
			<lne id="53" begin="5" end="6"/>
			<lne id="54" begin="5" end="7"/>
			<lne id="55" begin="0" end="8"/>
		</linenumbertable>
		<localvariabletable>
			<lve slot="0" name="3" begin="0" end="8"/>
		</localvariabletable>
	</operation>
</asm>
