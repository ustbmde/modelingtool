/**
 */
package edu.ustb.sei.mde.webapp;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>If Action</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.webapp.IfAction#getConditionCode <em>Condition Code</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.IfAction#getThenAction <em>Then Action</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.IfAction#getElseAction <em>Else Action</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getIfAction()
 * @model
 * @generated
 */
public interface IfAction extends Action, StatementContainer {
	/**
	 * Returns the value of the '<em><b>Condition Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Condition Code</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Condition Code</em>' attribute.
	 * @see #setConditionCode(String)
	 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getIfAction_ConditionCode()
	 * @model required="true"
	 * @generated
	 */
	String getConditionCode();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.webapp.IfAction#getConditionCode <em>Condition Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Condition Code</em>' attribute.
	 * @see #getConditionCode()
	 * @generated
	 */
	void setConditionCode(String value);

	/**
	 * Returns the value of the '<em><b>Then Action</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Then Action</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Then Action</em>' containment reference.
	 * @see #setThenAction(Action)
	 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getIfAction_ThenAction()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Action getThenAction();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.webapp.IfAction#getThenAction <em>Then Action</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Then Action</em>' containment reference.
	 * @see #getThenAction()
	 * @generated
	 */
	void setThenAction(Action value);

	/**
	 * Returns the value of the '<em><b>Else Action</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Else Action</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Else Action</em>' containment reference.
	 * @see #setElseAction(Action)
	 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getIfAction_ElseAction()
	 * @model containment="true"
	 * @generated
	 */
	Action getElseAction();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.webapp.IfAction#getElseAction <em>Else Action</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Else Action</em>' containment reference.
	 * @see #getElseAction()
	 * @generated
	 */
	void setElseAction(Action value);

} // IfAction
