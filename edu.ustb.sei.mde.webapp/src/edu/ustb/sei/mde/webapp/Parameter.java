/**
 */
package edu.ustb.sei.mde.webapp;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Parameter</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getParameter()
 * @model
 * @generated
 */
public interface Parameter extends NamedElement, TypedElement {
} // Parameter
