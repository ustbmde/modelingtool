/**
 */
package edu.ustb.sei.mde.webapp;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Let Action</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.webapp.LetAction#getLeftCode <em>Left Code</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.LetAction#getRight <em>Right</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getLetAction()
 * @model
 * @generated
 */
public interface LetAction extends Action {
	/**
	 * Returns the value of the '<em><b>Left Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Left Code</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Left Code</em>' attribute.
	 * @see #setLeftCode(String)
	 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getLetAction_LeftCode()
	 * @model required="true"
	 * @generated
	 */
	String getLeftCode();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.webapp.LetAction#getLeftCode <em>Left Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Left Code</em>' attribute.
	 * @see #getLeftCode()
	 * @generated
	 */
	void setLeftCode(String value);

	/**
	 * Returns the value of the '<em><b>Right</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Right</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Right</em>' containment reference.
	 * @see #setRight(Action)
	 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getLetAction_Right()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Action getRight();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.webapp.LetAction#getRight <em>Right</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Right</em>' containment reference.
	 * @see #getRight()
	 * @generated
	 */
	void setRight(Action value);

} // LetAction
