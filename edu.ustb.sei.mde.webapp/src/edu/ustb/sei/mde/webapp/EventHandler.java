/**
 */
package edu.ustb.sei.mde.webapp;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Event Handler</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.webapp.EventHandler#getEvent <em>Event</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.EventHandler#getOwner <em>Owner</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.EventHandler#getHandler <em>Handler</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getEventHandler()
 * @model
 * @generated
 */
public interface EventHandler extends EObject {
	/**
	 * Returns the value of the '<em><b>Event</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Event</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Event</em>' attribute.
	 * @see #setEvent(String)
	 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getEventHandler_Event()
	 * @model
	 * @generated
	 */
	String getEvent();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.webapp.EventHandler#getEvent <em>Event</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Event</em>' attribute.
	 * @see #getEvent()
	 * @generated
	 */
	void setEvent(String value);

	/**
	 * Returns the value of the '<em><b>Owner</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link edu.ustb.sei.mde.webapp.HTMLNode#getHandlers <em>Handlers</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Owner</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Owner</em>' container reference.
	 * @see #setOwner(HTMLNode)
	 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getEventHandler_Owner()
	 * @see edu.ustb.sei.mde.webapp.HTMLNode#getHandlers
	 * @model opposite="handlers" transient="false"
	 * @generated
	 */
	HTMLNode getOwner();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.webapp.EventHandler#getOwner <em>Owner</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Owner</em>' container reference.
	 * @see #getOwner()
	 * @generated
	 */
	void setOwner(HTMLNode value);

	/**
	 * Returns the value of the '<em><b>Handler</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Handler</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Handler</em>' reference.
	 * @see #setHandler(Operation)
	 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getEventHandler_Handler()
	 * @model
	 * @generated
	 */
	Operation getHandler();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.webapp.EventHandler#getHandler <em>Handler</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Handler</em>' reference.
	 * @see #getHandler()
	 * @generated
	 */
	void setHandler(Operation value);

} // EventHandler
