/**
 */
package edu.ustb.sei.mde.webapp;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>SQL Delete Action</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.webapp.SQLDeleteAction#getTable <em>Table</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.SQLDeleteAction#getConditionCode <em>Condition Code</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getSQLDeleteAction()
 * @model
 * @generated
 */
public interface SQLDeleteAction extends SQLAction {
	/**
	 * Returns the value of the '<em><b>Table</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Table</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Table</em>' attribute.
	 * @see #setTable(String)
	 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getSQLDeleteAction_Table()
	 * @model required="true"
	 * @generated
	 */
	String getTable();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.webapp.SQLDeleteAction#getTable <em>Table</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Table</em>' attribute.
	 * @see #getTable()
	 * @generated
	 */
	void setTable(String value);

	/**
	 * Returns the value of the '<em><b>Condition Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Condition Code</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Condition Code</em>' attribute.
	 * @see #setConditionCode(String)
	 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getSQLDeleteAction_ConditionCode()
	 * @model
	 * @generated
	 */
	String getConditionCode();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.webapp.SQLDeleteAction#getConditionCode <em>Condition Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Condition Code</em>' attribute.
	 * @see #getConditionCode()
	 * @generated
	 */
	void setConditionCode(String value);

} // SQLDeleteAction
