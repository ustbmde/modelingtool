/**
 */
package edu.ustb.sei.mde.webapp;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Deployable Pointer</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.webapp.DeployablePointer#getUrl <em>Url</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.DeployablePointer#getDeployable <em>Deployable</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getDeployablePointer()
 * @model abstract="true"
 * @generated
 */
public interface DeployablePointer extends EObject {
	/**
	 * Returns the value of the '<em><b>Url</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Url</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Url</em>' attribute.
	 * @see #setUrl(String)
	 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getDeployablePointer_Url()
	 * @model
	 * @generated
	 */
	String getUrl();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.webapp.DeployablePointer#getUrl <em>Url</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Url</em>' attribute.
	 * @see #getUrl()
	 * @generated
	 */
	void setUrl(String value);

	/**
	 * Returns the value of the '<em><b>Deployable</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Deployable</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Deployable</em>' reference.
	 * @see #setDeployable(Deployable)
	 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getDeployablePointer_Deployable()
	 * @model
	 * @generated
	 */
	Deployable getDeployable();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.webapp.DeployablePointer#getDeployable <em>Deployable</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Deployable</em>' reference.
	 * @see #getDeployable()
	 * @generated
	 */
	void setDeployable(Deployable value);

} // DeployablePointer
