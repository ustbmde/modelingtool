/**
 */
package edu.ustb.sei.mde.webapp;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Call Action</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.webapp.CallAction#getInvocationTarget <em>Invocation Target</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.CallAction#getParameterCodes <em>Parameter Codes</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getCallAction()
 * @model
 * @generated
 */
public interface CallAction extends Action {
	/**
	 * Returns the value of the '<em><b>Invocation Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Invocation Target</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Invocation Target</em>' reference.
	 * @see #setInvocationTarget(Operation)
	 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getCallAction_InvocationTarget()
	 * @model required="true"
	 * @generated
	 */
	Operation getInvocationTarget();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.webapp.CallAction#getInvocationTarget <em>Invocation Target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Invocation Target</em>' reference.
	 * @see #getInvocationTarget()
	 * @generated
	 */
	void setInvocationTarget(Operation value);

	/**
	 * Returns the value of the '<em><b>Parameter Codes</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parameter Codes</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parameter Codes</em>' attribute list.
	 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getCallAction_ParameterCodes()
	 * @model
	 * @generated
	 */
	EList<String> getParameterCodes();

} // CallAction
