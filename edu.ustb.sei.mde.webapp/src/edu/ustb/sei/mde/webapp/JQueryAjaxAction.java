/**
 */
package edu.ustb.sei.mde.webapp;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>JQuery Ajax Action</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.webapp.JQueryAjaxAction#getSelectorCode <em>Selector Code</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.JQueryAjaxAction#getAjaxType <em>Ajax Type</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.JQueryAjaxAction#getDataCode <em>Data Code</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.JQueryAjaxAction#getDataType <em>Data Type</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.JQueryAjaxAction#getCallbackFunction <em>Callback Function</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getJQueryAjaxAction()
 * @model
 * @generated
 */
public interface JQueryAjaxAction extends JavascriptAction, DeployablePointer {
	/**
	 * Returns the value of the '<em><b>Selector Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Selector Code</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Selector Code</em>' attribute.
	 * @see #setSelectorCode(String)
	 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getJQueryAjaxAction_SelectorCode()
	 * @model required="true"
	 * @generated
	 */
	String getSelectorCode();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.webapp.JQueryAjaxAction#getSelectorCode <em>Selector Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Selector Code</em>' attribute.
	 * @see #getSelectorCode()
	 * @generated
	 */
	void setSelectorCode(String value);

	/**
	 * Returns the value of the '<em><b>Ajax Type</b></em>' attribute.
	 * The literals are from the enumeration {@link edu.ustb.sei.mde.webapp.AjaxType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ajax Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ajax Type</em>' attribute.
	 * @see edu.ustb.sei.mde.webapp.AjaxType
	 * @see #setAjaxType(AjaxType)
	 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getJQueryAjaxAction_AjaxType()
	 * @model required="true"
	 * @generated
	 */
	AjaxType getAjaxType();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.webapp.JQueryAjaxAction#getAjaxType <em>Ajax Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ajax Type</em>' attribute.
	 * @see edu.ustb.sei.mde.webapp.AjaxType
	 * @see #getAjaxType()
	 * @generated
	 */
	void setAjaxType(AjaxType value);

	/**
	 * Returns the value of the '<em><b>Data Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Code</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Code</em>' attribute.
	 * @see #setDataCode(String)
	 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getJQueryAjaxAction_DataCode()
	 * @model
	 * @generated
	 */
	String getDataCode();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.webapp.JQueryAjaxAction#getDataCode <em>Data Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data Code</em>' attribute.
	 * @see #getDataCode()
	 * @generated
	 */
	void setDataCode(String value);

	/**
	 * Returns the value of the '<em><b>Data Type</b></em>' attribute.
	 * The literals are from the enumeration {@link edu.ustb.sei.mde.webapp.DataFormat}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Type</em>' attribute.
	 * @see edu.ustb.sei.mde.webapp.DataFormat
	 * @see #setDataType(DataFormat)
	 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getJQueryAjaxAction_DataType()
	 * @model
	 * @generated
	 */
	DataFormat getDataType();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.webapp.JQueryAjaxAction#getDataType <em>Data Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data Type</em>' attribute.
	 * @see edu.ustb.sei.mde.webapp.DataFormat
	 * @see #getDataType()
	 * @generated
	 */
	void setDataType(DataFormat value);

	/**
	 * Returns the value of the '<em><b>Callback Function</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Callback Function</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Callback Function</em>' containment reference.
	 * @see #setCallbackFunction(OperationRealization)
	 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getJQueryAjaxAction_CallbackFunction()
	 * @model containment="true"
	 * @generated
	 */
	OperationRealization getCallbackFunction();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.webapp.JQueryAjaxAction#getCallbackFunction <em>Callback Function</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Callback Function</em>' containment reference.
	 * @see #getCallbackFunction()
	 * @generated
	 */
	void setCallbackFunction(OperationRealization value);

} // JQueryAjaxAction
