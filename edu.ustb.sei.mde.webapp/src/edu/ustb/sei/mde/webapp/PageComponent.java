/**
 */
package edu.ustb.sei.mde.webapp;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Page Component</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.webapp.PageComponent#getNode <em>Node</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.PageComponent#isPage <em>Page</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getPageComponent()
 * @model
 * @generated
 */
public interface PageComponent extends Component, Deployable {
	/**
	 * Returns the value of the '<em><b>Node</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Node</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Node</em>' containment reference.
	 * @see #setNode(HTMLNode)
	 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getPageComponent_Node()
	 * @model containment="true"
	 * @generated
	 */
	HTMLNode getNode();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.webapp.PageComponent#getNode <em>Node</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Node</em>' containment reference.
	 * @see #getNode()
	 * @generated
	 */
	void setNode(HTMLNode value);

	/**
	 * Returns the value of the '<em><b>Page</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Page</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Page</em>' attribute.
	 * @see #setPage(boolean)
	 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getPageComponent_Page()
	 * @model default="false"
	 * @generated
	 */
	boolean isPage();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.webapp.PageComponent#isPage <em>Page</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Page</em>' attribute.
	 * @see #isPage()
	 * @generated
	 */
	void setPage(boolean value);

} // PageComponent
