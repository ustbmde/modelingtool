/**
 */
package edu.ustb.sei.mde.webapp;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>SQL Execution Action</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.webapp.SQLExecutionAction#getSqlCode <em>Sql Code</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getSQLExecutionAction()
 * @model
 * @generated
 */
public interface SQLExecutionAction extends SQLAction {
	/**
	 * Returns the value of the '<em><b>Sql Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sql Code</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sql Code</em>' attribute.
	 * @see #setSqlCode(String)
	 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getSQLExecutionAction_SqlCode()
	 * @model required="true"
	 * @generated
	 */
	String getSqlCode();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.webapp.SQLExecutionAction#getSqlCode <em>Sql Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sql Code</em>' attribute.
	 * @see #getSqlCode()
	 * @generated
	 */
	void setSqlCode(String value);

} // SQLExecutionAction
