/**
 */
package edu.ustb.sei.mde.webapp;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Javascript Action</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getJavascriptAction()
 * @model abstract="true"
 * @generated
 */
public interface JavascriptAction extends Action {
} // JavascriptAction
