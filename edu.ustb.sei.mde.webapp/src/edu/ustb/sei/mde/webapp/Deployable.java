/**
 */
package edu.ustb.sei.mde.webapp;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Deployable</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.webapp.Deployable#getDeployPath <em>Deploy Path</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getDeployable()
 * @model abstract="true"
 * @generated
 */
public interface Deployable extends EObject {
	/**
	 * Returns the value of the '<em><b>Deploy Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Deploy Path</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Deploy Path</em>' attribute.
	 * @see #setDeployPath(String)
	 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getDeployable_DeployPath()
	 * @model
	 * @generated
	 */
	String getDeployPath();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.webapp.Deployable#getDeployPath <em>Deploy Path</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Deploy Path</em>' attribute.
	 * @see #getDeployPath()
	 * @generated
	 */
	void setDeployPath(String value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	String getURL();

} // Deployable
