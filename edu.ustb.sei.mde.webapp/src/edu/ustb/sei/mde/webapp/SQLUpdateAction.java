/**
 */
package edu.ustb.sei.mde.webapp;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>SQL Update Action</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.webapp.SQLUpdateAction#getTable <em>Table</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.SQLUpdateAction#getColumns <em>Columns</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.SQLUpdateAction#getValueCodes <em>Value Codes</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.SQLUpdateAction#getConditionCode <em>Condition Code</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getSQLUpdateAction()
 * @model
 * @generated
 */
public interface SQLUpdateAction extends SQLAction {
	/**
	 * Returns the value of the '<em><b>Table</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Table</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Table</em>' attribute.
	 * @see #setTable(String)
	 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getSQLUpdateAction_Table()
	 * @model required="true"
	 * @generated
	 */
	String getTable();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.webapp.SQLUpdateAction#getTable <em>Table</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Table</em>' attribute.
	 * @see #getTable()
	 * @generated
	 */
	void setTable(String value);

	/**
	 * Returns the value of the '<em><b>Columns</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Columns</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Columns</em>' attribute list.
	 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getSQLUpdateAction_Columns()
	 * @model required="true"
	 * @generated
	 */
	EList<String> getColumns();

	/**
	 * Returns the value of the '<em><b>Value Codes</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value Codes</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value Codes</em>' attribute list.
	 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getSQLUpdateAction_ValueCodes()
	 * @model
	 * @generated
	 */
	EList<String> getValueCodes();

	/**
	 * Returns the value of the '<em><b>Condition Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Condition Code</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Condition Code</em>' attribute.
	 * @see #setConditionCode(String)
	 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getSQLUpdateAction_ConditionCode()
	 * @model
	 * @generated
	 */
	String getConditionCode();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.webapp.SQLUpdateAction#getConditionCode <em>Condition Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Condition Code</em>' attribute.
	 * @see #getConditionCode()
	 * @generated
	 */
	void setConditionCode(String value);

} // SQLUpdateAction
