/**
 */
package edu.ustb.sei.mde.webapp;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Catch Part</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.webapp.CatchPart#getExceptionVariable <em>Exception Variable</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.CatchPart#getActioin <em>Actioin</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getCatchPart()
 * @model
 * @generated
 */
public interface CatchPart extends StatementContainer {
	/**
	 * Returns the value of the '<em><b>Exception Variable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Exception Variable</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Exception Variable</em>' attribute.
	 * @see #setExceptionVariable(String)
	 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getCatchPart_ExceptionVariable()
	 * @model
	 * @generated
	 */
	String getExceptionVariable();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.webapp.CatchPart#getExceptionVariable <em>Exception Variable</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Exception Variable</em>' attribute.
	 * @see #getExceptionVariable()
	 * @generated
	 */
	void setExceptionVariable(String value);

	/**
	 * Returns the value of the '<em><b>Actioin</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Actioin</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Actioin</em>' containment reference.
	 * @see #setActioin(Action)
	 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getCatchPart_Actioin()
	 * @model containment="true"
	 * @generated
	 */
	Action getActioin();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.webapp.CatchPart#getActioin <em>Actioin</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Actioin</em>' containment reference.
	 * @see #getActioin()
	 * @generated
	 */
	void setActioin(Action value);

} // CatchPart
