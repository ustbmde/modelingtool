/**
 */
package edu.ustb.sei.mde.webapp;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Server Page Action</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getServerPageAction()
 * @model abstract="true"
 * @generated
 */
public interface ServerPageAction extends Action {
} // ServerPageAction
