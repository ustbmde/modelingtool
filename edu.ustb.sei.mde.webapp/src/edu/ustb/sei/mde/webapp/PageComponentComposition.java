/**
 */
package edu.ustb.sei.mde.webapp;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Page Component Composition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.webapp.PageComponentComposition#getTime <em>Time</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.PageComponentComposition#getContainerSelector <em>Container Selector</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getPageComponentComposition()
 * @model
 * @generated
 */
public interface PageComponentComposition extends Composition {
	/**
	 * Returns the value of the '<em><b>Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Time</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Time</em>' attribute.
	 * @see #setTime(String)
	 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getPageComponentComposition_Time()
	 * @model
	 * @generated
	 */
	String getTime();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.webapp.PageComponentComposition#getTime <em>Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Time</em>' attribute.
	 * @see #getTime()
	 * @generated
	 */
	void setTime(String value);

	/**
	 * Returns the value of the '<em><b>Container Selector</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Container Selector</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Container Selector</em>' attribute.
	 * @see #setContainerSelector(String)
	 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getPageComponentComposition_ContainerSelector()
	 * @model
	 * @generated
	 */
	String getContainerSelector();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.webapp.PageComponentComposition#getContainerSelector <em>Container Selector</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Container Selector</em>' attribute.
	 * @see #getContainerSelector()
	 * @generated
	 */
	void setContainerSelector(String value);

} // PageComponentComposition
