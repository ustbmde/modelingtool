/**
 */
package edu.ustb.sei.mde.webapp;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Web Application</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.webapp.WebApplication#getDatabaseUtilClass <em>Database Util Class</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.WebApplication#getDatabases <em>Databases</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.WebApplication#getInterfaces <em>Interfaces</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.WebApplication#getPageComponents <em>Page Components</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.WebApplication#getServerComponents <em>Server Components</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.WebApplication#getClassComponents <em>Class Components</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getWebApplication()
 * @model
 * @generated
 */
public interface WebApplication extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Database Util Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Database Util Class</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Database Util Class</em>' attribute.
	 * @see #setDatabaseUtilClass(String)
	 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getWebApplication_DatabaseUtilClass()
	 * @model
	 * @generated
	 */
	String getDatabaseUtilClass();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.webapp.WebApplication#getDatabaseUtilClass <em>Database Util Class</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Database Util Class</em>' attribute.
	 * @see #getDatabaseUtilClass()
	 * @generated
	 */
	void setDatabaseUtilClass(String value);

	/**
	 * Returns the value of the '<em><b>Databases</b></em>' containment reference list.
	 * The list contents are of type {@link edu.ustb.sei.mde.webapp.Database}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Databases</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Databases</em>' containment reference list.
	 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getWebApplication_Databases()
	 * @model containment="true"
	 * @generated
	 */
	EList<Database> getDatabases();

	/**
	 * Returns the value of the '<em><b>Interfaces</b></em>' containment reference list.
	 * The list contents are of type {@link edu.ustb.sei.mde.webapp.Interface}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Interfaces</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Interfaces</em>' containment reference list.
	 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getWebApplication_Interfaces()
	 * @model containment="true"
	 * @generated
	 */
	EList<Interface> getInterfaces();

	/**
	 * Returns the value of the '<em><b>Page Components</b></em>' containment reference list.
	 * The list contents are of type {@link edu.ustb.sei.mde.webapp.PageComponent}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Page Components</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Page Components</em>' containment reference list.
	 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getWebApplication_PageComponents()
	 * @model containment="true"
	 * @generated
	 */
	EList<PageComponent> getPageComponents();

	/**
	 * Returns the value of the '<em><b>Server Components</b></em>' containment reference list.
	 * The list contents are of type {@link edu.ustb.sei.mde.webapp.ServerComponent}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Server Components</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Server Components</em>' containment reference list.
	 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getWebApplication_ServerComponents()
	 * @model containment="true"
	 * @generated
	 */
	EList<ServerComponent> getServerComponents();

	/**
	 * Returns the value of the '<em><b>Class Components</b></em>' containment reference list.
	 * The list contents are of type {@link edu.ustb.sei.mde.webapp.ClassComponent}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Class Components</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Class Components</em>' containment reference list.
	 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getWebApplication_ClassComponents()
	 * @model containment="true"
	 * @generated
	 */
	EList<ClassComponent> getClassComponents();

} // WebApplication
