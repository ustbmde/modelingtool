/**
 */
package edu.ustb.sei.mde.webapp;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Action</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getAction()
 * @model abstract="true"
 * @generated
 */
public interface Action extends EObject {
} // Action
