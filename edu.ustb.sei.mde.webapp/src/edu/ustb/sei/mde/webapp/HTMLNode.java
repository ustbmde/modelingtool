/**
 */
package edu.ustb.sei.mde.webapp;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>HTML Node</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.webapp.HTMLNode#getTag <em>Tag</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.HTMLNode#getId <em>Id</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.HTMLNode#getClass_ <em>Class</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.HTMLNode#getExtra <em>Extra</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.HTMLNode#getInnerText <em>Inner Text</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.HTMLNode#getChildren <em>Children</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.HTMLNode#getHandlers <em>Handlers</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.HTMLNode#getStyles <em>Styles</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getHTMLNode()
 * @model
 * @generated
 */
public interface HTMLNode extends EObject {
	/**
	 * Returns the value of the '<em><b>Tag</b></em>' attribute.
	 * The default value is <code>"div"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Tag</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tag</em>' attribute.
	 * @see #setTag(String)
	 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getHTMLNode_Tag()
	 * @model default="div"
	 * @generated
	 */
	String getTag();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.webapp.HTMLNode#getTag <em>Tag</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Tag</em>' attribute.
	 * @see #getTag()
	 * @generated
	 */
	void setTag(String value);

	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(String)
	 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getHTMLNode_Id()
	 * @model
	 * @generated
	 */
	String getId();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.webapp.HTMLNode#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(String value);

	/**
	 * Returns the value of the '<em><b>Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Class</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Class</em>' attribute.
	 * @see #setClass(String)
	 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getHTMLNode_Class()
	 * @model
	 * @generated
	 */
	String getClass_();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.webapp.HTMLNode#getClass_ <em>Class</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Class</em>' attribute.
	 * @see #getClass_()
	 * @generated
	 */
	void setClass(String value);

	/**
	 * Returns the value of the '<em><b>Extra</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Extra</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Extra</em>' attribute list.
	 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getHTMLNode_Extra()
	 * @model
	 * @generated
	 */
	EList<String> getExtra();

	/**
	 * Returns the value of the '<em><b>Inner Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Inner Text</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Inner Text</em>' attribute.
	 * @see #setInnerText(String)
	 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getHTMLNode_InnerText()
	 * @model
	 * @generated
	 */
	String getInnerText();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.webapp.HTMLNode#getInnerText <em>Inner Text</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Inner Text</em>' attribute.
	 * @see #getInnerText()
	 * @generated
	 */
	void setInnerText(String value);

	/**
	 * Returns the value of the '<em><b>Children</b></em>' containment reference list.
	 * The list contents are of type {@link edu.ustb.sei.mde.webapp.HTMLNode}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Children</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Children</em>' containment reference list.
	 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getHTMLNode_Children()
	 * @model containment="true"
	 * @generated
	 */
	EList<HTMLNode> getChildren();

	/**
	 * Returns the value of the '<em><b>Handlers</b></em>' containment reference list.
	 * The list contents are of type {@link edu.ustb.sei.mde.webapp.EventHandler}.
	 * It is bidirectional and its opposite is '{@link edu.ustb.sei.mde.webapp.EventHandler#getOwner <em>Owner</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Handlers</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Handlers</em>' containment reference list.
	 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getHTMLNode_Handlers()
	 * @see edu.ustb.sei.mde.webapp.EventHandler#getOwner
	 * @model opposite="owner" containment="true"
	 * @generated
	 */
	EList<EventHandler> getHandlers();

	/**
	 * Returns the value of the '<em><b>Styles</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Styles</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Styles</em>' attribute list.
	 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getHTMLNode_Styles()
	 * @model
	 * @generated
	 */
	EList<String> getStyles();

} // HTMLNode
