/**
 */
package edu.ustb.sei.mde.webapp;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Try Catch Action</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.webapp.TryCatchAction#getTryPart <em>Try Part</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.TryCatchAction#getCatchParts <em>Catch Parts</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.TryCatchAction#getFinalPart <em>Final Part</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getTryCatchAction()
 * @model
 * @generated
 */
public interface TryCatchAction extends Action, StatementContainer {
	/**
	 * Returns the value of the '<em><b>Try Part</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Try Part</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Try Part</em>' containment reference.
	 * @see #setTryPart(Action)
	 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getTryCatchAction_TryPart()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Action getTryPart();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.webapp.TryCatchAction#getTryPart <em>Try Part</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Try Part</em>' containment reference.
	 * @see #getTryPart()
	 * @generated
	 */
	void setTryPart(Action value);

	/**
	 * Returns the value of the '<em><b>Catch Parts</b></em>' containment reference list.
	 * The list contents are of type {@link edu.ustb.sei.mde.webapp.CatchPart}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Catch Parts</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Catch Parts</em>' containment reference list.
	 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getTryCatchAction_CatchParts()
	 * @model containment="true"
	 * @generated
	 */
	EList<CatchPart> getCatchParts();

	/**
	 * Returns the value of the '<em><b>Final Part</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Final Part</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Final Part</em>' containment reference.
	 * @see #setFinalPart(Action)
	 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getTryCatchAction_FinalPart()
	 * @model containment="true"
	 * @generated
	 */
	Action getFinalPart();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.webapp.TryCatchAction#getFinalPart <em>Final Part</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Final Part</em>' containment reference.
	 * @see #getFinalPart()
	 * @generated
	 */
	void setFinalPart(Action value);

} // TryCatchAction
