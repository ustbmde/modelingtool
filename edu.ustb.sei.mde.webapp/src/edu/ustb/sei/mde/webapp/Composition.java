/**
 */
package edu.ustb.sei.mde.webapp;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Composition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.webapp.Composition#getOwner <em>Owner</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getComposition()
 * @model
 * @generated
 */
public interface Composition extends DeployablePointer {
	/**
	 * Returns the value of the '<em><b>Owner</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link edu.ustb.sei.mde.webapp.Component#getCompositions <em>Compositions</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Owner</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Owner</em>' container reference.
	 * @see #setOwner(Component)
	 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getComposition_Owner()
	 * @see edu.ustb.sei.mde.webapp.Component#getCompositions
	 * @model opposite="compositions" transient="false"
	 * @generated
	 */
	Component getOwner();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.webapp.Composition#getOwner <em>Owner</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Owner</em>' container reference.
	 * @see #getOwner()
	 * @generated
	 */
	void setOwner(Component value);

} // Composition
