/**
 */
package edu.ustb.sei.mde.webapp;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>JQuery Event Action</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.webapp.JQueryEventAction#getSelectorCode <em>Selector Code</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.JQueryEventAction#getEvent <em>Event</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.JQueryEventAction#getHandler <em>Handler</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getJQueryEventAction()
 * @model
 * @generated
 */
public interface JQueryEventAction extends JavascriptAction {
	/**
	 * Returns the value of the '<em><b>Selector Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Selector Code</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Selector Code</em>' attribute.
	 * @see #setSelectorCode(String)
	 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getJQueryEventAction_SelectorCode()
	 * @model required="true"
	 * @generated
	 */
	String getSelectorCode();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.webapp.JQueryEventAction#getSelectorCode <em>Selector Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Selector Code</em>' attribute.
	 * @see #getSelectorCode()
	 * @generated
	 */
	void setSelectorCode(String value);

	/**
	 * Returns the value of the '<em><b>Event</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Event</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Event</em>' attribute.
	 * @see #setEvent(String)
	 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getJQueryEventAction_Event()
	 * @model required="true"
	 * @generated
	 */
	String getEvent();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.webapp.JQueryEventAction#getEvent <em>Event</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Event</em>' attribute.
	 * @see #getEvent()
	 * @generated
	 */
	void setEvent(String value);

	/**
	 * Returns the value of the '<em><b>Handler</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Handler</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Handler</em>' containment reference.
	 * @see #setHandler(OperationRealization)
	 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getJQueryEventAction_Handler()
	 * @model containment="true" required="true"
	 * @generated
	 */
	OperationRealization getHandler();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.webapp.JQueryEventAction#getHandler <em>Handler</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Handler</em>' containment reference.
	 * @see #getHandler()
	 * @generated
	 */
	void setHandler(OperationRealization value);

} // JQueryEventAction
