/**
 */
package edu.ustb.sei.mde.webapp;

import org.eclipse.emf.ecore.EObject;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Database</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.webapp.Database#getDriverName <em>Driver Name</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.Database#getUrl <em>Url</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.Database#getUsername <em>Username</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.Database#getPassword <em>Password</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.Database#getInitialSize <em>Initial Size</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.Database#getMinIdle <em>Min Idle</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.Database#getMaxIdle <em>Max Idle</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.Database#getMaxActive <em>Max Active</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.Database#getMaxWait <em>Max Wait</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.Database#getDatamodel <em>Datamodel</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getDatabase()
 * @model
 * @generated
 */
public interface Database extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Driver Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Driver Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Driver Name</em>' attribute.
	 * @see #setDriverName(String)
	 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getDatabase_DriverName()
	 * @model required="true"
	 * @generated
	 */
	String getDriverName();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.webapp.Database#getDriverName <em>Driver Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Driver Name</em>' attribute.
	 * @see #getDriverName()
	 * @generated
	 */
	void setDriverName(String value);

	/**
	 * Returns the value of the '<em><b>Url</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Url</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Url</em>' attribute.
	 * @see #setUrl(String)
	 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getDatabase_Url()
	 * @model required="true"
	 * @generated
	 */
	String getUrl();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.webapp.Database#getUrl <em>Url</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Url</em>' attribute.
	 * @see #getUrl()
	 * @generated
	 */
	void setUrl(String value);

	/**
	 * Returns the value of the '<em><b>Username</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Username</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Username</em>' attribute.
	 * @see #setUsername(String)
	 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getDatabase_Username()
	 * @model required="true"
	 * @generated
	 */
	String getUsername();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.webapp.Database#getUsername <em>Username</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Username</em>' attribute.
	 * @see #getUsername()
	 * @generated
	 */
	void setUsername(String value);

	/**
	 * Returns the value of the '<em><b>Password</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Password</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Password</em>' attribute.
	 * @see #setPassword(String)
	 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getDatabase_Password()
	 * @model required="true"
	 * @generated
	 */
	String getPassword();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.webapp.Database#getPassword <em>Password</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Password</em>' attribute.
	 * @see #getPassword()
	 * @generated
	 */
	void setPassword(String value);

	/**
	 * Returns the value of the '<em><b>Initial Size</b></em>' attribute.
	 * The default value is <code>"50"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Initial Size</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Initial Size</em>' attribute.
	 * @see #setInitialSize(int)
	 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getDatabase_InitialSize()
	 * @model default="50" required="true"
	 * @generated
	 */
	int getInitialSize();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.webapp.Database#getInitialSize <em>Initial Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Initial Size</em>' attribute.
	 * @see #getInitialSize()
	 * @generated
	 */
	void setInitialSize(int value);

	/**
	 * Returns the value of the '<em><b>Min Idle</b></em>' attribute.
	 * The default value is <code>"10"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Min Idle</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Min Idle</em>' attribute.
	 * @see #setMinIdle(int)
	 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getDatabase_MinIdle()
	 * @model default="10" required="true"
	 * @generated
	 */
	int getMinIdle();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.webapp.Database#getMinIdle <em>Min Idle</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Min Idle</em>' attribute.
	 * @see #getMinIdle()
	 * @generated
	 */
	void setMinIdle(int value);

	/**
	 * Returns the value of the '<em><b>Max Idle</b></em>' attribute.
	 * The default value is <code>"40"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Max Idle</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Max Idle</em>' attribute.
	 * @see #setMaxIdle(int)
	 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getDatabase_MaxIdle()
	 * @model default="40" required="true"
	 * @generated
	 */
	int getMaxIdle();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.webapp.Database#getMaxIdle <em>Max Idle</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Max Idle</em>' attribute.
	 * @see #getMaxIdle()
	 * @generated
	 */
	void setMaxIdle(int value);

	/**
	 * Returns the value of the '<em><b>Max Active</b></em>' attribute.
	 * The default value is <code>"100"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Max Active</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Max Active</em>' attribute.
	 * @see #setMaxActive(int)
	 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getDatabase_MaxActive()
	 * @model default="100" required="true"
	 * @generated
	 */
	int getMaxActive();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.webapp.Database#getMaxActive <em>Max Active</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Max Active</em>' attribute.
	 * @see #getMaxActive()
	 * @generated
	 */
	void setMaxActive(int value);

	/**
	 * Returns the value of the '<em><b>Max Wait</b></em>' attribute.
	 * The default value is <code>"10000"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Max Wait</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Max Wait</em>' attribute.
	 * @see #setMaxWait(long)
	 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getDatabase_MaxWait()
	 * @model default="10000" required="true"
	 * @generated
	 */
	long getMaxWait();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.webapp.Database#getMaxWait <em>Max Wait</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Max Wait</em>' attribute.
	 * @see #getMaxWait()
	 * @generated
	 */
	void setMaxWait(long value);

	/**
	 * Returns the value of the '<em><b>Datamodel</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Datamodel</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Datamodel</em>' reference.
	 * @see #setDatamodel(EObject)
	 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getDatabase_Datamodel()
	 * @model
	 * @generated
	 */
	EObject getDatamodel();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.webapp.Database#getDatamodel <em>Datamodel</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Datamodel</em>' reference.
	 * @see #getDatamodel()
	 * @generated
	 */
	void setDatamodel(EObject value);

} // Database
