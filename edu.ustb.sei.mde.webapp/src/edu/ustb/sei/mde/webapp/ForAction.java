/**
 */
package edu.ustb.sei.mde.webapp;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>For Action</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.webapp.ForAction#getConditionCode <em>Condition Code</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.ForAction#getBodyAction <em>Body Action</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getForAction()
 * @model
 * @generated
 */
public interface ForAction extends Action, StatementContainer {
	/**
	 * Returns the value of the '<em><b>Condition Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Condition Code</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Condition Code</em>' attribute.
	 * @see #setConditionCode(String)
	 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getForAction_ConditionCode()
	 * @model required="true"
	 * @generated
	 */
	String getConditionCode();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.webapp.ForAction#getConditionCode <em>Condition Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Condition Code</em>' attribute.
	 * @see #getConditionCode()
	 * @generated
	 */
	void setConditionCode(String value);

	/**
	 * Returns the value of the '<em><b>Body Action</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Body Action</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Body Action</em>' containment reference.
	 * @see #setBodyAction(Action)
	 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getForAction_BodyAction()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Action getBodyAction();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.webapp.ForAction#getBodyAction <em>Body Action</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Body Action</em>' containment reference.
	 * @see #getBodyAction()
	 * @generated
	 */
	void setBodyAction(Action value);

} // ForAction
