/**
 */
package edu.ustb.sei.mde.webapp;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Statement Container</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getStatementContainer()
 * @model abstract="true"
 * @generated
 */
public interface StatementContainer extends EObject {
} // StatementContainer
