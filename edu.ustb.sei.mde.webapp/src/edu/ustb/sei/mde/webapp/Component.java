/**
 */
package edu.ustb.sei.mde.webapp;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Component</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.webapp.Component#getOperations <em>Operations</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.Component#getRequiredInterfaces <em>Required Interfaces</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.Component#getProvidedInterfaces <em>Provided Interfaces</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.Component#getCompositions <em>Compositions</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getComponent()
 * @model abstract="true"
 * @generated
 */
public interface Component extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Operations</b></em>' containment reference list.
	 * The list contents are of type {@link edu.ustb.sei.mde.webapp.OperationRealization}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operations</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operations</em>' containment reference list.
	 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getComponent_Operations()
	 * @model containment="true"
	 * @generated
	 */
	EList<OperationRealization> getOperations();

	/**
	 * Returns the value of the '<em><b>Required Interfaces</b></em>' reference list.
	 * The list contents are of type {@link edu.ustb.sei.mde.webapp.Interface}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Required Interfaces</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Required Interfaces</em>' reference list.
	 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getComponent_RequiredInterfaces()
	 * @model
	 * @generated
	 */
	EList<Interface> getRequiredInterfaces();

	/**
	 * Returns the value of the '<em><b>Provided Interfaces</b></em>' reference list.
	 * The list contents are of type {@link edu.ustb.sei.mde.webapp.Interface}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Provided Interfaces</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Provided Interfaces</em>' reference list.
	 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getComponent_ProvidedInterfaces()
	 * @model
	 * @generated
	 */
	EList<Interface> getProvidedInterfaces();

	/**
	 * Returns the value of the '<em><b>Compositions</b></em>' containment reference list.
	 * The list contents are of type {@link edu.ustb.sei.mde.webapp.Composition}.
	 * It is bidirectional and its opposite is '{@link edu.ustb.sei.mde.webapp.Composition#getOwner <em>Owner</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Compositions</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Compositions</em>' containment reference list.
	 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getComponent_Compositions()
	 * @see edu.ustb.sei.mde.webapp.Composition#getOwner
	 * @model opposite="owner" containment="true"
	 * @generated
	 */
	EList<Composition> getCompositions();

} // Component
