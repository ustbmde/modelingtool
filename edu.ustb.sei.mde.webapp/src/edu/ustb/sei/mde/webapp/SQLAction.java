/**
 */
package edu.ustb.sei.mde.webapp;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>SQL Action</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.webapp.SQLAction#getDatabase <em>Database</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.SQLAction#getSqlName <em>Sql Name</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.SQLAction#getHandlingAction <em>Handling Action</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getSQLAction()
 * @model abstract="true"
 * @generated
 */
public interface SQLAction extends Action, StatementContainer {
	/**
	 * Returns the value of the '<em><b>Database</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Database</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Database</em>' reference.
	 * @see #setDatabase(Database)
	 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getSQLAction_Database()
	 * @model
	 * @generated
	 */
	Database getDatabase();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.webapp.SQLAction#getDatabase <em>Database</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Database</em>' reference.
	 * @see #getDatabase()
	 * @generated
	 */
	void setDatabase(Database value);

	/**
	 * Returns the value of the '<em><b>Sql Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sql Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sql Name</em>' attribute.
	 * @see #setSqlName(String)
	 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getSQLAction_SqlName()
	 * @model required="true"
	 * @generated
	 */
	String getSqlName();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.webapp.SQLAction#getSqlName <em>Sql Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sql Name</em>' attribute.
	 * @see #getSqlName()
	 * @generated
	 */
	void setSqlName(String value);

	/**
	 * Returns the value of the '<em><b>Handling Action</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Handling Action</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Handling Action</em>' containment reference.
	 * @see #setHandlingAction(Action)
	 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getSQLAction_HandlingAction()
	 * @model containment="true"
	 * @generated
	 */
	Action getHandlingAction();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.webapp.SQLAction#getHandlingAction <em>Handling Action</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Handling Action</em>' containment reference.
	 * @see #getHandlingAction()
	 * @generated
	 */
	void setHandlingAction(Action value);

} // SQLAction
