/**
 */
package edu.ustb.sei.mde.webapp;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Operation Realization</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.webapp.OperationRealization#getDeclaration <em>Declaration</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getOperationRealization()
 * @model abstract="true"
 * @generated
 */
public interface OperationRealization extends Operation {
	/**
	 * Returns the value of the '<em><b>Declaration</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Declaration</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Declaration</em>' reference.
	 * @see #setDeclaration(OperationDeclaration)
	 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getOperationRealization_Declaration()
	 * @model
	 * @generated
	 */
	OperationDeclaration getDeclaration();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.webapp.OperationRealization#getDeclaration <em>Declaration</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Declaration</em>' reference.
	 * @see #getDeclaration()
	 * @generated
	 */
	void setDeclaration(OperationDeclaration value);

} // OperationRealization
