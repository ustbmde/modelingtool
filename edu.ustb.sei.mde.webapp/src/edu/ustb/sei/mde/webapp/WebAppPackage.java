/**
 */
package edu.ustb.sei.mde.webapp;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see edu.ustb.sei.mde.webapp.WebAppFactory
 * @model kind="package"
 * @generated
 */
public interface WebAppPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "webapp";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.ustb.edu.cn/sei/mde/webapp";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "webapp";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	WebAppPackage eINSTANCE = edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl.init();

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.webapp.impl.NamedElementImpl <em>Named Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.webapp.impl.NamedElementImpl
	 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getNamedElement()
	 * @generated
	 */
	int NAMED_ELEMENT = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ELEMENT__NAME = 0;

	/**
	 * The number of structural features of the '<em>Named Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ELEMENT_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Named Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ELEMENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.webapp.impl.TypedElementImpl <em>Typed Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.webapp.impl.TypedElementImpl
	 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getTypedElement()
	 * @generated
	 */
	int TYPED_ELEMENT = 1;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPED_ELEMENT__TYPE = 0;

	/**
	 * The number of structural features of the '<em>Typed Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPED_ELEMENT_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Typed Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TYPED_ELEMENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.webapp.impl.DeployableImpl <em>Deployable</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.webapp.impl.DeployableImpl
	 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getDeployable()
	 * @generated
	 */
	int DEPLOYABLE = 2;

	/**
	 * The feature id for the '<em><b>Deploy Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPLOYABLE__DEPLOY_PATH = 0;

	/**
	 * The number of structural features of the '<em>Deployable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPLOYABLE_FEATURE_COUNT = 1;

	/**
	 * The operation id for the '<em>Get URL</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPLOYABLE___GET_URL = 0;

	/**
	 * The number of operations of the '<em>Deployable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPLOYABLE_OPERATION_COUNT = 1;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.webapp.impl.DeployablePointerImpl <em>Deployable Pointer</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.webapp.impl.DeployablePointerImpl
	 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getDeployablePointer()
	 * @generated
	 */
	int DEPLOYABLE_POINTER = 3;

	/**
	 * The feature id for the '<em><b>Url</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPLOYABLE_POINTER__URL = 0;

	/**
	 * The feature id for the '<em><b>Deployable</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPLOYABLE_POINTER__DEPLOYABLE = 1;

	/**
	 * The number of structural features of the '<em>Deployable Pointer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPLOYABLE_POINTER_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Deployable Pointer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPLOYABLE_POINTER_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.webapp.impl.InterfaceImpl <em>Interface</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.webapp.impl.InterfaceImpl
	 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getInterface()
	 * @generated
	 */
	int INTERFACE = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERFACE__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Operations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERFACE__OPERATIONS = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Interface</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERFACE_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Interface</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int INTERFACE_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.webapp.impl.OperationImpl <em>Operation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.webapp.impl.OperationImpl
	 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getOperation()
	 * @generated
	 */
	int OPERATION = 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION__TYPE = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION__PARAMETERS = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.webapp.impl.OperationDeclarationImpl <em>Operation Declaration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.webapp.impl.OperationDeclarationImpl
	 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getOperationDeclaration()
	 * @generated
	 */
	int OPERATION_DECLARATION = 6;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_DECLARATION__NAME = OPERATION__NAME;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_DECLARATION__TYPE = OPERATION__TYPE;

	/**
	 * The feature id for the '<em><b>Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_DECLARATION__PARAMETERS = OPERATION__PARAMETERS;

	/**
	 * The number of structural features of the '<em>Operation Declaration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_DECLARATION_FEATURE_COUNT = OPERATION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Operation Declaration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_DECLARATION_OPERATION_COUNT = OPERATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.webapp.impl.ParameterImpl <em>Parameter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.webapp.impl.ParameterImpl
	 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getParameter()
	 * @generated
	 */
	int PARAMETER = 7;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER__TYPE = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Parameter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Parameter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARAMETER_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.webapp.impl.ComponentImpl <em>Component</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.webapp.impl.ComponentImpl
	 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getComponent()
	 * @generated
	 */
	int COMPONENT = 8;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Operations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT__OPERATIONS = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Required Interfaces</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT__REQUIRED_INTERFACES = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Provided Interfaces</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT__PROVIDED_INTERFACES = NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Compositions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT__COMPOSITIONS = NAMED_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Component</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>Component</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.webapp.impl.OperationRealizationImpl <em>Operation Realization</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.webapp.impl.OperationRealizationImpl
	 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getOperationRealization()
	 * @generated
	 */
	int OPERATION_REALIZATION = 9;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_REALIZATION__NAME = OPERATION__NAME;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_REALIZATION__TYPE = OPERATION__TYPE;

	/**
	 * The feature id for the '<em><b>Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_REALIZATION__PARAMETERS = OPERATION__PARAMETERS;

	/**
	 * The feature id for the '<em><b>Declaration</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_REALIZATION__DECLARATION = OPERATION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Operation Realization</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_REALIZATION_FEATURE_COUNT = OPERATION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Operation Realization</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_REALIZATION_OPERATION_COUNT = OPERATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.webapp.impl.CodeOperationImpl <em>Code Operation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.webapp.impl.CodeOperationImpl
	 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getCodeOperation()
	 * @generated
	 */
	int CODE_OPERATION = 10;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODE_OPERATION__NAME = OPERATION_REALIZATION__NAME;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODE_OPERATION__TYPE = OPERATION_REALIZATION__TYPE;

	/**
	 * The feature id for the '<em><b>Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODE_OPERATION__PARAMETERS = OPERATION_REALIZATION__PARAMETERS;

	/**
	 * The feature id for the '<em><b>Declaration</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODE_OPERATION__DECLARATION = OPERATION_REALIZATION__DECLARATION;

	/**
	 * The feature id for the '<em><b>Body Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODE_OPERATION__BODY_CODE = OPERATION_REALIZATION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Code Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODE_OPERATION_FEATURE_COUNT = OPERATION_REALIZATION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Code Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODE_OPERATION_OPERATION_COUNT = OPERATION_REALIZATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.webapp.impl.ActionOperationImpl <em>Action Operation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.webapp.impl.ActionOperationImpl
	 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getActionOperation()
	 * @generated
	 */
	int ACTION_OPERATION = 11;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_OPERATION__NAME = OPERATION_REALIZATION__NAME;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_OPERATION__TYPE = OPERATION_REALIZATION__TYPE;

	/**
	 * The feature id for the '<em><b>Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_OPERATION__PARAMETERS = OPERATION_REALIZATION__PARAMETERS;

	/**
	 * The feature id for the '<em><b>Declaration</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_OPERATION__DECLARATION = OPERATION_REALIZATION__DECLARATION;

	/**
	 * The feature id for the '<em><b>Action</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_OPERATION__ACTION = OPERATION_REALIZATION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Action Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_OPERATION_FEATURE_COUNT = OPERATION_REALIZATION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Action Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_OPERATION_OPERATION_COUNT = OPERATION_REALIZATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.webapp.impl.ActionImpl <em>Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.webapp.impl.ActionImpl
	 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getAction()
	 * @generated
	 */
	int ACTION = 12;

	/**
	 * The number of structural features of the '<em>Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.webapp.impl.PageComponentImpl <em>Page Component</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.webapp.impl.PageComponentImpl
	 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getPageComponent()
	 * @generated
	 */
	int PAGE_COMPONENT = 13;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAGE_COMPONENT__NAME = COMPONENT__NAME;

	/**
	 * The feature id for the '<em><b>Operations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAGE_COMPONENT__OPERATIONS = COMPONENT__OPERATIONS;

	/**
	 * The feature id for the '<em><b>Required Interfaces</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAGE_COMPONENT__REQUIRED_INTERFACES = COMPONENT__REQUIRED_INTERFACES;

	/**
	 * The feature id for the '<em><b>Provided Interfaces</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAGE_COMPONENT__PROVIDED_INTERFACES = COMPONENT__PROVIDED_INTERFACES;

	/**
	 * The feature id for the '<em><b>Compositions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAGE_COMPONENT__COMPOSITIONS = COMPONENT__COMPOSITIONS;

	/**
	 * The feature id for the '<em><b>Deploy Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAGE_COMPONENT__DEPLOY_PATH = COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Node</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAGE_COMPONENT__NODE = COMPONENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Page</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAGE_COMPONENT__PAGE = COMPONENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Page Component</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAGE_COMPONENT_FEATURE_COUNT = COMPONENT_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Get URL</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAGE_COMPONENT___GET_URL = COMPONENT_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>Page Component</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAGE_COMPONENT_OPERATION_COUNT = COMPONENT_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.webapp.impl.HTMLNodeImpl <em>HTML Node</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.webapp.impl.HTMLNodeImpl
	 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getHTMLNode()
	 * @generated
	 */
	int HTML_NODE = 14;

	/**
	 * The feature id for the '<em><b>Tag</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HTML_NODE__TAG = 0;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HTML_NODE__ID = 1;

	/**
	 * The feature id for the '<em><b>Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HTML_NODE__CLASS = 2;

	/**
	 * The feature id for the '<em><b>Extra</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HTML_NODE__EXTRA = 3;

	/**
	 * The feature id for the '<em><b>Inner Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HTML_NODE__INNER_TEXT = 4;

	/**
	 * The feature id for the '<em><b>Children</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HTML_NODE__CHILDREN = 5;

	/**
	 * The feature id for the '<em><b>Handlers</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HTML_NODE__HANDLERS = 6;

	/**
	 * The feature id for the '<em><b>Styles</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HTML_NODE__STYLES = 7;

	/**
	 * The number of structural features of the '<em>HTML Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HTML_NODE_FEATURE_COUNT = 8;

	/**
	 * The number of operations of the '<em>HTML Node</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HTML_NODE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.webapp.impl.CompositionImpl <em>Composition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.webapp.impl.CompositionImpl
	 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getComposition()
	 * @generated
	 */
	int COMPOSITION = 15;

	/**
	 * The feature id for the '<em><b>Url</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITION__URL = DEPLOYABLE_POINTER__URL;

	/**
	 * The feature id for the '<em><b>Deployable</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITION__DEPLOYABLE = DEPLOYABLE_POINTER__DEPLOYABLE;

	/**
	 * The feature id for the '<em><b>Owner</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITION__OWNER = DEPLOYABLE_POINTER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Composition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITION_FEATURE_COUNT = DEPLOYABLE_POINTER_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Composition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITION_OPERATION_COUNT = DEPLOYABLE_POINTER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.webapp.impl.PageComponentCompositionImpl <em>Page Component Composition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.webapp.impl.PageComponentCompositionImpl
	 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getPageComponentComposition()
	 * @generated
	 */
	int PAGE_COMPONENT_COMPOSITION = 16;

	/**
	 * The feature id for the '<em><b>Url</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAGE_COMPONENT_COMPOSITION__URL = COMPOSITION__URL;

	/**
	 * The feature id for the '<em><b>Deployable</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAGE_COMPONENT_COMPOSITION__DEPLOYABLE = COMPOSITION__DEPLOYABLE;

	/**
	 * The feature id for the '<em><b>Owner</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAGE_COMPONENT_COMPOSITION__OWNER = COMPOSITION__OWNER;

	/**
	 * The feature id for the '<em><b>Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAGE_COMPONENT_COMPOSITION__TIME = COMPOSITION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Container Selector</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAGE_COMPONENT_COMPOSITION__CONTAINER_SELECTOR = COMPOSITION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Page Component Composition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAGE_COMPONENT_COMPOSITION_FEATURE_COUNT = COMPOSITION_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Page Component Composition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAGE_COMPONENT_COMPOSITION_OPERATION_COUNT = COMPOSITION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.webapp.impl.EventHandlerImpl <em>Event Handler</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.webapp.impl.EventHandlerImpl
	 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getEventHandler()
	 * @generated
	 */
	int EVENT_HANDLER = 17;

	/**
	 * The feature id for the '<em><b>Event</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_HANDLER__EVENT = 0;

	/**
	 * The feature id for the '<em><b>Owner</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_HANDLER__OWNER = 1;

	/**
	 * The feature id for the '<em><b>Handler</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_HANDLER__HANDLER = 2;

	/**
	 * The number of structural features of the '<em>Event Handler</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_HANDLER_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Event Handler</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_HANDLER_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.webapp.impl.ServerComponentImpl <em>Server Component</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.webapp.impl.ServerComponentImpl
	 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getServerComponent()
	 * @generated
	 */
	int SERVER_COMPONENT = 18;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER_COMPONENT__NAME = COMPONENT__NAME;

	/**
	 * The feature id for the '<em><b>Operations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER_COMPONENT__OPERATIONS = COMPONENT__OPERATIONS;

	/**
	 * The feature id for the '<em><b>Required Interfaces</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER_COMPONENT__REQUIRED_INTERFACES = COMPONENT__REQUIRED_INTERFACES;

	/**
	 * The feature id for the '<em><b>Provided Interfaces</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER_COMPONENT__PROVIDED_INTERFACES = COMPONENT__PROVIDED_INTERFACES;

	/**
	 * The feature id for the '<em><b>Compositions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER_COMPONENT__COMPOSITIONS = COMPONENT__COMPOSITIONS;

	/**
	 * The feature id for the '<em><b>Deploy Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER_COMPONENT__DEPLOY_PATH = COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Imports</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER_COMPONENT__IMPORTS = COMPONENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Content Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER_COMPONENT__CONTENT_TYPE = COMPONENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Server Component</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER_COMPONENT_FEATURE_COUNT = COMPONENT_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Get URL</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER_COMPONENT___GET_URL = COMPONENT_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>Server Component</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER_COMPONENT_OPERATION_COUNT = COMPONENT_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.webapp.impl.ClassComponentImpl <em>Class Component</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.webapp.impl.ClassComponentImpl
	 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getClassComponent()
	 * @generated
	 */
	int CLASS_COMPONENT = 19;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_COMPONENT__NAME = COMPONENT__NAME;

	/**
	 * The feature id for the '<em><b>Operations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_COMPONENT__OPERATIONS = COMPONENT__OPERATIONS;

	/**
	 * The feature id for the '<em><b>Required Interfaces</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_COMPONENT__REQUIRED_INTERFACES = COMPONENT__REQUIRED_INTERFACES;

	/**
	 * The feature id for the '<em><b>Provided Interfaces</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_COMPONENT__PROVIDED_INTERFACES = COMPONENT__PROVIDED_INTERFACES;

	/**
	 * The feature id for the '<em><b>Compositions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_COMPONENT__COMPOSITIONS = COMPONENT__COMPOSITIONS;

	/**
	 * The feature id for the '<em><b>Package Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_COMPONENT__PACKAGE_NAME = COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Imports</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_COMPONENT__IMPORTS = COMPONENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Class Component</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_COMPONENT_FEATURE_COUNT = COMPONENT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Class Component</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLASS_COMPONENT_OPERATION_COUNT = COMPONENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.webapp.impl.StatementContainerImpl <em>Statement Container</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.webapp.impl.StatementContainerImpl
	 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getStatementContainer()
	 * @generated
	 */
	int STATEMENT_CONTAINER = 20;

	/**
	 * The number of structural features of the '<em>Statement Container</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATEMENT_CONTAINER_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Statement Container</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATEMENT_CONTAINER_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.webapp.impl.DatabaseImpl <em>Database</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.webapp.impl.DatabaseImpl
	 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getDatabase()
	 * @generated
	 */
	int DATABASE = 21;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATABASE__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Driver Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATABASE__DRIVER_NAME = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Url</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATABASE__URL = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Username</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATABASE__USERNAME = NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Password</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATABASE__PASSWORD = NAMED_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Initial Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATABASE__INITIAL_SIZE = NAMED_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Min Idle</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATABASE__MIN_IDLE = NAMED_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Max Idle</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATABASE__MAX_IDLE = NAMED_ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Max Active</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATABASE__MAX_ACTIVE = NAMED_ELEMENT_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Max Wait</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATABASE__MAX_WAIT = NAMED_ELEMENT_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Datamodel</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATABASE__DATAMODEL = NAMED_ELEMENT_FEATURE_COUNT + 9;

	/**
	 * The number of structural features of the '<em>Database</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATABASE_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 10;

	/**
	 * The number of operations of the '<em>Database</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATABASE_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.webapp.impl.CodeActionImpl <em>Code Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.webapp.impl.CodeActionImpl
	 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getCodeAction()
	 * @generated
	 */
	int CODE_ACTION = 22;

	/**
	 * The feature id for the '<em><b>Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODE_ACTION__CODE = ACTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Code Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODE_ACTION_FEATURE_COUNT = ACTION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Code Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CODE_ACTION_OPERATION_COUNT = ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.webapp.impl.CallActionImpl <em>Call Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.webapp.impl.CallActionImpl
	 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getCallAction()
	 * @generated
	 */
	int CALL_ACTION = 23;

	/**
	 * The feature id for the '<em><b>Invocation Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_ACTION__INVOCATION_TARGET = ACTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Parameter Codes</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_ACTION__PARAMETER_CODES = ACTION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Call Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_ACTION_FEATURE_COUNT = ACTION_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Call Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CALL_ACTION_OPERATION_COUNT = ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.webapp.impl.LetActionImpl <em>Let Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.webapp.impl.LetActionImpl
	 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getLetAction()
	 * @generated
	 */
	int LET_ACTION = 24;

	/**
	 * The feature id for the '<em><b>Left Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LET_ACTION__LEFT_CODE = ACTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Right</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LET_ACTION__RIGHT = ACTION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Let Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LET_ACTION_FEATURE_COUNT = ACTION_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Let Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LET_ACTION_OPERATION_COUNT = ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.webapp.impl.ForActionImpl <em>For Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.webapp.impl.ForActionImpl
	 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getForAction()
	 * @generated
	 */
	int FOR_ACTION = 25;

	/**
	 * The feature id for the '<em><b>Condition Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_ACTION__CONDITION_CODE = ACTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Body Action</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_ACTION__BODY_ACTION = ACTION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>For Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_ACTION_FEATURE_COUNT = ACTION_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>For Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FOR_ACTION_OPERATION_COUNT = ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.webapp.impl.IfActionImpl <em>If Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.webapp.impl.IfActionImpl
	 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getIfAction()
	 * @generated
	 */
	int IF_ACTION = 26;

	/**
	 * The feature id for the '<em><b>Condition Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ACTION__CONDITION_CODE = ACTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Then Action</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ACTION__THEN_ACTION = ACTION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Else Action</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ACTION__ELSE_ACTION = ACTION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>If Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ACTION_FEATURE_COUNT = ACTION_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>If Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_ACTION_OPERATION_COUNT = ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.webapp.impl.SequenceActionImpl <em>Sequence Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.webapp.impl.SequenceActionImpl
	 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getSequenceAction()
	 * @generated
	 */
	int SEQUENCE_ACTION = 27;

	/**
	 * The feature id for the '<em><b>Actions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE_ACTION__ACTIONS = ACTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Sequence Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE_ACTION_FEATURE_COUNT = ACTION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Sequence Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE_ACTION_OPERATION_COUNT = ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.webapp.impl.TryCatchActionImpl <em>Try Catch Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.webapp.impl.TryCatchActionImpl
	 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getTryCatchAction()
	 * @generated
	 */
	int TRY_CATCH_ACTION = 28;

	/**
	 * The feature id for the '<em><b>Try Part</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRY_CATCH_ACTION__TRY_PART = ACTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Catch Parts</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRY_CATCH_ACTION__CATCH_PARTS = ACTION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Final Part</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRY_CATCH_ACTION__FINAL_PART = ACTION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Try Catch Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRY_CATCH_ACTION_FEATURE_COUNT = ACTION_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Try Catch Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRY_CATCH_ACTION_OPERATION_COUNT = ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.webapp.impl.CatchPartImpl <em>Catch Part</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.webapp.impl.CatchPartImpl
	 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getCatchPart()
	 * @generated
	 */
	int CATCH_PART = 29;

	/**
	 * The feature id for the '<em><b>Exception Variable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATCH_PART__EXCEPTION_VARIABLE = STATEMENT_CONTAINER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Actioin</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATCH_PART__ACTIOIN = STATEMENT_CONTAINER_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Catch Part</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATCH_PART_FEATURE_COUNT = STATEMENT_CONTAINER_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Catch Part</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CATCH_PART_OPERATION_COUNT = STATEMENT_CONTAINER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.webapp.impl.JavascriptActionImpl <em>Javascript Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.webapp.impl.JavascriptActionImpl
	 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getJavascriptAction()
	 * @generated
	 */
	int JAVASCRIPT_ACTION = 30;

	/**
	 * The number of structural features of the '<em>Javascript Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVASCRIPT_ACTION_FEATURE_COUNT = ACTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Javascript Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JAVASCRIPT_ACTION_OPERATION_COUNT = ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.webapp.impl.JQueryAjaxActionImpl <em>JQuery Ajax Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.webapp.impl.JQueryAjaxActionImpl
	 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getJQueryAjaxAction()
	 * @generated
	 */
	int JQUERY_AJAX_ACTION = 31;

	/**
	 * The feature id for the '<em><b>Url</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JQUERY_AJAX_ACTION__URL = JAVASCRIPT_ACTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Deployable</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JQUERY_AJAX_ACTION__DEPLOYABLE = JAVASCRIPT_ACTION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Selector Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JQUERY_AJAX_ACTION__SELECTOR_CODE = JAVASCRIPT_ACTION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Ajax Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JQUERY_AJAX_ACTION__AJAX_TYPE = JAVASCRIPT_ACTION_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Data Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JQUERY_AJAX_ACTION__DATA_CODE = JAVASCRIPT_ACTION_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Data Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JQUERY_AJAX_ACTION__DATA_TYPE = JAVASCRIPT_ACTION_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Callback Function</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JQUERY_AJAX_ACTION__CALLBACK_FUNCTION = JAVASCRIPT_ACTION_FEATURE_COUNT + 6;

	/**
	 * The number of structural features of the '<em>JQuery Ajax Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JQUERY_AJAX_ACTION_FEATURE_COUNT = JAVASCRIPT_ACTION_FEATURE_COUNT + 7;

	/**
	 * The number of operations of the '<em>JQuery Ajax Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JQUERY_AJAX_ACTION_OPERATION_COUNT = JAVASCRIPT_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.webapp.impl.JQueryEventActionImpl <em>JQuery Event Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.webapp.impl.JQueryEventActionImpl
	 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getJQueryEventAction()
	 * @generated
	 */
	int JQUERY_EVENT_ACTION = 32;

	/**
	 * The feature id for the '<em><b>Selector Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JQUERY_EVENT_ACTION__SELECTOR_CODE = JAVASCRIPT_ACTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Event</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JQUERY_EVENT_ACTION__EVENT = JAVASCRIPT_ACTION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Handler</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JQUERY_EVENT_ACTION__HANDLER = JAVASCRIPT_ACTION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>JQuery Event Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JQUERY_EVENT_ACTION_FEATURE_COUNT = JAVASCRIPT_ACTION_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>JQuery Event Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int JQUERY_EVENT_ACTION_OPERATION_COUNT = JAVASCRIPT_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.webapp.impl.SQLActionImpl <em>SQL Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.webapp.impl.SQLActionImpl
	 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getSQLAction()
	 * @generated
	 */
	int SQL_ACTION = 33;

	/**
	 * The feature id for the '<em><b>Database</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SQL_ACTION__DATABASE = ACTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Sql Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SQL_ACTION__SQL_NAME = ACTION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Handling Action</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SQL_ACTION__HANDLING_ACTION = ACTION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>SQL Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SQL_ACTION_FEATURE_COUNT = ACTION_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>SQL Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SQL_ACTION_OPERATION_COUNT = ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.webapp.impl.SQLSelectActionImpl <em>SQL Select Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.webapp.impl.SQLSelectActionImpl
	 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getSQLSelectAction()
	 * @generated
	 */
	int SQL_SELECT_ACTION = 34;

	/**
	 * The feature id for the '<em><b>Database</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SQL_SELECT_ACTION__DATABASE = SQL_ACTION__DATABASE;

	/**
	 * The feature id for the '<em><b>Sql Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SQL_SELECT_ACTION__SQL_NAME = SQL_ACTION__SQL_NAME;

	/**
	 * The feature id for the '<em><b>Handling Action</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SQL_SELECT_ACTION__HANDLING_ACTION = SQL_ACTION__HANDLING_ACTION;

	/**
	 * The feature id for the '<em><b>Tables</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SQL_SELECT_ACTION__TABLES = SQL_ACTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Columns</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SQL_SELECT_ACTION__COLUMNS = SQL_ACTION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Condition Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SQL_SELECT_ACTION__CONDITION_CODE = SQL_ACTION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>SQL Select Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SQL_SELECT_ACTION_FEATURE_COUNT = SQL_ACTION_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>SQL Select Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SQL_SELECT_ACTION_OPERATION_COUNT = SQL_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.webapp.impl.SQLInsertActionImpl <em>SQL Insert Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.webapp.impl.SQLInsertActionImpl
	 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getSQLInsertAction()
	 * @generated
	 */
	int SQL_INSERT_ACTION = 35;

	/**
	 * The feature id for the '<em><b>Database</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SQL_INSERT_ACTION__DATABASE = SQL_ACTION__DATABASE;

	/**
	 * The feature id for the '<em><b>Sql Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SQL_INSERT_ACTION__SQL_NAME = SQL_ACTION__SQL_NAME;

	/**
	 * The feature id for the '<em><b>Handling Action</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SQL_INSERT_ACTION__HANDLING_ACTION = SQL_ACTION__HANDLING_ACTION;

	/**
	 * The feature id for the '<em><b>Table</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SQL_INSERT_ACTION__TABLE = SQL_ACTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Columns</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SQL_INSERT_ACTION__COLUMNS = SQL_ACTION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Value Codes</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SQL_INSERT_ACTION__VALUE_CODES = SQL_ACTION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>SQL Insert Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SQL_INSERT_ACTION_FEATURE_COUNT = SQL_ACTION_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>SQL Insert Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SQL_INSERT_ACTION_OPERATION_COUNT = SQL_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.webapp.impl.SQLUpdateActionImpl <em>SQL Update Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.webapp.impl.SQLUpdateActionImpl
	 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getSQLUpdateAction()
	 * @generated
	 */
	int SQL_UPDATE_ACTION = 36;

	/**
	 * The feature id for the '<em><b>Database</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SQL_UPDATE_ACTION__DATABASE = SQL_ACTION__DATABASE;

	/**
	 * The feature id for the '<em><b>Sql Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SQL_UPDATE_ACTION__SQL_NAME = SQL_ACTION__SQL_NAME;

	/**
	 * The feature id for the '<em><b>Handling Action</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SQL_UPDATE_ACTION__HANDLING_ACTION = SQL_ACTION__HANDLING_ACTION;

	/**
	 * The feature id for the '<em><b>Table</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SQL_UPDATE_ACTION__TABLE = SQL_ACTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Columns</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SQL_UPDATE_ACTION__COLUMNS = SQL_ACTION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Value Codes</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SQL_UPDATE_ACTION__VALUE_CODES = SQL_ACTION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Condition Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SQL_UPDATE_ACTION__CONDITION_CODE = SQL_ACTION_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>SQL Update Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SQL_UPDATE_ACTION_FEATURE_COUNT = SQL_ACTION_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>SQL Update Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SQL_UPDATE_ACTION_OPERATION_COUNT = SQL_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.webapp.impl.SQLDeleteActionImpl <em>SQL Delete Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.webapp.impl.SQLDeleteActionImpl
	 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getSQLDeleteAction()
	 * @generated
	 */
	int SQL_DELETE_ACTION = 37;

	/**
	 * The feature id for the '<em><b>Database</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SQL_DELETE_ACTION__DATABASE = SQL_ACTION__DATABASE;

	/**
	 * The feature id for the '<em><b>Sql Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SQL_DELETE_ACTION__SQL_NAME = SQL_ACTION__SQL_NAME;

	/**
	 * The feature id for the '<em><b>Handling Action</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SQL_DELETE_ACTION__HANDLING_ACTION = SQL_ACTION__HANDLING_ACTION;

	/**
	 * The feature id for the '<em><b>Table</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SQL_DELETE_ACTION__TABLE = SQL_ACTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Condition Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SQL_DELETE_ACTION__CONDITION_CODE = SQL_ACTION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>SQL Delete Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SQL_DELETE_ACTION_FEATURE_COUNT = SQL_ACTION_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>SQL Delete Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SQL_DELETE_ACTION_OPERATION_COUNT = SQL_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.webapp.impl.SQLExecutionActionImpl <em>SQL Execution Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.webapp.impl.SQLExecutionActionImpl
	 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getSQLExecutionAction()
	 * @generated
	 */
	int SQL_EXECUTION_ACTION = 38;

	/**
	 * The feature id for the '<em><b>Database</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SQL_EXECUTION_ACTION__DATABASE = SQL_ACTION__DATABASE;

	/**
	 * The feature id for the '<em><b>Sql Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SQL_EXECUTION_ACTION__SQL_NAME = SQL_ACTION__SQL_NAME;

	/**
	 * The feature id for the '<em><b>Handling Action</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SQL_EXECUTION_ACTION__HANDLING_ACTION = SQL_ACTION__HANDLING_ACTION;

	/**
	 * The feature id for the '<em><b>Sql Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SQL_EXECUTION_ACTION__SQL_CODE = SQL_ACTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>SQL Execution Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SQL_EXECUTION_ACTION_FEATURE_COUNT = SQL_ACTION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>SQL Execution Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SQL_EXECUTION_ACTION_OPERATION_COUNT = SQL_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.webapp.impl.SQLResultSetIterationActionImpl <em>SQL Result Set Iteration Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.webapp.impl.SQLResultSetIterationActionImpl
	 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getSQLResultSetIterationAction()
	 * @generated
	 */
	int SQL_RESULT_SET_ITERATION_ACTION = 39;

	/**
	 * The feature id for the '<em><b>Action</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SQL_RESULT_SET_ITERATION_ACTION__ACTION = ACTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>SQL Result Set Iteration Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SQL_RESULT_SET_ITERATION_ACTION_FEATURE_COUNT = ACTION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>SQL Result Set Iteration Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SQL_RESULT_SET_ITERATION_ACTION_OPERATION_COUNT = ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.webapp.impl.ServerPageActionImpl <em>Server Page Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.webapp.impl.ServerPageActionImpl
	 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getServerPageAction()
	 * @generated
	 */
	int SERVER_PAGE_ACTION = 40;

	/**
	 * The number of structural features of the '<em>Server Page Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER_PAGE_ACTION_FEATURE_COUNT = ACTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Server Page Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER_PAGE_ACTION_OPERATION_COUNT = ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.webapp.impl.ServerPageTemplateActionImpl <em>Server Page Template Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.webapp.impl.ServerPageTemplateActionImpl
	 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getServerPageTemplateAction()
	 * @generated
	 */
	int SERVER_PAGE_TEMPLATE_ACTION = 41;

	/**
	 * The feature id for the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER_PAGE_TEMPLATE_ACTION__TEXT = SERVER_PAGE_ACTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Server Page Template Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER_PAGE_TEMPLATE_ACTION_FEATURE_COUNT = SERVER_PAGE_ACTION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Server Page Template Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER_PAGE_TEMPLATE_ACTION_OPERATION_COUNT = SERVER_PAGE_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.webapp.impl.ServerPageExpressionActionImpl <em>Server Page Expression Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.webapp.impl.ServerPageExpressionActionImpl
	 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getServerPageExpressionAction()
	 * @generated
	 */
	int SERVER_PAGE_EXPRESSION_ACTION = 42;

	/**
	 * The feature id for the '<em><b>Expression</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER_PAGE_EXPRESSION_ACTION__EXPRESSION = SERVER_PAGE_ACTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Server Page Expression Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER_PAGE_EXPRESSION_ACTION_FEATURE_COUNT = SERVER_PAGE_ACTION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Server Page Expression Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER_PAGE_EXPRESSION_ACTION_OPERATION_COUNT = SERVER_PAGE_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.webapp.impl.ServerPageStructuredTemplateActionImpl <em>Server Page Structured Template Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.webapp.impl.ServerPageStructuredTemplateActionImpl
	 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getServerPageStructuredTemplateAction()
	 * @generated
	 */
	int SERVER_PAGE_STRUCTURED_TEMPLATE_ACTION = 43;

	/**
	 * The feature id for the '<em><b>Prefix</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER_PAGE_STRUCTURED_TEMPLATE_ACTION__PREFIX = SERVER_PAGE_ACTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Action</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER_PAGE_STRUCTURED_TEMPLATE_ACTION__ACTION = SERVER_PAGE_ACTION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Suffix</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER_PAGE_STRUCTURED_TEMPLATE_ACTION__SUFFIX = SERVER_PAGE_ACTION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Server Page Structured Template Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER_PAGE_STRUCTURED_TEMPLATE_ACTION_FEATURE_COUNT = SERVER_PAGE_ACTION_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Server Page Structured Template Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVER_PAGE_STRUCTURED_TEMPLATE_ACTION_OPERATION_COUNT = SERVER_PAGE_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.webapp.impl.WebApplicationImpl <em>Web Application</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.webapp.impl.WebApplicationImpl
	 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getWebApplication()
	 * @generated
	 */
	int WEB_APPLICATION = 44;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WEB_APPLICATION__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Database Util Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WEB_APPLICATION__DATABASE_UTIL_CLASS = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Databases</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WEB_APPLICATION__DATABASES = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Interfaces</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WEB_APPLICATION__INTERFACES = NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Page Components</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WEB_APPLICATION__PAGE_COMPONENTS = NAMED_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Server Components</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WEB_APPLICATION__SERVER_COMPONENTS = NAMED_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Class Components</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WEB_APPLICATION__CLASS_COMPONENTS = NAMED_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>Web Application</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WEB_APPLICATION_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The number of operations of the '<em>Web Application</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WEB_APPLICATION_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.webapp.AjaxType <em>Ajax Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.webapp.AjaxType
	 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getAjaxType()
	 * @generated
	 */
	int AJAX_TYPE = 45;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.webapp.PageContentType <em>Page Content Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.webapp.PageContentType
	 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getPageContentType()
	 * @generated
	 */
	int PAGE_CONTENT_TYPE = 46;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.webapp.DataFormat <em>Data Format</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.webapp.DataFormat
	 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getDataFormat()
	 * @generated
	 */
	int DATA_FORMAT = 47;


	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.webapp.NamedElement <em>Named Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Named Element</em>'.
	 * @see edu.ustb.sei.mde.webapp.NamedElement
	 * @generated
	 */
	EClass getNamedElement();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.webapp.NamedElement#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see edu.ustb.sei.mde.webapp.NamedElement#getName()
	 * @see #getNamedElement()
	 * @generated
	 */
	EAttribute getNamedElement_Name();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.webapp.TypedElement <em>Typed Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Typed Element</em>'.
	 * @see edu.ustb.sei.mde.webapp.TypedElement
	 * @generated
	 */
	EClass getTypedElement();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.webapp.TypedElement#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see edu.ustb.sei.mde.webapp.TypedElement#getType()
	 * @see #getTypedElement()
	 * @generated
	 */
	EAttribute getTypedElement_Type();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.webapp.Deployable <em>Deployable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Deployable</em>'.
	 * @see edu.ustb.sei.mde.webapp.Deployable
	 * @generated
	 */
	EClass getDeployable();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.webapp.Deployable#getDeployPath <em>Deploy Path</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Deploy Path</em>'.
	 * @see edu.ustb.sei.mde.webapp.Deployable#getDeployPath()
	 * @see #getDeployable()
	 * @generated
	 */
	EAttribute getDeployable_DeployPath();

	/**
	 * Returns the meta object for the '{@link edu.ustb.sei.mde.webapp.Deployable#getURL() <em>Get URL</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get URL</em>' operation.
	 * @see edu.ustb.sei.mde.webapp.Deployable#getURL()
	 * @generated
	 */
	EOperation getDeployable__GetURL();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.webapp.DeployablePointer <em>Deployable Pointer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Deployable Pointer</em>'.
	 * @see edu.ustb.sei.mde.webapp.DeployablePointer
	 * @generated
	 */
	EClass getDeployablePointer();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.webapp.DeployablePointer#getUrl <em>Url</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Url</em>'.
	 * @see edu.ustb.sei.mde.webapp.DeployablePointer#getUrl()
	 * @see #getDeployablePointer()
	 * @generated
	 */
	EAttribute getDeployablePointer_Url();

	/**
	 * Returns the meta object for the reference '{@link edu.ustb.sei.mde.webapp.DeployablePointer#getDeployable <em>Deployable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Deployable</em>'.
	 * @see edu.ustb.sei.mde.webapp.DeployablePointer#getDeployable()
	 * @see #getDeployablePointer()
	 * @generated
	 */
	EReference getDeployablePointer_Deployable();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.webapp.Interface <em>Interface</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Interface</em>'.
	 * @see edu.ustb.sei.mde.webapp.Interface
	 * @generated
	 */
	EClass getInterface();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.ustb.sei.mde.webapp.Interface#getOperations <em>Operations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Operations</em>'.
	 * @see edu.ustb.sei.mde.webapp.Interface#getOperations()
	 * @see #getInterface()
	 * @generated
	 */
	EReference getInterface_Operations();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.webapp.Operation <em>Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Operation</em>'.
	 * @see edu.ustb.sei.mde.webapp.Operation
	 * @generated
	 */
	EClass getOperation();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.ustb.sei.mde.webapp.Operation#getParameters <em>Parameters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Parameters</em>'.
	 * @see edu.ustb.sei.mde.webapp.Operation#getParameters()
	 * @see #getOperation()
	 * @generated
	 */
	EReference getOperation_Parameters();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.webapp.OperationDeclaration <em>Operation Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Operation Declaration</em>'.
	 * @see edu.ustb.sei.mde.webapp.OperationDeclaration
	 * @generated
	 */
	EClass getOperationDeclaration();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.webapp.Parameter <em>Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Parameter</em>'.
	 * @see edu.ustb.sei.mde.webapp.Parameter
	 * @generated
	 */
	EClass getParameter();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.webapp.Component <em>Component</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Component</em>'.
	 * @see edu.ustb.sei.mde.webapp.Component
	 * @generated
	 */
	EClass getComponent();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.ustb.sei.mde.webapp.Component#getOperations <em>Operations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Operations</em>'.
	 * @see edu.ustb.sei.mde.webapp.Component#getOperations()
	 * @see #getComponent()
	 * @generated
	 */
	EReference getComponent_Operations();

	/**
	 * Returns the meta object for the reference list '{@link edu.ustb.sei.mde.webapp.Component#getRequiredInterfaces <em>Required Interfaces</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Required Interfaces</em>'.
	 * @see edu.ustb.sei.mde.webapp.Component#getRequiredInterfaces()
	 * @see #getComponent()
	 * @generated
	 */
	EReference getComponent_RequiredInterfaces();

	/**
	 * Returns the meta object for the reference list '{@link edu.ustb.sei.mde.webapp.Component#getProvidedInterfaces <em>Provided Interfaces</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Provided Interfaces</em>'.
	 * @see edu.ustb.sei.mde.webapp.Component#getProvidedInterfaces()
	 * @see #getComponent()
	 * @generated
	 */
	EReference getComponent_ProvidedInterfaces();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.ustb.sei.mde.webapp.Component#getCompositions <em>Compositions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Compositions</em>'.
	 * @see edu.ustb.sei.mde.webapp.Component#getCompositions()
	 * @see #getComponent()
	 * @generated
	 */
	EReference getComponent_Compositions();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.webapp.OperationRealization <em>Operation Realization</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Operation Realization</em>'.
	 * @see edu.ustb.sei.mde.webapp.OperationRealization
	 * @generated
	 */
	EClass getOperationRealization();

	/**
	 * Returns the meta object for the reference '{@link edu.ustb.sei.mde.webapp.OperationRealization#getDeclaration <em>Declaration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Declaration</em>'.
	 * @see edu.ustb.sei.mde.webapp.OperationRealization#getDeclaration()
	 * @see #getOperationRealization()
	 * @generated
	 */
	EReference getOperationRealization_Declaration();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.webapp.CodeOperation <em>Code Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Code Operation</em>'.
	 * @see edu.ustb.sei.mde.webapp.CodeOperation
	 * @generated
	 */
	EClass getCodeOperation();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.webapp.CodeOperation#getBodyCode <em>Body Code</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Body Code</em>'.
	 * @see edu.ustb.sei.mde.webapp.CodeOperation#getBodyCode()
	 * @see #getCodeOperation()
	 * @generated
	 */
	EAttribute getCodeOperation_BodyCode();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.webapp.ActionOperation <em>Action Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Action Operation</em>'.
	 * @see edu.ustb.sei.mde.webapp.ActionOperation
	 * @generated
	 */
	EClass getActionOperation();

	/**
	 * Returns the meta object for the containment reference '{@link edu.ustb.sei.mde.webapp.ActionOperation#getAction <em>Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Action</em>'.
	 * @see edu.ustb.sei.mde.webapp.ActionOperation#getAction()
	 * @see #getActionOperation()
	 * @generated
	 */
	EReference getActionOperation_Action();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.webapp.Action <em>Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Action</em>'.
	 * @see edu.ustb.sei.mde.webapp.Action
	 * @generated
	 */
	EClass getAction();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.webapp.PageComponent <em>Page Component</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Page Component</em>'.
	 * @see edu.ustb.sei.mde.webapp.PageComponent
	 * @generated
	 */
	EClass getPageComponent();

	/**
	 * Returns the meta object for the containment reference '{@link edu.ustb.sei.mde.webapp.PageComponent#getNode <em>Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Node</em>'.
	 * @see edu.ustb.sei.mde.webapp.PageComponent#getNode()
	 * @see #getPageComponent()
	 * @generated
	 */
	EReference getPageComponent_Node();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.webapp.PageComponent#isPage <em>Page</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Page</em>'.
	 * @see edu.ustb.sei.mde.webapp.PageComponent#isPage()
	 * @see #getPageComponent()
	 * @generated
	 */
	EAttribute getPageComponent_Page();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.webapp.HTMLNode <em>HTML Node</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>HTML Node</em>'.
	 * @see edu.ustb.sei.mde.webapp.HTMLNode
	 * @generated
	 */
	EClass getHTMLNode();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.webapp.HTMLNode#getTag <em>Tag</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Tag</em>'.
	 * @see edu.ustb.sei.mde.webapp.HTMLNode#getTag()
	 * @see #getHTMLNode()
	 * @generated
	 */
	EAttribute getHTMLNode_Tag();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.webapp.HTMLNode#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see edu.ustb.sei.mde.webapp.HTMLNode#getId()
	 * @see #getHTMLNode()
	 * @generated
	 */
	EAttribute getHTMLNode_Id();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.webapp.HTMLNode#getClass_ <em>Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Class</em>'.
	 * @see edu.ustb.sei.mde.webapp.HTMLNode#getClass_()
	 * @see #getHTMLNode()
	 * @generated
	 */
	EAttribute getHTMLNode_Class();

	/**
	 * Returns the meta object for the attribute list '{@link edu.ustb.sei.mde.webapp.HTMLNode#getExtra <em>Extra</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Extra</em>'.
	 * @see edu.ustb.sei.mde.webapp.HTMLNode#getExtra()
	 * @see #getHTMLNode()
	 * @generated
	 */
	EAttribute getHTMLNode_Extra();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.webapp.HTMLNode#getInnerText <em>Inner Text</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Inner Text</em>'.
	 * @see edu.ustb.sei.mde.webapp.HTMLNode#getInnerText()
	 * @see #getHTMLNode()
	 * @generated
	 */
	EAttribute getHTMLNode_InnerText();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.ustb.sei.mde.webapp.HTMLNode#getChildren <em>Children</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Children</em>'.
	 * @see edu.ustb.sei.mde.webapp.HTMLNode#getChildren()
	 * @see #getHTMLNode()
	 * @generated
	 */
	EReference getHTMLNode_Children();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.ustb.sei.mde.webapp.HTMLNode#getHandlers <em>Handlers</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Handlers</em>'.
	 * @see edu.ustb.sei.mde.webapp.HTMLNode#getHandlers()
	 * @see #getHTMLNode()
	 * @generated
	 */
	EReference getHTMLNode_Handlers();

	/**
	 * Returns the meta object for the attribute list '{@link edu.ustb.sei.mde.webapp.HTMLNode#getStyles <em>Styles</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Styles</em>'.
	 * @see edu.ustb.sei.mde.webapp.HTMLNode#getStyles()
	 * @see #getHTMLNode()
	 * @generated
	 */
	EAttribute getHTMLNode_Styles();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.webapp.Composition <em>Composition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Composition</em>'.
	 * @see edu.ustb.sei.mde.webapp.Composition
	 * @generated
	 */
	EClass getComposition();

	/**
	 * Returns the meta object for the container reference '{@link edu.ustb.sei.mde.webapp.Composition#getOwner <em>Owner</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Owner</em>'.
	 * @see edu.ustb.sei.mde.webapp.Composition#getOwner()
	 * @see #getComposition()
	 * @generated
	 */
	EReference getComposition_Owner();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.webapp.PageComponentComposition <em>Page Component Composition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Page Component Composition</em>'.
	 * @see edu.ustb.sei.mde.webapp.PageComponentComposition
	 * @generated
	 */
	EClass getPageComponentComposition();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.webapp.PageComponentComposition#getTime <em>Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Time</em>'.
	 * @see edu.ustb.sei.mde.webapp.PageComponentComposition#getTime()
	 * @see #getPageComponentComposition()
	 * @generated
	 */
	EAttribute getPageComponentComposition_Time();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.webapp.PageComponentComposition#getContainerSelector <em>Container Selector</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Container Selector</em>'.
	 * @see edu.ustb.sei.mde.webapp.PageComponentComposition#getContainerSelector()
	 * @see #getPageComponentComposition()
	 * @generated
	 */
	EAttribute getPageComponentComposition_ContainerSelector();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.webapp.EventHandler <em>Event Handler</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Event Handler</em>'.
	 * @see edu.ustb.sei.mde.webapp.EventHandler
	 * @generated
	 */
	EClass getEventHandler();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.webapp.EventHandler#getEvent <em>Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Event</em>'.
	 * @see edu.ustb.sei.mde.webapp.EventHandler#getEvent()
	 * @see #getEventHandler()
	 * @generated
	 */
	EAttribute getEventHandler_Event();

	/**
	 * Returns the meta object for the container reference '{@link edu.ustb.sei.mde.webapp.EventHandler#getOwner <em>Owner</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Owner</em>'.
	 * @see edu.ustb.sei.mde.webapp.EventHandler#getOwner()
	 * @see #getEventHandler()
	 * @generated
	 */
	EReference getEventHandler_Owner();

	/**
	 * Returns the meta object for the reference '{@link edu.ustb.sei.mde.webapp.EventHandler#getHandler <em>Handler</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Handler</em>'.
	 * @see edu.ustb.sei.mde.webapp.EventHandler#getHandler()
	 * @see #getEventHandler()
	 * @generated
	 */
	EReference getEventHandler_Handler();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.webapp.ServerComponent <em>Server Component</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Server Component</em>'.
	 * @see edu.ustb.sei.mde.webapp.ServerComponent
	 * @generated
	 */
	EClass getServerComponent();

	/**
	 * Returns the meta object for the attribute list '{@link edu.ustb.sei.mde.webapp.ServerComponent#getImports <em>Imports</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Imports</em>'.
	 * @see edu.ustb.sei.mde.webapp.ServerComponent#getImports()
	 * @see #getServerComponent()
	 * @generated
	 */
	EAttribute getServerComponent_Imports();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.webapp.ServerComponent#getContentType <em>Content Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Content Type</em>'.
	 * @see edu.ustb.sei.mde.webapp.ServerComponent#getContentType()
	 * @see #getServerComponent()
	 * @generated
	 */
	EAttribute getServerComponent_ContentType();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.webapp.ClassComponent <em>Class Component</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Class Component</em>'.
	 * @see edu.ustb.sei.mde.webapp.ClassComponent
	 * @generated
	 */
	EClass getClassComponent();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.webapp.ClassComponent#getPackageName <em>Package Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Package Name</em>'.
	 * @see edu.ustb.sei.mde.webapp.ClassComponent#getPackageName()
	 * @see #getClassComponent()
	 * @generated
	 */
	EAttribute getClassComponent_PackageName();

	/**
	 * Returns the meta object for the attribute list '{@link edu.ustb.sei.mde.webapp.ClassComponent#getImports <em>Imports</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Imports</em>'.
	 * @see edu.ustb.sei.mde.webapp.ClassComponent#getImports()
	 * @see #getClassComponent()
	 * @generated
	 */
	EAttribute getClassComponent_Imports();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.webapp.StatementContainer <em>Statement Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Statement Container</em>'.
	 * @see edu.ustb.sei.mde.webapp.StatementContainer
	 * @generated
	 */
	EClass getStatementContainer();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.webapp.Database <em>Database</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Database</em>'.
	 * @see edu.ustb.sei.mde.webapp.Database
	 * @generated
	 */
	EClass getDatabase();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.webapp.Database#getDriverName <em>Driver Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Driver Name</em>'.
	 * @see edu.ustb.sei.mde.webapp.Database#getDriverName()
	 * @see #getDatabase()
	 * @generated
	 */
	EAttribute getDatabase_DriverName();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.webapp.Database#getUrl <em>Url</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Url</em>'.
	 * @see edu.ustb.sei.mde.webapp.Database#getUrl()
	 * @see #getDatabase()
	 * @generated
	 */
	EAttribute getDatabase_Url();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.webapp.Database#getUsername <em>Username</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Username</em>'.
	 * @see edu.ustb.sei.mde.webapp.Database#getUsername()
	 * @see #getDatabase()
	 * @generated
	 */
	EAttribute getDatabase_Username();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.webapp.Database#getPassword <em>Password</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Password</em>'.
	 * @see edu.ustb.sei.mde.webapp.Database#getPassword()
	 * @see #getDatabase()
	 * @generated
	 */
	EAttribute getDatabase_Password();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.webapp.Database#getInitialSize <em>Initial Size</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Initial Size</em>'.
	 * @see edu.ustb.sei.mde.webapp.Database#getInitialSize()
	 * @see #getDatabase()
	 * @generated
	 */
	EAttribute getDatabase_InitialSize();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.webapp.Database#getMinIdle <em>Min Idle</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Min Idle</em>'.
	 * @see edu.ustb.sei.mde.webapp.Database#getMinIdle()
	 * @see #getDatabase()
	 * @generated
	 */
	EAttribute getDatabase_MinIdle();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.webapp.Database#getMaxIdle <em>Max Idle</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Max Idle</em>'.
	 * @see edu.ustb.sei.mde.webapp.Database#getMaxIdle()
	 * @see #getDatabase()
	 * @generated
	 */
	EAttribute getDatabase_MaxIdle();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.webapp.Database#getMaxActive <em>Max Active</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Max Active</em>'.
	 * @see edu.ustb.sei.mde.webapp.Database#getMaxActive()
	 * @see #getDatabase()
	 * @generated
	 */
	EAttribute getDatabase_MaxActive();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.webapp.Database#getMaxWait <em>Max Wait</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Max Wait</em>'.
	 * @see edu.ustb.sei.mde.webapp.Database#getMaxWait()
	 * @see #getDatabase()
	 * @generated
	 */
	EAttribute getDatabase_MaxWait();

	/**
	 * Returns the meta object for the reference '{@link edu.ustb.sei.mde.webapp.Database#getDatamodel <em>Datamodel</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Datamodel</em>'.
	 * @see edu.ustb.sei.mde.webapp.Database#getDatamodel()
	 * @see #getDatabase()
	 * @generated
	 */
	EReference getDatabase_Datamodel();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.webapp.CodeAction <em>Code Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Code Action</em>'.
	 * @see edu.ustb.sei.mde.webapp.CodeAction
	 * @generated
	 */
	EClass getCodeAction();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.webapp.CodeAction#getCode <em>Code</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Code</em>'.
	 * @see edu.ustb.sei.mde.webapp.CodeAction#getCode()
	 * @see #getCodeAction()
	 * @generated
	 */
	EAttribute getCodeAction_Code();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.webapp.CallAction <em>Call Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Call Action</em>'.
	 * @see edu.ustb.sei.mde.webapp.CallAction
	 * @generated
	 */
	EClass getCallAction();

	/**
	 * Returns the meta object for the reference '{@link edu.ustb.sei.mde.webapp.CallAction#getInvocationTarget <em>Invocation Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Invocation Target</em>'.
	 * @see edu.ustb.sei.mde.webapp.CallAction#getInvocationTarget()
	 * @see #getCallAction()
	 * @generated
	 */
	EReference getCallAction_InvocationTarget();

	/**
	 * Returns the meta object for the attribute list '{@link edu.ustb.sei.mde.webapp.CallAction#getParameterCodes <em>Parameter Codes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Parameter Codes</em>'.
	 * @see edu.ustb.sei.mde.webapp.CallAction#getParameterCodes()
	 * @see #getCallAction()
	 * @generated
	 */
	EAttribute getCallAction_ParameterCodes();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.webapp.LetAction <em>Let Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Let Action</em>'.
	 * @see edu.ustb.sei.mde.webapp.LetAction
	 * @generated
	 */
	EClass getLetAction();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.webapp.LetAction#getLeftCode <em>Left Code</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Left Code</em>'.
	 * @see edu.ustb.sei.mde.webapp.LetAction#getLeftCode()
	 * @see #getLetAction()
	 * @generated
	 */
	EAttribute getLetAction_LeftCode();

	/**
	 * Returns the meta object for the containment reference '{@link edu.ustb.sei.mde.webapp.LetAction#getRight <em>Right</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Right</em>'.
	 * @see edu.ustb.sei.mde.webapp.LetAction#getRight()
	 * @see #getLetAction()
	 * @generated
	 */
	EReference getLetAction_Right();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.webapp.ForAction <em>For Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>For Action</em>'.
	 * @see edu.ustb.sei.mde.webapp.ForAction
	 * @generated
	 */
	EClass getForAction();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.webapp.ForAction#getConditionCode <em>Condition Code</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Condition Code</em>'.
	 * @see edu.ustb.sei.mde.webapp.ForAction#getConditionCode()
	 * @see #getForAction()
	 * @generated
	 */
	EAttribute getForAction_ConditionCode();

	/**
	 * Returns the meta object for the containment reference '{@link edu.ustb.sei.mde.webapp.ForAction#getBodyAction <em>Body Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Body Action</em>'.
	 * @see edu.ustb.sei.mde.webapp.ForAction#getBodyAction()
	 * @see #getForAction()
	 * @generated
	 */
	EReference getForAction_BodyAction();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.webapp.IfAction <em>If Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>If Action</em>'.
	 * @see edu.ustb.sei.mde.webapp.IfAction
	 * @generated
	 */
	EClass getIfAction();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.webapp.IfAction#getConditionCode <em>Condition Code</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Condition Code</em>'.
	 * @see edu.ustb.sei.mde.webapp.IfAction#getConditionCode()
	 * @see #getIfAction()
	 * @generated
	 */
	EAttribute getIfAction_ConditionCode();

	/**
	 * Returns the meta object for the containment reference '{@link edu.ustb.sei.mde.webapp.IfAction#getThenAction <em>Then Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Then Action</em>'.
	 * @see edu.ustb.sei.mde.webapp.IfAction#getThenAction()
	 * @see #getIfAction()
	 * @generated
	 */
	EReference getIfAction_ThenAction();

	/**
	 * Returns the meta object for the containment reference '{@link edu.ustb.sei.mde.webapp.IfAction#getElseAction <em>Else Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Else Action</em>'.
	 * @see edu.ustb.sei.mde.webapp.IfAction#getElseAction()
	 * @see #getIfAction()
	 * @generated
	 */
	EReference getIfAction_ElseAction();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.webapp.SequenceAction <em>Sequence Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Sequence Action</em>'.
	 * @see edu.ustb.sei.mde.webapp.SequenceAction
	 * @generated
	 */
	EClass getSequenceAction();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.ustb.sei.mde.webapp.SequenceAction#getActions <em>Actions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Actions</em>'.
	 * @see edu.ustb.sei.mde.webapp.SequenceAction#getActions()
	 * @see #getSequenceAction()
	 * @generated
	 */
	EReference getSequenceAction_Actions();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.webapp.TryCatchAction <em>Try Catch Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Try Catch Action</em>'.
	 * @see edu.ustb.sei.mde.webapp.TryCatchAction
	 * @generated
	 */
	EClass getTryCatchAction();

	/**
	 * Returns the meta object for the containment reference '{@link edu.ustb.sei.mde.webapp.TryCatchAction#getTryPart <em>Try Part</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Try Part</em>'.
	 * @see edu.ustb.sei.mde.webapp.TryCatchAction#getTryPart()
	 * @see #getTryCatchAction()
	 * @generated
	 */
	EReference getTryCatchAction_TryPart();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.ustb.sei.mde.webapp.TryCatchAction#getCatchParts <em>Catch Parts</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Catch Parts</em>'.
	 * @see edu.ustb.sei.mde.webapp.TryCatchAction#getCatchParts()
	 * @see #getTryCatchAction()
	 * @generated
	 */
	EReference getTryCatchAction_CatchParts();

	/**
	 * Returns the meta object for the containment reference '{@link edu.ustb.sei.mde.webapp.TryCatchAction#getFinalPart <em>Final Part</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Final Part</em>'.
	 * @see edu.ustb.sei.mde.webapp.TryCatchAction#getFinalPart()
	 * @see #getTryCatchAction()
	 * @generated
	 */
	EReference getTryCatchAction_FinalPart();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.webapp.CatchPart <em>Catch Part</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Catch Part</em>'.
	 * @see edu.ustb.sei.mde.webapp.CatchPart
	 * @generated
	 */
	EClass getCatchPart();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.webapp.CatchPart#getExceptionVariable <em>Exception Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Exception Variable</em>'.
	 * @see edu.ustb.sei.mde.webapp.CatchPart#getExceptionVariable()
	 * @see #getCatchPart()
	 * @generated
	 */
	EAttribute getCatchPart_ExceptionVariable();

	/**
	 * Returns the meta object for the containment reference '{@link edu.ustb.sei.mde.webapp.CatchPart#getActioin <em>Actioin</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Actioin</em>'.
	 * @see edu.ustb.sei.mde.webapp.CatchPart#getActioin()
	 * @see #getCatchPart()
	 * @generated
	 */
	EReference getCatchPart_Actioin();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.webapp.JavascriptAction <em>Javascript Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Javascript Action</em>'.
	 * @see edu.ustb.sei.mde.webapp.JavascriptAction
	 * @generated
	 */
	EClass getJavascriptAction();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.webapp.JQueryAjaxAction <em>JQuery Ajax Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>JQuery Ajax Action</em>'.
	 * @see edu.ustb.sei.mde.webapp.JQueryAjaxAction
	 * @generated
	 */
	EClass getJQueryAjaxAction();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.webapp.JQueryAjaxAction#getSelectorCode <em>Selector Code</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Selector Code</em>'.
	 * @see edu.ustb.sei.mde.webapp.JQueryAjaxAction#getSelectorCode()
	 * @see #getJQueryAjaxAction()
	 * @generated
	 */
	EAttribute getJQueryAjaxAction_SelectorCode();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.webapp.JQueryAjaxAction#getAjaxType <em>Ajax Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Ajax Type</em>'.
	 * @see edu.ustb.sei.mde.webapp.JQueryAjaxAction#getAjaxType()
	 * @see #getJQueryAjaxAction()
	 * @generated
	 */
	EAttribute getJQueryAjaxAction_AjaxType();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.webapp.JQueryAjaxAction#getDataCode <em>Data Code</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Data Code</em>'.
	 * @see edu.ustb.sei.mde.webapp.JQueryAjaxAction#getDataCode()
	 * @see #getJQueryAjaxAction()
	 * @generated
	 */
	EAttribute getJQueryAjaxAction_DataCode();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.webapp.JQueryAjaxAction#getDataType <em>Data Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Data Type</em>'.
	 * @see edu.ustb.sei.mde.webapp.JQueryAjaxAction#getDataType()
	 * @see #getJQueryAjaxAction()
	 * @generated
	 */
	EAttribute getJQueryAjaxAction_DataType();

	/**
	 * Returns the meta object for the containment reference '{@link edu.ustb.sei.mde.webapp.JQueryAjaxAction#getCallbackFunction <em>Callback Function</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Callback Function</em>'.
	 * @see edu.ustb.sei.mde.webapp.JQueryAjaxAction#getCallbackFunction()
	 * @see #getJQueryAjaxAction()
	 * @generated
	 */
	EReference getJQueryAjaxAction_CallbackFunction();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.webapp.JQueryEventAction <em>JQuery Event Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>JQuery Event Action</em>'.
	 * @see edu.ustb.sei.mde.webapp.JQueryEventAction
	 * @generated
	 */
	EClass getJQueryEventAction();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.webapp.JQueryEventAction#getSelectorCode <em>Selector Code</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Selector Code</em>'.
	 * @see edu.ustb.sei.mde.webapp.JQueryEventAction#getSelectorCode()
	 * @see #getJQueryEventAction()
	 * @generated
	 */
	EAttribute getJQueryEventAction_SelectorCode();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.webapp.JQueryEventAction#getEvent <em>Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Event</em>'.
	 * @see edu.ustb.sei.mde.webapp.JQueryEventAction#getEvent()
	 * @see #getJQueryEventAction()
	 * @generated
	 */
	EAttribute getJQueryEventAction_Event();

	/**
	 * Returns the meta object for the containment reference '{@link edu.ustb.sei.mde.webapp.JQueryEventAction#getHandler <em>Handler</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Handler</em>'.
	 * @see edu.ustb.sei.mde.webapp.JQueryEventAction#getHandler()
	 * @see #getJQueryEventAction()
	 * @generated
	 */
	EReference getJQueryEventAction_Handler();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.webapp.SQLAction <em>SQL Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>SQL Action</em>'.
	 * @see edu.ustb.sei.mde.webapp.SQLAction
	 * @generated
	 */
	EClass getSQLAction();

	/**
	 * Returns the meta object for the reference '{@link edu.ustb.sei.mde.webapp.SQLAction#getDatabase <em>Database</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Database</em>'.
	 * @see edu.ustb.sei.mde.webapp.SQLAction#getDatabase()
	 * @see #getSQLAction()
	 * @generated
	 */
	EReference getSQLAction_Database();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.webapp.SQLAction#getSqlName <em>Sql Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Sql Name</em>'.
	 * @see edu.ustb.sei.mde.webapp.SQLAction#getSqlName()
	 * @see #getSQLAction()
	 * @generated
	 */
	EAttribute getSQLAction_SqlName();

	/**
	 * Returns the meta object for the containment reference '{@link edu.ustb.sei.mde.webapp.SQLAction#getHandlingAction <em>Handling Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Handling Action</em>'.
	 * @see edu.ustb.sei.mde.webapp.SQLAction#getHandlingAction()
	 * @see #getSQLAction()
	 * @generated
	 */
	EReference getSQLAction_HandlingAction();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.webapp.SQLSelectAction <em>SQL Select Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>SQL Select Action</em>'.
	 * @see edu.ustb.sei.mde.webapp.SQLSelectAction
	 * @generated
	 */
	EClass getSQLSelectAction();

	/**
	 * Returns the meta object for the attribute list '{@link edu.ustb.sei.mde.webapp.SQLSelectAction#getTables <em>Tables</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Tables</em>'.
	 * @see edu.ustb.sei.mde.webapp.SQLSelectAction#getTables()
	 * @see #getSQLSelectAction()
	 * @generated
	 */
	EAttribute getSQLSelectAction_Tables();

	/**
	 * Returns the meta object for the attribute list '{@link edu.ustb.sei.mde.webapp.SQLSelectAction#getColumns <em>Columns</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Columns</em>'.
	 * @see edu.ustb.sei.mde.webapp.SQLSelectAction#getColumns()
	 * @see #getSQLSelectAction()
	 * @generated
	 */
	EAttribute getSQLSelectAction_Columns();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.webapp.SQLSelectAction#getConditionCode <em>Condition Code</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Condition Code</em>'.
	 * @see edu.ustb.sei.mde.webapp.SQLSelectAction#getConditionCode()
	 * @see #getSQLSelectAction()
	 * @generated
	 */
	EAttribute getSQLSelectAction_ConditionCode();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.webapp.SQLInsertAction <em>SQL Insert Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>SQL Insert Action</em>'.
	 * @see edu.ustb.sei.mde.webapp.SQLInsertAction
	 * @generated
	 */
	EClass getSQLInsertAction();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.webapp.SQLInsertAction#getTable <em>Table</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Table</em>'.
	 * @see edu.ustb.sei.mde.webapp.SQLInsertAction#getTable()
	 * @see #getSQLInsertAction()
	 * @generated
	 */
	EAttribute getSQLInsertAction_Table();

	/**
	 * Returns the meta object for the attribute list '{@link edu.ustb.sei.mde.webapp.SQLInsertAction#getColumns <em>Columns</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Columns</em>'.
	 * @see edu.ustb.sei.mde.webapp.SQLInsertAction#getColumns()
	 * @see #getSQLInsertAction()
	 * @generated
	 */
	EAttribute getSQLInsertAction_Columns();

	/**
	 * Returns the meta object for the attribute list '{@link edu.ustb.sei.mde.webapp.SQLInsertAction#getValueCodes <em>Value Codes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Value Codes</em>'.
	 * @see edu.ustb.sei.mde.webapp.SQLInsertAction#getValueCodes()
	 * @see #getSQLInsertAction()
	 * @generated
	 */
	EAttribute getSQLInsertAction_ValueCodes();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.webapp.SQLUpdateAction <em>SQL Update Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>SQL Update Action</em>'.
	 * @see edu.ustb.sei.mde.webapp.SQLUpdateAction
	 * @generated
	 */
	EClass getSQLUpdateAction();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.webapp.SQLUpdateAction#getTable <em>Table</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Table</em>'.
	 * @see edu.ustb.sei.mde.webapp.SQLUpdateAction#getTable()
	 * @see #getSQLUpdateAction()
	 * @generated
	 */
	EAttribute getSQLUpdateAction_Table();

	/**
	 * Returns the meta object for the attribute list '{@link edu.ustb.sei.mde.webapp.SQLUpdateAction#getColumns <em>Columns</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Columns</em>'.
	 * @see edu.ustb.sei.mde.webapp.SQLUpdateAction#getColumns()
	 * @see #getSQLUpdateAction()
	 * @generated
	 */
	EAttribute getSQLUpdateAction_Columns();

	/**
	 * Returns the meta object for the attribute list '{@link edu.ustb.sei.mde.webapp.SQLUpdateAction#getValueCodes <em>Value Codes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Value Codes</em>'.
	 * @see edu.ustb.sei.mde.webapp.SQLUpdateAction#getValueCodes()
	 * @see #getSQLUpdateAction()
	 * @generated
	 */
	EAttribute getSQLUpdateAction_ValueCodes();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.webapp.SQLUpdateAction#getConditionCode <em>Condition Code</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Condition Code</em>'.
	 * @see edu.ustb.sei.mde.webapp.SQLUpdateAction#getConditionCode()
	 * @see #getSQLUpdateAction()
	 * @generated
	 */
	EAttribute getSQLUpdateAction_ConditionCode();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.webapp.SQLDeleteAction <em>SQL Delete Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>SQL Delete Action</em>'.
	 * @see edu.ustb.sei.mde.webapp.SQLDeleteAction
	 * @generated
	 */
	EClass getSQLDeleteAction();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.webapp.SQLDeleteAction#getTable <em>Table</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Table</em>'.
	 * @see edu.ustb.sei.mde.webapp.SQLDeleteAction#getTable()
	 * @see #getSQLDeleteAction()
	 * @generated
	 */
	EAttribute getSQLDeleteAction_Table();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.webapp.SQLDeleteAction#getConditionCode <em>Condition Code</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Condition Code</em>'.
	 * @see edu.ustb.sei.mde.webapp.SQLDeleteAction#getConditionCode()
	 * @see #getSQLDeleteAction()
	 * @generated
	 */
	EAttribute getSQLDeleteAction_ConditionCode();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.webapp.SQLExecutionAction <em>SQL Execution Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>SQL Execution Action</em>'.
	 * @see edu.ustb.sei.mde.webapp.SQLExecutionAction
	 * @generated
	 */
	EClass getSQLExecutionAction();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.webapp.SQLExecutionAction#getSqlCode <em>Sql Code</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Sql Code</em>'.
	 * @see edu.ustb.sei.mde.webapp.SQLExecutionAction#getSqlCode()
	 * @see #getSQLExecutionAction()
	 * @generated
	 */
	EAttribute getSQLExecutionAction_SqlCode();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.webapp.SQLResultSetIterationAction <em>SQL Result Set Iteration Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>SQL Result Set Iteration Action</em>'.
	 * @see edu.ustb.sei.mde.webapp.SQLResultSetIterationAction
	 * @generated
	 */
	EClass getSQLResultSetIterationAction();

	/**
	 * Returns the meta object for the containment reference '{@link edu.ustb.sei.mde.webapp.SQLResultSetIterationAction#getAction <em>Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Action</em>'.
	 * @see edu.ustb.sei.mde.webapp.SQLResultSetIterationAction#getAction()
	 * @see #getSQLResultSetIterationAction()
	 * @generated
	 */
	EReference getSQLResultSetIterationAction_Action();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.webapp.ServerPageAction <em>Server Page Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Server Page Action</em>'.
	 * @see edu.ustb.sei.mde.webapp.ServerPageAction
	 * @generated
	 */
	EClass getServerPageAction();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.webapp.ServerPageTemplateAction <em>Server Page Template Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Server Page Template Action</em>'.
	 * @see edu.ustb.sei.mde.webapp.ServerPageTemplateAction
	 * @generated
	 */
	EClass getServerPageTemplateAction();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.webapp.ServerPageTemplateAction#getText <em>Text</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Text</em>'.
	 * @see edu.ustb.sei.mde.webapp.ServerPageTemplateAction#getText()
	 * @see #getServerPageTemplateAction()
	 * @generated
	 */
	EAttribute getServerPageTemplateAction_Text();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.webapp.ServerPageExpressionAction <em>Server Page Expression Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Server Page Expression Action</em>'.
	 * @see edu.ustb.sei.mde.webapp.ServerPageExpressionAction
	 * @generated
	 */
	EClass getServerPageExpressionAction();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.webapp.ServerPageExpressionAction#getExpression <em>Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Expression</em>'.
	 * @see edu.ustb.sei.mde.webapp.ServerPageExpressionAction#getExpression()
	 * @see #getServerPageExpressionAction()
	 * @generated
	 */
	EAttribute getServerPageExpressionAction_Expression();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.webapp.ServerPageStructuredTemplateAction <em>Server Page Structured Template Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Server Page Structured Template Action</em>'.
	 * @see edu.ustb.sei.mde.webapp.ServerPageStructuredTemplateAction
	 * @generated
	 */
	EClass getServerPageStructuredTemplateAction();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.webapp.ServerPageStructuredTemplateAction#getPrefix <em>Prefix</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Prefix</em>'.
	 * @see edu.ustb.sei.mde.webapp.ServerPageStructuredTemplateAction#getPrefix()
	 * @see #getServerPageStructuredTemplateAction()
	 * @generated
	 */
	EAttribute getServerPageStructuredTemplateAction_Prefix();

	/**
	 * Returns the meta object for the containment reference '{@link edu.ustb.sei.mde.webapp.ServerPageStructuredTemplateAction#getAction <em>Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Action</em>'.
	 * @see edu.ustb.sei.mde.webapp.ServerPageStructuredTemplateAction#getAction()
	 * @see #getServerPageStructuredTemplateAction()
	 * @generated
	 */
	EReference getServerPageStructuredTemplateAction_Action();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.webapp.ServerPageStructuredTemplateAction#getSuffix <em>Suffix</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Suffix</em>'.
	 * @see edu.ustb.sei.mde.webapp.ServerPageStructuredTemplateAction#getSuffix()
	 * @see #getServerPageStructuredTemplateAction()
	 * @generated
	 */
	EAttribute getServerPageStructuredTemplateAction_Suffix();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.webapp.WebApplication <em>Web Application</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Web Application</em>'.
	 * @see edu.ustb.sei.mde.webapp.WebApplication
	 * @generated
	 */
	EClass getWebApplication();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.webapp.WebApplication#getDatabaseUtilClass <em>Database Util Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Database Util Class</em>'.
	 * @see edu.ustb.sei.mde.webapp.WebApplication#getDatabaseUtilClass()
	 * @see #getWebApplication()
	 * @generated
	 */
	EAttribute getWebApplication_DatabaseUtilClass();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.ustb.sei.mde.webapp.WebApplication#getDatabases <em>Databases</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Databases</em>'.
	 * @see edu.ustb.sei.mde.webapp.WebApplication#getDatabases()
	 * @see #getWebApplication()
	 * @generated
	 */
	EReference getWebApplication_Databases();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.ustb.sei.mde.webapp.WebApplication#getInterfaces <em>Interfaces</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Interfaces</em>'.
	 * @see edu.ustb.sei.mde.webapp.WebApplication#getInterfaces()
	 * @see #getWebApplication()
	 * @generated
	 */
	EReference getWebApplication_Interfaces();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.ustb.sei.mde.webapp.WebApplication#getPageComponents <em>Page Components</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Page Components</em>'.
	 * @see edu.ustb.sei.mde.webapp.WebApplication#getPageComponents()
	 * @see #getWebApplication()
	 * @generated
	 */
	EReference getWebApplication_PageComponents();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.ustb.sei.mde.webapp.WebApplication#getServerComponents <em>Server Components</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Server Components</em>'.
	 * @see edu.ustb.sei.mde.webapp.WebApplication#getServerComponents()
	 * @see #getWebApplication()
	 * @generated
	 */
	EReference getWebApplication_ServerComponents();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.ustb.sei.mde.webapp.WebApplication#getClassComponents <em>Class Components</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Class Components</em>'.
	 * @see edu.ustb.sei.mde.webapp.WebApplication#getClassComponents()
	 * @see #getWebApplication()
	 * @generated
	 */
	EReference getWebApplication_ClassComponents();

	/**
	 * Returns the meta object for enum '{@link edu.ustb.sei.mde.webapp.AjaxType <em>Ajax Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Ajax Type</em>'.
	 * @see edu.ustb.sei.mde.webapp.AjaxType
	 * @generated
	 */
	EEnum getAjaxType();

	/**
	 * Returns the meta object for enum '{@link edu.ustb.sei.mde.webapp.PageContentType <em>Page Content Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Page Content Type</em>'.
	 * @see edu.ustb.sei.mde.webapp.PageContentType
	 * @generated
	 */
	EEnum getPageContentType();

	/**
	 * Returns the meta object for enum '{@link edu.ustb.sei.mde.webapp.DataFormat <em>Data Format</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Data Format</em>'.
	 * @see edu.ustb.sei.mde.webapp.DataFormat
	 * @generated
	 */
	EEnum getDataFormat();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	WebAppFactory getWebAppFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.webapp.impl.NamedElementImpl <em>Named Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.webapp.impl.NamedElementImpl
		 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getNamedElement()
		 * @generated
		 */
		EClass NAMED_ELEMENT = eINSTANCE.getNamedElement();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NAMED_ELEMENT__NAME = eINSTANCE.getNamedElement_Name();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.webapp.impl.TypedElementImpl <em>Typed Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.webapp.impl.TypedElementImpl
		 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getTypedElement()
		 * @generated
		 */
		EClass TYPED_ELEMENT = eINSTANCE.getTypedElement();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TYPED_ELEMENT__TYPE = eINSTANCE.getTypedElement_Type();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.webapp.impl.DeployableImpl <em>Deployable</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.webapp.impl.DeployableImpl
		 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getDeployable()
		 * @generated
		 */
		EClass DEPLOYABLE = eINSTANCE.getDeployable();

		/**
		 * The meta object literal for the '<em><b>Deploy Path</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DEPLOYABLE__DEPLOY_PATH = eINSTANCE.getDeployable_DeployPath();

		/**
		 * The meta object literal for the '<em><b>Get URL</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation DEPLOYABLE___GET_URL = eINSTANCE.getDeployable__GetURL();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.webapp.impl.DeployablePointerImpl <em>Deployable Pointer</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.webapp.impl.DeployablePointerImpl
		 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getDeployablePointer()
		 * @generated
		 */
		EClass DEPLOYABLE_POINTER = eINSTANCE.getDeployablePointer();

		/**
		 * The meta object literal for the '<em><b>Url</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DEPLOYABLE_POINTER__URL = eINSTANCE.getDeployablePointer_Url();

		/**
		 * The meta object literal for the '<em><b>Deployable</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEPLOYABLE_POINTER__DEPLOYABLE = eINSTANCE.getDeployablePointer_Deployable();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.webapp.impl.InterfaceImpl <em>Interface</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.webapp.impl.InterfaceImpl
		 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getInterface()
		 * @generated
		 */
		EClass INTERFACE = eINSTANCE.getInterface();

		/**
		 * The meta object literal for the '<em><b>Operations</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference INTERFACE__OPERATIONS = eINSTANCE.getInterface_Operations();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.webapp.impl.OperationImpl <em>Operation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.webapp.impl.OperationImpl
		 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getOperation()
		 * @generated
		 */
		EClass OPERATION = eINSTANCE.getOperation();

		/**
		 * The meta object literal for the '<em><b>Parameters</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATION__PARAMETERS = eINSTANCE.getOperation_Parameters();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.webapp.impl.OperationDeclarationImpl <em>Operation Declaration</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.webapp.impl.OperationDeclarationImpl
		 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getOperationDeclaration()
		 * @generated
		 */
		EClass OPERATION_DECLARATION = eINSTANCE.getOperationDeclaration();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.webapp.impl.ParameterImpl <em>Parameter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.webapp.impl.ParameterImpl
		 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getParameter()
		 * @generated
		 */
		EClass PARAMETER = eINSTANCE.getParameter();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.webapp.impl.ComponentImpl <em>Component</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.webapp.impl.ComponentImpl
		 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getComponent()
		 * @generated
		 */
		EClass COMPONENT = eINSTANCE.getComponent();

		/**
		 * The meta object literal for the '<em><b>Operations</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPONENT__OPERATIONS = eINSTANCE.getComponent_Operations();

		/**
		 * The meta object literal for the '<em><b>Required Interfaces</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPONENT__REQUIRED_INTERFACES = eINSTANCE.getComponent_RequiredInterfaces();

		/**
		 * The meta object literal for the '<em><b>Provided Interfaces</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPONENT__PROVIDED_INTERFACES = eINSTANCE.getComponent_ProvidedInterfaces();

		/**
		 * The meta object literal for the '<em><b>Compositions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPONENT__COMPOSITIONS = eINSTANCE.getComponent_Compositions();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.webapp.impl.OperationRealizationImpl <em>Operation Realization</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.webapp.impl.OperationRealizationImpl
		 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getOperationRealization()
		 * @generated
		 */
		EClass OPERATION_REALIZATION = eINSTANCE.getOperationRealization();

		/**
		 * The meta object literal for the '<em><b>Declaration</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPERATION_REALIZATION__DECLARATION = eINSTANCE.getOperationRealization_Declaration();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.webapp.impl.CodeOperationImpl <em>Code Operation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.webapp.impl.CodeOperationImpl
		 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getCodeOperation()
		 * @generated
		 */
		EClass CODE_OPERATION = eINSTANCE.getCodeOperation();

		/**
		 * The meta object literal for the '<em><b>Body Code</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CODE_OPERATION__BODY_CODE = eINSTANCE.getCodeOperation_BodyCode();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.webapp.impl.ActionOperationImpl <em>Action Operation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.webapp.impl.ActionOperationImpl
		 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getActionOperation()
		 * @generated
		 */
		EClass ACTION_OPERATION = eINSTANCE.getActionOperation();

		/**
		 * The meta object literal for the '<em><b>Action</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTION_OPERATION__ACTION = eINSTANCE.getActionOperation_Action();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.webapp.impl.ActionImpl <em>Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.webapp.impl.ActionImpl
		 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getAction()
		 * @generated
		 */
		EClass ACTION = eINSTANCE.getAction();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.webapp.impl.PageComponentImpl <em>Page Component</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.webapp.impl.PageComponentImpl
		 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getPageComponent()
		 * @generated
		 */
		EClass PAGE_COMPONENT = eINSTANCE.getPageComponent();

		/**
		 * The meta object literal for the '<em><b>Node</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PAGE_COMPONENT__NODE = eINSTANCE.getPageComponent_Node();

		/**
		 * The meta object literal for the '<em><b>Page</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PAGE_COMPONENT__PAGE = eINSTANCE.getPageComponent_Page();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.webapp.impl.HTMLNodeImpl <em>HTML Node</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.webapp.impl.HTMLNodeImpl
		 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getHTMLNode()
		 * @generated
		 */
		EClass HTML_NODE = eINSTANCE.getHTMLNode();

		/**
		 * The meta object literal for the '<em><b>Tag</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HTML_NODE__TAG = eINSTANCE.getHTMLNode_Tag();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HTML_NODE__ID = eINSTANCE.getHTMLNode_Id();

		/**
		 * The meta object literal for the '<em><b>Class</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HTML_NODE__CLASS = eINSTANCE.getHTMLNode_Class();

		/**
		 * The meta object literal for the '<em><b>Extra</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HTML_NODE__EXTRA = eINSTANCE.getHTMLNode_Extra();

		/**
		 * The meta object literal for the '<em><b>Inner Text</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HTML_NODE__INNER_TEXT = eINSTANCE.getHTMLNode_InnerText();

		/**
		 * The meta object literal for the '<em><b>Children</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HTML_NODE__CHILDREN = eINSTANCE.getHTMLNode_Children();

		/**
		 * The meta object literal for the '<em><b>Handlers</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HTML_NODE__HANDLERS = eINSTANCE.getHTMLNode_Handlers();

		/**
		 * The meta object literal for the '<em><b>Styles</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HTML_NODE__STYLES = eINSTANCE.getHTMLNode_Styles();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.webapp.impl.CompositionImpl <em>Composition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.webapp.impl.CompositionImpl
		 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getComposition()
		 * @generated
		 */
		EClass COMPOSITION = eINSTANCE.getComposition();

		/**
		 * The meta object literal for the '<em><b>Owner</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPOSITION__OWNER = eINSTANCE.getComposition_Owner();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.webapp.impl.PageComponentCompositionImpl <em>Page Component Composition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.webapp.impl.PageComponentCompositionImpl
		 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getPageComponentComposition()
		 * @generated
		 */
		EClass PAGE_COMPONENT_COMPOSITION = eINSTANCE.getPageComponentComposition();

		/**
		 * The meta object literal for the '<em><b>Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PAGE_COMPONENT_COMPOSITION__TIME = eINSTANCE.getPageComponentComposition_Time();

		/**
		 * The meta object literal for the '<em><b>Container Selector</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PAGE_COMPONENT_COMPOSITION__CONTAINER_SELECTOR = eINSTANCE.getPageComponentComposition_ContainerSelector();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.webapp.impl.EventHandlerImpl <em>Event Handler</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.webapp.impl.EventHandlerImpl
		 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getEventHandler()
		 * @generated
		 */
		EClass EVENT_HANDLER = eINSTANCE.getEventHandler();

		/**
		 * The meta object literal for the '<em><b>Event</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EVENT_HANDLER__EVENT = eINSTANCE.getEventHandler_Event();

		/**
		 * The meta object literal for the '<em><b>Owner</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EVENT_HANDLER__OWNER = eINSTANCE.getEventHandler_Owner();

		/**
		 * The meta object literal for the '<em><b>Handler</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EVENT_HANDLER__HANDLER = eINSTANCE.getEventHandler_Handler();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.webapp.impl.ServerComponentImpl <em>Server Component</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.webapp.impl.ServerComponentImpl
		 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getServerComponent()
		 * @generated
		 */
		EClass SERVER_COMPONENT = eINSTANCE.getServerComponent();

		/**
		 * The meta object literal for the '<em><b>Imports</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SERVER_COMPONENT__IMPORTS = eINSTANCE.getServerComponent_Imports();

		/**
		 * The meta object literal for the '<em><b>Content Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SERVER_COMPONENT__CONTENT_TYPE = eINSTANCE.getServerComponent_ContentType();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.webapp.impl.ClassComponentImpl <em>Class Component</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.webapp.impl.ClassComponentImpl
		 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getClassComponent()
		 * @generated
		 */
		EClass CLASS_COMPONENT = eINSTANCE.getClassComponent();

		/**
		 * The meta object literal for the '<em><b>Package Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CLASS_COMPONENT__PACKAGE_NAME = eINSTANCE.getClassComponent_PackageName();

		/**
		 * The meta object literal for the '<em><b>Imports</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CLASS_COMPONENT__IMPORTS = eINSTANCE.getClassComponent_Imports();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.webapp.impl.StatementContainerImpl <em>Statement Container</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.webapp.impl.StatementContainerImpl
		 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getStatementContainer()
		 * @generated
		 */
		EClass STATEMENT_CONTAINER = eINSTANCE.getStatementContainer();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.webapp.impl.DatabaseImpl <em>Database</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.webapp.impl.DatabaseImpl
		 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getDatabase()
		 * @generated
		 */
		EClass DATABASE = eINSTANCE.getDatabase();

		/**
		 * The meta object literal for the '<em><b>Driver Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DATABASE__DRIVER_NAME = eINSTANCE.getDatabase_DriverName();

		/**
		 * The meta object literal for the '<em><b>Url</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DATABASE__URL = eINSTANCE.getDatabase_Url();

		/**
		 * The meta object literal for the '<em><b>Username</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DATABASE__USERNAME = eINSTANCE.getDatabase_Username();

		/**
		 * The meta object literal for the '<em><b>Password</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DATABASE__PASSWORD = eINSTANCE.getDatabase_Password();

		/**
		 * The meta object literal for the '<em><b>Initial Size</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DATABASE__INITIAL_SIZE = eINSTANCE.getDatabase_InitialSize();

		/**
		 * The meta object literal for the '<em><b>Min Idle</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DATABASE__MIN_IDLE = eINSTANCE.getDatabase_MinIdle();

		/**
		 * The meta object literal for the '<em><b>Max Idle</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DATABASE__MAX_IDLE = eINSTANCE.getDatabase_MaxIdle();

		/**
		 * The meta object literal for the '<em><b>Max Active</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DATABASE__MAX_ACTIVE = eINSTANCE.getDatabase_MaxActive();

		/**
		 * The meta object literal for the '<em><b>Max Wait</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DATABASE__MAX_WAIT = eINSTANCE.getDatabase_MaxWait();

		/**
		 * The meta object literal for the '<em><b>Datamodel</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATABASE__DATAMODEL = eINSTANCE.getDatabase_Datamodel();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.webapp.impl.CodeActionImpl <em>Code Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.webapp.impl.CodeActionImpl
		 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getCodeAction()
		 * @generated
		 */
		EClass CODE_ACTION = eINSTANCE.getCodeAction();

		/**
		 * The meta object literal for the '<em><b>Code</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CODE_ACTION__CODE = eINSTANCE.getCodeAction_Code();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.webapp.impl.CallActionImpl <em>Call Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.webapp.impl.CallActionImpl
		 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getCallAction()
		 * @generated
		 */
		EClass CALL_ACTION = eINSTANCE.getCallAction();

		/**
		 * The meta object literal for the '<em><b>Invocation Target</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CALL_ACTION__INVOCATION_TARGET = eINSTANCE.getCallAction_InvocationTarget();

		/**
		 * The meta object literal for the '<em><b>Parameter Codes</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CALL_ACTION__PARAMETER_CODES = eINSTANCE.getCallAction_ParameterCodes();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.webapp.impl.LetActionImpl <em>Let Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.webapp.impl.LetActionImpl
		 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getLetAction()
		 * @generated
		 */
		EClass LET_ACTION = eINSTANCE.getLetAction();

		/**
		 * The meta object literal for the '<em><b>Left Code</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LET_ACTION__LEFT_CODE = eINSTANCE.getLetAction_LeftCode();

		/**
		 * The meta object literal for the '<em><b>Right</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LET_ACTION__RIGHT = eINSTANCE.getLetAction_Right();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.webapp.impl.ForActionImpl <em>For Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.webapp.impl.ForActionImpl
		 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getForAction()
		 * @generated
		 */
		EClass FOR_ACTION = eINSTANCE.getForAction();

		/**
		 * The meta object literal for the '<em><b>Condition Code</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FOR_ACTION__CONDITION_CODE = eINSTANCE.getForAction_ConditionCode();

		/**
		 * The meta object literal for the '<em><b>Body Action</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FOR_ACTION__BODY_ACTION = eINSTANCE.getForAction_BodyAction();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.webapp.impl.IfActionImpl <em>If Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.webapp.impl.IfActionImpl
		 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getIfAction()
		 * @generated
		 */
		EClass IF_ACTION = eINSTANCE.getIfAction();

		/**
		 * The meta object literal for the '<em><b>Condition Code</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IF_ACTION__CONDITION_CODE = eINSTANCE.getIfAction_ConditionCode();

		/**
		 * The meta object literal for the '<em><b>Then Action</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IF_ACTION__THEN_ACTION = eINSTANCE.getIfAction_ThenAction();

		/**
		 * The meta object literal for the '<em><b>Else Action</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IF_ACTION__ELSE_ACTION = eINSTANCE.getIfAction_ElseAction();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.webapp.impl.SequenceActionImpl <em>Sequence Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.webapp.impl.SequenceActionImpl
		 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getSequenceAction()
		 * @generated
		 */
		EClass SEQUENCE_ACTION = eINSTANCE.getSequenceAction();

		/**
		 * The meta object literal for the '<em><b>Actions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SEQUENCE_ACTION__ACTIONS = eINSTANCE.getSequenceAction_Actions();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.webapp.impl.TryCatchActionImpl <em>Try Catch Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.webapp.impl.TryCatchActionImpl
		 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getTryCatchAction()
		 * @generated
		 */
		EClass TRY_CATCH_ACTION = eINSTANCE.getTryCatchAction();

		/**
		 * The meta object literal for the '<em><b>Try Part</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRY_CATCH_ACTION__TRY_PART = eINSTANCE.getTryCatchAction_TryPart();

		/**
		 * The meta object literal for the '<em><b>Catch Parts</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRY_CATCH_ACTION__CATCH_PARTS = eINSTANCE.getTryCatchAction_CatchParts();

		/**
		 * The meta object literal for the '<em><b>Final Part</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRY_CATCH_ACTION__FINAL_PART = eINSTANCE.getTryCatchAction_FinalPart();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.webapp.impl.CatchPartImpl <em>Catch Part</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.webapp.impl.CatchPartImpl
		 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getCatchPart()
		 * @generated
		 */
		EClass CATCH_PART = eINSTANCE.getCatchPart();

		/**
		 * The meta object literal for the '<em><b>Exception Variable</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CATCH_PART__EXCEPTION_VARIABLE = eINSTANCE.getCatchPart_ExceptionVariable();

		/**
		 * The meta object literal for the '<em><b>Actioin</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CATCH_PART__ACTIOIN = eINSTANCE.getCatchPart_Actioin();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.webapp.impl.JavascriptActionImpl <em>Javascript Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.webapp.impl.JavascriptActionImpl
		 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getJavascriptAction()
		 * @generated
		 */
		EClass JAVASCRIPT_ACTION = eINSTANCE.getJavascriptAction();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.webapp.impl.JQueryAjaxActionImpl <em>JQuery Ajax Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.webapp.impl.JQueryAjaxActionImpl
		 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getJQueryAjaxAction()
		 * @generated
		 */
		EClass JQUERY_AJAX_ACTION = eINSTANCE.getJQueryAjaxAction();

		/**
		 * The meta object literal for the '<em><b>Selector Code</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute JQUERY_AJAX_ACTION__SELECTOR_CODE = eINSTANCE.getJQueryAjaxAction_SelectorCode();

		/**
		 * The meta object literal for the '<em><b>Ajax Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute JQUERY_AJAX_ACTION__AJAX_TYPE = eINSTANCE.getJQueryAjaxAction_AjaxType();

		/**
		 * The meta object literal for the '<em><b>Data Code</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute JQUERY_AJAX_ACTION__DATA_CODE = eINSTANCE.getJQueryAjaxAction_DataCode();

		/**
		 * The meta object literal for the '<em><b>Data Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute JQUERY_AJAX_ACTION__DATA_TYPE = eINSTANCE.getJQueryAjaxAction_DataType();

		/**
		 * The meta object literal for the '<em><b>Callback Function</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference JQUERY_AJAX_ACTION__CALLBACK_FUNCTION = eINSTANCE.getJQueryAjaxAction_CallbackFunction();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.webapp.impl.JQueryEventActionImpl <em>JQuery Event Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.webapp.impl.JQueryEventActionImpl
		 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getJQueryEventAction()
		 * @generated
		 */
		EClass JQUERY_EVENT_ACTION = eINSTANCE.getJQueryEventAction();

		/**
		 * The meta object literal for the '<em><b>Selector Code</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute JQUERY_EVENT_ACTION__SELECTOR_CODE = eINSTANCE.getJQueryEventAction_SelectorCode();

		/**
		 * The meta object literal for the '<em><b>Event</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute JQUERY_EVENT_ACTION__EVENT = eINSTANCE.getJQueryEventAction_Event();

		/**
		 * The meta object literal for the '<em><b>Handler</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference JQUERY_EVENT_ACTION__HANDLER = eINSTANCE.getJQueryEventAction_Handler();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.webapp.impl.SQLActionImpl <em>SQL Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.webapp.impl.SQLActionImpl
		 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getSQLAction()
		 * @generated
		 */
		EClass SQL_ACTION = eINSTANCE.getSQLAction();

		/**
		 * The meta object literal for the '<em><b>Database</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SQL_ACTION__DATABASE = eINSTANCE.getSQLAction_Database();

		/**
		 * The meta object literal for the '<em><b>Sql Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SQL_ACTION__SQL_NAME = eINSTANCE.getSQLAction_SqlName();

		/**
		 * The meta object literal for the '<em><b>Handling Action</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SQL_ACTION__HANDLING_ACTION = eINSTANCE.getSQLAction_HandlingAction();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.webapp.impl.SQLSelectActionImpl <em>SQL Select Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.webapp.impl.SQLSelectActionImpl
		 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getSQLSelectAction()
		 * @generated
		 */
		EClass SQL_SELECT_ACTION = eINSTANCE.getSQLSelectAction();

		/**
		 * The meta object literal for the '<em><b>Tables</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SQL_SELECT_ACTION__TABLES = eINSTANCE.getSQLSelectAction_Tables();

		/**
		 * The meta object literal for the '<em><b>Columns</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SQL_SELECT_ACTION__COLUMNS = eINSTANCE.getSQLSelectAction_Columns();

		/**
		 * The meta object literal for the '<em><b>Condition Code</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SQL_SELECT_ACTION__CONDITION_CODE = eINSTANCE.getSQLSelectAction_ConditionCode();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.webapp.impl.SQLInsertActionImpl <em>SQL Insert Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.webapp.impl.SQLInsertActionImpl
		 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getSQLInsertAction()
		 * @generated
		 */
		EClass SQL_INSERT_ACTION = eINSTANCE.getSQLInsertAction();

		/**
		 * The meta object literal for the '<em><b>Table</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SQL_INSERT_ACTION__TABLE = eINSTANCE.getSQLInsertAction_Table();

		/**
		 * The meta object literal for the '<em><b>Columns</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SQL_INSERT_ACTION__COLUMNS = eINSTANCE.getSQLInsertAction_Columns();

		/**
		 * The meta object literal for the '<em><b>Value Codes</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SQL_INSERT_ACTION__VALUE_CODES = eINSTANCE.getSQLInsertAction_ValueCodes();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.webapp.impl.SQLUpdateActionImpl <em>SQL Update Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.webapp.impl.SQLUpdateActionImpl
		 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getSQLUpdateAction()
		 * @generated
		 */
		EClass SQL_UPDATE_ACTION = eINSTANCE.getSQLUpdateAction();

		/**
		 * The meta object literal for the '<em><b>Table</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SQL_UPDATE_ACTION__TABLE = eINSTANCE.getSQLUpdateAction_Table();

		/**
		 * The meta object literal for the '<em><b>Columns</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SQL_UPDATE_ACTION__COLUMNS = eINSTANCE.getSQLUpdateAction_Columns();

		/**
		 * The meta object literal for the '<em><b>Value Codes</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SQL_UPDATE_ACTION__VALUE_CODES = eINSTANCE.getSQLUpdateAction_ValueCodes();

		/**
		 * The meta object literal for the '<em><b>Condition Code</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SQL_UPDATE_ACTION__CONDITION_CODE = eINSTANCE.getSQLUpdateAction_ConditionCode();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.webapp.impl.SQLDeleteActionImpl <em>SQL Delete Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.webapp.impl.SQLDeleteActionImpl
		 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getSQLDeleteAction()
		 * @generated
		 */
		EClass SQL_DELETE_ACTION = eINSTANCE.getSQLDeleteAction();

		/**
		 * The meta object literal for the '<em><b>Table</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SQL_DELETE_ACTION__TABLE = eINSTANCE.getSQLDeleteAction_Table();

		/**
		 * The meta object literal for the '<em><b>Condition Code</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SQL_DELETE_ACTION__CONDITION_CODE = eINSTANCE.getSQLDeleteAction_ConditionCode();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.webapp.impl.SQLExecutionActionImpl <em>SQL Execution Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.webapp.impl.SQLExecutionActionImpl
		 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getSQLExecutionAction()
		 * @generated
		 */
		EClass SQL_EXECUTION_ACTION = eINSTANCE.getSQLExecutionAction();

		/**
		 * The meta object literal for the '<em><b>Sql Code</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SQL_EXECUTION_ACTION__SQL_CODE = eINSTANCE.getSQLExecutionAction_SqlCode();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.webapp.impl.SQLResultSetIterationActionImpl <em>SQL Result Set Iteration Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.webapp.impl.SQLResultSetIterationActionImpl
		 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getSQLResultSetIterationAction()
		 * @generated
		 */
		EClass SQL_RESULT_SET_ITERATION_ACTION = eINSTANCE.getSQLResultSetIterationAction();

		/**
		 * The meta object literal for the '<em><b>Action</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SQL_RESULT_SET_ITERATION_ACTION__ACTION = eINSTANCE.getSQLResultSetIterationAction_Action();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.webapp.impl.ServerPageActionImpl <em>Server Page Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.webapp.impl.ServerPageActionImpl
		 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getServerPageAction()
		 * @generated
		 */
		EClass SERVER_PAGE_ACTION = eINSTANCE.getServerPageAction();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.webapp.impl.ServerPageTemplateActionImpl <em>Server Page Template Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.webapp.impl.ServerPageTemplateActionImpl
		 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getServerPageTemplateAction()
		 * @generated
		 */
		EClass SERVER_PAGE_TEMPLATE_ACTION = eINSTANCE.getServerPageTemplateAction();

		/**
		 * The meta object literal for the '<em><b>Text</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SERVER_PAGE_TEMPLATE_ACTION__TEXT = eINSTANCE.getServerPageTemplateAction_Text();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.webapp.impl.ServerPageExpressionActionImpl <em>Server Page Expression Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.webapp.impl.ServerPageExpressionActionImpl
		 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getServerPageExpressionAction()
		 * @generated
		 */
		EClass SERVER_PAGE_EXPRESSION_ACTION = eINSTANCE.getServerPageExpressionAction();

		/**
		 * The meta object literal for the '<em><b>Expression</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SERVER_PAGE_EXPRESSION_ACTION__EXPRESSION = eINSTANCE.getServerPageExpressionAction_Expression();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.webapp.impl.ServerPageStructuredTemplateActionImpl <em>Server Page Structured Template Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.webapp.impl.ServerPageStructuredTemplateActionImpl
		 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getServerPageStructuredTemplateAction()
		 * @generated
		 */
		EClass SERVER_PAGE_STRUCTURED_TEMPLATE_ACTION = eINSTANCE.getServerPageStructuredTemplateAction();

		/**
		 * The meta object literal for the '<em><b>Prefix</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SERVER_PAGE_STRUCTURED_TEMPLATE_ACTION__PREFIX = eINSTANCE.getServerPageStructuredTemplateAction_Prefix();

		/**
		 * The meta object literal for the '<em><b>Action</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SERVER_PAGE_STRUCTURED_TEMPLATE_ACTION__ACTION = eINSTANCE.getServerPageStructuredTemplateAction_Action();

		/**
		 * The meta object literal for the '<em><b>Suffix</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SERVER_PAGE_STRUCTURED_TEMPLATE_ACTION__SUFFIX = eINSTANCE.getServerPageStructuredTemplateAction_Suffix();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.webapp.impl.WebApplicationImpl <em>Web Application</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.webapp.impl.WebApplicationImpl
		 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getWebApplication()
		 * @generated
		 */
		EClass WEB_APPLICATION = eINSTANCE.getWebApplication();

		/**
		 * The meta object literal for the '<em><b>Database Util Class</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute WEB_APPLICATION__DATABASE_UTIL_CLASS = eINSTANCE.getWebApplication_DatabaseUtilClass();

		/**
		 * The meta object literal for the '<em><b>Databases</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference WEB_APPLICATION__DATABASES = eINSTANCE.getWebApplication_Databases();

		/**
		 * The meta object literal for the '<em><b>Interfaces</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference WEB_APPLICATION__INTERFACES = eINSTANCE.getWebApplication_Interfaces();

		/**
		 * The meta object literal for the '<em><b>Page Components</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference WEB_APPLICATION__PAGE_COMPONENTS = eINSTANCE.getWebApplication_PageComponents();

		/**
		 * The meta object literal for the '<em><b>Server Components</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference WEB_APPLICATION__SERVER_COMPONENTS = eINSTANCE.getWebApplication_ServerComponents();

		/**
		 * The meta object literal for the '<em><b>Class Components</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference WEB_APPLICATION__CLASS_COMPONENTS = eINSTANCE.getWebApplication_ClassComponents();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.webapp.AjaxType <em>Ajax Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.webapp.AjaxType
		 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getAjaxType()
		 * @generated
		 */
		EEnum AJAX_TYPE = eINSTANCE.getAjaxType();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.webapp.PageContentType <em>Page Content Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.webapp.PageContentType
		 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getPageContentType()
		 * @generated
		 */
		EEnum PAGE_CONTENT_TYPE = eINSTANCE.getPageContentType();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.webapp.DataFormat <em>Data Format</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.webapp.DataFormat
		 * @see edu.ustb.sei.mde.webapp.impl.WebAppPackageImpl#getDataFormat()
		 * @generated
		 */
		EEnum DATA_FORMAT = eINSTANCE.getDataFormat();

	}

} //WebAppPackage
