/**
 */
package edu.ustb.sei.mde.webapp.impl;

import edu.ustb.sei.mde.webapp.Action;
import edu.ustb.sei.mde.webapp.ActionOperation;
import edu.ustb.sei.mde.webapp.AjaxType;
import edu.ustb.sei.mde.webapp.CallAction;
import edu.ustb.sei.mde.webapp.CatchPart;
import edu.ustb.sei.mde.webapp.ClassComponent;
import edu.ustb.sei.mde.webapp.CodeAction;
import edu.ustb.sei.mde.webapp.CodeOperation;
import edu.ustb.sei.mde.webapp.Component;
import edu.ustb.sei.mde.webapp.Composition;
import edu.ustb.sei.mde.webapp.DataFormat;
import edu.ustb.sei.mde.webapp.Database;
import edu.ustb.sei.mde.webapp.Deployable;
import edu.ustb.sei.mde.webapp.DeployablePointer;
import edu.ustb.sei.mde.webapp.EventHandler;
import edu.ustb.sei.mde.webapp.ForAction;
import edu.ustb.sei.mde.webapp.HTMLNode;
import edu.ustb.sei.mde.webapp.IfAction;
import edu.ustb.sei.mde.webapp.Interface;
import edu.ustb.sei.mde.webapp.JQueryAjaxAction;
import edu.ustb.sei.mde.webapp.JQueryEventAction;
import edu.ustb.sei.mde.webapp.JavascriptAction;
import edu.ustb.sei.mde.webapp.LetAction;
import edu.ustb.sei.mde.webapp.NamedElement;
import edu.ustb.sei.mde.webapp.Operation;
import edu.ustb.sei.mde.webapp.OperationDeclaration;
import edu.ustb.sei.mde.webapp.OperationRealization;
import edu.ustb.sei.mde.webapp.PageComponent;
import edu.ustb.sei.mde.webapp.PageComponentComposition;
import edu.ustb.sei.mde.webapp.PageContentType;
import edu.ustb.sei.mde.webapp.Parameter;
import edu.ustb.sei.mde.webapp.SQLAction;
import edu.ustb.sei.mde.webapp.SQLDeleteAction;
import edu.ustb.sei.mde.webapp.SQLExecutionAction;
import edu.ustb.sei.mde.webapp.SQLInsertAction;
import edu.ustb.sei.mde.webapp.SQLResultSetIterationAction;
import edu.ustb.sei.mde.webapp.SQLSelectAction;
import edu.ustb.sei.mde.webapp.SQLUpdateAction;
import edu.ustb.sei.mde.webapp.SequenceAction;
import edu.ustb.sei.mde.webapp.ServerComponent;
import edu.ustb.sei.mde.webapp.ServerPageAction;
import edu.ustb.sei.mde.webapp.ServerPageExpressionAction;
import edu.ustb.sei.mde.webapp.ServerPageStructuredTemplateAction;
import edu.ustb.sei.mde.webapp.ServerPageTemplateAction;
import edu.ustb.sei.mde.webapp.StatementContainer;
import edu.ustb.sei.mde.webapp.TryCatchAction;
import edu.ustb.sei.mde.webapp.TypedElement;
import edu.ustb.sei.mde.webapp.WebAppFactory;
import edu.ustb.sei.mde.webapp.WebAppPackage;

import edu.ustb.sei.mde.webapp.WebApplication;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class WebAppPackageImpl extends EPackageImpl implements WebAppPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass namedElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass typedElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass deployableEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass deployablePointerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass interfaceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass operationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass operationDeclarationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass parameterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass componentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass operationRealizationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass codeOperationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass actionOperationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass actionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass pageComponentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass htmlNodeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass compositionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass pageComponentCompositionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass eventHandlerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass serverComponentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass classComponentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass statementContainerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass databaseEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass codeActionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass callActionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass letActionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass forActionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ifActionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sequenceActionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tryCatchActionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass catchPartEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass javascriptActionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass jQueryAjaxActionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass jQueryEventActionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sqlActionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sqlSelectActionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sqlInsertActionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sqlUpdateActionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sqlDeleteActionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sqlExecutionActionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sqlResultSetIterationActionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass serverPageActionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass serverPageTemplateActionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass serverPageExpressionActionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass serverPageStructuredTemplateActionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass webApplicationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum ajaxTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum pageContentTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum dataFormatEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see edu.ustb.sei.mde.webapp.WebAppPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private WebAppPackageImpl() {
		super(eNS_URI, WebAppFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link WebAppPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static WebAppPackage init() {
		if (isInited) return (WebAppPackage)EPackage.Registry.INSTANCE.getEPackage(WebAppPackage.eNS_URI);

		// Obtain or create and register package
		WebAppPackageImpl theWebAppPackage = (WebAppPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof WebAppPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new WebAppPackageImpl());

		isInited = true;

		// Create package meta-data objects
		theWebAppPackage.createPackageContents();

		// Initialize created meta-data
		theWebAppPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theWebAppPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(WebAppPackage.eNS_URI, theWebAppPackage);
		return theWebAppPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNamedElement() {
		return namedElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getNamedElement_Name() {
		return (EAttribute)namedElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTypedElement() {
		return typedElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTypedElement_Type() {
		return (EAttribute)typedElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDeployable() {
		return deployableEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDeployable_DeployPath() {
		return (EAttribute)deployableEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getDeployable__GetURL() {
		return deployableEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDeployablePointer() {
		return deployablePointerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDeployablePointer_Url() {
		return (EAttribute)deployablePointerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDeployablePointer_Deployable() {
		return (EReference)deployablePointerEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getInterface() {
		return interfaceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getInterface_Operations() {
		return (EReference)interfaceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOperation() {
		return operationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperation_Parameters() {
		return (EReference)operationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOperationDeclaration() {
		return operationDeclarationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getParameter() {
		return parameterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getComponent() {
		return componentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getComponent_Operations() {
		return (EReference)componentEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getComponent_RequiredInterfaces() {
		return (EReference)componentEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getComponent_ProvidedInterfaces() {
		return (EReference)componentEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getComponent_Compositions() {
		return (EReference)componentEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOperationRealization() {
		return operationRealizationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getOperationRealization_Declaration() {
		return (EReference)operationRealizationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCodeOperation() {
		return codeOperationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCodeOperation_BodyCode() {
		return (EAttribute)codeOperationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getActionOperation() {
		return actionOperationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getActionOperation_Action() {
		return (EReference)actionOperationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAction() {
		return actionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPageComponent() {
		return pageComponentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPageComponent_Node() {
		return (EReference)pageComponentEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPageComponent_Page() {
		return (EAttribute)pageComponentEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getHTMLNode() {
		return htmlNodeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getHTMLNode_Tag() {
		return (EAttribute)htmlNodeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getHTMLNode_Id() {
		return (EAttribute)htmlNodeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getHTMLNode_Class() {
		return (EAttribute)htmlNodeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getHTMLNode_Extra() {
		return (EAttribute)htmlNodeEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getHTMLNode_InnerText() {
		return (EAttribute)htmlNodeEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHTMLNode_Children() {
		return (EReference)htmlNodeEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHTMLNode_Handlers() {
		return (EReference)htmlNodeEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getHTMLNode_Styles() {
		return (EAttribute)htmlNodeEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getComposition() {
		return compositionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getComposition_Owner() {
		return (EReference)compositionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPageComponentComposition() {
		return pageComponentCompositionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPageComponentComposition_Time() {
		return (EAttribute)pageComponentCompositionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPageComponentComposition_ContainerSelector() {
		return (EAttribute)pageComponentCompositionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEventHandler() {
		return eventHandlerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEventHandler_Event() {
		return (EAttribute)eventHandlerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEventHandler_Owner() {
		return (EReference)eventHandlerEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEventHandler_Handler() {
		return (EReference)eventHandlerEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getServerComponent() {
		return serverComponentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getServerComponent_Imports() {
		return (EAttribute)serverComponentEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getServerComponent_ContentType() {
		return (EAttribute)serverComponentEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getClassComponent() {
		return classComponentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getClassComponent_PackageName() {
		return (EAttribute)classComponentEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getClassComponent_Imports() {
		return (EAttribute)classComponentEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getStatementContainer() {
		return statementContainerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDatabase() {
		return databaseEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDatabase_DriverName() {
		return (EAttribute)databaseEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDatabase_Url() {
		return (EAttribute)databaseEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDatabase_Username() {
		return (EAttribute)databaseEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDatabase_Password() {
		return (EAttribute)databaseEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDatabase_InitialSize() {
		return (EAttribute)databaseEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDatabase_MinIdle() {
		return (EAttribute)databaseEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDatabase_MaxIdle() {
		return (EAttribute)databaseEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDatabase_MaxActive() {
		return (EAttribute)databaseEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDatabase_MaxWait() {
		return (EAttribute)databaseEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDatabase_Datamodel() {
		return (EReference)databaseEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCodeAction() {
		return codeActionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCodeAction_Code() {
		return (EAttribute)codeActionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCallAction() {
		return callActionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCallAction_InvocationTarget() {
		return (EReference)callActionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCallAction_ParameterCodes() {
		return (EAttribute)callActionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLetAction() {
		return letActionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLetAction_LeftCode() {
		return (EAttribute)letActionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLetAction_Right() {
		return (EReference)letActionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getForAction() {
		return forActionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getForAction_ConditionCode() {
		return (EAttribute)forActionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getForAction_BodyAction() {
		return (EReference)forActionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIfAction() {
		return ifActionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIfAction_ConditionCode() {
		return (EAttribute)ifActionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIfAction_ThenAction() {
		return (EReference)ifActionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getIfAction_ElseAction() {
		return (EReference)ifActionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSequenceAction() {
		return sequenceActionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSequenceAction_Actions() {
		return (EReference)sequenceActionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTryCatchAction() {
		return tryCatchActionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTryCatchAction_TryPart() {
		return (EReference)tryCatchActionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTryCatchAction_CatchParts() {
		return (EReference)tryCatchActionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTryCatchAction_FinalPart() {
		return (EReference)tryCatchActionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCatchPart() {
		return catchPartEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCatchPart_ExceptionVariable() {
		return (EAttribute)catchPartEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCatchPart_Actioin() {
		return (EReference)catchPartEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getJavascriptAction() {
		return javascriptActionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getJQueryAjaxAction() {
		return jQueryAjaxActionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getJQueryAjaxAction_SelectorCode() {
		return (EAttribute)jQueryAjaxActionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getJQueryAjaxAction_AjaxType() {
		return (EAttribute)jQueryAjaxActionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getJQueryAjaxAction_DataCode() {
		return (EAttribute)jQueryAjaxActionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getJQueryAjaxAction_DataType() {
		return (EAttribute)jQueryAjaxActionEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getJQueryAjaxAction_CallbackFunction() {
		return (EReference)jQueryAjaxActionEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getJQueryEventAction() {
		return jQueryEventActionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getJQueryEventAction_SelectorCode() {
		return (EAttribute)jQueryEventActionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getJQueryEventAction_Event() {
		return (EAttribute)jQueryEventActionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getJQueryEventAction_Handler() {
		return (EReference)jQueryEventActionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSQLAction() {
		return sqlActionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSQLAction_Database() {
		return (EReference)sqlActionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSQLAction_SqlName() {
		return (EAttribute)sqlActionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSQLAction_HandlingAction() {
		return (EReference)sqlActionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSQLSelectAction() {
		return sqlSelectActionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSQLSelectAction_Tables() {
		return (EAttribute)sqlSelectActionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSQLSelectAction_Columns() {
		return (EAttribute)sqlSelectActionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSQLSelectAction_ConditionCode() {
		return (EAttribute)sqlSelectActionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSQLInsertAction() {
		return sqlInsertActionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSQLInsertAction_Table() {
		return (EAttribute)sqlInsertActionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSQLInsertAction_Columns() {
		return (EAttribute)sqlInsertActionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSQLInsertAction_ValueCodes() {
		return (EAttribute)sqlInsertActionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSQLUpdateAction() {
		return sqlUpdateActionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSQLUpdateAction_Table() {
		return (EAttribute)sqlUpdateActionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSQLUpdateAction_Columns() {
		return (EAttribute)sqlUpdateActionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSQLUpdateAction_ValueCodes() {
		return (EAttribute)sqlUpdateActionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSQLUpdateAction_ConditionCode() {
		return (EAttribute)sqlUpdateActionEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSQLDeleteAction() {
		return sqlDeleteActionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSQLDeleteAction_Table() {
		return (EAttribute)sqlDeleteActionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSQLDeleteAction_ConditionCode() {
		return (EAttribute)sqlDeleteActionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSQLExecutionAction() {
		return sqlExecutionActionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSQLExecutionAction_SqlCode() {
		return (EAttribute)sqlExecutionActionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSQLResultSetIterationAction() {
		return sqlResultSetIterationActionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSQLResultSetIterationAction_Action() {
		return (EReference)sqlResultSetIterationActionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getServerPageAction() {
		return serverPageActionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getServerPageTemplateAction() {
		return serverPageTemplateActionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getServerPageTemplateAction_Text() {
		return (EAttribute)serverPageTemplateActionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getServerPageExpressionAction() {
		return serverPageExpressionActionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getServerPageExpressionAction_Expression() {
		return (EAttribute)serverPageExpressionActionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getServerPageStructuredTemplateAction() {
		return serverPageStructuredTemplateActionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getServerPageStructuredTemplateAction_Prefix() {
		return (EAttribute)serverPageStructuredTemplateActionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getServerPageStructuredTemplateAction_Action() {
		return (EReference)serverPageStructuredTemplateActionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getServerPageStructuredTemplateAction_Suffix() {
		return (EAttribute)serverPageStructuredTemplateActionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getWebApplication() {
		return webApplicationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getWebApplication_DatabaseUtilClass() {
		return (EAttribute)webApplicationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getWebApplication_Databases() {
		return (EReference)webApplicationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getWebApplication_Interfaces() {
		return (EReference)webApplicationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getWebApplication_PageComponents() {
		return (EReference)webApplicationEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getWebApplication_ServerComponents() {
		return (EReference)webApplicationEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getWebApplication_ClassComponents() {
		return (EReference)webApplicationEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getAjaxType() {
		return ajaxTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getPageContentType() {
		return pageContentTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getDataFormat() {
		return dataFormatEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WebAppFactory getWebAppFactory() {
		return (WebAppFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		namedElementEClass = createEClass(NAMED_ELEMENT);
		createEAttribute(namedElementEClass, NAMED_ELEMENT__NAME);

		typedElementEClass = createEClass(TYPED_ELEMENT);
		createEAttribute(typedElementEClass, TYPED_ELEMENT__TYPE);

		deployableEClass = createEClass(DEPLOYABLE);
		createEAttribute(deployableEClass, DEPLOYABLE__DEPLOY_PATH);
		createEOperation(deployableEClass, DEPLOYABLE___GET_URL);

		deployablePointerEClass = createEClass(DEPLOYABLE_POINTER);
		createEAttribute(deployablePointerEClass, DEPLOYABLE_POINTER__URL);
		createEReference(deployablePointerEClass, DEPLOYABLE_POINTER__DEPLOYABLE);

		interfaceEClass = createEClass(INTERFACE);
		createEReference(interfaceEClass, INTERFACE__OPERATIONS);

		operationEClass = createEClass(OPERATION);
		createEReference(operationEClass, OPERATION__PARAMETERS);

		operationDeclarationEClass = createEClass(OPERATION_DECLARATION);

		parameterEClass = createEClass(PARAMETER);

		componentEClass = createEClass(COMPONENT);
		createEReference(componentEClass, COMPONENT__OPERATIONS);
		createEReference(componentEClass, COMPONENT__REQUIRED_INTERFACES);
		createEReference(componentEClass, COMPONENT__PROVIDED_INTERFACES);
		createEReference(componentEClass, COMPONENT__COMPOSITIONS);

		operationRealizationEClass = createEClass(OPERATION_REALIZATION);
		createEReference(operationRealizationEClass, OPERATION_REALIZATION__DECLARATION);

		codeOperationEClass = createEClass(CODE_OPERATION);
		createEAttribute(codeOperationEClass, CODE_OPERATION__BODY_CODE);

		actionOperationEClass = createEClass(ACTION_OPERATION);
		createEReference(actionOperationEClass, ACTION_OPERATION__ACTION);

		actionEClass = createEClass(ACTION);

		pageComponentEClass = createEClass(PAGE_COMPONENT);
		createEReference(pageComponentEClass, PAGE_COMPONENT__NODE);
		createEAttribute(pageComponentEClass, PAGE_COMPONENT__PAGE);

		htmlNodeEClass = createEClass(HTML_NODE);
		createEAttribute(htmlNodeEClass, HTML_NODE__TAG);
		createEAttribute(htmlNodeEClass, HTML_NODE__ID);
		createEAttribute(htmlNodeEClass, HTML_NODE__CLASS);
		createEAttribute(htmlNodeEClass, HTML_NODE__EXTRA);
		createEAttribute(htmlNodeEClass, HTML_NODE__INNER_TEXT);
		createEReference(htmlNodeEClass, HTML_NODE__CHILDREN);
		createEReference(htmlNodeEClass, HTML_NODE__HANDLERS);
		createEAttribute(htmlNodeEClass, HTML_NODE__STYLES);

		compositionEClass = createEClass(COMPOSITION);
		createEReference(compositionEClass, COMPOSITION__OWNER);

		pageComponentCompositionEClass = createEClass(PAGE_COMPONENT_COMPOSITION);
		createEAttribute(pageComponentCompositionEClass, PAGE_COMPONENT_COMPOSITION__TIME);
		createEAttribute(pageComponentCompositionEClass, PAGE_COMPONENT_COMPOSITION__CONTAINER_SELECTOR);

		eventHandlerEClass = createEClass(EVENT_HANDLER);
		createEAttribute(eventHandlerEClass, EVENT_HANDLER__EVENT);
		createEReference(eventHandlerEClass, EVENT_HANDLER__OWNER);
		createEReference(eventHandlerEClass, EVENT_HANDLER__HANDLER);

		serverComponentEClass = createEClass(SERVER_COMPONENT);
		createEAttribute(serverComponentEClass, SERVER_COMPONENT__IMPORTS);
		createEAttribute(serverComponentEClass, SERVER_COMPONENT__CONTENT_TYPE);

		classComponentEClass = createEClass(CLASS_COMPONENT);
		createEAttribute(classComponentEClass, CLASS_COMPONENT__PACKAGE_NAME);
		createEAttribute(classComponentEClass, CLASS_COMPONENT__IMPORTS);

		statementContainerEClass = createEClass(STATEMENT_CONTAINER);

		databaseEClass = createEClass(DATABASE);
		createEAttribute(databaseEClass, DATABASE__DRIVER_NAME);
		createEAttribute(databaseEClass, DATABASE__URL);
		createEAttribute(databaseEClass, DATABASE__USERNAME);
		createEAttribute(databaseEClass, DATABASE__PASSWORD);
		createEAttribute(databaseEClass, DATABASE__INITIAL_SIZE);
		createEAttribute(databaseEClass, DATABASE__MIN_IDLE);
		createEAttribute(databaseEClass, DATABASE__MAX_IDLE);
		createEAttribute(databaseEClass, DATABASE__MAX_ACTIVE);
		createEAttribute(databaseEClass, DATABASE__MAX_WAIT);
		createEReference(databaseEClass, DATABASE__DATAMODEL);

		codeActionEClass = createEClass(CODE_ACTION);
		createEAttribute(codeActionEClass, CODE_ACTION__CODE);

		callActionEClass = createEClass(CALL_ACTION);
		createEReference(callActionEClass, CALL_ACTION__INVOCATION_TARGET);
		createEAttribute(callActionEClass, CALL_ACTION__PARAMETER_CODES);

		letActionEClass = createEClass(LET_ACTION);
		createEAttribute(letActionEClass, LET_ACTION__LEFT_CODE);
		createEReference(letActionEClass, LET_ACTION__RIGHT);

		forActionEClass = createEClass(FOR_ACTION);
		createEAttribute(forActionEClass, FOR_ACTION__CONDITION_CODE);
		createEReference(forActionEClass, FOR_ACTION__BODY_ACTION);

		ifActionEClass = createEClass(IF_ACTION);
		createEAttribute(ifActionEClass, IF_ACTION__CONDITION_CODE);
		createEReference(ifActionEClass, IF_ACTION__THEN_ACTION);
		createEReference(ifActionEClass, IF_ACTION__ELSE_ACTION);

		sequenceActionEClass = createEClass(SEQUENCE_ACTION);
		createEReference(sequenceActionEClass, SEQUENCE_ACTION__ACTIONS);

		tryCatchActionEClass = createEClass(TRY_CATCH_ACTION);
		createEReference(tryCatchActionEClass, TRY_CATCH_ACTION__TRY_PART);
		createEReference(tryCatchActionEClass, TRY_CATCH_ACTION__CATCH_PARTS);
		createEReference(tryCatchActionEClass, TRY_CATCH_ACTION__FINAL_PART);

		catchPartEClass = createEClass(CATCH_PART);
		createEAttribute(catchPartEClass, CATCH_PART__EXCEPTION_VARIABLE);
		createEReference(catchPartEClass, CATCH_PART__ACTIOIN);

		javascriptActionEClass = createEClass(JAVASCRIPT_ACTION);

		jQueryAjaxActionEClass = createEClass(JQUERY_AJAX_ACTION);
		createEAttribute(jQueryAjaxActionEClass, JQUERY_AJAX_ACTION__SELECTOR_CODE);
		createEAttribute(jQueryAjaxActionEClass, JQUERY_AJAX_ACTION__AJAX_TYPE);
		createEAttribute(jQueryAjaxActionEClass, JQUERY_AJAX_ACTION__DATA_CODE);
		createEAttribute(jQueryAjaxActionEClass, JQUERY_AJAX_ACTION__DATA_TYPE);
		createEReference(jQueryAjaxActionEClass, JQUERY_AJAX_ACTION__CALLBACK_FUNCTION);

		jQueryEventActionEClass = createEClass(JQUERY_EVENT_ACTION);
		createEAttribute(jQueryEventActionEClass, JQUERY_EVENT_ACTION__SELECTOR_CODE);
		createEAttribute(jQueryEventActionEClass, JQUERY_EVENT_ACTION__EVENT);
		createEReference(jQueryEventActionEClass, JQUERY_EVENT_ACTION__HANDLER);

		sqlActionEClass = createEClass(SQL_ACTION);
		createEReference(sqlActionEClass, SQL_ACTION__DATABASE);
		createEAttribute(sqlActionEClass, SQL_ACTION__SQL_NAME);
		createEReference(sqlActionEClass, SQL_ACTION__HANDLING_ACTION);

		sqlSelectActionEClass = createEClass(SQL_SELECT_ACTION);
		createEAttribute(sqlSelectActionEClass, SQL_SELECT_ACTION__TABLES);
		createEAttribute(sqlSelectActionEClass, SQL_SELECT_ACTION__COLUMNS);
		createEAttribute(sqlSelectActionEClass, SQL_SELECT_ACTION__CONDITION_CODE);

		sqlInsertActionEClass = createEClass(SQL_INSERT_ACTION);
		createEAttribute(sqlInsertActionEClass, SQL_INSERT_ACTION__TABLE);
		createEAttribute(sqlInsertActionEClass, SQL_INSERT_ACTION__COLUMNS);
		createEAttribute(sqlInsertActionEClass, SQL_INSERT_ACTION__VALUE_CODES);

		sqlUpdateActionEClass = createEClass(SQL_UPDATE_ACTION);
		createEAttribute(sqlUpdateActionEClass, SQL_UPDATE_ACTION__TABLE);
		createEAttribute(sqlUpdateActionEClass, SQL_UPDATE_ACTION__COLUMNS);
		createEAttribute(sqlUpdateActionEClass, SQL_UPDATE_ACTION__VALUE_CODES);
		createEAttribute(sqlUpdateActionEClass, SQL_UPDATE_ACTION__CONDITION_CODE);

		sqlDeleteActionEClass = createEClass(SQL_DELETE_ACTION);
		createEAttribute(sqlDeleteActionEClass, SQL_DELETE_ACTION__TABLE);
		createEAttribute(sqlDeleteActionEClass, SQL_DELETE_ACTION__CONDITION_CODE);

		sqlExecutionActionEClass = createEClass(SQL_EXECUTION_ACTION);
		createEAttribute(sqlExecutionActionEClass, SQL_EXECUTION_ACTION__SQL_CODE);

		sqlResultSetIterationActionEClass = createEClass(SQL_RESULT_SET_ITERATION_ACTION);
		createEReference(sqlResultSetIterationActionEClass, SQL_RESULT_SET_ITERATION_ACTION__ACTION);

		serverPageActionEClass = createEClass(SERVER_PAGE_ACTION);

		serverPageTemplateActionEClass = createEClass(SERVER_PAGE_TEMPLATE_ACTION);
		createEAttribute(serverPageTemplateActionEClass, SERVER_PAGE_TEMPLATE_ACTION__TEXT);

		serverPageExpressionActionEClass = createEClass(SERVER_PAGE_EXPRESSION_ACTION);
		createEAttribute(serverPageExpressionActionEClass, SERVER_PAGE_EXPRESSION_ACTION__EXPRESSION);

		serverPageStructuredTemplateActionEClass = createEClass(SERVER_PAGE_STRUCTURED_TEMPLATE_ACTION);
		createEAttribute(serverPageStructuredTemplateActionEClass, SERVER_PAGE_STRUCTURED_TEMPLATE_ACTION__PREFIX);
		createEReference(serverPageStructuredTemplateActionEClass, SERVER_PAGE_STRUCTURED_TEMPLATE_ACTION__ACTION);
		createEAttribute(serverPageStructuredTemplateActionEClass, SERVER_PAGE_STRUCTURED_TEMPLATE_ACTION__SUFFIX);

		webApplicationEClass = createEClass(WEB_APPLICATION);
		createEAttribute(webApplicationEClass, WEB_APPLICATION__DATABASE_UTIL_CLASS);
		createEReference(webApplicationEClass, WEB_APPLICATION__DATABASES);
		createEReference(webApplicationEClass, WEB_APPLICATION__INTERFACES);
		createEReference(webApplicationEClass, WEB_APPLICATION__PAGE_COMPONENTS);
		createEReference(webApplicationEClass, WEB_APPLICATION__SERVER_COMPONENTS);
		createEReference(webApplicationEClass, WEB_APPLICATION__CLASS_COMPONENTS);

		// Create enums
		ajaxTypeEEnum = createEEnum(AJAX_TYPE);
		pageContentTypeEEnum = createEEnum(PAGE_CONTENT_TYPE);
		dataFormatEEnum = createEEnum(DATA_FORMAT);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		interfaceEClass.getESuperTypes().add(this.getNamedElement());
		operationEClass.getESuperTypes().add(this.getNamedElement());
		operationEClass.getESuperTypes().add(this.getTypedElement());
		operationDeclarationEClass.getESuperTypes().add(this.getOperation());
		parameterEClass.getESuperTypes().add(this.getNamedElement());
		parameterEClass.getESuperTypes().add(this.getTypedElement());
		componentEClass.getESuperTypes().add(this.getNamedElement());
		operationRealizationEClass.getESuperTypes().add(this.getOperation());
		codeOperationEClass.getESuperTypes().add(this.getOperationRealization());
		actionOperationEClass.getESuperTypes().add(this.getOperationRealization());
		actionOperationEClass.getESuperTypes().add(this.getStatementContainer());
		pageComponentEClass.getESuperTypes().add(this.getComponent());
		pageComponentEClass.getESuperTypes().add(this.getDeployable());
		compositionEClass.getESuperTypes().add(this.getDeployablePointer());
		pageComponentCompositionEClass.getESuperTypes().add(this.getComposition());
		serverComponentEClass.getESuperTypes().add(this.getComponent());
		serverComponentEClass.getESuperTypes().add(this.getDeployable());
		classComponentEClass.getESuperTypes().add(this.getComponent());
		databaseEClass.getESuperTypes().add(this.getNamedElement());
		codeActionEClass.getESuperTypes().add(this.getAction());
		callActionEClass.getESuperTypes().add(this.getAction());
		letActionEClass.getESuperTypes().add(this.getAction());
		forActionEClass.getESuperTypes().add(this.getAction());
		forActionEClass.getESuperTypes().add(this.getStatementContainer());
		ifActionEClass.getESuperTypes().add(this.getAction());
		ifActionEClass.getESuperTypes().add(this.getStatementContainer());
		sequenceActionEClass.getESuperTypes().add(this.getAction());
		sequenceActionEClass.getESuperTypes().add(this.getStatementContainer());
		tryCatchActionEClass.getESuperTypes().add(this.getAction());
		tryCatchActionEClass.getESuperTypes().add(this.getStatementContainer());
		catchPartEClass.getESuperTypes().add(this.getStatementContainer());
		javascriptActionEClass.getESuperTypes().add(this.getAction());
		jQueryAjaxActionEClass.getESuperTypes().add(this.getJavascriptAction());
		jQueryAjaxActionEClass.getESuperTypes().add(this.getDeployablePointer());
		jQueryEventActionEClass.getESuperTypes().add(this.getJavascriptAction());
		sqlActionEClass.getESuperTypes().add(this.getAction());
		sqlActionEClass.getESuperTypes().add(this.getStatementContainer());
		sqlSelectActionEClass.getESuperTypes().add(this.getSQLAction());
		sqlInsertActionEClass.getESuperTypes().add(this.getSQLAction());
		sqlUpdateActionEClass.getESuperTypes().add(this.getSQLAction());
		sqlDeleteActionEClass.getESuperTypes().add(this.getSQLAction());
		sqlExecutionActionEClass.getESuperTypes().add(this.getSQLAction());
		sqlResultSetIterationActionEClass.getESuperTypes().add(this.getAction());
		sqlResultSetIterationActionEClass.getESuperTypes().add(this.getStatementContainer());
		serverPageActionEClass.getESuperTypes().add(this.getAction());
		serverPageTemplateActionEClass.getESuperTypes().add(this.getServerPageAction());
		serverPageExpressionActionEClass.getESuperTypes().add(this.getServerPageAction());
		serverPageStructuredTemplateActionEClass.getESuperTypes().add(this.getServerPageAction());
		serverPageStructuredTemplateActionEClass.getESuperTypes().add(this.getStatementContainer());
		webApplicationEClass.getESuperTypes().add(this.getNamedElement());

		// Initialize classes, features, and operations; add parameters
		initEClass(namedElementEClass, NamedElement.class, "NamedElement", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getNamedElement_Name(), ecorePackage.getEString(), "name", null, 0, 1, NamedElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(typedElementEClass, TypedElement.class, "TypedElement", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTypedElement_Type(), ecorePackage.getEString(), "type", null, 0, 1, TypedElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(deployableEClass, Deployable.class, "Deployable", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDeployable_DeployPath(), ecorePackage.getEString(), "deployPath", null, 0, 1, Deployable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEOperation(getDeployable__GetURL(), ecorePackage.getEString(), "getURL", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(deployablePointerEClass, DeployablePointer.class, "DeployablePointer", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDeployablePointer_Url(), ecorePackage.getEString(), "url", null, 0, 1, DeployablePointer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDeployablePointer_Deployable(), this.getDeployable(), null, "deployable", null, 0, 1, DeployablePointer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(interfaceEClass, Interface.class, "Interface", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getInterface_Operations(), this.getOperationDeclaration(), null, "operations", null, 0, -1, Interface.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(operationEClass, Operation.class, "Operation", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getOperation_Parameters(), this.getParameter(), null, "parameters", null, 0, -1, Operation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(operationDeclarationEClass, OperationDeclaration.class, "OperationDeclaration", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(parameterEClass, Parameter.class, "Parameter", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(componentEClass, Component.class, "Component", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getComponent_Operations(), this.getOperationRealization(), null, "operations", null, 0, -1, Component.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getComponent_RequiredInterfaces(), this.getInterface(), null, "requiredInterfaces", null, 0, -1, Component.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getComponent_ProvidedInterfaces(), this.getInterface(), null, "providedInterfaces", null, 0, -1, Component.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getComponent_Compositions(), this.getComposition(), this.getComposition_Owner(), "compositions", null, 0, -1, Component.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(operationRealizationEClass, OperationRealization.class, "OperationRealization", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getOperationRealization_Declaration(), this.getOperationDeclaration(), null, "declaration", null, 0, 1, OperationRealization.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(codeOperationEClass, CodeOperation.class, "CodeOperation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCodeOperation_BodyCode(), ecorePackage.getEString(), "bodyCode", null, 0, 1, CodeOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(actionOperationEClass, ActionOperation.class, "ActionOperation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getActionOperation_Action(), this.getAction(), null, "action", null, 1, 1, ActionOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(actionEClass, Action.class, "Action", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(pageComponentEClass, PageComponent.class, "PageComponent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPageComponent_Node(), this.getHTMLNode(), null, "node", null, 0, 1, PageComponent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPageComponent_Page(), ecorePackage.getEBoolean(), "page", "false", 0, 1, PageComponent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(htmlNodeEClass, HTMLNode.class, "HTMLNode", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getHTMLNode_Tag(), ecorePackage.getEString(), "tag", "div", 0, 1, HTMLNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getHTMLNode_Id(), ecorePackage.getEString(), "id", null, 0, 1, HTMLNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getHTMLNode_Class(), ecorePackage.getEString(), "class", null, 0, 1, HTMLNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getHTMLNode_Extra(), ecorePackage.getEString(), "extra", null, 0, -1, HTMLNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getHTMLNode_InnerText(), ecorePackage.getEString(), "innerText", null, 0, 1, HTMLNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getHTMLNode_Children(), this.getHTMLNode(), null, "children", null, 0, -1, HTMLNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getHTMLNode_Handlers(), this.getEventHandler(), this.getEventHandler_Owner(), "handlers", null, 0, -1, HTMLNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getHTMLNode_Styles(), ecorePackage.getEString(), "styles", null, 0, -1, HTMLNode.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(compositionEClass, Composition.class, "Composition", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getComposition_Owner(), this.getComponent(), this.getComponent_Compositions(), "owner", null, 0, 1, Composition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(pageComponentCompositionEClass, PageComponentComposition.class, "PageComponentComposition", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPageComponentComposition_Time(), ecorePackage.getEString(), "time", null, 0, 1, PageComponentComposition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPageComponentComposition_ContainerSelector(), ecorePackage.getEString(), "containerSelector", null, 0, 1, PageComponentComposition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(eventHandlerEClass, EventHandler.class, "EventHandler", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getEventHandler_Event(), ecorePackage.getEString(), "event", null, 0, 1, EventHandler.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEventHandler_Owner(), this.getHTMLNode(), this.getHTMLNode_Handlers(), "owner", null, 0, 1, EventHandler.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEventHandler_Handler(), this.getOperation(), null, "handler", null, 0, 1, EventHandler.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(serverComponentEClass, ServerComponent.class, "ServerComponent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getServerComponent_Imports(), ecorePackage.getEString(), "imports", null, 0, -1, ServerComponent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getServerComponent_ContentType(), this.getPageContentType(), "contentType", null, 0, 1, ServerComponent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(classComponentEClass, ClassComponent.class, "ClassComponent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getClassComponent_PackageName(), ecorePackage.getEString(), "packageName", null, 0, 1, ClassComponent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getClassComponent_Imports(), ecorePackage.getEString(), "imports", null, 0, -1, ClassComponent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(statementContainerEClass, StatementContainer.class, "StatementContainer", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(databaseEClass, Database.class, "Database", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDatabase_DriverName(), ecorePackage.getEString(), "driverName", null, 1, 1, Database.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDatabase_Url(), ecorePackage.getEString(), "url", null, 1, 1, Database.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDatabase_Username(), ecorePackage.getEString(), "username", null, 1, 1, Database.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDatabase_Password(), ecorePackage.getEString(), "password", null, 1, 1, Database.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDatabase_InitialSize(), ecorePackage.getEInt(), "initialSize", "50", 1, 1, Database.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDatabase_MinIdle(), ecorePackage.getEInt(), "minIdle", "10", 1, 1, Database.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDatabase_MaxIdle(), ecorePackage.getEInt(), "maxIdle", "40", 1, 1, Database.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDatabase_MaxActive(), ecorePackage.getEInt(), "maxActive", "100", 1, 1, Database.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getDatabase_MaxWait(), ecorePackage.getELong(), "maxWait", "10000", 1, 1, Database.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDatabase_Datamodel(), ecorePackage.getEObject(), null, "datamodel", null, 0, 1, Database.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(codeActionEClass, CodeAction.class, "CodeAction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCodeAction_Code(), ecorePackage.getEString(), "code", null, 0, 1, CodeAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(callActionEClass, CallAction.class, "CallAction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCallAction_InvocationTarget(), this.getOperation(), null, "invocationTarget", null, 1, 1, CallAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCallAction_ParameterCodes(), ecorePackage.getEString(), "parameterCodes", null, 0, -1, CallAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(letActionEClass, LetAction.class, "LetAction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getLetAction_LeftCode(), ecorePackage.getEString(), "leftCode", null, 1, 1, LetAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getLetAction_Right(), this.getAction(), null, "right", null, 1, 1, LetAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(forActionEClass, ForAction.class, "ForAction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getForAction_ConditionCode(), ecorePackage.getEString(), "conditionCode", null, 1, 1, ForAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getForAction_BodyAction(), this.getAction(), null, "bodyAction", null, 1, 1, ForAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(ifActionEClass, IfAction.class, "IfAction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getIfAction_ConditionCode(), ecorePackage.getEString(), "conditionCode", null, 1, 1, IfAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getIfAction_ThenAction(), this.getAction(), null, "thenAction", null, 1, 1, IfAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getIfAction_ElseAction(), this.getAction(), null, "elseAction", null, 0, 1, IfAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(sequenceActionEClass, SequenceAction.class, "SequenceAction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSequenceAction_Actions(), this.getAction(), null, "actions", null, 0, -1, SequenceAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(tryCatchActionEClass, TryCatchAction.class, "TryCatchAction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTryCatchAction_TryPart(), this.getAction(), null, "tryPart", null, 1, 1, TryCatchAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTryCatchAction_CatchParts(), this.getCatchPart(), null, "catchParts", null, 0, -1, TryCatchAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTryCatchAction_FinalPart(), this.getAction(), null, "finalPart", null, 0, 1, TryCatchAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(catchPartEClass, CatchPart.class, "CatchPart", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCatchPart_ExceptionVariable(), ecorePackage.getEString(), "exceptionVariable", null, 0, 1, CatchPart.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCatchPart_Actioin(), this.getAction(), null, "actioin", null, 0, 1, CatchPart.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(javascriptActionEClass, JavascriptAction.class, "JavascriptAction", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(jQueryAjaxActionEClass, JQueryAjaxAction.class, "JQueryAjaxAction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getJQueryAjaxAction_SelectorCode(), ecorePackage.getEString(), "selectorCode", null, 1, 1, JQueryAjaxAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getJQueryAjaxAction_AjaxType(), this.getAjaxType(), "ajaxType", null, 1, 1, JQueryAjaxAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getJQueryAjaxAction_DataCode(), ecorePackage.getEString(), "dataCode", null, 0, 1, JQueryAjaxAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getJQueryAjaxAction_DataType(), this.getDataFormat(), "dataType", null, 0, 1, JQueryAjaxAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getJQueryAjaxAction_CallbackFunction(), this.getOperationRealization(), null, "callbackFunction", null, 0, 1, JQueryAjaxAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(jQueryEventActionEClass, JQueryEventAction.class, "JQueryEventAction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getJQueryEventAction_SelectorCode(), ecorePackage.getEString(), "selectorCode", null, 1, 1, JQueryEventAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getJQueryEventAction_Event(), ecorePackage.getEString(), "event", null, 1, 1, JQueryEventAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getJQueryEventAction_Handler(), this.getOperationRealization(), null, "handler", null, 1, 1, JQueryEventAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(sqlActionEClass, SQLAction.class, "SQLAction", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSQLAction_Database(), this.getDatabase(), null, "database", null, 0, 1, SQLAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSQLAction_SqlName(), ecorePackage.getEString(), "sqlName", null, 1, 1, SQLAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSQLAction_HandlingAction(), this.getAction(), null, "handlingAction", null, 0, 1, SQLAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(sqlSelectActionEClass, SQLSelectAction.class, "SQLSelectAction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSQLSelectAction_Tables(), ecorePackage.getEString(), "tables", null, 1, -1, SQLSelectAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSQLSelectAction_Columns(), ecorePackage.getEString(), "columns", null, 0, -1, SQLSelectAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSQLSelectAction_ConditionCode(), ecorePackage.getEString(), "conditionCode", null, 0, 1, SQLSelectAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(sqlInsertActionEClass, SQLInsertAction.class, "SQLInsertAction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSQLInsertAction_Table(), ecorePackage.getEString(), "table", null, 1, 1, SQLInsertAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSQLInsertAction_Columns(), ecorePackage.getEString(), "columns", null, 0, -1, SQLInsertAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSQLInsertAction_ValueCodes(), ecorePackage.getEString(), "valueCodes", null, 0, -1, SQLInsertAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(sqlUpdateActionEClass, SQLUpdateAction.class, "SQLUpdateAction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSQLUpdateAction_Table(), ecorePackage.getEString(), "table", null, 1, 1, SQLUpdateAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSQLUpdateAction_Columns(), ecorePackage.getEString(), "columns", null, 1, -1, SQLUpdateAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSQLUpdateAction_ValueCodes(), ecorePackage.getEString(), "valueCodes", null, 0, -1, SQLUpdateAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSQLUpdateAction_ConditionCode(), ecorePackage.getEString(), "conditionCode", null, 0, 1, SQLUpdateAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(sqlDeleteActionEClass, SQLDeleteAction.class, "SQLDeleteAction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSQLDeleteAction_Table(), ecorePackage.getEString(), "table", null, 1, 1, SQLDeleteAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSQLDeleteAction_ConditionCode(), ecorePackage.getEString(), "conditionCode", null, 0, 1, SQLDeleteAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(sqlExecutionActionEClass, SQLExecutionAction.class, "SQLExecutionAction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSQLExecutionAction_SqlCode(), ecorePackage.getEString(), "sqlCode", null, 1, 1, SQLExecutionAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(sqlResultSetIterationActionEClass, SQLResultSetIterationAction.class, "SQLResultSetIterationAction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSQLResultSetIterationAction_Action(), this.getAction(), null, "action", null, 0, 1, SQLResultSetIterationAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(serverPageActionEClass, ServerPageAction.class, "ServerPageAction", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(serverPageTemplateActionEClass, ServerPageTemplateAction.class, "ServerPageTemplateAction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getServerPageTemplateAction_Text(), ecorePackage.getEString(), "text", null, 0, 1, ServerPageTemplateAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(serverPageExpressionActionEClass, ServerPageExpressionAction.class, "ServerPageExpressionAction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getServerPageExpressionAction_Expression(), ecorePackage.getEString(), "expression", null, 1, 1, ServerPageExpressionAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(serverPageStructuredTemplateActionEClass, ServerPageStructuredTemplateAction.class, "ServerPageStructuredTemplateAction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getServerPageStructuredTemplateAction_Prefix(), ecorePackage.getEString(), "prefix", null, 0, 1, ServerPageStructuredTemplateAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getServerPageStructuredTemplateAction_Action(), this.getAction(), null, "action", null, 0, 1, ServerPageStructuredTemplateAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getServerPageStructuredTemplateAction_Suffix(), ecorePackage.getEString(), "suffix", null, 0, 1, ServerPageStructuredTemplateAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(webApplicationEClass, WebApplication.class, "WebApplication", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getWebApplication_DatabaseUtilClass(), ecorePackage.getEString(), "databaseUtilClass", null, 0, 1, WebApplication.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getWebApplication_Databases(), this.getDatabase(), null, "databases", null, 0, -1, WebApplication.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getWebApplication_Interfaces(), this.getInterface(), null, "interfaces", null, 0, -1, WebApplication.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getWebApplication_PageComponents(), this.getPageComponent(), null, "pageComponents", null, 0, -1, WebApplication.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getWebApplication_ServerComponents(), this.getServerComponent(), null, "serverComponents", null, 0, -1, WebApplication.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getWebApplication_ClassComponents(), this.getClassComponent(), null, "classComponents", null, 0, -1, WebApplication.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(ajaxTypeEEnum, AjaxType.class, "AjaxType");
		addEEnumLiteral(ajaxTypeEEnum, AjaxType.AJAX_LOAD);
		addEEnumLiteral(ajaxTypeEEnum, AjaxType.AJAX_GET);
		addEEnumLiteral(ajaxTypeEEnum, AjaxType.AJAX_POST);

		initEEnum(pageContentTypeEEnum, PageContentType.class, "PageContentType");
		addEEnumLiteral(pageContentTypeEEnum, PageContentType.HTML);
		addEEnumLiteral(pageContentTypeEEnum, PageContentType.XML);
		addEEnumLiteral(pageContentTypeEEnum, PageContentType.JSON);

		initEEnum(dataFormatEEnum, DataFormat.class, "DataFormat");
		addEEnumLiteral(dataFormatEEnum, DataFormat.XML);
		addEEnumLiteral(dataFormatEEnum, DataFormat.JSON);
		addEEnumLiteral(dataFormatEEnum, DataFormat.HTML);
		addEEnumLiteral(dataFormatEEnum, DataFormat.SCRIPT);

		// Create resource
		createResource(eNS_URI);
	}

} //WebAppPackageImpl
