/**
 */
package edu.ustb.sei.mde.webapp.impl;

import edu.ustb.sei.mde.webapp.CallAction;
import edu.ustb.sei.mde.webapp.Operation;
import edu.ustb.sei.mde.webapp.WebAppPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Call Action</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.webapp.impl.CallActionImpl#getInvocationTarget <em>Invocation Target</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.impl.CallActionImpl#getParameterCodes <em>Parameter Codes</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class CallActionImpl extends ActionImpl implements CallAction {
	/**
	 * The cached value of the '{@link #getInvocationTarget() <em>Invocation Target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInvocationTarget()
	 * @generated
	 * @ordered
	 */
	protected Operation invocationTarget;

	/**
	 * The cached value of the '{@link #getParameterCodes() <em>Parameter Codes</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParameterCodes()
	 * @generated
	 * @ordered
	 */
	protected EList<String> parameterCodes;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CallActionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WebAppPackage.Literals.CALL_ACTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Operation getInvocationTarget() {
		if (invocationTarget != null && invocationTarget.eIsProxy()) {
			InternalEObject oldInvocationTarget = (InternalEObject)invocationTarget;
			invocationTarget = (Operation)eResolveProxy(oldInvocationTarget);
			if (invocationTarget != oldInvocationTarget) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, WebAppPackage.CALL_ACTION__INVOCATION_TARGET, oldInvocationTarget, invocationTarget));
			}
		}
		return invocationTarget;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Operation basicGetInvocationTarget() {
		return invocationTarget;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInvocationTarget(Operation newInvocationTarget) {
		Operation oldInvocationTarget = invocationTarget;
		invocationTarget = newInvocationTarget;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WebAppPackage.CALL_ACTION__INVOCATION_TARGET, oldInvocationTarget, invocationTarget));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<String> getParameterCodes() {
		if (parameterCodes == null) {
			parameterCodes = new EDataTypeUniqueEList<String>(String.class, this, WebAppPackage.CALL_ACTION__PARAMETER_CODES);
		}
		return parameterCodes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case WebAppPackage.CALL_ACTION__INVOCATION_TARGET:
				if (resolve) return getInvocationTarget();
				return basicGetInvocationTarget();
			case WebAppPackage.CALL_ACTION__PARAMETER_CODES:
				return getParameterCodes();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case WebAppPackage.CALL_ACTION__INVOCATION_TARGET:
				setInvocationTarget((Operation)newValue);
				return;
			case WebAppPackage.CALL_ACTION__PARAMETER_CODES:
				getParameterCodes().clear();
				getParameterCodes().addAll((Collection<? extends String>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case WebAppPackage.CALL_ACTION__INVOCATION_TARGET:
				setInvocationTarget((Operation)null);
				return;
			case WebAppPackage.CALL_ACTION__PARAMETER_CODES:
				getParameterCodes().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case WebAppPackage.CALL_ACTION__INVOCATION_TARGET:
				return invocationTarget != null;
			case WebAppPackage.CALL_ACTION__PARAMETER_CODES:
				return parameterCodes != null && !parameterCodes.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (parameterCodes: ");
		result.append(parameterCodes);
		result.append(')');
		return result.toString();
	}

} //CallActionImpl
