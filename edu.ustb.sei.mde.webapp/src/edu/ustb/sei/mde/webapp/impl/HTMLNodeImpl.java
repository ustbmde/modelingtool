/**
 */
package edu.ustb.sei.mde.webapp.impl;

import edu.ustb.sei.mde.webapp.EventHandler;
import edu.ustb.sei.mde.webapp.HTMLNode;
import edu.ustb.sei.mde.webapp.WebAppPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>HTML Node</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.webapp.impl.HTMLNodeImpl#getTag <em>Tag</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.impl.HTMLNodeImpl#getId <em>Id</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.impl.HTMLNodeImpl#getClass_ <em>Class</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.impl.HTMLNodeImpl#getExtra <em>Extra</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.impl.HTMLNodeImpl#getInnerText <em>Inner Text</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.impl.HTMLNodeImpl#getChildren <em>Children</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.impl.HTMLNodeImpl#getHandlers <em>Handlers</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.impl.HTMLNodeImpl#getStyles <em>Styles</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class HTMLNodeImpl extends MinimalEObjectImpl.Container implements HTMLNode {
	/**
	 * The default value of the '{@link #getTag() <em>Tag</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTag()
	 * @generated
	 * @ordered
	 */
	protected static final String TAG_EDEFAULT = "div";

	/**
	 * The cached value of the '{@link #getTag() <em>Tag</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTag()
	 * @generated
	 * @ordered
	 */
	protected String tag = TAG_EDEFAULT;

	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected String id = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getClass_() <em>Class</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClass_()
	 * @generated
	 * @ordered
	 */
	protected static final String CLASS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getClass_() <em>Class</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClass_()
	 * @generated
	 * @ordered
	 */
	protected String class_ = CLASS_EDEFAULT;

	/**
	 * The cached value of the '{@link #getExtra() <em>Extra</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExtra()
	 * @generated
	 * @ordered
	 */
	protected EList<String> extra;

	/**
	 * The default value of the '{@link #getInnerText() <em>Inner Text</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInnerText()
	 * @generated
	 * @ordered
	 */
	protected static final String INNER_TEXT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getInnerText() <em>Inner Text</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInnerText()
	 * @generated
	 * @ordered
	 */
	protected String innerText = INNER_TEXT_EDEFAULT;

	/**
	 * The cached value of the '{@link #getChildren() <em>Children</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChildren()
	 * @generated
	 * @ordered
	 */
	protected EList<HTMLNode> children;

	/**
	 * The cached value of the '{@link #getHandlers() <em>Handlers</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHandlers()
	 * @generated
	 * @ordered
	 */
	protected EList<EventHandler> handlers;

	/**
	 * The cached value of the '{@link #getStyles() <em>Styles</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStyles()
	 * @generated
	 * @ordered
	 */
	protected EList<String> styles;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected HTMLNodeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WebAppPackage.Literals.HTML_NODE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getTag() {
		return tag;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTag(String newTag) {
		String oldTag = tag;
		tag = newTag;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WebAppPackage.HTML_NODE__TAG, oldTag, tag));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setId(String newId) {
		String oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WebAppPackage.HTML_NODE__ID, oldId, id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getClass_() {
		return class_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setClass(String newClass) {
		String oldClass = class_;
		class_ = newClass;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WebAppPackage.HTML_NODE__CLASS, oldClass, class_));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<String> getExtra() {
		if (extra == null) {
			extra = new EDataTypeUniqueEList<String>(String.class, this, WebAppPackage.HTML_NODE__EXTRA);
		}
		return extra;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getInnerText() {
		return innerText;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInnerText(String newInnerText) {
		String oldInnerText = innerText;
		innerText = newInnerText;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WebAppPackage.HTML_NODE__INNER_TEXT, oldInnerText, innerText));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<HTMLNode> getChildren() {
		if (children == null) {
			children = new EObjectContainmentEList<HTMLNode>(HTMLNode.class, this, WebAppPackage.HTML_NODE__CHILDREN);
		}
		return children;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EventHandler> getHandlers() {
		if (handlers == null) {
			handlers = new EObjectContainmentWithInverseEList<EventHandler>(EventHandler.class, this, WebAppPackage.HTML_NODE__HANDLERS, WebAppPackage.EVENT_HANDLER__OWNER);
		}
		return handlers;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<String> getStyles() {
		if (styles == null) {
			styles = new EDataTypeUniqueEList<String>(String.class, this, WebAppPackage.HTML_NODE__STYLES);
		}
		return styles;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case WebAppPackage.HTML_NODE__HANDLERS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getHandlers()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case WebAppPackage.HTML_NODE__CHILDREN:
				return ((InternalEList<?>)getChildren()).basicRemove(otherEnd, msgs);
			case WebAppPackage.HTML_NODE__HANDLERS:
				return ((InternalEList<?>)getHandlers()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case WebAppPackage.HTML_NODE__TAG:
				return getTag();
			case WebAppPackage.HTML_NODE__ID:
				return getId();
			case WebAppPackage.HTML_NODE__CLASS:
				return getClass_();
			case WebAppPackage.HTML_NODE__EXTRA:
				return getExtra();
			case WebAppPackage.HTML_NODE__INNER_TEXT:
				return getInnerText();
			case WebAppPackage.HTML_NODE__CHILDREN:
				return getChildren();
			case WebAppPackage.HTML_NODE__HANDLERS:
				return getHandlers();
			case WebAppPackage.HTML_NODE__STYLES:
				return getStyles();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case WebAppPackage.HTML_NODE__TAG:
				setTag((String)newValue);
				return;
			case WebAppPackage.HTML_NODE__ID:
				setId((String)newValue);
				return;
			case WebAppPackage.HTML_NODE__CLASS:
				setClass((String)newValue);
				return;
			case WebAppPackage.HTML_NODE__EXTRA:
				getExtra().clear();
				getExtra().addAll((Collection<? extends String>)newValue);
				return;
			case WebAppPackage.HTML_NODE__INNER_TEXT:
				setInnerText((String)newValue);
				return;
			case WebAppPackage.HTML_NODE__CHILDREN:
				getChildren().clear();
				getChildren().addAll((Collection<? extends HTMLNode>)newValue);
				return;
			case WebAppPackage.HTML_NODE__HANDLERS:
				getHandlers().clear();
				getHandlers().addAll((Collection<? extends EventHandler>)newValue);
				return;
			case WebAppPackage.HTML_NODE__STYLES:
				getStyles().clear();
				getStyles().addAll((Collection<? extends String>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case WebAppPackage.HTML_NODE__TAG:
				setTag(TAG_EDEFAULT);
				return;
			case WebAppPackage.HTML_NODE__ID:
				setId(ID_EDEFAULT);
				return;
			case WebAppPackage.HTML_NODE__CLASS:
				setClass(CLASS_EDEFAULT);
				return;
			case WebAppPackage.HTML_NODE__EXTRA:
				getExtra().clear();
				return;
			case WebAppPackage.HTML_NODE__INNER_TEXT:
				setInnerText(INNER_TEXT_EDEFAULT);
				return;
			case WebAppPackage.HTML_NODE__CHILDREN:
				getChildren().clear();
				return;
			case WebAppPackage.HTML_NODE__HANDLERS:
				getHandlers().clear();
				return;
			case WebAppPackage.HTML_NODE__STYLES:
				getStyles().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case WebAppPackage.HTML_NODE__TAG:
				return TAG_EDEFAULT == null ? tag != null : !TAG_EDEFAULT.equals(tag);
			case WebAppPackage.HTML_NODE__ID:
				return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
			case WebAppPackage.HTML_NODE__CLASS:
				return CLASS_EDEFAULT == null ? class_ != null : !CLASS_EDEFAULT.equals(class_);
			case WebAppPackage.HTML_NODE__EXTRA:
				return extra != null && !extra.isEmpty();
			case WebAppPackage.HTML_NODE__INNER_TEXT:
				return INNER_TEXT_EDEFAULT == null ? innerText != null : !INNER_TEXT_EDEFAULT.equals(innerText);
			case WebAppPackage.HTML_NODE__CHILDREN:
				return children != null && !children.isEmpty();
			case WebAppPackage.HTML_NODE__HANDLERS:
				return handlers != null && !handlers.isEmpty();
			case WebAppPackage.HTML_NODE__STYLES:
				return styles != null && !styles.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (tag: ");
		result.append(tag);
		result.append(", id: ");
		result.append(id);
		result.append(", class: ");
		result.append(class_);
		result.append(", extra: ");
		result.append(extra);
		result.append(", innerText: ");
		result.append(innerText);
		result.append(", styles: ");
		result.append(styles);
		result.append(')');
		return result.toString();
	}

} //HTMLNodeImpl
