/**
 */
package edu.ustb.sei.mde.webapp.impl;

import edu.ustb.sei.mde.webapp.Database;
import edu.ustb.sei.mde.webapp.WebAppPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Database</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.webapp.impl.DatabaseImpl#getDriverName <em>Driver Name</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.impl.DatabaseImpl#getUrl <em>Url</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.impl.DatabaseImpl#getUsername <em>Username</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.impl.DatabaseImpl#getPassword <em>Password</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.impl.DatabaseImpl#getInitialSize <em>Initial Size</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.impl.DatabaseImpl#getMinIdle <em>Min Idle</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.impl.DatabaseImpl#getMaxIdle <em>Max Idle</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.impl.DatabaseImpl#getMaxActive <em>Max Active</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.impl.DatabaseImpl#getMaxWait <em>Max Wait</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.impl.DatabaseImpl#getDatamodel <em>Datamodel</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class DatabaseImpl extends NamedElementImpl implements Database {
	/**
	 * The default value of the '{@link #getDriverName() <em>Driver Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDriverName()
	 * @generated
	 * @ordered
	 */
	protected static final String DRIVER_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDriverName() <em>Driver Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDriverName()
	 * @generated
	 * @ordered
	 */
	protected String driverName = DRIVER_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getUrl() <em>Url</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUrl()
	 * @generated
	 * @ordered
	 */
	protected static final String URL_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getUrl() <em>Url</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUrl()
	 * @generated
	 * @ordered
	 */
	protected String url = URL_EDEFAULT;

	/**
	 * The default value of the '{@link #getUsername() <em>Username</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUsername()
	 * @generated
	 * @ordered
	 */
	protected static final String USERNAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getUsername() <em>Username</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUsername()
	 * @generated
	 * @ordered
	 */
	protected String username = USERNAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getPassword() <em>Password</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPassword()
	 * @generated
	 * @ordered
	 */
	protected static final String PASSWORD_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getPassword() <em>Password</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPassword()
	 * @generated
	 * @ordered
	 */
	protected String password = PASSWORD_EDEFAULT;

	/**
	 * The default value of the '{@link #getInitialSize() <em>Initial Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInitialSize()
	 * @generated
	 * @ordered
	 */
	protected static final int INITIAL_SIZE_EDEFAULT = 50;

	/**
	 * The cached value of the '{@link #getInitialSize() <em>Initial Size</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInitialSize()
	 * @generated
	 * @ordered
	 */
	protected int initialSize = INITIAL_SIZE_EDEFAULT;

	/**
	 * The default value of the '{@link #getMinIdle() <em>Min Idle</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMinIdle()
	 * @generated
	 * @ordered
	 */
	protected static final int MIN_IDLE_EDEFAULT = 10;

	/**
	 * The cached value of the '{@link #getMinIdle() <em>Min Idle</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMinIdle()
	 * @generated
	 * @ordered
	 */
	protected int minIdle = MIN_IDLE_EDEFAULT;

	/**
	 * The default value of the '{@link #getMaxIdle() <em>Max Idle</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaxIdle()
	 * @generated
	 * @ordered
	 */
	protected static final int MAX_IDLE_EDEFAULT = 40;

	/**
	 * The cached value of the '{@link #getMaxIdle() <em>Max Idle</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaxIdle()
	 * @generated
	 * @ordered
	 */
	protected int maxIdle = MAX_IDLE_EDEFAULT;

	/**
	 * The default value of the '{@link #getMaxActive() <em>Max Active</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaxActive()
	 * @generated
	 * @ordered
	 */
	protected static final int MAX_ACTIVE_EDEFAULT = 100;

	/**
	 * The cached value of the '{@link #getMaxActive() <em>Max Active</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaxActive()
	 * @generated
	 * @ordered
	 */
	protected int maxActive = MAX_ACTIVE_EDEFAULT;

	/**
	 * The default value of the '{@link #getMaxWait() <em>Max Wait</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaxWait()
	 * @generated
	 * @ordered
	 */
	protected static final long MAX_WAIT_EDEFAULT = 10000L;

	/**
	 * The cached value of the '{@link #getMaxWait() <em>Max Wait</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaxWait()
	 * @generated
	 * @ordered
	 */
	protected long maxWait = MAX_WAIT_EDEFAULT;

	/**
	 * The cached value of the '{@link #getDatamodel() <em>Datamodel</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDatamodel()
	 * @generated
	 * @ordered
	 */
	protected EObject datamodel;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DatabaseImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WebAppPackage.Literals.DATABASE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDriverName() {
		return driverName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDriverName(String newDriverName) {
		String oldDriverName = driverName;
		driverName = newDriverName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WebAppPackage.DATABASE__DRIVER_NAME, oldDriverName, driverName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUrl(String newUrl) {
		String oldUrl = url;
		url = newUrl;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WebAppPackage.DATABASE__URL, oldUrl, url));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUsername(String newUsername) {
		String oldUsername = username;
		username = newUsername;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WebAppPackage.DATABASE__USERNAME, oldUsername, username));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPassword(String newPassword) {
		String oldPassword = password;
		password = newPassword;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WebAppPackage.DATABASE__PASSWORD, oldPassword, password));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getInitialSize() {
		return initialSize;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInitialSize(int newInitialSize) {
		int oldInitialSize = initialSize;
		initialSize = newInitialSize;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WebAppPackage.DATABASE__INITIAL_SIZE, oldInitialSize, initialSize));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getMinIdle() {
		return minIdle;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMinIdle(int newMinIdle) {
		int oldMinIdle = minIdle;
		minIdle = newMinIdle;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WebAppPackage.DATABASE__MIN_IDLE, oldMinIdle, minIdle));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getMaxIdle() {
		return maxIdle;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMaxIdle(int newMaxIdle) {
		int oldMaxIdle = maxIdle;
		maxIdle = newMaxIdle;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WebAppPackage.DATABASE__MAX_IDLE, oldMaxIdle, maxIdle));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getMaxActive() {
		return maxActive;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMaxActive(int newMaxActive) {
		int oldMaxActive = maxActive;
		maxActive = newMaxActive;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WebAppPackage.DATABASE__MAX_ACTIVE, oldMaxActive, maxActive));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public long getMaxWait() {
		return maxWait;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMaxWait(long newMaxWait) {
		long oldMaxWait = maxWait;
		maxWait = newMaxWait;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WebAppPackage.DATABASE__MAX_WAIT, oldMaxWait, maxWait));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EObject getDatamodel() {
		if (datamodel != null && datamodel.eIsProxy()) {
			InternalEObject oldDatamodel = (InternalEObject)datamodel;
			datamodel = eResolveProxy(oldDatamodel);
			if (datamodel != oldDatamodel) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, WebAppPackage.DATABASE__DATAMODEL, oldDatamodel, datamodel));
			}
		}
		return datamodel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EObject basicGetDatamodel() {
		return datamodel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDatamodel(EObject newDatamodel) {
		EObject oldDatamodel = datamodel;
		datamodel = newDatamodel;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WebAppPackage.DATABASE__DATAMODEL, oldDatamodel, datamodel));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case WebAppPackage.DATABASE__DRIVER_NAME:
				return getDriverName();
			case WebAppPackage.DATABASE__URL:
				return getUrl();
			case WebAppPackage.DATABASE__USERNAME:
				return getUsername();
			case WebAppPackage.DATABASE__PASSWORD:
				return getPassword();
			case WebAppPackage.DATABASE__INITIAL_SIZE:
				return getInitialSize();
			case WebAppPackage.DATABASE__MIN_IDLE:
				return getMinIdle();
			case WebAppPackage.DATABASE__MAX_IDLE:
				return getMaxIdle();
			case WebAppPackage.DATABASE__MAX_ACTIVE:
				return getMaxActive();
			case WebAppPackage.DATABASE__MAX_WAIT:
				return getMaxWait();
			case WebAppPackage.DATABASE__DATAMODEL:
				if (resolve) return getDatamodel();
				return basicGetDatamodel();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case WebAppPackage.DATABASE__DRIVER_NAME:
				setDriverName((String)newValue);
				return;
			case WebAppPackage.DATABASE__URL:
				setUrl((String)newValue);
				return;
			case WebAppPackage.DATABASE__USERNAME:
				setUsername((String)newValue);
				return;
			case WebAppPackage.DATABASE__PASSWORD:
				setPassword((String)newValue);
				return;
			case WebAppPackage.DATABASE__INITIAL_SIZE:
				setInitialSize((Integer)newValue);
				return;
			case WebAppPackage.DATABASE__MIN_IDLE:
				setMinIdle((Integer)newValue);
				return;
			case WebAppPackage.DATABASE__MAX_IDLE:
				setMaxIdle((Integer)newValue);
				return;
			case WebAppPackage.DATABASE__MAX_ACTIVE:
				setMaxActive((Integer)newValue);
				return;
			case WebAppPackage.DATABASE__MAX_WAIT:
				setMaxWait((Long)newValue);
				return;
			case WebAppPackage.DATABASE__DATAMODEL:
				setDatamodel((EObject)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case WebAppPackage.DATABASE__DRIVER_NAME:
				setDriverName(DRIVER_NAME_EDEFAULT);
				return;
			case WebAppPackage.DATABASE__URL:
				setUrl(URL_EDEFAULT);
				return;
			case WebAppPackage.DATABASE__USERNAME:
				setUsername(USERNAME_EDEFAULT);
				return;
			case WebAppPackage.DATABASE__PASSWORD:
				setPassword(PASSWORD_EDEFAULT);
				return;
			case WebAppPackage.DATABASE__INITIAL_SIZE:
				setInitialSize(INITIAL_SIZE_EDEFAULT);
				return;
			case WebAppPackage.DATABASE__MIN_IDLE:
				setMinIdle(MIN_IDLE_EDEFAULT);
				return;
			case WebAppPackage.DATABASE__MAX_IDLE:
				setMaxIdle(MAX_IDLE_EDEFAULT);
				return;
			case WebAppPackage.DATABASE__MAX_ACTIVE:
				setMaxActive(MAX_ACTIVE_EDEFAULT);
				return;
			case WebAppPackage.DATABASE__MAX_WAIT:
				setMaxWait(MAX_WAIT_EDEFAULT);
				return;
			case WebAppPackage.DATABASE__DATAMODEL:
				setDatamodel((EObject)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case WebAppPackage.DATABASE__DRIVER_NAME:
				return DRIVER_NAME_EDEFAULT == null ? driverName != null : !DRIVER_NAME_EDEFAULT.equals(driverName);
			case WebAppPackage.DATABASE__URL:
				return URL_EDEFAULT == null ? url != null : !URL_EDEFAULT.equals(url);
			case WebAppPackage.DATABASE__USERNAME:
				return USERNAME_EDEFAULT == null ? username != null : !USERNAME_EDEFAULT.equals(username);
			case WebAppPackage.DATABASE__PASSWORD:
				return PASSWORD_EDEFAULT == null ? password != null : !PASSWORD_EDEFAULT.equals(password);
			case WebAppPackage.DATABASE__INITIAL_SIZE:
				return initialSize != INITIAL_SIZE_EDEFAULT;
			case WebAppPackage.DATABASE__MIN_IDLE:
				return minIdle != MIN_IDLE_EDEFAULT;
			case WebAppPackage.DATABASE__MAX_IDLE:
				return maxIdle != MAX_IDLE_EDEFAULT;
			case WebAppPackage.DATABASE__MAX_ACTIVE:
				return maxActive != MAX_ACTIVE_EDEFAULT;
			case WebAppPackage.DATABASE__MAX_WAIT:
				return maxWait != MAX_WAIT_EDEFAULT;
			case WebAppPackage.DATABASE__DATAMODEL:
				return datamodel != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (driverName: ");
		result.append(driverName);
		result.append(", url: ");
		result.append(url);
		result.append(", username: ");
		result.append(username);
		result.append(", password: ");
		result.append(password);
		result.append(", initialSize: ");
		result.append(initialSize);
		result.append(", minIdle: ");
		result.append(minIdle);
		result.append(", maxIdle: ");
		result.append(maxIdle);
		result.append(", maxActive: ");
		result.append(maxActive);
		result.append(", maxWait: ");
		result.append(maxWait);
		result.append(')');
		return result.toString();
	}

} //DatabaseImpl
