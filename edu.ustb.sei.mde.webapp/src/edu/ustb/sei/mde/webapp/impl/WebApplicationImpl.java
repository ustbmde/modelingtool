/**
 */
package edu.ustb.sei.mde.webapp.impl;

import edu.ustb.sei.mde.webapp.ClassComponent;
import edu.ustb.sei.mde.webapp.Database;
import edu.ustb.sei.mde.webapp.Interface;
import edu.ustb.sei.mde.webapp.PageComponent;
import edu.ustb.sei.mde.webapp.ServerComponent;
import edu.ustb.sei.mde.webapp.WebAppPackage;
import edu.ustb.sei.mde.webapp.WebApplication;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Web Application</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.webapp.impl.WebApplicationImpl#getDatabaseUtilClass <em>Database Util Class</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.impl.WebApplicationImpl#getDatabases <em>Databases</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.impl.WebApplicationImpl#getInterfaces <em>Interfaces</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.impl.WebApplicationImpl#getPageComponents <em>Page Components</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.impl.WebApplicationImpl#getServerComponents <em>Server Components</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.impl.WebApplicationImpl#getClassComponents <em>Class Components</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class WebApplicationImpl extends NamedElementImpl implements WebApplication {
	/**
	 * The default value of the '{@link #getDatabaseUtilClass() <em>Database Util Class</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDatabaseUtilClass()
	 * @generated
	 * @ordered
	 */
	protected static final String DATABASE_UTIL_CLASS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDatabaseUtilClass() <em>Database Util Class</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDatabaseUtilClass()
	 * @generated
	 * @ordered
	 */
	protected String databaseUtilClass = DATABASE_UTIL_CLASS_EDEFAULT;

	/**
	 * The cached value of the '{@link #getDatabases() <em>Databases</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDatabases()
	 * @generated
	 * @ordered
	 */
	protected EList<Database> databases;

	/**
	 * The cached value of the '{@link #getInterfaces() <em>Interfaces</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInterfaces()
	 * @generated
	 * @ordered
	 */
	protected EList<Interface> interfaces;

	/**
	 * The cached value of the '{@link #getPageComponents() <em>Page Components</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPageComponents()
	 * @generated
	 * @ordered
	 */
	protected EList<PageComponent> pageComponents;

	/**
	 * The cached value of the '{@link #getServerComponents() <em>Server Components</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getServerComponents()
	 * @generated
	 * @ordered
	 */
	protected EList<ServerComponent> serverComponents;

	/**
	 * The cached value of the '{@link #getClassComponents() <em>Class Components</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClassComponents()
	 * @generated
	 * @ordered
	 */
	protected EList<ClassComponent> classComponents;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected WebApplicationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WebAppPackage.Literals.WEB_APPLICATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDatabaseUtilClass() {
		return databaseUtilClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDatabaseUtilClass(String newDatabaseUtilClass) {
		String oldDatabaseUtilClass = databaseUtilClass;
		databaseUtilClass = newDatabaseUtilClass;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WebAppPackage.WEB_APPLICATION__DATABASE_UTIL_CLASS, oldDatabaseUtilClass, databaseUtilClass));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Database> getDatabases() {
		if (databases == null) {
			databases = new EObjectContainmentEList<Database>(Database.class, this, WebAppPackage.WEB_APPLICATION__DATABASES);
		}
		return databases;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Interface> getInterfaces() {
		if (interfaces == null) {
			interfaces = new EObjectContainmentEList<Interface>(Interface.class, this, WebAppPackage.WEB_APPLICATION__INTERFACES);
		}
		return interfaces;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PageComponent> getPageComponents() {
		if (pageComponents == null) {
			pageComponents = new EObjectContainmentEList<PageComponent>(PageComponent.class, this, WebAppPackage.WEB_APPLICATION__PAGE_COMPONENTS);
		}
		return pageComponents;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ServerComponent> getServerComponents() {
		if (serverComponents == null) {
			serverComponents = new EObjectContainmentEList<ServerComponent>(ServerComponent.class, this, WebAppPackage.WEB_APPLICATION__SERVER_COMPONENTS);
		}
		return serverComponents;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ClassComponent> getClassComponents() {
		if (classComponents == null) {
			classComponents = new EObjectContainmentEList<ClassComponent>(ClassComponent.class, this, WebAppPackage.WEB_APPLICATION__CLASS_COMPONENTS);
		}
		return classComponents;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case WebAppPackage.WEB_APPLICATION__DATABASES:
				return ((InternalEList<?>)getDatabases()).basicRemove(otherEnd, msgs);
			case WebAppPackage.WEB_APPLICATION__INTERFACES:
				return ((InternalEList<?>)getInterfaces()).basicRemove(otherEnd, msgs);
			case WebAppPackage.WEB_APPLICATION__PAGE_COMPONENTS:
				return ((InternalEList<?>)getPageComponents()).basicRemove(otherEnd, msgs);
			case WebAppPackage.WEB_APPLICATION__SERVER_COMPONENTS:
				return ((InternalEList<?>)getServerComponents()).basicRemove(otherEnd, msgs);
			case WebAppPackage.WEB_APPLICATION__CLASS_COMPONENTS:
				return ((InternalEList<?>)getClassComponents()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case WebAppPackage.WEB_APPLICATION__DATABASE_UTIL_CLASS:
				return getDatabaseUtilClass();
			case WebAppPackage.WEB_APPLICATION__DATABASES:
				return getDatabases();
			case WebAppPackage.WEB_APPLICATION__INTERFACES:
				return getInterfaces();
			case WebAppPackage.WEB_APPLICATION__PAGE_COMPONENTS:
				return getPageComponents();
			case WebAppPackage.WEB_APPLICATION__SERVER_COMPONENTS:
				return getServerComponents();
			case WebAppPackage.WEB_APPLICATION__CLASS_COMPONENTS:
				return getClassComponents();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case WebAppPackage.WEB_APPLICATION__DATABASE_UTIL_CLASS:
				setDatabaseUtilClass((String)newValue);
				return;
			case WebAppPackage.WEB_APPLICATION__DATABASES:
				getDatabases().clear();
				getDatabases().addAll((Collection<? extends Database>)newValue);
				return;
			case WebAppPackage.WEB_APPLICATION__INTERFACES:
				getInterfaces().clear();
				getInterfaces().addAll((Collection<? extends Interface>)newValue);
				return;
			case WebAppPackage.WEB_APPLICATION__PAGE_COMPONENTS:
				getPageComponents().clear();
				getPageComponents().addAll((Collection<? extends PageComponent>)newValue);
				return;
			case WebAppPackage.WEB_APPLICATION__SERVER_COMPONENTS:
				getServerComponents().clear();
				getServerComponents().addAll((Collection<? extends ServerComponent>)newValue);
				return;
			case WebAppPackage.WEB_APPLICATION__CLASS_COMPONENTS:
				getClassComponents().clear();
				getClassComponents().addAll((Collection<? extends ClassComponent>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case WebAppPackage.WEB_APPLICATION__DATABASE_UTIL_CLASS:
				setDatabaseUtilClass(DATABASE_UTIL_CLASS_EDEFAULT);
				return;
			case WebAppPackage.WEB_APPLICATION__DATABASES:
				getDatabases().clear();
				return;
			case WebAppPackage.WEB_APPLICATION__INTERFACES:
				getInterfaces().clear();
				return;
			case WebAppPackage.WEB_APPLICATION__PAGE_COMPONENTS:
				getPageComponents().clear();
				return;
			case WebAppPackage.WEB_APPLICATION__SERVER_COMPONENTS:
				getServerComponents().clear();
				return;
			case WebAppPackage.WEB_APPLICATION__CLASS_COMPONENTS:
				getClassComponents().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case WebAppPackage.WEB_APPLICATION__DATABASE_UTIL_CLASS:
				return DATABASE_UTIL_CLASS_EDEFAULT == null ? databaseUtilClass != null : !DATABASE_UTIL_CLASS_EDEFAULT.equals(databaseUtilClass);
			case WebAppPackage.WEB_APPLICATION__DATABASES:
				return databases != null && !databases.isEmpty();
			case WebAppPackage.WEB_APPLICATION__INTERFACES:
				return interfaces != null && !interfaces.isEmpty();
			case WebAppPackage.WEB_APPLICATION__PAGE_COMPONENTS:
				return pageComponents != null && !pageComponents.isEmpty();
			case WebAppPackage.WEB_APPLICATION__SERVER_COMPONENTS:
				return serverComponents != null && !serverComponents.isEmpty();
			case WebAppPackage.WEB_APPLICATION__CLASS_COMPONENTS:
				return classComponents != null && !classComponents.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (databaseUtilClass: ");
		result.append(databaseUtilClass);
		result.append(')');
		return result.toString();
	}

} //WebApplicationImpl
