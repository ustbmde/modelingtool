/**
 */
package edu.ustb.sei.mde.webapp.impl;

import edu.ustb.sei.mde.webapp.SQLSelectAction;
import edu.ustb.sei.mde.webapp.WebAppPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>SQL Select Action</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.webapp.impl.SQLSelectActionImpl#getTables <em>Tables</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.impl.SQLSelectActionImpl#getColumns <em>Columns</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.impl.SQLSelectActionImpl#getConditionCode <em>Condition Code</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class SQLSelectActionImpl extends SQLActionImpl implements SQLSelectAction {
	/**
	 * The cached value of the '{@link #getTables() <em>Tables</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTables()
	 * @generated
	 * @ordered
	 */
	protected EList<String> tables;

	/**
	 * The cached value of the '{@link #getColumns() <em>Columns</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getColumns()
	 * @generated
	 * @ordered
	 */
	protected EList<String> columns;

	/**
	 * The default value of the '{@link #getConditionCode() <em>Condition Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConditionCode()
	 * @generated
	 * @ordered
	 */
	protected static final String CONDITION_CODE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getConditionCode() <em>Condition Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConditionCode()
	 * @generated
	 * @ordered
	 */
	protected String conditionCode = CONDITION_CODE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SQLSelectActionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WebAppPackage.Literals.SQL_SELECT_ACTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<String> getTables() {
		if (tables == null) {
			tables = new EDataTypeUniqueEList<String>(String.class, this, WebAppPackage.SQL_SELECT_ACTION__TABLES);
		}
		return tables;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<String> getColumns() {
		if (columns == null) {
			columns = new EDataTypeUniqueEList<String>(String.class, this, WebAppPackage.SQL_SELECT_ACTION__COLUMNS);
		}
		return columns;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getConditionCode() {
		return conditionCode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConditionCode(String newConditionCode) {
		String oldConditionCode = conditionCode;
		conditionCode = newConditionCode;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WebAppPackage.SQL_SELECT_ACTION__CONDITION_CODE, oldConditionCode, conditionCode));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case WebAppPackage.SQL_SELECT_ACTION__TABLES:
				return getTables();
			case WebAppPackage.SQL_SELECT_ACTION__COLUMNS:
				return getColumns();
			case WebAppPackage.SQL_SELECT_ACTION__CONDITION_CODE:
				return getConditionCode();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case WebAppPackage.SQL_SELECT_ACTION__TABLES:
				getTables().clear();
				getTables().addAll((Collection<? extends String>)newValue);
				return;
			case WebAppPackage.SQL_SELECT_ACTION__COLUMNS:
				getColumns().clear();
				getColumns().addAll((Collection<? extends String>)newValue);
				return;
			case WebAppPackage.SQL_SELECT_ACTION__CONDITION_CODE:
				setConditionCode((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case WebAppPackage.SQL_SELECT_ACTION__TABLES:
				getTables().clear();
				return;
			case WebAppPackage.SQL_SELECT_ACTION__COLUMNS:
				getColumns().clear();
				return;
			case WebAppPackage.SQL_SELECT_ACTION__CONDITION_CODE:
				setConditionCode(CONDITION_CODE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case WebAppPackage.SQL_SELECT_ACTION__TABLES:
				return tables != null && !tables.isEmpty();
			case WebAppPackage.SQL_SELECT_ACTION__COLUMNS:
				return columns != null && !columns.isEmpty();
			case WebAppPackage.SQL_SELECT_ACTION__CONDITION_CODE:
				return CONDITION_CODE_EDEFAULT == null ? conditionCode != null : !CONDITION_CODE_EDEFAULT.equals(conditionCode);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (tables: ");
		result.append(tables);
		result.append(", columns: ");
		result.append(columns);
		result.append(", conditionCode: ");
		result.append(conditionCode);
		result.append(')');
		return result.toString();
	}

} //SQLSelectActionImpl
