/**
 */
package edu.ustb.sei.mde.webapp.impl;

import edu.ustb.sei.mde.webapp.JQueryEventAction;
import edu.ustb.sei.mde.webapp.OperationRealization;
import edu.ustb.sei.mde.webapp.WebAppPackage;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>JQuery Event Action</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.webapp.impl.JQueryEventActionImpl#getSelectorCode <em>Selector Code</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.impl.JQueryEventActionImpl#getEvent <em>Event</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.impl.JQueryEventActionImpl#getHandler <em>Handler</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class JQueryEventActionImpl extends JavascriptActionImpl implements JQueryEventAction {
	/**
	 * The default value of the '{@link #getSelectorCode() <em>Selector Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSelectorCode()
	 * @generated
	 * @ordered
	 */
	protected static final String SELECTOR_CODE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSelectorCode() <em>Selector Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSelectorCode()
	 * @generated
	 * @ordered
	 */
	protected String selectorCode = SELECTOR_CODE_EDEFAULT;

	/**
	 * The default value of the '{@link #getEvent() <em>Event</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEvent()
	 * @generated
	 * @ordered
	 */
	protected static final String EVENT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getEvent() <em>Event</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEvent()
	 * @generated
	 * @ordered
	 */
	protected String event = EVENT_EDEFAULT;

	/**
	 * The cached value of the '{@link #getHandler() <em>Handler</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHandler()
	 * @generated
	 * @ordered
	 */
	protected OperationRealization handler;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected JQueryEventActionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WebAppPackage.Literals.JQUERY_EVENT_ACTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSelectorCode() {
		return selectorCode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSelectorCode(String newSelectorCode) {
		String oldSelectorCode = selectorCode;
		selectorCode = newSelectorCode;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WebAppPackage.JQUERY_EVENT_ACTION__SELECTOR_CODE, oldSelectorCode, selectorCode));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getEvent() {
		return event;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEvent(String newEvent) {
		String oldEvent = event;
		event = newEvent;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WebAppPackage.JQUERY_EVENT_ACTION__EVENT, oldEvent, event));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationRealization getHandler() {
		return handler;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetHandler(OperationRealization newHandler, NotificationChain msgs) {
		OperationRealization oldHandler = handler;
		handler = newHandler;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, WebAppPackage.JQUERY_EVENT_ACTION__HANDLER, oldHandler, newHandler);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHandler(OperationRealization newHandler) {
		if (newHandler != handler) {
			NotificationChain msgs = null;
			if (handler != null)
				msgs = ((InternalEObject)handler).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - WebAppPackage.JQUERY_EVENT_ACTION__HANDLER, null, msgs);
			if (newHandler != null)
				msgs = ((InternalEObject)newHandler).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - WebAppPackage.JQUERY_EVENT_ACTION__HANDLER, null, msgs);
			msgs = basicSetHandler(newHandler, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WebAppPackage.JQUERY_EVENT_ACTION__HANDLER, newHandler, newHandler));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case WebAppPackage.JQUERY_EVENT_ACTION__HANDLER:
				return basicSetHandler(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case WebAppPackage.JQUERY_EVENT_ACTION__SELECTOR_CODE:
				return getSelectorCode();
			case WebAppPackage.JQUERY_EVENT_ACTION__EVENT:
				return getEvent();
			case WebAppPackage.JQUERY_EVENT_ACTION__HANDLER:
				return getHandler();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case WebAppPackage.JQUERY_EVENT_ACTION__SELECTOR_CODE:
				setSelectorCode((String)newValue);
				return;
			case WebAppPackage.JQUERY_EVENT_ACTION__EVENT:
				setEvent((String)newValue);
				return;
			case WebAppPackage.JQUERY_EVENT_ACTION__HANDLER:
				setHandler((OperationRealization)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case WebAppPackage.JQUERY_EVENT_ACTION__SELECTOR_CODE:
				setSelectorCode(SELECTOR_CODE_EDEFAULT);
				return;
			case WebAppPackage.JQUERY_EVENT_ACTION__EVENT:
				setEvent(EVENT_EDEFAULT);
				return;
			case WebAppPackage.JQUERY_EVENT_ACTION__HANDLER:
				setHandler((OperationRealization)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case WebAppPackage.JQUERY_EVENT_ACTION__SELECTOR_CODE:
				return SELECTOR_CODE_EDEFAULT == null ? selectorCode != null : !SELECTOR_CODE_EDEFAULT.equals(selectorCode);
			case WebAppPackage.JQUERY_EVENT_ACTION__EVENT:
				return EVENT_EDEFAULT == null ? event != null : !EVENT_EDEFAULT.equals(event);
			case WebAppPackage.JQUERY_EVENT_ACTION__HANDLER:
				return handler != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (selectorCode: ");
		result.append(selectorCode);
		result.append(", event: ");
		result.append(event);
		result.append(')');
		return result.toString();
	}

} //JQueryEventActionImpl
