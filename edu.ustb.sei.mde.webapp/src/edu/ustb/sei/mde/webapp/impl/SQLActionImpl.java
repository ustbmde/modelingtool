/**
 */
package edu.ustb.sei.mde.webapp.impl;

import edu.ustb.sei.mde.webapp.Action;
import edu.ustb.sei.mde.webapp.Database;
import edu.ustb.sei.mde.webapp.SQLAction;
import edu.ustb.sei.mde.webapp.WebAppPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>SQL Action</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.webapp.impl.SQLActionImpl#getDatabase <em>Database</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.impl.SQLActionImpl#getSqlName <em>Sql Name</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.impl.SQLActionImpl#getHandlingAction <em>Handling Action</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public abstract class SQLActionImpl extends ActionImpl implements SQLAction {
	/**
	 * The cached value of the '{@link #getDatabase() <em>Database</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDatabase()
	 * @generated
	 * @ordered
	 */
	protected Database database;

	/**
	 * The default value of the '{@link #getSqlName() <em>Sql Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSqlName()
	 * @generated
	 * @ordered
	 */
	protected static final String SQL_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSqlName() <em>Sql Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSqlName()
	 * @generated
	 * @ordered
	 */
	protected String sqlName = SQL_NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getHandlingAction() <em>Handling Action</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHandlingAction()
	 * @generated
	 * @ordered
	 */
	protected Action handlingAction;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SQLActionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WebAppPackage.Literals.SQL_ACTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Database getDatabase() {
		if (database != null && database.eIsProxy()) {
			InternalEObject oldDatabase = (InternalEObject)database;
			database = (Database)eResolveProxy(oldDatabase);
			if (database != oldDatabase) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, WebAppPackage.SQL_ACTION__DATABASE, oldDatabase, database));
			}
		}
		return database;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Database basicGetDatabase() {
		return database;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDatabase(Database newDatabase) {
		Database oldDatabase = database;
		database = newDatabase;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WebAppPackage.SQL_ACTION__DATABASE, oldDatabase, database));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSqlName() {
		return sqlName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSqlName(String newSqlName) {
		String oldSqlName = sqlName;
		sqlName = newSqlName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WebAppPackage.SQL_ACTION__SQL_NAME, oldSqlName, sqlName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Action getHandlingAction() {
		return handlingAction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetHandlingAction(Action newHandlingAction, NotificationChain msgs) {
		Action oldHandlingAction = handlingAction;
		handlingAction = newHandlingAction;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, WebAppPackage.SQL_ACTION__HANDLING_ACTION, oldHandlingAction, newHandlingAction);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHandlingAction(Action newHandlingAction) {
		if (newHandlingAction != handlingAction) {
			NotificationChain msgs = null;
			if (handlingAction != null)
				msgs = ((InternalEObject)handlingAction).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - WebAppPackage.SQL_ACTION__HANDLING_ACTION, null, msgs);
			if (newHandlingAction != null)
				msgs = ((InternalEObject)newHandlingAction).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - WebAppPackage.SQL_ACTION__HANDLING_ACTION, null, msgs);
			msgs = basicSetHandlingAction(newHandlingAction, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WebAppPackage.SQL_ACTION__HANDLING_ACTION, newHandlingAction, newHandlingAction));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case WebAppPackage.SQL_ACTION__HANDLING_ACTION:
				return basicSetHandlingAction(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case WebAppPackage.SQL_ACTION__DATABASE:
				if (resolve) return getDatabase();
				return basicGetDatabase();
			case WebAppPackage.SQL_ACTION__SQL_NAME:
				return getSqlName();
			case WebAppPackage.SQL_ACTION__HANDLING_ACTION:
				return getHandlingAction();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case WebAppPackage.SQL_ACTION__DATABASE:
				setDatabase((Database)newValue);
				return;
			case WebAppPackage.SQL_ACTION__SQL_NAME:
				setSqlName((String)newValue);
				return;
			case WebAppPackage.SQL_ACTION__HANDLING_ACTION:
				setHandlingAction((Action)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case WebAppPackage.SQL_ACTION__DATABASE:
				setDatabase((Database)null);
				return;
			case WebAppPackage.SQL_ACTION__SQL_NAME:
				setSqlName(SQL_NAME_EDEFAULT);
				return;
			case WebAppPackage.SQL_ACTION__HANDLING_ACTION:
				setHandlingAction((Action)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case WebAppPackage.SQL_ACTION__DATABASE:
				return database != null;
			case WebAppPackage.SQL_ACTION__SQL_NAME:
				return SQL_NAME_EDEFAULT == null ? sqlName != null : !SQL_NAME_EDEFAULT.equals(sqlName);
			case WebAppPackage.SQL_ACTION__HANDLING_ACTION:
				return handlingAction != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (sqlName: ");
		result.append(sqlName);
		result.append(')');
		return result.toString();
	}

} //SQLActionImpl
