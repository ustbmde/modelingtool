/**
 */
package edu.ustb.sei.mde.webapp.impl;

import edu.ustb.sei.mde.webapp.AjaxType;
import edu.ustb.sei.mde.webapp.DataFormat;
import edu.ustb.sei.mde.webapp.Deployable;
import edu.ustb.sei.mde.webapp.DeployablePointer;
import edu.ustb.sei.mde.webapp.JQueryAjaxAction;
import edu.ustb.sei.mde.webapp.OperationRealization;
import edu.ustb.sei.mde.webapp.WebAppPackage;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>JQuery Ajax Action</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.webapp.impl.JQueryAjaxActionImpl#getUrl <em>Url</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.impl.JQueryAjaxActionImpl#getDeployable <em>Deployable</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.impl.JQueryAjaxActionImpl#getSelectorCode <em>Selector Code</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.impl.JQueryAjaxActionImpl#getAjaxType <em>Ajax Type</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.impl.JQueryAjaxActionImpl#getDataCode <em>Data Code</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.impl.JQueryAjaxActionImpl#getDataType <em>Data Type</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.impl.JQueryAjaxActionImpl#getCallbackFunction <em>Callback Function</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class JQueryAjaxActionImpl extends JavascriptActionImpl implements JQueryAjaxAction {
	/**
	 * The default value of the '{@link #getUrl() <em>Url</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUrl()
	 * @generated
	 * @ordered
	 */
	protected static final String URL_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getUrl() <em>Url</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUrl()
	 * @generated
	 * @ordered
	 */
	protected String url = URL_EDEFAULT;

	/**
	 * The cached value of the '{@link #getDeployable() <em>Deployable</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDeployable()
	 * @generated
	 * @ordered
	 */
	protected Deployable deployable;

	/**
	 * The default value of the '{@link #getSelectorCode() <em>Selector Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSelectorCode()
	 * @generated
	 * @ordered
	 */
	protected static final String SELECTOR_CODE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSelectorCode() <em>Selector Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSelectorCode()
	 * @generated
	 * @ordered
	 */
	protected String selectorCode = SELECTOR_CODE_EDEFAULT;

	/**
	 * The default value of the '{@link #getAjaxType() <em>Ajax Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAjaxType()
	 * @generated
	 * @ordered
	 */
	protected static final AjaxType AJAX_TYPE_EDEFAULT = AjaxType.AJAX_LOAD;

	/**
	 * The cached value of the '{@link #getAjaxType() <em>Ajax Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAjaxType()
	 * @generated
	 * @ordered
	 */
	protected AjaxType ajaxType = AJAX_TYPE_EDEFAULT;

	/**
	 * The default value of the '{@link #getDataCode() <em>Data Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDataCode()
	 * @generated
	 * @ordered
	 */
	protected static final String DATA_CODE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDataCode() <em>Data Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDataCode()
	 * @generated
	 * @ordered
	 */
	protected String dataCode = DATA_CODE_EDEFAULT;

	/**
	 * The default value of the '{@link #getDataType() <em>Data Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDataType()
	 * @generated
	 * @ordered
	 */
	protected static final DataFormat DATA_TYPE_EDEFAULT = DataFormat.XML;

	/**
	 * The cached value of the '{@link #getDataType() <em>Data Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDataType()
	 * @generated
	 * @ordered
	 */
	protected DataFormat dataType = DATA_TYPE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getCallbackFunction() <em>Callback Function</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCallbackFunction()
	 * @generated
	 * @ordered
	 */
	protected OperationRealization callbackFunction;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected JQueryAjaxActionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WebAppPackage.Literals.JQUERY_AJAX_ACTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUrl(String newUrl) {
		String oldUrl = url;
		url = newUrl;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WebAppPackage.JQUERY_AJAX_ACTION__URL, oldUrl, url));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Deployable getDeployable() {
		if (deployable != null && deployable.eIsProxy()) {
			InternalEObject oldDeployable = (InternalEObject)deployable;
			deployable = (Deployable)eResolveProxy(oldDeployable);
			if (deployable != oldDeployable) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, WebAppPackage.JQUERY_AJAX_ACTION__DEPLOYABLE, oldDeployable, deployable));
			}
		}
		return deployable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Deployable basicGetDeployable() {
		return deployable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDeployable(Deployable newDeployable) {
		Deployable oldDeployable = deployable;
		deployable = newDeployable;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WebAppPackage.JQUERY_AJAX_ACTION__DEPLOYABLE, oldDeployable, deployable));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSelectorCode() {
		return selectorCode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSelectorCode(String newSelectorCode) {
		String oldSelectorCode = selectorCode;
		selectorCode = newSelectorCode;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WebAppPackage.JQUERY_AJAX_ACTION__SELECTOR_CODE, oldSelectorCode, selectorCode));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AjaxType getAjaxType() {
		return ajaxType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAjaxType(AjaxType newAjaxType) {
		AjaxType oldAjaxType = ajaxType;
		ajaxType = newAjaxType == null ? AJAX_TYPE_EDEFAULT : newAjaxType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WebAppPackage.JQUERY_AJAX_ACTION__AJAX_TYPE, oldAjaxType, ajaxType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDataCode() {
		return dataCode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDataCode(String newDataCode) {
		String oldDataCode = dataCode;
		dataCode = newDataCode;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WebAppPackage.JQUERY_AJAX_ACTION__DATA_CODE, oldDataCode, dataCode));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataFormat getDataType() {
		return dataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDataType(DataFormat newDataType) {
		DataFormat oldDataType = dataType;
		dataType = newDataType == null ? DATA_TYPE_EDEFAULT : newDataType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WebAppPackage.JQUERY_AJAX_ACTION__DATA_TYPE, oldDataType, dataType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationRealization getCallbackFunction() {
		return callbackFunction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCallbackFunction(OperationRealization newCallbackFunction, NotificationChain msgs) {
		OperationRealization oldCallbackFunction = callbackFunction;
		callbackFunction = newCallbackFunction;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, WebAppPackage.JQUERY_AJAX_ACTION__CALLBACK_FUNCTION, oldCallbackFunction, newCallbackFunction);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCallbackFunction(OperationRealization newCallbackFunction) {
		if (newCallbackFunction != callbackFunction) {
			NotificationChain msgs = null;
			if (callbackFunction != null)
				msgs = ((InternalEObject)callbackFunction).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - WebAppPackage.JQUERY_AJAX_ACTION__CALLBACK_FUNCTION, null, msgs);
			if (newCallbackFunction != null)
				msgs = ((InternalEObject)newCallbackFunction).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - WebAppPackage.JQUERY_AJAX_ACTION__CALLBACK_FUNCTION, null, msgs);
			msgs = basicSetCallbackFunction(newCallbackFunction, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WebAppPackage.JQUERY_AJAX_ACTION__CALLBACK_FUNCTION, newCallbackFunction, newCallbackFunction));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case WebAppPackage.JQUERY_AJAX_ACTION__CALLBACK_FUNCTION:
				return basicSetCallbackFunction(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case WebAppPackage.JQUERY_AJAX_ACTION__URL:
				return getUrl();
			case WebAppPackage.JQUERY_AJAX_ACTION__DEPLOYABLE:
				if (resolve) return getDeployable();
				return basicGetDeployable();
			case WebAppPackage.JQUERY_AJAX_ACTION__SELECTOR_CODE:
				return getSelectorCode();
			case WebAppPackage.JQUERY_AJAX_ACTION__AJAX_TYPE:
				return getAjaxType();
			case WebAppPackage.JQUERY_AJAX_ACTION__DATA_CODE:
				return getDataCode();
			case WebAppPackage.JQUERY_AJAX_ACTION__DATA_TYPE:
				return getDataType();
			case WebAppPackage.JQUERY_AJAX_ACTION__CALLBACK_FUNCTION:
				return getCallbackFunction();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case WebAppPackage.JQUERY_AJAX_ACTION__URL:
				setUrl((String)newValue);
				return;
			case WebAppPackage.JQUERY_AJAX_ACTION__DEPLOYABLE:
				setDeployable((Deployable)newValue);
				return;
			case WebAppPackage.JQUERY_AJAX_ACTION__SELECTOR_CODE:
				setSelectorCode((String)newValue);
				return;
			case WebAppPackage.JQUERY_AJAX_ACTION__AJAX_TYPE:
				setAjaxType((AjaxType)newValue);
				return;
			case WebAppPackage.JQUERY_AJAX_ACTION__DATA_CODE:
				setDataCode((String)newValue);
				return;
			case WebAppPackage.JQUERY_AJAX_ACTION__DATA_TYPE:
				setDataType((DataFormat)newValue);
				return;
			case WebAppPackage.JQUERY_AJAX_ACTION__CALLBACK_FUNCTION:
				setCallbackFunction((OperationRealization)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case WebAppPackage.JQUERY_AJAX_ACTION__URL:
				setUrl(URL_EDEFAULT);
				return;
			case WebAppPackage.JQUERY_AJAX_ACTION__DEPLOYABLE:
				setDeployable((Deployable)null);
				return;
			case WebAppPackage.JQUERY_AJAX_ACTION__SELECTOR_CODE:
				setSelectorCode(SELECTOR_CODE_EDEFAULT);
				return;
			case WebAppPackage.JQUERY_AJAX_ACTION__AJAX_TYPE:
				setAjaxType(AJAX_TYPE_EDEFAULT);
				return;
			case WebAppPackage.JQUERY_AJAX_ACTION__DATA_CODE:
				setDataCode(DATA_CODE_EDEFAULT);
				return;
			case WebAppPackage.JQUERY_AJAX_ACTION__DATA_TYPE:
				setDataType(DATA_TYPE_EDEFAULT);
				return;
			case WebAppPackage.JQUERY_AJAX_ACTION__CALLBACK_FUNCTION:
				setCallbackFunction((OperationRealization)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case WebAppPackage.JQUERY_AJAX_ACTION__URL:
				return URL_EDEFAULT == null ? url != null : !URL_EDEFAULT.equals(url);
			case WebAppPackage.JQUERY_AJAX_ACTION__DEPLOYABLE:
				return deployable != null;
			case WebAppPackage.JQUERY_AJAX_ACTION__SELECTOR_CODE:
				return SELECTOR_CODE_EDEFAULT == null ? selectorCode != null : !SELECTOR_CODE_EDEFAULT.equals(selectorCode);
			case WebAppPackage.JQUERY_AJAX_ACTION__AJAX_TYPE:
				return ajaxType != AJAX_TYPE_EDEFAULT;
			case WebAppPackage.JQUERY_AJAX_ACTION__DATA_CODE:
				return DATA_CODE_EDEFAULT == null ? dataCode != null : !DATA_CODE_EDEFAULT.equals(dataCode);
			case WebAppPackage.JQUERY_AJAX_ACTION__DATA_TYPE:
				return dataType != DATA_TYPE_EDEFAULT;
			case WebAppPackage.JQUERY_AJAX_ACTION__CALLBACK_FUNCTION:
				return callbackFunction != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == DeployablePointer.class) {
			switch (derivedFeatureID) {
				case WebAppPackage.JQUERY_AJAX_ACTION__URL: return WebAppPackage.DEPLOYABLE_POINTER__URL;
				case WebAppPackage.JQUERY_AJAX_ACTION__DEPLOYABLE: return WebAppPackage.DEPLOYABLE_POINTER__DEPLOYABLE;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == DeployablePointer.class) {
			switch (baseFeatureID) {
				case WebAppPackage.DEPLOYABLE_POINTER__URL: return WebAppPackage.JQUERY_AJAX_ACTION__URL;
				case WebAppPackage.DEPLOYABLE_POINTER__DEPLOYABLE: return WebAppPackage.JQUERY_AJAX_ACTION__DEPLOYABLE;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (url: ");
		result.append(url);
		result.append(", selectorCode: ");
		result.append(selectorCode);
		result.append(", ajaxType: ");
		result.append(ajaxType);
		result.append(", dataCode: ");
		result.append(dataCode);
		result.append(", dataType: ");
		result.append(dataType);
		result.append(')');
		return result.toString();
	}

} //JQueryAjaxActionImpl
