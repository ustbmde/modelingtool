/**
 */
package edu.ustb.sei.mde.webapp.impl;

import edu.ustb.sei.mde.webapp.SQLUpdateAction;
import edu.ustb.sei.mde.webapp.WebAppPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>SQL Update Action</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.webapp.impl.SQLUpdateActionImpl#getTable <em>Table</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.impl.SQLUpdateActionImpl#getColumns <em>Columns</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.impl.SQLUpdateActionImpl#getValueCodes <em>Value Codes</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.impl.SQLUpdateActionImpl#getConditionCode <em>Condition Code</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class SQLUpdateActionImpl extends SQLActionImpl implements SQLUpdateAction {
	/**
	 * The default value of the '{@link #getTable() <em>Table</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTable()
	 * @generated
	 * @ordered
	 */
	protected static final String TABLE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTable() <em>Table</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTable()
	 * @generated
	 * @ordered
	 */
	protected String table = TABLE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getColumns() <em>Columns</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getColumns()
	 * @generated
	 * @ordered
	 */
	protected EList<String> columns;

	/**
	 * The cached value of the '{@link #getValueCodes() <em>Value Codes</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValueCodes()
	 * @generated
	 * @ordered
	 */
	protected EList<String> valueCodes;

	/**
	 * The default value of the '{@link #getConditionCode() <em>Condition Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConditionCode()
	 * @generated
	 * @ordered
	 */
	protected static final String CONDITION_CODE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getConditionCode() <em>Condition Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConditionCode()
	 * @generated
	 * @ordered
	 */
	protected String conditionCode = CONDITION_CODE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SQLUpdateActionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WebAppPackage.Literals.SQL_UPDATE_ACTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getTable() {
		return table;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTable(String newTable) {
		String oldTable = table;
		table = newTable;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WebAppPackage.SQL_UPDATE_ACTION__TABLE, oldTable, table));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<String> getColumns() {
		if (columns == null) {
			columns = new EDataTypeUniqueEList<String>(String.class, this, WebAppPackage.SQL_UPDATE_ACTION__COLUMNS);
		}
		return columns;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<String> getValueCodes() {
		if (valueCodes == null) {
			valueCodes = new EDataTypeUniqueEList<String>(String.class, this, WebAppPackage.SQL_UPDATE_ACTION__VALUE_CODES);
		}
		return valueCodes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getConditionCode() {
		return conditionCode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConditionCode(String newConditionCode) {
		String oldConditionCode = conditionCode;
		conditionCode = newConditionCode;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WebAppPackage.SQL_UPDATE_ACTION__CONDITION_CODE, oldConditionCode, conditionCode));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case WebAppPackage.SQL_UPDATE_ACTION__TABLE:
				return getTable();
			case WebAppPackage.SQL_UPDATE_ACTION__COLUMNS:
				return getColumns();
			case WebAppPackage.SQL_UPDATE_ACTION__VALUE_CODES:
				return getValueCodes();
			case WebAppPackage.SQL_UPDATE_ACTION__CONDITION_CODE:
				return getConditionCode();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case WebAppPackage.SQL_UPDATE_ACTION__TABLE:
				setTable((String)newValue);
				return;
			case WebAppPackage.SQL_UPDATE_ACTION__COLUMNS:
				getColumns().clear();
				getColumns().addAll((Collection<? extends String>)newValue);
				return;
			case WebAppPackage.SQL_UPDATE_ACTION__VALUE_CODES:
				getValueCodes().clear();
				getValueCodes().addAll((Collection<? extends String>)newValue);
				return;
			case WebAppPackage.SQL_UPDATE_ACTION__CONDITION_CODE:
				setConditionCode((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case WebAppPackage.SQL_UPDATE_ACTION__TABLE:
				setTable(TABLE_EDEFAULT);
				return;
			case WebAppPackage.SQL_UPDATE_ACTION__COLUMNS:
				getColumns().clear();
				return;
			case WebAppPackage.SQL_UPDATE_ACTION__VALUE_CODES:
				getValueCodes().clear();
				return;
			case WebAppPackage.SQL_UPDATE_ACTION__CONDITION_CODE:
				setConditionCode(CONDITION_CODE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case WebAppPackage.SQL_UPDATE_ACTION__TABLE:
				return TABLE_EDEFAULT == null ? table != null : !TABLE_EDEFAULT.equals(table);
			case WebAppPackage.SQL_UPDATE_ACTION__COLUMNS:
				return columns != null && !columns.isEmpty();
			case WebAppPackage.SQL_UPDATE_ACTION__VALUE_CODES:
				return valueCodes != null && !valueCodes.isEmpty();
			case WebAppPackage.SQL_UPDATE_ACTION__CONDITION_CODE:
				return CONDITION_CODE_EDEFAULT == null ? conditionCode != null : !CONDITION_CODE_EDEFAULT.equals(conditionCode);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (table: ");
		result.append(table);
		result.append(", columns: ");
		result.append(columns);
		result.append(", valueCodes: ");
		result.append(valueCodes);
		result.append(", conditionCode: ");
		result.append(conditionCode);
		result.append(')');
		return result.toString();
	}

} //SQLUpdateActionImpl
