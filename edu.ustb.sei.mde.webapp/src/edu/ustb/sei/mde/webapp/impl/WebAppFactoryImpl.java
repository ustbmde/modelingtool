/**
 */
package edu.ustb.sei.mde.webapp.impl;

import edu.ustb.sei.mde.webapp.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class WebAppFactoryImpl extends EFactoryImpl implements WebAppFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static WebAppFactory init() {
		try {
			WebAppFactory theWebAppFactory = (WebAppFactory)EPackage.Registry.INSTANCE.getEFactory(WebAppPackage.eNS_URI);
			if (theWebAppFactory != null) {
				return theWebAppFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new WebAppFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WebAppFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case WebAppPackage.INTERFACE: return createInterface();
			case WebAppPackage.OPERATION_DECLARATION: return createOperationDeclaration();
			case WebAppPackage.PARAMETER: return createParameter();
			case WebAppPackage.CODE_OPERATION: return createCodeOperation();
			case WebAppPackage.ACTION_OPERATION: return createActionOperation();
			case WebAppPackage.PAGE_COMPONENT: return createPageComponent();
			case WebAppPackage.HTML_NODE: return createHTMLNode();
			case WebAppPackage.COMPOSITION: return createComposition();
			case WebAppPackage.PAGE_COMPONENT_COMPOSITION: return createPageComponentComposition();
			case WebAppPackage.EVENT_HANDLER: return createEventHandler();
			case WebAppPackage.SERVER_COMPONENT: return createServerComponent();
			case WebAppPackage.CLASS_COMPONENT: return createClassComponent();
			case WebAppPackage.DATABASE: return createDatabase();
			case WebAppPackage.CODE_ACTION: return createCodeAction();
			case WebAppPackage.CALL_ACTION: return createCallAction();
			case WebAppPackage.LET_ACTION: return createLetAction();
			case WebAppPackage.FOR_ACTION: return createForAction();
			case WebAppPackage.IF_ACTION: return createIfAction();
			case WebAppPackage.SEQUENCE_ACTION: return createSequenceAction();
			case WebAppPackage.TRY_CATCH_ACTION: return createTryCatchAction();
			case WebAppPackage.CATCH_PART: return createCatchPart();
			case WebAppPackage.JQUERY_AJAX_ACTION: return createJQueryAjaxAction();
			case WebAppPackage.JQUERY_EVENT_ACTION: return createJQueryEventAction();
			case WebAppPackage.SQL_SELECT_ACTION: return createSQLSelectAction();
			case WebAppPackage.SQL_INSERT_ACTION: return createSQLInsertAction();
			case WebAppPackage.SQL_UPDATE_ACTION: return createSQLUpdateAction();
			case WebAppPackage.SQL_DELETE_ACTION: return createSQLDeleteAction();
			case WebAppPackage.SQL_EXECUTION_ACTION: return createSQLExecutionAction();
			case WebAppPackage.SQL_RESULT_SET_ITERATION_ACTION: return createSQLResultSetIterationAction();
			case WebAppPackage.SERVER_PAGE_TEMPLATE_ACTION: return createServerPageTemplateAction();
			case WebAppPackage.SERVER_PAGE_EXPRESSION_ACTION: return createServerPageExpressionAction();
			case WebAppPackage.SERVER_PAGE_STRUCTURED_TEMPLATE_ACTION: return createServerPageStructuredTemplateAction();
			case WebAppPackage.WEB_APPLICATION: return createWebApplication();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case WebAppPackage.AJAX_TYPE:
				return createAjaxTypeFromString(eDataType, initialValue);
			case WebAppPackage.PAGE_CONTENT_TYPE:
				return createPageContentTypeFromString(eDataType, initialValue);
			case WebAppPackage.DATA_FORMAT:
				return createDataFormatFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case WebAppPackage.AJAX_TYPE:
				return convertAjaxTypeToString(eDataType, instanceValue);
			case WebAppPackage.PAGE_CONTENT_TYPE:
				return convertPageContentTypeToString(eDataType, instanceValue);
			case WebAppPackage.DATA_FORMAT:
				return convertDataFormatToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Interface createInterface() {
		InterfaceImpl interface_ = new InterfaceImpl();
		return interface_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OperationDeclaration createOperationDeclaration() {
		OperationDeclarationImpl operationDeclaration = new OperationDeclarationImpl();
		return operationDeclaration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Parameter createParameter() {
		ParameterImpl parameter = new ParameterImpl();
		return parameter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CodeOperation createCodeOperation() {
		CodeOperationImpl codeOperation = new CodeOperationImpl();
		return codeOperation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActionOperation createActionOperation() {
		ActionOperationImpl actionOperation = new ActionOperationImpl();
		return actionOperation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PageComponent createPageComponent() {
		PageComponentImpl pageComponent = new PageComponentImpl();
		return pageComponent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HTMLNode createHTMLNode() {
		HTMLNodeImpl htmlNode = new HTMLNodeImpl();
		return htmlNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Composition createComposition() {
		CompositionImpl composition = new CompositionImpl();
		return composition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PageComponentComposition createPageComponentComposition() {
		PageComponentCompositionImpl pageComponentComposition = new PageComponentCompositionImpl();
		return pageComponentComposition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EventHandler createEventHandler() {
		EventHandlerImpl eventHandler = new EventHandlerImpl();
		return eventHandler;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ServerComponent createServerComponent() {
		ServerComponentImpl serverComponent = new ServerComponentImpl();
		return serverComponent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClassComponent createClassComponent() {
		ClassComponentImpl classComponent = new ClassComponentImpl();
		return classComponent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Database createDatabase() {
		DatabaseImpl database = new DatabaseImpl();
		return database;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CodeAction createCodeAction() {
		CodeActionImpl codeAction = new CodeActionImpl();
		return codeAction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CallAction createCallAction() {
		CallActionImpl callAction = new CallActionImpl();
		return callAction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LetAction createLetAction() {
		LetActionImpl letAction = new LetActionImpl();
		return letAction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ForAction createForAction() {
		ForActionImpl forAction = new ForActionImpl();
		return forAction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IfAction createIfAction() {
		IfActionImpl ifAction = new IfActionImpl();
		return ifAction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SequenceAction createSequenceAction() {
		SequenceActionImpl sequenceAction = new SequenceActionImpl();
		return sequenceAction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TryCatchAction createTryCatchAction() {
		TryCatchActionImpl tryCatchAction = new TryCatchActionImpl();
		return tryCatchAction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CatchPart createCatchPart() {
		CatchPartImpl catchPart = new CatchPartImpl();
		return catchPart;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public JQueryAjaxAction createJQueryAjaxAction() {
		JQueryAjaxActionImpl jQueryAjaxAction = new JQueryAjaxActionImpl();
		return jQueryAjaxAction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public JQueryEventAction createJQueryEventAction() {
		JQueryEventActionImpl jQueryEventAction = new JQueryEventActionImpl();
		return jQueryEventAction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SQLSelectAction createSQLSelectAction() {
		SQLSelectActionImpl sqlSelectAction = new SQLSelectActionImpl();
		return sqlSelectAction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SQLInsertAction createSQLInsertAction() {
		SQLInsertActionImpl sqlInsertAction = new SQLInsertActionImpl();
		return sqlInsertAction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SQLUpdateAction createSQLUpdateAction() {
		SQLUpdateActionImpl sqlUpdateAction = new SQLUpdateActionImpl();
		return sqlUpdateAction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SQLDeleteAction createSQLDeleteAction() {
		SQLDeleteActionImpl sqlDeleteAction = new SQLDeleteActionImpl();
		return sqlDeleteAction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SQLExecutionAction createSQLExecutionAction() {
		SQLExecutionActionImpl sqlExecutionAction = new SQLExecutionActionImpl();
		return sqlExecutionAction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SQLResultSetIterationAction createSQLResultSetIterationAction() {
		SQLResultSetIterationActionImpl sqlResultSetIterationAction = new SQLResultSetIterationActionImpl();
		return sqlResultSetIterationAction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ServerPageTemplateAction createServerPageTemplateAction() {
		ServerPageTemplateActionImpl serverPageTemplateAction = new ServerPageTemplateActionImpl();
		return serverPageTemplateAction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ServerPageExpressionAction createServerPageExpressionAction() {
		ServerPageExpressionActionImpl serverPageExpressionAction = new ServerPageExpressionActionImpl();
		return serverPageExpressionAction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ServerPageStructuredTemplateAction createServerPageStructuredTemplateAction() {
		ServerPageStructuredTemplateActionImpl serverPageStructuredTemplateAction = new ServerPageStructuredTemplateActionImpl();
		return serverPageStructuredTemplateAction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WebApplication createWebApplication() {
		WebApplicationImpl webApplication = new WebApplicationImpl();
		return webApplication;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AjaxType createAjaxTypeFromString(EDataType eDataType, String initialValue) {
		AjaxType result = AjaxType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertAjaxTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PageContentType createPageContentTypeFromString(EDataType eDataType, String initialValue) {
		PageContentType result = PageContentType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertPageContentTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataFormat createDataFormatFromString(EDataType eDataType, String initialValue) {
		DataFormat result = DataFormat.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertDataFormatToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WebAppPackage getWebAppPackage() {
		return (WebAppPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static WebAppPackage getPackage() {
		return WebAppPackage.eINSTANCE;
	}

} //WebAppFactoryImpl
