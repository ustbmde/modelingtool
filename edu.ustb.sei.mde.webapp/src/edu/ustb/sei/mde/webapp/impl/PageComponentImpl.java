/**
 */
package edu.ustb.sei.mde.webapp.impl;

import edu.ustb.sei.mde.webapp.Deployable;
import edu.ustb.sei.mde.webapp.HTMLNode;
import edu.ustb.sei.mde.webapp.PageComponent;
import edu.ustb.sei.mde.webapp.WebAppPackage;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Page Component</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.webapp.impl.PageComponentImpl#getDeployPath <em>Deploy Path</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.impl.PageComponentImpl#getNode <em>Node</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.impl.PageComponentImpl#isPage <em>Page</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class PageComponentImpl extends ComponentImpl implements PageComponent {
	/**
	 * The default value of the '{@link #getDeployPath() <em>Deploy Path</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDeployPath()
	 * @generated
	 * @ordered
	 */
	protected static final String DEPLOY_PATH_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDeployPath() <em>Deploy Path</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDeployPath()
	 * @generated
	 * @ordered
	 */
	protected String deployPath = DEPLOY_PATH_EDEFAULT;

	/**
	 * The cached value of the '{@link #getNode() <em>Node</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNode()
	 * @generated
	 * @ordered
	 */
	protected HTMLNode node;

	/**
	 * The default value of the '{@link #isPage() <em>Page</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isPage()
	 * @generated
	 * @ordered
	 */
	protected static final boolean PAGE_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isPage() <em>Page</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isPage()
	 * @generated
	 * @ordered
	 */
	protected boolean page = PAGE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PageComponentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WebAppPackage.Literals.PAGE_COMPONENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDeployPath() {
		return deployPath;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDeployPath(String newDeployPath) {
		String oldDeployPath = deployPath;
		deployPath = newDeployPath;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WebAppPackage.PAGE_COMPONENT__DEPLOY_PATH, oldDeployPath, deployPath));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HTMLNode getNode() {
		return node;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNode(HTMLNode newNode, NotificationChain msgs) {
		HTMLNode oldNode = node;
		node = newNode;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, WebAppPackage.PAGE_COMPONENT__NODE, oldNode, newNode);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNode(HTMLNode newNode) {
		if (newNode != node) {
			NotificationChain msgs = null;
			if (node != null)
				msgs = ((InternalEObject)node).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - WebAppPackage.PAGE_COMPONENT__NODE, null, msgs);
			if (newNode != null)
				msgs = ((InternalEObject)newNode).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - WebAppPackage.PAGE_COMPONENT__NODE, null, msgs);
			msgs = basicSetNode(newNode, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WebAppPackage.PAGE_COMPONENT__NODE, newNode, newNode));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isPage() {
		return page;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPage(boolean newPage) {
		boolean oldPage = page;
		page = newPage;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WebAppPackage.PAGE_COMPONENT__PAGE, oldPage, page));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public String getURL() {
		StringBuilder sb = new StringBuilder();
		if(!(deployPath==null || deployPath.length()==0)) {
			sb.append(deployPath);
			sb.append('/');
		}
		sb.append(name);
		if(page) {
			sb.append(".html");
		} else {
			sb.append(".hf");
		}
		return sb.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case WebAppPackage.PAGE_COMPONENT__NODE:
				return basicSetNode(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case WebAppPackage.PAGE_COMPONENT__DEPLOY_PATH:
				return getDeployPath();
			case WebAppPackage.PAGE_COMPONENT__NODE:
				return getNode();
			case WebAppPackage.PAGE_COMPONENT__PAGE:
				return isPage();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case WebAppPackage.PAGE_COMPONENT__DEPLOY_PATH:
				setDeployPath((String)newValue);
				return;
			case WebAppPackage.PAGE_COMPONENT__NODE:
				setNode((HTMLNode)newValue);
				return;
			case WebAppPackage.PAGE_COMPONENT__PAGE:
				setPage((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case WebAppPackage.PAGE_COMPONENT__DEPLOY_PATH:
				setDeployPath(DEPLOY_PATH_EDEFAULT);
				return;
			case WebAppPackage.PAGE_COMPONENT__NODE:
				setNode((HTMLNode)null);
				return;
			case WebAppPackage.PAGE_COMPONENT__PAGE:
				setPage(PAGE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case WebAppPackage.PAGE_COMPONENT__DEPLOY_PATH:
				return DEPLOY_PATH_EDEFAULT == null ? deployPath != null : !DEPLOY_PATH_EDEFAULT.equals(deployPath);
			case WebAppPackage.PAGE_COMPONENT__NODE:
				return node != null;
			case WebAppPackage.PAGE_COMPONENT__PAGE:
				return page != PAGE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == Deployable.class) {
			switch (derivedFeatureID) {
				case WebAppPackage.PAGE_COMPONENT__DEPLOY_PATH: return WebAppPackage.DEPLOYABLE__DEPLOY_PATH;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == Deployable.class) {
			switch (baseFeatureID) {
				case WebAppPackage.DEPLOYABLE__DEPLOY_PATH: return WebAppPackage.PAGE_COMPONENT__DEPLOY_PATH;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedOperationID(int baseOperationID, Class<?> baseClass) {
		if (baseClass == Deployable.class) {
			switch (baseOperationID) {
				case WebAppPackage.DEPLOYABLE___GET_URL: return WebAppPackage.PAGE_COMPONENT___GET_URL;
				default: return -1;
			}
		}
		return super.eDerivedOperationID(baseOperationID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case WebAppPackage.PAGE_COMPONENT___GET_URL:
				return getURL();
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (deployPath: ");
		result.append(deployPath);
		result.append(", page: ");
		result.append(page);
		result.append(')');
		return result.toString();
	}

} //PageComponentImpl
