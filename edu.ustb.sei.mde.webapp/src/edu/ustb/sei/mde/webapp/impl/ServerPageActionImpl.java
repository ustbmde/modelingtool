/**
 */
package edu.ustb.sei.mde.webapp.impl;

import edu.ustb.sei.mde.webapp.ServerPageAction;
import edu.ustb.sei.mde.webapp.WebAppPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Server Page Action</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public abstract class ServerPageActionImpl extends ActionImpl implements ServerPageAction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ServerPageActionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WebAppPackage.Literals.SERVER_PAGE_ACTION;
	}

} //ServerPageActionImpl
