/**
 */
package edu.ustb.sei.mde.webapp.impl;

import edu.ustb.sei.mde.webapp.Action;
import edu.ustb.sei.mde.webapp.CatchPart;
import edu.ustb.sei.mde.webapp.WebAppPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Catch Part</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.webapp.impl.CatchPartImpl#getExceptionVariable <em>Exception Variable</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.impl.CatchPartImpl#getActioin <em>Actioin</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class CatchPartImpl extends StatementContainerImpl implements CatchPart {
	/**
	 * The default value of the '{@link #getExceptionVariable() <em>Exception Variable</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExceptionVariable()
	 * @generated
	 * @ordered
	 */
	protected static final String EXCEPTION_VARIABLE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getExceptionVariable() <em>Exception Variable</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExceptionVariable()
	 * @generated
	 * @ordered
	 */
	protected String exceptionVariable = EXCEPTION_VARIABLE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getActioin() <em>Actioin</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActioin()
	 * @generated
	 * @ordered
	 */
	protected Action actioin;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CatchPartImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WebAppPackage.Literals.CATCH_PART;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getExceptionVariable() {
		return exceptionVariable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExceptionVariable(String newExceptionVariable) {
		String oldExceptionVariable = exceptionVariable;
		exceptionVariable = newExceptionVariable;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WebAppPackage.CATCH_PART__EXCEPTION_VARIABLE, oldExceptionVariable, exceptionVariable));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Action getActioin() {
		return actioin;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetActioin(Action newActioin, NotificationChain msgs) {
		Action oldActioin = actioin;
		actioin = newActioin;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, WebAppPackage.CATCH_PART__ACTIOIN, oldActioin, newActioin);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setActioin(Action newActioin) {
		if (newActioin != actioin) {
			NotificationChain msgs = null;
			if (actioin != null)
				msgs = ((InternalEObject)actioin).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - WebAppPackage.CATCH_PART__ACTIOIN, null, msgs);
			if (newActioin != null)
				msgs = ((InternalEObject)newActioin).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - WebAppPackage.CATCH_PART__ACTIOIN, null, msgs);
			msgs = basicSetActioin(newActioin, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WebAppPackage.CATCH_PART__ACTIOIN, newActioin, newActioin));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case WebAppPackage.CATCH_PART__ACTIOIN:
				return basicSetActioin(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case WebAppPackage.CATCH_PART__EXCEPTION_VARIABLE:
				return getExceptionVariable();
			case WebAppPackage.CATCH_PART__ACTIOIN:
				return getActioin();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case WebAppPackage.CATCH_PART__EXCEPTION_VARIABLE:
				setExceptionVariable((String)newValue);
				return;
			case WebAppPackage.CATCH_PART__ACTIOIN:
				setActioin((Action)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case WebAppPackage.CATCH_PART__EXCEPTION_VARIABLE:
				setExceptionVariable(EXCEPTION_VARIABLE_EDEFAULT);
				return;
			case WebAppPackage.CATCH_PART__ACTIOIN:
				setActioin((Action)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case WebAppPackage.CATCH_PART__EXCEPTION_VARIABLE:
				return EXCEPTION_VARIABLE_EDEFAULT == null ? exceptionVariable != null : !EXCEPTION_VARIABLE_EDEFAULT.equals(exceptionVariable);
			case WebAppPackage.CATCH_PART__ACTIOIN:
				return actioin != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (exceptionVariable: ");
		result.append(exceptionVariable);
		result.append(')');
		return result.toString();
	}

} //CatchPartImpl
