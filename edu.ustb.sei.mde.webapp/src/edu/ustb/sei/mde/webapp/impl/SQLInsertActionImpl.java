/**
 */
package edu.ustb.sei.mde.webapp.impl;

import edu.ustb.sei.mde.webapp.SQLInsertAction;
import edu.ustb.sei.mde.webapp.WebAppPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>SQL Insert Action</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.webapp.impl.SQLInsertActionImpl#getTable <em>Table</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.impl.SQLInsertActionImpl#getColumns <em>Columns</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.impl.SQLInsertActionImpl#getValueCodes <em>Value Codes</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class SQLInsertActionImpl extends SQLActionImpl implements SQLInsertAction {
	/**
	 * The default value of the '{@link #getTable() <em>Table</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTable()
	 * @generated
	 * @ordered
	 */
	protected static final String TABLE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTable() <em>Table</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTable()
	 * @generated
	 * @ordered
	 */
	protected String table = TABLE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getColumns() <em>Columns</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getColumns()
	 * @generated
	 * @ordered
	 */
	protected EList<String> columns;

	/**
	 * The cached value of the '{@link #getValueCodes() <em>Value Codes</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValueCodes()
	 * @generated
	 * @ordered
	 */
	protected EList<String> valueCodes;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SQLInsertActionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WebAppPackage.Literals.SQL_INSERT_ACTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getTable() {
		return table;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTable(String newTable) {
		String oldTable = table;
		table = newTable;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WebAppPackage.SQL_INSERT_ACTION__TABLE, oldTable, table));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<String> getColumns() {
		if (columns == null) {
			columns = new EDataTypeUniqueEList<String>(String.class, this, WebAppPackage.SQL_INSERT_ACTION__COLUMNS);
		}
		return columns;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<String> getValueCodes() {
		if (valueCodes == null) {
			valueCodes = new EDataTypeUniqueEList<String>(String.class, this, WebAppPackage.SQL_INSERT_ACTION__VALUE_CODES);
		}
		return valueCodes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case WebAppPackage.SQL_INSERT_ACTION__TABLE:
				return getTable();
			case WebAppPackage.SQL_INSERT_ACTION__COLUMNS:
				return getColumns();
			case WebAppPackage.SQL_INSERT_ACTION__VALUE_CODES:
				return getValueCodes();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case WebAppPackage.SQL_INSERT_ACTION__TABLE:
				setTable((String)newValue);
				return;
			case WebAppPackage.SQL_INSERT_ACTION__COLUMNS:
				getColumns().clear();
				getColumns().addAll((Collection<? extends String>)newValue);
				return;
			case WebAppPackage.SQL_INSERT_ACTION__VALUE_CODES:
				getValueCodes().clear();
				getValueCodes().addAll((Collection<? extends String>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case WebAppPackage.SQL_INSERT_ACTION__TABLE:
				setTable(TABLE_EDEFAULT);
				return;
			case WebAppPackage.SQL_INSERT_ACTION__COLUMNS:
				getColumns().clear();
				return;
			case WebAppPackage.SQL_INSERT_ACTION__VALUE_CODES:
				getValueCodes().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case WebAppPackage.SQL_INSERT_ACTION__TABLE:
				return TABLE_EDEFAULT == null ? table != null : !TABLE_EDEFAULT.equals(table);
			case WebAppPackage.SQL_INSERT_ACTION__COLUMNS:
				return columns != null && !columns.isEmpty();
			case WebAppPackage.SQL_INSERT_ACTION__VALUE_CODES:
				return valueCodes != null && !valueCodes.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (table: ");
		result.append(table);
		result.append(", columns: ");
		result.append(columns);
		result.append(", valueCodes: ");
		result.append(valueCodes);
		result.append(')');
		return result.toString();
	}

} //SQLInsertActionImpl
