/**
 */
package edu.ustb.sei.mde.webapp.impl;

import edu.ustb.sei.mde.webapp.Action;
import edu.ustb.sei.mde.webapp.ForAction;
import edu.ustb.sei.mde.webapp.WebAppPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>For Action</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.webapp.impl.ForActionImpl#getConditionCode <em>Condition Code</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.impl.ForActionImpl#getBodyAction <em>Body Action</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ForActionImpl extends ActionImpl implements ForAction {
	/**
	 * The default value of the '{@link #getConditionCode() <em>Condition Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConditionCode()
	 * @generated
	 * @ordered
	 */
	protected static final String CONDITION_CODE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getConditionCode() <em>Condition Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConditionCode()
	 * @generated
	 * @ordered
	 */
	protected String conditionCode = CONDITION_CODE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getBodyAction() <em>Body Action</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBodyAction()
	 * @generated
	 * @ordered
	 */
	protected Action bodyAction;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ForActionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WebAppPackage.Literals.FOR_ACTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getConditionCode() {
		return conditionCode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConditionCode(String newConditionCode) {
		String oldConditionCode = conditionCode;
		conditionCode = newConditionCode;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WebAppPackage.FOR_ACTION__CONDITION_CODE, oldConditionCode, conditionCode));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Action getBodyAction() {
		return bodyAction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetBodyAction(Action newBodyAction, NotificationChain msgs) {
		Action oldBodyAction = bodyAction;
		bodyAction = newBodyAction;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, WebAppPackage.FOR_ACTION__BODY_ACTION, oldBodyAction, newBodyAction);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBodyAction(Action newBodyAction) {
		if (newBodyAction != bodyAction) {
			NotificationChain msgs = null;
			if (bodyAction != null)
				msgs = ((InternalEObject)bodyAction).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - WebAppPackage.FOR_ACTION__BODY_ACTION, null, msgs);
			if (newBodyAction != null)
				msgs = ((InternalEObject)newBodyAction).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - WebAppPackage.FOR_ACTION__BODY_ACTION, null, msgs);
			msgs = basicSetBodyAction(newBodyAction, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WebAppPackage.FOR_ACTION__BODY_ACTION, newBodyAction, newBodyAction));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case WebAppPackage.FOR_ACTION__BODY_ACTION:
				return basicSetBodyAction(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case WebAppPackage.FOR_ACTION__CONDITION_CODE:
				return getConditionCode();
			case WebAppPackage.FOR_ACTION__BODY_ACTION:
				return getBodyAction();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case WebAppPackage.FOR_ACTION__CONDITION_CODE:
				setConditionCode((String)newValue);
				return;
			case WebAppPackage.FOR_ACTION__BODY_ACTION:
				setBodyAction((Action)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case WebAppPackage.FOR_ACTION__CONDITION_CODE:
				setConditionCode(CONDITION_CODE_EDEFAULT);
				return;
			case WebAppPackage.FOR_ACTION__BODY_ACTION:
				setBodyAction((Action)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case WebAppPackage.FOR_ACTION__CONDITION_CODE:
				return CONDITION_CODE_EDEFAULT == null ? conditionCode != null : !CONDITION_CODE_EDEFAULT.equals(conditionCode);
			case WebAppPackage.FOR_ACTION__BODY_ACTION:
				return bodyAction != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (conditionCode: ");
		result.append(conditionCode);
		result.append(')');
		return result.toString();
	}

} //ForActionImpl
