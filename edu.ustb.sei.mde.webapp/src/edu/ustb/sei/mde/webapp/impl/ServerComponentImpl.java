/**
 */
package edu.ustb.sei.mde.webapp.impl;

import edu.ustb.sei.mde.webapp.Deployable;
import edu.ustb.sei.mde.webapp.PageContentType;
import edu.ustb.sei.mde.webapp.ServerComponent;
import edu.ustb.sei.mde.webapp.WebAppPackage;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Server Component</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.webapp.impl.ServerComponentImpl#getDeployPath <em>Deploy Path</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.impl.ServerComponentImpl#getImports <em>Imports</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.impl.ServerComponentImpl#getContentType <em>Content Type</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ServerComponentImpl extends ComponentImpl implements ServerComponent {
	/**
	 * The default value of the '{@link #getDeployPath() <em>Deploy Path</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDeployPath()
	 * @generated
	 * @ordered
	 */
	protected static final String DEPLOY_PATH_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDeployPath() <em>Deploy Path</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDeployPath()
	 * @generated
	 * @ordered
	 */
	protected String deployPath = DEPLOY_PATH_EDEFAULT;

	/**
	 * The cached value of the '{@link #getImports() <em>Imports</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getImports()
	 * @generated
	 * @ordered
	 */
	protected EList<String> imports;

	/**
	 * The default value of the '{@link #getContentType() <em>Content Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContentType()
	 * @generated
	 * @ordered
	 */
	protected static final PageContentType CONTENT_TYPE_EDEFAULT = PageContentType.HTML;

	/**
	 * The cached value of the '{@link #getContentType() <em>Content Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContentType()
	 * @generated
	 * @ordered
	 */
	protected PageContentType contentType = CONTENT_TYPE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ServerComponentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WebAppPackage.Literals.SERVER_COMPONENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDeployPath() {
		return deployPath;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDeployPath(String newDeployPath) {
		String oldDeployPath = deployPath;
		deployPath = newDeployPath;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WebAppPackage.SERVER_COMPONENT__DEPLOY_PATH, oldDeployPath, deployPath));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<String> getImports() {
		if (imports == null) {
			imports = new EDataTypeUniqueEList<String>(String.class, this, WebAppPackage.SERVER_COMPONENT__IMPORTS);
		}
		return imports;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PageContentType getContentType() {
		return contentType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setContentType(PageContentType newContentType) {
		PageContentType oldContentType = contentType;
		contentType = newContentType == null ? CONTENT_TYPE_EDEFAULT : newContentType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WebAppPackage.SERVER_COMPONENT__CONTENT_TYPE, oldContentType, contentType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public String getURL() {
		StringBuilder sb = new StringBuilder();
		if(!(deployPath==null || deployPath.length()==0)) {
			sb.append(deployPath);
			sb.append('/');
		}
		sb.append(name);
		sb.append(".jsp");
		return sb.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case WebAppPackage.SERVER_COMPONENT__DEPLOY_PATH:
				return getDeployPath();
			case WebAppPackage.SERVER_COMPONENT__IMPORTS:
				return getImports();
			case WebAppPackage.SERVER_COMPONENT__CONTENT_TYPE:
				return getContentType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case WebAppPackage.SERVER_COMPONENT__DEPLOY_PATH:
				setDeployPath((String)newValue);
				return;
			case WebAppPackage.SERVER_COMPONENT__IMPORTS:
				getImports().clear();
				getImports().addAll((Collection<? extends String>)newValue);
				return;
			case WebAppPackage.SERVER_COMPONENT__CONTENT_TYPE:
				setContentType((PageContentType)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case WebAppPackage.SERVER_COMPONENT__DEPLOY_PATH:
				setDeployPath(DEPLOY_PATH_EDEFAULT);
				return;
			case WebAppPackage.SERVER_COMPONENT__IMPORTS:
				getImports().clear();
				return;
			case WebAppPackage.SERVER_COMPONENT__CONTENT_TYPE:
				setContentType(CONTENT_TYPE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case WebAppPackage.SERVER_COMPONENT__DEPLOY_PATH:
				return DEPLOY_PATH_EDEFAULT == null ? deployPath != null : !DEPLOY_PATH_EDEFAULT.equals(deployPath);
			case WebAppPackage.SERVER_COMPONENT__IMPORTS:
				return imports != null && !imports.isEmpty();
			case WebAppPackage.SERVER_COMPONENT__CONTENT_TYPE:
				return contentType != CONTENT_TYPE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == Deployable.class) {
			switch (derivedFeatureID) {
				case WebAppPackage.SERVER_COMPONENT__DEPLOY_PATH: return WebAppPackage.DEPLOYABLE__DEPLOY_PATH;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == Deployable.class) {
			switch (baseFeatureID) {
				case WebAppPackage.DEPLOYABLE__DEPLOY_PATH: return WebAppPackage.SERVER_COMPONENT__DEPLOY_PATH;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedOperationID(int baseOperationID, Class<?> baseClass) {
		if (baseClass == Deployable.class) {
			switch (baseOperationID) {
				case WebAppPackage.DEPLOYABLE___GET_URL: return WebAppPackage.SERVER_COMPONENT___GET_URL;
				default: return -1;
			}
		}
		return super.eDerivedOperationID(baseOperationID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case WebAppPackage.SERVER_COMPONENT___GET_URL:
				return getURL();
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (deployPath: ");
		result.append(deployPath);
		result.append(", imports: ");
		result.append(imports);
		result.append(", contentType: ");
		result.append(contentType);
		result.append(')');
		return result.toString();
	}

} //ServerComponentImpl
