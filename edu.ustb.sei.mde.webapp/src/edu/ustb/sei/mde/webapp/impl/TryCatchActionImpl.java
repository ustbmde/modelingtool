/**
 */
package edu.ustb.sei.mde.webapp.impl;

import edu.ustb.sei.mde.webapp.Action;
import edu.ustb.sei.mde.webapp.CatchPart;
import edu.ustb.sei.mde.webapp.TryCatchAction;
import edu.ustb.sei.mde.webapp.WebAppPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Try Catch Action</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.webapp.impl.TryCatchActionImpl#getTryPart <em>Try Part</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.impl.TryCatchActionImpl#getCatchParts <em>Catch Parts</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.impl.TryCatchActionImpl#getFinalPart <em>Final Part</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class TryCatchActionImpl extends ActionImpl implements TryCatchAction {
	/**
	 * The cached value of the '{@link #getTryPart() <em>Try Part</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTryPart()
	 * @generated
	 * @ordered
	 */
	protected Action tryPart;

	/**
	 * The cached value of the '{@link #getCatchParts() <em>Catch Parts</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCatchParts()
	 * @generated
	 * @ordered
	 */
	protected EList<CatchPart> catchParts;

	/**
	 * The cached value of the '{@link #getFinalPart() <em>Final Part</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFinalPart()
	 * @generated
	 * @ordered
	 */
	protected Action finalPart;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TryCatchActionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WebAppPackage.Literals.TRY_CATCH_ACTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Action getTryPart() {
		return tryPart;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTryPart(Action newTryPart, NotificationChain msgs) {
		Action oldTryPart = tryPart;
		tryPart = newTryPart;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, WebAppPackage.TRY_CATCH_ACTION__TRY_PART, oldTryPart, newTryPart);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTryPart(Action newTryPart) {
		if (newTryPart != tryPart) {
			NotificationChain msgs = null;
			if (tryPart != null)
				msgs = ((InternalEObject)tryPart).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - WebAppPackage.TRY_CATCH_ACTION__TRY_PART, null, msgs);
			if (newTryPart != null)
				msgs = ((InternalEObject)newTryPart).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - WebAppPackage.TRY_CATCH_ACTION__TRY_PART, null, msgs);
			msgs = basicSetTryPart(newTryPart, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WebAppPackage.TRY_CATCH_ACTION__TRY_PART, newTryPart, newTryPart));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<CatchPart> getCatchParts() {
		if (catchParts == null) {
			catchParts = new EObjectContainmentEList<CatchPart>(CatchPart.class, this, WebAppPackage.TRY_CATCH_ACTION__CATCH_PARTS);
		}
		return catchParts;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Action getFinalPart() {
		return finalPart;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFinalPart(Action newFinalPart, NotificationChain msgs) {
		Action oldFinalPart = finalPart;
		finalPart = newFinalPart;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, WebAppPackage.TRY_CATCH_ACTION__FINAL_PART, oldFinalPart, newFinalPart);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFinalPart(Action newFinalPart) {
		if (newFinalPart != finalPart) {
			NotificationChain msgs = null;
			if (finalPart != null)
				msgs = ((InternalEObject)finalPart).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - WebAppPackage.TRY_CATCH_ACTION__FINAL_PART, null, msgs);
			if (newFinalPart != null)
				msgs = ((InternalEObject)newFinalPart).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - WebAppPackage.TRY_CATCH_ACTION__FINAL_PART, null, msgs);
			msgs = basicSetFinalPart(newFinalPart, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WebAppPackage.TRY_CATCH_ACTION__FINAL_PART, newFinalPart, newFinalPart));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case WebAppPackage.TRY_CATCH_ACTION__TRY_PART:
				return basicSetTryPart(null, msgs);
			case WebAppPackage.TRY_CATCH_ACTION__CATCH_PARTS:
				return ((InternalEList<?>)getCatchParts()).basicRemove(otherEnd, msgs);
			case WebAppPackage.TRY_CATCH_ACTION__FINAL_PART:
				return basicSetFinalPart(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case WebAppPackage.TRY_CATCH_ACTION__TRY_PART:
				return getTryPart();
			case WebAppPackage.TRY_CATCH_ACTION__CATCH_PARTS:
				return getCatchParts();
			case WebAppPackage.TRY_CATCH_ACTION__FINAL_PART:
				return getFinalPart();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case WebAppPackage.TRY_CATCH_ACTION__TRY_PART:
				setTryPart((Action)newValue);
				return;
			case WebAppPackage.TRY_CATCH_ACTION__CATCH_PARTS:
				getCatchParts().clear();
				getCatchParts().addAll((Collection<? extends CatchPart>)newValue);
				return;
			case WebAppPackage.TRY_CATCH_ACTION__FINAL_PART:
				setFinalPart((Action)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case WebAppPackage.TRY_CATCH_ACTION__TRY_PART:
				setTryPart((Action)null);
				return;
			case WebAppPackage.TRY_CATCH_ACTION__CATCH_PARTS:
				getCatchParts().clear();
				return;
			case WebAppPackage.TRY_CATCH_ACTION__FINAL_PART:
				setFinalPart((Action)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case WebAppPackage.TRY_CATCH_ACTION__TRY_PART:
				return tryPart != null;
			case WebAppPackage.TRY_CATCH_ACTION__CATCH_PARTS:
				return catchParts != null && !catchParts.isEmpty();
			case WebAppPackage.TRY_CATCH_ACTION__FINAL_PART:
				return finalPart != null;
		}
		return super.eIsSet(featureID);
	}

} //TryCatchActionImpl
