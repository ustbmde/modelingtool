/**
 */
package edu.ustb.sei.mde.webapp.impl;

import edu.ustb.sei.mde.webapp.OperationDeclaration;
import edu.ustb.sei.mde.webapp.WebAppPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Operation Declaration</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class OperationDeclarationImpl extends OperationImpl implements OperationDeclaration {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OperationDeclarationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WebAppPackage.Literals.OPERATION_DECLARATION;
	}

} //OperationDeclarationImpl
