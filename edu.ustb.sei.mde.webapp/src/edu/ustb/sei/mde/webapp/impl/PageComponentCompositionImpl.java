/**
 */
package edu.ustb.sei.mde.webapp.impl;

import edu.ustb.sei.mde.webapp.PageComponentComposition;
import edu.ustb.sei.mde.webapp.WebAppPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Page Component Composition</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.webapp.impl.PageComponentCompositionImpl#getTime <em>Time</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.impl.PageComponentCompositionImpl#getContainerSelector <em>Container Selector</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class PageComponentCompositionImpl extends CompositionImpl implements PageComponentComposition {
	/**
	 * The default value of the '{@link #getTime() <em>Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTime()
	 * @generated
	 * @ordered
	 */
	protected static final String TIME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTime() <em>Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTime()
	 * @generated
	 * @ordered
	 */
	protected String time = TIME_EDEFAULT;

	/**
	 * The default value of the '{@link #getContainerSelector() <em>Container Selector</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContainerSelector()
	 * @generated
	 * @ordered
	 */
	protected static final String CONTAINER_SELECTOR_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getContainerSelector() <em>Container Selector</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContainerSelector()
	 * @generated
	 * @ordered
	 */
	protected String containerSelector = CONTAINER_SELECTOR_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PageComponentCompositionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WebAppPackage.Literals.PAGE_COMPONENT_COMPOSITION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getTime() {
		return time;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTime(String newTime) {
		String oldTime = time;
		time = newTime;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WebAppPackage.PAGE_COMPONENT_COMPOSITION__TIME, oldTime, time));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getContainerSelector() {
		return containerSelector;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setContainerSelector(String newContainerSelector) {
		String oldContainerSelector = containerSelector;
		containerSelector = newContainerSelector;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WebAppPackage.PAGE_COMPONENT_COMPOSITION__CONTAINER_SELECTOR, oldContainerSelector, containerSelector));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case WebAppPackage.PAGE_COMPONENT_COMPOSITION__TIME:
				return getTime();
			case WebAppPackage.PAGE_COMPONENT_COMPOSITION__CONTAINER_SELECTOR:
				return getContainerSelector();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case WebAppPackage.PAGE_COMPONENT_COMPOSITION__TIME:
				setTime((String)newValue);
				return;
			case WebAppPackage.PAGE_COMPONENT_COMPOSITION__CONTAINER_SELECTOR:
				setContainerSelector((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case WebAppPackage.PAGE_COMPONENT_COMPOSITION__TIME:
				setTime(TIME_EDEFAULT);
				return;
			case WebAppPackage.PAGE_COMPONENT_COMPOSITION__CONTAINER_SELECTOR:
				setContainerSelector(CONTAINER_SELECTOR_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case WebAppPackage.PAGE_COMPONENT_COMPOSITION__TIME:
				return TIME_EDEFAULT == null ? time != null : !TIME_EDEFAULT.equals(time);
			case WebAppPackage.PAGE_COMPONENT_COMPOSITION__CONTAINER_SELECTOR:
				return CONTAINER_SELECTOR_EDEFAULT == null ? containerSelector != null : !CONTAINER_SELECTOR_EDEFAULT.equals(containerSelector);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (time: ");
		result.append(time);
		result.append(", containerSelector: ");
		result.append(containerSelector);
		result.append(')');
		return result.toString();
	}

} //PageComponentCompositionImpl
