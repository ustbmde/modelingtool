/**
 */
package edu.ustb.sei.mde.webapp.impl;

import edu.ustb.sei.mde.webapp.Action;
import edu.ustb.sei.mde.webapp.IfAction;
import edu.ustb.sei.mde.webapp.WebAppPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>If Action</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.webapp.impl.IfActionImpl#getConditionCode <em>Condition Code</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.impl.IfActionImpl#getThenAction <em>Then Action</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.impl.IfActionImpl#getElseAction <em>Else Action</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class IfActionImpl extends ActionImpl implements IfAction {
	/**
	 * The default value of the '{@link #getConditionCode() <em>Condition Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConditionCode()
	 * @generated
	 * @ordered
	 */
	protected static final String CONDITION_CODE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getConditionCode() <em>Condition Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConditionCode()
	 * @generated
	 * @ordered
	 */
	protected String conditionCode = CONDITION_CODE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getThenAction() <em>Then Action</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getThenAction()
	 * @generated
	 * @ordered
	 */
	protected Action thenAction;

	/**
	 * The cached value of the '{@link #getElseAction() <em>Else Action</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElseAction()
	 * @generated
	 * @ordered
	 */
	protected Action elseAction;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IfActionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return WebAppPackage.Literals.IF_ACTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getConditionCode() {
		return conditionCode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConditionCode(String newConditionCode) {
		String oldConditionCode = conditionCode;
		conditionCode = newConditionCode;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WebAppPackage.IF_ACTION__CONDITION_CODE, oldConditionCode, conditionCode));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Action getThenAction() {
		return thenAction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetThenAction(Action newThenAction, NotificationChain msgs) {
		Action oldThenAction = thenAction;
		thenAction = newThenAction;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, WebAppPackage.IF_ACTION__THEN_ACTION, oldThenAction, newThenAction);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setThenAction(Action newThenAction) {
		if (newThenAction != thenAction) {
			NotificationChain msgs = null;
			if (thenAction != null)
				msgs = ((InternalEObject)thenAction).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - WebAppPackage.IF_ACTION__THEN_ACTION, null, msgs);
			if (newThenAction != null)
				msgs = ((InternalEObject)newThenAction).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - WebAppPackage.IF_ACTION__THEN_ACTION, null, msgs);
			msgs = basicSetThenAction(newThenAction, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WebAppPackage.IF_ACTION__THEN_ACTION, newThenAction, newThenAction));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Action getElseAction() {
		return elseAction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetElseAction(Action newElseAction, NotificationChain msgs) {
		Action oldElseAction = elseAction;
		elseAction = newElseAction;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, WebAppPackage.IF_ACTION__ELSE_ACTION, oldElseAction, newElseAction);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setElseAction(Action newElseAction) {
		if (newElseAction != elseAction) {
			NotificationChain msgs = null;
			if (elseAction != null)
				msgs = ((InternalEObject)elseAction).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - WebAppPackage.IF_ACTION__ELSE_ACTION, null, msgs);
			if (newElseAction != null)
				msgs = ((InternalEObject)newElseAction).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - WebAppPackage.IF_ACTION__ELSE_ACTION, null, msgs);
			msgs = basicSetElseAction(newElseAction, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, WebAppPackage.IF_ACTION__ELSE_ACTION, newElseAction, newElseAction));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case WebAppPackage.IF_ACTION__THEN_ACTION:
				return basicSetThenAction(null, msgs);
			case WebAppPackage.IF_ACTION__ELSE_ACTION:
				return basicSetElseAction(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case WebAppPackage.IF_ACTION__CONDITION_CODE:
				return getConditionCode();
			case WebAppPackage.IF_ACTION__THEN_ACTION:
				return getThenAction();
			case WebAppPackage.IF_ACTION__ELSE_ACTION:
				return getElseAction();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case WebAppPackage.IF_ACTION__CONDITION_CODE:
				setConditionCode((String)newValue);
				return;
			case WebAppPackage.IF_ACTION__THEN_ACTION:
				setThenAction((Action)newValue);
				return;
			case WebAppPackage.IF_ACTION__ELSE_ACTION:
				setElseAction((Action)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case WebAppPackage.IF_ACTION__CONDITION_CODE:
				setConditionCode(CONDITION_CODE_EDEFAULT);
				return;
			case WebAppPackage.IF_ACTION__THEN_ACTION:
				setThenAction((Action)null);
				return;
			case WebAppPackage.IF_ACTION__ELSE_ACTION:
				setElseAction((Action)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case WebAppPackage.IF_ACTION__CONDITION_CODE:
				return CONDITION_CODE_EDEFAULT == null ? conditionCode != null : !CONDITION_CODE_EDEFAULT.equals(conditionCode);
			case WebAppPackage.IF_ACTION__THEN_ACTION:
				return thenAction != null;
			case WebAppPackage.IF_ACTION__ELSE_ACTION:
				return elseAction != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (conditionCode: ");
		result.append(conditionCode);
		result.append(')');
		return result.toString();
	}

} //IfActionImpl
