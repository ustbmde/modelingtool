/**
 */
package edu.ustb.sei.mde.webapp;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Server Component</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.webapp.ServerComponent#getImports <em>Imports</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.ServerComponent#getContentType <em>Content Type</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getServerComponent()
 * @model
 * @generated
 */
public interface ServerComponent extends Component, Deployable {
	/**
	 * Returns the value of the '<em><b>Imports</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Imports</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Imports</em>' attribute list.
	 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getServerComponent_Imports()
	 * @model
	 * @generated
	 */
	EList<String> getImports();

	/**
	 * Returns the value of the '<em><b>Content Type</b></em>' attribute.
	 * The literals are from the enumeration {@link edu.ustb.sei.mde.webapp.PageContentType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Content Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Content Type</em>' attribute.
	 * @see edu.ustb.sei.mde.webapp.PageContentType
	 * @see #setContentType(PageContentType)
	 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getServerComponent_ContentType()
	 * @model
	 * @generated
	 */
	PageContentType getContentType();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.webapp.ServerComponent#getContentType <em>Content Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Content Type</em>' attribute.
	 * @see edu.ustb.sei.mde.webapp.PageContentType
	 * @see #getContentType()
	 * @generated
	 */
	void setContentType(PageContentType value);

} // ServerComponent
