/**
 */
package edu.ustb.sei.mde.webapp;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Code Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.webapp.CodeOperation#getBodyCode <em>Body Code</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getCodeOperation()
 * @model
 * @generated
 */
public interface CodeOperation extends OperationRealization {
	/**
	 * Returns the value of the '<em><b>Body Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Body Code</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Body Code</em>' attribute.
	 * @see #setBodyCode(String)
	 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getCodeOperation_BodyCode()
	 * @model
	 * @generated
	 */
	String getBodyCode();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.webapp.CodeOperation#getBodyCode <em>Body Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Body Code</em>' attribute.
	 * @see #getBodyCode()
	 * @generated
	 */
	void setBodyCode(String value);

} // CodeOperation
