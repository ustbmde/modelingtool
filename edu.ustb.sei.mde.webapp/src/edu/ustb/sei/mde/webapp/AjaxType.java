/**
 */
package edu.ustb.sei.mde.webapp;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Ajax Type</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getAjaxType()
 * @model
 * @generated
 */
public enum AjaxType implements Enumerator {
	/**
	 * The '<em><b>Ajax Load</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #AJAX_LOAD_VALUE
	 * @generated
	 * @ordered
	 */
	AJAX_LOAD(0, "ajaxLoad", "ajaxLoad"),

	/**
	 * The '<em><b>Ajax Get</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #AJAX_GET_VALUE
	 * @generated
	 * @ordered
	 */
	AJAX_GET(1, "ajaxGet", "ajaxGet"),

	/**
	 * The '<em><b>Ajax Post</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #AJAX_POST_VALUE
	 * @generated
	 * @ordered
	 */
	AJAX_POST(2, "ajaxPost", "ajaxPost");

	/**
	 * The '<em><b>Ajax Load</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Ajax Load</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #AJAX_LOAD
	 * @model name="ajaxLoad"
	 * @generated
	 * @ordered
	 */
	public static final int AJAX_LOAD_VALUE = 0;

	/**
	 * The '<em><b>Ajax Get</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Ajax Get</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #AJAX_GET
	 * @model name="ajaxGet"
	 * @generated
	 * @ordered
	 */
	public static final int AJAX_GET_VALUE = 1;

	/**
	 * The '<em><b>Ajax Post</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Ajax Post</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #AJAX_POST
	 * @model name="ajaxPost"
	 * @generated
	 * @ordered
	 */
	public static final int AJAX_POST_VALUE = 2;

	/**
	 * An array of all the '<em><b>Ajax Type</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final AjaxType[] VALUES_ARRAY =
		new AjaxType[] {
			AJAX_LOAD,
			AJAX_GET,
			AJAX_POST,
		};

	/**
	 * A public read-only list of all the '<em><b>Ajax Type</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<AjaxType> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Ajax Type</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static AjaxType get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			AjaxType result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Ajax Type</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static AjaxType getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			AjaxType result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Ajax Type</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static AjaxType get(int value) {
		switch (value) {
			case AJAX_LOAD_VALUE: return AJAX_LOAD;
			case AJAX_GET_VALUE: return AJAX_GET;
			case AJAX_POST_VALUE: return AJAX_POST;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private AjaxType(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //AjaxType
