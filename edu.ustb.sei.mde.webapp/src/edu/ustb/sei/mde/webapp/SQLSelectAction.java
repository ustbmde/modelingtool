/**
 */
package edu.ustb.sei.mde.webapp;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>SQL Select Action</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.webapp.SQLSelectAction#getTables <em>Tables</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.SQLSelectAction#getColumns <em>Columns</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.webapp.SQLSelectAction#getConditionCode <em>Condition Code</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getSQLSelectAction()
 * @model
 * @generated
 */
public interface SQLSelectAction extends SQLAction {
	/**
	 * Returns the value of the '<em><b>Tables</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Tables</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tables</em>' attribute list.
	 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getSQLSelectAction_Tables()
	 * @model required="true"
	 * @generated
	 */
	EList<String> getTables();

	/**
	 * Returns the value of the '<em><b>Columns</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Columns</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Columns</em>' attribute list.
	 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getSQLSelectAction_Columns()
	 * @model
	 * @generated
	 */
	EList<String> getColumns();

	/**
	 * Returns the value of the '<em><b>Condition Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Condition Code</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Condition Code</em>' attribute.
	 * @see #setConditionCode(String)
	 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getSQLSelectAction_ConditionCode()
	 * @model
	 * @generated
	 */
	String getConditionCode();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.webapp.SQLSelectAction#getConditionCode <em>Condition Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Condition Code</em>' attribute.
	 * @see #getConditionCode()
	 * @generated
	 */
	void setConditionCode(String value);

} // SQLSelectAction
