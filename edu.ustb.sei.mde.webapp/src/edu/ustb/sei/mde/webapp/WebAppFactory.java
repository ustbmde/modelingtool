/**
 */
package edu.ustb.sei.mde.webapp;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see edu.ustb.sei.mde.webapp.WebAppPackage
 * @generated
 */
public interface WebAppFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	WebAppFactory eINSTANCE = edu.ustb.sei.mde.webapp.impl.WebAppFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Interface</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Interface</em>'.
	 * @generated
	 */
	Interface createInterface();

	/**
	 * Returns a new object of class '<em>Operation Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Operation Declaration</em>'.
	 * @generated
	 */
	OperationDeclaration createOperationDeclaration();

	/**
	 * Returns a new object of class '<em>Parameter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Parameter</em>'.
	 * @generated
	 */
	Parameter createParameter();

	/**
	 * Returns a new object of class '<em>Code Operation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Code Operation</em>'.
	 * @generated
	 */
	CodeOperation createCodeOperation();

	/**
	 * Returns a new object of class '<em>Action Operation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Action Operation</em>'.
	 * @generated
	 */
	ActionOperation createActionOperation();

	/**
	 * Returns a new object of class '<em>Page Component</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Page Component</em>'.
	 * @generated
	 */
	PageComponent createPageComponent();

	/**
	 * Returns a new object of class '<em>HTML Node</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>HTML Node</em>'.
	 * @generated
	 */
	HTMLNode createHTMLNode();

	/**
	 * Returns a new object of class '<em>Composition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Composition</em>'.
	 * @generated
	 */
	Composition createComposition();

	/**
	 * Returns a new object of class '<em>Page Component Composition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Page Component Composition</em>'.
	 * @generated
	 */
	PageComponentComposition createPageComponentComposition();

	/**
	 * Returns a new object of class '<em>Event Handler</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Event Handler</em>'.
	 * @generated
	 */
	EventHandler createEventHandler();

	/**
	 * Returns a new object of class '<em>Server Component</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Server Component</em>'.
	 * @generated
	 */
	ServerComponent createServerComponent();

	/**
	 * Returns a new object of class '<em>Class Component</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Class Component</em>'.
	 * @generated
	 */
	ClassComponent createClassComponent();

	/**
	 * Returns a new object of class '<em>Database</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Database</em>'.
	 * @generated
	 */
	Database createDatabase();

	/**
	 * Returns a new object of class '<em>Code Action</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Code Action</em>'.
	 * @generated
	 */
	CodeAction createCodeAction();

	/**
	 * Returns a new object of class '<em>Call Action</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Call Action</em>'.
	 * @generated
	 */
	CallAction createCallAction();

	/**
	 * Returns a new object of class '<em>Let Action</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Let Action</em>'.
	 * @generated
	 */
	LetAction createLetAction();

	/**
	 * Returns a new object of class '<em>For Action</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>For Action</em>'.
	 * @generated
	 */
	ForAction createForAction();

	/**
	 * Returns a new object of class '<em>If Action</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>If Action</em>'.
	 * @generated
	 */
	IfAction createIfAction();

	/**
	 * Returns a new object of class '<em>Sequence Action</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Sequence Action</em>'.
	 * @generated
	 */
	SequenceAction createSequenceAction();

	/**
	 * Returns a new object of class '<em>Try Catch Action</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Try Catch Action</em>'.
	 * @generated
	 */
	TryCatchAction createTryCatchAction();

	/**
	 * Returns a new object of class '<em>Catch Part</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Catch Part</em>'.
	 * @generated
	 */
	CatchPart createCatchPart();

	/**
	 * Returns a new object of class '<em>JQuery Ajax Action</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>JQuery Ajax Action</em>'.
	 * @generated
	 */
	JQueryAjaxAction createJQueryAjaxAction();

	/**
	 * Returns a new object of class '<em>JQuery Event Action</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>JQuery Event Action</em>'.
	 * @generated
	 */
	JQueryEventAction createJQueryEventAction();

	/**
	 * Returns a new object of class '<em>SQL Select Action</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>SQL Select Action</em>'.
	 * @generated
	 */
	SQLSelectAction createSQLSelectAction();

	/**
	 * Returns a new object of class '<em>SQL Insert Action</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>SQL Insert Action</em>'.
	 * @generated
	 */
	SQLInsertAction createSQLInsertAction();

	/**
	 * Returns a new object of class '<em>SQL Update Action</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>SQL Update Action</em>'.
	 * @generated
	 */
	SQLUpdateAction createSQLUpdateAction();

	/**
	 * Returns a new object of class '<em>SQL Delete Action</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>SQL Delete Action</em>'.
	 * @generated
	 */
	SQLDeleteAction createSQLDeleteAction();

	/**
	 * Returns a new object of class '<em>SQL Execution Action</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>SQL Execution Action</em>'.
	 * @generated
	 */
	SQLExecutionAction createSQLExecutionAction();

	/**
	 * Returns a new object of class '<em>SQL Result Set Iteration Action</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>SQL Result Set Iteration Action</em>'.
	 * @generated
	 */
	SQLResultSetIterationAction createSQLResultSetIterationAction();

	/**
	 * Returns a new object of class '<em>Server Page Template Action</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Server Page Template Action</em>'.
	 * @generated
	 */
	ServerPageTemplateAction createServerPageTemplateAction();

	/**
	 * Returns a new object of class '<em>Server Page Expression Action</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Server Page Expression Action</em>'.
	 * @generated
	 */
	ServerPageExpressionAction createServerPageExpressionAction();

	/**
	 * Returns a new object of class '<em>Server Page Structured Template Action</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Server Page Structured Template Action</em>'.
	 * @generated
	 */
	ServerPageStructuredTemplateAction createServerPageStructuredTemplateAction();

	/**
	 * Returns a new object of class '<em>Web Application</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Web Application</em>'.
	 * @generated
	 */
	WebApplication createWebApplication();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	WebAppPackage getWebAppPackage();

} //WebAppFactory
