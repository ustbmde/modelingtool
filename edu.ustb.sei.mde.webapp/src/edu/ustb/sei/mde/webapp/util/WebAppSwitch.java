/**
 */
package edu.ustb.sei.mde.webapp.util;

import edu.ustb.sei.mde.webapp.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see edu.ustb.sei.mde.webapp.WebAppPackage
 * @generated
 */
public class WebAppSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static WebAppPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WebAppSwitch() {
		if (modelPackage == null) {
			modelPackage = WebAppPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @parameter ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case WebAppPackage.NAMED_ELEMENT: {
				NamedElement namedElement = (NamedElement)theEObject;
				T result = caseNamedElement(namedElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WebAppPackage.TYPED_ELEMENT: {
				TypedElement typedElement = (TypedElement)theEObject;
				T result = caseTypedElement(typedElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WebAppPackage.DEPLOYABLE: {
				Deployable deployable = (Deployable)theEObject;
				T result = caseDeployable(deployable);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WebAppPackage.DEPLOYABLE_POINTER: {
				DeployablePointer deployablePointer = (DeployablePointer)theEObject;
				T result = caseDeployablePointer(deployablePointer);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WebAppPackage.INTERFACE: {
				Interface interface_ = (Interface)theEObject;
				T result = caseInterface(interface_);
				if (result == null) result = caseNamedElement(interface_);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WebAppPackage.OPERATION: {
				Operation operation = (Operation)theEObject;
				T result = caseOperation(operation);
				if (result == null) result = caseNamedElement(operation);
				if (result == null) result = caseTypedElement(operation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WebAppPackage.OPERATION_DECLARATION: {
				OperationDeclaration operationDeclaration = (OperationDeclaration)theEObject;
				T result = caseOperationDeclaration(operationDeclaration);
				if (result == null) result = caseOperation(operationDeclaration);
				if (result == null) result = caseNamedElement(operationDeclaration);
				if (result == null) result = caseTypedElement(operationDeclaration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WebAppPackage.PARAMETER: {
				Parameter parameter = (Parameter)theEObject;
				T result = caseParameter(parameter);
				if (result == null) result = caseNamedElement(parameter);
				if (result == null) result = caseTypedElement(parameter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WebAppPackage.COMPONENT: {
				Component component = (Component)theEObject;
				T result = caseComponent(component);
				if (result == null) result = caseNamedElement(component);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WebAppPackage.OPERATION_REALIZATION: {
				OperationRealization operationRealization = (OperationRealization)theEObject;
				T result = caseOperationRealization(operationRealization);
				if (result == null) result = caseOperation(operationRealization);
				if (result == null) result = caseNamedElement(operationRealization);
				if (result == null) result = caseTypedElement(operationRealization);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WebAppPackage.CODE_OPERATION: {
				CodeOperation codeOperation = (CodeOperation)theEObject;
				T result = caseCodeOperation(codeOperation);
				if (result == null) result = caseOperationRealization(codeOperation);
				if (result == null) result = caseOperation(codeOperation);
				if (result == null) result = caseNamedElement(codeOperation);
				if (result == null) result = caseTypedElement(codeOperation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WebAppPackage.ACTION_OPERATION: {
				ActionOperation actionOperation = (ActionOperation)theEObject;
				T result = caseActionOperation(actionOperation);
				if (result == null) result = caseOperationRealization(actionOperation);
				if (result == null) result = caseStatementContainer(actionOperation);
				if (result == null) result = caseOperation(actionOperation);
				if (result == null) result = caseNamedElement(actionOperation);
				if (result == null) result = caseTypedElement(actionOperation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WebAppPackage.ACTION: {
				Action action = (Action)theEObject;
				T result = caseAction(action);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WebAppPackage.PAGE_COMPONENT: {
				PageComponent pageComponent = (PageComponent)theEObject;
				T result = casePageComponent(pageComponent);
				if (result == null) result = caseComponent(pageComponent);
				if (result == null) result = caseDeployable(pageComponent);
				if (result == null) result = caseNamedElement(pageComponent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WebAppPackage.HTML_NODE: {
				HTMLNode htmlNode = (HTMLNode)theEObject;
				T result = caseHTMLNode(htmlNode);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WebAppPackage.COMPOSITION: {
				Composition composition = (Composition)theEObject;
				T result = caseComposition(composition);
				if (result == null) result = caseDeployablePointer(composition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WebAppPackage.PAGE_COMPONENT_COMPOSITION: {
				PageComponentComposition pageComponentComposition = (PageComponentComposition)theEObject;
				T result = casePageComponentComposition(pageComponentComposition);
				if (result == null) result = caseComposition(pageComponentComposition);
				if (result == null) result = caseDeployablePointer(pageComponentComposition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WebAppPackage.EVENT_HANDLER: {
				EventHandler eventHandler = (EventHandler)theEObject;
				T result = caseEventHandler(eventHandler);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WebAppPackage.SERVER_COMPONENT: {
				ServerComponent serverComponent = (ServerComponent)theEObject;
				T result = caseServerComponent(serverComponent);
				if (result == null) result = caseComponent(serverComponent);
				if (result == null) result = caseDeployable(serverComponent);
				if (result == null) result = caseNamedElement(serverComponent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WebAppPackage.CLASS_COMPONENT: {
				ClassComponent classComponent = (ClassComponent)theEObject;
				T result = caseClassComponent(classComponent);
				if (result == null) result = caseComponent(classComponent);
				if (result == null) result = caseNamedElement(classComponent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WebAppPackage.STATEMENT_CONTAINER: {
				StatementContainer statementContainer = (StatementContainer)theEObject;
				T result = caseStatementContainer(statementContainer);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WebAppPackage.DATABASE: {
				Database database = (Database)theEObject;
				T result = caseDatabase(database);
				if (result == null) result = caseNamedElement(database);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WebAppPackage.CODE_ACTION: {
				CodeAction codeAction = (CodeAction)theEObject;
				T result = caseCodeAction(codeAction);
				if (result == null) result = caseAction(codeAction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WebAppPackage.CALL_ACTION: {
				CallAction callAction = (CallAction)theEObject;
				T result = caseCallAction(callAction);
				if (result == null) result = caseAction(callAction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WebAppPackage.LET_ACTION: {
				LetAction letAction = (LetAction)theEObject;
				T result = caseLetAction(letAction);
				if (result == null) result = caseAction(letAction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WebAppPackage.FOR_ACTION: {
				ForAction forAction = (ForAction)theEObject;
				T result = caseForAction(forAction);
				if (result == null) result = caseAction(forAction);
				if (result == null) result = caseStatementContainer(forAction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WebAppPackage.IF_ACTION: {
				IfAction ifAction = (IfAction)theEObject;
				T result = caseIfAction(ifAction);
				if (result == null) result = caseAction(ifAction);
				if (result == null) result = caseStatementContainer(ifAction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WebAppPackage.SEQUENCE_ACTION: {
				SequenceAction sequenceAction = (SequenceAction)theEObject;
				T result = caseSequenceAction(sequenceAction);
				if (result == null) result = caseAction(sequenceAction);
				if (result == null) result = caseStatementContainer(sequenceAction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WebAppPackage.TRY_CATCH_ACTION: {
				TryCatchAction tryCatchAction = (TryCatchAction)theEObject;
				T result = caseTryCatchAction(tryCatchAction);
				if (result == null) result = caseAction(tryCatchAction);
				if (result == null) result = caseStatementContainer(tryCatchAction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WebAppPackage.CATCH_PART: {
				CatchPart catchPart = (CatchPart)theEObject;
				T result = caseCatchPart(catchPart);
				if (result == null) result = caseStatementContainer(catchPart);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WebAppPackage.JAVASCRIPT_ACTION: {
				JavascriptAction javascriptAction = (JavascriptAction)theEObject;
				T result = caseJavascriptAction(javascriptAction);
				if (result == null) result = caseAction(javascriptAction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WebAppPackage.JQUERY_AJAX_ACTION: {
				JQueryAjaxAction jQueryAjaxAction = (JQueryAjaxAction)theEObject;
				T result = caseJQueryAjaxAction(jQueryAjaxAction);
				if (result == null) result = caseJavascriptAction(jQueryAjaxAction);
				if (result == null) result = caseDeployablePointer(jQueryAjaxAction);
				if (result == null) result = caseAction(jQueryAjaxAction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WebAppPackage.JQUERY_EVENT_ACTION: {
				JQueryEventAction jQueryEventAction = (JQueryEventAction)theEObject;
				T result = caseJQueryEventAction(jQueryEventAction);
				if (result == null) result = caseJavascriptAction(jQueryEventAction);
				if (result == null) result = caseAction(jQueryEventAction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WebAppPackage.SQL_ACTION: {
				SQLAction sqlAction = (SQLAction)theEObject;
				T result = caseSQLAction(sqlAction);
				if (result == null) result = caseAction(sqlAction);
				if (result == null) result = caseStatementContainer(sqlAction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WebAppPackage.SQL_SELECT_ACTION: {
				SQLSelectAction sqlSelectAction = (SQLSelectAction)theEObject;
				T result = caseSQLSelectAction(sqlSelectAction);
				if (result == null) result = caseSQLAction(sqlSelectAction);
				if (result == null) result = caseAction(sqlSelectAction);
				if (result == null) result = caseStatementContainer(sqlSelectAction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WebAppPackage.SQL_INSERT_ACTION: {
				SQLInsertAction sqlInsertAction = (SQLInsertAction)theEObject;
				T result = caseSQLInsertAction(sqlInsertAction);
				if (result == null) result = caseSQLAction(sqlInsertAction);
				if (result == null) result = caseAction(sqlInsertAction);
				if (result == null) result = caseStatementContainer(sqlInsertAction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WebAppPackage.SQL_UPDATE_ACTION: {
				SQLUpdateAction sqlUpdateAction = (SQLUpdateAction)theEObject;
				T result = caseSQLUpdateAction(sqlUpdateAction);
				if (result == null) result = caseSQLAction(sqlUpdateAction);
				if (result == null) result = caseAction(sqlUpdateAction);
				if (result == null) result = caseStatementContainer(sqlUpdateAction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WebAppPackage.SQL_DELETE_ACTION: {
				SQLDeleteAction sqlDeleteAction = (SQLDeleteAction)theEObject;
				T result = caseSQLDeleteAction(sqlDeleteAction);
				if (result == null) result = caseSQLAction(sqlDeleteAction);
				if (result == null) result = caseAction(sqlDeleteAction);
				if (result == null) result = caseStatementContainer(sqlDeleteAction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WebAppPackage.SQL_EXECUTION_ACTION: {
				SQLExecutionAction sqlExecutionAction = (SQLExecutionAction)theEObject;
				T result = caseSQLExecutionAction(sqlExecutionAction);
				if (result == null) result = caseSQLAction(sqlExecutionAction);
				if (result == null) result = caseAction(sqlExecutionAction);
				if (result == null) result = caseStatementContainer(sqlExecutionAction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WebAppPackage.SQL_RESULT_SET_ITERATION_ACTION: {
				SQLResultSetIterationAction sqlResultSetIterationAction = (SQLResultSetIterationAction)theEObject;
				T result = caseSQLResultSetIterationAction(sqlResultSetIterationAction);
				if (result == null) result = caseAction(sqlResultSetIterationAction);
				if (result == null) result = caseStatementContainer(sqlResultSetIterationAction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WebAppPackage.SERVER_PAGE_ACTION: {
				ServerPageAction serverPageAction = (ServerPageAction)theEObject;
				T result = caseServerPageAction(serverPageAction);
				if (result == null) result = caseAction(serverPageAction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WebAppPackage.SERVER_PAGE_TEMPLATE_ACTION: {
				ServerPageTemplateAction serverPageTemplateAction = (ServerPageTemplateAction)theEObject;
				T result = caseServerPageTemplateAction(serverPageTemplateAction);
				if (result == null) result = caseServerPageAction(serverPageTemplateAction);
				if (result == null) result = caseAction(serverPageTemplateAction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WebAppPackage.SERVER_PAGE_EXPRESSION_ACTION: {
				ServerPageExpressionAction serverPageExpressionAction = (ServerPageExpressionAction)theEObject;
				T result = caseServerPageExpressionAction(serverPageExpressionAction);
				if (result == null) result = caseServerPageAction(serverPageExpressionAction);
				if (result == null) result = caseAction(serverPageExpressionAction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WebAppPackage.SERVER_PAGE_STRUCTURED_TEMPLATE_ACTION: {
				ServerPageStructuredTemplateAction serverPageStructuredTemplateAction = (ServerPageStructuredTemplateAction)theEObject;
				T result = caseServerPageStructuredTemplateAction(serverPageStructuredTemplateAction);
				if (result == null) result = caseServerPageAction(serverPageStructuredTemplateAction);
				if (result == null) result = caseStatementContainer(serverPageStructuredTemplateAction);
				if (result == null) result = caseAction(serverPageStructuredTemplateAction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case WebAppPackage.WEB_APPLICATION: {
				WebApplication webApplication = (WebApplication)theEObject;
				T result = caseWebApplication(webApplication);
				if (result == null) result = caseNamedElement(webApplication);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Named Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Named Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNamedElement(NamedElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Typed Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Typed Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTypedElement(TypedElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Deployable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Deployable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDeployable(Deployable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Deployable Pointer</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Deployable Pointer</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDeployablePointer(DeployablePointer object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Interface</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Interface</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseInterface(Interface object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Operation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Operation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOperation(Operation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Operation Declaration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Operation Declaration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOperationDeclaration(OperationDeclaration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Parameter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Parameter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseParameter(Parameter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Component</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Component</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseComponent(Component object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Operation Realization</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Operation Realization</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOperationRealization(OperationRealization object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Code Operation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Code Operation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCodeOperation(CodeOperation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Action Operation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Action Operation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseActionOperation(ActionOperation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Action</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Action</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAction(Action object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Page Component</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Page Component</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePageComponent(PageComponent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>HTML Node</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>HTML Node</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHTMLNode(HTMLNode object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Composition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Composition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseComposition(Composition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Page Component Composition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Page Component Composition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePageComponentComposition(PageComponentComposition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Event Handler</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Event Handler</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEventHandler(EventHandler object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Server Component</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Server Component</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseServerComponent(ServerComponent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Class Component</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Class Component</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseClassComponent(ClassComponent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Statement Container</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Statement Container</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseStatementContainer(StatementContainer object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Database</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Database</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDatabase(Database object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Code Action</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Code Action</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCodeAction(CodeAction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Call Action</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Call Action</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCallAction(CallAction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Let Action</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Let Action</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLetAction(LetAction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>For Action</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>For Action</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseForAction(ForAction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>If Action</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>If Action</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseIfAction(IfAction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Sequence Action</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Sequence Action</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSequenceAction(SequenceAction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Try Catch Action</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Try Catch Action</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTryCatchAction(TryCatchAction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Catch Part</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Catch Part</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCatchPart(CatchPart object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Javascript Action</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Javascript Action</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseJavascriptAction(JavascriptAction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>JQuery Ajax Action</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>JQuery Ajax Action</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseJQueryAjaxAction(JQueryAjaxAction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>JQuery Event Action</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>JQuery Event Action</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseJQueryEventAction(JQueryEventAction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SQL Action</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SQL Action</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSQLAction(SQLAction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SQL Select Action</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SQL Select Action</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSQLSelectAction(SQLSelectAction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SQL Insert Action</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SQL Insert Action</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSQLInsertAction(SQLInsertAction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SQL Update Action</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SQL Update Action</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSQLUpdateAction(SQLUpdateAction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SQL Delete Action</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SQL Delete Action</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSQLDeleteAction(SQLDeleteAction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SQL Execution Action</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SQL Execution Action</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSQLExecutionAction(SQLExecutionAction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SQL Result Set Iteration Action</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SQL Result Set Iteration Action</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSQLResultSetIterationAction(SQLResultSetIterationAction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Server Page Action</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Server Page Action</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseServerPageAction(ServerPageAction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Server Page Template Action</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Server Page Template Action</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseServerPageTemplateAction(ServerPageTemplateAction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Server Page Expression Action</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Server Page Expression Action</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseServerPageExpressionAction(ServerPageExpressionAction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Server Page Structured Template Action</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Server Page Structured Template Action</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseServerPageStructuredTemplateAction(ServerPageStructuredTemplateAction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Web Application</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Web Application</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseWebApplication(WebApplication object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //WebAppSwitch
