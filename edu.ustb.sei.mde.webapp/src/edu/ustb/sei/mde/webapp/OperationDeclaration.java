/**
 */
package edu.ustb.sei.mde.webapp;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Operation Declaration</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.ustb.sei.mde.webapp.WebAppPackage#getOperationDeclaration()
 * @model
 * @generated
 */
public interface OperationDeclaration extends Operation {
} // OperationDeclaration
