/**
 */
package edu.ustb.sei.mde.mql.provider;


import edu.ustb.sei.mde.mql.AttributeNode;
import edu.ustb.sei.mde.mql.Direction;
import edu.ustb.sei.mde.mql.EntityNode;
import edu.ustb.sei.mde.mql.MQLPackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link edu.ustb.sei.mde.mql.AttributeNode} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class AttributeNodeItemProvider
	extends ItemProviderAdapter
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AttributeNodeItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addDirectionPropertyDescriptor(object);
			addAttributePropertyDescriptor(object);
			addValuePropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Attribute feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected void addAttributePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(new CustomedAttributeItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_AttributeNode_attribute_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_AttributeNode_attribute_feature", "_UI_AttributeNode_type"),
				 MQLPackage.Literals.ATTRIBUTE_NODE__ATTRIBUTE,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Value feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addValuePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_AttributeNode_value_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_AttributeNode_value_feature", "_UI_AttributeNode_type"),
				 MQLPackage.Literals.ATTRIBUTE_NODE__VALUE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Direction feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDirectionPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_AttributeNode_direction_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_AttributeNode_direction_feature", "_UI_AttributeNode_type"),
				 MQLPackage.Literals.ATTRIBUTE_NODE__DIRECTION,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This returns AttributeNode.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/AttributeNode"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		Direction labelValue = ((AttributeNode)object).getDirection();
		String label = labelValue == null ? null : labelValue.toString();
		return label == null || label.length() == 0 ?
			getString("_UI_AttributeNode_type") :
			getString("_UI_AttributeNode_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(AttributeNode.class)) {
			case MQLPackage.ATTRIBUTE_NODE__DIRECTION:
			case MQLPackage.ATTRIBUTE_NODE__VALUE:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return MQLEditPlugin.INSTANCE;
	}

}

class CustomedAttributeItemPropertyDescriptor extends ItemPropertyDescriptor {

	public CustomedAttributeItemPropertyDescriptor(
			AdapterFactory adapterFactory, ResourceLocator resourceLocator,
			String displayName, String description,
			EReference[] parentReferences, boolean isSettable, String category,
			String[] filterFlags) {
		super(adapterFactory, resourceLocator, displayName, description,
				parentReferences, isSettable, category, filterFlags);
		// TODO Auto-generated constructor stub
	}

	public CustomedAttributeItemPropertyDescriptor(
			AdapterFactory adapterFactory, ResourceLocator resourceLocator,
			String displayName, String description,
			EReference[] parentReferences, boolean isSettable, String category) {
		super(adapterFactory, resourceLocator, displayName, description,
				parentReferences, isSettable, category);
		// TODO Auto-generated constructor stub
	}

	public CustomedAttributeItemPropertyDescriptor(
			AdapterFactory adapterFactory, ResourceLocator resourceLocator,
			String displayName, String description,
			EReference[] parentReferences, boolean isSettable) {
		super(adapterFactory, resourceLocator, displayName, description,
				parentReferences, isSettable);
		// TODO Auto-generated constructor stub
	}

	public CustomedAttributeItemPropertyDescriptor(
			AdapterFactory adapterFactory, ResourceLocator resourceLocator,
			String displayName, String description,
			EReference[] parentReferences) {
		super(adapterFactory, resourceLocator, displayName, description,
				parentReferences);
		// TODO Auto-generated constructor stub
	}

	public CustomedAttributeItemPropertyDescriptor(
			AdapterFactory adapterFactory, ResourceLocator resourceLocator,
			String displayName, String description, EStructuralFeature feature,
			boolean isSettable, boolean multiLine, boolean sortChoices,
			Object staticImage, String category, String[] filterFlags) {
		super(adapterFactory, resourceLocator, displayName, description, feature,
				isSettable, multiLine, sortChoices, staticImage, category, filterFlags);
		// TODO Auto-generated constructor stub
	}

	public CustomedAttributeItemPropertyDescriptor(
			AdapterFactory adapterFactory, ResourceLocator resourceLocator,
			String displayName, String description, EStructuralFeature feature,
			boolean isSettable, Object staticImage, String category,
			String[] filterFlags) {
		super(adapterFactory, resourceLocator, displayName, description, feature,
				isSettable, staticImage, category, filterFlags);
		// TODO Auto-generated constructor stub
	}

	public CustomedAttributeItemPropertyDescriptor(
			AdapterFactory adapterFactory, ResourceLocator resourceLocator,
			String displayName, String description, EStructuralFeature feature,
			boolean isSettable, Object staticImage, String category) {
		super(adapterFactory, resourceLocator, displayName, description, feature,
				isSettable, staticImage, category);
		// TODO Auto-generated constructor stub
	}

	public CustomedAttributeItemPropertyDescriptor(
			AdapterFactory adapterFactory, ResourceLocator resourceLocator,
			String displayName, String description, EStructuralFeature feature,
			boolean isSettable, Object staticImage) {
		super(adapterFactory, resourceLocator, displayName, description, feature,
				isSettable, staticImage);
		// TODO Auto-generated constructor stub
	}

	public CustomedAttributeItemPropertyDescriptor(
			AdapterFactory adapterFactory, ResourceLocator resourceLocator,
			String displayName, String description, EStructuralFeature feature,
			boolean isSettable, String category, String[] filterFlags) {
		super(adapterFactory, resourceLocator, displayName, description, feature,
				isSettable, category, filterFlags);
		// TODO Auto-generated constructor stub
	}

	public CustomedAttributeItemPropertyDescriptor(
			AdapterFactory adapterFactory, ResourceLocator resourceLocator,
			String displayName, String description, EStructuralFeature feature,
			boolean isSettable, String category) {
		super(adapterFactory, resourceLocator, displayName, description, feature,
				isSettable, category);
		// TODO Auto-generated constructor stub
	}

	public CustomedAttributeItemPropertyDescriptor(
			AdapterFactory adapterFactory, ResourceLocator resourceLocator,
			String displayName, String description, EStructuralFeature feature,
			boolean isSettable) {
		super(adapterFactory, resourceLocator, displayName, description, feature,
				isSettable);
		// TODO Auto-generated constructor stub
	}

	public CustomedAttributeItemPropertyDescriptor(
			AdapterFactory adapterFactory, ResourceLocator resourceLocator,
			String displayName, String description, EStructuralFeature feature) {
		super(adapterFactory, resourceLocator, displayName, description, feature);
		// TODO Auto-generated constructor stub
	}

	public CustomedAttributeItemPropertyDescriptor(
			AdapterFactory adapterFactory, String displayName,
			String description, EReference[] parentReferences,
			boolean isSettable, String category, String[] filterFlags) {
		super(adapterFactory, displayName, description, parentReferences, isSettable,
				category, filterFlags);
		// TODO Auto-generated constructor stub
	}

	public CustomedAttributeItemPropertyDescriptor(
			AdapterFactory adapterFactory, String displayName,
			String description, EReference[] parentReferences,
			boolean isSettable, String category) {
		super(adapterFactory, displayName, description, parentReferences, isSettable,
				category);
		// TODO Auto-generated constructor stub
	}

	public CustomedAttributeItemPropertyDescriptor(
			AdapterFactory adapterFactory, String displayName,
			String description, EReference[] parentReferences,
			boolean isSettable) {
		super(adapterFactory, displayName, description, parentReferences, isSettable);
		// TODO Auto-generated constructor stub
	}

	public CustomedAttributeItemPropertyDescriptor(
			AdapterFactory adapterFactory, String displayName,
			String description, EReference[] parentReferences) {
		super(adapterFactory, displayName, description, parentReferences);
		// TODO Auto-generated constructor stub
	}

	public CustomedAttributeItemPropertyDescriptor(
			AdapterFactory adapterFactory, String displayName,
			String description, EStructuralFeature feature, boolean isSettable,
			Object staticImage, String category, String[] filterFlags) {
		super(adapterFactory, displayName, description, feature, isSettable,
				staticImage, category, filterFlags);
		// TODO Auto-generated constructor stub
	}

	public CustomedAttributeItemPropertyDescriptor(
			AdapterFactory adapterFactory, String displayName,
			String description, EStructuralFeature feature, boolean isSettable,
			Object staticImage, String category) {
		super(adapterFactory, displayName, description, feature, isSettable,
				staticImage, category);
		// TODO Auto-generated constructor stub
	}

	public CustomedAttributeItemPropertyDescriptor(
			AdapterFactory adapterFactory, String displayName,
			String description, EStructuralFeature feature, boolean isSettable,
			Object staticImage) {
		super(adapterFactory, displayName, description, feature, isSettable,
				staticImage);
		// TODO Auto-generated constructor stub
	}

	public CustomedAttributeItemPropertyDescriptor(
			AdapterFactory adapterFactory, String displayName,
			String description, EStructuralFeature feature, boolean isSettable,
			String category, String[] filterFlags) {
		super(adapterFactory, displayName, description, feature, isSettable, category,
				filterFlags);
		// TODO Auto-generated constructor stub
	}

	public CustomedAttributeItemPropertyDescriptor(
			AdapterFactory adapterFactory, String displayName,
			String description, EStructuralFeature feature, boolean isSettable,
			String category) {
		super(adapterFactory, displayName, description, feature, isSettable, category);
		// TODO Auto-generated constructor stub
	}

	public CustomedAttributeItemPropertyDescriptor(
			AdapterFactory adapterFactory, String displayName,
			String description, EStructuralFeature feature, boolean isSettable) {
		super(adapterFactory, displayName, description, feature, isSettable);
		// TODO Auto-generated constructor stub
	}

	public CustomedAttributeItemPropertyDescriptor(
			AdapterFactory adapterFactory, String displayName,
			String description, EStructuralFeature feature) {
		super(adapterFactory, displayName, description, feature);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected Collection<?> getComboBoxObjects(Object object) {
		AttributeNode node = (AttributeNode)object;
		EClass cls = ((EntityNode)node.eContainer()).getEntity();
		if(cls==null)
			return super.getComboBoxObjects(object);
		else return cls.getEAllAttributes();
	}
	
}