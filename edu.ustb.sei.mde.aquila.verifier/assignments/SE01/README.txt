说明
在线授课已经成为大学教学过程中的必要手段，目前已经存在了不少在线授课平台。假设你是一名致力于开发新型在线授课系统的开发人员，请综合你使用过的在线授课系统（例如：雨课堂、腾讯课堂、课程中心等等），针对这个领域的实际需求建立一个用况模型，力求做到功能全面。

一个功能全面的在线授课系统应当能够满足教师、学生、教务、督导等多个教学相关角色的需求。至少包括但不限于以下核心功能，你应当以此为基础，进一步捕获和诱导需求以完成题目。
教师能够开启课堂
教师能够通过共享屏幕等方式为学生上课，并能使用电子白板笔的功能
教师能够进行点名
教师能够要求学生发言
教师能够发布在线测试和作业
教师能够批阅做作业和测试
教师能够统计学生的学习情况
学生能够加入课堂
学生能够举手发言
学生能够通过共享屏幕等方式参与讨论
学生能够参与在线测试、提交作业
教务可以导入课程和选课信息
教务可以导出成绩信息
督导可以加入课堂旁听
督导可以填写听课评语

要求
使用Aquila工具中的用况图建立模型
针对点名、举手发言这两个用况，编写用况说明
提交方式：以附件的形式提交一个.zip文件（不接受其他格式！！！），其中包含如下两个文件
PDF格式的作业文档，包含用况模型截图和文字说明。格式要求：小节标题黑体+四号，正文宋体+五号，1.2倍行距。
Aquila模型（.aquila文件），以备自动化检查和查重。
文件命名规则：学号-姓名.pdf和学号-姓名.aquila，例如12345678-张三.pdf和12345678-张三.aquila。

评价标准
总分：10分
符号正确性：3分
参与者识别正确和完整：2分
用况识别正确和完整：4分
进一步需求捕获和诱导：1分（体现为额外的用况和/或参与者，合理则得分）
形式检查：作业提交不符合要求的，最多可扣除5分