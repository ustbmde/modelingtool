package edu.ustb.sei.mde.aquila.verifier;

import org.junit.jupiter.api.Test;

import edu.ustb.sei.mde.aquila.verifier.ModelRatingComputer.Tuple;

class TestExtractor {

	@Test
	void test() {
		SubmissionExtractor o = new SubmissionExtractor("/Volumes/Macintosh HD Data/Eclipse Projects/Modeling/edu.ustb.sei.mde.aquila.verifier/model");
		o.decompressZipFiles();
	}
	
	@Test
	void test2() {
		AquilaModelLoader modelLoader = new AquilaModelLoader("/Volumes/Macintosh HD Data/Eclipse Projects/Modeling/edu.ustb.sei.mde.aquila.verifier/model/models");
		modelLoader.loadModels();
		SubmissionChecker o = new SubmissionChecker(modelLoader);
		o.checkUUIDs();
		o.checkEquality();
	}
	
	@Test
	void test3() {
		AquilaModelLoader modelLoader = new AquilaModelLoader("/Volumes/Macintosh HD Data/Eclipse Projects/git/Modelingtool/edu.ustb.sei.mde.aquila.verifier/model/models");
		modelLoader.loadModels();
		
		SubmissionVerifier o = new SubmissionVerifier(modelLoader);
		
		o.verifyUseCase("/Volumes/Macintosh HD Data/Eclipse Projects/git/Modelingtool/edu.ustb.sei.mde.aquila.verifier/assignments/SE01/SE01.aquila", result->{
			double ps = 0;
			if(result.coveredActors > 0) ps += 1;
			if(result.coveredUseCases > 2) ps += 1;
			if(result.coveredAssociations > 4) ps += 1;
			double ns = 0;
			if(result.redundantActors > 4) ns += 0.5;
			if(result.redundantUseCases > 3) ns += 0.5;
			
			return new Tuple<Double,Double>(ps, ns);
		});
	}

}
