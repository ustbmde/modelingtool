package edu.ustb.sei.mde.aquila.verifier;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.eclipse.emf.ecore.resource.Resource;

import com.google.common.base.Function;

import edu.ustb.sei.mde.aquila.verifier.ModelRatingComputer.ClassRating;
import edu.ustb.sei.mde.aquila.verifier.ModelRatingComputer.Tuple;
import edu.ustb.sei.mde.aquila.verifier.ModelRatingComputer.UseCaseRating;

public class SubmissionVerifier {
	private AquilaModelLoader modelLoader;

	public SubmissionVerifier(AquilaModelLoader modelLoader) {
		super();
		this.modelLoader = modelLoader;
	}
	
	public void verifyUseCase(String expectedResultPath, Function<UseCaseRating, Tuple<Double, Double>> ratingFunction) {
		Map<String, List<String>> keywordsMap = loadKeywords(expectedResultPath);
		Resource expected = modelLoader.loadModel(expectedResultPath);
		
		System.out.println("Rating Use Case Model");
		
		List<Resource> curResList = new ArrayList<>(modelLoader.getModels());
		
		for(Resource res : curResList) {
			if(res == expected) continue;
			try {
				ModelRatingComputer rating = new ModelRatingComputer(expected, res, keywordsMap);
				Tuple<Double,Double> rate = rating.ratingUseCase(ratingFunction);
				System.out.print(res.getURI().lastSegment());
				System.out.print("\t");
				System.out.print(rate.first);
				System.out.print(", -");
				System.out.print(rate.second);
				System.out.print(", ");
				System.out.println(rate.first - rate.second);
			} catch (Exception e) {
				System.out.print(res.getURI().lastSegment());
				System.out.print(" cannot be rated due to ");
				System.out.println(e.getMessage());
				
			}
		}
		
		System.out.println("End of rating use case model");	
	}
	
	public void verifyClassModel(String expectedResultPath, Function<ClassRating, Tuple<Double, Double>> ratingFunction) {
		Map<String, List<String>> keywordsMap = loadKeywords(expectedResultPath);
		Resource expected = modelLoader.loadModel(expectedResultPath);
		
		System.out.println("Rating Class Model");
		
		for(Resource res : modelLoader.getModels()) {
			if(res == expected) continue;
			ModelRatingComputer rating = new ModelRatingComputer(expected, res, keywordsMap);
			Tuple<Double,Double> rate = rating.ratingClassModel(ratingFunction);
			System.out.print(res.getURI().lastSegment());
			System.out.print("\t");
			System.out.print(rate.first);
			System.out.print(", -");
			System.out.print(rate.second);
			System.out.print(", ");
			System.out.println(rate.first - rate.second);
		}
		
		System.out.println("End of rating class model");	
	}

	private Map<String, List<String>> loadKeywords(String expectedResultPath) {
		String keywordMapPath = expectedResultPath.substring(0, expectedResultPath.lastIndexOf('.')+1)+"properties";
		File keywordMapFile = new File(keywordMapPath);
		Properties prop = new Properties();
		Map<String, List<String>> keywordsMap = new HashMap<>();
		
		InputStream in;
		try {
			in = new FileInputStream(keywordMapFile);
			InputStreamReader inr = new InputStreamReader(in, "UTF-8");
			prop.load(inr);
			
			prop.forEach((k,v)->{
				List<String> vals = new ArrayList<>();
				vals.add(k.toString());
				for(String vv : v.toString().split(",")) 
					vals.add(vv);
				keywordsMap.put(k.toString(), vals);
			});
			inr.close();
			in.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return keywordsMap;
	}
}
