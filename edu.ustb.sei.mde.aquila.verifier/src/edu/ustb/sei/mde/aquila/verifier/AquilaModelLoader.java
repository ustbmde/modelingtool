package edu.ustb.sei.mde.aquila.verifier;

import java.io.File;
import java.util.List;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;

import edu.ustb.sei.mde.Aquila.AquilaPackage;

public class AquilaModelLoader {
	private String modelFolderName;
	private ResourceSet resourceSet;
	
	public AquilaModelLoader(String modelFolderName) {
		super();
		this.modelFolderName = modelFolderName;
		
		resourceSet = new ResourceSetImpl();
		resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put(
			    Resource.Factory.Registry.DEFAULT_EXTENSION, new XMIResourceFactoryImpl());
		
		resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put(
			    "aquila", new XMIResourceFactoryImpl());
		
		resourceSet.getPackageRegistry().put(AquilaPackage.eNS_URI, AquilaPackage.eINSTANCE);
	}
	
	public void loadModels() {
		File modelFolders = new File(modelFolderName);
		if(modelFolders.exists()) {
			File[] files = modelFolders.listFiles((dir,name)->name.endsWith(".aquila"));
			for(File modelFile : files) {
				String path = modelFile.getAbsolutePath();
				loadModel(path);
			}
		}
	}
	
	public List<Resource> getModels() {
		return resourceSet.getResources();
	}
	
	public Resource loadModel(String path) {
		return resourceSet.getResource(URI.createFileURI(path), true);
	}
}
