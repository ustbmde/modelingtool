package edu.ustb.sei.mde.aquila.verifier;

import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

import org.eclipse.emf.ecore.resource.Resource;

import com.google.common.base.Function;

import edu.ustb.sei.mde.Aquila.AqActor;
import edu.ustb.sei.mde.Aquila.AqAttribute;
import edu.ustb.sei.mde.Aquila.AqClass;
import edu.ustb.sei.mde.Aquila.AqDependencyKind;
import edu.ustb.sei.mde.Aquila.AqOperation;
import edu.ustb.sei.mde.Aquila.AqReference;
import edu.ustb.sei.mde.Aquila.AqUseCase;
import edu.ustb.sei.mde.Aquila.Model;
import edu.ustb.sei.mde.Aquila.NamedElement;

public class ModelRatingComputer {
	private Resource expectedResult;
	public ModelRatingComputer(Resource expectedResult, Resource userResult, Map<String, List<String>> keywordMap) {
		super();
		this.expectedResult = expectedResult;
		this.userResult = userResult;
		this.keywordMap = keywordMap;
	}

	private Resource userResult;
	private Map<String, List<String>> keywordMap;
	
	static public class ClassRating {
		public int coveredClasses;
		public int coveredAttributes;
		public int coveredReferences;
		public int coveredOperations;
		public int coveredMessages;
		
		public int redundantClasses;
		public int redundantAttributes;
		public int redundantReferences;
		public int redundantOperations;
		public int redundantMessages;
	}
	
	static public class UseCaseRating {
		public int coveredActors = 0;
		public int coveredUseCases = 0;
		public int coveredAssociations = 0;
		
		public int redundantActors = 0;
		public int redundantUseCases = 0;
		public int redundantAssociations = 0;
	}
	
	static public class Tuple<F,S> {
		final public F first;
		final public S second;
		public Tuple(F first, S second) {
			super();
			this.first = first;
			this.second = second;
		}
		
	}
	
	static public class Tuple3<F,S, M> {
		final public F first;
		final public S second;
		final public M third;
		public Tuple3(F first, S second, M third) {
			super();
			this.first = first;
			this.second = second;
			this.third = third;
		}
		
	}
	
	// for each node in expected, find a similar node in user model
	// for each edge in expected, find a similar edge in user model
	// then we know correct score
	
	// for uncovered user nodes and edges, we know wrong score
	
	
	protected boolean matchNamedElement(NamedElement expected, NamedElement user) {
		String expectedName = expected.getName();
		String userName = user.getName();
		return checkName(expectedName, userName);
	}
	
	private boolean shouldIgnore(String expected) {
		List<String> ignoreLists = keywordMap.getOrDefault("@ignores", Collections.emptyList());
		return ignoreLists.contains(expected);
	}

	private boolean checkName(String expectedName, String userName) {
		List<String> keywords = keywordMap.getOrDefault(expectedName, Collections.singletonList(expectedName));
		if(userName==null) return false;
		for(String k : keywords) {
			if(userName.contains(k)) return true;
		}
		return false;
	}
	

	
	public Tuple<Double, Double> ratingUseCase(Function<UseCaseRating, Tuple<Double, Double>> rating) {
		return rating.apply(checkUseCase());
	}
	
	public Tuple<Double, Double> ratingClassModel(Function<ClassRating, Tuple<Double, Double>> rating) {
		return rating.apply(checkClassModel());
	}
	
	protected <T> void checkNamedElements(Set<? extends NamedElement> expected, Set<? extends NamedElement> user, T table, Consumer<T> addCover, BiConsumer<T, Integer> setRedundant) {
		Set<Object> del = new HashSet<>();
		del.addAll(user);
		for(NamedElement e : expected) {
			if(shouldIgnore(e.getName())) continue;
			for(NamedElement u : user) {
				if(matchNamedElement(e, u)) {
					addCover.accept(table);
					del.remove(u);
				}
			}
		}
		setRedundant.accept(table, del.size());
	}
	
	protected <F extends NamedElement, S extends NamedElement, T> void checkNamedElementEdges(Set<Tuple<F,S>> expected, Set<Tuple<F,S>> user, 
			T table, Consumer<T> addCover, BiConsumer<T, Integer> setRedundant) {
		Set<Object> del = new HashSet<>();
		del.addAll(user);
		for(Tuple<? extends NamedElement,? extends NamedElement> e : expected) {
			for(Tuple<? extends NamedElement,? extends NamedElement> u : user) {
				if(matchNamedElement(e.first, u.first) && matchNamedElement(e.second, u.second)) {
					addCover.accept(table);
					del.remove(u);
				}
			}
		}
		setRedundant.accept(table, del.size());
	}
	
	protected <F extends NamedElement, S extends NamedElement,  M, T> void checkNamedElementEdges(Set<Tuple3<F,S, M>> expected, Set<Tuple3<F,S, M>> user, 
			Function<M, String> nameFunc, T table, Consumer<T> addCover, BiConsumer<T, Integer> setRedundant) {
		Set<Object> del = new HashSet<>();
		del.addAll(user);
		for(Tuple3<F,S,M> e : expected) {
			for(Tuple3<F,S,M> u : user) {
				if(matchNamedElement(e.first, u.first) 
//						&& matchNamedElement(e.second, u.second) 
						&& checkName(nameFunc.apply(e.third), nameFunc.apply(u.third))) {
					addCover.accept(table);
					del.remove(u);
				}
			}
		}
		setRedundant.accept(table, del.size());
	}
	
	
	protected UseCaseRating checkUseCase() {
		Model expectedModel = (Model) expectedResult.getContents().get(0);
		Model userModel = (Model) userResult.getContents().get(0);
		
		Set<AqActor> expectedActors = new HashSet<>();
		Set<AqActor> userActors = new HashSet<>();
		Set<AqUseCase> expectedUseCases = new HashSet<>();
		Set<AqUseCase> userUseCases = new HashSet<>();
		Set<Tuple<AqActor,AqUseCase>> expectedAssoc = new HashSet<>();
		Set<Tuple<AqActor,AqUseCase>> userAssoc = new HashSet<>();
		
		extractUseCaseModel(expectedModel, expectedActors, expectedUseCases, expectedAssoc);
		extractUseCaseModel(userModel, userActors, userUseCases, userAssoc);
		
		UseCaseRating rating = new UseCaseRating();
		
		checkNamedElements(expectedActors, userActors, rating, r->{r.coveredActors++;}, (r,s)->{r.redundantActors = s;});
		checkNamedElements(expectedUseCases, userUseCases, rating, r->{r.coveredUseCases++;}, (r,s)->{r.redundantUseCases = s;});
		checkNamedElementEdges(expectedAssoc, userAssoc, rating, r->{r.coveredAssociations++;}, (r,s)->{r.redundantAssociations = s;});
		
		return rating;

	}
	
	private void extractUseCaseModel(Model expectedModel, Set<AqActor> expectedActors, Set<AqUseCase> expectedUseCases, Set<Tuple<AqActor,AqUseCase>> expectedAssoc) {
		expectedModel.getUseCaseElements().forEach(e->{
			if(e instanceof AqUseCase) {
				expectedUseCases.add((AqUseCase) e);
			} else if(e instanceof AqActor) {
				expectedActors.add((AqActor) e);
				extractActorUseCaseAssoc((AqActor) e, (AqActor) e, expectedAssoc);
			}
		});
	}

	private void extractActorUseCaseAssoc(AqActor e, AqActor n, Set<Tuple<AqActor, AqUseCase>> expectedAssoc) {
		e.getUseCases().forEach(u->{
			expectedAssoc.add(new Tuple<AqActor,AqUseCase>(n, u));
		});
		
		e.getSuperActors().forEach(s->{
			extractActorUseCaseAssoc(s, n, expectedAssoc);
		});
	}
		
	protected ClassRating checkClassModel() {
		Model expectedModel = (Model) expectedResult.getContents().get(0);
		Model userModel = (Model) userResult.getContents().get(0);
		
		Set<AqClass> expectedClasses = new HashSet<>();
		Set<AqClass> userClasses = new HashSet<>();
		
		Set<Tuple<AqClass,AqAttribute>> expectedAttributes = new HashSet<>();
		Set<Tuple<AqClass,AqAttribute>> userAttributes = new HashSet<>();
		
		Set<Tuple3<AqClass, AqClass, AqReference>> expectedAssoc = new HashSet<>();
		Set<Tuple3<AqClass, AqClass, AqReference>> userAssoc = new HashSet<>();
		
		Set<Tuple<AqClass, AqOperation>> expectedOperations = new HashSet<>();
		Set<Tuple<AqClass, AqOperation>> userOperations = new HashSet<>();
		
		Set<Tuple<AqClass, AqClass>> expectedMessages = new HashSet<>();
		Set<Tuple<AqClass, AqClass>> userMessages = new HashSet<>();
		
		extractClassModel(expectedModel, expectedClasses, expectedAttributes, expectedAssoc,expectedOperations, expectedMessages);
		extractClassModel(userModel, userClasses, userAttributes, userAssoc, userOperations, userMessages);
		
		ClassRating rating = new ClassRating();
		
		checkNamedElements(expectedClasses, userClasses, rating, r->{r.coveredClasses++;}, (r,s)->{r.redundantClasses = s;});
		checkNamedElementEdges(expectedAttributes, userAttributes, rating, r->{r.coveredAttributes++;}, (r,s)->{r.redundantAttributes = s;});
		checkNamedElementEdges(expectedAssoc, userAssoc, r->r.getName(), rating, r->{r.coveredReferences++;}, (r,s)->{r.redundantReferences = s;});
		checkNamedElementEdges(expectedOperations, userOperations, rating, r->{r.coveredOperations++;}, (r,s)->{r.redundantOperations = s;});
		checkNamedElementEdges(expectedMessages, userMessages, rating, r->{r.coveredMessages++;}, (r,s)->{r.redundantMessages = s;});
		
		return rating;
	}

	private void extractClassModel(Model model, Set<AqClass> classes, Set<Tuple<AqClass,AqAttribute>> attributes, Set<Tuple3<AqClass, AqClass, AqReference>> references, Set<Tuple<AqClass, AqOperation>> operations, Set<Tuple<AqClass, AqClass>> messages) {
		model.getElements().forEach(e->{
			if(e instanceof AqClass) {
				classes.add((AqClass) e);
				extractClassFeatures((AqClass) e, (AqClass) e, attributes, references, operations, messages);
			}
		});
	}

	private void extractClassFeatures(AqClass c, AqClass r, Set<Tuple<AqClass, AqAttribute>> attributes, Set<Tuple3<AqClass, AqClass, AqReference>> references, Set<Tuple<AqClass, AqOperation>> operations, Set<Tuple<AqClass, AqClass>> messages) {
		c.getStructuralFeatures().forEach(f->{
			if(f instanceof AqAttribute) attributes.add(new Tuple<>(r, (AqAttribute) f));
			else references.add(new Tuple3<>(r, (AqClass) f.getType(), (AqReference) f));
		});
		c.getOperations().forEach(o->{
			operations.add(new Tuple<>(r, (AqOperation) o));
		});
		c.getDependencies().forEach(d->{
			if(d.getKind()==AqDependencyKind.CALL) {
				messages.add(new Tuple<>(r, d.getTarget()));
			}
		});
		
		c.getSuperTypes().forEach(s->{
			extractClassFeatures(s, r, attributes, references, operations, messages);
		});
	}
	
}
