package edu.ustb.sei.mde.aquila.verifier;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.compare.Comparison;
import org.eclipse.emf.compare.EMFCompare;
import org.eclipse.emf.compare.Match;
import org.eclipse.emf.compare.match.IMatchEngine;
import org.eclipse.emf.compare.match.impl.MatchEngineFactoryImpl;
import org.eclipse.emf.compare.match.impl.MatchEngineFactoryRegistryImpl;
import org.eclipse.emf.compare.scope.DefaultComparisonScope;
import org.eclipse.emf.compare.scope.IComparisonScope;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.Resource;

import edu.ustb.sei.mde.Aquila.Model;

public class SubmissionChecker {
	private AquilaModelLoader modelLoader;

	public SubmissionChecker(AquilaModelLoader modelLoader) {
		super();
		this.modelLoader = modelLoader;
	}
	
	public void checkUUIDs() {
		List<Resource> resources = modelLoader.getModels();
		System.out.println("Checking UUIDs");
		
		for(int i=0;i<resources.size(); i++) {
			Resource former = resources.get(i);
			String formerID = getID(former);
			for(int j=i+1; j< resources.size() ; j++) {
				Resource later = resources.get(j);
				String laterID = getID(later);
				
				if(formerID.equals(laterID)) {
					String formerName = former.getURI().lastSegment();
					String laterName = later.getURI().lastSegment();
					
					System.out.print(formerName);
					System.out.print("(");
					System.out.print(i);
					System.out.print(")");
					System.out.print(",");
					System.out.print(laterName);
					System.out.print("(");
					System.out.print(j);
					System.out.print(")");
					System.out.println();
				}
			}
		}
		
		System.out.println("End of checking UUIDs");
		System.out.println();
	}
	
	public void checkEquality() {
		List<Resource> resources = modelLoader.getModels();
		System.out.println("Checking equality");
		
		IMatchEngine.Factory matchEngineFactory = new MatchEngineFactoryImpl();
		matchEngineFactory.setRanking(20);
		IMatchEngine.Factory.Registry matchEngineRegistry = new MatchEngineFactoryRegistryImpl();
		matchEngineRegistry.add(matchEngineFactory);
		EMFCompare comparator = EMFCompare.builder().setMatchEngineFactoryRegistry(matchEngineRegistry).build();

		for(int i=0;i<resources.size(); i++) {
			Resource former = resources.get(i);
			for(int j=i+1; j< resources.size() ; j++) {
				Resource later = resources.get(j);
				
				IComparisonScope scope = new DefaultComparisonScope(former, later, null);
				
				Comparison result = comparator.compare(scope);
				
				
				if(checkComparison(result)) {
					String formerName = former.getURI().lastSegment();
					String laterName = later.getURI().lastSegment();
					
					System.out.print(formerName);
					System.out.print("(");
					System.out.print(i);
					System.out.print(")");
					System.out.print(",");
					System.out.print(laterName);
					System.out.print("(");
					System.out.print(j);
					System.out.print(")");
					System.out.println();
				}
			}
		}
		
		System.out.println("End of checking equality");
		System.out.println();
	}

	private boolean checkComparison(Comparison result) {
		int totalElements = 0;
		int matchedElements = 0;
		
		TreeIterator<EObject> it = result.eAllContents();
		while(it.hasNext()) {
			EObject o = it.next();
			if(o instanceof Match) {
				Match match = (Match) o;
				if(match.getLeft() == null || match.getRight() == null) {
					totalElements ++;
				} else {
					Set<EStructuralFeature> allFeatures = new HashSet<>();
					allFeatures.addAll(match.getLeft().eClass().getEAllStructuralFeatures());
					allFeatures.addAll(match.getRight().eClass().getEAllStructuralFeatures());
					
					boolean hasEnoughDiffs = match.getDifferences().size() > allFeatures.size() * 0.5;
					if(hasEnoughDiffs) {
						totalElements += 2;
					} else {
						totalElements ++;
						matchedElements ++;
					}
				}
			}
		}
		
		return totalElements * 0.8 < matchedElements;
	}

	private String getID(Resource former) {
		if(former==null) return "";
		List<EObject> contents = former.getContents();
		if(contents.isEmpty()) return "";
		Model model = (Model) contents.get(0);
		return model.getUUID();
	}
	
	
}
