package edu.ustb.sei.mde.aquila.verifier;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.nio.charset.MalformedInputException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipArchiveInputStream;
import org.apache.commons.compress.utils.IOUtils;

public class SubmissionExtractor {
	private String baseFolder;
	private Map<String,String> extensionToSubFolder;
	private Set<String> extractedFileNames;
	
	public SubmissionExtractor(String bf) {
		baseFolder = bf;
		extensionToSubFolder = new HashMap<>();
		
		extractedFileNames = new HashSet<>();
		
		//default rules
		addMapping("pdf", "documents");
		addMapping("aquila", "models");
	}
	
	public void addMapping(String extName, String folderName) {
		extensionToSubFolder.put(extName, folderName);
	}
	
	public void decompressZipFiles() {
		String folderPath = baseFolder + File.separatorChar + "submissions";
		File folder = new File(folderPath);
		decompressZipFiles(folder);
		
	}

	private void decompressZipFiles(File folder) {
		if(folder.exists()==false) return;
		
		for(File file : folder.listFiles()) {
			if(file.isHidden()) continue;
			if(file.isFile()) {
				String extName = file.getName().substring(file.getName().lastIndexOf('.')+1);
				if(extName.equalsIgnoreCase("zip")) {
					decompressFromZip(file.getAbsolutePath());
				}
			} else if(file.isDirectory()) {
				decompressZipFiles(file);
			}
		}
	}

	private void decompressFromZip(String zipFilePath) {
		File file = new File(zipFilePath);
		String encoding = "GBK";
		decompressFromZipWithEncoding(zipFilePath, file, encoding);
	}

	private void decompressFromZipWithEncoding(String zipFilePath, File file, String encoding) {
		try (ZipArchiveInputStream zipInputStream = new ZipArchiveInputStream(new BufferedInputStream(new FileInputStream(file)), encoding)) {
			ZipArchiveEntry fileInZip = null;
			while ((fileInZip = zipInputStream.getNextZipEntry()) != null) {
				if(fileInZip.isDirectory()) continue;
				String fileName = fileInZip.getName();
				fileName = fileName.substring(fileName.lastIndexOf(File.separatorChar)+1);
				if(fileName.startsWith(".")) continue;
				if(fileInZip.getSize()<=0) continue;
				
				String extName = fileName.substring(fileName.lastIndexOf('.')+1).toLowerCase();
				String subFolderName = extensionToSubFolder.getOrDefault(extName, "unresolved");
				String targetName = baseFolder + File.separatorChar + subFolderName + File.separatorChar + fileName;
				
				if(extractedFileNames.contains(targetName)) {
					System.out.print(zipFilePath);
					System.out.println("\t does not match naming constraint");
					targetName = targetName.substring(0, targetName.length() - extName.length()) + "2."+ extName;
				}
				extractedFileNames.add(targetName);
				
				File targetFile = new File(targetName);
				
				// 创建文件之前必须保证父文件夹存在
				if (!targetFile.getParentFile().exists()) {
					targetFile.getParentFile().mkdirs();
				}
				// 创建文件
				targetFile.createNewFile();
				
				try(OutputStream output = new FileOutputStream(targetFile)) {
					IOUtils.copy(zipInputStream, output);
					output.flush();
					output.close();
				} catch(Exception e) {
					System.out.println("Failed at "+fileInZip.getName()+ " due to "+e.getMessage());
				}
			}
			zipInputStream.close();
		} catch(MalformedInputException e) {
			if("GBK".equals(encoding)) {
				encoding = "UTF-8";
			} else if("UTF-8".equals(encoding)){
				encoding = "GB2312";
			} else if("GB2312".equals(encoding)){
				encoding = "iso-8859-1";
			} else {
				System.out.println("Unzip Failed at "+zipFilePath+" due to "+e.getMessage());
				return;
			}
			decompressFromZipWithEncoding(zipFilePath, file, encoding);
		} catch (Exception e) {
			System.out.println("Unzip Failed at "+zipFilePath+" due to "+e.getMessage());
		}
	}

}
