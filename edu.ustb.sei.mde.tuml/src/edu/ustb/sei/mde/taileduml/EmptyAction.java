/**
 */
package edu.ustb.sei.mde.taileduml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Empty Action</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.ustb.sei.mde.taileduml.TailedUMLPackage#getEmptyAction()
 * @model
 * @generated
 */
public interface EmptyAction extends BuiltInAction {
} // EmptyAction
