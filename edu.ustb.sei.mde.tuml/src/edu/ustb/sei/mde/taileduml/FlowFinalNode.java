/**
 */
package edu.ustb.sei.mde.taileduml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Flow Final Node</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.ustb.sei.mde.taileduml.TailedUMLPackage#getFlowFinalNode()
 * @model
 * @generated
 */
public interface FlowFinalNode extends FinalNode {
} // FlowFinalNode
