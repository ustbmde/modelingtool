/**
 */
package edu.ustb.sei.mde.taileduml;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Assign Action</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.taileduml.AssignAction#getCopies <em>Copies</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.ustb.sei.mde.taileduml.TailedUMLPackage#getAssignAction()
 * @model
 * @generated
 */
public interface AssignAction extends BuiltInAction {
	/**
	 * Returns the value of the '<em><b>Copies</b></em>' containment reference list.
	 * The list contents are of type {@link edu.ustb.sei.mde.taileduml.CopyTask}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Copies</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Copies</em>' containment reference list.
	 * @see edu.ustb.sei.mde.taileduml.TailedUMLPackage#getAssignAction_Copies()
	 * @model containment="true"
	 * @generated
	 */
	EList<CopyTask> getCopies();

} // AssignAction
