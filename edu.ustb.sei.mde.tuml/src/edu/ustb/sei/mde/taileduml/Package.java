/**
 */
package edu.ustb.sei.mde.taileduml;

import org.eclipse.emf.common.util.EList;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Package</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.taileduml.Package#getClassifiers <em>Classifiers</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.taileduml.Package#getRelationships <em>Relationships</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.ustb.sei.mde.taileduml.TailedUMLPackage#getPackage()
 * @model
 * @generated
 */
public interface Package extends NamedElement, ModelElement {

	/**
	 * Returns the value of the '<em><b>Classifiers</b></em>' containment reference list.
	 * The list contents are of type {@link edu.ustb.sei.mde.taileduml.Type}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Classifiers</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Classifiers</em>' containment reference list.
	 * @see edu.ustb.sei.mde.taileduml.TailedUMLPackage#getPackage_Classifiers()
	 * @model containment="true"
	 * @generated
	 */
	EList<Type> getClassifiers();

	/**
	 * Returns the value of the '<em><b>Relationships</b></em>' containment reference list.
	 * The list contents are of type {@link edu.ustb.sei.mde.taileduml.Relationship}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Relationships</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Relationships</em>' containment reference list.
	 * @see edu.ustb.sei.mde.taileduml.TailedUMLPackage#getPackage_Relationships()
	 * @model containment="true"
	 * @generated
	 */
	EList<Relationship> getRelationships();
} // Package
