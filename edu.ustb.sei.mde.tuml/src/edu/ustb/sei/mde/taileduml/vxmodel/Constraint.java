/**
 */
package edu.ustb.sei.mde.taileduml.vxmodel;

import edu.ustb.sei.mde.taileduml.Relationship;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Constraint</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.taileduml.vxmodel.Constraint#getType <em>Type</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.ustb.sei.mde.taileduml.vxmodel.VxModelPackage#getConstraint()
 * @model
 * @generated
 */
public interface Constraint extends Relationship {
	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * The literals are from the enumeration {@link edu.ustb.sei.mde.taileduml.vxmodel.ConstraintType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see edu.ustb.sei.mde.taileduml.vxmodel.ConstraintType
	 * @see #setType(ConstraintType)
	 * @see edu.ustb.sei.mde.taileduml.vxmodel.VxModelPackage#getConstraint_Type()
	 * @model
	 * @generated
	 */
	ConstraintType getType();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.taileduml.vxmodel.Constraint#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see edu.ustb.sei.mde.taileduml.vxmodel.ConstraintType
	 * @see #getType()
	 * @generated
	 */
	void setType(ConstraintType value);

} // Constraint
