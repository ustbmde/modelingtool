/**
 */
package edu.ustb.sei.mde.taileduml.vxmodel;

import edu.ustb.sei.mde.taileduml.Classifier;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Variation Point</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.taileduml.vxmodel.VariationPoint#getVariations <em>Variations</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.ustb.sei.mde.taileduml.vxmodel.VxModelPackage#getVariationPoint()
 * @model
 * @generated
 */
public interface VariationPoint extends Classifier {
	/**
	 * Returns the value of the '<em><b>Variations</b></em>' reference list.
	 * The list contents are of type {@link edu.ustb.sei.mde.taileduml.vxmodel.Variation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Variations</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Variations</em>' reference list.
	 * @see edu.ustb.sei.mde.taileduml.vxmodel.VxModelPackage#getVariationPoint_Variations()
	 * @model
	 * @generated
	 */
	EList<Variation> getVariations();

} // VariationPoint
