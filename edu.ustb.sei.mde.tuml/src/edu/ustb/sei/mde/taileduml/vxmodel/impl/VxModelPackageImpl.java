/**
 */
package edu.ustb.sei.mde.taileduml.vxmodel.impl;

import edu.ustb.sei.mde.taileduml.TailedUMLPackage;

import edu.ustb.sei.mde.taileduml.impl.TailedUMLPackageImpl;

import edu.ustb.sei.mde.taileduml.vxactivity.VxActivityPackage;

import edu.ustb.sei.mde.taileduml.vxactivity.impl.VxActivityPackageImpl;

import edu.ustb.sei.mde.taileduml.vxmodel.Constraint;
import edu.ustb.sei.mde.taileduml.vxmodel.ConstraintType;
import edu.ustb.sei.mde.taileduml.vxmodel.Variation;
import edu.ustb.sei.mde.taileduml.vxmodel.VariationPoint;
import edu.ustb.sei.mde.taileduml.vxmodel.VxModelFactory;
import edu.ustb.sei.mde.taileduml.vxmodel.VxModelPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class VxModelPackageImpl extends EPackageImpl implements VxModelPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass variationPointEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass variationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass constraintEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum constraintTypeEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see edu.ustb.sei.mde.taileduml.vxmodel.VxModelPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private VxModelPackageImpl() {
		super(eNS_URI, VxModelFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link VxModelPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static VxModelPackage init() {
		if (isInited) return (VxModelPackage)EPackage.Registry.INSTANCE.getEPackage(VxModelPackage.eNS_URI);

		// Obtain or create and register package
		VxModelPackageImpl theVxModelPackage = (VxModelPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof VxModelPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new VxModelPackageImpl());

		isInited = true;

		// Obtain or create and register interdependencies
		TailedUMLPackageImpl theTailedUMLPackage = (TailedUMLPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(TailedUMLPackage.eNS_URI) instanceof TailedUMLPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(TailedUMLPackage.eNS_URI) : TailedUMLPackage.eINSTANCE);
		VxActivityPackageImpl theVxActivityPackage = (VxActivityPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(VxActivityPackage.eNS_URI) instanceof VxActivityPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(VxActivityPackage.eNS_URI) : VxActivityPackage.eINSTANCE);

		// Create package meta-data objects
		theVxModelPackage.createPackageContents();
		theTailedUMLPackage.createPackageContents();
		theVxActivityPackage.createPackageContents();

		// Initialize created meta-data
		theVxModelPackage.initializePackageContents();
		theTailedUMLPackage.initializePackageContents();
		theVxActivityPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theVxModelPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(VxModelPackage.eNS_URI, theVxModelPackage);
		return theVxModelPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVariationPoint() {
		return variationPointEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getVariationPoint_Variations() {
		return (EReference)variationPointEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVariation() {
		return variationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getConstraint() {
		return constraintEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getConstraint_Type() {
		return (EAttribute)constraintEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getConstraintType() {
		return constraintTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VxModelFactory getVxModelFactory() {
		return (VxModelFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		variationPointEClass = createEClass(VARIATION_POINT);
		createEReference(variationPointEClass, VARIATION_POINT__VARIATIONS);

		variationEClass = createEClass(VARIATION);

		constraintEClass = createEClass(CONSTRAINT);
		createEAttribute(constraintEClass, CONSTRAINT__TYPE);

		// Create enums
		constraintTypeEEnum = createEEnum(CONSTRAINT_TYPE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		TailedUMLPackage theTailedUMLPackage = (TailedUMLPackage)EPackage.Registry.INSTANCE.getEPackage(TailedUMLPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		variationPointEClass.getESuperTypes().add(theTailedUMLPackage.getClassifier());
		variationEClass.getESuperTypes().add(theTailedUMLPackage.getClassifier());
		constraintEClass.getESuperTypes().add(theTailedUMLPackage.getRelationship());

		// Initialize classes, features, and operations; add parameters
		initEClass(variationPointEClass, VariationPoint.class, "VariationPoint", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getVariationPoint_Variations(), this.getVariation(), null, "variations", null, 0, -1, VariationPoint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(variationEClass, Variation.class, "Variation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(constraintEClass, Constraint.class, "Constraint", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getConstraint_Type(), this.getConstraintType(), "type", null, 0, 1, Constraint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(constraintTypeEEnum, ConstraintType.class, "ConstraintType");
		addEEnumLiteral(constraintTypeEEnum, ConstraintType.REQUIRE);
		addEEnumLiteral(constraintTypeEEnum, ConstraintType.EXCLUDE);
	}

} //VxModelPackageImpl
