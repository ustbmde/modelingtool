/**
 */
package edu.ustb.sei.mde.taileduml.vxmodel.impl;

import edu.ustb.sei.mde.taileduml.impl.ClassifierImpl;

import edu.ustb.sei.mde.taileduml.vxmodel.Variation;
import edu.ustb.sei.mde.taileduml.vxmodel.VxModelPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Variation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class VariationImpl extends ClassifierImpl implements Variation {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VariationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return VxModelPackage.Literals.VARIATION;
	}

} //VariationImpl
