/**
 */
package edu.ustb.sei.mde.taileduml.vxmodel;

import edu.ustb.sei.mde.taileduml.Classifier;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Variation</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.ustb.sei.mde.taileduml.vxmodel.VxModelPackage#getVariation()
 * @model
 * @generated
 */
public interface Variation extends Classifier {
} // Variation
