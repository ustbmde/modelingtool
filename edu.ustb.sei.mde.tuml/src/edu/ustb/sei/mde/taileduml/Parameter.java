/**
 */
package edu.ustb.sei.mde.taileduml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Parameter</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.ustb.sei.mde.taileduml.TailedUMLPackage#getParameter()
 * @model
 * @generated
 */
public interface Parameter extends NamedElement, BoundedElement, TypedElement {
} // Parameter
