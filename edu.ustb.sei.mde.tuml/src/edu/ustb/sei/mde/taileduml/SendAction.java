/**
 */
package edu.ustb.sei.mde.taileduml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Send Action</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.ustb.sei.mde.taileduml.TailedUMLPackage#getSendAction()
 * @model
 * @generated
 */
public interface SendAction extends Action {
} // SendAction
