/**
 */
package edu.ustb.sei.mde.taileduml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Object Node</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.ustb.sei.mde.taileduml.TailedUMLPackage#getObjectNode()
 * @model
 * @generated
 */
public interface ObjectNode extends NamedElement, TypedElement, ActivityNode {
} // ObjectNode
