/**
 */
package edu.ustb.sei.mde.taileduml.vxactivity.impl;

import edu.ustb.sei.mde.taileduml.TailedUMLPackage;

import edu.ustb.sei.mde.taileduml.impl.TailedUMLPackageImpl;

import edu.ustb.sei.mde.taileduml.vxactivity.VariationActivityFragment;
import edu.ustb.sei.mde.taileduml.vxactivity.VxAction;
import edu.ustb.sei.mde.taileduml.vxactivity.VxActivityFactory;
import edu.ustb.sei.mde.taileduml.vxactivity.VxActivityPackage;

import edu.ustb.sei.mde.taileduml.vxmodel.VxModelPackage;

import edu.ustb.sei.mde.taileduml.vxmodel.impl.VxModelPackageImpl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class VxActivityPackageImpl extends EPackageImpl implements VxActivityPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass vxActionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass variationActivityFragmentEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see edu.ustb.sei.mde.taileduml.vxactivity.VxActivityPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private VxActivityPackageImpl() {
		super(eNS_URI, VxActivityFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link VxActivityPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static VxActivityPackage init() {
		if (isInited) return (VxActivityPackage)EPackage.Registry.INSTANCE.getEPackage(VxActivityPackage.eNS_URI);

		// Obtain or create and register package
		VxActivityPackageImpl theVxActivityPackage = (VxActivityPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof VxActivityPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new VxActivityPackageImpl());

		isInited = true;

		// Obtain or create and register interdependencies
		TailedUMLPackageImpl theTailedUMLPackage = (TailedUMLPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(TailedUMLPackage.eNS_URI) instanceof TailedUMLPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(TailedUMLPackage.eNS_URI) : TailedUMLPackage.eINSTANCE);
		VxModelPackageImpl theVxModelPackage = (VxModelPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(VxModelPackage.eNS_URI) instanceof VxModelPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(VxModelPackage.eNS_URI) : VxModelPackage.eINSTANCE);

		// Create package meta-data objects
		theVxActivityPackage.createPackageContents();
		theTailedUMLPackage.createPackageContents();
		theVxModelPackage.createPackageContents();

		// Initialize created meta-data
		theVxActivityPackage.initializePackageContents();
		theTailedUMLPackage.initializePackageContents();
		theVxModelPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theVxActivityPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(VxActivityPackage.eNS_URI, theVxActivityPackage);
		return theVxActivityPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVxAction() {
		return vxActionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getVxAction_VariantFragments() {
		return (EReference)vxActionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getVxAction_VariationPoint() {
		return (EReference)vxActionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVariationActivityFragment() {
		return variationActivityFragmentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getVariationActivityFragment_Owner() {
		return (EReference)variationActivityFragmentEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getVariationActivityFragment_Variation() {
		return (EReference)variationActivityFragmentEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VxActivityFactory getVxActivityFactory() {
		return (VxActivityFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		vxActionEClass = createEClass(VX_ACTION);
		createEReference(vxActionEClass, VX_ACTION__VARIANT_FRAGMENTS);
		createEReference(vxActionEClass, VX_ACTION__VARIATION_POINT);

		variationActivityFragmentEClass = createEClass(VARIATION_ACTIVITY_FRAGMENT);
		createEReference(variationActivityFragmentEClass, VARIATION_ACTIVITY_FRAGMENT__OWNER);
		createEReference(variationActivityFragmentEClass, VARIATION_ACTIVITY_FRAGMENT__VARIATION);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		TailedUMLPackage theTailedUMLPackage = (TailedUMLPackage)EPackage.Registry.INSTANCE.getEPackage(TailedUMLPackage.eNS_URI);
		VxModelPackage theVxModelPackage = (VxModelPackage)EPackage.Registry.INSTANCE.getEPackage(VxModelPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		vxActionEClass.getESuperTypes().add(theTailedUMLPackage.getAction());
		variationActivityFragmentEClass.getESuperTypes().add(theTailedUMLPackage.getActivityGroup());

		// Initialize classes, features, and operations; add parameters
		initEClass(vxActionEClass, VxAction.class, "VxAction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getVxAction_VariantFragments(), this.getVariationActivityFragment(), this.getVariationActivityFragment_Owner(), "variantFragments", null, 0, -1, VxAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getVxAction_VariationPoint(), theVxModelPackage.getVariationPoint(), null, "variationPoint", null, 0, 1, VxAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(variationActivityFragmentEClass, VariationActivityFragment.class, "VariationActivityFragment", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getVariationActivityFragment_Owner(), this.getVxAction(), this.getVxAction_VariantFragments(), "owner", null, 0, 1, VariationActivityFragment.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getVariationActivityFragment_Variation(), theVxModelPackage.getVariation(), null, "variation", null, 0, 1, VariationActivityFragment.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
	}

} //VxActivityPackageImpl
