/**
 */
package edu.ustb.sei.mde.taileduml.vxactivity.impl;

import edu.ustb.sei.mde.taileduml.impl.ActivityGroupImpl;

import edu.ustb.sei.mde.taileduml.vxactivity.VariationActivityFragment;
import edu.ustb.sei.mde.taileduml.vxactivity.VxAction;
import edu.ustb.sei.mde.taileduml.vxactivity.VxActivityPackage;

import edu.ustb.sei.mde.taileduml.vxmodel.Variation;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Variation Activity Fragment</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.taileduml.vxactivity.impl.VariationActivityFragmentImpl#getOwner <em>Owner</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.taileduml.vxactivity.impl.VariationActivityFragmentImpl#getVariation <em>Variation</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class VariationActivityFragmentImpl extends ActivityGroupImpl implements VariationActivityFragment {
	/**
	 * The cached value of the '{@link #getOwner() <em>Owner</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOwner()
	 * @generated
	 * @ordered
	 */
	protected VxAction owner;

	/**
	 * The cached value of the '{@link #getVariation() <em>Variation</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVariation()
	 * @generated
	 * @ordered
	 */
	protected Variation variation;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VariationActivityFragmentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return VxActivityPackage.Literals.VARIATION_ACTIVITY_FRAGMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VxAction getOwner() {
		if (owner != null && owner.eIsProxy()) {
			InternalEObject oldOwner = (InternalEObject)owner;
			owner = (VxAction)eResolveProxy(oldOwner);
			if (owner != oldOwner) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, VxActivityPackage.VARIATION_ACTIVITY_FRAGMENT__OWNER, oldOwner, owner));
			}
		}
		return owner;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VxAction basicGetOwner() {
		return owner;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOwner(VxAction newOwner, NotificationChain msgs) {
		VxAction oldOwner = owner;
		owner = newOwner;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, VxActivityPackage.VARIATION_ACTIVITY_FRAGMENT__OWNER, oldOwner, newOwner);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOwner(VxAction newOwner) {
		if (newOwner != owner) {
			NotificationChain msgs = null;
			if (owner != null)
				msgs = ((InternalEObject)owner).eInverseRemove(this, VxActivityPackage.VX_ACTION__VARIANT_FRAGMENTS, VxAction.class, msgs);
			if (newOwner != null)
				msgs = ((InternalEObject)newOwner).eInverseAdd(this, VxActivityPackage.VX_ACTION__VARIANT_FRAGMENTS, VxAction.class, msgs);
			msgs = basicSetOwner(newOwner, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VxActivityPackage.VARIATION_ACTIVITY_FRAGMENT__OWNER, newOwner, newOwner));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Variation getVariation() {
		if (variation != null && variation.eIsProxy()) {
			InternalEObject oldVariation = (InternalEObject)variation;
			variation = (Variation)eResolveProxy(oldVariation);
			if (variation != oldVariation) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, VxActivityPackage.VARIATION_ACTIVITY_FRAGMENT__VARIATION, oldVariation, variation));
			}
		}
		return variation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Variation basicGetVariation() {
		return variation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVariation(Variation newVariation) {
		Variation oldVariation = variation;
		variation = newVariation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VxActivityPackage.VARIATION_ACTIVITY_FRAGMENT__VARIATION, oldVariation, variation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case VxActivityPackage.VARIATION_ACTIVITY_FRAGMENT__OWNER:
				if (owner != null)
					msgs = ((InternalEObject)owner).eInverseRemove(this, VxActivityPackage.VX_ACTION__VARIANT_FRAGMENTS, VxAction.class, msgs);
				return basicSetOwner((VxAction)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case VxActivityPackage.VARIATION_ACTIVITY_FRAGMENT__OWNER:
				return basicSetOwner(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case VxActivityPackage.VARIATION_ACTIVITY_FRAGMENT__OWNER:
				if (resolve) return getOwner();
				return basicGetOwner();
			case VxActivityPackage.VARIATION_ACTIVITY_FRAGMENT__VARIATION:
				if (resolve) return getVariation();
				return basicGetVariation();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case VxActivityPackage.VARIATION_ACTIVITY_FRAGMENT__OWNER:
				setOwner((VxAction)newValue);
				return;
			case VxActivityPackage.VARIATION_ACTIVITY_FRAGMENT__VARIATION:
				setVariation((Variation)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case VxActivityPackage.VARIATION_ACTIVITY_FRAGMENT__OWNER:
				setOwner((VxAction)null);
				return;
			case VxActivityPackage.VARIATION_ACTIVITY_FRAGMENT__VARIATION:
				setVariation((Variation)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case VxActivityPackage.VARIATION_ACTIVITY_FRAGMENT__OWNER:
				return owner != null;
			case VxActivityPackage.VARIATION_ACTIVITY_FRAGMENT__VARIATION:
				return variation != null;
		}
		return super.eIsSet(featureID);
	}

} //VariationActivityFragmentImpl
