/**
 */
package edu.ustb.sei.mde.taileduml.vxactivity.impl;

import edu.ustb.sei.mde.taileduml.vxactivity.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class VxActivityFactoryImpl extends EFactoryImpl implements VxActivityFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static VxActivityFactory init() {
		try {
			VxActivityFactory theVxActivityFactory = (VxActivityFactory)EPackage.Registry.INSTANCE.getEFactory(VxActivityPackage.eNS_URI);
			if (theVxActivityFactory != null) {
				return theVxActivityFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new VxActivityFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VxActivityFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case VxActivityPackage.VX_ACTION: return createVxAction();
			case VxActivityPackage.VARIATION_ACTIVITY_FRAGMENT: return createVariationActivityFragment();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VxAction createVxAction() {
		VxActionImpl vxAction = new VxActionImpl();
		return vxAction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VariationActivityFragment createVariationActivityFragment() {
		VariationActivityFragmentImpl variationActivityFragment = new VariationActivityFragmentImpl();
		return variationActivityFragment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VxActivityPackage getVxActivityPackage() {
		return (VxActivityPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static VxActivityPackage getPackage() {
		return VxActivityPackage.eINSTANCE;
	}

} //VxActivityFactoryImpl
