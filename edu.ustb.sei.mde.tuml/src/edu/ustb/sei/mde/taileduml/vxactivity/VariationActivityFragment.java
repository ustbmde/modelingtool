/**
 */
package edu.ustb.sei.mde.taileduml.vxactivity;


import edu.ustb.sei.mde.taileduml.ActivityGroup;
import edu.ustb.sei.mde.taileduml.vxmodel.Variation;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Variation Activity Fragment</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.taileduml.vxactivity.VariationActivityFragment#getOwner <em>Owner</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.taileduml.vxactivity.VariationActivityFragment#getVariation <em>Variation</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.ustb.sei.mde.taileduml.vxactivity.VxActivityPackage#getVariationActivityFragment()
 * @model
 * @generated
 */
public interface VariationActivityFragment extends ActivityGroup {
	/**
	 * Returns the value of the '<em><b>Owner</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link edu.ustb.sei.mde.taileduml.vxactivity.VxAction#getVariantFragments <em>Variant Fragments</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Owner</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Owner</em>' reference.
	 * @see #setOwner(VxAction)
	 * @see edu.ustb.sei.mde.taileduml.vxactivity.VxActivityPackage#getVariationActivityFragment_Owner()
	 * @see edu.ustb.sei.mde.taileduml.vxactivity.VxAction#getVariantFragments
	 * @model opposite="variantFragments"
	 * @generated
	 */
	VxAction getOwner();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.taileduml.vxactivity.VariationActivityFragment#getOwner <em>Owner</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Owner</em>' reference.
	 * @see #getOwner()
	 * @generated
	 */
	void setOwner(VxAction value);

	/**
	 * Returns the value of the '<em><b>Variation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Variation</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Variation</em>' reference.
	 * @see #setVariation(Variation)
	 * @see edu.ustb.sei.mde.taileduml.vxactivity.VxActivityPackage#getVariationActivityFragment_Variation()
	 * @model
	 * @generated
	 */
	Variation getVariation();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.taileduml.vxactivity.VariationActivityFragment#getVariation <em>Variation</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Variation</em>' reference.
	 * @see #getVariation()
	 * @generated
	 */
	void setVariation(Variation value);

} // VariationActivityFragment
