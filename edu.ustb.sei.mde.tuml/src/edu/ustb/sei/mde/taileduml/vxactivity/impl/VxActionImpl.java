/**
 */
package edu.ustb.sei.mde.taileduml.vxactivity.impl;

import edu.ustb.sei.mde.taileduml.impl.ActionImpl;

import edu.ustb.sei.mde.taileduml.vxactivity.VariationActivityFragment;
import edu.ustb.sei.mde.taileduml.vxactivity.VxAction;
import edu.ustb.sei.mde.taileduml.vxactivity.VxActivityPackage;

import edu.ustb.sei.mde.taileduml.vxmodel.VariationPoint;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Vx Action</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.taileduml.vxactivity.impl.VxActionImpl#getVariantFragments <em>Variant Fragments</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.taileduml.vxactivity.impl.VxActionImpl#getVariationPoint <em>Variation Point</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class VxActionImpl extends ActionImpl implements VxAction {
	/**
	 * The cached value of the '{@link #getVariantFragments() <em>Variant Fragments</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVariantFragments()
	 * @generated
	 * @ordered
	 */
	protected EList<VariationActivityFragment> variantFragments;

	/**
	 * The cached value of the '{@link #getVariationPoint() <em>Variation Point</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVariationPoint()
	 * @generated
	 * @ordered
	 */
	protected VariationPoint variationPoint;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VxActionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return VxActivityPackage.Literals.VX_ACTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<VariationActivityFragment> getVariantFragments() {
		if (variantFragments == null) {
			variantFragments = new EObjectWithInverseResolvingEList<VariationActivityFragment>(VariationActivityFragment.class, this, VxActivityPackage.VX_ACTION__VARIANT_FRAGMENTS, VxActivityPackage.VARIATION_ACTIVITY_FRAGMENT__OWNER);
		}
		return variantFragments;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VariationPoint getVariationPoint() {
		if (variationPoint != null && variationPoint.eIsProxy()) {
			InternalEObject oldVariationPoint = (InternalEObject)variationPoint;
			variationPoint = (VariationPoint)eResolveProxy(oldVariationPoint);
			if (variationPoint != oldVariationPoint) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, VxActivityPackage.VX_ACTION__VARIATION_POINT, oldVariationPoint, variationPoint));
			}
		}
		return variationPoint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VariationPoint basicGetVariationPoint() {
		return variationPoint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVariationPoint(VariationPoint newVariationPoint) {
		VariationPoint oldVariationPoint = variationPoint;
		variationPoint = newVariationPoint;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VxActivityPackage.VX_ACTION__VARIATION_POINT, oldVariationPoint, variationPoint));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case VxActivityPackage.VX_ACTION__VARIANT_FRAGMENTS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getVariantFragments()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case VxActivityPackage.VX_ACTION__VARIANT_FRAGMENTS:
				return ((InternalEList<?>)getVariantFragments()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case VxActivityPackage.VX_ACTION__VARIANT_FRAGMENTS:
				return getVariantFragments();
			case VxActivityPackage.VX_ACTION__VARIATION_POINT:
				if (resolve) return getVariationPoint();
				return basicGetVariationPoint();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case VxActivityPackage.VX_ACTION__VARIANT_FRAGMENTS:
				getVariantFragments().clear();
				getVariantFragments().addAll((Collection<? extends VariationActivityFragment>)newValue);
				return;
			case VxActivityPackage.VX_ACTION__VARIATION_POINT:
				setVariationPoint((VariationPoint)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case VxActivityPackage.VX_ACTION__VARIANT_FRAGMENTS:
				getVariantFragments().clear();
				return;
			case VxActivityPackage.VX_ACTION__VARIATION_POINT:
				setVariationPoint((VariationPoint)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case VxActivityPackage.VX_ACTION__VARIANT_FRAGMENTS:
				return variantFragments != null && !variantFragments.isEmpty();
			case VxActivityPackage.VX_ACTION__VARIATION_POINT:
				return variationPoint != null;
		}
		return super.eIsSet(featureID);
	}

} //VxActionImpl
