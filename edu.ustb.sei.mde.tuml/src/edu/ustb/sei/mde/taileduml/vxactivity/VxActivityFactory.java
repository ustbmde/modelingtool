/**
 */
package edu.ustb.sei.mde.taileduml.vxactivity;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see edu.ustb.sei.mde.taileduml.vxactivity.VxActivityPackage
 * @generated
 */
public interface VxActivityFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	VxActivityFactory eINSTANCE = edu.ustb.sei.mde.taileduml.vxactivity.impl.VxActivityFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Vx Action</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Vx Action</em>'.
	 * @generated
	 */
	VxAction createVxAction();

	/**
	 * Returns a new object of class '<em>Variation Activity Fragment</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Variation Activity Fragment</em>'.
	 * @generated
	 */
	VariationActivityFragment createVariationActivityFragment();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	VxActivityPackage getVxActivityPackage();

} //VxActivityFactory
