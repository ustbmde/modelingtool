/**
 */
package edu.ustb.sei.mde.taileduml.vxactivity;

import edu.ustb.sei.mde.taileduml.Action;

import edu.ustb.sei.mde.taileduml.vxmodel.VariationPoint;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Vx Action</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.taileduml.vxactivity.VxAction#getVariantFragments <em>Variant Fragments</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.taileduml.vxactivity.VxAction#getVariationPoint <em>Variation Point</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.ustb.sei.mde.taileduml.vxactivity.VxActivityPackage#getVxAction()
 * @model
 * @generated
 */
public interface VxAction extends Action {
	/**
	 * Returns the value of the '<em><b>Variant Fragments</b></em>' reference list.
	 * The list contents are of type {@link edu.ustb.sei.mde.taileduml.vxactivity.VariationActivityFragment}.
	 * It is bidirectional and its opposite is '{@link edu.ustb.sei.mde.taileduml.vxactivity.VariationActivityFragment#getOwner <em>Owner</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Variant Fragments</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Variant Fragments</em>' reference list.
	 * @see edu.ustb.sei.mde.taileduml.vxactivity.VxActivityPackage#getVxAction_VariantFragments()
	 * @see edu.ustb.sei.mde.taileduml.vxactivity.VariationActivityFragment#getOwner
	 * @model opposite="owner"
	 * @generated
	 */
	EList<VariationActivityFragment> getVariantFragments();

	/**
	 * Returns the value of the '<em><b>Variation Point</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Variation Point</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Variation Point</em>' reference.
	 * @see #setVariationPoint(VariationPoint)
	 * @see edu.ustb.sei.mde.taileduml.vxactivity.VxActivityPackage#getVxAction_VariationPoint()
	 * @model
	 * @generated
	 */
	VariationPoint getVariationPoint();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.taileduml.vxactivity.VxAction#getVariationPoint <em>Variation Point</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Variation Point</em>' reference.
	 * @see #getVariationPoint()
	 * @generated
	 */
	void setVariationPoint(VariationPoint value);

} // VxAction
