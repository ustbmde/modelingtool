/**
 */
package edu.ustb.sei.mde.taileduml.vxactivity;

import edu.ustb.sei.mde.taileduml.TailedUMLPackage;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see edu.ustb.sei.mde.taileduml.vxactivity.VxActivityFactory
 * @model kind="package"
 * @generated
 */
public interface VxActivityPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "vxactivity";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "edu.ustb.sei.mde.taileduml.vxactivity";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "vxactivity";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	VxActivityPackage eINSTANCE = edu.ustb.sei.mde.taileduml.vxactivity.impl.VxActivityPackageImpl.init();

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.taileduml.vxactivity.impl.VxActionImpl <em>Vx Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.taileduml.vxactivity.impl.VxActionImpl
	 * @see edu.ustb.sei.mde.taileduml.vxactivity.impl.VxActivityPackageImpl#getVxAction()
	 * @generated
	 */
	int VX_ACTION = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VX_ACTION__NAME = TailedUMLPackage.ACTION__NAME;

	/**
	 * The feature id for the '<em><b>Variant Fragments</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VX_ACTION__VARIANT_FRAGMENTS = TailedUMLPackage.ACTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Variation Point</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VX_ACTION__VARIATION_POINT = TailedUMLPackage.ACTION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Vx Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VX_ACTION_FEATURE_COUNT = TailedUMLPackage.ACTION_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Vx Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VX_ACTION_OPERATION_COUNT = TailedUMLPackage.ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.taileduml.vxactivity.impl.VariationActivityFragmentImpl <em>Variation Activity Fragment</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.taileduml.vxactivity.impl.VariationActivityFragmentImpl
	 * @see edu.ustb.sei.mde.taileduml.vxactivity.impl.VxActivityPackageImpl#getVariationActivityFragment()
	 * @generated
	 */
	int VARIATION_ACTIVITY_FRAGMENT = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIATION_ACTIVITY_FRAGMENT__NAME = TailedUMLPackage.ACTIVITY_GROUP__NAME;

	/**
	 * The feature id for the '<em><b>Nodes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIATION_ACTIVITY_FRAGMENT__NODES = TailedUMLPackage.ACTIVITY_GROUP__NODES;

	/**
	 * The feature id for the '<em><b>Edges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIATION_ACTIVITY_FRAGMENT__EDGES = TailedUMLPackage.ACTIVITY_GROUP__EDGES;

	/**
	 * The feature id for the '<em><b>Owner</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIATION_ACTIVITY_FRAGMENT__OWNER = TailedUMLPackage.ACTIVITY_GROUP_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Variation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIATION_ACTIVITY_FRAGMENT__VARIATION = TailedUMLPackage.ACTIVITY_GROUP_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Variation Activity Fragment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIATION_ACTIVITY_FRAGMENT_FEATURE_COUNT = TailedUMLPackage.ACTIVITY_GROUP_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Variation Activity Fragment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIATION_ACTIVITY_FRAGMENT_OPERATION_COUNT = TailedUMLPackage.ACTIVITY_GROUP_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.taileduml.vxactivity.VxAction <em>Vx Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Vx Action</em>'.
	 * @see edu.ustb.sei.mde.taileduml.vxactivity.VxAction
	 * @generated
	 */
	EClass getVxAction();

	/**
	 * Returns the meta object for the reference list '{@link edu.ustb.sei.mde.taileduml.vxactivity.VxAction#getVariantFragments <em>Variant Fragments</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Variant Fragments</em>'.
	 * @see edu.ustb.sei.mde.taileduml.vxactivity.VxAction#getVariantFragments()
	 * @see #getVxAction()
	 * @generated
	 */
	EReference getVxAction_VariantFragments();

	/**
	 * Returns the meta object for the reference '{@link edu.ustb.sei.mde.taileduml.vxactivity.VxAction#getVariationPoint <em>Variation Point</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Variation Point</em>'.
	 * @see edu.ustb.sei.mde.taileduml.vxactivity.VxAction#getVariationPoint()
	 * @see #getVxAction()
	 * @generated
	 */
	EReference getVxAction_VariationPoint();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.taileduml.vxactivity.VariationActivityFragment <em>Variation Activity Fragment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Variation Activity Fragment</em>'.
	 * @see edu.ustb.sei.mde.taileduml.vxactivity.VariationActivityFragment
	 * @generated
	 */
	EClass getVariationActivityFragment();

	/**
	 * Returns the meta object for the reference '{@link edu.ustb.sei.mde.taileduml.vxactivity.VariationActivityFragment#getOwner <em>Owner</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Owner</em>'.
	 * @see edu.ustb.sei.mde.taileduml.vxactivity.VariationActivityFragment#getOwner()
	 * @see #getVariationActivityFragment()
	 * @generated
	 */
	EReference getVariationActivityFragment_Owner();

	/**
	 * Returns the meta object for the reference '{@link edu.ustb.sei.mde.taileduml.vxactivity.VariationActivityFragment#getVariation <em>Variation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Variation</em>'.
	 * @see edu.ustb.sei.mde.taileduml.vxactivity.VariationActivityFragment#getVariation()
	 * @see #getVariationActivityFragment()
	 * @generated
	 */
	EReference getVariationActivityFragment_Variation();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	VxActivityFactory getVxActivityFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.taileduml.vxactivity.impl.VxActionImpl <em>Vx Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.taileduml.vxactivity.impl.VxActionImpl
		 * @see edu.ustb.sei.mde.taileduml.vxactivity.impl.VxActivityPackageImpl#getVxAction()
		 * @generated
		 */
		EClass VX_ACTION = eINSTANCE.getVxAction();

		/**
		 * The meta object literal for the '<em><b>Variant Fragments</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VX_ACTION__VARIANT_FRAGMENTS = eINSTANCE.getVxAction_VariantFragments();

		/**
		 * The meta object literal for the '<em><b>Variation Point</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VX_ACTION__VARIATION_POINT = eINSTANCE.getVxAction_VariationPoint();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.taileduml.vxactivity.impl.VariationActivityFragmentImpl <em>Variation Activity Fragment</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.taileduml.vxactivity.impl.VariationActivityFragmentImpl
		 * @see edu.ustb.sei.mde.taileduml.vxactivity.impl.VxActivityPackageImpl#getVariationActivityFragment()
		 * @generated
		 */
		EClass VARIATION_ACTIVITY_FRAGMENT = eINSTANCE.getVariationActivityFragment();

		/**
		 * The meta object literal for the '<em><b>Owner</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VARIATION_ACTIVITY_FRAGMENT__OWNER = eINSTANCE.getVariationActivityFragment_Owner();

		/**
		 * The meta object literal for the '<em><b>Variation</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VARIATION_ACTIVITY_FRAGMENT__VARIATION = eINSTANCE.getVariationActivityFragment_Variation();

	}

} //VxActivityPackage
