package edu.ustb.sei.mde.taileduml.figures;

import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.geometry.Rectangle;

public class DiamondFigure extends Figure {

	private int[] points = new int[8];
	
	@Override
	protected void paintFigure(Graphics graphics) {
		Rectangle rect = this.getBounds();
		
		points[0] = rect.x; 
		points[1] = rect.y+rect.height/2;
		
		points[2] = rect.x+rect.width/2; 
		points[3] = rect.y;
		
		points[4] = rect.x+rect.width; 
		points[5] = rect.y+rect.height/2;
		
		points[6] = rect.x+rect.width/2; 
		points[7] = rect.y+rect.height;
		
//		int x1 = rect.x, y1 = rect.y+rect.height/2;
//		int x2 = rect.x+rect.width/2, y2 = rect.y;
//		int x3 = rect.x+rect.width, y3 = rect.y+rect.height/2;
//		int x4 = rect.x+rect.width/2, y4 = rect.y+rect.height;
		
		graphics.fillPolygon(points);
		graphics.drawPolygon(points);
	}



//	@Override
//	protected void fillShape(Graphics graphics) {
//		// TODO Auto-generated method stub
//		Rectangle rect = this.getBounds();
//		rect = rect.getShrinked(3, 3);
//		graphics.pushState();
//		graphics.setForegroundColor(ColorConstants.black);
//		graphics.fillOval(rect);
//		graphics.popState();
//	}
//
//	@Override
//	protected void outlineShape(Graphics graphics) {
//		// TODO Auto-generated method stub
//		Rectangle rect = this.getBounds();
//		graphics.drawOval(rect);
//	}

}
