package edu.ustb.sei.mde.taileduml.figures;

import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.geometry.Rectangle;

public class ActivityFinalNodeFigure extends Figure {

	@Override
	protected void paintFigure(Graphics graphics) {
		Rectangle rect = this.getBounds();
		graphics.drawOval(rect);
		rect = rect.getShrinked(3, 3);
		graphics.pushState();
		graphics.fillOval(rect);
		graphics.popState();
	}



//	@Override
//	protected void fillShape(Graphics graphics) {
//		// TODO Auto-generated method stub
//		Rectangle rect = this.getBounds();
//		rect = rect.getShrinked(3, 3);
//		graphics.pushState();
//		graphics.setForegroundColor(ColorConstants.black);
//		graphics.fillOval(rect);
//		graphics.popState();
//	}
//
//	@Override
//	protected void outlineShape(Graphics graphics) {
//		// TODO Auto-generated method stub
//		Rectangle rect = this.getBounds();
//		graphics.drawOval(rect);
//	}

}
