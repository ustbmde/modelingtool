package edu.ustb.sei.mde.taileduml.figures;

import org.eclipse.draw2d.Figure;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.geometry.PointList;
import org.eclipse.draw2d.geometry.Rectangle;

public class SendActionFigure extends Figure {
	
	private int[] points = new int[10];

	@Override
	protected void paintFigure(Graphics graphics) {
		Rectangle r = this.getBounds();
		
		points[0] = r.x;
		points[1] = r.y;
		
		points[2] = r.x + r.width*2/3;
		points[3] = r.y;
		
		points[4] = r.x + r.width;
		points[5] = r.y + r.height/2;
		
		points[6] = r.x + r.width*2/3;
		points[7] = r.y+r.height;
		
		points[8] = r.x;
		points[9] = r.y + r.height;
		
		graphics.fillPolygon(points);
		graphics.drawPolygon(points);
	}



//	@Override
//	protected void fillShape(Graphics graphics) {
//		// TODO Auto-generated method stub
//		Rectangle rect = this.getBounds();
//		rect = rect.getShrinked(3, 3);
//		graphics.pushState();
//		graphics.setForegroundColor(ColorConstants.black);
//		graphics.fillOval(rect);
//		graphics.popState();
//	}
//
//	@Override
//	protected void outlineShape(Graphics graphics) {
//		// TODO Auto-generated method stub
//		Rectangle rect = this.getBounds();
//		graphics.drawOval(rect);
//	}

}
