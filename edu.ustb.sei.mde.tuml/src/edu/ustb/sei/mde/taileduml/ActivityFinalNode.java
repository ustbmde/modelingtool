/**
 */
package edu.ustb.sei.mde.taileduml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Activity Final Node</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.ustb.sei.mde.taileduml.TailedUMLPackage#getActivityFinalNode()
 * @model
 * @generated
 */
public interface ActivityFinalNode extends FinalNode {
} // ActivityFinalNode
