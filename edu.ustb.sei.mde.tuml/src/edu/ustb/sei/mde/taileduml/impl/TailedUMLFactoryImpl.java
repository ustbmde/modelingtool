/**
 */
package edu.ustb.sei.mde.taileduml.impl;

import edu.ustb.sei.mde.taileduml.AcceptAction;
import edu.ustb.sei.mde.taileduml.Activity;
import edu.ustb.sei.mde.taileduml.ActivityFinalNode;
import edu.ustb.sei.mde.taileduml.AssignAction;
import edu.ustb.sei.mde.taileduml.CallAction;
import edu.ustb.sei.mde.taileduml.ControlFlow;
import edu.ustb.sei.mde.taileduml.CopyTask;
import edu.ustb.sei.mde.taileduml.DecisionNode;
import edu.ustb.sei.mde.taileduml.EmptyAction;
import edu.ustb.sei.mde.taileduml.Enumeration;
import edu.ustb.sei.mde.taileduml.EnumerationLiteral;
import edu.ustb.sei.mde.taileduml.FlowFinalNode;
import edu.ustb.sei.mde.taileduml.ForkNode;
import edu.ustb.sei.mde.taileduml.Generalization;
import edu.ustb.sei.mde.taileduml.InitialNode;
import edu.ustb.sei.mde.taileduml.Interface;
import edu.ustb.sei.mde.taileduml.JoinNode;
import edu.ustb.sei.mde.taileduml.MergeNode;
import edu.ustb.sei.mde.taileduml.Model;
import edu.ustb.sei.mde.taileduml.ObjectFlow;
import edu.ustb.sei.mde.taileduml.ObjectNode;
import edu.ustb.sei.mde.taileduml.Operation;
import edu.ustb.sei.mde.taileduml.Parameter;
import edu.ustb.sei.mde.taileduml.PrimitiveType;
import edu.ustb.sei.mde.taileduml.Property;
import edu.ustb.sei.mde.taileduml.Realization;
import edu.ustb.sei.mde.taileduml.Reference;
import edu.ustb.sei.mde.taileduml.SendAction;
import edu.ustb.sei.mde.taileduml.TailedUMLFactory;
import edu.ustb.sei.mde.taileduml.TailedUMLPackage;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class TailedUMLFactoryImpl extends EFactoryImpl implements TailedUMLFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static TailedUMLFactory init() {
		try {
			TailedUMLFactory theTailedUMLFactory = (TailedUMLFactory)EPackage.Registry.INSTANCE.getEFactory(TailedUMLPackage.eNS_URI);
			if (theTailedUMLFactory != null) {
				return theTailedUMLFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new TailedUMLFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TailedUMLFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case TailedUMLPackage.MODEL: return createModel();
			case TailedUMLPackage.PACKAGE: return createPackage();
			case TailedUMLPackage.CLASS: return createClass();
			case TailedUMLPackage.INTERFACE: return createInterface();
			case TailedUMLPackage.PRIMITIVE_TYPE: return createPrimitiveType();
			case TailedUMLPackage.ENUMERATION: return createEnumeration();
			case TailedUMLPackage.ENUMERATION_LITERAL: return createEnumerationLiteral();
			case TailedUMLPackage.PROPERTY: return createProperty();
			case TailedUMLPackage.OPERATION: return createOperation();
			case TailedUMLPackage.PARAMETER: return createParameter();
			case TailedUMLPackage.GENERALIZATION: return createGeneralization();
			case TailedUMLPackage.REALIZATION: return createRealization();
			case TailedUMLPackage.REFERENCE: return createReference();
			case TailedUMLPackage.ACTIVITY: return createActivity();
			case TailedUMLPackage.CALL_ACTION: return createCallAction();
			case TailedUMLPackage.EMPTY_ACTION: return createEmptyAction();
			case TailedUMLPackage.ASSIGN_ACTION: return createAssignAction();
			case TailedUMLPackage.COPY_TASK: return createCopyTask();
			case TailedUMLPackage.SEND_ACTION: return createSendAction();
			case TailedUMLPackage.ACCEPT_ACTION: return createAcceptAction();
			case TailedUMLPackage.OBJECT_NODE: return createObjectNode();
			case TailedUMLPackage.DECISION_NODE: return createDecisionNode();
			case TailedUMLPackage.MERGE_NODE: return createMergeNode();
			case TailedUMLPackage.FORK_NODE: return createForkNode();
			case TailedUMLPackage.JOIN_NODE: return createJoinNode();
			case TailedUMLPackage.FLOW_FINAL_NODE: return createFlowFinalNode();
			case TailedUMLPackage.ACTIVITY_FINAL_NODE: return createActivityFinalNode();
			case TailedUMLPackage.INITIAL_NODE: return createInitialNode();
			case TailedUMLPackage.CONTROL_FLOW: return createControlFlow();
			case TailedUMLPackage.OBJECT_FLOW: return createObjectFlow();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Model createModel() {
		ModelImpl model = new ModelImpl();
		return model;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public edu.ustb.sei.mde.taileduml.Package createPackage() {
		PackageImpl package_ = new PackageImpl();
		return package_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public edu.ustb.sei.mde.taileduml.Class createClass() {
		ClassImpl class_ = new ClassImpl();
		return class_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Interface createInterface() {
		InterfaceImpl interface_ = new InterfaceImpl();
		return interface_;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PrimitiveType createPrimitiveType() {
		PrimitiveTypeImpl primitiveType = new PrimitiveTypeImpl();
		return primitiveType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Enumeration createEnumeration() {
		EnumerationImpl enumeration = new EnumerationImpl();
		return enumeration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EnumerationLiteral createEnumerationLiteral() {
		EnumerationLiteralImpl enumerationLiteral = new EnumerationLiteralImpl();
		return enumerationLiteral;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Property createProperty() {
		PropertyImpl property = new PropertyImpl();
		return property;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Operation createOperation() {
		OperationImpl operation = new OperationImpl();
		return operation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Parameter createParameter() {
		ParameterImpl parameter = new ParameterImpl();
		return parameter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Generalization createGeneralization() {
		GeneralizationImpl generalization = new GeneralizationImpl();
		return generalization;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Realization createRealization() {
		RealizationImpl realization = new RealizationImpl();
		return realization;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Reference createReference() {
		ReferenceImpl reference = new ReferenceImpl();
		return reference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Activity createActivity() {
		ActivityImpl activity = new ActivityImpl();
		return activity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CallAction createCallAction() {
		CallActionImpl callAction = new CallActionImpl();
		return callAction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EmptyAction createEmptyAction() {
		EmptyActionImpl emptyAction = new EmptyActionImpl();
		return emptyAction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AssignAction createAssignAction() {
		AssignActionImpl assignAction = new AssignActionImpl();
		return assignAction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CopyTask createCopyTask() {
		CopyTaskImpl copyTask = new CopyTaskImpl();
		return copyTask;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SendAction createSendAction() {
		SendActionImpl sendAction = new SendActionImpl();
		return sendAction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AcceptAction createAcceptAction() {
		AcceptActionImpl acceptAction = new AcceptActionImpl();
		return acceptAction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObjectNode createObjectNode() {
		ObjectNodeImpl objectNode = new ObjectNodeImpl();
		return objectNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DecisionNode createDecisionNode() {
		DecisionNodeImpl decisionNode = new DecisionNodeImpl();
		return decisionNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MergeNode createMergeNode() {
		MergeNodeImpl mergeNode = new MergeNodeImpl();
		return mergeNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ForkNode createForkNode() {
		ForkNodeImpl forkNode = new ForkNodeImpl();
		return forkNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public JoinNode createJoinNode() {
		JoinNodeImpl joinNode = new JoinNodeImpl();
		return joinNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FlowFinalNode createFlowFinalNode() {
		FlowFinalNodeImpl flowFinalNode = new FlowFinalNodeImpl();
		return flowFinalNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActivityFinalNode createActivityFinalNode() {
		ActivityFinalNodeImpl activityFinalNode = new ActivityFinalNodeImpl();
		return activityFinalNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public InitialNode createInitialNode() {
		InitialNodeImpl initialNode = new InitialNodeImpl();
		return initialNode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ControlFlow createControlFlow() {
		ControlFlowImpl controlFlow = new ControlFlowImpl();
		return controlFlow;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObjectFlow createObjectFlow() {
		ObjectFlowImpl objectFlow = new ObjectFlowImpl();
		return objectFlow;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TailedUMLPackage getTailedUMLPackage() {
		return (TailedUMLPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static TailedUMLPackage getPackage() {
		return TailedUMLPackage.eINSTANCE;
	}

} //TailedUMLFactoryImpl
