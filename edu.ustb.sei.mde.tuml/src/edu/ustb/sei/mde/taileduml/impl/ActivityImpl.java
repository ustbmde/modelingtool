/**
 */
package edu.ustb.sei.mde.taileduml.impl;

import edu.ustb.sei.mde.taileduml.Activity;
import edu.ustb.sei.mde.taileduml.Operation;

import edu.ustb.sei.mde.taileduml.TailedUMLPackage;

import org.eclipse.emf.common.notify.Notification;


import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;


/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Activity</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.taileduml.impl.ActivityImpl#getRelatedOperation <em>Related Operation</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ActivityImpl extends ActivityGroupImpl implements Activity {
	/**
	 * The cached value of the '{@link #getRelatedOperation() <em>Related Operation</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRelatedOperation()
	 * @generated
	 * @ordered
	 */
	protected Operation relatedOperation;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ActivityImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TailedUMLPackage.Literals.ACTIVITY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Operation getRelatedOperation() {
		if (relatedOperation != null && relatedOperation.eIsProxy()) {
			InternalEObject oldRelatedOperation = (InternalEObject)relatedOperation;
			relatedOperation = (Operation)eResolveProxy(oldRelatedOperation);
			if (relatedOperation != oldRelatedOperation) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TailedUMLPackage.ACTIVITY__RELATED_OPERATION, oldRelatedOperation, relatedOperation));
			}
		}
		return relatedOperation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Operation basicGetRelatedOperation() {
		return relatedOperation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRelatedOperation(Operation newRelatedOperation) {
		Operation oldRelatedOperation = relatedOperation;
		relatedOperation = newRelatedOperation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TailedUMLPackage.ACTIVITY__RELATED_OPERATION, oldRelatedOperation, relatedOperation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TailedUMLPackage.ACTIVITY__RELATED_OPERATION:
				if (resolve) return getRelatedOperation();
				return basicGetRelatedOperation();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TailedUMLPackage.ACTIVITY__RELATED_OPERATION:
				setRelatedOperation((Operation)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TailedUMLPackage.ACTIVITY__RELATED_OPERATION:
				setRelatedOperation((Operation)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TailedUMLPackage.ACTIVITY__RELATED_OPERATION:
				return relatedOperation != null;
		}
		return super.eIsSet(featureID);
	}

} //ActivityImpl
