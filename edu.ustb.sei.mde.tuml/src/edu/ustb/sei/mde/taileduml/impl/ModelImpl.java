/**
 */
package edu.ustb.sei.mde.taileduml.impl;

import edu.ustb.sei.mde.taileduml.ActivityGroup;
import edu.ustb.sei.mde.taileduml.Model;
import edu.ustb.sei.mde.taileduml.PrimitiveType;

import edu.ustb.sei.mde.taileduml.TailedUMLPackage;
import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.taileduml.impl.ModelImpl#getPackages <em>Packages</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.taileduml.impl.ModelImpl#getActivities <em>Activities</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.taileduml.impl.ModelImpl#getPrimitiveTypes <em>Primitive Types</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ModelImpl extends MinimalEObjectImpl.Container implements Model {
	/**
	 * The cached value of the '{@link #getPackages() <em>Packages</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPackages()
	 * @generated
	 * @ordered
	 */
	protected EList<edu.ustb.sei.mde.taileduml.Package> packages;
	/**
	 * The cached value of the '{@link #getActivities() <em>Activities</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActivities()
	 * @generated
	 * @ordered
	 */
	protected EList<ActivityGroup> activities;
	/**
	 * The cached value of the '{@link #getPrimitiveTypes() <em>Primitive Types</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPrimitiveTypes()
	 * @generated
	 * @ordered
	 */
	protected EList<PrimitiveType> primitiveTypes;
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TailedUMLPackage.Literals.MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<edu.ustb.sei.mde.taileduml.Package> getPackages() {
		if (packages == null) {
			packages = new EObjectContainmentEList<edu.ustb.sei.mde.taileduml.Package>(edu.ustb.sei.mde.taileduml.Package.class, this, TailedUMLPackage.MODEL__PACKAGES);
		}
		return packages;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ActivityGroup> getActivities() {
		if (activities == null) {
			activities = new EObjectContainmentEList<ActivityGroup>(ActivityGroup.class, this, TailedUMLPackage.MODEL__ACTIVITIES);
		}
		return activities;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PrimitiveType> getPrimitiveTypes() {
		if (primitiveTypes == null) {
			primitiveTypes = new EObjectContainmentEList<PrimitiveType>(PrimitiveType.class, this, TailedUMLPackage.MODEL__PRIMITIVE_TYPES);
		}
		return primitiveTypes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case TailedUMLPackage.MODEL__PACKAGES:
				return ((InternalEList<?>)getPackages()).basicRemove(otherEnd, msgs);
			case TailedUMLPackage.MODEL__ACTIVITIES:
				return ((InternalEList<?>)getActivities()).basicRemove(otherEnd, msgs);
			case TailedUMLPackage.MODEL__PRIMITIVE_TYPES:
				return ((InternalEList<?>)getPrimitiveTypes()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TailedUMLPackage.MODEL__PACKAGES:
				return getPackages();
			case TailedUMLPackage.MODEL__ACTIVITIES:
				return getActivities();
			case TailedUMLPackage.MODEL__PRIMITIVE_TYPES:
				return getPrimitiveTypes();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TailedUMLPackage.MODEL__PACKAGES:
				getPackages().clear();
				getPackages().addAll((Collection<? extends edu.ustb.sei.mde.taileduml.Package>)newValue);
				return;
			case TailedUMLPackage.MODEL__ACTIVITIES:
				getActivities().clear();
				getActivities().addAll((Collection<? extends ActivityGroup>)newValue);
				return;
			case TailedUMLPackage.MODEL__PRIMITIVE_TYPES:
				getPrimitiveTypes().clear();
				getPrimitiveTypes().addAll((Collection<? extends PrimitiveType>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TailedUMLPackage.MODEL__PACKAGES:
				getPackages().clear();
				return;
			case TailedUMLPackage.MODEL__ACTIVITIES:
				getActivities().clear();
				return;
			case TailedUMLPackage.MODEL__PRIMITIVE_TYPES:
				getPrimitiveTypes().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TailedUMLPackage.MODEL__PACKAGES:
				return packages != null && !packages.isEmpty();
			case TailedUMLPackage.MODEL__ACTIVITIES:
				return activities != null && !activities.isEmpty();
			case TailedUMLPackage.MODEL__PRIMITIVE_TYPES:
				return primitiveTypes != null && !primitiveTypes.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ModelImpl
