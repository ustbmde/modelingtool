/**
 */
package edu.ustb.sei.mde.taileduml.impl;

import edu.ustb.sei.mde.taileduml.CopyTask;
import edu.ustb.sei.mde.taileduml.TailedUMLPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Copy Task</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.taileduml.impl.CopyTaskImpl#getFromVar <em>From Var</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.taileduml.impl.CopyTaskImpl#getFromExpr <em>From Expr</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.taileduml.impl.CopyTaskImpl#getToVar <em>To Var</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.taileduml.impl.CopyTaskImpl#getToExpr <em>To Expr</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class CopyTaskImpl extends MinimalEObjectImpl.Container implements CopyTask {
	/**
	 * The default value of the '{@link #getFromVar() <em>From Var</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFromVar()
	 * @generated
	 * @ordered
	 */
	protected static final String FROM_VAR_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getFromVar() <em>From Var</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFromVar()
	 * @generated
	 * @ordered
	 */
	protected String fromVar = FROM_VAR_EDEFAULT;

	/**
	 * The default value of the '{@link #getFromExpr() <em>From Expr</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFromExpr()
	 * @generated
	 * @ordered
	 */
	protected static final String FROM_EXPR_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getFromExpr() <em>From Expr</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFromExpr()
	 * @generated
	 * @ordered
	 */
	protected String fromExpr = FROM_EXPR_EDEFAULT;

	/**
	 * The default value of the '{@link #getToVar() <em>To Var</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getToVar()
	 * @generated
	 * @ordered
	 */
	protected static final String TO_VAR_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getToVar() <em>To Var</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getToVar()
	 * @generated
	 * @ordered
	 */
	protected String toVar = TO_VAR_EDEFAULT;

	/**
	 * The default value of the '{@link #getToExpr() <em>To Expr</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getToExpr()
	 * @generated
	 * @ordered
	 */
	protected static final String TO_EXPR_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getToExpr() <em>To Expr</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getToExpr()
	 * @generated
	 * @ordered
	 */
	protected String toExpr = TO_EXPR_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CopyTaskImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TailedUMLPackage.Literals.COPY_TASK;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getFromVar() {
		return fromVar;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFromVar(String newFromVar) {
		String oldFromVar = fromVar;
		fromVar = newFromVar;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TailedUMLPackage.COPY_TASK__FROM_VAR, oldFromVar, fromVar));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getFromExpr() {
		return fromExpr;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFromExpr(String newFromExpr) {
		String oldFromExpr = fromExpr;
		fromExpr = newFromExpr;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TailedUMLPackage.COPY_TASK__FROM_EXPR, oldFromExpr, fromExpr));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getToVar() {
		return toVar;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setToVar(String newToVar) {
		String oldToVar = toVar;
		toVar = newToVar;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TailedUMLPackage.COPY_TASK__TO_VAR, oldToVar, toVar));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getToExpr() {
		return toExpr;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setToExpr(String newToExpr) {
		String oldToExpr = toExpr;
		toExpr = newToExpr;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TailedUMLPackage.COPY_TASK__TO_EXPR, oldToExpr, toExpr));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TailedUMLPackage.COPY_TASK__FROM_VAR:
				return getFromVar();
			case TailedUMLPackage.COPY_TASK__FROM_EXPR:
				return getFromExpr();
			case TailedUMLPackage.COPY_TASK__TO_VAR:
				return getToVar();
			case TailedUMLPackage.COPY_TASK__TO_EXPR:
				return getToExpr();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TailedUMLPackage.COPY_TASK__FROM_VAR:
				setFromVar((String)newValue);
				return;
			case TailedUMLPackage.COPY_TASK__FROM_EXPR:
				setFromExpr((String)newValue);
				return;
			case TailedUMLPackage.COPY_TASK__TO_VAR:
				setToVar((String)newValue);
				return;
			case TailedUMLPackage.COPY_TASK__TO_EXPR:
				setToExpr((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TailedUMLPackage.COPY_TASK__FROM_VAR:
				setFromVar(FROM_VAR_EDEFAULT);
				return;
			case TailedUMLPackage.COPY_TASK__FROM_EXPR:
				setFromExpr(FROM_EXPR_EDEFAULT);
				return;
			case TailedUMLPackage.COPY_TASK__TO_VAR:
				setToVar(TO_VAR_EDEFAULT);
				return;
			case TailedUMLPackage.COPY_TASK__TO_EXPR:
				setToExpr(TO_EXPR_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TailedUMLPackage.COPY_TASK__FROM_VAR:
				return FROM_VAR_EDEFAULT == null ? fromVar != null : !FROM_VAR_EDEFAULT.equals(fromVar);
			case TailedUMLPackage.COPY_TASK__FROM_EXPR:
				return FROM_EXPR_EDEFAULT == null ? fromExpr != null : !FROM_EXPR_EDEFAULT.equals(fromExpr);
			case TailedUMLPackage.COPY_TASK__TO_VAR:
				return TO_VAR_EDEFAULT == null ? toVar != null : !TO_VAR_EDEFAULT.equals(toVar);
			case TailedUMLPackage.COPY_TASK__TO_EXPR:
				return TO_EXPR_EDEFAULT == null ? toExpr != null : !TO_EXPR_EDEFAULT.equals(toExpr);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (fromVar: ");
		result.append(fromVar);
		result.append(", fromExpr: ");
		result.append(fromExpr);
		result.append(", toVar: ");
		result.append(toVar);
		result.append(", toExpr: ");
		result.append(toExpr);
		result.append(')');
		return result.toString();
	}

} //CopyTaskImpl
