/**
 */
package edu.ustb.sei.mde.taileduml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Activity</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.taileduml.Activity#getRelatedOperation <em>Related Operation</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.ustb.sei.mde.taileduml.TailedUMLPackage#getActivity()
 * @model
 * @generated
 */
public interface Activity extends ActivityGroup {
	/**
	 * Returns the value of the '<em><b>Related Operation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Related Operation</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Related Operation</em>' reference.
	 * @see #setRelatedOperation(Operation)
	 * @see edu.ustb.sei.mde.taileduml.TailedUMLPackage#getActivity_RelatedOperation()
	 * @model
	 * @generated
	 */
	Operation getRelatedOperation();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.taileduml.Activity#getRelatedOperation <em>Related Operation</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Related Operation</em>' reference.
	 * @see #getRelatedOperation()
	 * @generated
	 */
	void setRelatedOperation(Operation value);

} // Activity
