/**
 */
package edu.ustb.sei.mde.taileduml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Copy Task</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.taileduml.CopyTask#getFromVar <em>From Var</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.taileduml.CopyTask#getFromExpr <em>From Expr</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.taileduml.CopyTask#getToVar <em>To Var</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.taileduml.CopyTask#getToExpr <em>To Expr</em>}</li>
 * </ul>
 * </p>
 *
 * @see edu.ustb.sei.mde.taileduml.TailedUMLPackage#getCopyTask()
 * @model
 * @generated
 */
public interface CopyTask extends EObject {
	/**
	 * Returns the value of the '<em><b>From Var</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>From Var</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>From Var</em>' attribute.
	 * @see #setFromVar(String)
	 * @see edu.ustb.sei.mde.taileduml.TailedUMLPackage#getCopyTask_FromVar()
	 * @model
	 * @generated
	 */
	String getFromVar();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.taileduml.CopyTask#getFromVar <em>From Var</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>From Var</em>' attribute.
	 * @see #getFromVar()
	 * @generated
	 */
	void setFromVar(String value);

	/**
	 * Returns the value of the '<em><b>From Expr</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>From Expr</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>From Expr</em>' attribute.
	 * @see #setFromExpr(String)
	 * @see edu.ustb.sei.mde.taileduml.TailedUMLPackage#getCopyTask_FromExpr()
	 * @model
	 * @generated
	 */
	String getFromExpr();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.taileduml.CopyTask#getFromExpr <em>From Expr</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>From Expr</em>' attribute.
	 * @see #getFromExpr()
	 * @generated
	 */
	void setFromExpr(String value);

	/**
	 * Returns the value of the '<em><b>To Var</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>To Var</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>To Var</em>' attribute.
	 * @see #setToVar(String)
	 * @see edu.ustb.sei.mde.taileduml.TailedUMLPackage#getCopyTask_ToVar()
	 * @model
	 * @generated
	 */
	String getToVar();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.taileduml.CopyTask#getToVar <em>To Var</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>To Var</em>' attribute.
	 * @see #getToVar()
	 * @generated
	 */
	void setToVar(String value);

	/**
	 * Returns the value of the '<em><b>To Expr</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>To Expr</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>To Expr</em>' attribute.
	 * @see #setToExpr(String)
	 * @see edu.ustb.sei.mde.taileduml.TailedUMLPackage#getCopyTask_ToExpr()
	 * @model
	 * @generated
	 */
	String getToExpr();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.taileduml.CopyTask#getToExpr <em>To Expr</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>To Expr</em>' attribute.
	 * @see #getToExpr()
	 * @generated
	 */
	void setToExpr(String value);

} // CopyTask
