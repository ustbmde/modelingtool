/**
 */
package edu.ustb.sei.mde.taileduml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Action</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.ustb.sei.mde.taileduml.TailedUMLPackage#getAction()
 * @model abstract="true"
 * @generated
 */
public interface Action extends NamedElement, ActivityNode {
} // Action
