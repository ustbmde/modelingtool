/**
 */
package edu.ustb.sei.mde.taileduml;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Activity Node</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.ustb.sei.mde.taileduml.TailedUMLPackage#getActivityNode()
 * @model abstract="true"
 * @generated
 */
public interface ActivityNode extends EObject {
} // ActivityNode
