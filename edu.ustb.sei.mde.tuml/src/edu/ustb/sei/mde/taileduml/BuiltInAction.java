/**
 */
package edu.ustb.sei.mde.taileduml;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Built In Action</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.ustb.sei.mde.taileduml.TailedUMLPackage#getBuiltInAction()
 * @model abstract="true"
 * @generated
 */
public interface BuiltInAction extends Action {
} // BuiltInAction
