/**
 */
package edu.ustb.sei.mde.bxmodel;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Model Container</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.bxmodel.ModelContainer#getElements <em>Elements</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.bxmodel.ModelContainer#getRelationships <em>Relationships</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.mde.bxmodel.BxModelPackage#getModelContainer()
 * @model abstract="true"
 * @generated
 */
public interface ModelContainer extends EObject {
	/**
	 * Returns the value of the '<em><b>Elements</b></em>' containment reference list.
	 * The list contents are of type {@link edu.ustb.sei.mde.bxmodel.GoalElement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Elements</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Elements</em>' containment reference list.
	 * @see edu.ustb.sei.mde.bxmodel.BxModelPackage#getModelContainer_Elements()
	 * @model containment="true"
	 * @generated
	 */
	EList<GoalElement> getElements();

	/**
	 * Returns the value of the '<em><b>Relationships</b></em>' containment reference list.
	 * The list contents are of type {@link edu.ustb.sei.mde.bxmodel.GoalRelationship}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Relationships</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Relationships</em>' containment reference list.
	 * @see edu.ustb.sei.mde.bxmodel.BxModelPackage#getModelContainer_Relationships()
	 * @model containment="true"
	 * @generated
	 */
	EList<GoalRelationship> getRelationships();

} // ModelContainer
