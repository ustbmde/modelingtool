/**
 */
package edu.ustb.sei.mde.bxmodel;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see edu.ustb.sei.mde.bxmodel.BxModelFactory
 * @model kind="package"
 * @generated
 */
public interface BxModelPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "bxmodel";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.ustb.edu.cn/sei/mde/bxmodel";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "bxm";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	BxModelPackage eINSTANCE = edu.ustb.sei.mde.bxmodel.impl.BxModelPackageImpl.init();

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.bxmodel.impl.ModelContainerImpl <em>Model Container</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.bxmodel.impl.ModelContainerImpl
	 * @see edu.ustb.sei.mde.bxmodel.impl.BxModelPackageImpl#getModelContainer()
	 * @generated
	 */
	int MODEL_CONTAINER = 0;

	/**
	 * The feature id for the '<em><b>Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_CONTAINER__ELEMENTS = 0;

	/**
	 * The feature id for the '<em><b>Relationships</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_CONTAINER__RELATIONSHIPS = 1;

	/**
	 * The number of structural features of the '<em>Model Container</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_CONTAINER_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Model Container</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_CONTAINER_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.bxmodel.impl.BxGoalModelImpl <em>Bx Goal Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.bxmodel.impl.BxGoalModelImpl
	 * @see edu.ustb.sei.mde.bxmodel.impl.BxModelPackageImpl#getBxGoalModel()
	 * @generated
	 */
	int BX_GOAL_MODEL = 1;

	/**
	 * The feature id for the '<em><b>Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BX_GOAL_MODEL__ELEMENTS = MODEL_CONTAINER__ELEMENTS;

	/**
	 * The feature id for the '<em><b>Relationships</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BX_GOAL_MODEL__RELATIONSHIPS = MODEL_CONTAINER__RELATIONSHIPS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BX_GOAL_MODEL__NAME = MODEL_CONTAINER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Bx Goal Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BX_GOAL_MODEL_FEATURE_COUNT = MODEL_CONTAINER_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Bx Goal Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BX_GOAL_MODEL_OPERATION_COUNT = MODEL_CONTAINER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.bxmodel.impl.GoalElementImpl <em>Goal Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.bxmodel.impl.GoalElementImpl
	 * @see edu.ustb.sei.mde.bxmodel.impl.BxModelPackageImpl#getGoalElement()
	 * @generated
	 */
	int GOAL_ELEMENT = 2;

	/**
	 * The number of structural features of the '<em>Goal Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOAL_ELEMENT_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Goal Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOAL_ELEMENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.bxmodel.impl.GoalImpl <em>Goal</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.bxmodel.impl.GoalImpl
	 * @see edu.ustb.sei.mde.bxmodel.impl.BxModelPackageImpl#getGoal()
	 * @generated
	 */
	int GOAL = 3;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOAL__DESCRIPTION = GOAL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Goal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOAL_FEATURE_COUNT = GOAL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Goal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOAL_OPERATION_COUNT = GOAL_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.bxmodel.impl.TaskImpl <em>Task</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.bxmodel.impl.TaskImpl
	 * @see edu.ustb.sei.mde.bxmodel.impl.BxModelPackageImpl#getTask()
	 * @generated
	 */
	int TASK = 4;

	/**
	 * The number of structural features of the '<em>Task</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_FEATURE_COUNT = GOAL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Task</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_OPERATION_COUNT = GOAL_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.bxmodel.impl.PutbackTaskImpl <em>Putback Task</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.bxmodel.impl.PutbackTaskImpl
	 * @see edu.ustb.sei.mde.bxmodel.impl.BxModelPackageImpl#getPutbackTask()
	 * @generated
	 */
	int PUTBACK_TASK = 5;

	/**
	 * The feature id for the '<em><b>Source</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUTBACK_TASK__SOURCE = TASK_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>View</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUTBACK_TASK__VIEW = TASK_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Putback Task</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUTBACK_TASK_FEATURE_COUNT = TASK_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Putback Task</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PUTBACK_TASK_OPERATION_COUNT = TASK_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.bxmodel.impl.SoftGoalImpl <em>Soft Goal</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.bxmodel.impl.SoftGoalImpl
	 * @see edu.ustb.sei.mde.bxmodel.impl.BxModelPackageImpl#getSoftGoal()
	 * @generated
	 */
	int SOFT_GOAL = 6;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFT_GOAL__DESCRIPTION = GOAL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFT_GOAL__TYPE = GOAL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Soft Goal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFT_GOAL_FEATURE_COUNT = GOAL_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Soft Goal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFT_GOAL_OPERATION_COUNT = GOAL_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.bxmodel.impl.OperationTaskImpl <em>Operation Task</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.bxmodel.impl.OperationTaskImpl
	 * @see edu.ustb.sei.mde.bxmodel.impl.BxModelPackageImpl#getOperationTask()
	 * @generated
	 */
	int OPERATION_TASK = 7;

	/**
	 * The feature id for the '<em><b>Operation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_TASK__OPERATION = TASK_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Operation Task</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_TASK_FEATURE_COUNT = TASK_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Operation Task</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPERATION_TASK_OPERATION_COUNT = TASK_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.bxmodel.impl.ConditionTaskImpl <em>Condition Task</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.bxmodel.impl.ConditionTaskImpl
	 * @see edu.ustb.sei.mde.bxmodel.impl.BxModelPackageImpl#getConditionTask()
	 * @generated
	 */
	int CONDITION_TASK = 8;

	/**
	 * The feature id for the '<em><b>Condition</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION_TASK__CONDITION = TASK_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Condition Task</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION_TASK_FEATURE_COUNT = TASK_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Condition Task</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION_TASK_OPERATION_COUNT = TASK_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.bxmodel.impl.BeliefImpl <em>Belief</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.bxmodel.impl.BeliefImpl
	 * @see edu.ustb.sei.mde.bxmodel.impl.BxModelPackageImpl#getBelief()
	 * @generated
	 */
	int BELIEF = 9;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BELIEF__DESCRIPTION = GOAL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Belief</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BELIEF_FEATURE_COUNT = GOAL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Belief</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BELIEF_OPERATION_COUNT = GOAL_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.bxmodel.impl.ContributionSourceImpl <em>Contribution Source</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.bxmodel.impl.ContributionSourceImpl
	 * @see edu.ustb.sei.mde.bxmodel.impl.BxModelPackageImpl#getContributionSource()
	 * @generated
	 */
	int CONTRIBUTION_SOURCE = 17;

	/**
	 * The number of structural features of the '<em>Contribution Source</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTRIBUTION_SOURCE_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Contribution Source</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTRIBUTION_SOURCE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.bxmodel.impl.GoalRelationshipImpl <em>Goal Relationship</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.bxmodel.impl.GoalRelationshipImpl
	 * @see edu.ustb.sei.mde.bxmodel.impl.BxModelPackageImpl#getGoalRelationship()
	 * @generated
	 */
	int GOAL_RELATIONSHIP = 10;

	/**
	 * The number of structural features of the '<em>Goal Relationship</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOAL_RELATIONSHIP_FEATURE_COUNT = CONTRIBUTION_SOURCE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Goal Relationship</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GOAL_RELATIONSHIP_OPERATION_COUNT = CONTRIBUTION_SOURCE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.bxmodel.impl.ContributionImpl <em>Contribution</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.bxmodel.impl.ContributionImpl
	 * @see edu.ustb.sei.mde.bxmodel.impl.BxModelPackageImpl#getContribution()
	 * @generated
	 */
	int CONTRIBUTION = 11;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTRIBUTION__TYPE = GOAL_RELATIONSHIP_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTRIBUTION__SOURCE = GOAL_RELATIONSHIP_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTRIBUTION__TARGET = GOAL_RELATIONSHIP_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Contribution</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTRIBUTION_FEATURE_COUNT = GOAL_RELATIONSHIP_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Contribution</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTRIBUTION_OPERATION_COUNT = GOAL_RELATIONSHIP_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.bxmodel.impl.DecompositionImpl <em>Decomposition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.bxmodel.impl.DecompositionImpl
	 * @see edu.ustb.sei.mde.bxmodel.impl.BxModelPackageImpl#getDecomposition()
	 * @generated
	 */
	int DECOMPOSITION = 12;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECOMPOSITION__SOURCE = GOAL_RELATIONSHIP_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECOMPOSITION__TARGET = GOAL_RELATIONSHIP_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Decomposition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECOMPOSITION_FEATURE_COUNT = GOAL_RELATIONSHIP_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Decomposition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECOMPOSITION_OPERATION_COUNT = GOAL_RELATIONSHIP_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.bxmodel.impl.MeansEndsImpl <em>Means Ends</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.bxmodel.impl.MeansEndsImpl
	 * @see edu.ustb.sei.mde.bxmodel.impl.BxModelPackageImpl#getMeansEnds()
	 * @generated
	 */
	int MEANS_ENDS = 13;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEANS_ENDS__SOURCE = GOAL_RELATIONSHIP_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEANS_ENDS__TARGET = GOAL_RELATIONSHIP_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Means Ends</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEANS_ENDS_FEATURE_COUNT = GOAL_RELATIONSHIP_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Means Ends</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEANS_ENDS_OPERATION_COUNT = GOAL_RELATIONSHIP_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.bxmodel.impl.CorrelationImpl <em>Correlation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.bxmodel.impl.CorrelationImpl
	 * @see edu.ustb.sei.mde.bxmodel.impl.BxModelPackageImpl#getCorrelation()
	 * @generated
	 */
	int CORRELATION = 14;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORRELATION__SOURCE = GOAL_RELATIONSHIP_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORRELATION__TARGET = GOAL_RELATIONSHIP_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORRELATION__TYPE = GOAL_RELATIONSHIP_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Correlation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORRELATION_FEATURE_COUNT = GOAL_RELATIONSHIP_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Correlation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORRELATION_OPERATION_COUNT = GOAL_RELATIONSHIP_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.bxmodel.impl.DecompositionSourceImpl <em>Decomposition Source</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.bxmodel.impl.DecompositionSourceImpl
	 * @see edu.ustb.sei.mde.bxmodel.impl.BxModelPackageImpl#getDecompositionSource()
	 * @generated
	 */
	int DECOMPOSITION_SOURCE = 15;

	/**
	 * The number of structural features of the '<em>Decomposition Source</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECOMPOSITION_SOURCE_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Decomposition Source</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECOMPOSITION_SOURCE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.bxmodel.impl.DecompositionTargetImpl <em>Decomposition Target</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.bxmodel.impl.DecompositionTargetImpl
	 * @see edu.ustb.sei.mde.bxmodel.impl.BxModelPackageImpl#getDecompositionTarget()
	 * @generated
	 */
	int DECOMPOSITION_TARGET = 16;

	/**
	 * The number of structural features of the '<em>Decomposition Target</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECOMPOSITION_TARGET_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Decomposition Target</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DECOMPOSITION_TARGET_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.bxmodel.impl.ContributionTargetImpl <em>Contribution Target</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.bxmodel.impl.ContributionTargetImpl
	 * @see edu.ustb.sei.mde.bxmodel.impl.BxModelPackageImpl#getContributionTarget()
	 * @generated
	 */
	int CONTRIBUTION_TARGET = 18;

	/**
	 * The number of structural features of the '<em>Contribution Target</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTRIBUTION_TARGET_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Contribution Target</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTRIBUTION_TARGET_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.bxmodel.impl.MeansEndsSourceImpl <em>Means Ends Source</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.bxmodel.impl.MeansEndsSourceImpl
	 * @see edu.ustb.sei.mde.bxmodel.impl.BxModelPackageImpl#getMeansEndsSource()
	 * @generated
	 */
	int MEANS_ENDS_SOURCE = 19;

	/**
	 * The number of structural features of the '<em>Means Ends Source</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEANS_ENDS_SOURCE_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Means Ends Source</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEANS_ENDS_SOURCE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.bxmodel.impl.MeansEndsTargetImpl <em>Means Ends Target</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.bxmodel.impl.MeansEndsTargetImpl
	 * @see edu.ustb.sei.mde.bxmodel.impl.BxModelPackageImpl#getMeansEndsTarget()
	 * @generated
	 */
	int MEANS_ENDS_TARGET = 20;

	/**
	 * The number of structural features of the '<em>Means Ends Target</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEANS_ENDS_TARGET_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Means Ends Target</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEANS_ENDS_TARGET_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.bxmodel.impl.CorrelationSourceImpl <em>Correlation Source</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.bxmodel.impl.CorrelationSourceImpl
	 * @see edu.ustb.sei.mde.bxmodel.impl.BxModelPackageImpl#getCorrelationSource()
	 * @generated
	 */
	int CORRELATION_SOURCE = 21;

	/**
	 * The number of structural features of the '<em>Correlation Source</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORRELATION_SOURCE_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Correlation Source</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORRELATION_SOURCE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.bxmodel.impl.CorrelationTargetImpl <em>Correlation Target</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.bxmodel.impl.CorrelationTargetImpl
	 * @see edu.ustb.sei.mde.bxmodel.impl.BxModelPackageImpl#getCorrelationTarget()
	 * @generated
	 */
	int CORRELATION_TARGET = 22;

	/**
	 * The number of structural features of the '<em>Correlation Target</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORRELATION_TARGET_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Correlation Target</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CORRELATION_TARGET_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.bxmodel.impl.MultiplicityGateImpl <em>Multiplicity Gate</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.bxmodel.impl.MultiplicityGateImpl
	 * @see edu.ustb.sei.mde.bxmodel.impl.BxModelPackageImpl#getMultiplicityGate()
	 * @generated
	 */
	int MULTIPLICITY_GATE = 23;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTIPLICITY_GATE__TYPE = GOAL_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Multiplicity Gate</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTIPLICITY_GATE_FEATURE_COUNT = GOAL_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Multiplicity Gate</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MULTIPLICITY_GATE_OPERATION_COUNT = GOAL_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.bxmodel.SoftGoalType <em>Soft Goal Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.bxmodel.SoftGoalType
	 * @see edu.ustb.sei.mde.bxmodel.impl.BxModelPackageImpl#getSoftGoalType()
	 * @generated
	 */
	int SOFT_GOAL_TYPE = 24;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.bxmodel.ContributionType <em>Contribution Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.bxmodel.ContributionType
	 * @see edu.ustb.sei.mde.bxmodel.impl.BxModelPackageImpl#getContributionType()
	 * @generated
	 */
	int CONTRIBUTION_TYPE = 25;

	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.bxmodel.CorrelationType <em>Correlation Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.bxmodel.CorrelationType
	 * @see edu.ustb.sei.mde.bxmodel.impl.BxModelPackageImpl#getCorrelationType()
	 * @generated
	 */
	int CORRELATION_TYPE = 26;


	/**
	 * The meta object id for the '{@link edu.ustb.sei.mde.bxmodel.MultiplicityGateKind <em>Multiplicity Gate Kind</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see edu.ustb.sei.mde.bxmodel.MultiplicityGateKind
	 * @see edu.ustb.sei.mde.bxmodel.impl.BxModelPackageImpl#getMultiplicityGateKind()
	 * @generated
	 */
	int MULTIPLICITY_GATE_KIND = 27;


	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.bxmodel.ModelContainer <em>Model Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Model Container</em>'.
	 * @see edu.ustb.sei.mde.bxmodel.ModelContainer
	 * @generated
	 */
	EClass getModelContainer();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.ustb.sei.mde.bxmodel.ModelContainer#getElements <em>Elements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Elements</em>'.
	 * @see edu.ustb.sei.mde.bxmodel.ModelContainer#getElements()
	 * @see #getModelContainer()
	 * @generated
	 */
	EReference getModelContainer_Elements();

	/**
	 * Returns the meta object for the containment reference list '{@link edu.ustb.sei.mde.bxmodel.ModelContainer#getRelationships <em>Relationships</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Relationships</em>'.
	 * @see edu.ustb.sei.mde.bxmodel.ModelContainer#getRelationships()
	 * @see #getModelContainer()
	 * @generated
	 */
	EReference getModelContainer_Relationships();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.bxmodel.BxGoalModel <em>Bx Goal Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Bx Goal Model</em>'.
	 * @see edu.ustb.sei.mde.bxmodel.BxGoalModel
	 * @generated
	 */
	EClass getBxGoalModel();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.bxmodel.BxGoalModel#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see edu.ustb.sei.mde.bxmodel.BxGoalModel#getName()
	 * @see #getBxGoalModel()
	 * @generated
	 */
	EAttribute getBxGoalModel_Name();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.bxmodel.GoalElement <em>Goal Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Goal Element</em>'.
	 * @see edu.ustb.sei.mde.bxmodel.GoalElement
	 * @generated
	 */
	EClass getGoalElement();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.bxmodel.Goal <em>Goal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Goal</em>'.
	 * @see edu.ustb.sei.mde.bxmodel.Goal
	 * @generated
	 */
	EClass getGoal();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.bxmodel.Goal#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see edu.ustb.sei.mde.bxmodel.Goal#getDescription()
	 * @see #getGoal()
	 * @generated
	 */
	EAttribute getGoal_Description();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.bxmodel.Task <em>Task</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Task</em>'.
	 * @see edu.ustb.sei.mde.bxmodel.Task
	 * @generated
	 */
	EClass getTask();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.bxmodel.PutbackTask <em>Putback Task</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Putback Task</em>'.
	 * @see edu.ustb.sei.mde.bxmodel.PutbackTask
	 * @generated
	 */
	EClass getPutbackTask();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.bxmodel.PutbackTask#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Source</em>'.
	 * @see edu.ustb.sei.mde.bxmodel.PutbackTask#getSource()
	 * @see #getPutbackTask()
	 * @generated
	 */
	EAttribute getPutbackTask_Source();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.bxmodel.PutbackTask#getView <em>View</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>View</em>'.
	 * @see edu.ustb.sei.mde.bxmodel.PutbackTask#getView()
	 * @see #getPutbackTask()
	 * @generated
	 */
	EAttribute getPutbackTask_View();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.bxmodel.SoftGoal <em>Soft Goal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Soft Goal</em>'.
	 * @see edu.ustb.sei.mde.bxmodel.SoftGoal
	 * @generated
	 */
	EClass getSoftGoal();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.bxmodel.SoftGoal#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see edu.ustb.sei.mde.bxmodel.SoftGoal#getDescription()
	 * @see #getSoftGoal()
	 * @generated
	 */
	EAttribute getSoftGoal_Description();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.bxmodel.SoftGoal#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see edu.ustb.sei.mde.bxmodel.SoftGoal#getType()
	 * @see #getSoftGoal()
	 * @generated
	 */
	EAttribute getSoftGoal_Type();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.bxmodel.OperationTask <em>Operation Task</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Operation Task</em>'.
	 * @see edu.ustb.sei.mde.bxmodel.OperationTask
	 * @generated
	 */
	EClass getOperationTask();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.bxmodel.OperationTask#getOperation <em>Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Operation</em>'.
	 * @see edu.ustb.sei.mde.bxmodel.OperationTask#getOperation()
	 * @see #getOperationTask()
	 * @generated
	 */
	EAttribute getOperationTask_Operation();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.bxmodel.ConditionTask <em>Condition Task</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Condition Task</em>'.
	 * @see edu.ustb.sei.mde.bxmodel.ConditionTask
	 * @generated
	 */
	EClass getConditionTask();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.bxmodel.ConditionTask#getCondition <em>Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Condition</em>'.
	 * @see edu.ustb.sei.mde.bxmodel.ConditionTask#getCondition()
	 * @see #getConditionTask()
	 * @generated
	 */
	EAttribute getConditionTask_Condition();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.bxmodel.Belief <em>Belief</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Belief</em>'.
	 * @see edu.ustb.sei.mde.bxmodel.Belief
	 * @generated
	 */
	EClass getBelief();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.bxmodel.Belief#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see edu.ustb.sei.mde.bxmodel.Belief#getDescription()
	 * @see #getBelief()
	 * @generated
	 */
	EAttribute getBelief_Description();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.bxmodel.GoalRelationship <em>Goal Relationship</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Goal Relationship</em>'.
	 * @see edu.ustb.sei.mde.bxmodel.GoalRelationship
	 * @generated
	 */
	EClass getGoalRelationship();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.bxmodel.Contribution <em>Contribution</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Contribution</em>'.
	 * @see edu.ustb.sei.mde.bxmodel.Contribution
	 * @generated
	 */
	EClass getContribution();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.bxmodel.Contribution#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see edu.ustb.sei.mde.bxmodel.Contribution#getType()
	 * @see #getContribution()
	 * @generated
	 */
	EAttribute getContribution_Type();

	/**
	 * Returns the meta object for the reference '{@link edu.ustb.sei.mde.bxmodel.Contribution#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Source</em>'.
	 * @see edu.ustb.sei.mde.bxmodel.Contribution#getSource()
	 * @see #getContribution()
	 * @generated
	 */
	EReference getContribution_Source();

	/**
	 * Returns the meta object for the reference '{@link edu.ustb.sei.mde.bxmodel.Contribution#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target</em>'.
	 * @see edu.ustb.sei.mde.bxmodel.Contribution#getTarget()
	 * @see #getContribution()
	 * @generated
	 */
	EReference getContribution_Target();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.bxmodel.Decomposition <em>Decomposition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Decomposition</em>'.
	 * @see edu.ustb.sei.mde.bxmodel.Decomposition
	 * @generated
	 */
	EClass getDecomposition();

	/**
	 * Returns the meta object for the reference '{@link edu.ustb.sei.mde.bxmodel.Decomposition#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Source</em>'.
	 * @see edu.ustb.sei.mde.bxmodel.Decomposition#getSource()
	 * @see #getDecomposition()
	 * @generated
	 */
	EReference getDecomposition_Source();

	/**
	 * Returns the meta object for the reference '{@link edu.ustb.sei.mde.bxmodel.Decomposition#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target</em>'.
	 * @see edu.ustb.sei.mde.bxmodel.Decomposition#getTarget()
	 * @see #getDecomposition()
	 * @generated
	 */
	EReference getDecomposition_Target();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.bxmodel.MeansEnds <em>Means Ends</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Means Ends</em>'.
	 * @see edu.ustb.sei.mde.bxmodel.MeansEnds
	 * @generated
	 */
	EClass getMeansEnds();

	/**
	 * Returns the meta object for the reference '{@link edu.ustb.sei.mde.bxmodel.MeansEnds#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Source</em>'.
	 * @see edu.ustb.sei.mde.bxmodel.MeansEnds#getSource()
	 * @see #getMeansEnds()
	 * @generated
	 */
	EReference getMeansEnds_Source();

	/**
	 * Returns the meta object for the reference '{@link edu.ustb.sei.mde.bxmodel.MeansEnds#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target</em>'.
	 * @see edu.ustb.sei.mde.bxmodel.MeansEnds#getTarget()
	 * @see #getMeansEnds()
	 * @generated
	 */
	EReference getMeansEnds_Target();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.bxmodel.Correlation <em>Correlation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Correlation</em>'.
	 * @see edu.ustb.sei.mde.bxmodel.Correlation
	 * @generated
	 */
	EClass getCorrelation();

	/**
	 * Returns the meta object for the reference '{@link edu.ustb.sei.mde.bxmodel.Correlation#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Source</em>'.
	 * @see edu.ustb.sei.mde.bxmodel.Correlation#getSource()
	 * @see #getCorrelation()
	 * @generated
	 */
	EReference getCorrelation_Source();

	/**
	 * Returns the meta object for the reference '{@link edu.ustb.sei.mde.bxmodel.Correlation#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target</em>'.
	 * @see edu.ustb.sei.mde.bxmodel.Correlation#getTarget()
	 * @see #getCorrelation()
	 * @generated
	 */
	EReference getCorrelation_Target();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.bxmodel.Correlation#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see edu.ustb.sei.mde.bxmodel.Correlation#getType()
	 * @see #getCorrelation()
	 * @generated
	 */
	EAttribute getCorrelation_Type();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.bxmodel.DecompositionSource <em>Decomposition Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Decomposition Source</em>'.
	 * @see edu.ustb.sei.mde.bxmodel.DecompositionSource
	 * @generated
	 */
	EClass getDecompositionSource();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.bxmodel.DecompositionTarget <em>Decomposition Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Decomposition Target</em>'.
	 * @see edu.ustb.sei.mde.bxmodel.DecompositionTarget
	 * @generated
	 */
	EClass getDecompositionTarget();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.bxmodel.ContributionSource <em>Contribution Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Contribution Source</em>'.
	 * @see edu.ustb.sei.mde.bxmodel.ContributionSource
	 * @generated
	 */
	EClass getContributionSource();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.bxmodel.ContributionTarget <em>Contribution Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Contribution Target</em>'.
	 * @see edu.ustb.sei.mde.bxmodel.ContributionTarget
	 * @generated
	 */
	EClass getContributionTarget();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.bxmodel.MeansEndsSource <em>Means Ends Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Means Ends Source</em>'.
	 * @see edu.ustb.sei.mde.bxmodel.MeansEndsSource
	 * @generated
	 */
	EClass getMeansEndsSource();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.bxmodel.MeansEndsTarget <em>Means Ends Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Means Ends Target</em>'.
	 * @see edu.ustb.sei.mde.bxmodel.MeansEndsTarget
	 * @generated
	 */
	EClass getMeansEndsTarget();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.bxmodel.CorrelationSource <em>Correlation Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Correlation Source</em>'.
	 * @see edu.ustb.sei.mde.bxmodel.CorrelationSource
	 * @generated
	 */
	EClass getCorrelationSource();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.bxmodel.CorrelationTarget <em>Correlation Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Correlation Target</em>'.
	 * @see edu.ustb.sei.mde.bxmodel.CorrelationTarget
	 * @generated
	 */
	EClass getCorrelationTarget();

	/**
	 * Returns the meta object for class '{@link edu.ustb.sei.mde.bxmodel.MultiplicityGate <em>Multiplicity Gate</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Multiplicity Gate</em>'.
	 * @see edu.ustb.sei.mde.bxmodel.MultiplicityGate
	 * @generated
	 */
	EClass getMultiplicityGate();

	/**
	 * Returns the meta object for the attribute '{@link edu.ustb.sei.mde.bxmodel.MultiplicityGate#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see edu.ustb.sei.mde.bxmodel.MultiplicityGate#getType()
	 * @see #getMultiplicityGate()
	 * @generated
	 */
	EAttribute getMultiplicityGate_Type();

	/**
	 * Returns the meta object for enum '{@link edu.ustb.sei.mde.bxmodel.SoftGoalType <em>Soft Goal Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Soft Goal Type</em>'.
	 * @see edu.ustb.sei.mde.bxmodel.SoftGoalType
	 * @generated
	 */
	EEnum getSoftGoalType();

	/**
	 * Returns the meta object for enum '{@link edu.ustb.sei.mde.bxmodel.ContributionType <em>Contribution Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Contribution Type</em>'.
	 * @see edu.ustb.sei.mde.bxmodel.ContributionType
	 * @generated
	 */
	EEnum getContributionType();

	/**
	 * Returns the meta object for enum '{@link edu.ustb.sei.mde.bxmodel.CorrelationType <em>Correlation Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Correlation Type</em>'.
	 * @see edu.ustb.sei.mde.bxmodel.CorrelationType
	 * @generated
	 */
	EEnum getCorrelationType();

	/**
	 * Returns the meta object for enum '{@link edu.ustb.sei.mde.bxmodel.MultiplicityGateKind <em>Multiplicity Gate Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Multiplicity Gate Kind</em>'.
	 * @see edu.ustb.sei.mde.bxmodel.MultiplicityGateKind
	 * @generated
	 */
	EEnum getMultiplicityGateKind();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	BxModelFactory getBxModelFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.bxmodel.impl.ModelContainerImpl <em>Model Container</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.bxmodel.impl.ModelContainerImpl
		 * @see edu.ustb.sei.mde.bxmodel.impl.BxModelPackageImpl#getModelContainer()
		 * @generated
		 */
		EClass MODEL_CONTAINER = eINSTANCE.getModelContainer();

		/**
		 * The meta object literal for the '<em><b>Elements</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MODEL_CONTAINER__ELEMENTS = eINSTANCE.getModelContainer_Elements();

		/**
		 * The meta object literal for the '<em><b>Relationships</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MODEL_CONTAINER__RELATIONSHIPS = eINSTANCE.getModelContainer_Relationships();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.bxmodel.impl.BxGoalModelImpl <em>Bx Goal Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.bxmodel.impl.BxGoalModelImpl
		 * @see edu.ustb.sei.mde.bxmodel.impl.BxModelPackageImpl#getBxGoalModel()
		 * @generated
		 */
		EClass BX_GOAL_MODEL = eINSTANCE.getBxGoalModel();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BX_GOAL_MODEL__NAME = eINSTANCE.getBxGoalModel_Name();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.bxmodel.impl.GoalElementImpl <em>Goal Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.bxmodel.impl.GoalElementImpl
		 * @see edu.ustb.sei.mde.bxmodel.impl.BxModelPackageImpl#getGoalElement()
		 * @generated
		 */
		EClass GOAL_ELEMENT = eINSTANCE.getGoalElement();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.bxmodel.impl.GoalImpl <em>Goal</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.bxmodel.impl.GoalImpl
		 * @see edu.ustb.sei.mde.bxmodel.impl.BxModelPackageImpl#getGoal()
		 * @generated
		 */
		EClass GOAL = eINSTANCE.getGoal();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GOAL__DESCRIPTION = eINSTANCE.getGoal_Description();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.bxmodel.impl.TaskImpl <em>Task</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.bxmodel.impl.TaskImpl
		 * @see edu.ustb.sei.mde.bxmodel.impl.BxModelPackageImpl#getTask()
		 * @generated
		 */
		EClass TASK = eINSTANCE.getTask();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.bxmodel.impl.PutbackTaskImpl <em>Putback Task</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.bxmodel.impl.PutbackTaskImpl
		 * @see edu.ustb.sei.mde.bxmodel.impl.BxModelPackageImpl#getPutbackTask()
		 * @generated
		 */
		EClass PUTBACK_TASK = eINSTANCE.getPutbackTask();

		/**
		 * The meta object literal for the '<em><b>Source</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PUTBACK_TASK__SOURCE = eINSTANCE.getPutbackTask_Source();

		/**
		 * The meta object literal for the '<em><b>View</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PUTBACK_TASK__VIEW = eINSTANCE.getPutbackTask_View();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.bxmodel.impl.SoftGoalImpl <em>Soft Goal</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.bxmodel.impl.SoftGoalImpl
		 * @see edu.ustb.sei.mde.bxmodel.impl.BxModelPackageImpl#getSoftGoal()
		 * @generated
		 */
		EClass SOFT_GOAL = eINSTANCE.getSoftGoal();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SOFT_GOAL__DESCRIPTION = eINSTANCE.getSoftGoal_Description();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SOFT_GOAL__TYPE = eINSTANCE.getSoftGoal_Type();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.bxmodel.impl.OperationTaskImpl <em>Operation Task</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.bxmodel.impl.OperationTaskImpl
		 * @see edu.ustb.sei.mde.bxmodel.impl.BxModelPackageImpl#getOperationTask()
		 * @generated
		 */
		EClass OPERATION_TASK = eINSTANCE.getOperationTask();

		/**
		 * The meta object literal for the '<em><b>Operation</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPERATION_TASK__OPERATION = eINSTANCE.getOperationTask_Operation();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.bxmodel.impl.ConditionTaskImpl <em>Condition Task</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.bxmodel.impl.ConditionTaskImpl
		 * @see edu.ustb.sei.mde.bxmodel.impl.BxModelPackageImpl#getConditionTask()
		 * @generated
		 */
		EClass CONDITION_TASK = eINSTANCE.getConditionTask();

		/**
		 * The meta object literal for the '<em><b>Condition</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONDITION_TASK__CONDITION = eINSTANCE.getConditionTask_Condition();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.bxmodel.impl.BeliefImpl <em>Belief</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.bxmodel.impl.BeliefImpl
		 * @see edu.ustb.sei.mde.bxmodel.impl.BxModelPackageImpl#getBelief()
		 * @generated
		 */
		EClass BELIEF = eINSTANCE.getBelief();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BELIEF__DESCRIPTION = eINSTANCE.getBelief_Description();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.bxmodel.impl.GoalRelationshipImpl <em>Goal Relationship</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.bxmodel.impl.GoalRelationshipImpl
		 * @see edu.ustb.sei.mde.bxmodel.impl.BxModelPackageImpl#getGoalRelationship()
		 * @generated
		 */
		EClass GOAL_RELATIONSHIP = eINSTANCE.getGoalRelationship();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.bxmodel.impl.ContributionImpl <em>Contribution</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.bxmodel.impl.ContributionImpl
		 * @see edu.ustb.sei.mde.bxmodel.impl.BxModelPackageImpl#getContribution()
		 * @generated
		 */
		EClass CONTRIBUTION = eINSTANCE.getContribution();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONTRIBUTION__TYPE = eINSTANCE.getContribution_Type();

		/**
		 * The meta object literal for the '<em><b>Source</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONTRIBUTION__SOURCE = eINSTANCE.getContribution_Source();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONTRIBUTION__TARGET = eINSTANCE.getContribution_Target();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.bxmodel.impl.DecompositionImpl <em>Decomposition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.bxmodel.impl.DecompositionImpl
		 * @see edu.ustb.sei.mde.bxmodel.impl.BxModelPackageImpl#getDecomposition()
		 * @generated
		 */
		EClass DECOMPOSITION = eINSTANCE.getDecomposition();

		/**
		 * The meta object literal for the '<em><b>Source</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DECOMPOSITION__SOURCE = eINSTANCE.getDecomposition_Source();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DECOMPOSITION__TARGET = eINSTANCE.getDecomposition_Target();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.bxmodel.impl.MeansEndsImpl <em>Means Ends</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.bxmodel.impl.MeansEndsImpl
		 * @see edu.ustb.sei.mde.bxmodel.impl.BxModelPackageImpl#getMeansEnds()
		 * @generated
		 */
		EClass MEANS_ENDS = eINSTANCE.getMeansEnds();

		/**
		 * The meta object literal for the '<em><b>Source</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MEANS_ENDS__SOURCE = eINSTANCE.getMeansEnds_Source();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MEANS_ENDS__TARGET = eINSTANCE.getMeansEnds_Target();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.bxmodel.impl.CorrelationImpl <em>Correlation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.bxmodel.impl.CorrelationImpl
		 * @see edu.ustb.sei.mde.bxmodel.impl.BxModelPackageImpl#getCorrelation()
		 * @generated
		 */
		EClass CORRELATION = eINSTANCE.getCorrelation();

		/**
		 * The meta object literal for the '<em><b>Source</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CORRELATION__SOURCE = eINSTANCE.getCorrelation_Source();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CORRELATION__TARGET = eINSTANCE.getCorrelation_Target();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CORRELATION__TYPE = eINSTANCE.getCorrelation_Type();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.bxmodel.impl.DecompositionSourceImpl <em>Decomposition Source</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.bxmodel.impl.DecompositionSourceImpl
		 * @see edu.ustb.sei.mde.bxmodel.impl.BxModelPackageImpl#getDecompositionSource()
		 * @generated
		 */
		EClass DECOMPOSITION_SOURCE = eINSTANCE.getDecompositionSource();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.bxmodel.impl.DecompositionTargetImpl <em>Decomposition Target</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.bxmodel.impl.DecompositionTargetImpl
		 * @see edu.ustb.sei.mde.bxmodel.impl.BxModelPackageImpl#getDecompositionTarget()
		 * @generated
		 */
		EClass DECOMPOSITION_TARGET = eINSTANCE.getDecompositionTarget();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.bxmodel.impl.ContributionSourceImpl <em>Contribution Source</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.bxmodel.impl.ContributionSourceImpl
		 * @see edu.ustb.sei.mde.bxmodel.impl.BxModelPackageImpl#getContributionSource()
		 * @generated
		 */
		EClass CONTRIBUTION_SOURCE = eINSTANCE.getContributionSource();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.bxmodel.impl.ContributionTargetImpl <em>Contribution Target</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.bxmodel.impl.ContributionTargetImpl
		 * @see edu.ustb.sei.mde.bxmodel.impl.BxModelPackageImpl#getContributionTarget()
		 * @generated
		 */
		EClass CONTRIBUTION_TARGET = eINSTANCE.getContributionTarget();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.bxmodel.impl.MeansEndsSourceImpl <em>Means Ends Source</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.bxmodel.impl.MeansEndsSourceImpl
		 * @see edu.ustb.sei.mde.bxmodel.impl.BxModelPackageImpl#getMeansEndsSource()
		 * @generated
		 */
		EClass MEANS_ENDS_SOURCE = eINSTANCE.getMeansEndsSource();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.bxmodel.impl.MeansEndsTargetImpl <em>Means Ends Target</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.bxmodel.impl.MeansEndsTargetImpl
		 * @see edu.ustb.sei.mde.bxmodel.impl.BxModelPackageImpl#getMeansEndsTarget()
		 * @generated
		 */
		EClass MEANS_ENDS_TARGET = eINSTANCE.getMeansEndsTarget();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.bxmodel.impl.CorrelationSourceImpl <em>Correlation Source</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.bxmodel.impl.CorrelationSourceImpl
		 * @see edu.ustb.sei.mde.bxmodel.impl.BxModelPackageImpl#getCorrelationSource()
		 * @generated
		 */
		EClass CORRELATION_SOURCE = eINSTANCE.getCorrelationSource();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.bxmodel.impl.CorrelationTargetImpl <em>Correlation Target</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.bxmodel.impl.CorrelationTargetImpl
		 * @see edu.ustb.sei.mde.bxmodel.impl.BxModelPackageImpl#getCorrelationTarget()
		 * @generated
		 */
		EClass CORRELATION_TARGET = eINSTANCE.getCorrelationTarget();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.bxmodel.impl.MultiplicityGateImpl <em>Multiplicity Gate</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.bxmodel.impl.MultiplicityGateImpl
		 * @see edu.ustb.sei.mde.bxmodel.impl.BxModelPackageImpl#getMultiplicityGate()
		 * @generated
		 */
		EClass MULTIPLICITY_GATE = eINSTANCE.getMultiplicityGate();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MULTIPLICITY_GATE__TYPE = eINSTANCE.getMultiplicityGate_Type();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.bxmodel.SoftGoalType <em>Soft Goal Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.bxmodel.SoftGoalType
		 * @see edu.ustb.sei.mde.bxmodel.impl.BxModelPackageImpl#getSoftGoalType()
		 * @generated
		 */
		EEnum SOFT_GOAL_TYPE = eINSTANCE.getSoftGoalType();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.bxmodel.ContributionType <em>Contribution Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.bxmodel.ContributionType
		 * @see edu.ustb.sei.mde.bxmodel.impl.BxModelPackageImpl#getContributionType()
		 * @generated
		 */
		EEnum CONTRIBUTION_TYPE = eINSTANCE.getContributionType();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.bxmodel.CorrelationType <em>Correlation Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.bxmodel.CorrelationType
		 * @see edu.ustb.sei.mde.bxmodel.impl.BxModelPackageImpl#getCorrelationType()
		 * @generated
		 */
		EEnum CORRELATION_TYPE = eINSTANCE.getCorrelationType();

		/**
		 * The meta object literal for the '{@link edu.ustb.sei.mde.bxmodel.MultiplicityGateKind <em>Multiplicity Gate Kind</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see edu.ustb.sei.mde.bxmodel.MultiplicityGateKind
		 * @see edu.ustb.sei.mde.bxmodel.impl.BxModelPackageImpl#getMultiplicityGateKind()
		 * @generated
		 */
		EEnum MULTIPLICITY_GATE_KIND = eINSTANCE.getMultiplicityGateKind();

	}

} //BxModelPackage
