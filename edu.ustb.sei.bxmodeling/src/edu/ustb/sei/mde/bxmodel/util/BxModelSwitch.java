/**
 */
package edu.ustb.sei.mde.bxmodel.util;

import edu.ustb.sei.mde.bxmodel.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see edu.ustb.sei.mde.bxmodel.BxModelPackage
 * @generated
 */
public class BxModelSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static BxModelPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BxModelSwitch() {
		if (modelPackage == null) {
			modelPackage = BxModelPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case BxModelPackage.MODEL_CONTAINER: {
				ModelContainer modelContainer = (ModelContainer)theEObject;
				T result = caseModelContainer(modelContainer);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BxModelPackage.BX_GOAL_MODEL: {
				BxGoalModel bxGoalModel = (BxGoalModel)theEObject;
				T result = caseBxGoalModel(bxGoalModel);
				if (result == null) result = caseModelContainer(bxGoalModel);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BxModelPackage.GOAL_ELEMENT: {
				GoalElement goalElement = (GoalElement)theEObject;
				T result = caseGoalElement(goalElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BxModelPackage.GOAL: {
				Goal goal = (Goal)theEObject;
				T result = caseGoal(goal);
				if (result == null) result = caseGoalElement(goal);
				if (result == null) result = caseDecompositionSource(goal);
				if (result == null) result = caseDecompositionTarget(goal);
				if (result == null) result = caseMeansEndsTarget(goal);
				if (result == null) result = caseCorrelationTarget(goal);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BxModelPackage.TASK: {
				Task task = (Task)theEObject;
				T result = caseTask(task);
				if (result == null) result = caseGoalElement(task);
				if (result == null) result = caseDecompositionSource(task);
				if (result == null) result = caseDecompositionTarget(task);
				if (result == null) result = caseMeansEndsSource(task);
				if (result == null) result = caseContributionSource(task);
				if (result == null) result = caseCorrelationSource(task);
				if (result == null) result = caseCorrelationTarget(task);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BxModelPackage.PUTBACK_TASK: {
				PutbackTask putbackTask = (PutbackTask)theEObject;
				T result = casePutbackTask(putbackTask);
				if (result == null) result = caseTask(putbackTask);
				if (result == null) result = caseGoalElement(putbackTask);
				if (result == null) result = caseDecompositionSource(putbackTask);
				if (result == null) result = caseDecompositionTarget(putbackTask);
				if (result == null) result = caseMeansEndsSource(putbackTask);
				if (result == null) result = caseContributionSource(putbackTask);
				if (result == null) result = caseCorrelationSource(putbackTask);
				if (result == null) result = caseCorrelationTarget(putbackTask);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BxModelPackage.SOFT_GOAL: {
				SoftGoal softGoal = (SoftGoal)theEObject;
				T result = caseSoftGoal(softGoal);
				if (result == null) result = caseGoalElement(softGoal);
				if (result == null) result = caseDecompositionSource(softGoal);
				if (result == null) result = caseContributionSource(softGoal);
				if (result == null) result = caseContributionTarget(softGoal);
				if (result == null) result = caseCorrelationSource(softGoal);
				if (result == null) result = caseCorrelationTarget(softGoal);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BxModelPackage.OPERATION_TASK: {
				OperationTask operationTask = (OperationTask)theEObject;
				T result = caseOperationTask(operationTask);
				if (result == null) result = caseTask(operationTask);
				if (result == null) result = caseGoalElement(operationTask);
				if (result == null) result = caseDecompositionSource(operationTask);
				if (result == null) result = caseDecompositionTarget(operationTask);
				if (result == null) result = caseMeansEndsSource(operationTask);
				if (result == null) result = caseContributionSource(operationTask);
				if (result == null) result = caseCorrelationSource(operationTask);
				if (result == null) result = caseCorrelationTarget(operationTask);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BxModelPackage.CONDITION_TASK: {
				ConditionTask conditionTask = (ConditionTask)theEObject;
				T result = caseConditionTask(conditionTask);
				if (result == null) result = caseTask(conditionTask);
				if (result == null) result = caseGoalElement(conditionTask);
				if (result == null) result = caseDecompositionSource(conditionTask);
				if (result == null) result = caseDecompositionTarget(conditionTask);
				if (result == null) result = caseMeansEndsSource(conditionTask);
				if (result == null) result = caseContributionSource(conditionTask);
				if (result == null) result = caseCorrelationSource(conditionTask);
				if (result == null) result = caseCorrelationTarget(conditionTask);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BxModelPackage.BELIEF: {
				Belief belief = (Belief)theEObject;
				T result = caseBelief(belief);
				if (result == null) result = caseGoalElement(belief);
				if (result == null) result = caseContributionSource(belief);
				if (result == null) result = caseContributionTarget(belief);
				if (result == null) result = caseDecompositionSource(belief);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BxModelPackage.GOAL_RELATIONSHIP: {
				GoalRelationship goalRelationship = (GoalRelationship)theEObject;
				T result = caseGoalRelationship(goalRelationship);
				if (result == null) result = caseContributionSource(goalRelationship);
				if (result == null) result = caseContributionTarget(goalRelationship);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BxModelPackage.CONTRIBUTION: {
				Contribution contribution = (Contribution)theEObject;
				T result = caseContribution(contribution);
				if (result == null) result = caseGoalRelationship(contribution);
				if (result == null) result = caseContributionSource(contribution);
				if (result == null) result = caseContributionTarget(contribution);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BxModelPackage.DECOMPOSITION: {
				Decomposition decomposition = (Decomposition)theEObject;
				T result = caseDecomposition(decomposition);
				if (result == null) result = caseGoalRelationship(decomposition);
				if (result == null) result = caseContributionSource(decomposition);
				if (result == null) result = caseContributionTarget(decomposition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BxModelPackage.MEANS_ENDS: {
				MeansEnds meansEnds = (MeansEnds)theEObject;
				T result = caseMeansEnds(meansEnds);
				if (result == null) result = caseGoalRelationship(meansEnds);
				if (result == null) result = caseContributionSource(meansEnds);
				if (result == null) result = caseContributionTarget(meansEnds);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BxModelPackage.CORRELATION: {
				Correlation correlation = (Correlation)theEObject;
				T result = caseCorrelation(correlation);
				if (result == null) result = caseGoalRelationship(correlation);
				if (result == null) result = caseContributionSource(correlation);
				if (result == null) result = caseContributionTarget(correlation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BxModelPackage.DECOMPOSITION_SOURCE: {
				DecompositionSource decompositionSource = (DecompositionSource)theEObject;
				T result = caseDecompositionSource(decompositionSource);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BxModelPackage.DECOMPOSITION_TARGET: {
				DecompositionTarget decompositionTarget = (DecompositionTarget)theEObject;
				T result = caseDecompositionTarget(decompositionTarget);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BxModelPackage.CONTRIBUTION_SOURCE: {
				ContributionSource contributionSource = (ContributionSource)theEObject;
				T result = caseContributionSource(contributionSource);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BxModelPackage.CONTRIBUTION_TARGET: {
				ContributionTarget contributionTarget = (ContributionTarget)theEObject;
				T result = caseContributionTarget(contributionTarget);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BxModelPackage.MEANS_ENDS_SOURCE: {
				MeansEndsSource meansEndsSource = (MeansEndsSource)theEObject;
				T result = caseMeansEndsSource(meansEndsSource);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BxModelPackage.MEANS_ENDS_TARGET: {
				MeansEndsTarget meansEndsTarget = (MeansEndsTarget)theEObject;
				T result = caseMeansEndsTarget(meansEndsTarget);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BxModelPackage.CORRELATION_SOURCE: {
				CorrelationSource correlationSource = (CorrelationSource)theEObject;
				T result = caseCorrelationSource(correlationSource);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BxModelPackage.CORRELATION_TARGET: {
				CorrelationTarget correlationTarget = (CorrelationTarget)theEObject;
				T result = caseCorrelationTarget(correlationTarget);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case BxModelPackage.MULTIPLICITY_GATE: {
				MultiplicityGate multiplicityGate = (MultiplicityGate)theEObject;
				T result = caseMultiplicityGate(multiplicityGate);
				if (result == null) result = caseTask(multiplicityGate);
				if (result == null) result = caseGoalElement(multiplicityGate);
				if (result == null) result = caseDecompositionSource(multiplicityGate);
				if (result == null) result = caseDecompositionTarget(multiplicityGate);
				if (result == null) result = caseMeansEndsSource(multiplicityGate);
				if (result == null) result = caseContributionSource(multiplicityGate);
				if (result == null) result = caseCorrelationSource(multiplicityGate);
				if (result == null) result = caseCorrelationTarget(multiplicityGate);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Model Container</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Model Container</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseModelContainer(ModelContainer object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Bx Goal Model</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Bx Goal Model</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBxGoalModel(BxGoalModel object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Goal Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Goal Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGoalElement(GoalElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Goal</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Goal</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGoal(Goal object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Task</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Task</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTask(Task object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Putback Task</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Putback Task</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePutbackTask(PutbackTask object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Soft Goal</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Soft Goal</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSoftGoal(SoftGoal object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Operation Task</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Operation Task</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOperationTask(OperationTask object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Condition Task</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Condition Task</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConditionTask(ConditionTask object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Belief</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Belief</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBelief(Belief object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Goal Relationship</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Goal Relationship</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGoalRelationship(GoalRelationship object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Contribution</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Contribution</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseContribution(Contribution object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Decomposition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Decomposition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDecomposition(Decomposition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Means Ends</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Means Ends</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMeansEnds(MeansEnds object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Correlation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Correlation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCorrelation(Correlation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Decomposition Source</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Decomposition Source</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDecompositionSource(DecompositionSource object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Decomposition Target</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Decomposition Target</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDecompositionTarget(DecompositionTarget object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Contribution Source</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Contribution Source</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseContributionSource(ContributionSource object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Contribution Target</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Contribution Target</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseContributionTarget(ContributionTarget object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Means Ends Source</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Means Ends Source</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMeansEndsSource(MeansEndsSource object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Means Ends Target</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Means Ends Target</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMeansEndsTarget(MeansEndsTarget object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Correlation Source</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Correlation Source</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCorrelationSource(CorrelationSource object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Correlation Target</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Correlation Target</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCorrelationTarget(CorrelationTarget object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Multiplicity Gate</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Multiplicity Gate</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMultiplicityGate(MultiplicityGate object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //BxModelSwitch
