/**
 */
package edu.ustb.sei.mde.bxmodel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Soft Goal</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.bxmodel.SoftGoal#getDescription <em>Description</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.bxmodel.SoftGoal#getType <em>Type</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.mde.bxmodel.BxModelPackage#getSoftGoal()
 * @model
 * @generated
 */
public interface SoftGoal extends GoalElement, DecompositionSource, ContributionSource, ContributionTarget, CorrelationSource, CorrelationTarget {
	/**
	 * Returns the value of the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' attribute.
	 * @see #setDescription(String)
	 * @see edu.ustb.sei.mde.bxmodel.BxModelPackage#getSoftGoal_Description()
	 * @model
	 * @generated
	 */
	String getDescription();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.bxmodel.SoftGoal#getDescription <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' attribute.
	 * @see #getDescription()
	 * @generated
	 */
	void setDescription(String value);

	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * The default value is <code>"default"</code>.
	 * The literals are from the enumeration {@link edu.ustb.sei.mde.bxmodel.SoftGoalType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see edu.ustb.sei.mde.bxmodel.SoftGoalType
	 * @see #setType(SoftGoalType)
	 * @see edu.ustb.sei.mde.bxmodel.BxModelPackage#getSoftGoal_Type()
	 * @model default="default" required="true"
	 * @generated
	 */
	SoftGoalType getType();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.bxmodel.SoftGoal#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see edu.ustb.sei.mde.bxmodel.SoftGoalType
	 * @see #getType()
	 * @generated
	 */
	void setType(SoftGoalType value);

} // SoftGoal
