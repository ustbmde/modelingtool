/**
 */
package edu.ustb.sei.mde.bxmodel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Means Ends Target</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.ustb.sei.mde.bxmodel.BxModelPackage#getMeansEndsTarget()
 * @model abstract="true"
 * @generated
 */
public interface MeansEndsTarget extends EObject {
} // MeansEndsTarget
