/**
 */
package edu.ustb.sei.mde.bxmodel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Multiplicity Gate</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.bxmodel.MultiplicityGate#getType <em>Type</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.mde.bxmodel.BxModelPackage#getMultiplicityGate()
 * @model
 * @generated
 */
public interface MultiplicityGate extends GoalElement, Task {
	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * The default value is <code>"one"</code>.
	 * The literals are from the enumeration {@link edu.ustb.sei.mde.bxmodel.MultiplicityGateKind}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see edu.ustb.sei.mde.bxmodel.MultiplicityGateKind
	 * @see #setType(MultiplicityGateKind)
	 * @see edu.ustb.sei.mde.bxmodel.BxModelPackage#getMultiplicityGate_Type()
	 * @model default="one" required="true"
	 * @generated
	 */
	MultiplicityGateKind getType();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.bxmodel.MultiplicityGate#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see edu.ustb.sei.mde.bxmodel.MultiplicityGateKind
	 * @see #getType()
	 * @generated
	 */
	void setType(MultiplicityGateKind value);

} // MultiplicityGate
