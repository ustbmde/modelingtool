/**
 */
package edu.ustb.sei.mde.bxmodel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Means Ends</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.bxmodel.MeansEnds#getSource <em>Source</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.bxmodel.MeansEnds#getTarget <em>Target</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.mde.bxmodel.BxModelPackage#getMeansEnds()
 * @model
 * @generated
 */
public interface MeansEnds extends GoalRelationship {
	/**
	 * Returns the value of the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source</em>' reference.
	 * @see #setSource(MeansEndsSource)
	 * @see edu.ustb.sei.mde.bxmodel.BxModelPackage#getMeansEnds_Source()
	 * @model
	 * @generated
	 */
	MeansEndsSource getSource();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.bxmodel.MeansEnds#getSource <em>Source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source</em>' reference.
	 * @see #getSource()
	 * @generated
	 */
	void setSource(MeansEndsSource value);

	/**
	 * Returns the value of the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target</em>' reference.
	 * @see #setTarget(MeansEndsTarget)
	 * @see edu.ustb.sei.mde.bxmodel.BxModelPackage#getMeansEnds_Target()
	 * @model
	 * @generated
	 */
	MeansEndsTarget getTarget();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.bxmodel.MeansEnds#getTarget <em>Target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target</em>' reference.
	 * @see #getTarget()
	 * @generated
	 */
	void setTarget(MeansEndsTarget value);

} // MeansEnds
