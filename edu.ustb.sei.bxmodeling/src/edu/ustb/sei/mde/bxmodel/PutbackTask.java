/**
 */
package edu.ustb.sei.mde.bxmodel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Putback Task</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.bxmodel.PutbackTask#getSource <em>Source</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.bxmodel.PutbackTask#getView <em>View</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.mde.bxmodel.BxModelPackage#getPutbackTask()
 * @model
 * @generated
 */
public interface PutbackTask extends Task {
	/**
	 * Returns the value of the '<em><b>Source</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source</em>' attribute.
	 * @see #setSource(String)
	 * @see edu.ustb.sei.mde.bxmodel.BxModelPackage#getPutbackTask_Source()
	 * @model
	 * @generated
	 */
	String getSource();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.bxmodel.PutbackTask#getSource <em>Source</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source</em>' attribute.
	 * @see #getSource()
	 * @generated
	 */
	void setSource(String value);

	/**
	 * Returns the value of the '<em><b>View</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>View</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>View</em>' attribute.
	 * @see #setView(String)
	 * @see edu.ustb.sei.mde.bxmodel.BxModelPackage#getPutbackTask_View()
	 * @model
	 * @generated
	 */
	String getView();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.bxmodel.PutbackTask#getView <em>View</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>View</em>' attribute.
	 * @see #getView()
	 * @generated
	 */
	void setView(String value);

} // PutbackTask
