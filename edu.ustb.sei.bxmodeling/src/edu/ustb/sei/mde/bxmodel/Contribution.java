/**
 */
package edu.ustb.sei.mde.bxmodel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Contribution</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.bxmodel.Contribution#getType <em>Type</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.bxmodel.Contribution#getSource <em>Source</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.bxmodel.Contribution#getTarget <em>Target</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.mde.bxmodel.BxModelPackage#getContribution()
 * @model
 * @generated
 */
public interface Contribution extends GoalRelationship {
	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * The default value is <code>"and"</code>.
	 * The literals are from the enumeration {@link edu.ustb.sei.mde.bxmodel.ContributionType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see edu.ustb.sei.mde.bxmodel.ContributionType
	 * @see #setType(ContributionType)
	 * @see edu.ustb.sei.mde.bxmodel.BxModelPackage#getContribution_Type()
	 * @model default="and" required="true"
	 * @generated
	 */
	ContributionType getType();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.bxmodel.Contribution#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see edu.ustb.sei.mde.bxmodel.ContributionType
	 * @see #getType()
	 * @generated
	 */
	void setType(ContributionType value);

	/**
	 * Returns the value of the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source</em>' reference.
	 * @see #setSource(ContributionSource)
	 * @see edu.ustb.sei.mde.bxmodel.BxModelPackage#getContribution_Source()
	 * @model
	 * @generated
	 */
	ContributionSource getSource();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.bxmodel.Contribution#getSource <em>Source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source</em>' reference.
	 * @see #getSource()
	 * @generated
	 */
	void setSource(ContributionSource value);

	/**
	 * Returns the value of the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target</em>' reference.
	 * @see #setTarget(ContributionTarget)
	 * @see edu.ustb.sei.mde.bxmodel.BxModelPackage#getContribution_Target()
	 * @model
	 * @generated
	 */
	ContributionTarget getTarget();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.bxmodel.Contribution#getTarget <em>Target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target</em>' reference.
	 * @see #getTarget()
	 * @generated
	 */
	void setTarget(ContributionTarget value);

} // Contribution
