/**
 */
package edu.ustb.sei.mde.bxmodel;

import org.eclipse.emf.ecore.EObject;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Goal Element</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.ustb.sei.mde.bxmodel.BxModelPackage#getGoalElement()
 * @model abstract="true"
 * @generated
 */
public interface GoalElement extends EObject {
} // GoalElement
