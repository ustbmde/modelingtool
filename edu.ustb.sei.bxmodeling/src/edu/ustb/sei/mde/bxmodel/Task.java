/**
 */
package edu.ustb.sei.mde.bxmodel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Task</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.ustb.sei.mde.bxmodel.BxModelPackage#getTask()
 * @model abstract="true"
 * @generated
 */
public interface Task extends GoalElement, DecompositionSource, DecompositionTarget, MeansEndsSource, ContributionSource, CorrelationSource, CorrelationTarget {
} // Task
