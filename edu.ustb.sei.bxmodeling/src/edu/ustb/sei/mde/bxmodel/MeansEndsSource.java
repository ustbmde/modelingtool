/**
 */
package edu.ustb.sei.mde.bxmodel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Means Ends Source</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.ustb.sei.mde.bxmodel.BxModelPackage#getMeansEndsSource()
 * @model abstract="true"
 * @generated
 */
public interface MeansEndsSource extends EObject {
} // MeansEndsSource
