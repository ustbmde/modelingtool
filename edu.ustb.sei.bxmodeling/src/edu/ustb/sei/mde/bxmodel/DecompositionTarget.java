/**
 */
package edu.ustb.sei.mde.bxmodel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Decomposition Target</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.ustb.sei.mde.bxmodel.BxModelPackage#getDecompositionTarget()
 * @model abstract="true"
 * @generated
 */
public interface DecompositionTarget extends EObject {
} // DecompositionTarget
