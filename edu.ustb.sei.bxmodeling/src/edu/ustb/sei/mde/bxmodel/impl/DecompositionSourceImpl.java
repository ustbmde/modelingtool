/**
 */
package edu.ustb.sei.mde.bxmodel.impl;

import edu.ustb.sei.mde.bxmodel.BxModelPackage;
import edu.ustb.sei.mde.bxmodel.DecompositionSource;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Decomposition Source</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class DecompositionSourceImpl extends MinimalEObjectImpl.Container implements DecompositionSource {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DecompositionSourceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BxModelPackage.Literals.DECOMPOSITION_SOURCE;
	}

} //DecompositionSourceImpl
