/**
 */
package edu.ustb.sei.mde.bxmodel.impl;

import edu.ustb.sei.mde.bxmodel.Belief;
import edu.ustb.sei.mde.bxmodel.BxGoalModel;
import edu.ustb.sei.mde.bxmodel.BxModelFactory;
import edu.ustb.sei.mde.bxmodel.BxModelPackage;
import edu.ustb.sei.mde.bxmodel.ConditionTask;
import edu.ustb.sei.mde.bxmodel.Contribution;
import edu.ustb.sei.mde.bxmodel.ContributionSource;
import edu.ustb.sei.mde.bxmodel.ContributionTarget;
import edu.ustb.sei.mde.bxmodel.ContributionType;
import edu.ustb.sei.mde.bxmodel.Correlation;
import edu.ustb.sei.mde.bxmodel.CorrelationSource;
import edu.ustb.sei.mde.bxmodel.CorrelationTarget;
import edu.ustb.sei.mde.bxmodel.CorrelationType;
import edu.ustb.sei.mde.bxmodel.Decomposition;
import edu.ustb.sei.mde.bxmodel.DecompositionSource;
import edu.ustb.sei.mde.bxmodel.DecompositionTarget;
import edu.ustb.sei.mde.bxmodel.Goal;
import edu.ustb.sei.mde.bxmodel.GoalElement;
import edu.ustb.sei.mde.bxmodel.GoalRelationship;
import edu.ustb.sei.mde.bxmodel.MeansEnds;
import edu.ustb.sei.mde.bxmodel.MeansEndsSource;
import edu.ustb.sei.mde.bxmodel.MeansEndsTarget;
import edu.ustb.sei.mde.bxmodel.ModelContainer;
import edu.ustb.sei.mde.bxmodel.MultiplicityGate;
import edu.ustb.sei.mde.bxmodel.MultiplicityGateKind;
import edu.ustb.sei.mde.bxmodel.OperationTask;
import edu.ustb.sei.mde.bxmodel.PutbackTask;
import edu.ustb.sei.mde.bxmodel.SoftGoal;
import edu.ustb.sei.mde.bxmodel.SoftGoalType;
import edu.ustb.sei.mde.bxmodel.Task;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class BxModelPackageImpl extends EPackageImpl implements BxModelPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass modelContainerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass bxGoalModelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass goalElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass goalEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass taskEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass putbackTaskEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass softGoalEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass operationTaskEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass conditionTaskEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass beliefEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass goalRelationshipEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass contributionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass decompositionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass meansEndsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass correlationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass decompositionSourceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass decompositionTargetEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass contributionSourceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass contributionTargetEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass meansEndsSourceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass meansEndsTargetEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass correlationSourceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass correlationTargetEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass multiplicityGateEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum softGoalTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum contributionTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum correlationTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum multiplicityGateKindEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see edu.ustb.sei.mde.bxmodel.BxModelPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private BxModelPackageImpl() {
		super(eNS_URI, BxModelFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link BxModelPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static BxModelPackage init() {
		if (isInited) return (BxModelPackage)EPackage.Registry.INSTANCE.getEPackage(BxModelPackage.eNS_URI);

		// Obtain or create and register package
		BxModelPackageImpl theBxModelPackage = (BxModelPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof BxModelPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new BxModelPackageImpl());

		isInited = true;

		// Create package meta-data objects
		theBxModelPackage.createPackageContents();

		// Initialize created meta-data
		theBxModelPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theBxModelPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(BxModelPackage.eNS_URI, theBxModelPackage);
		return theBxModelPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getModelContainer() {
		return modelContainerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getModelContainer_Elements() {
		return (EReference)modelContainerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getModelContainer_Relationships() {
		return (EReference)modelContainerEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBxGoalModel() {
		return bxGoalModelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBxGoalModel_Name() {
		return (EAttribute)bxGoalModelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGoalElement() {
		return goalElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGoal() {
		return goalEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGoal_Description() {
		return (EAttribute)goalEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTask() {
		return taskEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPutbackTask() {
		return putbackTaskEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPutbackTask_Source() {
		return (EAttribute)putbackTaskEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPutbackTask_View() {
		return (EAttribute)putbackTaskEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSoftGoal() {
		return softGoalEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSoftGoal_Description() {
		return (EAttribute)softGoalEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSoftGoal_Type() {
		return (EAttribute)softGoalEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOperationTask() {
		return operationTaskEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOperationTask_Operation() {
		return (EAttribute)operationTaskEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getConditionTask() {
		return conditionTaskEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getConditionTask_Condition() {
		return (EAttribute)conditionTaskEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBelief() {
		return beliefEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBelief_Description() {
		return (EAttribute)beliefEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGoalRelationship() {
		return goalRelationshipEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getContribution() {
		return contributionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getContribution_Type() {
		return (EAttribute)contributionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getContribution_Source() {
		return (EReference)contributionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getContribution_Target() {
		return (EReference)contributionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDecomposition() {
		return decompositionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDecomposition_Source() {
		return (EReference)decompositionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDecomposition_Target() {
		return (EReference)decompositionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMeansEnds() {
		return meansEndsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMeansEnds_Source() {
		return (EReference)meansEndsEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMeansEnds_Target() {
		return (EReference)meansEndsEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCorrelation() {
		return correlationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCorrelation_Source() {
		return (EReference)correlationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCorrelation_Target() {
		return (EReference)correlationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCorrelation_Type() {
		return (EAttribute)correlationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDecompositionSource() {
		return decompositionSourceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDecompositionTarget() {
		return decompositionTargetEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getContributionSource() {
		return contributionSourceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getContributionTarget() {
		return contributionTargetEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMeansEndsSource() {
		return meansEndsSourceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMeansEndsTarget() {
		return meansEndsTargetEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCorrelationSource() {
		return correlationSourceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCorrelationTarget() {
		return correlationTargetEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMultiplicityGate() {
		return multiplicityGateEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMultiplicityGate_Type() {
		return (EAttribute)multiplicityGateEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getSoftGoalType() {
		return softGoalTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getContributionType() {
		return contributionTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getCorrelationType() {
		return correlationTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getMultiplicityGateKind() {
		return multiplicityGateKindEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BxModelFactory getBxModelFactory() {
		return (BxModelFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		modelContainerEClass = createEClass(MODEL_CONTAINER);
		createEReference(modelContainerEClass, MODEL_CONTAINER__ELEMENTS);
		createEReference(modelContainerEClass, MODEL_CONTAINER__RELATIONSHIPS);

		bxGoalModelEClass = createEClass(BX_GOAL_MODEL);
		createEAttribute(bxGoalModelEClass, BX_GOAL_MODEL__NAME);

		goalElementEClass = createEClass(GOAL_ELEMENT);

		goalEClass = createEClass(GOAL);
		createEAttribute(goalEClass, GOAL__DESCRIPTION);

		taskEClass = createEClass(TASK);

		putbackTaskEClass = createEClass(PUTBACK_TASK);
		createEAttribute(putbackTaskEClass, PUTBACK_TASK__SOURCE);
		createEAttribute(putbackTaskEClass, PUTBACK_TASK__VIEW);

		softGoalEClass = createEClass(SOFT_GOAL);
		createEAttribute(softGoalEClass, SOFT_GOAL__DESCRIPTION);
		createEAttribute(softGoalEClass, SOFT_GOAL__TYPE);

		operationTaskEClass = createEClass(OPERATION_TASK);
		createEAttribute(operationTaskEClass, OPERATION_TASK__OPERATION);

		conditionTaskEClass = createEClass(CONDITION_TASK);
		createEAttribute(conditionTaskEClass, CONDITION_TASK__CONDITION);

		beliefEClass = createEClass(BELIEF);
		createEAttribute(beliefEClass, BELIEF__DESCRIPTION);

		goalRelationshipEClass = createEClass(GOAL_RELATIONSHIP);

		contributionEClass = createEClass(CONTRIBUTION);
		createEAttribute(contributionEClass, CONTRIBUTION__TYPE);
		createEReference(contributionEClass, CONTRIBUTION__SOURCE);
		createEReference(contributionEClass, CONTRIBUTION__TARGET);

		decompositionEClass = createEClass(DECOMPOSITION);
		createEReference(decompositionEClass, DECOMPOSITION__SOURCE);
		createEReference(decompositionEClass, DECOMPOSITION__TARGET);

		meansEndsEClass = createEClass(MEANS_ENDS);
		createEReference(meansEndsEClass, MEANS_ENDS__SOURCE);
		createEReference(meansEndsEClass, MEANS_ENDS__TARGET);

		correlationEClass = createEClass(CORRELATION);
		createEReference(correlationEClass, CORRELATION__SOURCE);
		createEReference(correlationEClass, CORRELATION__TARGET);
		createEAttribute(correlationEClass, CORRELATION__TYPE);

		decompositionSourceEClass = createEClass(DECOMPOSITION_SOURCE);

		decompositionTargetEClass = createEClass(DECOMPOSITION_TARGET);

		contributionSourceEClass = createEClass(CONTRIBUTION_SOURCE);

		contributionTargetEClass = createEClass(CONTRIBUTION_TARGET);

		meansEndsSourceEClass = createEClass(MEANS_ENDS_SOURCE);

		meansEndsTargetEClass = createEClass(MEANS_ENDS_TARGET);

		correlationSourceEClass = createEClass(CORRELATION_SOURCE);

		correlationTargetEClass = createEClass(CORRELATION_TARGET);

		multiplicityGateEClass = createEClass(MULTIPLICITY_GATE);
		createEAttribute(multiplicityGateEClass, MULTIPLICITY_GATE__TYPE);

		// Create enums
		softGoalTypeEEnum = createEEnum(SOFT_GOAL_TYPE);
		contributionTypeEEnum = createEEnum(CONTRIBUTION_TYPE);
		correlationTypeEEnum = createEEnum(CORRELATION_TYPE);
		multiplicityGateKindEEnum = createEEnum(MULTIPLICITY_GATE_KIND);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		bxGoalModelEClass.getESuperTypes().add(this.getModelContainer());
		goalEClass.getESuperTypes().add(this.getGoalElement());
		goalEClass.getESuperTypes().add(this.getDecompositionSource());
		goalEClass.getESuperTypes().add(this.getDecompositionTarget());
		goalEClass.getESuperTypes().add(this.getMeansEndsTarget());
		goalEClass.getESuperTypes().add(this.getCorrelationTarget());
		taskEClass.getESuperTypes().add(this.getGoalElement());
		taskEClass.getESuperTypes().add(this.getDecompositionSource());
		taskEClass.getESuperTypes().add(this.getDecompositionTarget());
		taskEClass.getESuperTypes().add(this.getMeansEndsSource());
		taskEClass.getESuperTypes().add(this.getContributionSource());
		taskEClass.getESuperTypes().add(this.getCorrelationSource());
		taskEClass.getESuperTypes().add(this.getCorrelationTarget());
		putbackTaskEClass.getESuperTypes().add(this.getTask());
		softGoalEClass.getESuperTypes().add(this.getGoalElement());
		softGoalEClass.getESuperTypes().add(this.getDecompositionSource());
		softGoalEClass.getESuperTypes().add(this.getContributionSource());
		softGoalEClass.getESuperTypes().add(this.getContributionTarget());
		softGoalEClass.getESuperTypes().add(this.getCorrelationSource());
		softGoalEClass.getESuperTypes().add(this.getCorrelationTarget());
		operationTaskEClass.getESuperTypes().add(this.getTask());
		conditionTaskEClass.getESuperTypes().add(this.getTask());
		beliefEClass.getESuperTypes().add(this.getGoalElement());
		beliefEClass.getESuperTypes().add(this.getContributionSource());
		beliefEClass.getESuperTypes().add(this.getContributionTarget());
		beliefEClass.getESuperTypes().add(this.getDecompositionSource());
		goalRelationshipEClass.getESuperTypes().add(this.getContributionSource());
		goalRelationshipEClass.getESuperTypes().add(this.getContributionTarget());
		contributionEClass.getESuperTypes().add(this.getGoalRelationship());
		decompositionEClass.getESuperTypes().add(this.getGoalRelationship());
		meansEndsEClass.getESuperTypes().add(this.getGoalRelationship());
		correlationEClass.getESuperTypes().add(this.getGoalRelationship());
		multiplicityGateEClass.getESuperTypes().add(this.getGoalElement());
		multiplicityGateEClass.getESuperTypes().add(this.getTask());

		// Initialize classes, features, and operations; add parameters
		initEClass(modelContainerEClass, ModelContainer.class, "ModelContainer", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getModelContainer_Elements(), this.getGoalElement(), null, "elements", null, 0, -1, ModelContainer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getModelContainer_Relationships(), this.getGoalRelationship(), null, "relationships", null, 0, -1, ModelContainer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(bxGoalModelEClass, BxGoalModel.class, "BxGoalModel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getBxGoalModel_Name(), ecorePackage.getEString(), "name", null, 0, 1, BxGoalModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(goalElementEClass, GoalElement.class, "GoalElement", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(goalEClass, Goal.class, "Goal", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getGoal_Description(), ecorePackage.getEString(), "description", null, 0, 1, Goal.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(taskEClass, Task.class, "Task", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(putbackTaskEClass, PutbackTask.class, "PutbackTask", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPutbackTask_Source(), ecorePackage.getEString(), "source", null, 0, 1, PutbackTask.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPutbackTask_View(), ecorePackage.getEString(), "view", null, 0, 1, PutbackTask.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(softGoalEClass, SoftGoal.class, "SoftGoal", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSoftGoal_Description(), ecorePackage.getEString(), "description", null, 0, 1, SoftGoal.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSoftGoal_Type(), this.getSoftGoalType(), "type", "default", 1, 1, SoftGoal.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(operationTaskEClass, OperationTask.class, "OperationTask", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getOperationTask_Operation(), ecorePackage.getEString(), "operation", null, 0, 1, OperationTask.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(conditionTaskEClass, ConditionTask.class, "ConditionTask", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getConditionTask_Condition(), ecorePackage.getEString(), "condition", null, 0, 1, ConditionTask.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(beliefEClass, Belief.class, "Belief", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getBelief_Description(), ecorePackage.getEString(), "description", null, 0, 1, Belief.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(goalRelationshipEClass, GoalRelationship.class, "GoalRelationship", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(contributionEClass, Contribution.class, "Contribution", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getContribution_Type(), this.getContributionType(), "type", "and", 1, 1, Contribution.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getContribution_Source(), this.getContributionSource(), null, "source", null, 0, 1, Contribution.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getContribution_Target(), this.getContributionTarget(), null, "target", null, 0, 1, Contribution.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(decompositionEClass, Decomposition.class, "Decomposition", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDecomposition_Source(), this.getDecompositionSource(), null, "source", null, 0, 1, Decomposition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDecomposition_Target(), this.getDecompositionTarget(), null, "target", null, 0, 1, Decomposition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(meansEndsEClass, MeansEnds.class, "MeansEnds", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMeansEnds_Source(), this.getMeansEndsSource(), null, "source", null, 0, 1, MeansEnds.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMeansEnds_Target(), this.getMeansEndsTarget(), null, "target", null, 0, 1, MeansEnds.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(correlationEClass, Correlation.class, "Correlation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCorrelation_Source(), this.getCorrelationSource(), null, "source", null, 0, 1, Correlation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCorrelation_Target(), this.getCorrelationTarget(), null, "target", null, 0, 1, Correlation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCorrelation_Type(), this.getCorrelationType(), "type", "help", 1, 1, Correlation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(decompositionSourceEClass, DecompositionSource.class, "DecompositionSource", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(decompositionTargetEClass, DecompositionTarget.class, "DecompositionTarget", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(contributionSourceEClass, ContributionSource.class, "ContributionSource", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(contributionTargetEClass, ContributionTarget.class, "ContributionTarget", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(meansEndsSourceEClass, MeansEndsSource.class, "MeansEndsSource", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(meansEndsTargetEClass, MeansEndsTarget.class, "MeansEndsTarget", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(correlationSourceEClass, CorrelationSource.class, "CorrelationSource", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(correlationTargetEClass, CorrelationTarget.class, "CorrelationTarget", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(multiplicityGateEClass, MultiplicityGate.class, "MultiplicityGate", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getMultiplicityGate_Type(), this.getMultiplicityGateKind(), "type", "one", 1, 1, MultiplicityGate.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(softGoalTypeEEnum, SoftGoalType.class, "SoftGoalType");
		addEEnumLiteral(softGoalTypeEEnum, SoftGoalType.DEFAULT);
		addEEnumLiteral(softGoalTypeEEnum, SoftGoalType.SOURCE);
		addEEnumLiteral(softGoalTypeEEnum, SoftGoalType.VIEW);
		addEEnumLiteral(softGoalTypeEEnum, SoftGoalType.LIMITATION);

		initEEnum(contributionTypeEEnum, ContributionType.class, "ContributionType");
		addEEnumLiteral(contributionTypeEEnum, ContributionType.AND);
		addEEnumLiteral(contributionTypeEEnum, ContributionType.OR);
		addEEnumLiteral(contributionTypeEEnum, ContributionType.MAKE);
		addEEnumLiteral(contributionTypeEEnum, ContributionType.BREAK);
		addEEnumLiteral(contributionTypeEEnum, ContributionType.HELP);
		addEEnumLiteral(contributionTypeEEnum, ContributionType.HURT);

		initEEnum(correlationTypeEEnum, CorrelationType.class, "CorrelationType");
		addEEnumLiteral(correlationTypeEEnum, CorrelationType.MAKE);
		addEEnumLiteral(correlationTypeEEnum, CorrelationType.BREAK);
		addEEnumLiteral(correlationTypeEEnum, CorrelationType.HELP);
		addEEnumLiteral(correlationTypeEEnum, CorrelationType.HURT);

		initEEnum(multiplicityGateKindEEnum, MultiplicityGateKind.class, "MultiplicityGateKind");
		addEEnumLiteral(multiplicityGateKindEEnum, MultiplicityGateKind.LONE);
		addEEnumLiteral(multiplicityGateKindEEnum, MultiplicityGateKind.ONE);
		addEEnumLiteral(multiplicityGateKindEEnum, MultiplicityGateKind.SOME);

		// Create resource
		createResource(eNS_URI);
	}

} //BxModelPackageImpl
