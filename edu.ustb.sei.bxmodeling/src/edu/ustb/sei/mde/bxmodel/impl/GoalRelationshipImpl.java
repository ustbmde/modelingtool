/**
 */
package edu.ustb.sei.mde.bxmodel.impl;

import edu.ustb.sei.mde.bxmodel.BxModelPackage;
import edu.ustb.sei.mde.bxmodel.GoalRelationship;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Goal Relationship</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class GoalRelationshipImpl extends ContributionSourceImpl implements GoalRelationship {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected GoalRelationshipImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BxModelPackage.Literals.GOAL_RELATIONSHIP;
	}

} //GoalRelationshipImpl
