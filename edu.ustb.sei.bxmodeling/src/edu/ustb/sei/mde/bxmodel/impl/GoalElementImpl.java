/**
 */
package edu.ustb.sei.mde.bxmodel.impl;

import edu.ustb.sei.mde.bxmodel.BxModelPackage;
import edu.ustb.sei.mde.bxmodel.GoalElement;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Goal Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class GoalElementImpl extends MinimalEObjectImpl.Container implements GoalElement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected GoalElementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BxModelPackage.Literals.GOAL_ELEMENT;
	}

} //GoalElementImpl
