/**
 */
package edu.ustb.sei.mde.bxmodel.impl;

import edu.ustb.sei.mde.bxmodel.BxModelPackage;
import edu.ustb.sei.mde.bxmodel.ContributionTarget;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Contribution Target</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class ContributionTargetImpl extends MinimalEObjectImpl.Container implements ContributionTarget {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ContributionTargetImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BxModelPackage.Literals.CONTRIBUTION_TARGET;
	}

} //ContributionTargetImpl
