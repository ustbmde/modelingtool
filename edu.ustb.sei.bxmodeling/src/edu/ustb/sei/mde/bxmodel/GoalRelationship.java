/**
 */
package edu.ustb.sei.mde.bxmodel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Goal Relationship</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.ustb.sei.mde.bxmodel.BxModelPackage#getGoalRelationship()
 * @model abstract="true"
 * @generated
 */
public interface GoalRelationship extends ContributionSource, ContributionTarget {
} // GoalRelationship
