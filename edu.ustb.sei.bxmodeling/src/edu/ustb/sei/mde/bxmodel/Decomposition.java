/**
 */
package edu.ustb.sei.mde.bxmodel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Decomposition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.bxmodel.Decomposition#getSource <em>Source</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.bxmodel.Decomposition#getTarget <em>Target</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.mde.bxmodel.BxModelPackage#getDecomposition()
 * @model
 * @generated
 */
public interface Decomposition extends GoalRelationship {
	/**
	 * Returns the value of the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source</em>' reference.
	 * @see #setSource(DecompositionSource)
	 * @see edu.ustb.sei.mde.bxmodel.BxModelPackage#getDecomposition_Source()
	 * @model
	 * @generated
	 */
	DecompositionSource getSource();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.bxmodel.Decomposition#getSource <em>Source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source</em>' reference.
	 * @see #getSource()
	 * @generated
	 */
	void setSource(DecompositionSource value);

	/**
	 * Returns the value of the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target</em>' reference.
	 * @see #setTarget(DecompositionTarget)
	 * @see edu.ustb.sei.mde.bxmodel.BxModelPackage#getDecomposition_Target()
	 * @model
	 * @generated
	 */
	DecompositionTarget getTarget();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.bxmodel.Decomposition#getTarget <em>Target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target</em>' reference.
	 * @see #getTarget()
	 * @generated
	 */
	void setTarget(DecompositionTarget value);

} // Decomposition
