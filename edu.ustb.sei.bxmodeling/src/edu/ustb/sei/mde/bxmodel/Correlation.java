/**
 */
package edu.ustb.sei.mde.bxmodel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Correlation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link edu.ustb.sei.mde.bxmodel.Correlation#getSource <em>Source</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.bxmodel.Correlation#getTarget <em>Target</em>}</li>
 *   <li>{@link edu.ustb.sei.mde.bxmodel.Correlation#getType <em>Type</em>}</li>
 * </ul>
 *
 * @see edu.ustb.sei.mde.bxmodel.BxModelPackage#getCorrelation()
 * @model
 * @generated
 */
public interface Correlation extends GoalRelationship {
	/**
	 * Returns the value of the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source</em>' reference.
	 * @see #setSource(CorrelationSource)
	 * @see edu.ustb.sei.mde.bxmodel.BxModelPackage#getCorrelation_Source()
	 * @model
	 * @generated
	 */
	CorrelationSource getSource();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.bxmodel.Correlation#getSource <em>Source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source</em>' reference.
	 * @see #getSource()
	 * @generated
	 */
	void setSource(CorrelationSource value);

	/**
	 * Returns the value of the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target</em>' reference.
	 * @see #setTarget(CorrelationTarget)
	 * @see edu.ustb.sei.mde.bxmodel.BxModelPackage#getCorrelation_Target()
	 * @model
	 * @generated
	 */
	CorrelationTarget getTarget();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.bxmodel.Correlation#getTarget <em>Target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target</em>' reference.
	 * @see #getTarget()
	 * @generated
	 */
	void setTarget(CorrelationTarget value);

	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * The default value is <code>"help"</code>.
	 * The literals are from the enumeration {@link edu.ustb.sei.mde.bxmodel.CorrelationType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see edu.ustb.sei.mde.bxmodel.CorrelationType
	 * @see #setType(CorrelationType)
	 * @see edu.ustb.sei.mde.bxmodel.BxModelPackage#getCorrelation_Type()
	 * @model default="help" required="true"
	 * @generated
	 */
	CorrelationType getType();

	/**
	 * Sets the value of the '{@link edu.ustb.sei.mde.bxmodel.Correlation#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see edu.ustb.sei.mde.bxmodel.CorrelationType
	 * @see #getType()
	 * @generated
	 */
	void setType(CorrelationType value);

} // Correlation
