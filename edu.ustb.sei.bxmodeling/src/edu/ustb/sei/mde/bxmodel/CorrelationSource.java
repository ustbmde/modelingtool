/**
 */
package edu.ustb.sei.mde.bxmodel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Correlation Source</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see edu.ustb.sei.mde.bxmodel.BxModelPackage#getCorrelationSource()
 * @model abstract="true"
 * @generated
 */
public interface CorrelationSource extends EObject {
} // CorrelationSource
