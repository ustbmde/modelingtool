package edu.ustb.sei.mde.modeling.ui;

import java.io.PrintStream;

import org.eclipse.swt.graphics.Color;
import org.eclipse.ui.console.MessageConsoleStream;

public class ConsolePrinter extends PrintStream {
	private MessageConsoleStream stream;

	public int getFontStyle() {
		return stream.getFontStyle();
	}

	public void setFontStyle(int newFontStyle) {
		stream.setFontStyle(newFontStyle);
	}

	public boolean isActivateOnWrite() {
		return stream.isActivateOnWrite();
	}

	public void setActivateOnWrite(boolean activateOnWrite) {
		stream.setActivateOnWrite(activateOnWrite);
	}

	public void setColor(Color newColor) {
		stream.setColor(newColor);
	}

	public Color getColor() {
		return stream.getColor();
	}

	public void setEncoding(String encoding) {
		stream.setEncoding(encoding);
	}

	public ConsolePrinter(MessageConsoleStream stream) {
		super(stream);
		this.stream = stream;
	} 

}
