package edu.ustb.sei.mde.modeling.ui;

import java.util.HashMap;

import org.eclipse.ui.console.ConsolePlugin;
import org.eclipse.ui.console.IConsole;
import org.eclipse.ui.console.IConsoleFactory;
import org.eclipse.ui.console.IConsoleManager;
import org.eclipse.ui.console.MessageConsole;
import org.eclipse.ui.console.MessageConsoleStream;

public class ConsoleUtil implements IConsoleFactory {
	
	private static final String USTB_MDE = "USTB MDE";
	//private static MessageConsole console = new MessageConsole(USTB_MDE, null);  
	private static HashMap<String, MessageConsole> consoleMap = new HashMap<String, MessageConsole>();
	
	//static boolean exists = false;  
	
	/** 
	 * ����:�򿪿���̨ 
	 * */  
	public void openConsole() {  
		showConsole();  
	}  
	
	/** */  
	/** 
	 * ����:��ʾ����̨ 
	 * */  
	private static void showConsole() {  
		showConsole(USTB_MDE);
	}
	
	private static MessageConsole showConsole(String head) {  
		IConsoleManager manager = ConsolePlugin.getDefault()
				.getConsoleManager();
		
		MessageConsole console = consoleMap.get(head);
		
		if(console==null) {
			console = new MessageConsole(head, null);
			consoleMap.put(head, console);
		}

		// �õ����еĿ���̨ʵ��  
		IConsole[] existing = manager.getConsoles();  
		boolean exists = false;  
		// �´�����MessageConsoleʵ�������ھͼ��뵽����̨������������ʾ����  
		for (int i = 0; i < existing.length; i++) {  
			if (console == existing[i])  
				exists = true;  
		}  
		if (!exists) {  
			manager.addConsoles(new IConsole[] { console });  
		} 
		//console.activate(); 
		return console;
	}
	
	
	/** */  
	/** 
	 * ����:�رտ���̨ 
	 * */  
	public static void closeConsole() {  
		closeConsole(USTB_MDE); 
	}  
	
	public static void closeConsole(String head) {  
		IConsoleManager manager = ConsolePlugin.getDefault()  
				.getConsoleManager(); 
		
		MessageConsole console = consoleMap.get(head);
		
		if (console != null) {  
			manager.removeConsoles(new IConsole[] { console });  
		}  
	} 
	
	/** 
	 * ��ȡ����̨ 
	 *  
	 * @return 
	 */  
	public static MessageConsole getConsole() {  
		return getConsole(USTB_MDE);
	}
	
	public static MessageConsole getConsole(String head) {  
		return showConsole(head);   
	}
	
	public static void printToConsole(String message, String title, boolean activate) {
		printToConsole(USTB_MDE,message, title,activate);
	}
	
	public static void printToConsole(String head, String message, String title, boolean activate) { 
		MessageConsoleStream printer = ConsoleUtil.getConsole(head).newMessageStream();
		printer.setActivateOnWrite(activate);
		
		if(title==null) {
			printer.println(message);
		} else {
			if(message.indexOf('\n')!=-1) {
				printer.println(title+":");
				printer.println(message);
			} else {
				printer.println(title+":" + message);
			}		
		}
	}

//	public static ConsolePrinter getMessageWriter(String head) {
//		MessageConsoleStream printer = ConsoleUtil.getConsole(head).newMessageStream();
//		return new ConsolePrinter(printer);
//	}
	
	public static void clearConsole(String head) {
		ConsoleUtil.getConsole(head).clearConsole();
	}
	
	
}