package edu.ustb.sei.mde.modeling.ui;

import java.util.Arrays;
import java.util.Map;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.plugin.EcorePlugin;
import org.eclipse.emf.ecore.presentation.EcoreEditorPlugin;
import org.eclipse.emf.ecore.provider.EcoreEditPlugin;
import org.eclipse.emf.edit.ui.provider.ExtendedImageRegistry;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.dialogs.ElementListSelectionDialog;

/**
 * @since 2.9
 */
public class TargetPlatformPackageDialog extends ElementListSelectionDialog
{
  public TargetPlatformPackageDialog(Shell parent)
  {
    super
      (parent,
       new LabelProvider()
       {
         @Override
        public Image getImage(Object element)
         {
           return ExtendedImageRegistry.getInstance().getImage(EcoreEditPlugin.INSTANCE.getImage("full/obj16/EPackage"));
         }
       });

    setMultipleSelection(true);
    setMessage(EcoreEditorPlugin.INSTANCE.getString("_UI_SelectRegisteredPackageURI"));
    setFilter("*");
    setTitle(EcoreEditorPlugin.INSTANCE.getString("_UI_PackageSelection_label"));
  }

  protected void updateElements()
  {
    Map<String, URI> ePackageNsURItoGenModelLocationMap = EcorePlugin.getEPackageNsURIToGenModelLocationMap(true);
    Object [] result = ePackageNsURItoGenModelLocationMap.keySet().toArray(new Object[ePackageNsURItoGenModelLocationMap.size()]);
    Arrays.sort(result);
    setListElements(result);
  }

  @Override
  protected Control createDialogArea(Composite parent)
  {
    Composite result = (Composite)super.createDialogArea(parent);
    updateElements();
    return result;
  }
}