package edu.ustb.sei.mde.modeling.util;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;

public class ResourceUtil {

	public ResourceUtil() {
		
	}
	
	static public EClass searchClassByName(ResourceSet rs, String name) {
		if(name == null || name.length() == 0)
			return null;
		
		for(Resource r : rs.getResources()) {
			EClass type = searchClassByName(r, name);
			if(type!=null) return type;
		}
		return null;
	}

	public static EClass searchClassByName(Resource r, String name) {
		TreeIterator<EObject> it =r.getAllContents();
		while(it.hasNext()) {
			EObject o = it.next();
			if(o instanceof EClass) {
				if(name.equals(((EClass) o).getName())){
					return (EClass)o;
				}
			}
		}
		return null;
	}
	
	
	static public EReference searchReferenceByName(ResourceSet rs, String name, EClass source, EClass target) {
		if(name == null || name.length() == 0)
			return null;
		
		if(source!=null) {
			for(EReference r : source.getEAllReferences()) {
				if((target==null || r.getEReferenceType().isSuperTypeOf(target)) && name.equals(r.getName()))
					return r;
			}
		} else {
			for(Resource r : rs.getResources()) {
				TreeIterator<EObject> it =r.getAllContents();
				while(it.hasNext()) {
					EObject o = it.next();
					if(o instanceof EClass) {
						for(EReference ref : ((EClass) o).getEAllReferences()) {
							if((target==null || ref.getEReferenceType().isSuperTypeOf(target)) && name.equals(ref.getName()))
								return ref;
						}
					}
				}
			}
		}
		return null;
	}
	
	static public EAttribute searchAttributeByName(ResourceSet rs, String name, EClass owner) {
		if(name == null || name.length() == 0)
			return null;
		
		if(owner!=null) {
			for(EAttribute r : owner.getEAllAttributes()) {
				if(name.equals(r.getName()))
					return r;
			}
		} else {
			for(Resource r : rs.getResources()) {
				TreeIterator<EObject> it =r.getAllContents();
				while(it.hasNext()) {
					EObject o = it.next();
					if(o instanceof EClass) {
						for(EAttribute ref : ((EClass) o).getEAllAttributes()) {
							if(name.equals(ref.getName()))
								return ref;
						}
					}
				}
			}
		}
		return null;
	}

}
