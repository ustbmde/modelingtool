package edu.ustb.sei.mde.modeling.action;

import org.eclipse.emf.edit.ui.action.LoadResourceAction;
import org.eclipse.ui.PlatformUI;

public class ExtendedLoadResourceAction extends LoadResourceAction {
	  @Override
	  public void run()
	  {
		  edu.ustb.sei.mde.modeling.ui.ExtendedLoadResourceDialog loadResourceDialog =
	      new edu.ustb.sei.mde.modeling.ui.ExtendedLoadResourceDialog
	          (PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), domain, false, true);
	  
	    loadResourceDialog.open();
	  }
}
